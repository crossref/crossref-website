# .gitlab-ci.yml file to be placed in the root of your repository

variables:
  SLACK_BOT_CHANNEL: $SLACK_BOT_CHANNEL
  SLACK_BOT_TOKEN: $SLACK_BOT_TOKEN
  ALGOLIA_APP_ID: $ALGOLIA_APP_ID
  ALGOLIA_API_KEY: $ALGOLIA_API_KEY

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

stages:
  - preamble
  - member_data
  - build_hugo
  - algolia_index
  - build_container
  - deploy
  - postscript


default:
  before_script:
    # Short name of the committer and the commit message without EOL
    - export CR_AUTHOR_NAME="$(git log -1 ${CI_COMMIT_SHA} --pretty="%aN")";
    - export CR_COMMIT_MESSAGE="$(echo -n ${CI_COMMIT_MESSAGE})";
    - export CR_DEFAULT_COLOR="#FF99FF"
    # Environment dependent variables
    - if [ "${CI_COMMIT_REF_NAME}" == "main" ]; then
      export AWS_DEFAULT_PROFILE="production"
      export CR_SITE_URL="https://www.crossref.org";
      export CR_BRANCH_COLOR="#EF3340";
      export HUGO_CONFIG="config.toml,config-production.toml";
      fi
    - if [ "${CI_COMMIT_REF_NAME}" == "staging" ]; then
      export AWS_DEFAULT_PROFILE="staging"
      export CR_SITE_URL="https://www.staging.crossref.org";
      export CR_BRANCH_COLOR="#FFC72C";
      export HUGO_CONFIG="config.toml,config-staging.toml";
      fi
    - if [ "${CI_COMMIT_REF_NAME}" == "sandbox" ]; then
      export AWS_DEFAULT_PROFILE="sandbox"
      export CR_SITE_URL="https://www.sandbox.crossref.org";
      export CR_BRANCH_COLOR="#3EB1C8";
      export HUGO_CONFIG="config.toml,config-sandbox.toml";
      fi


# Update slack; Use artifacts to have the start time persist through the life of the pipeline run
preamble_to_slack:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: preamble
  tags:
    - aws
    - ec2
    - amd64
  script:
    # Use artifacts to persist the start timestamp through the life of the pipeline run
    - date "+%s" > START_TIME.txt
    - echo "_Deploying the website *${CI_COMMIT_REF_NAME}* branch to the *${AWS_DEFAULT_PROFILE}* environment_" | slacktee.sh -p -a "${CR_DEFAULT_COLOR}" -c $SLACK_BOT_CHANNEL
    - echo "*Commit info:* (<https://gitlab.com/crossref/crossref-website/-/commit/${CI_COMMIT_SHA}|${CI_COMMIT_SHORT_SHA}>) \"${CR_COMMIT_MESSAGE}\" added by \"${CR_AUTHOR_NAME}\" " | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
    - echo "*Gitlab pipeline:* <https://gitlab.com/crossref/crossref-website/-/pipelines/${CI_PIPELINE_ID}|${CI_PIPELINE_ID}>" | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
  artifacts:
    paths:
      - START_TIME.txt
  only:
    - sandbox
    - staging
    - main


# Download the members list from the api-summaries nightly archive
get_member_data:
  stage: member_data
  image: alpine:latest
  tags:
    - aws
    - ec2
    - amd64
  script:
    - export JOB_START=$(date +%s)
    # Add job dependencies, including for slackbot
    - apk add bash curl
    - echo "Preparing member data..." | ./bin/slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
    - wget -O artifacts.zip https://gitlab.com/crossref/api-summaries/-/jobs/artifacts/main/download?job=annotate
    # Extract only the members json
    - unzip -p artifacts.zip data/members.json > data/dashboard/member-list.json
    - echo "_Member data prepared in $(($(date +%s) - $JOB_START)) seconds_" | ./bin/slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
  artifacts:
    paths:
      - data/dashboard/member-list.json
  only:
    - sandbox
    - staging
    - main


# Build the hugo site
build_site:
  stage: build_hugo
  image: alpine:3.20.2
  tags:
    - aws
    - ec2
    - amd64
  script:
    - export JOB_START=$(date +%s)
    # Add job dependencies, including for slackbot
    - apk add git hugo bash curl
    - echo "Building the hugo site..." | ./bin/slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
    - hugo --minify --config "${HUGO_CONFIG}" | ./bin/slacktee.sh -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL 
    # Create secure.txt
    - ./scripts/create-secure-txt.sh
    - echo "_Site build completed in $(($(date +%s) - $JOB_START)) seconds_" | ./bin/slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
  artifacts:
    paths:
      - public/
  only:
    - sandbox
    - staging
    - main


# Create the algolia search index
create_algolia_index:
  stage: algolia_index
  image: alpine:latest
  tags:
    - aws
    - ec2
    - amd64
  script:
    - export JOB_START=$(date +%s)
    # Add job dependencies, including for slackbot
    - apk add git bash curl jq
    - echo "Creating the Algolia search index..." | ./bin/slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
    - wget https://github.com/algolia/cli/releases/download/v1.6.11/algolia_1.6.11_linux_amd64.tar.gz
    # - wget https://github.com/algolia/cli/releases/download/v1.3.2/algolia_1.3.2_linux_amd64.tar.gz
    - tar zxf algolia_1.6.11_linux_amd64.tar.gz
    - cp algolia_1.6.11_linux_amd64/algolia /bin
    - ./bin/indexer ${AWS_DEFAULT_PROFILE} | ./bin/slacktee.sh -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
    - echo "_Search index created in $(($(date +%s) - $JOB_START)) seconds_" | ./bin/slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL

  only:
    - sandbox
    - staging
    - main


# Build the docker image
build_image_amd64:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: build_container
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  script:
    - export JOB_START=$(date +%s)
    - echo "Creating the docker image..." | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
    - AWS_DEFAULT_PROFILE=portal build_container_image.sh $AWS_PRIMARY_REGION $AWS_ACCOUNT_ID_PORTAL "amd64" "${CI_PROJECT_DIR}/Dockerfile" www $CI_COMMIT_SHORT_SHA $CI_PROJECT_DIR
    - echo "_Docker image created in $(($(date +%s) - $JOB_START)) seconds_" | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
  only:
    - sandbox
    - staging
    - main


# Create a manifest file even though we only have one arch type
multi_arch_manifest:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: build_container
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  script:
    - export JOB_START=$(date +%s)
    - echo "Creating the ECR manifest..." | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
    - AWS_DEFAULT_PROFILE=portal create_singlearch_manifest.sh $AWS_PRIMARY_REGION $AWS_ACCOUNT_ID_PORTAL www $CI_COMMIT_SHORT_SHA "amd64"
    - echo "_Manifest created in $(($(date +%s) - $JOB_START)) seconds_" | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
  needs: ["build_image_amd64"]
  only:
    - sandbox
    - staging
    - main


# Build image and deploy to env
deploy_www:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  script:
    - export JOB_START=$(date +%s)
    - echo "Deploying the website container to ${AWS_DEFAULT_PROFILE}..." | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
    - deploy_container.sh $AWS_PRIMARY_REGION micro-${AWS_DEFAULT_PROFILE} www-${AWS_DEFAULT_PROFILE} www-${AWS_DEFAULT_PROFILE} $CI_COMMIT_SHORT_SHA
    - echo "_Website successfully deployed in $(($(date +%s) - $JOB_START)) seconds_" | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL
  only:
    - sandbox
    - staging
    - main


# Display the total elapsed time in slack
show_elapsed_time:
  stage: postscript
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  tags:
    - aws
    - ec2
    - amd64
  script:
    - export START_TIME=$(cat START_TIME.txt)
    - echo "View the website at ${CR_SITE_URL}" | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL;
    - echo "_Total build and publish process took $(($(date +%s)- $START_TIME)) seconds_" | slacktee.sh -p -a "${CR_BRANCH_COLOR}" -c $SLACK_BOT_CHANNEL;
  only:
    - sandbox
    - staging
    - main

