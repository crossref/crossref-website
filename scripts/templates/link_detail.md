The link with the href `{{ href }}`   
{% for name, pages in names.items() %} 
{% if name == "''" %}
Has no link text on {{ pages|length }} pages.
{% else %}
 Is linked with the text {{ name }} on {{ pages|length }} pages.
{% endif %}
For example:

- {{ pages.pop() }}

{% endfor %}

---




