#!/bin/sh
mkdir -p public/.well-known
echo -en "Contact: security@crossref.org\nPreferred-Languages: en\n" > public/.well-known/security.txt
date -d@"$((`date +%s`+90*86400))" "+Expires: %Y-%m-%dT%H:%M:%Sz" >> public/.well-known/security.txt

