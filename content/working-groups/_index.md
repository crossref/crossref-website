+++
title = "Working groups"
date = "2020-11-24"
draft = false
author = "Kornelia Korzec"
aliases = [
    "/fundref.crossref.org/working-groups/"
]
[menu.main]
rank = 3
parent = "Get involved"
weight = 30
+++

Advisory groups and working groups help us to stay focused and inclusive. We also have more formal [committees](/committees/) that have a role specified in the [by-laws](/board-and-governance/bylaws) or have been set up by the board with a particular remit. We've also listed a few 'interest groups' and these are the least formal, like community calls, where participants can be involved ad hoc and participate sporadically.

These groups are a good way for people across the community to get involved in Crossref's work to support and improve our scholarly infrastructure. They are slightly different as described below but both are open to non-members and members alike. 

## Advisory groups

We have advisory groups for established services or ongoing themes to get input and advice from our members and other stakeholders. Each advisory group has a statement of purpose and should represent our broad membership. Each group has a chair and staff facilitator who together set agendas, organize calls, and ensure that the group fulfills its purpose. Each group has an email list and meets regularly via conference call, although the frequency varies by group. These groups tend to be permanent and long-term.

Advisory group  | Facilitator | Chair | Status
--- | --- | --- | ---
[Similarity Check](/working-groups/similarity-check) | Lena Stoll, Madhura Amdekar | Lauren Flintoft, IOP Publishing | Active
[Crossmark](/working-groups/crossmark) | Martyn Rittman | TBC | Inactive
[Funder Community](/working-groups/funders) | Kora Korzec | TBC | Active
[Preprints community](/working-groups/preprints) | Martyn Rittman | Oya Rieger, Ithaka | Active | Active
[Event Data](/working-groups/event-data) | Martyn Rittman | John Chodacki, California Digital Library | Inactive

## Working groups

Working groups are more short-term than advisory groups and are set up for a specific task or ad-hoc purpose. They are usually set up to discuss and scope a specific idea, or oversee prototypes and pilots that could develop into new features. Working groups can also be set up jointly with other organizations to enable us to collaborate on projects.

Working groups don't always have a Chair but they bring stakeholders together. A working group either disbands when finished its work or can become an advisory group if and when the board approves the idea or prototype as a production service, feature, or record type.

Working group | Facilitator | Chair | Status
--- | --- | --- | ---
[Conferences and projects](/working-groups/conferences-projects) | Patricia Feeney  | Aliaksandr Birukou, Springer Nature | Inactive
[Distributed usage logging](/working-groups/distributed-usage-logging) |Martyn Rittman| Esther Heuver, Elsevier | Retired
 [Linked clinical trials](/working-groups/linked-clinical-trials) | N/A | Daniel Shanahan, BioMed Central | Inactive
Standards | Patricia Feeney| | Retired
Taxonomies | Rachael Lammey| | Retired

## Interest groups

Interest groups are more like community discussion forums with fairly low commitment needed from participants, where a large group of people meet to discuss a range of issues and can bring any topic under the theme to Crossref. They are the least formal of all our groups and vary in call frequency and scope.

Interest group  | Facilitator | Chair
--- | --- | ---
[Books](/working-groups/books) | Kora Korzec | David Woodworth, OCLC | Active
[Metadata Practitioners](/working-groups/metadata) | Patricia Feeney | n/a | Active
