---
title: PatentCite
author: Crossref
date: 2013-01-22

---

{{% divwrap blue-highlight %}}

**Note:** This experiment has graduated. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

Equivalent functionality can now be found in Crossref Metadata Search.

{{% /divwrap %}}

{{% labsection %}}



Crossref has been working with <a href="http://www.cambia.org/" target="_blank" rel="external">Cambia</a> and the <a href="http://www.lens.org/" target="_blank" rel="external">The Lens</a> to explore how we can better link scholarly literature to and from the patent literature. The first step of our collaboration was to link Lens entries to the relevant Scholarly literature (<a href="https://www.lens.org/lens/patent/115-530-025-142-127/collections?locale=ru" target="_blank" rel="external">For example</a>)

Crossref has taken this matched data and has now released a Crossref Labs **experimental** service, called <a href="" target="_blank" rel="external">PatentCite</a>, that allows you to take any Crossref DOI and see what Patents in the Lens system cite it.

As with all <a href="/labs" rel="self">Crossref Labs</a> services- this one is likely to be:

a) As stable as the global economy

c) As reliable as a UK train

ii) Out-of-date. It is based on a snapshot of Crossref /Lens data.

1) As accurate as my list ordering

Howzat for an SLA?

As we get feedback from Crossref&#8217;s membership and as we gain more experience linking Patents to and from the scholarly literature, we will explore including this functionality in our production CitedBY service. But until then, please send us your feedback on this experimental service to:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />

{{% /labsection %}}
