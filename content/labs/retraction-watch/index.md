title: Retraction Watch data in Labs
author: Crossref
date: 2023-11-22

---

{{% divwrap blue-highlight %}}

**Note:** We're currently supporting open Retraction Watch data via our Labs API and a .csv file. We plan to model and support it via our REST API in future, but it's supported via Labs while we do that work. Thanks, Labs! 

{{% /divwrap %}}

{{% labsection %}}

## How can I find the Retraction Watch data now? 

The full dataset has been released through Crossref’s Labs API, initially as a .csv file to download directly: [https://api.labs.crossref.org/data/retractionwatch?name@email.org](https://api.labs.crossref.org/data/retractionwatch?name@email.org) (add your ‘mailto’).

The Crossref Labs API also displays information about retractions in the /works/ route when metadata is available, such as [https://api.labs.crossref.org/works/10.2147/CMAR.S324920?name@email.org](https://api.labs.crossref.org/works/10.2147/CMAR.S324920?name@email.org) (add your ‘mailto’). 

We welcome [feedback](mailto:support@crossref.org) on how we've rendered the data in the Labs API to help inform how we model it alongside member-provided metadata in future. 

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />

{{% /labsection %}}
