---
title: Crossref Metadata Search
author: Crossref
date: 2013-01-20

---

{{% labsection %}}
[Crossref Metadata Search][1] allows you to search across the almost 50 million Crossref Metadata records* for journal articles and conference proceedings. It supports the following features:

  * ORCID Support
  * A completely new UI
  * [Faceted][2] searches
  * Copying of search results as formatted citations using [CSL][3]
  * [COinS][4], so that you can easily import results into Zotero and other document management tools
  * [An API][5], so that you can integrate Crossref Metadata Search into your own applications, plugins, etc.
  * Basic [OpenSearch][6] support- so that you can integrate Crossref Metadata Search into your browser’s search bar.
  * Searching for a particular Crossref DOI
  * Searching for a particular Crossref [ShortDOI][7]
  * Searching for articles in a particular journal via the journal’s ISSN
  * Links to any patents that cite a particular Crossref DOI

At the moment, Crossref Metadata Search (CRMDS) is a “labs” project and, as such, should be used with some trepidation. Our goal is to release CRMS as a production service ASAP, but we wanted to get public feedback on the service before making the move to a production system.

Specifically, please be aware that  not all record types available at Crossref have been included yet. We will be adding books and components to the index soon.

Please send us your feedback on Crossref Metadata Search to:

[<img class="alignnone size-full wp-image-75" alt="labs_email" src="http://labs.crossref.org/wp-/uploads/2013/01/labs_email.png" width="233" height="42" />][8]

*Please note that Crossref collect abstracts and, as such, the metadata does not include abstracts.

&nbsp;

 [1]: http://search.labs.crossref.org/
 [2]: http://en.wikipedia.org/wiki/Faceted_search
 [3]: http://en.wikipedia.org/wiki/Citation_Style_Language
 [4]: http://en.wikipedia.org/wiki/COinS
 [5]: http://search.labs.crossref.org/help/api
 [6]: http://en.wikipedia.org/wiki/OpenSearch
 [7]: http://shortdoi.org/
 [8]: http://labs.crossref.org/wp-/uploads/2013/01/labs_email.png


{{% /labsection %}}
