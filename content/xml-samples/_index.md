+++
title = "Example XML metadata"
date = "2021-08-27"
draft = false
author = "Patricia Feeney"
rank = 2
parent = "Get help"
weight = 10
+++

Here are some example XML files to help you get started with [Content Registration](/services/content-registration).  

### Books

Type | Version | Input | Output
---|---|--- | ---
Book / monograph|5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/book5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/9mIN5yS) (book) <br> [JSON](https://api.crossref.org/works/10.32013/cQy34FJ) (chapter) |
Book / monograph | 4.8.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/book_4.8.0.xml) |
Book series | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/book_series5.3.0.xml)| [JSON](https://api.crossref.org/works/10.32013/92m.e-7) (series) <br> [JSON](https://api.crossref.org/works/10.32013/pTdhShx) (book) <br> [JSON](https://api.crossref.org/works/10.32013/qe51g17) (chapter)
Book set | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/book_set5.3.0.xml)|[JSON](https://api.crossref.org/works/10.32013/iPdqRAp) (set) <br> [JSON](https://api.crossref.org/works/10.32013/4AvssJK) (book) <br> [JSON](https://api.crossref.org/works/10.32013/GMP2WvT) (chapter)

### Conference Proceedings

Type | Version | Input | Output
---|---|--- | ---
Single proceeding with papers | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/conf_single5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/BwMgAA2) (proceeding) <br> [JSON](https://api.crossref.org/works/10.32013/TM9b6MW) (paper)
Single proceeding with papers | 4.8.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/conf_single_4.8.0.xml) | [JSON](https://api.crossref.org/works/10.32013/BwMg..AA2) (proceeding) <br> [JSON](https://api.crossref.org/works/10.32013/TM9..b6MW) (paper)
Conference proceeding series | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/conf_series5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/XSdXses) (series) <br>  [JSON](https://api.crossref.org/works/10.32013/scicF9L) (proceeding) <br> [JSON](https://api.crossref.org/works/10.32013/auBagie) (paper)
Conference proceeding series | 4.8.0 |[XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/conf_series_4.8.0.xml)| [JSON](https://api.crossref.org/works/10.32013/XSdxxXses) (series) <br>  [JSON](https://api.crossref.org/works/10.32013/scicxxF9L) (proceeding) <br> [JSON](https://api.crossref.org/works/10.32013/auBxxagie) (paper)

### Components
Type | Version | Input | Output
---|---|--- | ---
Component | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/component5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/KKFKsC)
Component | 4.8.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/component_4.8.0.xml) | [JSON](https://api.crossref.org/works/10.32013/KKFKs)

### Datasets
Type | Version | Input | Output
---|---|--- | ---
Dataset | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/dataset.5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/-92iYfI) (database) <br> [JSON](https://api.crossref.org/works/10.32013/NP6QoBh) (dataset)

### Dissertations
Type | Version | Input | Output
---|---|--- | ---
Dissertation | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/dissertation.5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/GO7BE8)
Dissertation | 4.8.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/dissertation_4.8.0.xml) | [JSON](https://api.crossref.org/works/10.32013/GO78988s)

### Grants
Type | Version | Input | Output
---|---|--- | ---
Grant | 0.1.1| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/grants-0.1.1.xml) |<!-- [JSON](https://api.crossref.org/works/10.32013/vGVkUKIx)-->

### Journals

Type | Version | Input | Output
---|---|--- | ---
Journal with articles | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/journal.article5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/ScF8aU8) (journal title) <br> [JSON](https://api.crossref.org/works/10.32013/n0HRokm) (article)
Journal with articles | 4.8.0 |[XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/journal_article_4.8.0.xml) | [JSON](https://api.crossref.org/works/10.32013/8812345678) (journal title) <br> [JSON](https://api.crossref.org/works/10.32013/8812345678) (article)
Article with translation | 4.8.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/journal_article_translation_4.8.0.xml) | [JSON](https://api.crossref.org/works/10.32013/884859104)
Journal title, volume, issue | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/journal.vol.issue5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/ScF8aU8) (journal title) <br> [JSON](https://api.crossref.org/works/10.32013/U.4fAZy4) (volume) <br> [JSON](https://api.crossref.org/works/10.32013/B2Y7qLJ) (issue) <br>
Journal title | 4.8.0|[XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/journal_title_4.8.0.xml) | [JSON](https://api.crossref.org/works/10.32013/88487529)

### Peer reviews
Type | Version | Input | Output
---|---|--- | ---
Peer review | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/peer_review.5.3.0.xml)| [JSON](https://api.crossref.org/works/)

### Posted content (includes preprints)
Type | Version | Input | Output
---|---|--- | ---
Posted content | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/posted_content.5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/gRUKLJA)
Posted content | 4.8.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/posted_content_4.8.0.xml) | [JSON](https://api.crossref.org/works/10.32013/88gRUKLJA)

<!--### Pending Publication
Type | Version | Input | Output
---|---|--- | ---
-->


### Reports and working papers
Type | Version | Input | Output
---|---|--- | ---
Report | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/report.5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/tkQ96E0)

### Standards
Type | Version | Input | Output
---|---|--- | ---
Standard | 5.3.0| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/standard5.3.0.xml) | [JSON](https://api.crossref.org/works/10.32013/uzzXUo4)

### Resource Examples
Some metadata segments can be added to an existing record using [resource XML](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/).

Type | Version | Input
---|---|---
Clinical trial | 4.4.2| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_clinicaltrial_4.8.0.xml)
Crossmark | 4.4.2| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_crossmark_4.8.0.xml)
Funding | 4.4.2| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_funding_4.8.0.xml)
License and full text URL | 4.4.2 | [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_license_fulltext_4.8.0.xml)
Multiple resolution secondary URLs w/unlock flag | 4.4.2| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_mr_secondary-unlock_4.8.0.xml)
Multiple resolution secondary URLs only | 4.4.2| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_mr_secondary_4.8.0.xml)
Multiple resolution unlock only | 4.4.2| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_mr_unlock-only_4.8.0.xml)
References | 4.4.2| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_references_4.8.0.xml)
Relationships | 4.4.2| [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_relation_4.8.0.xml)
Similarity Check URLs | 4.4.2 | [XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_simcheck_4.8.0.xml)


<!--

- [Article with references](/xml-samples/article_with_references.xml)
- [book_series.xml](/xml-samples/book_series.xml)
- [book_set.xml](/xml-samples/book_set.xml)
- [components.xml](/xml-samples/components.xml)
- [conf.xml](/xml-samples/conf.xml)
- [conf_series.xml](/xml-samples/conf_series.xml)
- [Crossmark policy page.xml](/xml-samples/crossmark_policy_page.xml)
- [cr_update_test.xml](/xml-samples/cr_update_test.xml)
- [dataset.xml](/xml-samples/dataset.xml)
- [journal.xml](/xml-samples/journal.xml)
- [journaltitle.xml](/xml-samples/journaltitle.xml)
- [monograph.xml](/xml-samples/monograph.xml)
- [mr_full.xml](/xml-samples/mr_full.xml)
- [mr_secondary.xml](/xml-samples/mr_secondary.xml)
- [mr_secondaryunlock.xml](/xml-samples/mr_secondaryunlock.xml)
- [references.xml](/xml-samples/references.xml)
- [report.xml](/xml-samples/report.xml)
- [report.chapters.xml](/xml-samples/report.chapters.xml)
- [Report series](/xml-samples/report_series.xml)
- [sa_crossmark.xml](/xml-samples/sa_crossmark.xml)
- [sa_crossmark2.xml](/xml-samples/sa_crossmark2.xml)
- [sa_lic_ref.xml](/xml-samples/sa_lic_ref.xml)
- [similaritycheck.xml](/xml-samples/similaritycheck.xml)
- [similaritycheck-resource.xml](/xml-samples/similaritycheck-resource.xml)
- [stand-alone.xml](/xml-samples/stand-alone.xml)
- [standard.xml](/xml-samples/standard.xml)
-->
---

Please consult other users on our forum [community.crossref.org](https://community.crossref.org) or open a ticket with our [technical support specialists](mailto:support@crossref.org) if you have any questions.
