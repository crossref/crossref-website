+++
title = "Crossref annual meeting and board election 2023"
date = "2023-07-27"
draft = false
author = "Rosa Morais Clark"
image = "/images/banner-images/crossref-annual-meeting-2023.png"
rank = 6
parent = "Crossref LIVE Annual"
weight = 9
+++

## #Crossref2023 online, October 31, 2023

Our annual meeting, #Crossref2023, was held online on October 31 from 9:30 AM UTC to 4:30 PM UTC (universal coordinated time). We invited all our members from 170+ countries, and everyone in our community, to hear the results of our board election and team updates.

Please see information from #Crossref2023 below, and cite the outputs as `#Crossref2023 Annual Meeting and Board Election, October 31, 2023 retrieved [date], https://doi.org/10.13003/h3yygefpyf`:

#### Update on the Research Nexus   
- [The latest on the Research Nexus](https://youtu.be/NDAKMSd8tLw) by Ginny Hendricks, Crossref   

#### Updates from the community 
- [Making data citations available at scale: The Global Open Data Citation Corpus](https://youtu.be/LquGHwVJfrg?si=DMQXhSi7IosBHyrj&t=209) by Iratxe Puebla, DataCite    
- [Who cares? Defining Citation Style in Scholarly Journals](https://youtu.be/LquGHwVJfrg?si=UktFvN2aSmB1NM0x&t=883) by Vincas Grigas, Vilnius University  
- [DOI registration for scholarly blogs](https://youtu.be/LquGHwVJfrg?si=rnNasmWFrFZZEoCE&t=1492) by Martin Fenner, Front Matter  
- [Grant DOIs at the AU Publications Office](https://youtu.be/LquGHwVJfrg?si=Vf7oV_qPygA4-S9_&t=2110) by Izabela Szprowska, OP and European Commission; Nikolaos Mitrakis, European Commission; and Paola Mazzucchi, mEDRA


#### Demos, Experiments and Q&A
- [Registration form for journal content](https://youtu.be/4uN2vhHVZFE?si=X0uHjb0h9Lz2Ii3w&t=154)  by Lena Stoll, Crossref
- [PKP to demo the latest plugin](https://youtu.be/4uN2vhHVZFE?si=hGN_uOcJ_JyBvTW3&t=2505)  by Erik Hanson, PKP
- [Data citations matching](https://youtu.be/4uN2vhHVZFE?si=1eTZnvn7dg-uEbkv&t=1639) by Martyn Rittman, Crossref
- [Using Crossref API](https://youtu.be/4uN2vhHVZFE?si=49XbzINu4HGkYh-H&t=3561)by Luis Montilla, Crossref
- [RW data in Labs API](https://youtu.be/4uN2vhHVZFE?si=UyF4WjlGexzY6sTB&t=4350) by Rachael Lammey & Martin Eve, Crossref
- [Digital preservation reports](https://youtu.be/4uN2vhHVZFE?si=0PGr3nzsoHglQhUy&t=4732) by Martin Eve, Crossref
- [DOIs for static site generators](https://youtu.be/4uN2vhHVZFE?si=BZld-JLJSmyI91Uz&t=5491) by Esha Datta, Crossref  

#### Panel Discussion
What do we still need to build the Research Nexus? Panel discussion with Ginny Hendricks, Crossref; Patricia Feeney, Crossref; Matt Buys, DataCite; Kevin Stranack, PKP; Ludo Waltman, CWTS Leiden University; Mercury Shitindo, St. Paul's University, Kenya; Ran Dang, Atlantis Press. [Recording](https://youtu.be/d_u-Ad9-H64?si=7Nzk9wUQMJMdxffY)  

#### Annual meeting and board election and Crossref strategy
- [Crossref Strategy](https://youtu.be/nKdVo6vMvdA?si=RgVyjVcKt2cDdcYM&t=48) by Ed Pentz, Crossref    
- [Thanks to partners and advocates](https://youtu.be/nKdVo6vMvdA?si=LTbgU5_cPbca9Tmw&t=1132) by Johanssen Obanda   
- [Member governance and board election](https://youtu.be/nKdVo6vMvdA?si=yGHgcQ3idp7fi32H&t=1601) lead by Lucy Ofiesh, Crossref  

#### Spotlight on community initiatives 
- [Enhancing Research Connections through Metadata: A Case Study with AGU and CHORUS](https://youtu.be/7jfwJifyeHk?si=ixjhNYq0w-4dnQbZ&t=183) by Tara Packer (CHORUS), Sara Girard (CHORUS), Shelley Stall (AGU), Kristina Vrouwenvelder (AGU)
- [Index Crossref Integrity, Professional, and Institutional Development](https://youtu.be/7jfwJifyeHk?si=ZvCeVg_FnPF6WR3I&t=727) by Engjellushe Zenelaj, Reald University College  
- [Brazilian retractions in the Retraction Watch Database](https://youtu.be/7jfwJifyeHk?si=MAHOoWSH0AS_gs3x&t=1116) by Edilson Damasio, Maringá State University / Crossref Ambassador  
- [Now that you've published, what do you do with metadata?](https://youtu.be/7jfwJifyeHk?si=26CMPVHj1YaIORk5&t=2088) by Joann Fogleson, American Society of Civil Engineers
- [ROR / Open Funder Registry Overlap](https://youtu.be/7jfwJifyeHk?si=Fm92y0DrJ1PBS5WQ&t=2763) by Amanda French, Crossref

#### Other Outputs 
- Presentation: [Google slides](https://docs.google.com/presentation/d/1bL_-WjoA1FBTcFmTP9p-bUn81bcYVemDSWm0xWt-M4o/preview?slide=id.g29763a2c4ce_0_0) or [pdf slides](/pdfs/crossref2023-annual-meeting-shared.pdf)
- [#Crossref2023 Mastodon stream](https://mastodon.social/tags/crossref2023)
- [#Crossref2023 Twitter stream](https://twitter.com/hashtag/Crossref2023?src=hashtag_click)   
- [Posters from community guest speakers](https://community.crossref.org/c/crossref-events/crossref2023-presentations/44)    
- [Board election results](/board-and-governance/elections)  

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQe2LvfOOw4zEoZdruUlY43UNIgkdqRRuFqV2YGDBmXlMUokfhgYwCO5s0qsCPQIVfrwYTzl0dCoiXb/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

---

## LIVE22 online, October 26, 2022

Our annual meeting, LIVE22, was held online on October 26 at 4:00 PM UTC (universal coordinated time). We invited all our members from 140+ countries, and everyone in our community, to hear the results of our board election and team updates.

Here are some of the outputs from the full session:  

- Ed Pentz spoke about our vision, mission, strategic goals, update on our efforts toward POSI, and our role in ISR (inclusive scholarly record).  
- Vanessa Fairhurst and Isaac Farley highlighted contributors from those in our community who help make our work possible.  
- Kora Korzec led our community speaker session, "Building the Research Nexus together: flash talks", with [presentations by](https://community.crossref.org/c/live22-presentations/36) Hans de Jonge, Bianca Kramer, Javier Arias, Julie Lambert, Lettie Y. Conrad, and Edilson Damasio.   
- Amanda Bartell and Patricia Feeney talked about the state of membership and metadata members register with us.
- Dominika Tkaczyk talked about our work around linking grants to research outputs.
- Lucy Ofiesh led our annual meeting, and board election results looked at our financial performance and the 2023 draft budget.
- [Led by our ambassadors, four in-person 'LIVE' satellite events took place in Lithuania, Brazil, Turkey, and Kenya](https://twitter.com/CrossrefOrg/status/1585305657049509895). Some included a watch party of the meeting, and all included good talks and discussions about metadata and the scholarly record.  

Please check out the materials from LIVE22 below, and cite the outputs as `Crossref Annual Meeting LIVE22, Ocrober 26, 2022 retrieved [date], [https://doi.org/10.13003/i3t7l9ub7t]`:

- [YouTube recording](https://www.youtube.com/watch_popup?v=csy4mf8QNtY) 
- [Recording transcript](https://docs.google.com/document/d/1h2Pc0orQqnMJoNGx4E6W_Yhr1fiZaeZRqF5ItcjEjkc/preview)    
- [Zoom Q&A transcript](https://docs.google.com/spreadsheets/d/1sPlXW8rg3jMN2IiBbihdAuDc_zacUvIR8vXSiHEbNN0/preview#gid=0)    
- [Google slides](https://docs.google.com/presentation/d/1CWoueRlXDBr764zDxw3docHDWosD8UCqlXzE9zZxyRI/preview?slide=id.gf67b34ab3e_0_8) or [pdf slides](/pdfs/crossref-annual-meeting-live22-presentation-shared.pdf)
- [#CRLIVE22 Twitter stream](https://twitter.com/CrossrefOrg/status/1585301047748222979)   
- [Posters from community guest speakers](https://community.crossref.org/c/live22-presentations/36)    

[Board election results](/board-and-governance/elections)  


---


## The annual meeting archive

Browse our [archive of annual meetings](/crossref-live-annual/archive) with agendas and links to previous presentations from 2001 through 2015. Its a real trip down memory lane!

---

Please [contact us](mailto:feedback@crossref.org) with any questions.
