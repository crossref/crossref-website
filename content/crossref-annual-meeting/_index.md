+++
title = "Crossref Annual Meeting"
date = "2024-09-08"
draft = false
author = "Kornelia Korzec"
image = "/images/banner-images/crossref-annual-meeting-2024.png"
[menu.main]
rank = 3
weight = 50
aliases = [
    "/crossref-live-annual",
    "/crossref-live-annual/",
    "/crossref-live-annual/2024-annual-meeting",
    "/crossref-live-annual/2024-annual-meeting/"
]
+++


## #Crossref2024 online, 29 October 2024

Our annual meeting, #Crossref2024, was held online on 29 October 2024 starting at 8:00 AM UTC to 18:30 PM UTC (universal coordinated time). We invited all our members from 170+ countries, and everyone in our community, to hear the results of our board election and team updates.

Please see information from #Crossref2024 below, and cite the outputs as `#Crossref2024 Annual Meeting and Board Election, 29 October 2024 retrieved [date], [https://doi.org/10.13003/1KJ1GBDA9B](https://doi.org/10.13003/1KJ1GBDA9B)`:


**Session I**  

| Time     | Topic |
|----------|-------|
| 0:00     | [Welcome & Crossref updates](https://youtu.be/VBnfkOxVr6s?si=AWuKR2JFMGzsXtnp) |
| 1:20     | [Strategic programs & annual meeting](https://youtu.be/VBnfkOxVr6s?si=ebg6NvNDb7hiGdPe&t=80) |
| 31:49    | [Demos](https://youtu.be/VBnfkOxVr6s?si=yVVxcwPCRYJL5JWd&t=1916) |
| 59:08    | [Updates from the Community I](https://youtu.be/VBnfkOxVr6s?si=rxRVo6whPsYRQjqe&t=3548) |
| 1:01:12  | - [Michael Parkin, EMBL-EBI](https://youtu.be/VBnfkOxVr6s?si=1VV79KhplCHsWXNt&t=3701)- [[Slides]](/pdf/crossref2024-michaelparkin-EuropePMC.pdf) |
| 1:09:14  | - [Hans de Jonge, Dutch Research Council NWO](https://youtu.be/VBnfkOxVr6s?si=euFBcIGYp1UEDrHz&t=4169) - [[Slides]](pdfs/crossref2024-hansdejonge-NWO.pdf) |
| 1:23:22  | - [Fred Atherden, eLife](https://youtu.be/VBnfkOxVr6s?si=eNYAyjvIlX0OkCBJ&t=5002) - [[Slides]](pdfs/crossref2024-fredatherden-eLife.pdf) |
| 1:32:02  | - [Brietta Pike, CSIRO](https://youtu.be/VBnfkOxVr6s?si=1haopH2ahnb-xllw&t=5522) - [[Slides]](pdfs/crossref2024-briettapike-CSIRO.pdf) |
| 1:54:12  | [Panel discussion - Opportunities and challenges of the open scholarly infrastructure](https://youtu.be/VBnfkOxVr6s?si=tFTZ7aOjYH9wORAh&t=6854) |
| 3:10:37  | [Reflections break-outs (ISR, RCFS, Research Nexus, Reflections)](https://youtu.be/VBnfkOxVr6s?si=UOsLrZwuN-jz1kfY&t=11437) [Slides](pdfs/) |


**Session II**  

| Time     | Topic |
|----------|-------|
| 0:00     | [Welcome and introduction](https://www.youtube.com/watch?v=5ZI8idIDL_A&t=0s) |
| 1:38     | [Beyond the basics: Crossref API Workshop](https://www.youtube.com/watch?v=5ZI8idIDL_A&t=98s) |
| 25:08    | [Metadata Schema](https://www.youtube.com/watch?v=5ZI8idIDL_A&t=1508s) |
| 56:10    | [Resourcing Crossref for Future Sustainability (RCFS)](https://www.youtube.com/watch?v=5ZI8idIDL_A&t=3370s) |
| 1:43:30  | [The state of Crossref](https://youtu.be/5ZI8idIDL_A?si=UHj-O3PGG58AyQxF&t=6396) |
| 2:13     | [Board Election](https://youtu.be/5ZI8idIDL_A?si=9-k3Z-rJ6IzvFVUs&t=7931) |
| 2:32     | [Updates from the Community II](https://youtu.be/5ZI8idIDL_A?si=QdJsTVV_8to12QXp&t=9147) |
| 2:35:16  | - [Alice Wise, CLOCKSS](https://youtu.be/5ZI8idIDL_A?si=5FaVcSbwCfyo_OOX&t=9150)- [[Slides]](pdfs/crossref2024-aliciawise-CLOCKSS.pdf) |
| 2:48:03  | - [Mark Williams, Sciety](https://youtu.be/5ZI8idIDL_A?si=0fLneFHGEaSsnSzC&t=10082) - [[Slides]](pdfs/crossref2024-markwilliams-Sciety.pdf) |
| 2:58:34  | - [Arianna Garcia, AmeliCA/Redalyc](https://youtu.be/5ZI8idIDL_A?si=93KJA-36wgJ3Apg2&t=10708) - [[Slides]](pdfs/crossref2024-ariannabecerrilgarcia-Redalyc.pdf)|
| 3:27:00  | [Reflections break-outs (ISR, RCFS, Research Nexus, Reflections)](https://youtu.be/5ZI8idIDL_A?si=PmHZiR05xYlyBpAZ&t=12390)  |
| 3:32:21  | [Closing Remarks](https://youtu.be/5ZI8idIDL_A?si=tVRCHMv8S15csnVH&t=12055) |


[Posters](https://community.crossref.org/c/crossref-events/crossref-2024-presentations/49)  
[Slide deck](/pdfs/crossref2024-annual-meeting.pdf)

---
## The annual meeting archive

Browse our [archive of annual meetings](/crossref-annual-meeting/archive) with agendas and links to previous presentations from 2001 through 2015. Its a real trip down memory lane!

---

Please [contact us](mailto:feedback@crossref.org) with any questions.
