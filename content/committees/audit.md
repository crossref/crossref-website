+++
title = "Audit committee"
date = "2023-06-01"
draft = false
rank = 2
[menu.main]
parent = "Committees"
author = "Lucy Ofiesh"
weight = "12"
+++

The Audit Committee is made up of three board members who aren't officers. They oversee our accounting and financial reporting processes and the audit of our financial statements. The committee also appoints an independent auditor, reviews the results of the audit and oversees the compliance with any conflict of interest or whistleblower policies. You can see our financial statements in our [annual report](/operations-and-sustainability/annual-report) that we produce in the November of each year.

## 2025 Audit Committee members

Staff facilitator: Lucy Ofiesh  

* * Ashley Towne, University of Chicago Press (Chair)
committee in formation
