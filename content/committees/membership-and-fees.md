+++
title = "Membership & Fees committee"
date = "2021-04-09"
author = "Amanda Bartell"
draft = false
rank = 3
[menu.main]
parent = "Committees"
weight = 4
+++

The Membership & Fees Committee (M&F committee) was established in 2001 and plays an important role in our governance. Made up of organizations that include members, sponsored members, sponsor, and metadata user - some of whom are also board members.

The group makes recommendations to the board about fees and policies for all of our services and procedures in relation to fees and community work.

They review existing fees to discuss if any changes are needed. They also review new services while they are being developed, to assess if fees should be charged and if so, what those fees should be, in line with [our fee principles](/operations-and-sustainability/#fee-principles), and the Principles of Open Scholarly Infrastructure (POSI). In addition, the board can also delegate specific issues about policies and services to the M&F committee.

Most recently, in 2021, the committee reviewed a proposal to evolve the "fee assistance" program into a mroe expansive [Global Equitable Membership (GEM) program](/gem). In 2019 the committee undertook a regular fee review which led to three specific cases being recommended to the board, all of which passed:

1. Removing the Crossmark fee
2. Removing the fees for versions and translations (registered by the same member)
3. Updating a set of principles for guiding fee-setting

### 2024 Remit

At the 2023 November Board meeting, the following scope of work was agreed to support the "Resourcing Crossref Long-term" project":

* Recruit committee members as needed to represent Crossref stakeholders
* Review and provide feedback on project outputs, including SWOT analysis, modelling of new fees, and impact/effort assessments of fee changes
* Support staff in getting feedback from the community on fee models and possible changes to current fees. Committee work might also include advising on how we engage the community in the process, such as reviewing RFPs for a community engagement consultant, refining the questions we ask, and reviewing the input
* Make recommendations to the board for any proposed fee changes
* Share findings publicly with the community

## M&F committee members

**Chair:** Vincas Grigas, Vilnius University  
**Crossref facilitator:** Amanda Bartell

| Committee member                        | Representative                      | Country      | Size (records) | Org/Member type                         |
| :-------------------------------------- | :---------------------------------- | :----------- | -------------: | :-------------------------------------- |
| Academicus Journal                      | Arta Musaraj                        | Albania      |            300 | Publisher                               |
| ACM                                     | Scott Delman                        | USA          |        600,000 | Society, Publisher                      |
| Beilstein Institut*                     | Wendy Patterson                     | Germany      |         11,000 | Publisher, Research Institute           |
| Center for Open Science (COS)*          | Nici Pfeiffer                       | USA          |        150,000 | Researcher Service/Tool                 |
| Clarivate Analytics*                    | Francesca Buckland                  | USA          |         11,000 | Publisher, Metadata User                |
| DOAJ                                    | Joanna Ball                         | USA          |            N/A | Metadata User                           |
| eLife                                   | Damian Pattinson                    | UK           |         40,000 | Publisher                               |
| Elsevier*                               | Alok Pendse                         | Netherlands  |     20,000,000 | Publisher                               |
| Frontiers                               | Marie Souliere                      | Switzerland  |        400,000 | Publisher                               |
| Institute of Research and Community Services, Diponegoro University| Eko Didik Widianto | Indonesia          |            20,000 | University, Publisher                                |
| Japan Association for Language Teaching (JALT) | Theron Muller                | Japan        |           1000 | Society                                 |
| Kampala International University        | Ademola Olaniyan                    | Uganda       |            380 | University, Publisher                   |
| Liverpool University Press              | Clare Hooper                        | UK           |         46,000 | University, Publisher                   |
| l'Université de Parakou                 | Honoré Biaou                        | Benin        |             60 | University, Publisher                   |
| Noyam Publisher                         | Naa Kai Amanor-Mfoafo               | Ghana        |            500 | Publisher                               |
| Open Library of Humanities              | Rose Harris-Birtill                 | UK           |         11,000 | Scholar-led                             |  Publisher                               |
| Pakistan Journal of Botany              | Rehan Saleem                        | Pakistan     |          1,500 | Publisher
| TU Delft Open Publishing                | Frédérique Belliard                 | Netherlands  |          2,400 | University, Publisher                   |
| Universidad de Guadalajara              | Ramón Willman                       | Mexico       |          6,400 | University, Publisher                   |
| University of Lagos                     | Yetunde Zaid                        | Nigeria      |            130 | University, Publisher                   |
| University of Namibia                   | Anna Leonard                        | Namibia      |            170 | University, Publisher                   |
| Vilnius University*                      | Vincas Grigas                       | Lithuania    |         22,587 | Society, University, Publisher, Sponsor|
| Wits University Press                   | Andrew Joseph                       | South Africa |          1,800 | University, Publisher                   |

_(*) indicates current Crossref board member_

## About committee participation

The M&F Committee meets via one-hour conference calls about four times a year, although this can vary depending on what issues the committee is considering and what topics the board has delegated to them. Often proposals are developed by staff and then reviewed and discussed by the committee – so there is reading to do in preparation for the calls. The committee Chair is a board member and acts as a link between the two groups, presenting M&F recommendations to the board for them to vote.

This is very important work and it's essential the committee is broadly representative of Crossref’s diverse membership of over 19,000 organizations in 152 countries.

All members agree to abide by Crossref's [code of conduct](/code-of-conduct/).

---

Please contact the [community team](mailto:feedback@crossref.org) with any questions, or Amanda Bartell directly.
