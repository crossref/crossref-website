+++
title = "Display guidelines"
date = "2021-04-21"
draft = false
author = "Ginny Hendricks"
doi = "https://doi.org/10.13003/5jchdy"
aliases =[
    "/01company/15doi_info.html",
    "/education/metadata/persistent-identifiers/doi-display-guidelines/",
    "/education/metadata/persistent-identifiers/doi-display-guidelines",
    "/02publishers/doi_display_guidelines.html",
    "/02publishers/doi_display_guidelines"
]
parent = "Policies"
rank = 4
weight = 1
+++

**1 September 2022**
These 2017 guidelines are not changing but we’ve added a recommendation to improve accessibility for Crossref links on landing pages. Please see our recent [call for comments](https://www.crossref.org/blog/accessibility-for-crossref-doi-links-call-for-comments-on-proposed-new-guidelines/) for more information. This page will be updated when the recommendation has been finalized.

## Display guidelines for Crossref DOIs - effective from March 2017 <a id='00546' href='#00546'></a>

#### Cite as

> “Crossref Display Guidelines (March 2017)", retrieved [date], [https://doi.org/10.13003/5jchdy](https://doi.org/10.13003/5jchdy)

{{% imagewrap right %}} <img src="/images/blog/crossref-doi-display-march-2017.jpg
" alt="Crossref Link Display" width="300px" /> {{% /imagewrap %}}

It's really important for consistency and usability that all members follow these guidelines. We rarely have to change them and usually only do so for very good reasons. Please note that this is for display of Crossref DOIs, not anyone else's persistent links, as, for example, [not all DOIs are made equal](/membership/#member-obligations-and-benefits/).

The goals of the guidelines are to:

1. Make it as easy as possible for users without technical knowledge to cut and paste or click to share Crossref DOIs (for example, using right-click to copy a URL).
2. Get users to recognize a Crossref links as both a persistent link as well as a persistent identifier, even if they don't know what a Crossref DOI is.
3. Enable points 1 and 2 above by having all Crossref members display DOIs in a consistent way.
4. Enable robots and crawlers to recognize Crossref DOIs as URLs.

_When linking to a research work, use its Crossref DOI link rather than its URL. If the URL changes, the publisher will update the metadata in Crossref with the new URL, so that the link will always take you to the correct location of the work._

## How to display a Crossref link <a id='00472' href='#00472'></a>

When displaying DOIs, it’s important to follow these display guidelines. Crossref DOIs should:

* always be displayed as a full URL link in the form [https://doi.org/10.xxxx/xxxxx](https://doi.org/10.xxxx/xxxxx)
* not be preceded by doi: or DOI:
* not use *dx* in the domain name part of DOI links
* and we recommend HTTPS (rather than HTTP).

Here is an example of canonical DOI display:

<figure><a href='https://doi.org/10.3390/rel11010015'><img src='/images/documentation/DOI-display.png' alt='Canonical DOI display' title='' width='75%'></a></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image40">Show image</button>
<div id="image40" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/DOI-display.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## Changes to guidelines in March 2017 <a id='00555' href='#00555'></a>

These guidelines introduce two important changes that differ from the previous guidelines:

1. we have dropped the `dx` from the domain name portion of Crossref links
2. we recommend you use the secure HTTPS rather than HTTP

> Note this change is backwards compatible, so DOIs such as [http://dx.doi.org/](http://dx.doi.org/) and [http://doi.org/](http://doi.org/) which conform to older guidelines will continue to work indefinitely.

## Where to apply the display guidelines <a id='00473' href='#00473'></a>

Crossref links should be displayed as the full URL link wherever the bibliographic information about the content is displayed.

An [obligation of membership](/membership) is that Crossref persistent links must be displayed on members’ [landing pages](/documentation/member-setup/creating-a-landing-page/). We recommend that Crossref links also be displayed or distributed in the following contexts:

* Tables of contents
* Abstracts
* Full-text HTML and PDF articles, and other scholarly documents
* Citation downloads to reference management systems
* Metadata feeds to third parties
* "How to Cite This" instructions on content pages
* Social network links
* Anywhere users are directed to a permanent, stable, or persistent link to the content.

Crossref members should not use proprietary, internal, or other non-Crossref links in citation downloads, metadata feeds to third parties, nor in instructions to researchers on how to cite a document. The membership terms stipulate that Crossref persistent identifier links must be the default.

## Crossref links in reference lists and bibliographies <a id='00474' href='#00474'></a>

Linking references in *journal articles* using Crossref DOIs is a [condition of membership](/membership). This means including the link for each item in your reference list. We strongly encourage members to link references for other record types too. Because there are space constraints even in online references lists, Crossref DOIs can be displayed in several ways, depending on the publisher’s preference and publication style. We recommend the following options:

1. use the Crossref DOI URL as the permanent link. **Example**: Soleimani N, Mohabati Mobarez A, Farhangi B. Cloning, expression and purification flagellar sheath adhesion of Helicobacter pylori in Escherichia coli host as a vaccination target. Clin Exp Vaccine Res. 2016 Jan;5(1):19-25. [https://doi.org/10.7774/cevr.2016.5.1.19](https://doi.org/10.7774/cevr.2016.5.1.1)
2. display the text *Crossref* with a permanent DOI link behind the text. **Example**: Galli, S.J., and M. Tsai. 2010. Mast cells in allergy and infection: versatile effector and regulatory cells in innate and adaptive immunity. Eur. J. Immunol. 40:1843–1851. [Crossref](https://doi.org/10.1002/eji.201040559).

Learn more about [how to link your references](/documentation/reference-linking/).

## ShortDOI <a id='00547' href='#00547'></a>

The DOI Foundation created the [ShortDOI](http://shortdoi.org/) service as an open system that creates shortcuts to DOIs. DOIs can be long, so this service aimed to to the same thing as [URL shortening services](https://en.wikipedia.org/wiki/URL_shortening). ShortDOIs are not widely used and are not really actual DOIs themselves, which is confusing. We recommend simply creating shorter DOIs in the first place. Learn more about [constructing your DOIs](/documentation/member-setup/constructing-your-dois/).

## Frequently Asked Questions (FAQs) about the March 2017 changes <a id='00548' href='#00548'></a>

### Can we make the display changes now or do we need to wait? <a id='00550' href='#00550'></a>

These guidelines are now in effect. We set the date as March 2017, after giving our members six months' notice to make the changes.

### Why does a Crossref DOI have to be displayed as a link on the page that it links to? <a id='00475' href='#00475'></a>

Some members have reported resistance from colleagues to displaying the Crossref DOI on the landing page *as a link* (they say the link in that location appears superfluous as it appears to link to itself). However, the Crossref DOI must be displayed as a link, because it is both *an identifier* and *a persistent link*. It is also part of the membership terms agreed to when members join Crossref. It is easier for users when members display the DOI as a full link as they can copy it easily. Also, many users don’t know what a DOI is, but they know what a link is. We want to encourage the DOI to be used as a persistent link, and to be shared and used in other applications (such as reference management tools). A fully linked DOI enables this, wherever it appears.

### Do we need to redeposit our metadata to update the DOI display? <a id='00551' href='#00551'></a>

No - there is no need to redeposit metadata. These guidelines cover how you display DOIs on your website, not how to register them with us.

### Why not use doi: or DOI:? <a id='00476' href='#00476'></a>

When Crossref was founded in 2000, we recommended that DOIs be displayed in the format `doi:10.NNNN/doisuffix` and many members still use `doi:[space][doinumber]`, `DOI: [space][doinumber]`, or `DOI[space][doinumber]`. At the time that the DOI system was launched in the late 1990s it was thought that `doi:` would become native to browsers and automatically resolve DOIs, like `http:`. This did not happen, and so `doc:`/`DOI:` is not a valid way of displaying or linking Crossref DOIs.

Advantages to changing the display to a resolvable URL (even on the page the DOI itself resolves to) include:

* A Crossref DOI is both a link and an identifier. Users will more easily recognize them as an actionable link, regardless of whether they know about the infrastructure behind it.
* Users who do not know how to right-click on the link and choose *Copy link* will still be able to easily copy the DOI URL
* Machines and programs (such as bots) will recognize the Crossref DOI as a link, thereby increasing discoverability and usage.

### Why not use `dx` as in `http://dx.doi.org/`? <a id='00553' href='#00553'></a>

Originally the `dx` separated the DOI resolver from the International DOI Foundation (IDF) website but this changed a few years ago and the IDF recommends `http://doi.org` as the preferred form for the domain name in DOI URLs.

### Why should we use HTTPS? <a id='00554' href='#00554'></a>

Providing the central linking infrastructure for scholarly publishing is something we take seriously. Because we form the connections between publisher content all over the web, it’s important that we do our bit to enable secure browsing from start to finish. In addition, HTTPS is now a ranking signal for Google, which gives sites using HTTPS a small ranking boost.

The process of enabling HTTPS on publisher sites will be a long one, and given the number of members we have, it may take a while before everyone’s made the transition. But by using HTTPS we are future-proofing scholarly linking on the web.

Some years ago we started the process of making our new services available exclusively over HTTPS. The Crossref API is HTTPS enabled, and Crossmark and our Assets CDN use HTTPS exclusively. In 2015 we collaborated with Wikipedia to make all of their DOI links HTTPS. We hope that we’ll start to see more of the scholarly publishing industry doing the same.
