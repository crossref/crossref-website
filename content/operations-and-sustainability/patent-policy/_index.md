+++
title = "Patent non-assertion policy"
date = "2024-12-04"
draft = false
author = "Lucy Ofiesh"
rank = 4
[menu.main]
parent = "Operations & sustainability"
weight = 5
+++

As part of our committment to the [Principles of Open Scholarly Infrastructure](https://openscholarlyinfrastructure.org/) we have adopted a patent non-assertion policy. The patent non-assertion policy is intended to prevent open infrastructure organizations from inhibiting the community's ability to replicate the infrastructure. 

Crossref does not currently hold any patents. 

To the extent possible, we make our policies publicly available for inspection or reuse by others.

<iframe src="https://docs.google.com/document/d/e/2PACX-1vRA9OFH5UbuhjiMSLXnxsZQeT4y-fuLFAQmdCFUSBfUD6-wUn6U_gazrwCfRiZn06LQDgSRMS2VOPb_/pub?embedded=true"style="width:850px; height:700px;" frameborder="1"></iframe>


