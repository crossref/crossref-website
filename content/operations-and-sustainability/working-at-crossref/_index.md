+++
title = "Working at Crossref"
date = "2024-01-04"
draft = false
author = "Lucy Ofiesh"
rank = 4
[menu.main]
parent = "Operations & sustainability"
weight = 5
+++

Crossref is powered by a team of about 50 people who are based all over the world. We aim to provide a consistent employment experience, while also complying with the labour practices of the countries where our staff live. 

Our employee handbook details the benefits we offer the team. This document, along with our organizational [policies & procedures](/operations-and-sustainability/working-at-crossref/policies/), [Code of conduct](/code-of-conduct/), and respective employment contracts govern how we work together. 

To the extent possible, we make these policies publicly available for inspection or reuse by others. 

<iframe src="https://docs.google.com/document/d/e/2PACX-1vRra-QwdUb0v5lDp1xe_69BqCk6yRy1PTKiVbN1BM1OVaNzQynIvsnhssJFHuamHyCK-B-XHmHGtZhA/pub?embedded=true" style="width:850px; height:700px;" frameborder="1"></iframe>  

