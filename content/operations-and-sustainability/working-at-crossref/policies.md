+++
title = "Organizational policies & procedures"
date = "2024-01-04"
draft = false
author = "Lucy Ofiesh"
rank = 4
[menu.main]
parent = "Operations & sustainability"
weight = 5
+++

Our policies and procedures handbook details organizational policies related to how we work. Along with our [employee manual](/operations-and-sustainability/working-at-crossref), we share these policies for inspection or reuse by others. 

<iframe src="https://docs.google.com/document/d/e/2PACX-1vR8d638EJQRLYQA91Nsb1HPyyZhJN8M0-W2aC9h79prnXI9y2_7lMALJZo3I5bikLHV946xg5EcYYx4/pub?embedded=true" style="width:850px; height:700px;" frameborder="1"></iframe> 