> #### Cited-by supports members who display citations, enabling the community to discover connections between research outputs.

Scholars use citations to critique and build on existing research, acknowledging the contributions of others. Members can include references in their metadata deposits which Crossref uses to create links between works that cite each other. The number of citations each work receives is visible to anyone through our public APIs. Through our Cited-by service, members who deposit reference metadata can retrieve everything they need to display citations on their website.

Members who use this service are helping readers to:

* easily navigate to related research,
* see how the work has been received by the wider community,
* explore how ideas evolve over time by highlighting connections between works.

Watch the introductory Cited-by animation in your language:  

{{< vimeoPlayer id="services-citedby" >}}

## How Cited-by works <a id='00265' href='#00265'><i class='fas fa-link'></i></a>

Cited-by begins with references deposited as part of the metadata records for your content. Learn more about [depositing references](/documentation/content-registration/descriptive-metadata/references/).

{{< figure-linked
	src="/images/documentation/Cited-by-how-it-works.png"
	large="/images/documentation/Cited-by-how-it-works.png"
	alt="How Cited-by works - text description below diagram"
	title="How Cited-by works"
	id="image127"
>}}

A member registers content for a work, the *citing paper*. This metadata deposit includes the reference list. Crossref automatically checks these references for matches to other registered content. If this is successful, a relationship between the two works is created. Crossref logs these relationships and updates the citation counts for each work. You can [retrieve citation counts through our public APIs](#00266). 

Members can use the Cited-by service to retrieve the full list of *citing works*, along with all the bibliographic details needed to display them on their website.

Note that citations from Crossref may differ from those provided by other services because we only look for links between Crossref-registered works and don't share the same method to find matches.

## Obligations and fees for Cited-by <a id='00268' href='#00268'><i class='fas fa-link'></i></a>

* Participation in Cited-by is optional, but encouraged.
* There is no charge for Cited-by.
* Depositing references is not a requirement, but strongly encouraged if you are using Cited-by.

## Best practice for Cited-by <a id='00267' href='#00267'><i class='fas fa-link'></i></a>

Because citations can happen at any time, Cited-by links must be kept up-to-date. Members should either check regularly for new citations or (if [performing XML queries](/documentation/cited-by/retrieve-citations#00272)) set the alert attribute to *true*. This means the search will be saved in the system and you’ll get an alert when there is a new match.

Depositing your own references is strongly encouraged if you use Cited-by. If you don't, the citations you retrieve will not include those from your own works. This is likely to lead to under-reporting of citations counts by at least 20% and you are missing the opportunity to point readers to other similar works on your platform.

<figure><img src='/images/documentation/Infographic-Cited-by.png' alt='Cited-by infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image21">Show image</button>
<div id="image21" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Cited-by.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

[Download the Cited-by factsheet](/pdfs/about-cited-by.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).

## How to access citation matches <a id='00266' href='#00266'><i class='fas fa-link'></i></a>

Members who deposit references allow them to be retrieved by anyone using our public APIs. For example:

```
http://api.crossref.org/works?filter=has-references:true
```

Also in the metadata is the number of citations a work has received, under the tag `"is-referenced-by-count"`.

To retrieve the full list of citations you need be a member using Cited-by. While anyone can use an API query to see the number of citations a work has received, members can retrieve a full list of citing DOIs and [callback notifications](/documentation/content-registration/verify-your-registration/notification-callback-service/) informing them when one of their works has been cited. Details of the citing works can be displayed on your website alongside the article. 

In 2023 we are developing a new API endpoint that will make citations more accessible to the community, for a preview see [this announcement](https://community.crossref.org/t/relationships-are-here/3523).
