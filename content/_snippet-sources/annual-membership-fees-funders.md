Since 2019, funders are able to register DOIs for the research grants they have awarded. Their annual membership fees are lower than for all other members, but their grant registration fees are higher. Funder fees are tiered depending on your annual award value.

|Total annual award value (USD) | Annual membership fee|
| :------| :-----------:|
|< 500k	       |USD 200|
|0.5-2 million     |USD 400|
|2.1-10 million      |USD 600|
|10.1-500 million     |USD 800|
|500.1 million - 1 billion      |USD 1,000|
|> 1 billion      |USD 1,200|
