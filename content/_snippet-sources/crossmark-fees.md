### Crossmark fees  

Crossmark allows members to register status updates to their published content, including corrections, retractions, and errata. As a member, it also enables you to put a button on both your html articles and PDFs, so readers can always know its status whether current, updated, or retracted. There is fee for Crossmark (note that up to the end of 2019 there were fees for Crossmark, but they no longer apply).
