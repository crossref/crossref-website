If you want to get and use our metadata, you don’t need to be a member. We have a free public REST API that you can use without contacting us. [Metadata Plus](/services/metadata-retrieval/metadata-plus/) is a dedicated pool of servers and is a good option if you want a more predictable service level than we can support with the free API, and it comes with monthly snapshots of XML or JSON data dumps for you to do more flexible and customised analyses. There is an annual subscription fee which is payable each January or wll be pro-rated for the remainder of the calendar year.

Please select your tier from the table below, using the following criteria: 

The fee tier is selected based on whichever is the higher between
- your **total annual revenue** (including earned and fundraised, e.g. grants); or
- your **annual operating expenses** (including staff and non-staff, e.g. occupancy, equipment, licenses etc.). 

The subscriber is always considered to be the **largest legal entity**, unless:
- you are a university, then the subscriber is considered to be the university department(s), school(s), or faculty(ies) that is using the service e.g. Department of Earth Sciences, School of Law, or Faculty of Medicine.
- you are a government body, then the subscriber is considered to be the agency(ies), department(s), or ministry(ies) that is using the service, e.g. Agence de l'Innovation Industrielle, Department of Energy, or Ministry of Consumer Affairs.

|Total annual revenue/funding or operating expenses | Annual fee |
|:------| :-----------:|
| < USD 500,000                   |   USD 550
| USD 500,001-USD 999,999         |   USD 2,200
| USD 1 - 5 million               |   USD 3,300
| USD 5,000,001-USD 9,999,999     |   USD 11,000
| USD 10 million-USD 25 million   |   USD 16,500
| > USD 25 million                |   USD 44,000
