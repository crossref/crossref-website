> #### The Crossmark button gives readers quick and easy access to the current status of an item of content, including any corrections, retractions, or updates to that record.

Crossmark provides a cross-platform way for readers to quickly discover the status of a research output along with additional metadata related to the editorial process. Crucially, the Crossmark button can also be embedded in PDFs, which means that members have a way of alerting readers to changes months or even years after it’s been downloaded. 

Research doesn’t stand still: even after publication, articles [can be updated](/documentation/register-maintain-records/maintaining-your-metadata/) with supplementary data or corrections. It’s important to know if the content being cited has been updated, corrected, or retracted. Crossmark makes this information more visible to readers. With one click, you can see if content has changed, and access valuable additional metadata provided by the member, such as key publication dates (submission, revision, acceptance), plagiarism screening status, and information about licenses, handling editors, and peer review.

Crossmark lets readers know when a substantial change affecting the citation or interpretation has occurred, and that the member has updated the metadata record to reflect the new status.

Watch the introductory Crossmark animation in your language:  
{{< vimeoPlayer id="services-crossmark">}} 

## Benefits of Crossmark <a id='00321' href='#00321'><i class='fas fa-link'></i></a>

* Members can report updates to readers and showcase additional metadata.
* Researchers and librarians can easily see the changes to the content they are reading, which licenses apply to the content, see linked clinical trials, and more.
* Anyone can access metadata associated with Crossmark through our [REST API](https://api.crossref.org), providing a myriad of opportunities for integration with other systems and analysis of changes to the scholarly record.

## How Crossmark works <a id='00323' href='#00323'><i class='fas fa-link'></i></a>

Members place the Crossmark button close to the title of an item on their web pages and in PDFs. They commit to informing us if there is an update such as a correction or retraction, as well as optionally providing additional metadata about editorial procedures and practices. 

<figure><img src='/images/documentation/Crossmark-check-for-updates.png' alt='Crossmark button' title='' width='50%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image29">Show image</button>
<div id="image29" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Crossmark-check-for-updates.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

While members who implement Crossmark provide links to update policies and commit themselves to accurately reporting updates, the presence of Crossmark itself is not a guarantee. However, it allows the community to more easily verify how members are updating their content.

If you use Crossmark, the Crossmark button must be applied to all of your new content, not just content is updated. Selective implementation means that a reader, such as a research or librarian, who downloaded a PDF version before the update would have no way to know that it has been updated. We also encourage you to implement Crossmark for backfile content, although doing so is optional. At least, we encourage you to do so for backfile content that has been updated.

## Obligations for Crossmark <a id='00325' href='#00325'><i class='fas fa-link'></i></a>

Any member [can provide update metadata](/documentation/register-maintain-records/maintaining-your-metadata/registering-updates/) and register an update policy. If you are a member who implements the Crossmark button, you must:

* Maintain your content and promptly register any updates.
* Include the Crossmark button on all digital formats (HTML, PDF, ePub).
* Implement Crossmark using the script provided by us.
* Not alter the Crossmark button in any way other than adjusting its size.

Implementing the Crossmark button involves technical changes to your website and production processes. Check that you have the necessarily expertise to implement these before you start. If not, you can start to deliver update metadata and implement the Crossmark button at a later point.

Any organisation can also implement the Crossmark button on pages where they display content. If you do so, you must follow the guidelines above, except for the first point if you are not reponsible for the content.

There are no additional fees to participate in Crossmark.

## How to participate in Crossmark <a id='00324' href='#00324'><i class='fas fa-link'></i></a>

There are several steps for members to fully implement Crossmark:

1. Devise an update policy, assign it a DOI, and register it with us. 
1. Add the update policy and, optionally, other relevant metadata to your metadata records. 
1. Publish corrections, retractions, and other updates for works where necessary, and register their metadata. See our [guidance on registering updates](/documentation/register-maintain-records/maintaining-your-metadata/registering-updates/).
1. Implement the Crossmark button online and in PDFs. 

Learn more about [participating in Crossmark](/documentation/crossmark/participating-in-crossmark).

To see which Crossref members are registering Crossmark information, visit [Participation Reports](https://www.crossref.org/members/prep/). These reports give a clear picture for anyone to see the metadata Crossref has including Crossmark data.

Learn more about [version control, corrections, and retractions](/documentation/crossmark/version-control-corrections-and-retractions).

<figure><img src='/images/documentation/Infographic-Crossmark.png' alt='Crossmark infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image30">Show image</button>
<div id="image30" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Crossmark.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


[Download the Crossmark factsheet](/pdfs/about-crossmark.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).
