> #### Reference linking enables researchers to follow a link from the reference list to other full-text documents, helping them to make connections and discover new things.

To link references, you don’t need to be a Crossref member. Reference linking means including Crossref DOIs ([displayed as URLs](/display-guidelines)) in the reference lists that you provide in your own published work.  This enables researchers to follow a link from a reference list to the current landing page for that referenced work. And because it’s a DOI rather than just a link, it will remain persistent.

So, instead of just including the reference...

> Soleimani N, Mohabati Mobarez A, Farhangi B. Cloning, expression and purification flagellar sheath adhesion of Helicobacter pylori in Escherichia coli host as a vaccination target. Clin Exp Vaccine Res. 2016 Jan;5(1):19-25.

...you should also display the DOI link:

> Soleimani N, Mohabati Mobarez A, Farhangi B. Cloning, expression and purification flagellar sheath adhesion of Helicobacter pylori in Escherichia coli host as a vaccination target. Clin Exp Vaccine Res. 2016 Jan;5(1):19-25. [https://doi.org/10.7774/cevr.2016.5.1.19](https://doi.org/10.7774/cevr.2016.5.1.19)

Because Crossref is all about rallying the scholarly community to work together, reference linking is an obligation for all Crossref members and for all 'current' resources (published during this and the two previous years). It is also encouraged for for backfile resources (published longer ago than current resources).

Watch the introductory reference linking animation in your language: 

{{< vimeoPlayer id="referencelinking">}} 
## Benefits of reference linking <a id='00253' href='#00253'><i class='fas fa-link'></i></a>

Persistent links enhance scholarly communications. Reference linking offers important benefits:

* **Reciprocity**: members’ records are linked together and more discoverable because all members link their references.
As a member organization, we can obligate all our members to link their references, so that individual members can avoid the inconvenience of signing bilateral agreements to link to persistent resources on other platforms. The result is a scholarly communications infrastructure that enables the exchange of ideas and knowledge.
* **Discoverability**: research travels further when everyone links their references. Because DOIs don’t break if implemented correctly, they will always lead readers to the resource they’re looking for, including yours. When the DOIs are displayed, anyone can copy and share them. This will also enable better tracking of where and when people are talking about and sharing scholarly objects, including in social media.

## Obligations and fees for reference linking <a id='00256' href='#00256'><i class='fas fa-link'></i></a>

There’s no charge for reference linking but it is an [obligation of membership](/documentation/metadata-stewardship/understanding-your-member-obligations/). Reference linking is required for all Crossref members and for all current resources. We’d encourage you to also add reference linking for backfile records too.

<figure><img src='/images/documentation/Infographic-Reference-linking.png' alt='Reference linking infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image19">Show image</button>
<div id="image19" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Reference-linking.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


[Download the reference linking factsheet](/pdfs/about-reference-linking.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).

## How to participate in reference linking <a id='00255' href='#00255'><i class='fas fa-link'></i></a>

Note that reference linking is not the same as [registering references](/documentation/content-registration/descriptive-metadata/references/) - learn more about [the differences](/blog/linking-references-is-different-from-depositing-references).

To link references, you do not need to be a member, but reference linking is an obligation for Crossref members. When your organization becomes a Crossref member, [look up the DOIs for your references](/documentation/reference-linking/how-do-i-create-reference-links/), and add the DOI (as a URL) to reference lists for your records.

## Best practice for reference linking <a id='00257' href='#00257'><i class='fas fa-link'></i></a>

* Start reference linking within 18 months of joining Crossref
* Link references for backfile as well as current resources
* Link references in all relevant resource types such as preprints, books, data, conference proceedings, etc.
* Make sure the links in your references and other platforms conform to our [DOI display guidelines](/display-guidelines)
