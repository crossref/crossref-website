## Relationships

Research doesn’t stand alone and relationships show the connections between research outputs, people, and organizations. We deliver these connections via a [relationships API endpoint](http://api.crossref.org/beta/swagger-ui/index.html#/Relationships/getRelationships), which makes the [Research Nexus](https://www.crossref.org/documentation/research-nexus/) visible.

Relationships show how items are related to each other. A published research article may be supported by data, software, and peer reviews; previous versions might be available as a preprint and there could be online commentary and annotations. Research is reused and cited in other research works, as well as being mentioned in online venues such as blog posts and news articles. The relationships endpoint makes all of these connections available in a single place.

We collect relationships from a number of sources:
 - Metadata deposited by our members
 - Links we discover between existing metadata records, for example, through reference matching.
 - [Event Data](/documentation/retrieve-metadata/event-data/) agents (not yet implemented).
 - Trusted community partners, such as DataCite. 

**The relationships endpoint is currently released as a beta version**, meaning that it may contain undetected data quality issues and bugs, and we will not respond immediately to performance issues. The dataset available currently includes works updated since the beginning of 2024.