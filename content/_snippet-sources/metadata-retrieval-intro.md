> Analyse Crossref metadata to inform and understand research

Crossref is the [sustainable source of community-owned scholarly metadata](https://doi.org/10.1162/qss_a_00022) and is relied upon by thousands of systems across the research ecosystem and the globe.

{{% divwrap align-right %}} 
{{< figure src="/images/documentation/metadata-users-uses.png" width="100%" caption="Some of the typical users (outer) and uses (inner) of Crossref metadata" alt="Diagram depicting many types of users and uses of Crossref metadata" >}}
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image32">Show image</button>
<div id="image32" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/metadata-users-uses.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>
{{% /divwrap %}}

People using Crossref metadata need it for all sorts of reasons including metaresearch (researchers studying research itself such as through bibliometric analyses), publishing trends (such as finding works from an individual author or reviewer), or incorporation into specific databases (such as for discovery and search or in subject-specific repositories), and [many more detailed use cases](/services/metadata-retrieval/user-stories/).

All Crossref metadata is open and available for reuse without restriction. Our `160,104,382` records include information about research objects like articles, grants and awards, preprints, conference papers, book chapters, datasets, and more. The information covers elements like titles, contributors, descriptions, dates, references, connecting identifiers such as Crossref DOIs, [ROR IDs](/community/ror/) and [ORCID iDs](/community/orcid/), together with all sorts of metadata that helps to determine provenance, trust, and reusability---such as funding, clinical trial, and license information. 

Take a look at a list of some of the [organizations who rely on our REST API](/services/metadata-retrieval/user-stories/) and read some of the [case studies from a selection of users](/categories/api-case-study/). [Download the metadata retrieval fact sheet](/pdfs/about-metadata-retrieval.pdf) or read more about the [types of metadata and records we have](/documentation/principles-practices/best-practices/).