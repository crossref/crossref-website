When you apply for membership, you agree to our [membership terms](/membership/terms/) which include several obligations. Some of the important ones to note, are:

### 1. Register only what you have the legal rights to
Our terms (3 (c)) stipulate that "The Member will not deposit or register Metadata for any Content for which the Member does not have legal rights to do so". 

### 2. Deposit metadata and create DOI links
All members join in order to create records with metadata and DOIs and share these throughout Crossref infrastructure for others to use. If you publish journals, you are obliged to create records for all articles going forward. 

### 3. Maintain and update your metadata and landing page URLs over the long term
You'll need to [maintain and update your metadata](/documentation/register-maintain-records/maintaining-your-metadata), including updating URLs if your content moves or changes, and adding rich metadata as you collect more. Look into archiving or other preservation services to ensure your records live on after you.

### 4. Make sure you have a unique landing page for each metadata record/DOI and follow our DOI display guidelines

- Make sure that your DOI links resolve to a [unique landing page URL](/documentation/member-setup/creating-a-landing-page/). For example, if you are registering journal articles, each article needs to have a unique landing page URL. It is the same if you are, for example, a funder: each DOI link needs to resolve to a page with information about the specific grant.      

- Follow our DOI [display guidelines](https://doi.org/10.13003/5jchdy) and position your DOI links on the page or PDF near the descriptive information such as title and contributors. Always use DOI links to communicate and share your work across the web whether in documents or databases.

### 5. Link references 
[Reference linking](/services/reference-linking/) was the original reason that the community decided to start Crossref. It is so that members don't have to establish bilateral linking agreements with each other and instead can create and exchange connections through the Crossref infrastructure. If your work is the kind that typically has a list of references (e.g. articles or preprints), you are obliged to retrieve and use the DOI links that your fellow Crossref members create. You can do this using our [reference linking tools](/documentation/reference-linking/how-do-i-create-reference-links). 

We also strongly encourage you to include references within your own metadata records. This benefits you and others by further maximising the network effect.

### 6. Pay your fees
Crossref is sustained by fees and not reliant on grant funding, which would conflict with our sustainability model, guided by our commitment to [the Principles of Open Scholarly Infrastructure (POSI)](https://openscholarlyinfrastructure.org/). Your fees keep the organization afloat and the system running. We are a not-for-profit organisation so any surpluses are invested back into improving the infrastructure and solving new problems for the community. Please pay your invoices on time 👍🏽