Many types of organisations register their research objects with Crossref. You could be a research institution, a publisher, a government agency, a research funder, or a museum! In order to become a member, you need to meet the criteria set out in our [membership terms](/membership/terms) approved by our governing board which defines eligibility as:

> Membership in Crossref is open to organisations that produce professional and scholarly materials and content.

_(NB: We are bound by international sanctions so membership is restricted in certain countries; see our [sanctions page](/operations-and-sustainability/membership-operations/sanctions/) for details.)_

In most cases, if your work is likely to be cited in the research ecosystem and you consider it part of the evidence trail, then you’re eligible to join.