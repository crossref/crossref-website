The *Settings* tab controls [general](#00568), [document](#00569), and [report display](#00570) options. These options include the number of documents shown for each page, default report view, and controlling email notifications.

<figure><img src='/images/documentation/SC-settings.png' alt='Settings' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image98">Show image</button>
<div id="image98" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-settings.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

<br>

## General settings (v1) <a id='00568' href='#00568'><i class='fas fa-link'></i></a>

<figure><img src='/images/documentation/SC-settings-general.png' alt='General settings' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image99">Show image</button>
<div id="image99" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-settings-general.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

<br>

Use *General* settings to set your home folder - this is the folder will open by default when you log in to iThenticate. Choose your home folder from the drop-down menu.

From the *Number of documents to show* drop-down, choose how many uploaded documents are listed in your folders before a new page is created.

Choose what is displayed after you upload a document to iThenticate: *Display the upload folder* (to see the processing of the document you have just uploaded), or *Upload another document* (returns you to the upload form).

You can also choose the time zone and language for your account - the language you choose will set the language of your user interface.

Click *Update Settings* to save your changes.

## Documents settings (v1) <a id='00569' href='#00569'><i class='fas fa-link'></i></a>

<figure><img src='/images/documentation/SC-settings-document.png' alt='Document settings' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image100">Show image</button>
<div id="image100" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-settings-document.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

<br>

Use *Documents* settings to choose the default way iThenticate sorts your uploaded documents: by processed date, title, Similarity Score, and author. Choose your preferred option from the drop-down menu.

You can set the threshold at which the Similarity Score color changes, based on the percentage of similarity. All Similarity Scores above the percentage you set will appear in the folder in blue, all those beneath the percentage will appear in gray. This visual distinction helps you easily identify matches above a given threshold. Learn more about [how to interpret the Similarity Score](/documentation/similarity-check/ithenticate-account-use/similarity-report-understand/).

Click *Update Settings* to save your changes.

## Reports settings (v1) <a id='00570' href='#00570'><i class='fas fa-link'></i></a>

<figure><img src='/images/documentation/SC-settings-reports.png' alt='Reports settings' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image101">Show image</button>
<div id="image101" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-settings-reports.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

<br>

Use *Reports* settings to adjust your email notifications, choose whether to color-code your reports, and view available document repositories for your account.

Email notifications tell you when a Similarity Report has exceeded particular thresholds, including Similarity Reports in shared folders. Email notifications are sent to the email address you used to sign up to iThenticate.

* **Report email frequency**: choose whether to receive notifications, chose how often you would like to receive them every hour, once a day, every other day, or once a week
* **Similarity Report threshold**: this refers to a paper’s overall Similarity Score. If the Similarity Score of a paper in your account exceeds the threshold set, you will receive an email notification. The default setting is 'don't notify me'.
* **Content tracking report threshold**: this refers to the *All Sources* section of the Similarity Report. If a single source for a paper in your account exceeds the similarity threshold set, you will receive an email notification. The default setting is *don't notify me*.

**Color code report**: color-coding the Similarity Report can make viewing matches easier. Choose *Yes* or *No* to enable or disable this feature.

**Available document repositories**: this section shows the available repositories for your account. Modify them in the [folder settings](/documentation/similarity-check/ithenticate-account-use/folders#00587).
