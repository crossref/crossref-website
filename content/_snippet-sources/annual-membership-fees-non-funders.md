Our membership fees for organizations who will be registering published research outputs are tiered depending on the publishing revenue or expenses of your organization. Please select your tier from the table below, and use the higher number of either:

1. Total annual **publishing revenue** from all the divisions of your organization (the member is considered to be the largest legal entity) for all types of activities (advertising, sales, subscriptions, databases, article charges, membership dues, etc). Or, if no publishing revenue then:
2. Total annual **publishing operations expenses** including (but not limited to) staff costs, hosting, outsourcing, consulting, typesetting, etc.

| Total publishing revenue or expenses | Annual membership fee |
| :----------------------------------- | :-------------------: |
| <USD 1 million                       |        USD 275        |
| USD 1 million - USD 5 million        |        USD 550        |
| USD 5 million - USD 10 million       |       USD 1,650       |
| USD 10 million - USD 25 million      |       USD 3,900       |
| USD 25 million - USD 50 million      |       USD 8,300       |
| USD 50 million - USD 100 million     |      USD 14,000      |
| USD 100 million - USD 200 million    |      USD 22,000      |
| USD 200 million - USD 500 million    |      USD 33,000      |
| >USD 500 million                     |      USD 50,000      |
