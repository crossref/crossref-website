+++
title = "Senior Software Developer"
date = 2023-10-12
draft = false
image = "/images/banner-images/fish.jpg"
author = "Michelle Cancel"
caption = "Photo by Isaac Mijangos on Pexels"
weight = 15
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed October 27, 2023. {{% /divwrap %}}

Do you want to help make research communications better in all corners of the globe? Come and join the world of nonprofit open infrastructure as our new **Senior Software Developer**.

* **Location**: Remote and global (with 3-hour overlap with the UTC-0 time zone)
* **Type**: Full-time
* **Remuneration**: Approx. EUR 90-95k/USD 128-132k/GBP 56-65k or local equivalent, depending on experience. Note this is a general guide (as there is no universal currency) and local benchmarking will take place before the final offer.
* **Reports to**: Head of Software Development, [Joe Wass](/people/joe-wass/)
* **Timeline**: Advertise and recruit in October/hire in November

### About the role

We're looking for the next member of the Software Development team. We build, operate and maintain metadata services that enable the global research community. We're on a journey to build features to serve the evolving needs of our changing membership, and migrate our legacy technology. As a team, we have a deep understanding not only of technology, but also the needs of our diverse community.

You will specify, design and implement improvements, features and services. You will have a key voice in discussions about technical approaches and architecture. You will always keep an eye on software quality and ensure that the code you and your colleagues produce is maintainable, well tested, and of high quality.

Our new code is written primarily in **Kotlin** on the back-end and Typescript on the frontend. We have legacy code in Java. **We don't expect you to be familiar with our technologies yet**, though it's a bonus. This is primarily a back-end role.

You should have in-depth knowledge of at least one compiled, typed or functional language (e.g. Java, Clojure, Kotlin, Scala, C#, Go, Typescript etc) and a history of learning new languages and technologies on the job.

We are a geographically distributed, remote-first team with flexible working hours. 

#### Key responsibilities

* Understand Crossref’s mission and how we support it with our services 
* Collaborate with external stakeholders when needed
* Pursue continuous improvement across legacy and green-field codebases
* Work flexibly in multi-functional project teams, especially with the Product team, to design and develop services 
* Ensure that solutions are reliable, responsive, and efficient
* Produce well-scoped, testable, software specifications
* Implement and test solutions using Kotlin, Java and other relevant technologies
* Work closely with the Head of Software Development to solve problems, maintain and improve our services and execute technology changes
* Provide code reviews and guidance to other developers regarding development practices and help maintain and improve our development environment
* Identify vulnerabilities and inefficiencies in our system architecture and processes, particularly regarding cloud operations, metrics and testing
* Communicate proactively with membership and technical support colleagues ensuring they have all the information and tools required to serve our users
* Openly document and share development plans and workflow changes
* Be an escalation point for technical support; investigate and respond to occasional but complex user issues; help minimize support demands related to our systems; be part of our on-call team responding to service outages

### About you

We don't expect a successful candidate to tick all of these boxes right away! If you have any questions, please get in touch. 

Qualities

* Comfortable collaborating with colleagues or stakeholders in the community
* Have a proven track record of picking up new technologies
* Outstanding at interpersonal relations and relationship management 
* Comfortable collaborating with colleagues across the organisation
* Self-directed, a good manager of your own time, with the ability to focus
* Comfortable being part of a distributed team
* Curious and tenacious at learning new things and getting to the bottom of problems

Skills

* An expert senior developer with experience in Java, Kotlin, or related languages
* Experience in Spring or similar frameworks
* Experienced with continuous integration, testing and delivery frameworks, and cloud operations concepts and techniques
* Familiar with AWS, containerization and infrastructure management using tools like Terraform
* Some experience with Python, JavaScript or similar scripting languages
* Experience working on open source projects
* Able to quickly understand, refactor and improve legacy code and fix defects
* A working understanding of XML and document-oriented systems such as Elasticsearch
* Experience building tools for online scholarly communication or related fields such as Library and information science, etc
* Ability to create and maintain a project plan
* Strong at written and verbal communication skills, able to communicate clearly, simply, and effectively

### About the team

The Crossref team is distributed across the world. The Software Development team is based in Europe and the US. We work alongside the Product, Infrastructure and Labs teams.

All new code, and most issue tracking, is open source. We strongly believe in open scholarly infrastructure and openness at all stages of the software development lifecycle. We perform code reviews and practice continuous deployment for all new code. 

As a membership organization we keep closely in touch with our users, and encourage our developers to be familiar with our community. 

We work fully remotely, but try to meet in person at least once a year. This is a full-time position, but working hours are flexible. The applicant should expect they will need to travel internationally to work with colleagues for about 5-10 days a year. If you have any questions we would be happy to discuss.

You can be based anywhere in the world where we can employ staff, either directly or through an employer of record.  

### About Crossref

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

Crossref sits at the heart of the global exchange of research information, and our job is to make it possible—and easier—to find, cite, link, assess, and reuse research, from journals and books, to preprints, data, and grants. Through partnerships and collaborations we engage with members in 148 countries (and counting) and it’s very important to us to nurture that community.

We’re about [45 staff](/people/) and remote-first. This means that we support our teams working asynchronously and to flexible hours. Some international travel will likely be appropriate, for example to in-person meetings with colleagues and members, but in line with our [travel policy](/blog/rethinking-staff-travel-meetings-and-events/). We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organisation. Check out the [organisation chart](/people/org-chart/).

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

### Thinking of applying?

This position is full time and, as for all Crossref employees, location is flexible. The Crossref team is geographically distributed in Europe and North America and we fully support working from home. It would be good to have a minimum 3-hour overlap with the UTC-0 time zone.

We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

Applications Closed

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

### Thanks for your interest in joining Crossref. We are excited to hear from you!
