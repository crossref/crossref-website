+++
title = "Support Manager"
date = 2018-02-01
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Amanda Bartell"
weight = 3
rank = 4
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position are closed as of 2018-04-01 {{% /divwrap %}}

## Come and work with us as our **Support Manager**. It'll be fun!
<br>
- **Location:** Remote/home-based: anywhere from the Pacific to Eastern timezones<br>
- **Reports to:** Head of Member Experience<br>
- **Benefits:** Competitive<br>

## About Crossref

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.

Since January 2000 we have grown from strength to strength and now have almost 10,000 members across 114 countries and thousands of tools and services relying on our metadata.

## About the team

We have big ambitions in the member and community outreach group. We’re thirteen-strong, (soon to be sixteen), and split between Boston, New York, London, and Oxford. We are at the forefront of Crossref’s growth, building relationships with new audiences in new markets in new ways. We cover member experience and support, marketing communications, outreach, business development, and metadata education. We’re embarking on a new onboarding program for the thousands of (mostly research publishers) that join as members every year. There are plans for an educational program for existing members and users. And we’re aiming for a more open approach to having conversations with people all around the world, in multiple languages. We are fortunate to have strong product management, finance, and technology teams to work closely with to achieve our objectives.

## About the role

This is a key role in the Member Experience section of our Member and Community Outreach team. In this role you’ll be working closely with two Support Specialists to handle the most technical support queries, ensure that the member experience team has the tools and processes to effectively support members and users, work closely with the DevOps and Product teams on bug fixes and new developments to support users, and communicate with members and users on service issues, both 1:1 and in public through e.g. Twitter and GitHub.

## Key responsibilities

### Handling the most technical support queries

- Answering member and user queries---using Zendesk, Twitter, Discourse, and GitHub---owning the problem through to resolution.
- Being the escalation point for other members of the support team, handling the most complex customer support issues.
- Managing and adjusting publication title information within our metadata system.
- Monitoring conflict reports and working with members to resolve.
- Monitoring DOI crawler reports and contacting publishers who do not maintain their DOIs.

### Tools and processes

- Managing the support systems, setting KPIs and ensuring regular reporting is accurate and actionable.
- Identifying peaks and troughs in support queries and finding ways to smooth them out.
- Implementing support through new channels as we move to a philosophy of "open support".
- Ensuring that the member experience and outreach teams have everything they need to work efficiently and effectively.
- Assisting other Crossref staff in understanding metadata and schema issues.

### Bug fixes and new developments

- Identifying problems/opportunities resulting from customer issues.
- Working closely with the technical team on issues impacting members, running regular technical review meetings with the development team.
- Feeding into service development conversations to ensure support overhead is kept to a minimum.
- Leading or participating in community working groups.

### Communicating with members on support issues

- Managing outbound communications regarding service outages through multiple channels.
- Monitoring and responding to external or internal reporting systems that indicate the health of the DOI and Crossref systems.
- Suggesting measures to improve visibility into quality conditions and ways to better assist members in working with us.

## About you

This important role in the Member Experience team provides support to our diverse member and user base with very different levels of technical knowledge and many different languages. It’s also the key bridge between our community and our own technical teams. You’ll need:

- Experience in providing technical support/troubleshooting with the ability to organize and prioritize a very busy helpdesk.
- Critical thinking and problem solving skills, with a high level of attention to detail and be comfortable digging into unfamiliar and complex technical issues. We need someone who is a problem-solver---curious and tenacious at learning new things and getting to the bottom of problems.
- Strong written and verbal communication skills with the ability to communicate clearly, simply and effectively.
- Able to communicate technical issues to less technical audiences and use open questions to get to the bottom of things when the question doesn't seem to make sense.
- Strong interpersonal and relationship management skills.
- A passionate customer service orientation with experience in managing multiple stakeholders.
- Ability to work with colleagues in different teams and at different levels.
- Experience with XML-based publishing systems (ideal) or just XML with exposure to metadata vocabularies.
- A philosophy of transparency in everything you do with strong experience providing support publicly e.g. through discussion forums, technical repositories, and social media.

## To apply

If you are considering joining Crossref and contributing to [our mission](/about), please send your cover letter and resume to: [Amanda Bartell](mailto:jobs@crossref.org)

---
