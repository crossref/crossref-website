+++
title = "Community Engagement Manager (Funders)"
date = 2024-12-05
draft = false
image = "/images/banner-images/birds-flying-sunflare.png"
author = "Michelle Cancel"
caption = "“Silhouette flock of bird at sunrise” by momnoi via iStock"
weight = 30
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position will be closed on January 8, 2025. {{% /divwrap %}}

Do you want to help improve research communications in all corners of the globe? Come and join the world of nonprofit open infrastructure and be part of improving the creation and sharing of knowledge as our new **Community Engagement Manager** for the funding community.

* **Location**: Remote and global (to at least partially overlap with working hours in European timezones)
* **Type**: Full-time
* **Remuneration**: 70-78k USD or local equivalent, depending on experience. Note this is a general guide (as there is no universal currency) and local benchmarking will take place before the final offer. 
* **Reports to**: Director of Community, [Kora Korzec](/people/kornelia-korzec/)
* **Timeline**: Advertise in December/offer by February

### About the role

The Crossref members document the progress of knowledge through our sustainable, global, and open system. We run an infrastructure so they can curate, share, and preserve metadata, which is information that underpins and describes all research activities such as funding, authorship, dissemination, and attention—and the relationships between these activities.

Increasingly, the organisations that drive open science through policy, funding, and related support are turning their attention to the underlying data infrastructure and, as part of that, are joining Crossref to provide their part of the picture and link their awards (including the use of facilities and equipment) with Crossref’s vast corpus of publications and other outputs. 

For five years, [the Crossref Grant Linking System](/services/grant-linking-system/) has been steadily gaining traction. We are now at a tipping point, and we need an experienced community manager with strong connections with funders and funding platforms to supercharge the program and take it to the next level by growing membership and facilitating even more integrations with grant platforms.

#### Key responsibilities


**Increase awareness and grow global funder membership**
* Create opportunities to engage funders globally—especially in Africa, Asia, and South America—and listen to and learn their needs, introduce Crossref, and invite them to engage with our part of the research ecosystem.
* Contribute to the Funder Advisory Group and help make their work mutually beneficial, i.e. facilitate the sharing of experiences between funders and help the group contribute to development such as schema evolution and governance and fee changes.
* Develop the pipeline of interested and eligible funders to grow membership and, therefore, participation in Crossref through more records and metadata for the Grant Linking System.
* Support funders in preparing for and meeting membership obligations and help onboard new members so that they can take advantage of all relevant services.

**Facilitate new integrations**
* Create an action-focused campaign to incentivise publishers and other Crossref members to report funding acknowledgements using Crossref grant links.
* Create action plans with the 20+ grant management systems to encourage and facilitate integrations with the Crossref GLS. This could mean working with community consultants and developers to commission and oversee such projects.

**Forge partnerships with related organisations**
* Identify organisations and initiatives with which to collaborate for mutual benefit, particularly outside of Western countries.
* Strengthen relationships with long-standing partners such as Altum and Europe PMC.
* Contribute to community initiatives and others’ working groups that support the shared vision for a connected research nexus.

**Support all Crossref programs and the Research Nexus vision**
* Listen to the sentiment and feedback of our community, share insights with colleagues and contribute to other programs and services.
* Represent Crossref, attending and speaking at relevant industry events, online and in-person, on topics even beyond funding and grants, and use the role to bring stakeholders together. 
* Create content, such as writing articles and blogs, slides and diagrams, updating documentation, and creating new resources in support of the above work.

### About you

We are looking for a proactive candidate with a unique blend of customer service skills, analytical trouble-shooting skills, and a passion to help others. You’ll have an interest in data and technology and will be a quick learner of new technologies. You’ll be able to build relationships with our community members and serve their very diverse needs - from assisting those with basic queries to really digging into some knotty technical queries. Because of this, you’ll also be able to distill those complex and technically challenging queries into easy-to-follow guidance. 

You’ll need:  

As scientific community engagement is an emerging profession, practical experience in this area is more important to us than traditional qualifications. 

We will prioritise candidates who can demonstrate most of these characteristics:

* Experience working with or within the research funding community, either in grant-making (directly or indirectly) or in leading a community initiative centred around funding and policy-making.
* Experience in community building and management and/or planning, executing and evaluating participatory initiatives, including group facilitation and relationship management.
* Collaborative attitude and evidence of co-creation
* Experience working within technical or metadata-focused initiatives and systems
* Curiosity to explore complex concepts and to learn new skills and perspectives
* Excellent communication both written and spoken
* Track record of project management, working to budget and timelines, and reporting on progress against clear goals.
* Confidence in public speaking in-person and online, including delivery of webinars/workshops.
* Tried and tested strategies for ensuring that your programs are equitable, diverse, and inclusive.

It would be a bonus if you also have any of the following:

* Ability to communicate in languages other than English 
* Experience working in global or multicultural settings 
* Experience with or strong understanding of open infrastructure and metadata

### About the team

The role is based within the Community team. We collaborate across a variety of projects and programs, and you will be asked to represent other programs and communities where practical. We adopt an approachable tone and style in our communications and enjoy systematic planning combined with flexibility and resourcefulness. We’re looking to re-engage with our community through face-to-face opportunities as well as online, so the work will involve some travel (according to [our thinking on travel and sustainability](/blog/rethinking-staff-travel-meetings-and-events/)). 

Our team’s primary aim is to engage colleagues from member organisations and other stakeholders to be actively involved in documenting the scholarly progress and making it transparent. This contributes to co-creating a robust [research nexus](/blog/seeing-your-place-in-the-research-nexus/). As part of the wider Community group at Crossref, we seek to encourage wider adoption and development of best practices in research communications with regard to metadata and the persistence and integrity of the scholarly record. [Colleagues across the organisation](/people/) are helpful, easy-going and supportive, so if you’re open-minded and ready to work as part of the team and across different teams, you will fit right in. Watch the recording of [our recent event celebrating 5 years of the GLS](/blog/celebrating-five-years-of-grant-ids-where-are-we-with-the-crossref-grant-linking-system/) to learn more about the current conversations in our community.  

### About Crossref

We’re a nonprofit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

We envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society. We are working towards this vision of a ‘Research Nexus’ by demonstrating the value of richer and connected open metadata, incentivising people to meet best practices, while making it easier to do so. “We” means 20,000+ members from 160 countries, 160+ million records, and nearly 2 billion monthly metadata queries from thousands of tools across the research ecosystem. We want to be a sustainable source of complete, open, and global scholarly metadata and relationships.

Take a look at our [strategic agenda](/strategy/) to see the planned work that aims to achieve the vision. The [sustainability](/operations-and-sustainability/) area aims to make transparent all the processes and procedures we follow to run the operation long-term, including our financials and our ongoing commitment to the Principles of Open Scholarly Infrastructure [(POSI)](https://openscholarlyinfrastructure.org/). The [governance](/board-and-governance/) area describes our board and its role in community
oversight.

It also takes a strong team – because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it. We are a distributed group of 46 dedicated [people](/people/) who like to play quizzes, talk about celery (sometimes cucumber), measure coffee intake, and create 100s of custom slack emojis. We enthusiastically support the Oxford comma but waver between use of American or British English. Occasionally we do some work to improve knowledge sharing worldwide—
which we take a bit more seriously than ourselves. We do this through fair policies and working practices, a balanced approach to resourcing, and accountability to each other.

We can offer the successful candidate a challenging and fun environment to work in. Together we are dedicated to our global mission and we are constantly adapting to ensure we get there. Take a look at our [organisation chart](/people/org-chart/),  the latest [Annual Meeting recordings](/crossref-annual-meeting/), and our financial information [here](/operations-and-sustainability/financials/).

### Thinking of applying?

We encourage applications from excellent candidates especially from people with backgrounds historically under-represented in research and scholarly communications. You can be based anywhere in the world where we can employ staff, either directly or through an employer of record. This position is full-time, however we can make accommodations for alternative schedules on request. Our team is fully remote and distributed across time zones and continents. This role will require some regular work in European time zones. Our main working language is English, but there are many opportunities in this job to use other tongues if you’re able. If anything here is unclear, please contact Kora Korzec, the hiring manager, on kora@crossref.org.

[Please apply via this form](https://100hires.com/j/hiZs7AB/apply) which allows us to sort your application materials into neat folders for a faster review. One of the best ways of offering evidence of your suitability within the cover letter is with an example of a relevant project you’re particularly proud of – we would particularly welcome mentions of your work with research funders. If possible, we’d also love to see an example of content you’ve created – a link to a recording of your talk, blog post, infographic, or something else. There is space to share documents and links within the application form.

Lastly, if you don’t meet the majority of the criteria we listed here, but are confident you’d be natural in delivering the key responsibilities of the role, we encourage your interest and would still like to hear what strengths you would bring.

We aim to start reviewing applications on **January 8, 2025**. Please strive to send us your documents by then.

The role will report to Kora Korzec, Director of Community at Crossref. She will review all applications along with Michelle Cancel, our HR Manager, and Ginny Hendricks, Chief Program Officer. 

We will invite selected candidates to a brief initial call to discuss the role as soon as possible following an initial review. Following that, shortlisted candidates will be invited to an interview. You will receive all questions in advance, and the interview will include some exercises you’ll have a chance to prepare for. All interviews will be held remotely on Zoom. 


### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

### Thanks for your interest in joining Crossref. We are excited to hear from you!