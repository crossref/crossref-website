+++
title = "Contract Member Support"
author = "Amanda Bartell"
date = 2023-04-24
draft = false
image = "/images/banner-images/join-hello-sand.jpg"
maskcolor = "crossref-blue"
weight = 1
parent = "Jobs"
+++

## Request for services: Member Support Contractor

**Location:** Remote

We’re looking for contractors able to work remotely and help us to welcome new members from around the world. There is no set schedule and contractors would bill their hours monthly.

Crossref receives over 200 new applications every month from organisations who produce scholarly and professional materials and content.

## Key responsibilities

- Manage queries from applicants and members via our Zendesk support desk and potentially other channels.
- Follow the administrative process for new applicants, such as:
  - Check the details in application forms that come via our website.
  - Set them up in our Customer Relationship Management (CRM) System (CRM). We use SugarCRM.
  - Send them an invoice for the first year of membership, and once this is paid...
  - Set up and share their DOI prefix and account credentials.
- Ensure that the information in our CRM is kept clean and up-to-date.
- Work closely with the Member Experience team and our finance colleagues.

## About you

- Organized with an eye for details
- Happy with data entry and maintenance
- Comfortable following processes and taking on new systems
- Friendly and clear communication skills (in English)

## About Crossref
Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.

Since 2000 we have grown from strength to strength and now have over 15,000 members across 140 countries, and thousands of tools and services relying on our metadata.

---

## How to respond

A statement of interest that includes:

- Examples of similar work (and/or your CV)
- References from previous work
- Hourly rate

Please send your response, statement of interest, and resume to: [jobs@crossref.org](mailto:jobs@crossref.org).

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities, in accordance with applicable law.
