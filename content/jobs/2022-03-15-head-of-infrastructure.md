
+++
title = "Head of Infrastructure Services"
date = 2022-03-15
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Geoffrey Bilder"
weight = 1
parent = "Jobs"
+++


{{% divwrap red-highlight %}} Applications for this position closed 2022-03-30. {{% /divwrap %}}
## Come and work with us as **The Head of Infrastructure Services**.

Help us build and run the infrastructure that underlies the global scholarly communications ecosystem.  

- **Location:** Remote. But we are looking for somebody in the UTC, UTC+1 Time zones (e.g., Ireland, UK, Scandinavia, Central Europe, West/Central Africa)
- **Salary:** Between 128-174K EUR (or equivalent) depending on experience and location. Benchmarked every two years.
- **Benefits:** Competitive.
- **Reports to:** Director of Technology and Research.
- **Closing date:** March 30, 2022

## About the role

Crossref is looking for a Head of Infrastructure to lead our infrastructure services team.

This is a senior role, and it's crucial to Crossref's mission and future ability to deliver on our [strategy](/strategy). In addition, it is an opportunity to push an entire organization's infrastructure and way of working with it forward. And all in the service of helping scholarly researchers communicate more openly, efficiently, and effectively.

We're looking for a new member of our technology team who can bring leadership experience and help steer us through some interesting operations, development, and cultural challenges. Crossref operates the service that connects thousands of scholarly publishers, millions of research articles, and research content and serves an increasingly diverse set of communities within scholarly publishing, research, and beyond.

You will report to the Director of Technology and Research and will lead a group of one developer and one system administrator. You will also work extensively with the software development, R&D, and product teams.

## Key responsibilities

The infrastructure services group is primarily responsible for Crossref's infrastructure services. That is, central, crosscutting tools and systems that are used by our software development group as the common foundation we use for delivering services to our members and the broader research community.

In other words- you will be leading the team that is responsible for building, deploying, and managing tools and services used by other developers.

You will be responsible for ensuring that these infrastructure services are reliable and responsive and making sure they can evolve quickly to support the new requirements and new services that Crossref is developing on behalf of its membership.

Your challenge will be to accomplish this whilst simultaneously helping to drive the modernization of our current software stack, infrastructure, and software engineering culture. The entire technology team is migrating from a mostly self-hosted, manually-managed, and manually-tested environment to a cloud-based system and the SRE tools and processes.

This is both a cultural change and a technological one, so we are looking for someone experienced in helping teams navigate and adapt to new ways of thinking and doing things.

We currently use a blend of AWS, Docker, Terraform, self-hosted VMWare, Elastic Search, Kafka, and more. Most of our codebases are written in Java, Clojure, and Python, with growing Kotlin and Typescript codebases. All the code we write is open source.

There are a lot of skills that we are looking for, but we don't expect to find a [purple unicorn](https://www.urbandictionary.com/define.php?term=Purple%20Unicorn). Instead, our primary criterion is that you have a track record of leading teams through change and delivering projects using a variety of tools, languages, frameworks, and development paradigms.

But you get double bonus points if you have experience with:

- Leading DevOps, system administration, or SRE teams.
- Transitioning on-prem data center to the cloud.
- In-depth knowledge of one or more cloud providers.
- Immutable infrastructure.
- Virtualization and containerization of legacy code bases.
- Configuration management.
- Security infrastructure.
- Automation of development.
- Site monitoring and alerting.
- Web services software development.

And it would be very useful if you had a subset of the following skills:

- Containerisation using ECS/Docker.
- Core AWS Infrastructure including EC2, VPC, S3, RDS, IAM, Route53, and Cloudfront.
- Infrastructure configuration, management, and orchestration tools (such as Terraform, Kubernetes, CloudFormation, Ansible, Salt, or equivalents).
- Java.
- High proficiency in at least one other language (e.g. Python, Clojure).
- Extensive experience with SQL, particularly PostgreSQL and Oracle.
- GitLab
- Elasticsearch, Solr, Lucene, or similar.
- Distributed logging and monitoring frameworks.
- Continuous Integration, continuous delivery frameworks.
- Modern, HTTP-based API design and implementation.
- Experience with open source development.
- Experience with agile development methodologies.
- Experience with XML- particularly with mixed content models.

And please note that this is not a back-office position. On the contrary, we believe that it is vital that the entire technical team develops an understanding of our members, the broader community, and their needs. Without this kind of empathy, we cannot add value to our services. As such, you will also find yourself working closely with the product and outreach teams.

## What it's like working at Crossref

We're about [40 staff](/people) and now 'remote-first' although we have optional offices in Oxford, UK, and Boston, USA. We are dedicated to an open and fair research ecosystem, and that's reflected in our ethos and staff culture. We like to work hard, but we have fun too! We take a creative, iterative approach to our projects and believe that all team members can enrich the culture and performance of our whole organization. This means that while this is a senior role, it is also a hands-on role, like all roles at Crossref. Check out the [organization chart](/people/org-chart).

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation, and we only continue to grow. While we won't have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

## Location & travel requirements

This is a remote position. The technology team currently has members working in the US (Lynnfield, MA, New York City, NY), UK (Oxford, Sheffield), Jersey,  Ireland (Dublin), and France (Nîmes). We are looking for somebody on the Eastern side of the Atlantic for this position. Ideally +/- 1-2 hours UTC.

In normal, non-pandemic circumstances (assuming they ever return), technology staff should expect they will need to travel 7-14 days a year (possibly international).

## To apply

Send a cover letter and a CV via email to:

Lindsay Russell

jobs@crossref.org

Please apply by 30th March 2022.

## Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

**Thanks for your interest in joining Crossref. We are excited to hear from you!**
