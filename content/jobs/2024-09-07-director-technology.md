+++
title = "Director of Technology"
date = 2024-09-07
draft = false
image = "/images/banner-images/trails-rocks.jpg"
author = "Michelle Cancel"
summary = "Deliver next-level open infrastructure for global open science."
rank = 1
weight = 3
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed October 4, 2024. {{% /divwrap %}}

Do you want to take the lead in delivering next-level infrastructure for global open science? Come and join the global world of open research metadata as our new **Director of Technology**.

* **Location**: Remote and global
* **Type**: Full-time
* **Remuneration**: 160K USD or local equivalent. Note this is a general guide (as there is no universal currency) and local currency analysis will take place before the final offer.
* **Reports to**: Chief Operations Officer, [Lucy Ofiesh](/people/lucy-ofiesh/)
* **Timeline**: Advertise and recruit September-October, offer in November

### About the role

Crossref is seeking a Director of Technology to play a key role in the leadership team and to develop and execute Crossref’s technical strategy. Reporting to the Chief Operations Officer, the Director of Technology will lead a talented team of software developers to build and maintain a robust, scalable, and innovative open scholarly infrastructure. As part of the leadership team, they will contribute to
setting Crossref’s organisational strategy, develop and implement the organisation’s technology strategy, report to colleagues and the board, lead the technology team, and collaborate with other open scholarly infrastructure organisations.

Technology at Crossref helps us fulfil our vision of a rich and reusable open network of relationships connecting research organisations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society. 

We maintain a database of 161+ million metadata records registered by our nearly 20,000 members over the past 25 years. Our system acts as the backbone for preserving the scholarly record. We offer a wide array of services to ensure that scholarly research metadata is registered, linked, and distributed. When members register their content with us, we collect both bibliographic and non-bibliographic metadata. We process it so that connections can be made between publications, people, organisations, and other associated outputs. We preserve the metadata we receive as a critical part of the scholarly record. We also make it available across a range of interfaces and formats so that the community can use it and build tools with it.

Technology is a critical enabler of our mission and scalability and modernisation are at the heart of our strategy. By transitioning to a fully cloud-based infrastructure and modernising our systems and services, we will enhance our ability to meet the evolving needs of our community and keep pace with the growth in the amount and complexity of our metadata, the rapid changes in scholarly research and
communications, and the development of Open Research globally. When done right, our technology can: 

* enable best practices in scholarly communications
* solve problems shared by our 20,000 global members. We act on behalf of our members to advance their interests
* strengthen Crossref as a comprehensive, reliable, and secure infrastructure upon which ourcommunity can build tools and services
* adapt to how the scholarly community evolves to keep pace with our scale of growth
* model openness, ensuring that our data and software are available for reuse or inspection as a public good whenever possible, and support collaborative community development of services when possible

The Director of Technology will lead an experienced team of 11 people and work closely with the leadership and senior management teams. The right candidate will be experienced at managing a team, setting priorities, and defining practices.

This role will work closely with the Programs group to understand the community’s needs and design solutions. Although Crossref is not a traditional software company, we provide critical open infrastructure with a mission to deliver innovative, cloud-based solutions for our members and the broader scholarly communications community. To do that, we need to centre the community in our development and hire technology leadership that focuses on excellence in architecture and delivery.

### Key responsibilities

#### Technology Leadership:
* Develop and execute a comprehensive technology strategy that aligns with Crossref’s mission and goals, fostering innovation and continuous improvement and building out the research nexus vision.
* Collaborate with the leadership team to integrate technology initiatives into the organisation’s overall strategy.
* Lead the modernization of Crossref’s systems and infrastructure.
* Identify and assess new technologies and trends to inform decision making and ensure Crossref remains at the forefront of scholarly infrastructure innovation.
* Report to the board of directors and collaborate with the leadership team to make
recommendations to the board.
* Work closely with cross-functional teams, including product management, finance and
operations, and membership and community outreach.

#### Technical Operations and the Crossref System:
* Oversee the design, development, and maintenance of Crossref’s systems, services and technical infrastructure.
* Ensure high availability, security, and scalability of systems, including APIs, databases, and web services.
* Develop and manage technology budgets, vendor relationships, and effective resource
allocation.
* Identify opportunities for process optimization, automation, and efficiency gains.
* Collaborate closely with stakeholders across the organisation to understand requirements and deliver technology solutions that meet their needs.

#### Team Management and Mentorship:
* Lead and inspire a high-performing technology team, fostering a collaborative and inclusive culture that values diversity and professional growth.
* Lead the technology team in adopting best practices, methodologies, and industry standards to ensure high-quality, scalable, and secure systems.
* Provide strategic guidance, mentorship, and support to team members, encouraging their professional development and career advancement.
* Promote a culture of continuous learning, knowledge sharing, and cross-functional
collaboration within the technology team and across the organisation.

#### Collaboration and Community Engagement:
* Collaborate with adopters of the Principles of Open Scholarly Infrastructure and other open infrastructure organisations to enhance interoperability and data sharing.
* Engage with the scholarly community, attending conferences, workshops, sprints, and forums.
* Represent Crossref in technical discussions and contribute to open standards and protocols.
* Actively encourage community participation from the team and encourage co-creation and open-source contributions within the community.

### About you

The ideal candidate will be a strategic thinker with experience in big data, architecting systems, implementing change and leading technology teams.

#### Key professional experiences:
* Minimum of 10 years of progressive experience in technology leadership roles, with a proven track record of leading and managing high-performing teams.
* Proven ability to lead and manage change, foster innovation, and drive continuous
improvement in technology initiatives.
* Proven track record of technology leadership in complex, mission-driven organisations.
* Strong understanding of open-source technologies, cloud computing, distributed systems architectures, APIs, and web services.
* Experience balancing technical excellence with practical business needs.
* Extensive knowledge of software development methodologies, project management, and
technology stack selection.
* Demonstrated experience in metadata management, data integration, and interoperability standards within scholarly communications or related domains.
* Experience leading cybersecurity efforts and knowledge of best practices in cybersecurity.
* Familiarity with scholarly publishing, research workflows, and metadata standards (e.g., DOI, ORCID) is a plus.
* Bachelor’s or master’s degree in computer science, information technology, or a related field.

#### Key Skills:
* Strong strategic thinking and problem-solving abilities.
* Excellent communication and collaboration skills with the ability to articulate technical concepts to non-technical audiences.
*  People management.
*  Financial management and budgeting.
*  Passion for Crossref's mission and commitment to open scholarly infrastructure and research integrity.

### About Crossref and the team

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

We envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society. We are working towards this vision of a ‘Research Nexus’ by demonstrating the value of richer and connected open metadata, incentivising people to meet best practices, while making it easier to do so. “We” means 20,000+ members from 160 countries, 160+ million records, and nearly 2 billion monthly metadata queries from thousands of tools across the research ecosystem. We want to be a sustainable source of complete, open, and global scholarly metadata and relationships.

Take a look at our [strategic agenda](/strategy/) to see the planned work that aims to achieve the vision. The [sustainability](/operations-and-sustainability/) area aims to make transparent all the processes and procedures we follow to run the operation long-term, including our financials and our ongoing commitment to the Principles of Open Scholarly Infrastructure [(POSI)](https://openscholarlyinfrastructure.org/). The [governance](/board-and-governance/) area describes our board and its role in community
oversight.

It also takes a strong team – because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it. We are a distributed group of 46 dedicated [people](/people/) who like to play quizzes, talk about celery (sometimes cucumber), measure coffee intake, and create 100s of custom slack emojis. We enthusiastically support the Oxford comma but waver between use of American or British English. Occasionally we do some work to improve knowledge sharing worldwide—
which we take a bit more seriously than ourselves. We do this through fair policies and working practices, a balanced approach to resourcing, and accountability to each other.

We can offer the successful candidate a challenging and fun environment to work in. Together we are dedicated to our global mission and we are constantly adapting to ensure we get there. Take a look at our [organisation chart](/people/org-chart/) and view our Annual Reports and financial information [here](/operations-and-sustainability/financials/).

### How to apply

To apply, please submit a CV and cover letter, detailing how you fulfil the role description and personal specification to [Perrett Laver’s application page](https://candidates.perrettlaver.com/vacancies/4364/director_of_technology/) quoting reference 7465. The deadline for applications is Friday, October 4, 2024.

This is a remote position and the successful candidate can be based most anywhere as long as they are prepared to adapt their hours to European and East Coast US time zones. A moderate amount of travel is expected.

Anticipated salary for this role is approximately $160,000 USD, or equivalent amount paid in local currency. Crossref offers competitive compensation, a rich benefits package, flexible work arrangements, professional development opportunities, and a supportive work environment. As a non-profit organisation, we prioritize mission over profit.

The selection committee will together review all candidates’ applications and agree on a longlist for the role. Longlisted candidates will be invited to discuss the position with Perrett Laver in greater detail. The selection committee will subsequently meet to decide upon a final shortlist to be invited to the formal interview stage.

Protecting your personal data is of the utmost importance to Perrett Laver and we take this responsibility very seriously. Any information obtained by our trading divisions is held and processed in accordance with the relevant data protection legislation. The data you provide us with is securely stored on our computerized database and transferred to our clients for the purposes of presenting you as a candidate and/or considering your suitability for a role you have registered interest in.

Perrett Laver is a Data Controller and a Data Processor, and our legal basis for processing your personal data is ‘Legitimate Interests’. You have the right to object to us processing your data in this way. For more information about this, your rights, and our approach to Data Protection and Privacy, please see our [Privacy Statement](https://perrettlaver.com/privacy-statement/).

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

### Thanks for your interest in joining Crossref. We are excited to hear from you!