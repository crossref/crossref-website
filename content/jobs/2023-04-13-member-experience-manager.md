+++
title = "Member Experience Manager"
date = 2023-04-13
draft = false
image = "/images/banner-images/fish.jpg"
author = "Michelle Cancel"
caption = "Photo by Isaac Mijangos on Pexels"
weight = 15
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed May 1st, 2023. {{% /divwrap %}}

Do you want to help make research communications better in all corners of the globe? Come and join the world of nonprofit open infrastructure and be part of improving the creation and sharing of knowledge as our brand new **Member Experience Manager**.

* **Location**: Remote and global (to overlap with colleagues in Indonesia and East Coast USA)
* **Remuneration**: Approx. EUR 58,000 - 70,000 or local equivalent, depending on experience. Note this is a general guide (as there is no universal currency) and local benchmarking will take place before final offer.
* **Reports to**: Head of Member Experience, [Amanda Bartell](/people/amanda-bartell/)
* **Timeline**: Advertise and recruit in April/May; offer by end of May

### About the role

This position is a mix of community and relationship management alongside business process management, data quality, and analytics; it’s a very varied role and ideal for an experienced generalist with a passion for collaboration and transparency.

You’ll be managing two membership specialists to help ensure our members have a smooth experience with us, with a particular focus on a carefully-managed application and onboarding process, reducing manual tasks, and making things more efficient and transparent wherever possible. You’ll be ensuring that our members understand the role that they and Crossref play in building the vision of a [shared research nexus](/documentation/research-nexus/) and helping them to join and contribute the best quality and quantity of metadata about their research with the global scholarly community. You’ll be overseeing data integrity and reporting on trends to provide actionable insights. You’ll be active in the scholarly communications community, contributing to volunteer-led co-creation initiatives. And you’ll work hand-in-hand with community engagement colleagues to support key programs for members, sponsors, service providers, and metadata users.

#### Key responsibilities

1. Managing our small membership team of two member support specialists (one based in the UK, one based in Indonesia), along with three membership contractors.
2. Managing the [new member](/members-area/) onboarding process to ensure members have all the information they need to succeed.
    * Supporting the membership specialists by answering particularly involved or knotty questions through our support system (Zendesk), our Community [Forum](https://community.crossref.org) (Discourse), face-to-face (via zoom and in person), or on social media like our growing [mastodon](https://mastodon.online/@crossref) presence.
    * Making our membership application process as smooth as possible for new members, while ensuring that applicants have all the information they need to get the most out of their membership.
    * Identifying and implementing process improvements for a more efficient experience (including for our staff) by eliminating manual and intensive tasks where possible.
    * Managing our automated onboarding email program.
3. Working with long-term members to make the most of their membership and follow the member obligations
    * Providing virtual (and in-person) training, support, and metadata ‘health checks’, following up and triaging issues to other expert colleagues.
    * Identifying issues and working proactively with members to solve problems, including any who are not meeting their membership obligations.
4. Working with the membership specialists to ensure that our member data is accurate and up-to-date, and that our CRM system (Sugar) can meet the organisation’s reporting needs. Using the CRM reports to provide actionable insights on trends.
5. Being a key point of contact for and working with our finance team to help improve the payment and invoicing experience for everyone.
6. Supporting meeting our openness and transparency goals (see [POSI](https://openscholarlyinfrastructure.org/)) by exposing publicly all membership operations and activities.
7. Participating in community events and volunteer initiatives to maintain an awareness of community issues and providing guidance or co-creating shared resources.
8. Working hand-in-hand with community engagement colleagues to support key programs for sponsors, service providers, and metadata users - as these programs support all our members.
9. Helping to create and implement rollout plans for new features that will affect our community, such as new ways of logging in or interacting with our systems, changes to fees, or opportunities to participate in or test Crossref services and initiatives.

### About you

We’re looking for a colleague who will take this opportunity and make it their own. While we have many documented processes for handling such a large membership operation, your fresh eyes will be able to highlight and improve how we work. Ideally, you will have an understanding of the dynamics within the academic research and open science environment, know what metadata is and why it matters, and have the ability to engage with and enthuse a wide range of stakeholders. You’ll have a love of data and analytics, a comfort level with metadata and databases, and a logical, systematic approach to prioritising and work in general.

* You’ll be experienced working within the broad areas of community, process, and data. You’ll be as comfortable demonstrating online tools or explaining complex concepts (such as the ‘research nexus’) as you are with handling detailed data and systems like CRMs. And you are a persuasive presenter with excellent written English skills (with other languages highly desirable).
* It’s particularly important that you can explain complicated concepts and multi-step processes clearly and convincingly.
* You’re able to build strong relationships and collaborate with internal teams and community partners, keeping member experience at the forefront of colleagues’ activity. You’ll be comfortable taking the initiative to lead conversations with people at all levels, with team management experience with remote and international teams.
* You’ll need to be extremely organized and attentive to detail. You’ll be able to follow set processes carefully and accurately, but adapt when situations change and simplify processes where possible.
* You’ll have a passion to understand and improve member experience, and are able to build credibility, trust, and relationships with our members, sponsors, service providers, metadata users, and partners.
* You’re a quick learner of new technologies and can rapidly pick up new programs and systems. You’ll ideally have experience with Zendesk or similar support systems and have used CRM systems (such as Sugar) - and you might even dabble in XML or JSON (we all succumb eventually 😁).
* You’ll have a truly global perspective - we have 18,000 member organizations from 150 countries across numerous time zones and they engage and interact with us in numerous ways.

### About Crossref

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

Crossref sits at the heart of the global exchange of research information, and our job is to make it possible—and easier—to find, cite, link, assess, and reuse research, from journals and books, to preprints, data, and grants. Through partnerships and collaborations we engage with members in 148 countries (and counting) and it’s very important to us to nurture that community.

We’re about [45 staff](/people/) and remote-first. This means that we support our teams working asynchronously and to flexible hours. Some international travel will likely be appropriate, for example to in-person meetings with colleagues and members, but in line with our [travel policy](/blog/rethinking-staff-travel-meetings-and-events/). We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organisation. Check out the [organisation chart](/people/org-chart/).

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

This role is in our Member Experience team, part of our larger Community Outreach team - a fourteen-strong team split across the US, Africa, Asia and Europe.


### Thinking of applying?

We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

### Thanks for your interest in joining Crossref. We are excited to hear from you!
