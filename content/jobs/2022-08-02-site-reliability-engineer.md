+++
title = "Site Reliability Engineer"
date = 2022-08-03
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Stewart Houten"
weight = 1
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed 2022-08-23. {{% /divwrap %}}
## Come and work with us as a **Site Reliability Engineer**.

Help us to build and run the infrastructure that underlies the global scholarly communications ecosystem.

- **Location:** Remote. But we are looking for somebody in the UTC, UTC+1 Time zones (e.g., Ireland, UK, Scandinavia, Central Europe, West/Central Africa).
- **Salary:** Between 101K-125K EUR (or local equivalent) depending on experience and location. Benchmarked every two years. Excellent benefits.
- **Reports to:** Head of Infrastructure Services.
- **Closing date:**  August 23rd, 2022.

## About the role

Crossref is looking for a talented Site Reliability Engineer to help us optimize and evolve our infrastructure services.

We're looking for a new member of our technology team who can bring experience, leadership, and help us solve some interesting operations and development challenges. Crossref operates the service that connects thousands of publishers, millions of articles and research content, and serves a diverse set of communities within scholarly publishing, research and beyond.

You will report to the head of infrastructure services and will work closely with one systems administrator and extensively with the software development, R&D, and product teams also.

## Key responsibilities

The infrastructure services group is primarily responsible for Crossref's infrastructure services. That is, central, crosscutting tools and systems that are used by our software development group as the common foundation we use for delivering services to our members and the broader research community. In other words- you will be building, deploying, monitoring and managing tools and services used by other developers.

You will be responsible for ensuring that these infrastructure services are reliable and responsive as well as making sure they are able to evolve quickly to support the new requirements and new services that Crossref is developing on behalf of its membership.

Your challenge will be to accomplish this, whilst simultaneously helping to drive the modernization of our current software stack, infrastructure, and software engineering culture. The entire technology team is undertaking a migration from a mostly self-hosted, manually-managed, and manually-tested environment, to a cloud-based system and the SRE tools, processes and culture which that entails.

We currently use a blend of AWS, Docker, Terraform, self-hosted VMWare, Elastic Search, Kafka and more. Most of our codebases are written in Java, Clojure, and Python.

There are a lot of skills that we are looking for, but we don’t expect to find a [purple unicorn](https://www.urbandictionary.com/define.php?term=Purple%20Unicorn). Our primary criterion is that you have a track record of being able to deliver projects using a variety of languages, frameworks and development paradigms.

But you get double bonus points if you have experience with:

- Immutable infrastructure.
- Virtualization and containerization of legacy code bases.
- Configuration management.
- Security infrastructure.
- Automation of development.
- Site monitoring and alerting.
- Web services software development.
- Transitioning on-premise datacentre to the cloud.
- In-depth knowledge of one or more cloud providers.

And it would be very useful if you had a subset of the following skills:

- Containerisation using ECS/Docker.
- Core AWS Infrastructure including EC2, VPC, S3, RDS, IAM, Route53 and Cloudfront.
- Infrastructure configuration, management and orchestration tools (such as Terraform, Kubernetes, CloudFormation, Ansible, Salt, or equivalents).
- Java.
- High proficiency in at least one other language (e.g. Python, Clojure).
- Extensive experience with SQL, particularly PostgreSQL, MySQL or Oracle.
- Elasticsearch, Solr, Lucene, or similar.
- Distributed logging and monitoring frameworks.
- Continuous Integration, continuous delivery frameworks.
- Modern, HTTP-based API design and implementation.
- Experience with open source development.
- Experience with agile development methodologies.
- Experience with XML- particularly with mixed content models.

And please note that this is not a back-office position. We believe that it is vital that the entire technical team develops an understanding of our members, the broader community and their needs. Without this kind of empathy, we cannot add value to our services. As such, you will also find yourself working closely with the product and outreach teams.

## Location & travel requirements

This is a remote position. The technology team currently has members working in the UK, Europe and the east coast of the US. As a remote-first organization, we are not bound to a specific location.

Remote workers should expect they will need to visit an office approximately 5 days a quarter along with the travel (possibly international) which that entails. If you work from an office you will be expected to travel internationally for ~ 5 days once a year. In either case, travel _can_ increase should you have an interest in representing Crossref at community events.

## About Crossref

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.

Since January 2000 we have grown from strength to strength and now have over 12,000 members across 121 countries, and thousands of tools and services relying on our metadata.

We can offer the successful candidate a challenging and fun environment to work in. We’re fewer than 40 professionals but together we are dedicated to our global mission. We are constantly adapting to ensure we get there, and we don’t tend to take ourselves too seriously along the way.

## Equal opportunities commitment

Crossref is an equal opportunity employer. We believe that diversity and inclusion among our staff is critical to our success as a global organization, and we seek to recruit, develop and retain the most talented people from a diverse candidate pool.

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities, in accordance with applicable law.

## To apply

Send your CV and covering letter via email to:

Stewart Houten, Head of Infrastructure Services

jobs@crossref.org
