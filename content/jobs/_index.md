+++
title = "Jobs"
date = "2024-09-07"
draft = false
image = "/images/blog/crossref-strategic-direction-star-go-kart.jpg"
author = "Michelle Cancel"
Rank = 2
aliases = [
   "/01company/jobs.html"
]
[menu.main]
parent = "About us"
weight = 30
+++

Help us achieve [our mission](/strategy) to make research outputs easier to find, cite, link, assess, and reuse. We're a small but mighty group working with over 20,000 members from 160 countries, and we have thousands of tools and services relying on our metadata, which sees over 2 billion queries every month on average. We are fully remote and have 46 staff spanning San Diego to Hong Kong and we all like to interact with and co-create with our engaged community.

We take our work seriously but usually not ourselves... so come and work with us - where else can you do something a bit geeky and important that is also sometimes fun?! 


{{% divwrap blue-highlight %}}

**[Data Scientist](/jobs/2025-01-16-data-scientist)**, posted January 16, 2025

{{% /divwrap %}}


Take a look at our [current team](/people) and check out the [org chart](/people/org-chart) to see where our vacancies fit in. We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

<!-- {{% divwrap yellow-highlight %}} We don't have any jobs open currently; please check back another time! {{% /divwrap %}}   -->

<!-- ## Open Positions

{{% divwrap blue-highlight %}} <a href="">Title</a> (closes YYYY-MM-DD) {{% /divwrap %}} -->

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law. We regularly review and revise our [code of conduct](/code-of-conduct).

---

Please contact our HR Manager, [Michelle Cancel](mailto:jobs@crossref.org), for any quest