+++
title = "Technical Support Contractor"
date = 2024-10-31
draft = false
image = "/images/banner-images/birds-flying-sunflare.png"
author = "Isaac Farley"
caption = "“Silhouette flock of bird at sunrise” by momnoi via iStock"
weight = 30
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed December 5, 2024. {{% /divwrap %}}
## Request for services: contract technical support

Come and work with us as an independent **Technical Support Contractor**. It’ll be fun!

**Location:** Remote and global
 
## About the contractor role

The Technical Support Contractor will work closely with our Membership team, part of Crossref’s Programs team, a distributed team with members across Africa, Asia, Europe, and the US. We’re at the forefront of Crossref’s growth, building relationships with new communities in new markets in new ways. We’re aiming for a more open approach to having conversations with people all around the world - including within our growing community forum, which the right candidate will help us expand, in multiple languages. We’re looking for a Technical Support Contractor to provide front-line help to our international community of publishers, librarians, funders, researchers and developers on a range of services that help them deposit, find, link, cite, and assess scholarly content. 

We’re looking for an independent contractor able to work remotely. There is no set schedule and contractors bill hours monthly. 

## Scope of work

- Replying to and solving community queries using the Zendesk support system.
- Using our various tools and APIs to find the answers to these queries, or pointing users to support materials that will help them.
- Working with colleagues on particularly tricky tickets, escalating as necessary.
- Working efficiently but also kindly and with empathy with our very diverse, global community.

## About the team

You’ll be working closely with nine of the technical and membership support staff to provide support and guidance for people with a wide range of technical experience. You’ll help our community create and retrieve metadata records with tools ranging from simple user interfaces to robust APIs.

## About Crossref

We’re a nonprofit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

We envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society. We are working towards this vision of a ‘Research Nexus’ by demonstrating the value of richer and connected open metadata, incentivising people to meet best practices, while making it easier to do so. “We” means 20,000+ members from 160 countries, 160+ million records, and nearly 2 billion monthly metadata queries from thousands of tools across the research ecosystem. We want to be a sustainable source of complete, open, and global scholarly metadata and relationships.

Take a look at our [strategic agenda](/strategy/) to see the planned work that aims to achieve the vision. The [sustainability](/operations-and-sustainability/) area aims to make transparent all the processes and procedures we follow to run the operation long-term, including our financials and our ongoing commitment to the Principles of Open Scholarly Infrastructure [(POSI)](https://openscholarlyinfrastructure.org/). The [governance](/board-and-governance/) area describes our board and its role in community
oversight.

It also takes a strong team – because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it. We are a distributed group of 46 dedicated [people](/people/) who like to play quizzes, talk about celery (sometimes cucumber), measure coffee intake, and create 100s of custom slack emojis. We enthusiastically support the Oxford comma but waver between use of American or British English. Occasionally we do some work to improve knowledge sharing worldwide—
which we take a bit more seriously than ourselves. We do this through fair policies and working practices, a balanced approach to resourcing, and accountability to each other.

## How to respond  

**Responses should be submitted by December 31st, 2024**:

A statement of interest that includes:

- Examples of similar work (and/or your CV)
- References from previous work
- Hourly rate

Please send your response, statement of interest, and resume to: [jobs@crossref.org](mailto:jobs@crossref.org).