+++
title = "Billing Support Specialist, Part-time"
date = 2024-10-03
draft = false
image = "/images/banner-images/fish.jpg"
author = "Michelle Cancel"
caption = "Photo by Isaac Mijangos on Pexels"
weight = 15
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed October 21st, 2024. {{% /divwrap %}}

Do you want to help make research communications better in all corners of the globe? Come and join the world of nonprofit open infrastructure as our **Billing Support Specialist, Part-time**.

* **Location**: Remote and global (to overlap with colleagues in East Coast USA)
* **Type**: Part-time with 15 to 25 hours per week
* **Remuneration**: $20/hour USD or local equivalent. Note this is a general guide (as there is no universal currency) and local currency analysis will take place before the final offer.
* **Reports to**: Accounts Receivable Manager, [Amy Bosworth](/people/amy-bosworth/)
* **Timeline**: Advertise and recruit in October/interview and offer in November

### About the role

Reporting to the Accounts Receivable Manager, the part time Billing Support Specialist is a key support role within the Finance Team. 

The Billing Support Specialist assists in the administration of the financial functions for the Finance Team, with a focus on supporting the billing needs for our global membership community. Crossref has more than 20,000 members in 160 countries; this role will manage inquiries from members across the world, and as such an ideal candidate will be excited to engage with a wide range of community members. The Billing Support Specialist will need to exercise judgment in selecting and applying established procedures correctly and in determining when to refer situations to other support staff.

#### Key responsibilities

* Monitors Zendesk for billing inquiries and resolves or distributes when appropriate
* Oversees membership form requests
* Supports in collections initiatives
* Assists in payment application within accounting platform
* Aids in other ad hoc financial, administrative and operations projects

### About you

This role provides assistance and support to the Accounts Receivable Manager, the Billing Support Specialist and Director of Finance. This position also works closely with the Membership Experience team. 

The Billing Support Specialist will need to be organized and have exemplary communication skills. The Billing Support Specialist will be exposed to Accounts Receivable and other aspects of Finance where accuracy will be essential.


You’ll need:  

* 2-3 years of customer service experience
* Experience in Accounting Systems (Sage Intacct would be ideal) and Microsoft Excel 
* Strong written and verbal communication skills with the ability to communicate clearly and effectively
* Prior experience with a globally-minded organization is a plus  
* Critical thinking and problem-solving skills
* The ability to learn standard processes and apply them when appropriate
* The ability to track progress on initiatives and follow up on inquires
* High level of attention to detail 

### About the team

The Finance Team at Crossref holds the responsibility for recording and reporting on financial transactions accurately and in a timely manner. We also support the overall membership experience, by billing members accurately and timely, and actively communicating to resolve inquiries they may have about their invoices or payment options. We follow carefully crafted financial controls to ensure that our Financial Reports are robust, accurate and up to date. We work closely with Membership, Technology, and Programs in a variety of ways. Our goal is to provide accurate and timely information so that management can make informed decisions.

This is a remote role that will need to overlap with the East Coast, USA. You can be based anywhere in the world where we can employ staff, either directly or through an employer of record.

### About Crossref

We’re a nonprofit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

We envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society. We are working towards this vision of a ‘Research Nexus’ by demonstrating the value of richer and connected open metadata, incentivising people to meet best practices, while making it easier to do so. “We” means 20,000+ members from 160 countries, 160+ million records, and nearly 2 billion monthly metadata queries from thousands of tools across the research ecosystem. We want to be a sustainable source of complete, open, and global scholarly metadata and relationships.

Take a look at our [strategic agenda](/strategy/) to see the planned work that aims to achieve the vision. The [sustainability](/operations-and-sustainability/) area aims to make transparent all the processes and procedures we follow to run the operation long-term, including our financials and our ongoing commitment to the Principles of Open Scholarly Infrastructure [(POSI)](https://openscholarlyinfrastructure.org/). The [governance](/board-and-governance/) area describes our board and its role in community
oversight.

It also takes a strong team – because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it. We are a distributed group of 46 dedicated [people](/people/) who like to play quizzes, talk about celery (sometimes cucumber), measure coffee intake, and create 100s of custom slack emojis. We enthusiastically support the Oxford comma but waver between use of American or British English. Occasionally we do some work to improve knowledge sharing worldwide—
which we take a bit more seriously than ourselves. We do this through fair policies and working practices, a balanced approach to resourcing, and accountability to each other.

We can offer the successful candidate a challenging and fun environment to work in. Together we are dedicated to our global mission and we are constantly adapting to ensure we get there. Take a look at our [organisation chart](/people/org-chart/) and view our Annual Reports and financial information [here](/operations-and-sustainability/financials/).

### Thinking of applying?

We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

Application deadline was **October 21st, 2024**

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

### Thanks for your interest in joining Crossref. We are excited to hear from you!