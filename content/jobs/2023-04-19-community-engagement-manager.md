+++
title = "Community Engagement Manager (editorial)"
date = 2023-04-19
draft = false
image = "/images/banner-images/hot-air-balloon.jpg"
author = "Michelle Cancel"
caption = "Photo by Pesce Huang on Unsplash"
weight = 25
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed May 12, 2023. {{% /divwrap %}}

Do you want to help improve research communications in all corners of the globe? Come and join the world of nonprofit open infrastructure and be part of improving the creation and sharing of knowledge as our new **Community Engagement Manager** for the editorial community.

**Location**: Remote and global (with regular working in European timezones)    
**Salary**: Approx. EUR 58,000-70,000 or local equivalent, depending on experience. Note this is a general guide (as there is no universal currency) and local benchmarking will take place before final offer.     
**Reports to**: Head of Community Engagement and Communications. See [org chart](https://www.crossref.org/people/) and [team](https://crossref.org/people/org-chart).   
**Application timeline**: Advertise in April, interviews in May, and offer by end of May/start of June. 

The organisations that make up the Crossref community are involved in documenting the process of scholarship and the progress of knowledge. We provide infrastructure to curate, share, and preserve metadata, which is information that underpins and describes all research activities (such as funding, authorship, dissemination, attention, etc., and relationships between these activities). 

Increasingly, the community is concerned with matters of research integrity, and Crossref plays a central role with several tools and services to help record and track corrections and retractions and identify issues such as plagiarism, along with other trust indicators that metadata can provide.

As the scholarly communications landscape is dynamically changing, the Community Engagement Manager’s key responsibility is to engage with the global community of scholarly editors, working with publishers and partners like EASE and CSE. You will help the community leverage metadata to assert the integrity of the scholarly record. This is a new role, responding to a growing need identified through recent consultations with the community on these topics. 

### Key responsibilities

* Create opportunities to engage with editors regarding the integrity of the scholarly record, and develop a programme to sustain collaboration and devise activities and resources that help equip, mobilise and empower editors to collect and leverage rich metadata
* Build relationships with scientific editorial groups such as EASE (Europe), ACSE (Asia), and CSE (US) and support their communities, bringing insights to shape our development and priorities
* Identifying and creating opportunities to listen to the sentiment and feedback of the Crossref’s community, sharing community insights with colleagues
* Representing Crossref and using the role to bring people together, attending and speaking at relevant industry events, online and in-person
* Building and managing relationships with community partners and collaborators worldwide to help progress Crossref’s mission
* Creating content, such as writing articles and blogs, creating slides and diagrams
* Contribute to other outreach and communications activities 

The role is based within the Community Engagement and Communications team. We work collaboratively across a variety of projects and programmes. We adopt an approachable, community-appropriate tone and style in our communications. We’re looking to re-engage with our community through face-to-face opportunities as well as online, so the post-holder will have their share of travel (accordingly with [our latest thinking on travel and sustainability](https://www.crossref.org/blog/rethinking-staff-travel-meetings-and-events/)). 

Our primary aim is to engage colleagues from the member organisations and other stakeholders to be actively involved in documenting the scholarly progress and making it transparent. This contributes to co-creating a robust [research nexus](https://www.crossref.org/blog/seeing-your-place-in-the-research-nexus/). As part of the wider Outreach department at Crossref, we seek to encourage wider adoption and development of best practices in scholarly publishing and communication with regard to metadata and the permanence of scholarly record. [Colleagues across the organisation](https://www.crossref.org/people/) are helpful, easy-going and supportive, so if you’re open-minded and ready to work as part of the team and across different teams, you will fit right in. Watch the [recording of our recent Annual Meeting](https://www.crossref.org/crossref-annual-meeting/archive/) to learn more about the current conversations in our community and explore our blog for a series of articles concerning the [integrity of the scholarly record](https://www.crossref.org/blog/isr-part-one-what-is-our-role-in-preserving-the-integrity-of-the-scholarly-record/).  

### About you

As scientific community engagement is an emerging profession, practical experience in this area is more important to us than traditional qualifications. It’s best if you can demonstrate that you have most of these characteristics:

* Collaborative attitude 
* Demonstrable experience working within the scholarly editorial community
* Awareness of current trends in academic culture and scholarly communications
* Curiosity to explore complex concepts and to learn new skills and perspectives
* Ability to translate complex ideas into accessible narratives in English
* Experience of community building and management and/or of planning, executing and evaluating participatory initiatives
* Demonstrable skills in group facilitation and stakeholder relationships management
* Track record of programme development and improvement, working to budget
* Confidence in public speaking in-person and online, including delivery of webinars/workshops
* Event and project management experience
* Tried and tested strategies for ensuring that your engagement programs are equitable, diverse and inclusive
* It would be a plus if you also have any of the following:
    * Understanding of matters concerning metadata
    * Experience of working in global or multicultural settings
    * Ability to communicate in languages other than English  


### About Crossref

Crossref is a non-profit membership organisation that exists to make scholarly communications better. We make research objects easy to find, cite, link, assess, and reuse. We’re passionate about providing open foundational infrastructure for the scholarly communications ecosystem - and we’re continuously evolving our tools and services in response to emerging needs. 

Crossref is, at its core, a community organisation with 18,000 members across 150 countries. We work with the community to prototype and co-create solutions for broad benefit, and we’re committed to lowering barriers to global participation in the research enterprise. We’re funded by members and subscribers, and we forge deep collaborations with many like-minded partners, especially those who are equally as committed to the [POSI](https://openscholarlyinfrastructure.org/) Principles. 

#### What it’s like working at Crossref

We’re about 45 staff and ‘remote-first’. We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organisation. Check out [the organisation chart](https://www.crossref.org/people/org-chart/).

We actively support ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

### Thinking of applying?

We encourage applications from excellent candidates wherever you might be in the world, especially from people with backgrounds historically under-represented in research and scholarly communications. Our team is fully remote and distributed across time zones and continents. This role will require regular work in European time zones. Our main working language is English, but there are many opportunities in this job to use other tongues if you’re able. If anything here is unclear, please contact Kora Korzec, the hiring manager, on kora@crossref.org.

[Please apply via this form](https://100hires.com/j/enraese) which allows us to sort your application materials into neat folders for a faster review. One of the best ways of offering evidence of your suitability within the cover letter is with an example of a relevant project you’re particularly proud of – we would particularly welcome mentions of your work with scholarly editors. If possible, we’d also love to see an example of content you’ve created – a link to a recording of your talk, blog post, infographic, or something else. There is space to share documents and links within the application form.

Lastly, if you don’t meet the majority of the criteria we listed here, but are confident you’d be natural in delivering the key responsibilities of the role, we encourage your interest and would still like to hear what strengths you would bring.

We aim to start reviewing applications on May 12th. Please strive to send us your documents by then.

The role will report to Kora Korzec, Head of Community Engagement and Communications at Crossref, and she will review all applications along with Michelle Cancel, our HR Manager, and Ginny Hendricks, Director of Member & Community Outreach. 

We intend to invite selected candidates to a brief initial call to discuss the role as soon as possible following an initial review. Following those, shortlisted candidates will be invited to an interview taking place in May. The interview will include some exercises you’ll have a chance to prepare for. All interviews will be held remotely on Zoom. 

#### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

## Thanks for your interest in joining Crossref. We are excited to hear from you!