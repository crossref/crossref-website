+++
title = "Director of Programs & Services"
date = 2024-09-07
draft = false
author = "Michelle Cancel"
image = "/images/banner-images/viaduct.jpg"
summary = "Develop services and initiatives that help progress open science worldwide."
rank = 1
weight = 2
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed October 4, 2024. {{% /divwrap %}}

Do you want to drive the development of services and initiatives that help progress open science worldwide? Come and join the world of open scholarly infrastructure and metadata as our new **Director of Programs & Services**.

* **Location**: Remote and global
* **Type**: Full-time
* **Remuneration**: 135K USD or local equivalent. Note this is a general guide (as there is no universal currency) and local currency analysis will take place before the final offer.
* **Reports to**: Chief Program Officer, [Ginny Hendricks](/people/ginny-hendricks/)
* **Timeline**: Advertise and recruit September-October, offer in November

### About the role

We have created a new role for a Director of Programs and Services to be a key member of the senior management team at Crossref to help deliver on our [strategic agenda](/strategy/). The role is responsible for planning and driving the success of the tools and services that make up our primary programs such as metadata discovery, metadata sources, community integrations, and ensuring the integrity of the scholarly record—all programs that help us help our members meet the vision of an open and connected [research nexus](/documentation/research-nexus/) and enable a better open research ecosystem.

The successful candidate will be critical to Crossref’s transformation away from a more traditional software-focused structure and culture, and towards a more appropriate non-profit and community-guided structure, leading a global and collaborative approach to prioritizing and meeting our mission. While the work is similar to traditional product work (and we assume some candidates may have a
background in that area) this role is newly envisioned in the context of the vast scale and growth in our membership and users and the need to bring them more closely into the process. Whatever your background, if the CSCCE Community Participation Model1 looks like something you have done or could do, we want to hear from you. Success in this role requires an intrinsically open approach, experience in enabling co-creation at scale, the ability to listen to and act on evolving member needs,
and the practical capabilities to bring cross-organisation teams together to plan and establish clear processes to ensure a clear and measurable path to implementation and delivery of real-world impact for our community members.

<img src="https://images.app.goo.gl/97WVxvEzGPfwUnWY7" alt="CSCCE Community Participation Model">

{{% imagewrap center %}}
{{< figure src="/images/blog/2024/CSCCE.png" alt="Description of image" caption="CSCCE Participation Model" alt="Image of the CSCCE Participation Model" width="600px" class="img-responsive" >}}
{{% /imagewrap %}}

Center for Scientific Collaboration and Community Engagement. (2020) [The CSCCE Community Participation Model – A framework for member engagement and information flow in STEM communities](https://zenodo.org/records/3997802). Woodley and Pratt.

### Key responsibilities

#### Leadership and Management:
* Bring structure to our cross-organisational planning, working with the Operations group and members of the senior management team to translate our vision into concrete projects and deliverables.
* Develop a new program-wide approach and manage a team of seven to extend beyond
traditional product management, promote the value and role of program design and project management.
* Be responsible for what Crossref can commit to; estimate, resource, and organize work across existing and new programs and projects and ensure everyone who needs to be is involved.
* Help enhance our culture, which is based on remote working and open communication.
* Create an open decision-making culture for program development at Crossref.
* Work with and establish strong links with all teams and all levels of the organisation.

#### Program Strategy:
* Develop and deliver on our new vision for evolving product management, designing program and project approaches from design, management, delivery, and measurement.
* Develop and guide the team to optimally manage the key programs, which include:
   - The integrity of the scholarly record (services and tools like Crossmark and   Retraction)
   - Metadata retrieval and discovery (REST API and developing our Search tool)
   - Metadata sources (member metadata, improving registration forms, as well as
   incorporating new or partner data sources)
   - Metadata development (activities like input and output schema evolution,
   implementing mapping and matching projects such as for funders and affiliations)
   - Modern operations (membership automation, infrastructure optimization, cloud
   migration, as well as fee and resourcing projects)
   - Integrations and interfaces (participation reports, main admin interface, as well as integrations with key platform partners like Open Journal Systems)
* Centre metadata and the community in everything we do. Oversee and support the metadata development process from consultation and design through to delivery and developing best practices.
* In support of the [strategic agenda](/strategy/), conduct program design for any new programs, plan resources and milestones along the way, and determine and report on the expected outcomes.
* Contribute to the design and management of a new Research Nexus fund for community tools, data sources, and new initiatives.
* Work with the Operations group to ensure that all development work delivers value to our members.
* Build and maintain the [roadmap](https://roadmap.productboard.com/948fefa2-0ddf-4602-b05f-c4b050a2f9c4), ensuring all areas of the organisation are involved in prioritization and plans and actively share these with our community stakeholders.
* Be responsible for the team in scoping and planning new feature development, running pilots and beta test phases, and facilitating internal project teams and external Working Groups to deliver on time.

#### Community Focus:
* Work closely with the Community and Membership Directors to set development priorities based on user needs.
* Research community needs and engage people through Advisory Groups for each key area of our service as well as Working Groups for new initiatives.
* Develop introduction plans to roll out new features through open community consultation and co-creation.
* Be a visible part of Crossref in the community e.g. speaking at events, being directly accessible to members and setting expectations with the community, engaging on social media and the community forum, and blogging about our services and plans.
* Represent Crossref on others’ working groups and advising other community groups on
strategic initiatives.

### About you

We are looking for a proactive, communicative, analytical, and highly organized person to help take our product function to the next level of community co-creation and program management. The successful candidate will likely possess the following attributes and experience:

* Community-minded with a background in non-profit, social impact, open data, and/or open-source software.
* Driven by seeing real outcomes and impact for community members.
* Strong program design and implementation skills.
* Systems thinking and experience bringing large and dispersed groups together asynchronously.
* Willingness to adapt and be flexible based on new insights or data.
* A highly communicative and transparent way of working and sharing information.
* Curiosity and tendency to listen (you will never have all the answers).
* Strong written and public speaking skills.
* Experience engaging users and partners in product development processes.
* Experience with product methodologies and best practices in open-source software
development.
* Adept at planning and launching features and services in an open and transparent way.
* Tech-savvy, comfortable with API use, metadata formats, and databases.
* Analytical, highly organized, and process-focused.
* Demonstrated experience improving operational processes and systems.
* Love of data, keen to track usage and participation trends to inform decisions and measure success.
* Experience working globally across time zones and with diverse groups stakeholders and cultures.
* Experience managing budgets, external consultants, and oversight of project management.

### About Crossref and the team

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

We envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society. We are working towards this vision of a ‘Research Nexus’ by demonstrating the value of richer and connected open metadata, incentivising people to meet best practices, while making it easier to do so. “We” means 20,000+ members from 160 countries, 160+ million records, and nearly 2 billion monthly metadata queries from thousands of tools across the research ecosystem. We want to be a sustainable source of complete, open, and global scholarly metadata and relationships.

Take a look at our [strategic agenda](/strategy/) to see the planned work that aims to achieve the vision. The [sustainability](/operations-and-sustainability/) area aims to make transparent all the processes and procedures we follow to run the operation long-term, including our financials and our ongoing commitment to the Principles of Open Scholarly Infrastructure [(POSI)](https://openscholarlyinfrastructure.org/). The [governance](/board-and-governance/) area describes our board and its role in community
oversight.

It also takes a strong team – because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it. We are a distributed group of 46 dedicated [people](/people/) who like to play quizzes, talk about celery (sometimes cucumber), measure coffee intake, and create 100s of custom slack emojis. We enthusiastically support the Oxford comma but waver between use of American or British English. Occasionally we do some work to improve knowledge sharing worldwide—
which we take a bit more seriously than ourselves. We do this through fair policies and working practices, a balanced approach to resourcing, and accountability to each other.

We can offer the successful candidate a challenging and fun environment to work in. Together we are dedicated to our global mission and we are constantly adapting to ensure we get there. Take a look at our [organisation chart](/people/org-chart/) and view our Annual Reports and financial information [here](/operations-and-sustainability/financials/).

### How to apply

To apply, please submit a CV and cover letter, detailing how you fulfil the role description and personal specification to [Perrett Laver’s application page](https://candidates.perrettlaver.com/vacancies/4365/director_of_programs_and_services/) quoting reference 7556. The deadline for applications is Friday, October 4, 2024.

This is a remote position and the successful candidate can be based most anywhere as long as they are prepared to adapt their hours to European and East Coast US time zones. A moderate amount of travel is expected.

Anticipated salary for this role is approximately $135,000 USD, or equivalent amount paid in local currency. Crossref offers competitive compensation, a rich benefits package, flexible work arrangements, professional development opportunities, and a supportive work environment. As a non-profit organisation, we prioritize mission over profit.

The selection committee will together review all candidates’ applications and agree on a longlist for the role. Longlisted candidates will be invited to discuss the position with Perrett Laver in greater detail. The selection committee will subsequently meet to decide upon a final shortlist to be invited to the formal interview stage.

Protecting your personal data is of the utmost importance to Perrett Laver and we take this responsibility very seriously. Any information obtained by our trading divisions is held and processed in accordance with the relevant data protection legislation. The data you provide us with is securely stored on our computerized database and transferred to our clients for the purposes of presenting you as a candidate and/or considering your suitability for a role you have registered interest in.

Perrett Laver is a Data Controller and a Data Processor, and our legal basis for processing your personal data is ‘Legitimate Interests’. You have the right to object to us processing your data in this way. For more information about this, your rights, and our approach to Data Protection and Privacy, please see our [Privacy Statement](https://perrettlaver.com/privacy-statement/).

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

### Thanks for your interest in joining Crossref. We are excited to hear from you!

[def]: https://candidates.perrettlaver.com/vacancies/4365/director_of_programs_and_services/