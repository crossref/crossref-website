+++
title = "Technical Community Manager"
date = 2023-04-20
draft = false
image = "/images/banner-images/geometric-walls.jpg"
author = "Michelle Cancel"
caption = "Photo by Artem Lysenko on Pexels"
weight = 10
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed May 22nd, 2023. {{% /divwrap %}}

Do you want to help improve research communications in all corners of the globe? Come and join the world of nonprofit open infrastructure and be part of improving the creation and sharing of knowledge as our brand-new **Technical Community Manager**, working with our API users, service providers, and other metadata integrators. 

**Location**: Remote and global (with regular working in European timezones)    
**Salary**: Approx. EUR 60,000-72,000 or the local equivalent, depending on experience. Note this is a general guide (as there is no universal currency) and local benchmarking will take place before the final offer     
**Reports to**: Head of Community Engagement and Communications. See [team](https://www.crossref.org/people/) and [org chart](https://crossref.org/people/org-chart)    
**Application timeline**: Advertise in April, interviews in May, and offer by end of May/early June     

The organisations that make up the Crossref community are involved in documenting the process of scholarship and the progress of knowledge. We provide infrastructure to curate, share, and preserve metadata, which is information that underpins and describes all research activities (such as funding, authorship, dissemination, attention, etc., and relationships between these activities). This enables a rich network of relationships and data underpinning scholarship that improves the discoverability of individual works and supports efforts to increase the openness and integrity of research. 

As the scholarly communications landscape is dynamically changing, the Technical Community Manager’s key responsibility is to engage with tools and organisations that integrate Crossref metadata solutions into their operations – whether these are service providers enabling others to register and maintain their metadata, or organisations making use of our API and metadata – within their own processes, or developing tools and resources that embed them.

This is a new role, taking advantage of the progress we’ve seen towards the [research nexus](https://www.crossref.org/documentation/research-nexus/) vision, supporting the multiple integrations that rely on Crossref, and maximising the awareness of current and future metadata and API developments. 


### Key responsibilities



* Proactively build and maintain relationships with existing and new community integrators. Research and define their needs, involve them with changes to our services, and bring insights to colleagues to help prioritise service improvements and new developments. 
* Design and implement activities to engage all metadata users and grow the usage of our [API](https://api.crossref.org), ensuring the community is aware of the possibilities of the ‘research nexus’ and can integrate with us in robust and sustainable ways.
    * Create opportunities for testing, co-development, and mutual learning (e.g. through sprints or working groups), working closely with R&D and Product teams.
    * Provide consultation to all metadata users and facilitate advanced support such as for query efficiency at enterprise levels
    * Improve and manage documentation and create demonstrations and materials to engage existing and potential metadata users.
    * Redesign and grow the [Plus](https://www.crossref.org/services/metadata-retrieval/metadata-plus/) program to align with improvements in our (cloud-based) infrastructure, automating the onboarding experience and managing terms of use and service level agreements. 
* Develop our [Service Provider program](https://www.crossref.org/community/service-providers/) to include all the third-party tools and plugins that our community uses to participate in the Crossref infrastructure.
    * Work with Service Providers such as grant/manuscript/repository platforms and systems to help them understand Crossref’s services, policies, and plans.
    * Explore accreditation options and ensure that information about Service Providers’ offerings is transparent so that the community can assess the different options available to them.
* Represent Crossref and use the role to bring people together, attending and speaking at relevant community meetings and participating in working groups, hosting workshops and sprints, online and in-person
* Build and manage relationships with community partners and collaborators worldwide to help progress Crossref’s mission, especially with other adopters of the Principles of Open Scholarly Infrastructure ([POSI](https://openscholarlyinfrastructure.org/)) 
* Create content and materials such as writing articles and blogs, managing website content and documentation, creating slides, videos, demos, and diagrams
* Contribute to other outreach and communications activities 

The role is based within the Community Engagement and Communications team. We work collaboratively across a variety of projects and programmes. We adopt an approachable, community-appropriate tone and style in our communications. We’re looking to re-engage with our community through face-to-face opportunities as well as online, so the post-holder will have their share of travel (accordingly with [our latest thinking on travel and sustainability](https://www.crossref.org/blog/rethinking-staff-travel-meetings-and-events/)). 

Our primary aim is to engage colleagues from the member organisations and other stakeholders to be actively involved in capturing documentation of the scholarly progress and making it transparent. This contributes to co-creating a robust [research nexus](https://www.crossref.org/blog/seeing-your-place-in-the-research-nexus/). As part of the wider Outreach department at Crossref, we seek to encourage wider adoption and development of best practices in scholarly publishing and communication with regard to metadata and the permanence of the scholarly record. [Colleagues across the organisation](https://www.crossref.org/people/) are helpful, easy-going and supportive, so if you’re open-minded and ready to work as part of the team and across different teams, you will fit right in. Watch the [recording of our recent Annual Meeting](https://www.crossref.org/crossref-live-annual/) to learn more about the current conversations in our community. 


### About you

As scientific community engagement is an emerging profession, practical experience in this area is more important to us than traditional qualifications. Also, as this is a new and varied role, the list of requirements is long, but we don’t expect that candidates will meet all of those. It’s best if you can demonstrate that you have most of these characteristics:

* Collaborative attitude 
* Ability to translate complex ideas into accessible narratives in English 
* Ability to engage technical audiences on topics related to research metadata including discussing best practices
* Experience working with Git, RESTful APIs, JSON metadata, and API interfaces such as Postman 
* Some basic programming skills, ability to write short snippets of code for interacting with APIs and for data manipulation. Familiarity with interactive development environments like JupyterLab and Google Colab.
* Ability to demonstrate APIs, monitor and interpret usage statistics, and advise on querying in a compelling manner
* Demonstrable skills in group facilitation and building strong relationships with communities or customers 
* Track record of programme development and improvement, working to budget and timelines
* Confidence in public speaking in-person and online, including delivery of webinars/workshops
* Understanding and commitment to the highest standards of equity, diversity and inclusiveness

It would be a plus if you also have _any_ of the following:

* Understanding of research communications operations such as publishing/repository workflows
* Data visualisation skills
* Technical sales or contract management experience
* Experience working in global or multicultural settings
* Ability to communicate in languages other than English  


### About Crossref 

Crossref is a non-profit membership organisation that exists to make scholarly communications better. We make research objects easy to find, cite, link, assess, and reuse. We’re passionate about providing open foundational infrastructure for the scholarly communications ecosystem - and we’re continuously evolving our tools and services in response to emerging needs. 

Crossref is, at its core, a community organisation with 18,000 members across 150 countries. We work with the community to prototype and co-create solutions for broad benefit, and we’re committed to lowering barriers to global participation in the research enterprise. We’re funded by members and subscribers, and we forge deep collaborations with many like-minded partners, especially those who are equally as committed to the POSI Principles. 


#### What it’s like working at Crossref

We’re about 45 staff and now ‘remote-first’ although we have optional offices in Oxford, UK, and Boston, USA. We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organisation. Check out [the organisation chart](https://www.crossref.org/people/org-chart/).

We actively support ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.


### Thinking of applying?

We encourage applications from excellent candidates wherever you might be in the world, especially from people with backgrounds historically under-represented in research and scholarly communications. Our team is fully remote and distributed across time zones and continents. This role will require regular work in European time zones. Our main working language is English, but there are many opportunities in this job to use other tongues if you’re able. If anything here is unclear, please contact Kora Korzec, the hiring manager, at kora@crossref.org.

[Please apply via this form](https://100hires.com/j/ek6dy9f), which allows us to sort your application materials into neat folders for a faster review. We have provided space in the form for you to describe an example of how you use an API. In your cover letter, please feel free to include some examples of relevant projects that you're proud of, perhaps content you've created or talks you've given. We would particularly welcome mentions of collaborative work where you led a group or a community through implementing or improving technical solutions. This is a great way for you to show evidence of your suitability for this role.

Note that if you don’t meet the majority of the criteria we listed here, but are confident you’d be natural in delivering the key responsibilities of the role, we encourage your interest and would still like to hear what strengths you would bring.

We aim to start reviewing applications on May 22nd. Please strive to send us your documents by then.

The role will report to Kora Korzec, Head of Community Engagement and Communications at Crossref, and she will review all applications along with Michelle Cancel, our HR Manager, and Ginny Hendricks, Director of Member & Community Outreach. 

We intend to invite selected candidates to a brief initial call to discuss the role as soon as possible following an initial review. Following those, shortlisted candidates will be invited to an interview taking place in late April. The interview will include some exercises you’ll have a chance to prepare for. All interviews will be held remotely on Zoom. 


#### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.


## Thanks for your interest in joining Crossref. We are excited to hear from you!
