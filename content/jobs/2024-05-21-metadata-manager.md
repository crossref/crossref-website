+++
title = "Metadata Manager"
date = 2024-05-21
draft = false
image = "/images/banner-images/fish.jpg"
author = "Michelle Cancel"
rank = 1
caption = "Photo by Isaac Mijangos on Pexels"
weight = 3
parent = "Jobs"
+++


{{% divwrap blue-highlight %}} 

### APPLICATIONS ARE CLOSED EFFECTIVE JUNE 11, 2024

{{% /divwrap %}}
Do you want to help make research communications better in all corners of the globe? Come and join the world of nonprofit open infrastructure as our new **Metadata Manager**.

* **Location**: Remote and global (with 3-hour overlap with UTC 15:00 -18:00)
* **Type**: Full-time
* **Remuneration**: Approx. 70-80K USD or local equivalent, depending on experience. Note this is a general guide (as there is no universal currency) and local currency analysis will take place before the final offer.
* **Reports to**: Head of Metadata, [Patricia Feeney](/people/patricia-feeney/); this role is full time at Crossref and works closely with a [cross-organizational team running ROR](https://ror.org/about/#team), with colleagues based at California Digital Library and DataCite.
* **Timeline**: Advertise and recruit in May-June/hire in July

### About the role

We're looking for a new full-time Metadata Manager. This role will be based at Crossref and responsibilities will be split between ROR (75%) and Crossref (25%). This role will be responsible for day-to-day metadata curation activities for the ROR registry, including coordinating ongoing registry updates, working with ROR’s curation advisors and other community stakeholders, and maintaining ROR’s curation policies and practices. This role will also collaborate with Crossref’s metadata team in developing and strengthening Crossref metadata.

We are a geographically distributed, remote-first team with flexible working hours.

#### Key responsibilities

For ROR

Coordinate community-based curation processes:

* Triage incoming requests to prepare for community review and/or work with Crossref support colleagues to optimize triaging
* Oversee community review process to make sure requests are reviewed in a timely and accurate manner
* Optimize curation workflow as needed to improve experience for requesters and community curators
* Maintain guidance and documentation for community curators
* Schedule and facilitate regular curator meetings
* Onboard and offboard community curators
* Provide training and assign tasks to contract staff

Coordinate ROR registry updates

* Maintain regular schedule of registry updates
* Identify which updates will be in a given release
* Prepare metadata for records being added or updated
* Work with curators handling metadata records
* Ensure metadata records pass validation/QA
* Work with curators handling metadata records
* Review and test release candidates
* Deploy changes to production via Github-based workflow
* Publish release notes and announce releases to users
* Publish data dump on Zenodo
* Gather data and generate reports on curation processes to track volume, turnaround times, types of requests, etc.

Metadata management and QA:

* Maintain documentation of metadata policies and inclusion criteria, as well as schema documentation
* Analyze current registry data to identify opportunities for metadata QA and future improvements
* Work with ROR’s development team on schema updates and metadata clean-up

Community engagement:

* Respond to support questions about data issues, curation policies, and release timelines
* Support strategic initiatives and integrations
* Provide updates at community calls and webinars
* Collect feedback from community to inform curation policies and process

For Crossref:

* Participate in community-led efforts to expand and refine Crossref metadata by assisting with working groups and other community interaction
* Help with input (XML) and output (JSON) metadata modeling and testing
* Help maintain documentation

### About you

We don't expect a successful candidate to tick all of these boxes right away! If you have any questions, please get in touch.

Qualities

* Comfortable collaborating with colleagues or stakeholders in the community
* Comfortable being part of a distributed team
* Self-motivated to succeed and take initiative and seek continuous improvement
* Familiarity with scholarly research infrastructures and the open science landscape

Skills

* Strong at written and verbal communication skills, able to communicate clearly,simply, and effectively
* Experience in metadata curation
* Experience in data analysis
* Working knowledge of a scripting language, such as Python
* Experience in workflow development and optimization
* Experience facilitating community groups/collaborations
* Experience with Github and Markdown
* Experience working with RESTful APIs and related web services/technologies
* Experience or familiarity with XML and JSON

### About the team

The Crossref team is distributed across the world. The ROR team is based in the USA.

We work fully remotely, but try to meet in person at least once a year. This is a full-time position, but working hours are flexible. The applicant should expect they will need to travel internationally to work with colleagues for about 5-10 days a year. If you have any questions we would be happy to discuss.

You can be based anywhere in the world where we can employ staff, either directly or through an employer of record.

### About Crossref

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

Crossref sits at the heart of the global exchange of research information, and our job is to make it possible—and easier—to find, cite, link, assess, and reuse research, from journals and books, to preprints, data, and grants. Through partnerships and collaborations we engage with members in 148 countries (and counting) and it’s very important to us to nurture that community.

We’re about [46 staff](/people/) and remote-first. This means that we support our teams working asynchronously and to flexible hours. Some international travel will likely be appropriate, for example to in-person meetings with colleagues and members, but in line with our [travel policy](/blog/rethinking-staff-travel-meetings-and-events/). We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organisation. Check out the [organisation chart](/people/org-chart/).

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

#### WE ARE NO LONGER ACCEPTING APPLICATIONS. 

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

### Thanks for your interest in joining Crossref. We are excited to hear from you!
