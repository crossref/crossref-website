+++
title = "Product Manager"
date = 2019-12-20
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Bryan Vickery"
weight = 3
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position are closed {{% /divwrap %}}

Crossref makes scholarly content easy to find, cite, link, assess and reuse by collecting and sharing metadata from our 11000+ member organisations worldwide. Our services and value now extend well beyond persistent identifiers and reference linking, and our connected open infrastructure benefits our membership as well as all those involved in scholarly research. This is a time of considerable change at Crossref and you can help shape our future.

We are a small team with a big impact, and we’re looking for a creative, technically-oriented Product Manager to join us in improving scholarly communications.

### Key responsibilities
- Manage all aspects of the product life cycle for one or more key services or products within the Crossref ecosystem
- Articulate and influence product strategy focusing on business objectives and user experience
- Integrate usability studies, user research, system analysis and community feedback into product requirements
- Define objectives, methods and metrics for product adoption to track success
- Co-ordinate and direct working groups made up of external stakeholders
- Promote product adoption externally as well as liaise with and maintain relations with key market stakeholders & strategic partners
- Evangelize products and value stories internally to rally people and resources behind ideas and ambitions critical to success

### About you
- You can think in terms of the big picture, but deliver on the details
- You have a nose for great products and advocate for new features using qualitative and quantitative reasoning
- You will, ideally, have an understanding and experience of complex workflow systems
- You do whatever it takes to make your product and team successful, whether that means writing a QA plan or hunting down the root cause of a user’s frustration
- You can turn incomplete, conflicting, or ambiguous inputs into solid action plans
- You communicate with empathy and exceptional precision
- You’re comfortable working with developers. You are technical enough to ask engineers good questions about architecture and product decisions alike
- You obsess about continuous product improvement
- You are self-driven with a collaborative and can-do attitude and enjoy working with a small team across multiple time zones
- You maintain order in a dynamic environment, independently managing multiple priorities
- You champion agile best practices
- You are adept at communicating technical systems to non-technical audiences through writing, small group settings, and conference talks
- 5+ years of product management experience with internet technologies and/or equivalent experience in the research publishing arena


You can find more about our plans by viewing our latest annual report - a Fact File for 2019. Crossref Annual Report & Fact File 2018-19, [https://doi.org/10.13003/y8ygwm5](https://doi.org/10.13003/y8ygwm5).

This position is full time and, as for all Crossref employees, location is flexible - you can work remotely or be based out of either of our Crossref offices (Lynnfield, MA and Oxford, UK), with a minimum 3-hour overlap with US Eastern time zone. We provide a competitive benefits package.

To apply, please send your cover letter, resume, and at least one sample of an effective product specification or epic/story development to Lindsay Russell at [jobs@crossref.org](mailto:jobs@crossref.org).
