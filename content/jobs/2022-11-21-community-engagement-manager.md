+++
title = "Community Engagement Manager"
date = 2022-11-21
draft = false
image = "/images/banner-images/hr-job-wildflower-meadow.jpg"
author = "Michelle Cancel"
caption = "“Flower Garden”, Euskirchen, Germany, by Mina-Marie Michell via Pexels"
weight = 1
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed 2022-12-11. {{% /divwrap %}}

## Community Engagement Manager

Come and work with us as a **Community Engagement Manager**. It’ll be fun!

**Job title:** Community Engagement Manager  
**Location:** Remote and global (with regular working in European and some Asia Pacific time zones)  
**Remuneration:** €58,000 – €70,000, or local equivalent, depending on experience  
**Reports to:** Head of Community Engagement and Communications  
**Application timeline:** Advertise and recruitment in November/December, Start date Jan/Feb 2023  

Crossref is a non-profit membership organization that exists to make scholarly communications better. We make research objects easy to find, cite, link, assess, and reuse. We’re passionate about providing open foundational infrastructure for the scholarly communications ecosystem - and we’re continuously evolving our tools and services in response to emerging needs.

If you’ve got a passion for equity and diversity and would like to use your engagement and organizational skills to help strengthen our community and to help shed light on the inner workings behind the progress of science, this position may be for you. Read on and apply by December 11. 	

Crossref is at its core a community organization with 17,000 members across 146 countries (and counting)! We’re committed to lowering barriers for global participation in the research enterprise, we’re funded by members and subscribers, and we engage regularly with them in multiple ways from webinars to working groups.

The organizations in our community are involved in documenting the progress of scholarship. We provide infrastructure to preserve and curate metadata – that is data underpinning and describing scholarly outputs and processes (such as authorship, funding, modifications etc., and links between these works). Our community develops and promotes standards and best practices for such documentation in keeping with the changing world of scholarly infrastructure and communication.

The Community Engagement Manager’s key responsibility is management of our well-established [Ambassadors program](/community/ambassadors/), which has a global reach and an important role in informing and engaging our membership. It’s been growing over the past five years and there’s an opportunity for the new manager to build upon its success, innovate and shape it for the future.

## Key responsibilities of the role:

- Strategically manage the Crossref Ambassadors program – you will build and weave together strong relationships with and between our volunteers, and develop activities and resources that help equip, mobilize and empower Ambassadors to engage their respective communities with Crossref’s messages and initiatives

- Designing and coordinating activities that facilitate access to and understanding of Crossref’s membership and services, such as translations, community platforms, events, and other (as appropriate), in partnership with the other Community Engagement Manager, the Membership Team and other community partners, including the support for the nascent Publishers Learning and Community Exchange (PLACE) platform

- Identifying and creating opportunities to listen to the sentiment and feedback of the Crossref’s community, sharing community insights with colleagues

- Representing Crossref and using the role to bring people together, attending and speaking at relevant industry events, online and in-person

- Building and managing relationships with community partners and collaborators worldwide to help progress Crossref’s mission

- Creating content – such as writing articles and blogs, creating slides and diagrams

## Contribute to other outreach activities

The role is based within the Community Engagement and Communications team. We work collaboratively across a variety of projects and programmes. We adopt an approachable, community-appropriate tone and style in our communications. We’re looking to re-engage with our community through face-to-face opportunities as well as online, so the post-holder will have their share of travel (accordingly with [our latest thinking on travel and sustainability](/blog/rethinking-staff-travel-meetings-and-events/)).

Our primary aim is to engage colleagues from the member organizations and other stakeholders to be actively involved in capturing documentation of the scholarly progress and making it transparent. This contributes to co-creating a robust [research nexus](/blog/seeing-your-place-in-the-research-nexus/). As part of the wider Outreach department at Crossref, we seek to encourage adoption and development of best practices in scholarly publishing and communication with regards to metadata and permanence of scholarly record. [Colleagues across the organization](/people/) are helpful, easy-going and supportive, so if you’re open minded and ready to work as part of the team and across different teams, you will fit right in. Watch the [recording of our recent Annual Meeting](/crossref-live-annual/) to learn more about the current conversations in our community.


## About you

As scientific community engagement is an emerging profession, practical experience in this area is more important to us than traditional qualifications. It’s best if you can demonstrate that you have most of these characteristics:

- Collaborative attitude

- Curiosity to explore complex concepts and to learn new skills and perspectives

- Ability to translate complex ideas into accessible narratives in English

- 3+ years experience of community building and management and/or of planning, executing and evaluating participatory initiatives

- Demonstrable skills in group facilitation and stakeholder relationships management

- Track record of programme development and improvement, working to budgets

- Confidence in public speaking in-person and online, including delivery of webinars/workshops

- Event and project management experience

- Tried and tested strategies for ensuring that your programs are equitable, diverse and inclusive

- Awareness of current trends in academic culture and scholarly communications

It would be a plus if you also have any of the following:

- Understanding of matters concerning metadata

- Experience or background in communications or campaign management

- Experience of working in a multicultural setting

- Experience of moderating an online discussion forum, blogging platform or similar

- Ability to communicate in a language other than English  


## What it’s like working at Crossref

We’re about 45 staff and now ‘remote-first’ although we have optional offices in Oxford, UK, and Boston, USA. We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organization. Check out the [organization chart](/people/org-chart/).

We are active supporters of ongoing professional development opportunities and promote self- learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

## Thinking of applying?

We encourage applications from excellent candidates wherever you might be in the world, especially from people with backgrounds historically under-represented in research and scholarly communications. Our team is fully remote and distributed across time zones and continents. This role will require regular working in European and Asia-Pacific Time zones. Our main working language is English, but there are many opportunities in this job to use other tongues if you’re able. If anything here is unclear, feel free to contact Kora Korzec, the hiring manager, on [kkorzec@crossref.org](mailto:kkorzec@crossref.org).

To apply, please send a CV and a cover letter to share how you meet the requirements of the role to [jobs@crossref.org](mailto:jobs@crossref.org). One of the best ways of offering evidence of your suitability is with an example of a relevant project you’re particularly proud of, whether from professional, voluntary or personal experience. If possible, we’d also love to see an example of content you’ve created – a link to a recording of your talk, blog post, infographic, or something else. As it’s essential for the role that you have access to reliable high speed internet connection, please indicate clearly in your letter whether that is the case.

Lastly, if you don’t meet the criteria we listed here, but are confident you’d be natural in delivering the key responsibilities of the role, please explain what strengths you would be bringing to this job.

We aim to start reviewing applications on December 12. Please strive to send your documents to [jobs@crossref.org](mailto:jobs@crossref.org) by then.

The role will report to Kora Korzec, Head of Community Engagement and Communications at Crossref, and she will review all applications along with Michelle Cancel, our HR Manager, and Ginny Hendricks, Director of Member & Community Outreach.

We intend to invite selected candidates to a brief first interview to talk about the role as soon as possible following review. Following those, shortlisted candidates will be invited to an interview taking place in early to mid-January. The interview will include some exercises you’ll have a chance to prepare for. All interviews will be held remotely.


## Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

Thanks for your interest in joining Crossref. We are excited to hear from you!
