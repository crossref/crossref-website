+++
title = "Technical Support Specialist"
date = 2024-12-05
draft = false
image = "/images/banner-images/fish.jpg"
author = "Michelle Cancel"
caption = "Photo by Isaac Mijangos on Pexels"
weight = 15
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position will be closed on December 20, 2024. {{% /divwrap %}}

Do you want to work directly with Crossref members and metadata users to help progress open science worldwide? Come and join the world of open scholarly infrastructure and metadata as our next **Technical Support Specialist**.

 
* **Location**: Remote and global with availability from 13:00 to 15:00 UTC Monday through Friday
* **Type**: Full-time
* **Remuneration**: 45-50K USD or local equivalent. Note this is a general guide (as there is no universal currency) and local currency analysis will take place before the final offer.
* **Reports to**: Head of Participation and Support, [Isaac Farley](/people/isaac-farley/)
* **Timeline**: Advertise and recruit in December/offer in January
 

### About the role


Reporting to our Head of Participation and Support, the full time Technical Support Specialist is an important role in our Membership team.


We’re looking for a Technical Support Specialist to provide first-line help to our international community of publishers, librarians, funders, researchers, and developers on a range of services that help them deposit metadata to help them find, link, cite, and assess scholarly content. You’ll be working closely with nine other technical and membership support colleagues to provide support and guidance for people with a wide range of technical experience. The strongest candidates will not necessarily be from a technical background, but they’ll have interest and initiative to grow their technical skills while communicating the complexity of our products and services in straightforward and easy-to-understand terms. You’ll help our community both create and retrieve metadata records with tools ranging from simple user interfaces to APIs and integrations.


Crossref is a distributed team serving members and users around the world. We are seeking candidates to [foster a strong team](/strategy/#bolster-the-team). We work a flexible schedule; for training and synchronous problem-solving, we also ask that candidates have availability between 13:00 and 15:00 UTC.


#### Key responsibilities


* Replying to and solving community queries using the Zendesk support system.
* Using our various tools and APIs plus knowledge of our XML schema to find the answers to these queries, or pointing users to support materials that will help them.
* Liaising with colleagues on particularly tricky tickets, escalating as necessary.
* Working efficiently but also kindly and with empathy with our very diverse, global community.


### About you


We are looking for a proactive candidate with a unique blend of customer service skills, analytical trouble-shooting skills, and a passion to help others. You’ll have an interest in data and technology and will be a quick learner of new technologies. You’ll be able to build relationships with our community members and serve their very diverse needs - from assisting those with basic queries to really digging into some knotty technical queries. Because of this, you’ll also be able to distill those complex and technically challenging queries into easy-to-follow guidance.


You’ll need: 


* The ability to clearly communicate complex technical information to technical and non-technical users, using open questions to get to the bottom of things when queries don’t seem to make sense
* Quick learner of new technologies; can rapidly pick up new processes and systems; and, have interest and initiative to grow your own technical skills
* Extremely organized and can bring order to chaos, independently manage multiple priorities
* Ability to balance a very diverse role, wearing many different hats and providing a wide range of support
* Proactive in asking questions and making suggestions for improvements
* Process-driven but able to cope with occasional ambiguity and lack of clarity - open to feedback and adaptable when things change quickly
* A truly global perspective - we have over 20,000 member organizations from 160 countries across numerous time zones


Nice to have:


* Experience helping customers and solving problems in creative and unique ways
* Experience with or interest in XML, metadata, and Crossref as well as scholarly research and information science
* Experience with Zendesk and Gitlab or similar support and issue management software
* Experience with Discourse, similar community forums, and/or past community building


### About the team


Our technical support team, part of Crossref’s Outreach group, a distributed [team](/people/org-chart/) with colleagues across Africa, Asia, Europe, and the US, is critical to the ongoing success and day-to-day operations of the organization. We’re at the forefront of Crossref’s growth, building relationships with new communities in new markets in new ways. We’re aiming for a more [open approach](/strategy/#live-up-to-posi) to having conversations with people all around the world - including within our growing [community forum](https://community.crossref.org/), which the right candidate will help us expand.


This is a remote role. We ask that candidates have availability between 13:00 and 15:00 UTC. You can be based anywhere in the world where we can employ staff, either directly or through an employer of record. Our main working language is English, but there might be opportunities in this job to use other tongues if you are able.


### About Crossref


We’re a nonprofit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.


We envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society. We are working towards this vision of a ‘Research Nexus’ by demonstrating the value of richer and connected open metadata, incentivising people to meet best practices, while making it easier to do so. “We” means 20,000+ members from 160 countries, 160+ million records, and nearly 2 billion monthly metadata queries from thousands of tools across the research ecosystem. We want to be a sustainable source of complete, open, and global scholarly metadata and relationships.


Take a look at our [strategic agenda](/strategy/) to see the planned work that aims to achieve the vision. The [sustainability](/operations-and-sustainability/) area aims to make transparent all the processes and procedures we follow to run the operation long-term, including our financials and our ongoing commitment to the Principles of Open Scholarly Infrastructure [(POSI)](https://openscholarlyinfrastructure.org/). The [governance](/board-and-governance/) area describes our board and its role in community
oversight.


It also takes a strong team – because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it. We are a distributed group of 46 dedicated [people](/people/) who like to play quizzes, talk about celery (sometimes cucumber), measure coffee intake, and create 100s of custom slack emojis. We enthusiastically support the Oxford comma but waver between use of American or British English. Occasionally we do some work to improve knowledge sharing worldwide—
which we take a bit more seriously than ourselves. We do this through fair policies and working practices, a balanced approach to resourcing, and accountability to each other.


We can offer the successful candidate a challenging and fun environment to work in. Together we are dedicated to our global mission and we are constantly adapting to ensure we get there. Take a look at our [organisation chart](/people/org-chart/),  the latest [Annual Meeting recordings](/crossref-annual-meeting/), and our financial information [here](/operations-and-sustainability/financials/).


### Thinking of applying?


We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.


[Click here](https://100hires.com/j/73B3G49/apply) to apply!


Please strive to submit your application by **December 20, 2024.**


### Equal opportunities commitment


Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.


### Thanks for your interest in joining Crossref. We are excited to hear from you!

