+++
title = "Member Support Specialist"
author = "Amanda Bartell"
date = 2021-12-03
draft = false
image = "/images/banner-images/join-hello-sand.jpg"
weight = 1
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed 2022-March-01. {{% /divwrap %}}  


## Come and work with us as one of our **Member Support Specialists**. It’ll be fun!

​​Do you want to help make scholarly communications better? Come and join the world of open scholarly infrastructure and be part of improving the creation of and access to knowledge for all. It’s a serious job but we don’t take ourselves too seriously.

> This role serves our global membership in all countries but will be home-based in Indonesia or nearby		

## About Crossref

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

Crossref sits at the heart of the global exchange of research information, and our job is to make it possible—and easier—to find, cite, link, assess, and reuse research, from journals and books to preprints, data, and grants. Through partnerships and collaborations we engage with members in 146 countries (and counting) and it’s very important to us to make sure that everyone who wants to participate, can.

We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. Read more about [our strategy](/strategy).

We like to work hard but we have fun too!

## About the role

Member Support Specialist is a pivotal role in the Member Experience team. The "MemX team", for short, is part of Crossref’s Outreach team, which is at the forefront of Crossref’s growth, building relationships with new communities in new markets in new ways and enhancing Crossref's understanding of trends in scholarly communications. The Outreach team is currently 14 people dispersed across North America, East Africa, and Western Europe. Check out [our organisation chart](/people/org-chart).

The Member Support Specialist is a full time global role reporting to Amanda Bartell (Head of Member Experience). The work is a mix of involved consultations with applicants together with detailed systems and administrative work. You’ll need to have an understanding of the academic and scientific communications process, great attention to detail, the ability to ask probing questions of applicants, and a logical, systematic approach.

You'll be working primarily with new journal publishers to set them up as members, or to determine if they have other needs. Along with another Member Support Specialist (plus two contractors), you'll take these publishers through our application process, setting them up carefully in our CRM and other systems, paying extremely close attention to data quality. Once they’re members, you’ll continue to work with them closely - answering their questions via email, social media, and our community forum. You’ll help them take on new Crossref services, navigate platform migrations, and understand how to set up service providers to work with us on their behalf. It’s a very diverse role and is a great opportunity to get wide-ranging experience within Crossref and the global open scholarly infrastructure and communications community.

## Key responsibilities

* Work with new applicants to understand their internal structures and help them understand the various membership options available to them.
* Own and drive the administrative process for new applicants - ensuring we have all the information we need to help them get started and setting them up accurately in our central systems.
* Broker conversations between members, sponsors, platforms, and service providers to ensure the member is able to fulfill their aims while still meeting Crossref membership obligations.
* Manage queries from applicants and members via email, social media, our community forum and other channels.
* Ensure that the data in our CRM system is kept clean and up-to-date.
* Work closely with the support and finance teams to solve problems and ensure a smooth experience for members.

## Location

> Indonesia or surrounding region

We’re about [40 staff](/people) and now ‘remote-first’, although we have optional offices in Oxford (UK) and Boston (USA).

While we seek someone in Indonesia, they will need to be able to liaise with colleagues in Western Europe, East Africa, and North America. As Indonesia has emerged as the largest Open Access research-producing country in the world (which is reflected in Crossref’s huge membership there) there will be occasional opportunities to meet and engage with our members at conferences and other events in the region.


## About you

We’re looking for a motivated person who will take initiative, highlight things that seem inefficient, and be able to dig into things with our diverse membership to really get to the bottom of their needs.

You'll need to follow processes precisely and maintain accuracy while at the same time being comfortable with ambiguity; our community and environment is changing rapidly and we won’t always have a clear answer for everything. It keeps things interesting!

In addition...

* Able to balance a very busy role while still paying close attention to detail and keeping member experience at the forefront.
* Experience in helping customers and solving problems in creative and unique ways.
* Strong written and verbal communication skills with the ability to communicate clearly - able to use open questions to get to the bottom of things when members may not seem to make sense.
* A truly global perspective - we have 16,000 member organizations from 146 countries across numerous time zones.
* Be comfortable taking the initiative to lead conversations with people at all levels.
* Quick learner of new systems and processes and can rapidly pick up new techniques.
* Extremely organized and attentive to detail.
* Experience with Zendesk or similar support system is ideal, as is familiarity with CRM systems such as Sugar.
* Familiar with the scholarly publishing process, with a bonus being some knowledge of XML and metadata.


##  Thinking of applying?

Even if you don’t think you have all the specific experience, we’re looking for someone with the right approach who is keen to jump in and learn. Practical experience is more important to us than traditional qualifications.

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

To apply, please send your cover letter and resume to Lindsay Russell at [jobs@crossref.org](mailto:jobs@crossref.org).

##  Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all  employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law.  Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.
