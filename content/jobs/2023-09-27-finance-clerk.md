+++
title = "Finance Clerk"
date = 2023-09-27
draft = false
image = "/images/banner-images/fish.jpg"
author = "Michelle Cancel"
caption = "Photo by Isaac Mijangos on Pexels"
weight = 15
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed October, 2023. {{% /divwrap %}}

Do you want to help make research communications better in all corners of the globe? Come and join the world of nonprofit open infrastructure as our new **Finance Clerk**.

* **Location**: Remote and global (to overlap with colleagues in East Coast USA)
* **Type**: Full-time
* **Remuneration**: 40-44k USD
* **Reports to**: Supervising Accountant, [Maria Sullivan](/people/maria-sullivan/)
* **Timeline**: Advertise and recruit in September-October/hire in October-November

### About the role

Reporting to the Supervising Accountant, the Finance Clerk is a key role within the Finance team. The Finance Clerk is responsible for full cycle Accounts Payable, assuring financial transactions are proper recording within the accounting system. This position is the lead contact for vendor relations and our internal expense reporting application. The position will also be cross trained on cash application and membership support. 

A successful candidate in this role will be comfortable performing a broad range of duties,working remotely and independently,  and paying close attention to details. 

#### Key responsibilities

1. Responsible for the full cycle AP function for both the UK and USA entities including entering invoices into our accounting system, obtaining payment approvals and facilitating payment processing (checks/wires/direct debits/ACHs)
2. Responsible for managing corporate credit cards including reviewing and reconciling to statements monthly 
3. Responsible for the Expensify expense reporting platform; including maintaining knowledge of updates and enhancements and troubleshooting
4. Onboard/train new hires in Expensify and be the main point of contact for team members on system and reimbursement process inquiries
5. Responsible for the yearly 1099/1096 filing and vendor reporting
6. Act as backup for other Finance Team staff 
7. Responding to Zendesk inquires and assisting in collections as needed
8. Assist with cash application
9. Assist with monthly and quarterly financial reporting
10. Assist with audits
11. Other ad hoc financial and operational projects

### About you

The successful candidate will possess the following

* The ability to organize work, set priorities, follow-up and work proactively and accurately
* Excellent oral, written, data entry and communication skills
* Have experience in a multi-currency environment, to include USD & GBP
* Equally comfortable communicating with colleagues and our members to help problem solve 
* Experience working across timezones, with an understanding of the global nature of our work and community
* A self-starter and problem solver with an exceptional attention to detail
* Be comfortable using a variety of technology and software to communicate with a distributed staff

It would be a plus if you possess the following

* 2-5 years of accounting experience; basic understanding of US GAAP accounting practices 
* Solid experience using cloud-based/accounting applications (Intacct)
* Solid experience using Microsoft Excel and other tools (gmail/google docs, etc)
* Bachelor's Degree in Accounting/Business or equivalent business experience 

### About Crossref

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

Crossref sits at the heart of the global exchange of research information, and our job is to make it possible—and easier—to find, cite, link, assess, and reuse research, from journals and books, to preprints, data, and grants. Through partnerships and collaborations we engage with members in 148 countries (and counting) and it’s very important to us to nurture that community.

We’re about [45 staff](/people/) and remote-first. This means that we support our teams working asynchronously and to flexible hours. Some international travel will likely be appropriate, for example to in-person meetings with colleagues and members, but in line with our [travel policy](/blog/rethinking-staff-travel-meetings-and-events/). We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organisation. Check out the [organisation chart](/people/org-chart/).

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

### About the team

This role is in our Finance and Operations team which consists of 6 (soon to be 7) team members remotely sitting in the East Coast, USA. The Finance Clerk is a remote role that will need to overlap with East Coast, USA for at least 3-4 working hours. You can be based anywhere in the world where we can employ staff, either directly or through an employer of record. 

### Thinking of applying?

We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

Applications closed

### Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

### Thanks for your interest in joining Crossref. We are excited to hear from you!