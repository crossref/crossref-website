+++
title = "Schema versions"
date = "2025-02-10"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "schema-versions"]
identifier = "documentation/schema-library/schema-versions"
rank = 4
weight = 60404
aliases = [
    "/education/content-registration/metadata-deposit-schema/schema-versions",
    "/education/content-registration/metadata-deposit-schema/schema-versions/",
    "/documentation/content-registration/metadata-deposit-schema/schema-versions/",
    "/documentation/content-registration/metadata-deposit-schema/schema-versions"
]
+++

We support several versions of our metadata input and grants schema, as well as XML Schema Definition (XSD) schema for looking up and retrieving DOIs and metadata. A [quick reference](/documentation/content-registration/metadata-deposit-schema/xsd-schema-quick-reference) is available. The metadata input schema is used to deposit metadata for most record types, except Grants, which have their own schema.

We currently support versions `4.3.0` to `5.3.1` of our main metadata schema. If you are beginning to register your metadata with Crossref you should use the most recent version (currently [5.3.1](/documentation/schema-library/metadata-deposit-schema-5-3-1/)) to ensure you are able to take advantage of all metadata deposit options.
We also have a [resource-only deposit schema](/documentation/schema-library/resource-only-deposit-schema-4-4-2/) that may be used to add some pieces of metadata to an existing record.

### Metadata input schema versioning

All supported schema are available in our [Schema GitLab repository](https://gitlab.com/crossref/schema). Versions `4.3.0` - `4.8.1` of our schema are backwards-compatible with the exception of deposits for standards, which may only be deposited with version `4.3.6` and above.

We are now incrementing our input schema version numbers with each change for all updates after version `4.4.2`. Note that addtional schema versions are available via gitlab (`5.0` - `5.2` for example) but are not documented for use as subsequent versions were released at the same time (`5.3.1`).  

### Recommended metadata deposit schema  <a id='00001' href='#00001'><i class='fas fa-link'></i></a>

* [crossref5.3.1xsd](/documentation/schema-library/metadata-deposit-schema-5-3-1/): adds support for ROR identifiers in affiliation metadata

### Recommended grants deposit schema

* [grant_id0.2.0.xsd](/documentation/schema-library/grants-schema/): adds support for ROR identifiers to identify funders; adds new funding types (APC, BPC, infrastructure)

### Recommended resource-only deposit schema  <a id='00002' href='#00002'><i class='fas fa-link'></i></a>

* [doi_resources4.4.2.xsd](/documentation/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-4-2/)

### Also supported <a id='00003' href='#00003'><i class='fas fa-link'></i></a>
* [crossref4.8.1.xsd](/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-8-1/): changes include support for ISBN that begin with 979, changes to the regex for the `email_address` field, relaxed regex for `given_name` to allow numbers, and schema refactoring.
* [crossref4.4.2.xsd](/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-2/): adds support for pending publication, distributed usage logging (DUL), multiple dissertation authors, abstracts for all record types, support for JATS 1.2 abstracts, and adds acceptance_date element to journal article, book, book chapter, and conference papers
* [crossref4.4.1.xsd](/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-1/): adds support for [peer reviews](/documentation/schema-library/markup-guide-record-types/peer-reviews/)
* [crossref4.4.0.xsd](/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-0/): adds support for [posted content (includes preprints)](/documentation/schema-library/markup-guide-record-types/posted-content-includes-preprints/)
* crossref4.3.7.xsd: adds support for linked clinical trials, journal deposits without [ISSNs](/documentation/schema-library/markup-guide-metadata-segments/issn-isbn/)
* crossref4.3.6.xsd: expanded support for [standards](/documentation/schema-library/markup-guide-record-types/standards/)
* crossref4.3.5.xsd: supports [relationships between DOIs and other objects](/documentation/content-registration/structural-metadata/relationships/)
* crossref4.3.4.xsd: add `archive_location` option, change name element to `depositor_name`
* crossref4.3.3.xsd: modifications for [standards](/documentation/schema-library/markup-guide-record-types/standards/)
* crossref4.3.2.xsd: adds support for [license metadata](/documentation/content-registration/administrative-metadata/license-information/)
* crossref4.3.1.xsd: adds support for Crossmark, funding data, ORCID iDs
* crossref4.3.0.xsd: revisions to handling of [books](/documentation/schema-library/markup-guide-record-types/books-and-chapters/)
* [doi_resources4.3.6.xsd](/documentation/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-3-6/)
* doi_resources4.3.5.xsd
* doi_resources4.3.4.xsd
* doi_resources4.3.2.xsd
* doi_resources4.3.0.xsd
