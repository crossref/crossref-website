+++
title = "Required, recommended, and optional metadata"
date = "2021-05-13"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "required-recommended-elements"]
identifier = "documentation/schema-library/required-recommended-elements"
rank = 4
weight = 60401
aliases = [
    "/documentation/content-registration/content-type-markup-guide/required-recommended-elements",
    "/documentation/content-registration/content-type-markup-guide/required-recommended-elements/"
]
+++

Each record type we support has a unique set of requirements and recommended metadata. Contributor metadata is consistent across record types and, while not always required, has consistent recommendations if present.

_Required_ metadata must be included or your submission will be rejected. _Recommended_ metadata should be included to create a complete metadata record. _Optional_ metadata should be included if relevant, but will not be relevant for most records.

Find required, recommended and optional metadata for:

* [Contributor](#001)
* [Journal](#002)
* [Book](#003)
* [Conference Proceeding](#004)
* [Database](#005)
* [Dissertation](#006)
* [Posted content](#007)
* [Report / working paper](#008)
* [Standard](#009)
* [Peer Review](#010)

### Contributor metadata <a id="001"></a>

|Required|
|---|
|Surname|

|Recommended|
|---|
|given_name, suffix, affiliation, ORCID|

## Journal and article metadata <a id="002"></a>

|Required||
|---|---|
|**Journal** (journal_metadata)| full_title,  ISSN _or_ title-level DOI and URL|
|**Issue** (issue_metadata)|issue, publication_date (year)|
|**Article** (article_metadata)| titles, publication_date (year), doi_data|

Issue elements are only required if a DOI is being deposited at the issue level. Article elements are likewise only required for article DOI deposits.

|Recommended||
|---|---|
|**Journal** (journal_metadata)| abbrev_title, doi_data, coden, journal_issue, archive-locations (with one archive name), title-level DOI and URL|
|**Issue** (issue_metadata)|publication_date (month, day), journal_volume, [contributors](#001), issue, doi_data|
|**Article** (article_metadata)| contributors, ORCID, publication_date (day, month), pages (first_page, last_page), citation_list, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/),  [Crossmark](/documentation/crossmark/) metadata and [JATS-formatted abstracts](/documentation/content-registration/descriptive-metadata/abstracts/)|

|Optional|
|---|
|publisher_item, special_numbering, component_list|

## Book metadata <a id="003"></a>

|Required||
|---|---|
|**Series**| titles, ISSN, volume, publication_date (year), publisher (publisher_name)|
|**Set**| titles, volume|
|**Book**| titles, publication_date (year), publisher|
|**Chapter**| doi_data|

|Recommended||
|---|---|
|**Series**| doi_data,edition_number, ISBN, [contributors](#001), coden, series_number, citation_list|
|**Set**| [contributors](#001), ISBN, edition_number,  citation_list, doi_data|
|**Book**| [contributors](#001), ISBN, edition_number, doi_data, citation_list, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/), and [Crossmark](/documentation/crossmark/) metadata|
|**Chapter**|[contributors](#001), titles, pages, publication_date, citation_list, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/), and [Crossmark](/documentation/crossmark/) metadata|


|Optional|
|---|
|publisher_item, part_number, component_number,component_list|

## Conference Proceeding metadata <a id="004"></a>

|Required||
|---|---|
|**Series**| titles, ISSN|
|**Proceeding level**|proceedings_title, publisher, publication_date (year)|
|**Conference paper**|[contributors](#001), titles, doi_data|

|Recommended||
|---|---|
|**Series level**| doi_data, [contributors](#001), series_number, ISBN|
|**Conference level**| volume, [contributors](#001), ISBN, event_metadata (conference_date, conference_location, conference_acronym, conference_theme, conference_sponsor, conference_number), proceedings_subject|
|**Conference paper**| publication_date, pages, citation_list, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/), and [Crossmark](/documentation/crossmark/) metadata|

|Optional|
|---|
|publisher_item, coden, component_list|

## Dataset metadata <a id="005"></a>

|Required||
|---|---|
|**Database level**| titles|
|**Dataset level**| doi_data|

|Recommended||
|---|---|
|**Database level**|[contributors](#001), description, database_date, publisher, institution, doi_data|
|**Dataset level**| [contributors](#001), titles, database_date, description, format, citation_list, component_list, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/), and [Crossmark](/documentation/crossmark/) metadata|

|Optional|
|---|
|publisher_item, component_list|

## Dissertation metadata <a id="006"></a>

|Required|
|---|
|titles, approval_date, institution, doi_data|

|Recommended|
|---|
[contributors](#001), ISBN, degree, ORCID, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/), and [Crossmark](/documentation/crossmark/) metadata|

|Optional|
|---|
|citation_list, component_list|

## Posted content metadata <a id="007"></a>

|Required|
|---|
|titles, posted_date, doi_data|

|Recommended|
|---|
|group_title, [contributors](#001), acceptance_date, institution, item_number, abstracts, doi_data, citation_list, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/),  [Crossmark](/documentation/crossmark/) metadata and [JATS-formatted abstracts](/documentation/content-registration/descriptive-metadata/abstracts/)|

|Optional|
|---|
|component_list|

## Report / Working paper metadata <a id="008"></a>

|Required||
|---|---|
|**Series level**|titles, ISSN|
|**Report level**|title, publication_date (year)|

|Recommended||
|---|---|
|**Series level**|[contributors](#001), coden, series_number, volume, doi_data, edition_number, approval_date, publisher, institution, doi_data, citation_list|
|**Report level**|[contributors](#001), ORCIDs, edition_number, approval_date, ISBN, publisher, institution, citation_list, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/), and [Crossmark](/documentation/crossmark/) metadata|

|Optional|
|---|
|publisher_item, contract_number|

## Standard metadata <a id="009"></a>

|Required||
|---|---|
|**Standard level**| title, designator, approval_date, standard_body_name, standard_body_acronym|
|**Item level**| [contributors](#001), titles, component_number, publication_date (year), pages, publisher_item, doi_data|

|Recommended||
|---|---|
|**Standard level**| [contributors](#001), edition_number, ISBN, institution, citation_list, [funding](/documentation/funder-registry/funding-data-overview/), [license](/documentation/content-registration/administrative-metadata/license-information/), and [Crossmark](/documentation/crossmark/) metadata metadata|
|**Item level**| citation_list|

|Optional|
|---|
|publisher_item, content_item, component_list|

## Peer Review metadata <a id="010"></a>
|Required|
|---|
|title, review_date (year), relation (isReviewOf)|

|Recommended|
|---|
|[contributors](#001), institution, competing_interest_statement, running_number, [license](/documentation/content-registration/administrative-metadata/license-information/) metadata||
