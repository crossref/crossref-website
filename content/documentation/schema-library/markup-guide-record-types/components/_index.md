+++
title = "Components"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "components"]
identifier = "documentation/schema-library/markup-guide-record-types/books-and-chapters"
rank = 4
weight = 60604
aliases = [
    "/education/content-registration/structural-metadata/components",
    "/education/content-registration/structural-metadata/components/",
    "/documentation/content-registration/structural-metadata/components",
    "/documentation/content-registration/structural-metadata/components/"
]
+++

This guide gives markup examples for members registering books and chapters by direct deposit of XML. Component records are often registered for figures, tables, and supplemental materials associated with a journal article.

### Constructing component deposits <a id='00051' href='#00051'><i class='fas fa-link'></i></a>

Components may be deposited along with their parent DOI or they can be deposited by themselves in a separate XML file as a stand-alone component. Components have their own metadata which is distinct from that of the parent DOI(s).

Components may belong to more than one parent item. For example, two journal articles may include the same component DOI.

### Example of a stand-alone component deposit <a id='00052' href='#00052'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](http://www.crossref.org/xml-samples/components.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.7" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schema/deposit/crossref4.3.7.xsd">
<head>
<doi_batch_id>123456</doi_batch_id>
<timestamp>2015052016</timestamp>
<depositor>
<depositor_name>Crossref sample deposit</depositor_name>
<email_address>pfeeney@crossref.org</email_address>
</depositor>
<registrant>Crossref</registrant>
</head>
<body>
<sa_component parent_doi="10.5555/mrtest2">
<component_list>
<component parent_relation="isPartOf">
<titles>
<title>iso-6892-1.xsd</title>
</titles>
<description>ISO 6892 XML Schema, Reference Implementation</description>
<format mime_type="text/xml"/>
<doi_data>
<doi>10.5555/demo_1.1</doi>
<resource>http://tsturi.cen.eu/root/cwa_16200/iso-6892-1.xsd</resource>
</doi_data>
</component>
</component_list>
</sa_component>
</body>
</doi_batch>
```
