+++
title = "Grants schema"
date = "2025-02-10"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "grants-schema"]
identifier = "documentation/schema-library/grants-schema"
rank = 4
weight = 60406
+++

All metadata records and identifiers registered with Crossref are submitted as XML formatted using our metadata input schema. Unlike other objects registered with Crossref, grants have their own grant-specific input schema. 

Version [0.2.0](https://www.crossref.org/schemas/grant_id0.2.0.xsd) was released in January 2025 and added support for ROR identifiers to identify funders as well as new funding types (APC, BPC, infrastructure) 

## Also supported

* Version [0.1.1](https://www.crossref.org/schemas/grant_id0.1.1.xsd)
* Version [0.1.0](https://www.crossref.org/schemas/grant_id0.1.0.xsd)
