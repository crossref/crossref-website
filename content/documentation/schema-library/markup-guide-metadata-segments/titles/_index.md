+++
title = "Titles"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment", "titles"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/titles"
rank = 4
weight = 60630
aliases = [
  "/documentation/content-registration/descriptive-metadata/title",
  "/documentation/content-registration/descriptive-metadata/title/",
    "/education/content-registration/descriptive-metadata/title",
    "/education/content-registration/descriptive-metadata/title/"
]
+++

This information relates to the title of a work, such as a journal article, book or book chapter, or conference paper. For advice on registering the title of a series, such as a journal, book series, or conference proceedings, learn more about [journal title management](/documentation/content-registration/descriptive-metadata/journal-title-management/).

The title of your work is used for citation matching, so follow these best practices to make sure your metadata can be used correctly by reference management tools:

* Review how the title is treated or changed throughout the various stages of your production workflow
* Title must be in title or sentence case (not ALL CAPS)
* Title field must not include other metadata such as author, price, volume numbers
* Use separate title elements for different language titles - do not cram multiple titles in multiple languages into one element
* Subtitles should be recorded in a separate `subtitle` element
* Use UTF-8 encoding
* May include face markup, LaTeX, or MathML where appropriate

If you need to update or correct a title, learn more about [updating title records](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00167).
