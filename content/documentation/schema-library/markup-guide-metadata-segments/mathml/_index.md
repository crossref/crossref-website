+++
title = "MathML"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment", "mathml"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/MathML"
rank = 4
weight = 60622
aliases = [
    "/documentation/schema-library/including-mathml-in-deposits",
    "/documentation/schema-library/including-mathml-in-deposits",
    "/education/content-registration/metadata-deposit-schema/including-mathml-in-deposits",
    "/education/content-registration/metadata-deposit-schema/including-mathml-in-deposits/",
    "/documentation/content-registration/metadata-deposit-schema/including-mathml-in-deposits/",
    "/documentation/content-registration/metadata-deposit-schema/including-mathml-in-deposits"
]
+++

MathML may be included in the `title`, `subtitle`, `original_language_title`, and `abstract` elements. The MathML namespace (mml) must be defined in the schema declaration, for example:

``` XML
<doi_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.crossref.org/schema/4.4.2 https://data.crossref.org/schemas/crossref4.4.2.xsd"
xmlns="http://www.crossref.org/schema/4.4.2" xmlns:jats="http://www.ncbi.nlm.nih.gov/JATS1"
xmlns:mml="http://www.w3.org/1998/Math/MathML"​>
```

Note that all MathML markup must include an mml namespace prefix:

``` XML
<journal_article publication_type="full_text">
<titles>
<title>Selectron production at an <mml:math><mml:msup><mml:mi>e</mml:mi><mml:mo>&#x02212;</mml:mo></mml:msup><mml:msup><mml:mi>e</mml:mi><mml:mo>&#x02212;</mml:mo></mml:msup></mml:math> linear collider with transversely polarized beams</title>
</titles>
```
