+++
title = "License information"
date = "2024-02-16"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment","license-information"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/license-information"
rank = 4
weight = 60620
aliases = [
    "/education/content-registration/administrative-metadata/license-information",
    "/education/content-registration/administrative-metadata/license-information/",
    "/help/license-best-practice",
    "/help/license-best-practice/",
    "/documentation/content-registration/administrative-metadata/license-information",
    "/documentation/content-registration/administrative-metadata/license-information/"
]
+++

Copyright is a type of intellectual property, which allows the copyright owner to protect against others copying or reproducing their work. Copyright arises automatically when a work that qualifies for protection is created. Scholarly communications relies on researchers sharing, adapting, and building on the work of others, so a license (an official permission or permit) is needed in order for copyrighted content to be used in these ways.

Including license information (or access indicators) in your deposit is very helpful in letting readers know how they can access and use your content, for example, in [text and data mining](/documentation/retrieve-metadata/rest-api/text-and-data-mining). You can include access indicators in metadata deposits.

### Examples of licenses <a id='00026' href='#00026'><i class='fas fa-link'></i></a>

* [BMJ](https://www.bmj.com/company/) - [Text and Data Mining (TDM) Policy and License](https://www.bmj.com/company/legal-information/terms-conditions/legal-information/tdm-licencepolicy/)
* [Copyright Clearance Center](http://www.copyright.com/)’s [About Copyright](http://www.copyright.com/learn/about-copyright/)
* [Creative Commons](https://creativecommons.org/) - [Share your work](https://creativecommons.org/share-your-work/)
* [DOAJ](https://doaj.org/) - [Licensing guide](https://doaj.org/apply/guide/#licensing)
* [Elsevier](https://www.elsevier.com) - [Copyright](https://www.elsevier.com/about/policies/copyright)
* [IEEE](https://www.ieee.org/) - [License Agreements](https://www.ieee.org/publications/subscriptions/info/licensing.html)
* [JISC Collections](https://www.jisc.ac.uk/) - [Guide to the Model license](https://subscriptionsmanager.jisc.ac.uk/about/guide-to-model-licence)
* [PKP](https://pkp.sfu.ca/) - [Contributor License Agreement](https://pkp.sfu.ca/contributor-license-agreement/)

An additional element (`<ai:program>`) has been added from schema version 4.3.2 to support the access indicators schema ([AccessIndicators.xsd](https://data.crossref.org/schemas/AccessIndicators.xsd)).

License information metadata collected includes:

* free-to-read status (`free_to_read`)
* license URL element (`license_ref`)
* `start_date` attribute, optional, date format YYYY-MM-DD
* `applies_to` attribute, optional, allowed values are:
  * *vor* (version of record)
  * *am* (accepted manuscript)
  * *tdm* (text mining)
  * *stm-asf* ([Article Sharing Framework](https://www.stm-assoc.org/asf/))

Note that `free-to-read` is an access indicator, separate from the *license*. It’s used to show that a work is available at no charge for a limited time, but would normally be behind a paywall.

Access indicators may be included in a metadata deposit, submitted as a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/), or as a [supplemental metadata upload](/documentation/member-setup/web-deposit-form#00318), and may be included with Crossmark metadata where applicable. The `ai` namespace must be included in the schema declaration, for example:

```
<doi_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.crossref.org/schema/4.4.2 https://data.crossref.org/schemas/crossref4.4.2.xsd"
xmlns="http://www.crossref.org/schema/4.4.2" xmlns:jats="http://www.ncbi.nlm.nih.gov/JATS1"
xmlns:fr="http://www.crossref.org/fundref.xsd" xmlns:ai="http://www.crossref.org/AccessIndicators.xsd" version="4.4.2">
```

### Best practice for license information <a id='00481' href='#00481'><i class='fas fa-link'></i></a>

This guidance for members on how to register better license metadata with us is to help academic institutions identify content written by their researchers, and how this content may be used, particularly in an automated, machine-readable way.

Institutions need to know which article version may be exposed on an open repository, and from what date. It is no longer sufficient simply to describe in words how they may calculate the embargo end-date, for example, by referring them to a general set of terms and conditions that apply to all of your content across its whole lifecycle – they need to know whether *this* version of *this* article can be exposed on their repository and, if so, from what specific date, and what repository readers can then do with the content they find there.

Our schema contains all the fields you need to specify this unambiguously. By doing so, you can also be more confident that institutions will have the information they need to respect your terms and conditions.

In this section, learn more about:

* [How we collect license information](#00482)
  * [Example: Green OA with Creative Commons license](#00486)
  * [Example: Green OA with member-defined post-embargo license](#00488)
  * [Example: Gold OA](#00531)
* [Use cases](#00532)
* [How to populate your metadata with license information](#00533)

### How we collect license information <a id='00482' href='#00482'><i class='fas fa-link'></i></a>

A single Crossref DOI can be associated with metadata relating to multiple versions of a work: the author's accepted manuscript (AAM), version of record (VoR), or a version intended for text and data mining (TDM). Each of these versions can have their own license conditions attached to them. To reflect this, works registered with us can have multiple license elements. Each license element can contain a URL to a license, the article version to which the license applies, and the license start date. Together, these can describe nuanced license terms across different versions of the work.
An analysis done by Jisc of our metadata found that while 48% of journal articles published in 2017 had license information, the licenses most often referred to the text and data mining version of the work, and licenses were still being used inconsistently for the version of record (VoR) or accepted manuscript (AM).
A major concern is that many members link to their general terms and conditions rather than to licenses that apply at specific times to specific versions of a work. For example, a member may set its policies out in a general terms and conditions page, and link to it in the license metadata:

``` XML
<license_ref applies_to="vor" start_date="2019-01-01">
    http://www.publisherwebsite.com/general_terms_and_conditions
</license_ref>
```

On the terms and conditions page, the member could spell out, for example, the license that applies to the VoR, the restrictions that apply to the AAM during its embargo period, and details of how the AAM may be used after its embargo period. A repository manager would then have to go through the terms and conditions, and manually calculate the embargo end date, in order to determine whether the work could be deposited to a repository. This is a prohibitively onerous process for institutions, and risks content being used outside the terms of member policies because of human error.
It would be helpful if members could instead set out specific licenses for each stage in each article’s lifecycle, for each of its versions. If the licensing terms for a version will change (for example, because it may be exposed on a repository after an embargo period), then a separate license should be used, with the `start_date` element indicating when the new license comes into effect. Using start dates for this license information is best practice in general, as it can validate immediate open access, which is at the heart of many institutional and funder policies. This is set out in more detail in the examples below.

#### Example: Green OA with Creative Commons license <a id='00486' href='#00486'><i class='fas fa-link'></i></a>

In this example, a work is published on 1 January 2019. Under the member’s policy, the VoR is under access controls. The AAM is under embargo for a six-month period and then becomes open access under a CC BY NC ND license.
{{< figure src="/images/documentation/License-green-OA-with-CC.png" caption="Green OA with Creative Commons license" width="75%" >}}
By using a Creative Commons license with a start date, the embargo end date can be unambiguously deduced from the metadata.

#### Example: Green OA with member-defined post-embargo license <a id='00488' href='#00488'><i class='fas fa-link'></i></a>

Linking to a Creative Commons license is optimal whenever possible, as this is an unambiguously open license and so will be readily recognizable as identifying the post-embargo period. It is also a standard license which makes it more easily machine-readable. However, if you need to define your own open license, you can instead link to that in the metadata along with the appropriate start date.
{{< figure src="/images/documentation/License-green-OA-with-member-defined.png" caption="Green OA with member-defined post-embargo license" width="75%" >}}
Repository managers will still be able to unambiguously distinguish works that can be made available after an embargo period, albeit involving a brief manual check, provided the license identifies itself explicitly as referring specifically to the post-embargo period.
It would not be suitable to provide a single URL containing license terms for both the pre-embargo and post-embargo period, for example:

``` XML
<license_ref applies_to="am" start_date="2019-01-01">
    http://www.publisherwebsite.com/am_ general_terms
</license_ref>
```

This would not allow institutions to unambiguously determine the embargo end date and license, and so should be avoided.

#### Example: Gold OA <a id='00531' href='#00531'><i class='fas fa-link'></i></a>

In the case of gold OA, the licenses are simple: both the AAM and the VoR have an open license (in this example, CC BY) that starts no later than the date of publication. The start date could optionally be omitted entirely, since the license terms will apply for the article’s lifetime.
{{< figure src="/images/documentation/License-gold-OA.png" caption="Gold OA license" width="75%" >}}

### Use cases <a id='00532' href='#00532'><i class='fas fa-link'></i></a>

Having clear, unambiguous license metadata helps institutions use the content within your terms and conditions. For example, an institution could query our APIs to find works published by researchers at their organisation (provided you have also populated the affiliations of all the (co-)authors), and check programmatically for the presence and with-effect dates of any open license(s). This would show whether (and if so when) the work can be exposed on their repository.

### How to add license information to your Crossref metadata <a id='00533' href='#00533'><i class='fas fa-link'></i></a>

There are multiple ways that members can add license information to the metadata they deposit/have deposited with us:
* [Add license information to your regular deposits](/documentation/content-registration/administrative-metadata/license-information#00027)
* [Register license information as part of a resource-only deposit](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_license_fulltext_4.8.0.xml) with only license information to populate existing metadata records - learn more about [resource-only deposits](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/)
* [Use a .csv file with license information](/documentation/register-maintain-records/web-deposit-form/#00306) to populate existing metadata records and view an [example .csv file for license metadata](/documentation-files/license-metadata.csv)


### How to register license information as part of a metadata deposit <a id='00027' href='#00027'><i class='fas fa-link'></i></a>

``` XML
<publication_date media_type="print">
  <year>2013</year>
</publication_date>
<pages>
 <first_page>13</first_page>
</pages>
<ai:program name="AccessIndicators">
 <ai:free_to_read start_date="2011-02-11"/>
 <ai:license_ref applies_to="vor" start_date="2011-02-11">https://www.crossref.org/license</ai:license_ref>
</ai:program>
<doi_data>
 <doi>10.5555/openAI_test2</doi>
 <resource>https://www.crossref.org/test</resource>
</doi_data>
```

### How to register license information as part of a resource-only deposit <a id='00028' href='#00028'><i class='fas fa-link'></i></a>

``` XML
<body>
   <!-- license updates with dates / free to read info included-->
   <lic_ref_data>
     <doi>10.5555/pubdate1</doi>
      <ai:program name="AccessIndicators">
        <ai:free_to_read/>
         <ai:license_ref applies_to="vor" start_date="2011-01-11">https://www.crossref.org/vor-license</ai:license_ref>
         <ai:license_ref applies_to="am" start_date="2012-01-11">https://www.crossref.org/am-license</ai:license_ref>
         <ai:license_ref applies_to="tdm" start_date="2012-01-11">https://www.crossref.org/tdm-license</ai:license_ref>
      </ai:program>
   </lic_ref_data>
   <!-- license updates with just license URL included-->
   <lic_ref_data>
      <doi>10.5555/pubdate1</doi>
      <ai:program name="AccessIndicators">
         <ai:free_to_read/>
         <ai:license_ref>https://www.crossref.org/vor-license</ai:license_ref>
         </ai:program>
   </lic_ref_data>
</body>
```
