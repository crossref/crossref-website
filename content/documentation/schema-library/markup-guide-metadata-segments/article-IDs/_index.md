+++
title = "Article numbers or IDs"
date = "2023-02-23"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment", "article-IDs"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/article-IDs"
rank = 4
weight = 60608
aliases = [
    "/education/content-registration/administrative-metadata/article-IDs",
    "/education/content-registration/administrative-metadata/article-IDs/",
    "/documentation/content-registration/administrative-metadata/article-IDs",
    "/documentation/content-registration/administrative-metadata/article-IDs/"
]
+++

## Adding other identifiers <a id='00029' href='#00029'><i class='fas fa-link'></i></a>

Journal articles and other scholarly works often have an ID such as an article number, eLocator, or e-location ID instead of a page number. In these cases, do not use the `<first_page>` tag to capture the ID - instead, use the `<item_number>` tag with the `item_number_type attribute` value set to `article_number`.

### Example article number or ID <a id='00030' href='#00030'><i class='fas fa-link'></i></a>

``` XML
<publication_date media_type="online">
   <month>5</month>
   <day>10</day>
   <year>2017</year>
</publication_date>
<publisher_item>
   <item_number item_number_type="article_number">3D9324F1-16B1-11D7- 8645000102C</item_number>
</publisher_item>
<crossmark>
```

## Internal and other identifiers <a id='00031' href='#00031'><i class='fas fa-link'></i></a>

You can include identifiers that are not explicitly defined in our deposit schema section within the optional `<publisher_item>` section. `<publisher_item>` is also used to capture [article or e-location IDs](https://data.crossref.org/reports/help/schema_doc/5.3.1/common5_3_1_xsd.html#elocation_id). This option should only be used for identifiers that identify the item being registered. Use [relationships](/documentation/content-registration/structural-metadata/relationships/) to capture identifiers for related items.

Examples of identifier types include:

* PII
* SICI
* DOI
* DAI
* Z39.23
* ISO-std-ref
* std-designation
* report-number
* other

### Example of an identifier <a id='00032' href='#00032'><i class='fas fa-link'></i></a>

``` XML
<publisher_item>
   <identifier id_type="**pii**">s00022098195001808</identifier>
</publisher_item>
```
