+++
title = "Abstracts"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment", "abstracts"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/abstracts"
rank = 4
weight = 60602
aliases = [
  "/documentation/content-registration/descriptive-metadata/abstracts/",
  "/documentation/content-registration/descriptive-metadata/abstracts",
    "/education/content-registration/descriptive-metadata/abstracts",
    "/education/content-registration/descriptive-metadata/abstracts/"
]
+++
This guide gives markup examples of abstracts for members registering content by direct deposit of XML. Our [web deposit form](/documentation/content-registration/web-deposit-form/) support abstracts.

Abstracts imported from [JATS](https://jats.nlm.nih.gov/)-formatted XML may be included in records deposited with us. A namespace prefix (`jats:`) must be used for the `abstract` and all child elements, and the namespace must be included in the schema declaration. [MathML](/documentation/content-registration/metadata-deposit-schema/including-mathml-in-deposits) may be included in abstracts but must use a MathML-specific namespace prefix. Multiple abstracts may be included.

Abstracts may be registered for journal articles, books and book chapters, conference papers, posted content, dissertations, reports, and standards.

### Abstracts schema declaration <a id='00019' href='#00019'><i class='fas fa-link'></i></a>

``` XML
<doi_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.crossref.org/schema/4.4.2 https://data.crossref.org/schemas/crossref4.4.2.xsd"
xmlns="http://www.crossref.org/schema/4.4.2" xmlns:jats="http://www.ncbi.nlm.nih.gov/JATS1"
xmlns:mml="http://www.w3.org/1998/Math/MathML">
```

### Example of a JATS-formatted abstract <a id='00020' href='#00020'><i class='fas fa-link'></i></a>

``` XML
</person_name>
</contributors>
<jats:abstract><jats:p>Acute and chronic lung inflammation is associated with numerous important disease pathologies including asthma, chronic obstructive pulmonary disease and silicosis. Lung fibroblasts are a novel and important target of anti-inflammatory therapy, as they orchestrate, respond to, and amplify inflammatory cascades and are the key cell in the pathogenesis of lung fibrosis. Peroxisome proliferator-activated receptor gamma (PPAR**<mml:math><mml:mi>**γ**</mml:mi></mml:math>**) ligands are small molecules that induce anti-inflammatory responses in a variety of tissues. Here, we report for the first time that PPAR**<mml:math><mml:mi>**γ**</mml:mi></mml:math>** ligands have potent anti-inflammatory effects on human lung fibroblasts. 2-cyano-3, 12-dioxoolean-1, 9-dien-28-oic acid (CDDO) and 15-deoxy-**<mml:math><mml:msup><mml:mi>**Δ**</mml:mi><mml:mrow><mml:mn>**12**</mml:mn><mml:mo>**,**</mml:mo><mml:mn>**14**</mml:mn></mml:mrow></mml:msup></mml:math>**-prostaglandin J<jats:sub>2</jats:sub> (15d-PGJ<jats:sub>2</jats:sub>) inhibit production of the inflammatory mediators interleukin-6 (IL-6), monocyte chemoattractant protein-1 (MCP-1), COX-2, and prostaglandin (PG)E<jats:sub>2</jats:sub> in primary human lung fibroblasts stimulated with either IL-1**<mml:math><mml:mi>**β**</mml:mi></mml:math>** or silica. The anti-inflammatory properties of these molecules are not blocked by the PPAR**<mml:math><mml:mi>**γ**</mml:mi></mml:math>** antagonist GW9662 and thus are largely PPAR**<mml:math><mml:mi>**γ**</mml:mi></mml:math>** independent. However, they are dependent on the presence of an electrophilic carbon. CDDO and 15d-PGJ<jats:sub>2</jats:sub>, but not rosiglitazone, inhibited NF-**<mml:math><mml:mi>**κ**</mml:mi></mml:math>**B activity. These results demonstrate that CDDO and 15d-PGJ<jats:sub>2</jats:sub> are potent attenuators of proinflammatory responses in lung fibroblasts and suggest that these molecules should be explored as the basis for novel, targeted anti-inflammatory therapies in the lung and other organs.</jats:p></jats:abstract>
<publication_date media_type="print">
<year>2000</year>
</publication_date>
```
