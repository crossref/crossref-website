+++
title = "Funding data overview"
date = "2025-02-10"
draft = false
author = "Riley Marsh"
type = "documentation"
layout = "documentation_single"
documentation_section = ["funder-registry", "funding-data-overview"]
identifier = "documentation/funder-registry/funding-data-overview"
rank = 4
weight = 90200
aliases = [
  "/education/funder-registry/funding-data-overview",
  "/education/funder-registry/funding-data-overview/"
]
+++

The funding data service lets members register funding source information for content items deposited with Crossref.

## Things to understand before you deposit <a id='00286' href='#00286'><i class='fas fa-link'>``</i></a>

* Funders can be represented three ways: 1) the ROR id, 2) the funder name, or 3) the funder name nested with the funder identifier. Since the Open Funder Registry is transitioning into ROR, using the ROR id to identify funders is the preferred method. 
* If you are not using a ROR id, funding metadata **must** include the name of the funding organization **and** the funder identifier (where the funding organization is listed in the Registry), and should include an award/grant number or grant identifier. Funder names should **only** be deposited without the accompanying ID if the funder is **not** found in the Registry. While members can deposit the funder name without the identifier, those records will not be considered valid until such a time as the funder is added to the database and they are redeposited (updated) with an ID. What that means is that they will not be found using the filters on funding information that we support via our REST API, or show up in our [Open Funder Registry search](https://search.crossref.org/funding).

* Correct **nesting of funder names and identifiers** is essential as it significantly impacts how funders, funder identifiers, and award numbers are related to each other. If you use the ROR id to identify funders, this nesting is not neccessary and invalid. 

Here are some examples in order of most to least preferred:

<br/>
**Correct**: In this example, funder "National Science Foundation" is associated with the ROR id https://ror.org/021nxhr62. No name should be added. 

```
<fr:assertion name="ror">https://ror.org/021nxhr62</fr:assertion>
```

<br/>
**Correct**: In this example, funder "National Science Foundation" is associated with the funder identifier https://doi.org/10.13039/100000001

```
<fr:assertion name="funder_name">National Science Foundation
     <fr:assertion name="funder_identifier">https://doi.org/10.13039/100000001</fr:assertion>
 </fr:assertion>
```

<br/>
**Correct**: In this example, funder "National Science Foundation" is only identified by name.

```
<fr:assertion name="funder_name">National Science Foundation</fr:assertion>
```

<br/>
**Incorrect**: Here, the funder name and funder identifier are not nested - these assertions will be indexed as separate funders.

```
<fr:assertion name="funder_name">National Science Foundation</assertion>
 <fr:assertion name="funder_identifier">https://doi.org/10.13039/100000001</fr:assertion>
```

<br/>
**Incorrect**: Here, the funder name and ROR id will be indexed as separate funders.

```
<fr:assertion name="funder_name">National Science Foundation</assertion>
<fr:assertion name="ror">https://ror.org/021nxhr62</fr:assertion>
```

<br/>
**Incorrect**: Here, the funder name and ROR id are nested - this is invalid.

```
<fr:assertion name="funder_name">National Science Foundation</assertion>
<fr:assertion name="ror">https://ror.org/021nxhr62</fr:assertion>
```

* The purpose of *funder groups* is to establish relationships between funders and award numbers. A funder group assertion should **only** be used to associate funder names and identifiers with award numbers when **multiple funders** are present.
  Funding data deposit with one group of funders (no "fundgroup" needed):

<figure><img src='/images/documentation/Funders-one-group.png' alt='One group of funders' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image25">Show image</button>
<div id="image25" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Funders-one-group.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Funding data deposit with two fundgroups:

<figure><img src='/images/documentation/Funders-two-fundgroups.png' alt='Two fundgroups' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image26">Show image</button>
<div id="image26" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Funders-two-fundgroups.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

* **Incorrect**: Groups used to associate funder names with funder identifiers, these need to be nested as described above.

<figure><img src='/images/documentation/Funders-incorrect-groups-not-nested.png' alt='Groups not nested - incorrect' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image27">Show image</button>
<div id="image27" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >           
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Funders-incorrect-groups-not-nested.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

* Deposits using a *funder_identifier* that is not taken from the Open Funder Registry will be rejected.
* Deposits with only *funder_name* (no *funder_identifier*) will not appear in funder search results in [Open Funder Registry search](https://search.crossref.org/funding) or the [REST API](http://api.crossref.org).

## Funding data schema section <a id='00288' href='#00288'><i class='fas fa-link'>``</i></a>

The `<fr:program>` element in the [deposit schema section](https://data.crossref.org/schemas/crossref4.4.0.xsd) (see [documentation](https://data.crossref.org/reports/help/schema_doc/4.4.0/4.4.0.html)) supports the import of the [fundref.xsd schema](https://data.crossref.org/schemas/fundref.xsd) (see [documentation](https://data.crossref.org/reports/help/schema_doc/4.4.0/fundref_xsd.html)). The fundref namespace (xmlns:fr=https://www.crossref.org/fundref.xsd) must be included in the schema declaration, for example:

```
<doi_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.crossref.org/schema/4.4.2 https://data.crossref.org/schemas/crossref4.4.2.xsd"
xmlns="http://www.crossref.org/schema/4.4.2" xmlns:jats="http://www.ncbi.nlm.nih.gov/JATS1"
xmlns:fr="http://www.crossref.org/fundref.xsd" xmlns:ai="http://www.crossref.org/AccessIndicators.xsd" version="4.4.2">
```

The fundref.xsd consists of a series of nested `<fr:assertion>` tags with enumerated `name` attributes. The `name` attributes are:

* `fundgroup`: used to group a funder and its associated award number(s) for items with multiple funders.
* `ror`: identifier of the funding agency as it appears in the Research Organization Registry (ROR). To be used instead of nested `funder_name` and `funder_identifier`.
* `funder_name`: name of the funding agency as it appears in the funding Registry. Funder names that do not match those in the registry will be accepted to cover instances where the funding organization is not listed.
* `funder_identifier`: funding agency identifier in the form of a DOI, must be nested within the `funder_name` assertion. The `funder_identifier` must be taken from the funding Registry and cannot be created by the member. Deposits without `funder_identifier` or `ror` do not qualify as funding records.
* `award_number`: grant number or other fund identifier
* Either `ror` **or** `funder_name` and `funder_identifier` must be present in a deposit where the funding body is listed in the Open Funder Registry. Multiple `funder_name`, `funder_identifier`, and `award_number` assertions may be included.

## Funder and award number hierarchy <a id='00289' href='#00289'><i class='fas fa-link'>``</i></a>

A relationship between a single funder and an `award_number` is established by including assertions with a `<fr:program>`. 

In this example, funder *National Institute on Drug Abuse* with ROR id *https://ror.org/00fq5cm18* is associated with award number *JQY0937263*:

```
 <fr:program name="fundref">
      <fr:assertion name="ror">https://ror.org/00fq5cm18</fr:assertion>
      <fr:assertion name="award_number">JQY0937263</fr:assertion>
</fr:program>
```

In this example, funder *National Institute on Drug Abuse* with funder identifier *https://doi.org/10.13039/100000026* is associated with award number *JQY0937263*:

```
 <fr:program name="fundref">
      <fr:assertion name="funder_name">National Institute on Drug Abuse
         <fr:assertion name="funder_identifier">https://doi.org/10.13039/100000026</fr:assertion>
      </fr:assertion>
      <fr:assertion name="award_number">JQY0937263</fr:assertion>
   </fr:program>
```

If multiple funder and award combinations exist, each combination should be deposited within a `fundgroup` to ensure that the award number is associated with the appropriate funder(s). In this example, two funding groups exist:

1. Funder *National Science Foundation* with ROR id *https://ror.org/021nxhr62* is associated with award numbers *CBET-106* and *CBET-106*, and
2. Funder *Basic Energy Sciences, Office of Science, U.S. Department of Energy* with funder identifier *https://doi.org/10.13039/100006151* is associated with award number *1245-ABDS*.

```
<fr:program name="fundref">
     <fr:assertion name="fundgroup">
        <fr:assertion name="ror">https://ror.org/021nxhr62</fr:assertion>
        <fr:assertion name="award_number">CBET-106</fr:assertion>
        <fr:assertion name="award_number">CBET-7259</fr:assertion>
     </fr:assertion>
    <fr:assertion name="fundgroup">
        <fr:assertion name="funder_name">Basic Energy Sciences, Office of Science, U.S. Department of Energy
            <fr:assertion name="funder_identifier">https://doi.org/10.13039/100006151</fr:assertion>
        </fr:assertion>
        <fr:assertion name="award_number">1245-ABDS</fr:assertion>
     </fr:assertion>
</fr:program>
```

Items with multiple funder names but no award numbers may be deposited without a fundgroup.

At a minimum, a funding data deposit must contain either a `ror` **or** a `funder_name` and `funder_identifier` assertion, and using the ROR id is preferred. Deposits with just an `award_number` assertion are not allowed. A `ror` or nested `funder_name`\\`funder_identifier` **and** `award_number` should be included in deposits whenever possible. If a ROR id is used, it should not include a `funder_name` or `funder_identifier`.

If the funder name cannot be matched in ROR or the Open Funder Registry, you may submit `funder_name` only, and the funding body will be reviewed and considered for addition to the official Registry. Until it is added to the Registry, the deposit will not be considered a valid funding record and will not appear in funding search or the REST API.

As demonstrated in [Example 3](/documentation/funder-registry/funding-data-overview#00296) below, items with several award numbers associated with a single funding organization should be grouped together by enclosing the `funder_name`, `funder_identifier`, and `award_number(s)` within a `fundgroup` assertion.

Some rules will be enforced by the deposit logic, including:

* **Nesting of the** `<fr:assertion>` **elements**: the schema allows infinite nesting of the assertion element to accommodate nesting of an element within itself. Deposit code will only allow 3 levels of nesting (with attribute values of `fundgroup`, `funder_name`, and `funder_identifier`)
* **Values of different** `<fr:assertion>` **elements**: `funder_name`, `funder_identifier`, and `award_number` may have deposit rules imposed
* **Only valid funder identifiers will be accepted**: the `funder_identifier` value will be compared against the Open Funder Registry file. If the `funder_identifier` is not found, the deposit will be rejected.

## Deleting or updating funding metadata <a id='00290' href='#00290'><i class='fas fa-link'>``</i></a>

If funding metadata is incorrect or out-of-date, it may be updated by [redepositing the metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata). Be sure to redeposit all available metadata for an item, not just the elements being updated. A DOI may be updated without resubmitting funding metadata, as previously deposited funding metadata will remain associated with the DOI.

Funding metadata may be deleted by redepositing an item with an empty `<fr:program name="fundref">` element:

```
<publication_date media_type="print">  	
    <year>2011</year>
</publication_date>
<pages>
 	<first_page>15</first_page>
</pages>    
<fr:program name="fundref" />
<doi_data>
    <doi>10.5555/cm_test_1.1</doi>
      <resource>https://www.crossref.org/crossmark/index.html</resource>
</doi_data>
```


## Funder metadata examples <a id='00293' href='#00293'><i class='fas fa-link'>``</i></a>

### Example 1: Funder information with ROR id <a id='00295' href='#00295'><i class='fas fa-link'>``</i></a>

The `<fr:program>` element captures funding data. It should be placed before the `<doi_data>` element. This deposit contains minimal funding data - one `ror` must be present; it is recommended over using `funder_name` and `funder_identifier`.

```
<fr:program name="fundref">
     <fr:assertion name="ror">https://ror.org/021nxhr62</fr:assertion>
</fr:program>
```


### Example 2: One funder, two grant numbers <a id='00296' href='#00296'><i class='fas fa-link'>``</i></a>

This example contains one `funder_name` and one `funder_identifier`. Note that the `funder_identifier` is nested within the `funder_name` assertion, establishing https\://doi.org/10.13039.100000001 as the funder identifier for funder name *National Science Foundation*. Two award numbers are present.

```
<fr:program name="fundref">
     <fr:assertion name="funder_name">National Science Foundation
       	<fr:assertion name="funder_identifier">https://doi.org/10.13039/100000001</fr:assertion>
     </fr:assertion>
     <fr:assertion name="award_number">CBET-106</fr:assertion>
     <fr:assertion name="award_number">CBET-7259</fr:assertion>
</fr:program>
```


### Example 3: Multiple funders and grant numbers <a id='00297' href='#00297'><i class='fas fa-link'>``</i></a>

This example contains one `ror` (for the National Science Foundation) and one `funder_name/identifier` (for Basic Energy Sciences, Office of Science, U.S. Department of Energy) with two `award_numbers` for each funder. Each funding organization is within its own `fundgroup`.

```
<fr:program name="fundref">
     <fr:assertion name="fundgroup">
        <fr:assertion name="ror">https://ror.org/021nxhr62</fr:assertion>
        <fr:assertion name="award_number">CBET-106</fr:assertion>
        <fr:assertion name="award_number">CBET-7259</fr:assertion>
     </fr:assertion>
    <fr:assertion name="fundgroup">
        <fr:assertion name="funder_name">Basic Energy Sciences, Office of Science, U.S. Department of Energy
            <fr:assertion name="funder_identifier">https://doi.org/10.13039/100006151</fr:assertion>
        </fr:assertion>
        <fr:assertion name="award_number">1245-ABDS</fr:assertion>
        <fr:assertion name="award_number">98562-POIUB</fr:assertion>
     </fr:assertion>
</fr:program>
```
