+++
title = "Checklist for platform migration"
date = "2020-04-08"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["member-setup", "working-with-a-service-provider", "checklist-for-platform-migration"]
identifier = "documentation/member-setup/working-with-a-service-provider/checklist-for-platform-migration"
rank = 4
weight = 50904
aliases = [
    "/affiliates/platform-checklist/",
    "/affiliates/platform-migration-checklist",
    "/platform-migration-checklist",
    "/education/member-setup/working-with-a-service-provider/checklist-for-platform-migration",
    "/education/member-setup/working-with-a-service-provider/checklist-for-platform-migration/",
    "/service-providers/platform-checklist/",
    "/service-providers/platform-checklist"
]
+++

This checklist will help you ask the right questions of your content hosting providers when selecting a new platform. It helps ensure that you can continue to participate with us in the way you want to after a migration - without any surprises.

You can cut and paste the bits you need into your RFP, or use *File > Print* in your browser to save or print a copy to use in your meeting. But before you remove any sections, do check that you definitely won’t want them in the future. Even if you aren’t planning to use all the options now, it’s good to know what your chosen platform will be able to do for you in the future, and whether there will be any extra costs involved.

## Record types <a id='00518' href='#00518'><i class='fas fa-link'></i></a>

Is the potential provider able to register these [record types](/documentation/content-registration/content-types-intro/) with Crossref (using the relevant schema)?  

|Record type|Able to register?|Extra charge?|Charge (if applicable)|
|--- |--- |--- |--- |
|Books, chapters and reference works|
|Components (as part of other content)|
|Conference proceedings and conference papers|
|Datasets|
|Journal articles|
|Peer review reports|
|Pending publications (DOIs on acceptance)|
|Preprints|
|Reports and working papers|
|Standards|
|Theses and dissertations|
|Grants that you award|

## Metadata elements <a id='00519' href='#00519'><i class='fas fa-link'></i></a>

Is the potential provider able to gather and register these metadata elements with Crossref?

|Metadata type|Able to register?|Extra charge?|Charge (if applicable)|
|--- |--- |--- |--- |
|Abstracts|
|Award/grant numbers|
|Full-text URLs for Similarity Check|
|Full-text URLs for text mining|
|Funder IDs|
|License URLs (eg for Version of Record (VOR), Accepted Manuscript (AM), Text and Data Mining (TDM) or STM's Article Sharing Framework)|
|ORCID iDs|
|References|
|Relationships (such as data, translation, preprint)|
|URL, publication title, authors, and dates|
|ROR identifiers|

## Other Crossref services <a id='00520' href='#00520'><i class='fas fa-link'></i></a>

Is the potential provider able to support these services?

|Other Crossref services|Able to register?|Extra charge?|Charge (if applicable)|
|--- |--- |--- |--- |
|Cited-by display|
|Crossmark display|
|Reference linking (displaying Crossref DOIs in article reference lists)|

## Backfile content <a id='00521' href='#00521'><i class='fas fa-link'></i></a>

Is the potential provider able to support the deposit of metadata for backfile content?

|Backfile content|Extra charge?|Charge (if applicable)|
|--- |--- |--- |
|Will backfile content be supported?|
|Will backfile content be migrated at the same time as current?|
|Will backfile content registration happen at the same time?|

## DOI obligations and best practice <a id='00522' href='#00522'><i class='fas fa-link'></i></a>

Is the potential provider able to support the DOI best practice and obligations?

|DOI best practice and obligations|Extra charge?|Charge (if applicable)|
|--- |--- |--- |
|[DOI display guidelines compliance](/display-guidelines/)|
