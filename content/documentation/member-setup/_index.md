+++
title = "Setting up as a member"
date = "2025-02-20"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["member-setup"]
aliases = [
    "/get-started",
		"/get-started/",
    "/education/member-setup",
    "/education/member-setup/"
]
[menu.main]
parent = "Documentation"
identifier = "documentation/member-setup"
weight = 100000
[menu.documentation]
parent = "Documentation"
identifier = "documentation/member-setup"
weight = 100000
rank = 4
+++

You need to be a member of Crossref in order to get your Crossref prefix and register your content with us. Membership of Crossref is about more than just registering DOIs - find out more on our [membership page](/membership). You can apply to join there too.

After you’ve applied for membership and paid your pro-rated membership fee for the remainder of the current year, we set you up with your own Crossref DOI prefix. We also help you set up the [Crossref account credentials](/documentation/member-setup/account-credentials/) that you’ll use to access our systems and register your content.

There are three key steps to getting started, and you can even start step one before you’ve received your new prefix and credentials.

1. [Prepare to register your content](#00646)
2. [Register and verify](#00650)
3. [Display your DOIs](#00653)

## Step 1: Prepare to register your content <a id='00646' href='#00646'><i class='fas fa-link'></i></a>

### a) Choose your content registration method <a id='00647' href='#00647'><i class='fas fa-link'></i></a>

{{< snippet "/_snippet-sources/content-reg.md" >}}

[Find the best option for you](/documentation/member-setup/choose-content-registration-method#00046).

### b) Decide how you’ll construct your DOI suffixes <a id='00648' href='#00648'><i class='fas fa-link'></i></a>

A DOI has several sections, including a prefix and a suffix.  A DOI will always follow this structure:

https\://doi.org/[your prefix]/[a suffix of your choice]

We provide you with your prefix, but you decide what’s in the suffix for each of your DOIs when you register them with us. Your DOIs will look something like this:

{{< figure-linked
	src="/images/documentation/DOI-structure.png"
	large="/images/documentation/DOI-structure.png"
	alt="The structure of a DOI: resolver, prefix, suffix"
	title="The structure of a DOI: resolver, prefix, suffix"
	id="image125"
>}}

If you use the Crossref XML plugin for OJS, they can provide suffixes for you by default, but otherwise you’ll need to decide on your own suffix pattern. It’s important to keep this *opaque*.

{{< snippet "/_snippet-sources/opaque.md" >}}

Learn more about [constructing your DOIs](/documentation/member-setup/constructing-your-dois/).

### c) Ensure your landing pages meet the guidelines <a id='00649' href='#00649'><i class='fas fa-link'></i></a>

You’ll need a live, unique landing page on your website for each item you register and this landing page will need to contain specific information

Learn more about [landing pages](/documentation/member-setup/creating-a-landing-page/).

## Step 2: Register and verify <a id='00650' href='#00650'><i class='fas fa-link'></i></a>

### a) Set the password on your Crossref account credentials <a id='00676' href='#00676'><i class='fas fa-link'></i></a>

You’ll need a set of [Crossref account credentials](/documentation/member-setup/account-credentials/) to access our content registration tools. We'll send you an email so you can set your password.

### b) Register your content <a id='00651' href='#00651'><i class='fas fa-link'></i></a>

You should assign Crossref DOIs to anything that’s likely to be cited in the scholarly literature - journals and journal articles, books and book chapters, conference proceedings and papers, reports, working papers, standards, dissertations, datasets, and preprints.

{{< snippet "/_snippet-sources/DOI-can't-delete.md" >}}

Working with Crossref is about more than just DOIs. When you register content with us, you do  register the DOI and the resolution URL, but you also register a comprehensive set of metadata - rich information about the content. This metadata is then distributed widely and used by many different services throughout the scholarly community, helping with discoverability of your content.

If you are registering DOI records for journal articles, you will include metadata about the journal title that this article was published in. When you register your first article for a journal, be really careful about the journal title you enter - this will create a journal title record and any future submissions will have to match this. Your journal title doesn’t have to match the title in the ISSN portal, but if you do want it to match, make sure to check what this is before you register your first item.

Content registration instructions for helper tools:  

* [Crossref XML plugin for OJS](/documentation/content-registration/ojs-plugin/)
* [Web deposit form](/documentation/content-registration/web-deposit-form/)

Content registration instructions for direct deposit of XML:

* [Upload XML files using our admin tool](/documentation/member-setup/direct-deposit-xml/admin-tool/)
* [XML deposit using HTTPS POST](/documentation/member-setup/direct-deposit-xml/https-post/)
* [Upload JATS XML using the web deposit form](/documentation/content-registration/web-deposit-form/)

### c) Verify your registration <a id='00652' href='#00652'><i class='fas fa-link'></i></a>

When you register your content, you’ll receive a message telling you whether your submission has been successful, or whether there are any problems. If there are problems, your DOI may not be live so do check this message carefully.

Learn more about how to [verify your registration](/documentation/content-registration/verify-your-registration/).

## Step 3: Display your DOIs <a id='00653' href='#00653'><i class='fas fa-link'></i></a>

Don’t forget to display your DOI on the landing page of each item you register - this is an obligation of membership. You’ll need to display your DOI as a link like this:

https://doi.org/10.xxxx/xxxxx

Learn more about [Crossref DOI display guidelines](/display-guidelines/).

## How to get help and support <a id='00330' href='#00330'><i class='fas fa-link'></i></a>

Our support team is available to help if you have any problems, and you may find help from others in our community on our [Crossref Forum](https://community.crossref.org/c/new-crossref-members/14). We also run regular "Ask Me Anything" webinars for new members - learn more about our [webinars](/webinars/) and register to attend.

## What happens next? <a id='00654' href='#00654'><i class='fas fa-link'></i></a>

Once you’ve started registering your content with Crossref and displaying your DOIs on your landing pages, it doesn’t stop there. After you first join, we send you a series of onboarding emails to help you through the next stages. If you want to get started straight away, take a look at how to get started [constructing your DOIs](/documentation/member-setup/constructing-your-dois/).
