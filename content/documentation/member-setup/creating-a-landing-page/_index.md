+++
title = "Creating a landing page"
date = "2024-04-19"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["member-setup", "creating-a-landing-page"]
identifier = "documentation/member-setup/creating-a-landing-page"
rank = 4
weight = 50800
aliases = [
	"/education/member-setup/creating-a-landing-page",
	"/education/member-setup/creating-a-landing-page/"
]
+++

As soon your content is registered with Crossref, users will be able to retrieve identifiers and create links with them. Crossref DOIs must resolve to a unique landing (or response) page that you maintain.

A landing page is a web page that provides further information to someone who has clicked on a DOI link to help them confirm that they are in the right place. It's important that each DOI resolves to a unique landing page that is just for that specific item.

### Landing pages for published research outputs

The landing page for research outputs should be unique for that item and should contain:

* **Full bibliographic information**: so that the user can verify they have been delivered to the correct item
* **The DOI displayed as a URL**: so that if a reader wishes to cite this item, they can just copy and paste the DOI link (learn more about our [DOI display guidelines](/display-guidelines))
* **A way to access the full-text of the content**: It's acceptable for the full-text to be behind a login or paywall - this is fine as long as the landing page is accessible to everyone. A DOI can resolve to the HTML full-text of the content, and if this page includes the criteria above, a separate landing page is not necessary. It's not good practice to link directly to a PDF however, as it will start downloading when the DOI is clicked.

Here are some examples of landing pages for published research outputs:

* [https://doi.org/10.54825/IOLO6421](https://doi.org/10.54825/IOLO6421) - an open-access journal article
* [https://doi.org/10.11116/MTA.2.2.2](https://doi.org/10.11116/MTA.2.2.2) - a closed-access (i.e., paywalled) journal article
* [https://doi.org/10.54957/jolas.v2i2.182](https://doi.org/10.54957/jolas.v2i2.182) - an open-access journal article hosted on the OJS platform
* [https://doi.org/10.1109/AIPR.2015.7444535](https://doi.org/10.1109/AIPR.2015.7444535) - a closed-access conference proceeding
* [https://doi.org/10.5772/intechopen.106437](https://doi.org/10.5772/intechopen.106437) - an open-access book chapter
* [https://doi.org/10.18574/nyu/9780814768839.001.0001](https://doi.org/10.18574/nyu/9780814768839.001.0001) - a closed-access book
* [https://doi.org/10.3133/ofr93184](https://doi.org/10.3133/ofr93184) - an open-access report
* [https://doi.org/10.1079/cabicompendium.46299](https://doi.org/10.1079/cabicompendium.46299) - a closed-access dataset
* [https://doi.org/10.62481/f02ed1c5](https://doi.org/10.62481/f02ed1c5) - an open-access posted content document (a scientific blog)

Many publishers also include abstracts on their landing pages, especially for journal articles.

#### And a little more for preprints

As well as the criteria above, a preprint landing page (such as [https://doi.org/10.31235/osf.io/bkx3n](https://doi.org/10.31235/osf.io/bkx3n)) should also prominently identify the content as a preprint and include a link to any AAM or VOR. This information should be above the fold.

### Landing pages for grants

The landing pages for grants should be unique for that specific grant and contain:

* Information about the grant so the user can verify they've been delivered to the correct item
* The DOI displayed as a URL - learn more about our [DOI display guidelines](/display-guidelines).

Here are two example landing pages for grants: [https://doi.org/10.37717/220020589](https://doi.org/10.37717/220020589) and [https://doi.org/10.35802/107769](https://doi.org/10.35802/107769).
