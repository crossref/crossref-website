+++
title = "DOIs for different levels"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["metadata", "persistent-identifiers", "the-structure-of-a-doi"]
identifier = "documentation/metadata/persistent-identifiers/the-structure-of-a-doi"
rank = 4
weight = 50701
aliases = [
  "/education/metadata/persistent-identifiers/the-structure-of-a-doi/",
  "/education/metadata/persistent-identifiers/the-structure-of-a-doi",
  "/education/member-setup/constructing-your-dois/the-structure-of-a-doi",
  "/education/member-setup/constructing-your-dois/the-structure-of-a-doi/"
]
+++

## DOIs at different levels <a id='00469' href='#00469'><i class='fas fa-link'></i></a>

A DOI may refer to a journal or book (a *title-level DOI*), or to a specific article or chapter.

<figure><img src='/images/documentation/DOI-levels-A.png' alt='Like a set of nesting dolls, a journal may be made up of volumes, issues, and articles' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image37">Show image</button>
<div id="image37" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/DOI-levels-A.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


## Journals and DOIs <a id='00470' href='#00470'><i class='fas fa-link'></i></a>

Like a set of nesting dolls, a journal may be made up of volumes, each containing a number of issues, each containing a number of articles. You can assign a DOI at each level, for example:

* journal-level-DOI (sometimes called the *title-level-DOI*) `10.5555/QYPF2031`. Like an ISSN, it refers to the whole journal
* volume-level-DOI `10.5555/FFFU4804`
* issue-level-DOI `10.5555/QKLE5634`
* article-level-DOI `10.5555/CNBT7653`

<figure><img src='/images/documentation/DOI-levels-B.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image38">Show image</button>
<div id="image38" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/DOI-levels-B.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


The role of the journal-level-DOI, volume-level-DOI, and issue-level-DOI is to link persistently to a point in the journal structure. These DOIs do not have any associated content, and it does not cost anything to register these DOIs.

However, article-level-DOIs do have associated content, and therefore a fee applies to register these DOIs.

## Books and DOIs <a id='00471' href='#00471'><i class='fas fa-link'></i></a>

Like a set of nesting dolls, a book may be made up of chapters. Again, you can assign a DOI at each level, for example:

* book-level-DOI (sometimes called the *title-level-DOI*) `10.5555/ZAAR1365`. Just like an ISBN, it refers to the whole book.
* chapter-level-DOI `10.5555/TFWD2627`

<figure><img src='/images/documentation/DOI-levels-C.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image39">Show image</button>
<div id="image39" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/DOI-levels-C.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Both book-level-DOIs and chapter-level-DOIs have associated content, and therefore a fee applies to register these DOIs.

Learn more about our [fees for different record types](/fees#content-registration-fees), and [how to construct your DOIs](/documentation/member-setup/constructing-your-dois/).
