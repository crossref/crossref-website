+++
title = "Introduction to posted content (includes preprints)"
date = "2024-04-01"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "content-types-intro", "posted-content-includes-preprints"]
identifier = "documentation/content-registration/content-types-intro/posted-content-includes-preprints"
rank = 4
weight = 60509
aliases = [
    "/education/content-registration/content-types-intro/posted-content-includes-preprints",
    "/education/content-registration/content-types-intro/posted-content-includes-preprints/",
    "/services/content-registration/preprints/",
    "/services/content-registration/preprints"
]
+++

### Why register posted content

Posted content includes preprints, eprints, working papers, reports, dissertations, and many other types of content that has been posted but not formally published. Note that *accepted manuscripts* are not considered posted content.

To qualify as posted content, an item must be posted to a host platform where it will receive some level of stewardship. We’re all about persistence, so it’s vital that everything registered with us be maintained. A preprint should remain online once it has been posted, including once it has appeared in a journal or if an updated version becomes available. If different versions become available, the preprint owner should update the preprint metadata using [relations tags](/documentation/content-registration/structural-metadata/relationships/). In exceptional cases where a preprint is removed, such as in the case of plagiarism or other misconduct, we recommend that the DOI resolves to a page containing at least the title, authors, and a short explanation of the removal. Preprint owners should refer to good practice for journal article retraction in this case. Note that we cannot remove preprint metadata from our records.

{{< snippet "/_snippet-sources/preprints-label.md" >}}

{{< snippet "/_snippet-sources/preprints-benefits.md" >}}

### Obligations and limitations <a id='00080' href='#00080'><i class='fas fa-link'></i></a>

1. Follow the posted content [metadata best practices.](/documentation/principles-practices/posted-content)
2. Designate a specific contact with us who will receive match notifications when an accepted manuscript (AM) or version of record (VOR) of the posted content has been registered.
3. Link your posted content record to that other record within seven days of receiving an alert.
4. Clearly label the manuscript as a preprint above the fold on its landing page, and ensure that any link to the AAM or VOR is also prominently displayed above the fold.
5. Ensure that each version is assigned a new DOI, and associate the versions via a relationship with type *isVersionOf* - learn more about [structural metadata](/documentation/content-registration/structural-metadata) and [declaring relationship types](/documentation/content-registration/structural-metadata/relationships#00041)
6. Crossmark is not currently supported for posted content.
7. You can't add components to posted content.
8. How to register content - using the [Crossref Deposit Plugin for Open Preprint Systems (OPS)](https://docs.pkp.sfu.ca/learning-ops/en/setup#plugins) or [direct deposit of XML](/documentation/content-registration/direct-deposit-xml/).

### Fees <a id='00081' href='#00081'><i class='fas fa-link'></i></a>

In additional to annual membership dues, all records attract a one-time registration fee. There are volume discounts available for posted content. [Read about the fees.](/fees/#volume-discounts-posted-content)

### Key links

* [Posted content metadata best practices](/documentation/principles-practices/posted-content)
* How to register content via the [Crossref Deposit Plugin for Open Preprint Systems (OPS)](https://docs.pkp.sfu.ca/learning-ops/en/setup#plugins) or [direct deposit of XML](/documentation/content-registration/direct-deposit-xml/)
* [Posted content markup guide](/documentation/research-nexus/posted-content-includes-preprints/)

### Associating preprints with later published outputs  <a id='00082' href='#00082'><i class='fas fa-link'></i></a>

Once a research object has been published from the posted content and a DOI has been assigned to it, the preprint publisher will update their metadata to associate the posted content with the DOI of the accepted manuscript (AM) or version of record (VOR).

We will notify the member who deposited metadata for the posted content when we find a match between the title and first author of two publications, so that the potential relationship can be reviewed. The posted content publisher must then update the preprint metadata record by declaring the AM/VOR relationship. The notification is delivered by email to the technical contact on file. Please [contact us](/contact/) if you need the email notifications to be sent to a different address.

### Displaying and labeling on posted content publications <a id='00083' href='#00083'><i class='fas fa-link'></i></a>

You must make it clear that posted content is unpublished and you must ensure that any link to the AM/VOR is prominently displayed, specifically:

* The landing page (or equivalent) of the preprint must be labeled as not formally published (for example, *preprint*, *unpublished manuscript*). This label must appear above the fold (the part of a web page that is visible in a browser window when the page first loads)
* The landing page (or equivalent) of the preprint must link to the AM/VOR when it is made available. The link must be appropriately labeled (for example, *Now published in [Journal Name]*, *Version of record available in [Journal Name]*) and appear above the fold.

### History

We’ve been supporting registration of preprints and other posted content since [2016](https://www.crossref.org/blog/preprints-are-go-at-crossref/).  The [Preprints Advisory Group](https://www.crossref.org/working-groups/preprints/) provides ongoing guidance and advice for developing the preprints metadata we collect.

Our preprints record type was originally developed with advisors from bioRxiv and arXiv,  PLOS, Elsevier, AIP, IOP, and ACM.

You can register posted content (includes preprints) by [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/) - learn more about [markup examples for posted content (includes preprints)](/documentation/schema-library/markup-guide-record-types/posted-content-includes-preprints/).
