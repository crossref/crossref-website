+++
title = "Deposit results"
date = "2022-07-22"
draft = false
author = "Sara Bowman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "metadata-manager", "deposit-results"]
identifier = "documentation/register-maintain-records/metadata-manager/deposit-results"
rank = 4
weight = 50505
aliases = [
  "/education/member-setup/metadata-manager/deposit-results",
  "/education/member-setup/metadata-manager/deposit-results/",
  "/documentation/member-setup/metadata-manager/deposit-results",
  "/documentation/member-setup/metadata-manager/deposit-results/",
  "documentation/content-registration/metadata-manager/deposit-results",
  "documentation/content-registration/metadata-manager/deposit-results/"
]
+++

{{< snippet "/_snippet-sources/metadata-manager-deprecated.md" >}}

Once you click *Deposit*, we immediately process the deposit and display the results for accepted and rejected deposits. All deposit records accepted by the system have a live DOI.

All deposit results are archived and available for reference on the *Deposit history* tab on the top menu bar.

<figure><img src='/images/documentation/MM-deposit-history.png' alt='Metadata Manager deposit history' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image51">Show image</button>
<div id="image51" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-deposit-history.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

You can also see your deposit history in the [admin tool](https://doi.crossref.org) - go to the *Administration* tab, then the *Submissions* tab. Metadata Manager deposit filenames begin with *MDT*. You can even review the XML that Metadata Manager has created your behalf.

## Updating existing records and failed deposits <a id='00489' href='#00489'><i class='fas fa-link'></i></a>

Metadata Manager also makes it easy to update existing records, even if you didn’t use Metadata Manager to make the deposit in the first place. You must add the journal to your workspace before you can update the records associated with it - learn more about [setting up a new journal in your workspace](/documentation/register-maintain-records/metadata-manager/setting-up-a-new-journal-in-your-workspace).  

*Accepted* and *Failed* submissions can be updated using the respective tabs in the workspace. Click into the journal, and then click into the article. Add or make changes to the information, and then [deposit](/documentation/register-maintain-records/metadata-manager/review-and-submit#00487).

## What does the status “warning” in my submission result mean? <a id='00619' href='#00619'><i class='fas fa-link'></i></a>

When similar metadata is registered for more than one DOI, it's possible that the additional DOIs are duplicates. Because DOIs are intended to be unique, the potentially duplicated DOI is called a *conflict*. Learn more about the [conflict report](/documentation/reports/conflict-report/).

In Metadata Manager, if you register bibliographic metadata that is very similar to that for an existing DOI, you will see a status “warning” with your submission result. This is accurate.  

When you return to your journal workspace in Metadata Manager to review your list of DOIs, the DOI that returned the “warning” will display as “failed”. This is inaccurate, as you can see if you try to resolve the DOI in question. We are working on improving the wording in this part of the process to make it less confusing.
