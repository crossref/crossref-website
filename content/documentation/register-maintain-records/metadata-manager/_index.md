+++
title = "Metadata Manager (deprecated)"
date = "2022-07-22"
draft = false
author = "Sara Bowman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "metadata-manager"]
identifier = "documentation/register-maintain-records/metadata-manager"
rank = 4
weight = 50500
aliases = [
  "/education/member-setup/metadata-manager",
  "/education/member-setup/metadata-manager/",
  "/help/metadata-manager",
  "/help/metadata-manager/",
  "/documentation/member-setup/metadata-manager",
  "/documentation/member-setup/metadata-manager/",
  "/documentation/content-registration/metadata-manager",
  "/documentation/content-registration/metadata-manager/"
]
+++

{{< snippet "/_snippet-sources/metadata-manager-deprecated.md" >}}

Metadata Manager (beta) offers a way to deposit and update metadata for journal articles for both single and multiple deposits.

Take a look at the [Metadata Manager tutorial](https://www.youtube.com/embed/OgdVnKQPgQU) to get started.  
{{< youtube OgdVnKQPgQU css-class >}}

## Overview of the Metadata Manager workspace <a id='00480' href='#00480'><i class='fas fa-link'></i></a>

Start from [Metadata Manager](https://www.crossref.org/metadatamanager/), and log in using your [Crossref account credentials](/documentation/member-setup/account-credentials/).

You'll now see your Metadata Manager workspace. This is where all deposits occur, both new deposits and updates to content you’ve already registered with Crossref. To return to this view at any time, click *Home* at the top of the screen.

Your workspace holds your list of publications, and it will be blank when you first log in. As you add the publications you want to manage to Metadata Manager, they’ll start collecting on this screen.

You can add new publications and edit existing publications you have previously submitted to our system from your workspace. You can also click into each publication and add or edit articles against them.

<figure><img src='/images/documentation/MM-workspace.png' alt='The Metadata Manager workspace' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image41">Show image</button>
<div id="image41" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-workspace.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

1. The home button - Return to the overview of all your publications by clicking *Home*.
2. Deposit history - See your previous deposits made via Metadata Manager (excludes deposits via other deposit methods such as HTTPS POST, or the web deposit form).
3. To deposit - Shows items for which you’ve entered information, but have not yet deposited with us. The number next to *To deposit* shows how many records are awaiting deposit.
4. Your username - Shows the credential you’ve used to log in. Click the down arrow to access account functions, log out, and view a tutorial of Metadata Manager.
5. Search publication - This search bar allows you to find and add publications to your workspace. You can search by title name or title-level DOI.
6. New publication - This section allows you to create a new journal and add it to your workspace.
