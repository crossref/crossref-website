+++
title = "Registering new articles and working with volumes/issues"
date = "2022-07-22"
draft = false
author = "Sara Bowman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "metadata-manager", "registering-new-articles-and-working-with-volumes-issues"]
identifier = "documentation/register-maintain-records/metadata-manager/registering-new-articles-and-working-with-volumes-issues"
rank = 4
weight = 50502
aliases = [
  "/education/member-setup/metadata-manager/registering-new-articles-and-working-with-volumes-issues",
  "/education/member-setup/metadata-manager/registering-new-articles-and-working-with-volumes-issues/",
  "/documentation/member-setup/metadata-manager/registering-new-articles-and-working-with-volumes-issues",
  "/documentation/member-setup/metadata-manager/registering-new-articles-and-working-with-volumes-issues/",
  "/documentation/content-registration/metadata-manager/registering-new-articles-and-working-with-volumes-issues"
]
+++

{{< snippet "/_snippet-sources/metadata-manager-deprecated.md" >}}
 
Click into the journal to view all of its associated articles in your workspace. You will only see previous deposits made using Metadata Manager. To see deposits made using other deposit methods, manually add them by searching for the article using *Search*.

<figure><img src='/images/documentation/MM-article-search.png' alt='Search for an article in Metadata Manager' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image44">Show image</button>
<div id="image44" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-article-search.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

* If your journal has volumes and/or issues, and the article is in a new volume and/or issue, go to [new article in new volume and/or issue](/documentation/register-maintain-records/metadata-manager/registering-new-articles-and-working-with-volumes-issues#00483)
* If your journal has volumes and/or issues, and the article is in an existing volume and/or issue, go to [new article in existing issue and/or volume](/documentation/register-maintain-records/metadata-manager/registering-new-articles-and-working-with-volumes-issues#00484)
* If your journal does not have volumes or issues, click *Add record*, select *New article*, and go to [add article metadata](/documentation/register-maintain-records/metadata-manager/registering-new-articles-and-working-with-volumes-issues#00485).

## New article in new volume and/or issue <a id='00483' href='#00483'><i class='fas fa-link'></i></a>

If the article is part of a new volume and/or issue, click *Add record* and select *New volume/issue*. Complete the fields in the volume/issue form. The blue/asterisk \* mark indicates a required field. Click *Save*, then click *Close*. The volume/issue is now added into your workspace (you only need to do this once for all articles associated with this volume/issue). The volume/issue now appears in your journal *Record List* - click *Add article* on the right of that row.

<figure><img src='/images/documentation/MM-new-article-in-new-volumeissue.png' alt='Creating a new volume and/or issue' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image45">Show image</button>
<div id="image45" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-new-article-in-new-volumeissue.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Continue to [add article metadata](/documentation/register-maintain-records/metadata-manager/registering-new-articles-and-working-with-volumes-issues#00485).

## New article in existing issue and/or volume <a id='00484' href='#00484'><i class='fas fa-link'></i></a>

If the new article is part of an existing volume or issue, click on *Add article* by the relevant volume/issue. To add an existing volume/issue to your workspace, enter its DOI into the search bar and click *Add*.

<figure><img src='/images/documentation/MM-new-article-in-existing-volumeissue.png' alt='Creating a new article in an existing volume and/or issue' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image46">Show image</button>
<div id="image46" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-new-article-in-existing-volumeissue.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Continue to [add article metadata](/documentation/register-maintain-records/metadata-manager/registering-new-articles-and-working-with-volumes-issues#00485).

## Adding article metadata <a id='00485' href='#00485'><i class='fas fa-link'></i></a>

Provide contributor, funding, license, references, and additional metadata by clicking on each section to open it out. The blue/asterisk \* mark indicates a required field, and we recommend that you deposit as much metadata as possible for the optional fields.

At any time, click *Continue* (at the top right of the screen) and select *Add to deposit*, *Save*, or *Review*.

If you would like to know more about the metadata for each field, we provide tool tips that appear on the right side of the form. You can turn these off be selecting *Off* in *Show help* slider at the top of the form. For a broader overview, explore our [metadata best practices](/documentation/principles-practices/best-practices/).  

<figure><img src='/images/documentation/MM-adding-article-metadata.png' alt='Adding metadata' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image47">Show image</button>
<div id="image47" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-adding-article-metadata.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Metadata Manager checks your metadata to ensure that you provide the correct type of information needed for a successful deposit. You will see warnings when the metadata does not validate, which contain guidance on the type of metadata we are expecting. These do not need to be corrected until you are ready to submit the deposit.

<figure><img src='/images/documentation/MM-review-metadata.png' alt='Review metadata before making a deposit' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image48">Show image</button>
<div id="image48" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-review-metadata.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

If you participate in [Crossmark](/documentation/crossmark/), you can also add Crossmark metadata to the article record using Metadata Manager. This section will automatically appear at the bottom section of the article form for Crossmark participants - please [contact us](/contact/) if the section doesn’t appear for you.
