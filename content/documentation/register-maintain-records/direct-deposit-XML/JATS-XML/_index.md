+++
title = "Using JATS XML"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "direct-deposit-xml", "web-deposit-form"]
identifier = "documentation/register-maintain-records/direct-deposit-xml/jats-xml"
aliases =[
    "/education/member-setup/verifying-and-testing-your-xml/nlm-jats-to-crossref-conversion/",
    "/education/member-setup/web-deposit-form/",
    "/education/member-setup/verifying-and-testing-your-xml/nlm-jats-to-crossref-conversion",
    "/education/member-setup/direct-deposit-xml/jats-xml",
    "/education/member-setup/direct-deposit-xml/jats-xml/",
    "/documentation/member-setup/direct-deposit-xml/jats-xml",
    "/documentation/member-setup/direct-deposit-xml/jats-xml/",
    "/documentation/content-registration/direct-deposit-xml/jats-xml/",
    "/documentation/content-registration/direct-deposit-xml/jats-xml"
]
rank = 4
weight = 50606
+++

We recommend creating XML directly to our schema rather than trying to convert [JATS (NLM) formatted XML](https://jats.nlm.nih.gov/) as we don't currently have a reliable way to convert it.

There are two unreliable options available in beta.

1) Upload NLM JATS-formatted XML files into our system using the web deposit form
2) Use our basic JATS-to-Crossref XSLT conversion and then upload using the admin tool or https post.

## Upload NLM JATS-formatted XML files using the web deposit form  <a id='wdfjats1' href='#wdfjats1'><i class='fas fa-link'></i></a>

1. Start at the [web deposit form](https://apps.crossref.org/webDeposit/)
2. Under *data type selection*, choose *NLM file*
3. Log in using your [Crossref account credentials](/documentation/member-setup/account-credentials/)
4. Click on *select file* and select your NLM or JATS file
5. Enter the email address that should receive the all-important submission log email
6. Add the *DOI* in the relevant field (if your XML contains `<article-id pub-id-type="doi">` you can leave the *DOI*  field empty)
7. Add the *URL* in the relevant field (if your XML contains `<self-uri>` and that URI contains the URL you intend to register with your DOI, you can leave the *URL* field empty)
8. Click *Upload NLM Data* to submit

You'll receive a [submission log](/documentation/content-registration/verify-your-registration/interpret-submission-logs/) when your deposit is complete. Please review the log to be sure your DOIs have been updated successfully.

## NLM JATS to Crossref conversion <a id='00663' href='#00663'><i class='fas fa-link'></i></a>

We have a basic JATS-to-Crossref XSLT conversion that can be used to transform NLM JATS-formatted XML into Crossref-friendly XML. You may [download the .xsl file](https://github.com/CrossRef/jats-crossref-xslt) for local use.

Please note:

* The journal title used for Crossref deposits be included in the `<journal-title>` element
* The DOI should be included in `<article-id>` with attribute pub-id-type='doi'
* The DOI URL should be included in `<self-uri>`
* The JATS document type definition (DTD) does not have an appropriate place to include the email element used in Crossref deposits - this needs to be added manually.
