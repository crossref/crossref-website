+++
title = "Crossref XML plugin for OJS"
date = "2024-09-06"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "ojs-plugin"]
identifier = "documentation/register-maintain-records/ojs-plugin"
rank = 4
weight = 50300
aliases = [
	"/education/member-setup/ojs-plugin",
	"/education/member-setup/ojs-plugin/",
	"/documentation/member-setup/ojs-plugin/",
	"/documentation/member-setup/ojs-plugin",
	"/documentation/content-registration/ojs-plugin/",
	"/documentation/content-registration/ojs-plugin"
]
+++
# Registering your DOI records using the OJS platform

You can register your DOI records with us using the OJS platform with two extra plugins \- the DOI plugin, and the Crossref XML plugin for OJS. We highly recommend including your references in the metadata you send to us, too \- you can do this by adding the OJS references plugin. 

## Step 1: Set up the DOI plugin

Ask your OJS administrator to install the DOI plugin, and add the DOI prefix that we gave to you. Your prefix will start with 10\. and will be followed by other numbers.

You can check whether the DOI Plugin is already set up by following these steps:

1. Go to ‘Settings’ on your dashboard and click ‘Website’  
2. Switch to the ‘Plugins’ tab

<figure><img src='/images/documentation/step-one-1.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image100">Show image</button>
<div id="image100" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <img src="/images/documentation/step-one-1.png" class="img-responsive" style="width: 100%;">
      </div>
    </div>
  </div>
</div>

3. Search ‘Public Identifier Plugins’ and find ‘DOI’  
4. Click the checkbox on the right side of the DOI plugin description to enable it

<figure><img src='/images/documentation/step-one-2.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image101">Show image</button>
<div id="image101" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <img src="/images/documentation/step-one-2.png" class="img-responsive" style="width: 100%;">
      </div>
    </div>
  </div>
</div>

## Step 2: Set up the Crossef XML plugin for OJS 

To make best use of the plugin, make sure you’re using OJS version 3 or higher.

You can start by finding the Crossref plugin from your dashboard:

1. Click ‘Tools’  
2. Choose the ‘Import/Export’ tab  
3. Click ‘Crossref XML Export Plugin’

<figure><img src='/images/documentation/step-two-1.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image102">Show image</button>
<div id="image102" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/step-two-1.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

You can deposit content with us in one of three ways:

1. Register your content with us automatically using the OJS plugin  
2. Register the content with us manually, from the plugin interface  
3. Have the plugin create an XML file that you can then [upload to our admin tool](https://www.crossref.org/documentation/member-setup/direct-deposit-xml/admin-tool/)

We recommend automatic deposits. 

## Step 3: Enable automatic deposits

Simply click the checkbox at the bottom of the DOI plugin settings to enable automatic deposits.

<figure><img src='/images/documentation/step-three-1.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image103">Show image</button>
<div id="image103" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/step-three-1.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

You’ll then need to add information into the plugin: 

<figure><img src='/images/documentation/step-three-2.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image104">Show image</button>
<div id="image104" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/step-three-2.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Here’s what to enter into each of the fields shown in the screenshot above:

1. Depositor name \- the name of the organization registering the DOIs (note: this field is not authenticated with Crossref)  
2. Depositor email \- the email address of the individual responsible for registering content with Crossref (note: this field is not authenticated with Crossref)  
3. Username \- this is the username element of your Crossref depositor credentials. It will be passed to us to authenticate your submission(s). Your username might be just a collection of letters (role credentials), or it might be an email address (user credentials) \- there is more information on role versus user credentials below.  
4. Password \- this is the password associated with your Crossref depositor credentials

Note: if the combination of username and password is incorrect, OJS will return a ‘401 unauthorized status code’ error at the time of registration. This error indicates that the username and password are incorrectly entered. That is, they do not match the username and/or password set with Crossref.

* If you are using organization-wide, shared [role credentials](https://www.crossref.org/documentation/member-setup/account-credentials/#00376) (i.e. your username is a collection of letters), you can simply add in your shared username and password.  
* If you are using personal [user credentials](https://www.crossref.org/documentation/member-setup/account-credentials/#00368) that are unique to you (i.e. your username is your email address), you’ll need to add your email address *and* your role into the username field, and your personal password into the password field. Here’s an example of what this will look like:

Username: email@address.com/role  
Password: your password

## Step 4: Activate the OJS references plugin

The OJS references plugin is available from OJS 3.1.2 onwards. The plugin will use the Crossref API to check against plain text references and locate possible DOIs for articles. The plugin will also allow the display of reference lists on the article landing page in OJS and deposit them as part of your metadata deposit. Linking references is a requirement of Crossref membership.

Two things need to be set up to activate the references plugin:

a) Workflow Settings

1. Click ‘Settings’ and then ‘Workflow’ from your dashboard  
2. Under the ‘Submission’ tab, choose ‘Metadata’! 

<figure><img src='/images/documentation/step-four-1.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image105">Show image</button>
<div id="image105" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/step-four-1.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>  

3. Scroll down to the bottom and to find the ‘References’ section  
   Make sure you enable references metadata by clicking the checkbox ‘Enable references metadata’. You also need to select the option ‘Ask the author to provide references during submission’.  
4. Click save!  

<figure><img src='/images/documentation/step-four-2.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image106">Show image</button>
<div id="image106" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/step-four-2.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

b) Website Settings

Then you need to activate the references plugin on the website, too, by following the instructions here:

1. Click ‘Settings’ and then ‘Website’ from your dashboard  
2. Choose the ‘Plugins’ tab.

<figure><img src='/images/documentation/step-four-3.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image107">Show image</button>
<div id="image107" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/step-four-3.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

3. Search ‘Crossref reference linking’  
4. Click the ‘Crossref reference linking checkbox

This plugin will deposit the references that you enter into the XML deposit.

## Additional OJS plugins for Crossref

In addition to the Crossref XML plugin for OJS, there are also other important plugins that can be enabled in OJS to enrich your metadata records:

1. [Cited-by (OJS Scopus/Crossref plugin)](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/citationsplugin) \- as of OJS 3.2, this [third-party plugin](https://github.com/RBoelter/citations) allows journals to display citations and citation counts (using article DOIs) from Scopus and/or Crossref.  
2. [Funding Metadata plugin](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/funding) \- as of OJS 3.1.2, it is possible to enable a funder registry plugin for submitting [funding information](https://www.crossref.org/documentation/principles-practices/best-practices/funding/) to Crossref. The plugin will use the [Open Funder Registry](https://www.crossref.org/documentation/funder-registry/accessing-the-funder-registry/) to check against existing funding agencies. The plugin will include funding information in your Crossref DOI deposits.  
3. [Similarity Check plugin](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/simCheck) \- if you are using OJS 3.1.2 or above, you are able to use the Similarity Check plugin. This will enable you to automatically send manuscripts to your iThenticate account to check their similarity to already published content. You will need to be subscribed to Crossref’s [Similarity Check service](https://www.crossref.org/services/similarity-check/) for this to work.

## Getting help with OJS plugins

The team at Crossref didn’t create these plugins \- they were either created by the team at PKP, or by third-party developers. Because of this, we aren’t able to give in-depth help or troubleshooting on problems with these plugins.

If you need more help, you can learn more from [PKP’s Crossref OJS Manual](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/) and [PKP’s Open Journals System 3.3 How to guides on DOI & Crossref Plugins](https://scribehow.com/page/Open_Journal_System_33_How-to_Guides_on_DOI_and_Crossref_Plugins__c0dQgGEISlqjBIWbPUkjpw?referrer=documents) plus there’s a very active [PKP Community Forum](https://forum.pkp.sfu.ca/) that has more information on how to modify your OJS instance to submit metadata and register DOIs with Crossref.

Alternatively, you can [contact the support team at PKP](https://pkp.sfu.ca/contact-us/).