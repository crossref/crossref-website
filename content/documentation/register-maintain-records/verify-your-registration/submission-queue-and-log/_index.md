+++
title = "Submission queue and log"
date = "2022-07-22"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "verify-your-registration", "submission-queue-and-log"]
identifier = "documentation/register-maintain-records/verify-your-registration/submission-queue-and-log"
rank = 4
weight = 60701
aliases = [
    "/education/content-registration/verify-your-registration/submission-queue-and-log",
    "/education/content-registration/verify-your-registration/submission-queue-and-log/",
    "/documentation/content-registration/verify-your-registration/submission-queue-and-log",
    "/documentation/content-registration/verify-your-registration/submission-queue-and-log/"
]
+++

If you register content with us using the web deposit form, XML upload via our admin tool, or XML deposit using HTTPS POST, your submission will be placed in our [submission queue](#00639).

When your deposit has been processed, we’ll email you a submission log containing the final status of your submission. You should [review these submission logs](#00640) to make sure your content was registered or updated successfully.

If you register content with us by sending the files to us directly using the Crossref XML plugin for OJS, or if you’re still using the deprecated Metadata Manager, your submission is processed immediately (it isn’t placed in our submission queue). We don’t send you a submission log to show the final status of your submission; instead, you’ll see a message within Metadata Manager or OJS itself. But a submission log is still generated, and you can log in to our [admin tool](https://doi.crossref.org) using your [account credentials](/documentation/member-setup/account-credentials/) to [view the submission log for your deposit](#00143).

## The submission queue <a id='00639' href='#00639'><i class='fas fa-link'></i></a>

If you’ve registered some content with us using the web deposit form, XML upload via our admin tool, or XML deposit using HTTPS POST, and you don’t receive your submission log email immediately, it is likely that your deposit is waiting in the submission queue.

To see the submission queue, log in to the [admin tool](https://doi.crossref.org) using your [account credentials](/documentation/member-setup/account-credentials/), and click *Show My Submission Queue* on the opening page (or click *Submissions*, then *Show System Queue*).

At the top of the page, you will see all the submissions that are being actively processed at the moment. They are listed individually by submission ID number, along with file name, file type, percent completed, and timestamps.

The submissions that are still waiting to be processed are displayed at the bottom of the page. They are grouped by the role used to submit the files. Click *+* under *Details* (on the left, next to your depositor ID) to expand a list of your deposits waiting to be processed. You will also see the submission ID, filename, and position in the queue.

<figure><img src='/images/documentation/Submission-queue.png' alt='Submission queue in admin tool' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image116">Show image</button>
<div id="image116" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Submission-queue.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

It typically takes only a few minutes for a submission to be picked up for processing and then for the processing to be completed. Processing may take longer depending on overall system traffic, and submission size and complexity. If there is a problem with the submission queue, we usually post an update - please [check our status page for updates](https://status.crossref.org/). If you're concerned about your submission processing time, or are planning a large update and would like to coordinate with us about timing, please [contact us](/contact/).

## Submission logs <a id='00640' href='#00640'><i class='fas fa-link'></i></a>

Submission logs are delivered through these channels:

* [Email](#00641)
* The admin tool - you can [view submission logs for past deposits](#00143) or see the [deposit history for a DOI](#00144) using the admin tool.
* Polling - see [using HTTPS to retrieve logs](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00145)
* [Notification callback service](/documentation/register-maintain-records/verify-your-registration/notification-callback-service/)

## Submission log emails <a id='00641' href='#00641'><i class='fas fa-link'></i></a>

We email you an XML-formatted log for records that are submitted through the web deposit form or Simple Text Query, uploaded via our admin tool, or sent to us through HTTPS POST.

The log is sent to the email address you provided when using the web deposit form or Simple Text Query, or included in the `<email_address>` field in your deposit XML.

The email will have the subject line: *Crossref Submission ID* and it’s sent once your submission has made it through the queue. It includes your submission ID, tells you if your deposit has been successful, and provides the reason for any failure.

## View submission logs for past deposits <a id='00143' href='#00143'><i class='fas fa-link'></i></a>

If you didn't receive a submission log email, you can use the admin tool to search for submission logs for past deposits:

1. Log in to the [admin tool](https://doi.crossref.org) using your [account credentials](/documentation/member-setup/account-credentials/)
2. Click the *Submissions* tab, then the *Administration* sub-tab
3. Click *Search* at the bottom of the screen, and you'll see a list of all past deposits for your account, from newest to oldest.
4. Click on the *Submission ID* number to the left of any deposit to access the *Submission details*, including the submission log for that deposit, or click on the *file* icon to view the file that was submitted.

After step 3 above, you can also narrow your search by entering parameters into any of the following fields on the *Submissions administration* sub-tab page:

* Select a date range using the *Last Day*, *Last Three Days*, or *Last Week* buttons, or enter a custom date range to search for older deposits
* If your account submits metadata deposits for multiple prefixes, you can use the *Registrant* field to narrow your search to just the deposits for a single prefix.
    * Click *Find* next to Registrant
    * In the pop-up window, enter the member name associated with the prefix and click *Submit*
    * Select the appropriate member name/prefix and the pop-up window will close. You'll see a code for that prefix entered in the *Registrant* field
* Select a deposit type from the *Type* drop-down menu to limit your search to just one type of deposit.
    * *Metadata* will limit results to full metadata deposits. This is the most common type.
    * *DOI resources* will limit results to [resource-only deposits](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/), including references, Similarity Check full-text URLs, funding metadata, and license metadata
    * *Conflict Management* will limit results to text files that were deposited to [resolve conflicts](/documentation/reports/conflict-report#00243)
* Check the *Has Error* box to only search for deposits with errors.
* Check the *Has Conflict* box to only search for deposits with conflicts.

## View the history of a DOI <a id='00144' href='#00144'><i class='fas fa-link'></i></a>

Find the deposit history of an individual DOI using the admin tool, including all deposit files and submission logs.

To view a DOI history report:

1. Log in to the [admin tool](https://doi.crossref.org) using your [account credentials](/documentation/member-setup/account-credentials/)
2. Click the *Report* tab
3. Type or paste a DOI into the box
4. Click *Show* to view its report.

The report lists every successful deposit or update of the DOI being searched. View the [submission details](/documentation/register-maintain-records/verify-your-registration/interpret-submission-logs/) (including log and submitted XML) by clicking on the submission number:

<figure><img src='/images/documentation/History-of-a-DOI.png' alt='View the history of a DOI' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image7">Show image</button>
<div id="image7" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/History-of-a-DOI.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## Use HTTPS to retrieve logs <a id='00145' href='#00145'><i class='fas fa-link'></i></a>

In addition to the submission report you receive by email, you can also retrieve the results of submission processing or the contents of a submission at any time using HTTPS. You need to include your [account credentials](/documentation/member-setup/account-credentials/) in the URL.

If you are using organization-wide shared  **role credentials**, please use this version of the query, and swop "role" for your role, and "password" for your password.

```
https://doi.crossref.org/servlet/submissionDownload?usr=_role_&pwd=_password_&doi_batch_id=_doi batch id_&file_name=filename&type=_submission type_
```

If you are using personal, unique **user credentials**, please use this version of the query, and swop "name@someplace.com" for your email address, "role" for your role, and "password" for your personal password.

```
https://doi.crossref.org/servlet/submissionDownload?usr=name@someplace.com/role&pwd=_password_&doi_batch_id=_doi batch id_&file_name=filename&type=_submission type_
```

In both versions of the query, you can choose to track a submission by either its `doi_batch_id` or by its `file_name`. We recommend choosing `file_name`.

The main difference between using `doi_batch_id` and file_name is that `doi_batch_id` is inserted into the database after the submission has been parsed. Using `file_name` is preferable because submissions in the queue or in process can be tracked before deposit. Non-parse-able submissions can also be tracked using this method.

To use this feature effectively, make sure each tracking ID (`doi_batch_id` or `file_name`) is unique as only the first match is returned.

Finally, you need to add in the `type` of data you want back. Use `result` to retrieve submission results (deposit log) or use `contents` to retrieve the XML file.
