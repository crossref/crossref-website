+++
title = "Verify your registration"
date = "2024-10-07"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "verify-your-registration"]
identifier = "documentation/register-maintain-records/verify-your-registration"
rank = 4
weight = 60700
aliases = [
    "/education/content-registration/verify-your-registration",
    "/education/content-registration/verify-your-registration/",
    "/documentation/content-registration/verify-your-registration",
    "/documentation/content-registration/verify-your-registration/"
]
+++

The quickest way to test whether your DOI and its associated metadata have been registered successfully (and your DOI is now active) is to enter your DOI link (DOI displayed as a link, such as [https://doi.org/10.13003/5jchdy](https://doi.org/10.13003/5jchdy)) into a browser window, and check if it resolves correctly. DOI 10.13003/5jchdy has been registered so it is resolving to that DOI's landing page (or, resolution URL). DOIs that have not been registered will resolve to a DOI NOT FOUND error message on doi.org, such as [https://doi.org/10.13003/unregisteredDOI](https://doi.org/10.13003/unregisteredDOI).

If your DOI doesn't resolve successfully, read on for more information about the process your submission goes through, why there might be a delay, and which messages you’ll receive depending on your submission method.

* [Verify your registration - web deposit form](#00525)
* [Verify your registration - grant registration form](#00520)
* [Verify your registration - if you’re still using the deprecated Metadata Manager](#00524)
* [Verify your registration - direct deposit of XML using our admin tool](#00526)
* [Verify your registration - XML deposit using HTTPS POST](#00527)
* [Verify your registration - Crossref XML plugin for OJS](#00146)


## Verify your registration - web deposit form <a id='00525' href='#00525'><i class='fas fa-link'></i></a>

If you register your content using the web deposit form, your submission is sent to a submission queue. You’ll see a “success” message in the web deposit form confirming that your submission has been successfully sent to our submission queue, but this doesn’t mean that your registration is complete.

<figure><img src='/images/documentation/Success-web-deposit-form.png' alt='Web deposit form success message' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image113">Show image</button>
<div id="image113" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Success-web-deposit-form.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

As your submission is processed in the queue, we send you two messages:

1. **XML record email**, subject line: *Crossref WebDeposit - XML*. This email includes the XML created by the web deposit form. Do keep this information, as it may be useful in the future. Receiving this email is a confirmation that your file has been received for processing, and entered into our submission queue.
2. **submission log email**, subject line: *Crossref Submission ID*. This email is sent once your XML has made it through the queue, includes your submission ID, tells you if your deposit has been successful, and provides the reason for any failure.

If your submission log email tells you that your submission was successful, your DOI is now live and active (or your update to metadata for an existing DOI has worked).

If your submission failed, please address the errors flagged in the confirmation, and resubmit. Learn more about [error messages](/documentation/register-maintain-records/verify-your-registration/troubleshooting-submissions/).

If you don’t receive your submission log email immediately, it’s probably because your submission is still in the queue. It can stay in the queue between several minutes and several hours depending on how large your submission file is, and how busy our submission queue is at that time. Learn more about [how to view the submission queue](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00639).

If you don’t receive your submission log email and you can’t see your submission in the queue, it may be that your access to register content has been suspended due to unpaid invoices. If this is the case, please [contact us](/contact/).

## Verify your registration - grant deposit form <a id='00520' href='#00520'><i class='fas fa-link'></i></a>

The grant registration form registers your record in real time, with no queueing or delay. If your submission has been successful, you’ll see a “success” message, which means your DOI is now live and active or your update to an existing DOI has worked.

Your “success” message will also contain a submission ID. If you need to, you can log into our admin tool using your [account credentials](/documentation/member-setup/account-credentials/) and use this submission to [view your deposit](/documentation/content-registration/verify-your-registration/submission-queue-and-log/#00143).

If your submission hasn’t been successful, you’ll see an error message explaining the problem.

## Verify your registration - if you're still using the deprecated Metadata Manager <a id='00524' href='#00524'><i class='fa fa-link'></i></a>

{{< snippet "/_snippet-sources/metadata-manager-deprecated.md" >}}

If you’re still using Metadata Manager, here’s how to verify your registration.  

Unlike other content registration methods, Metadata Manager registers content in real-time - with no queueing of content. If your submission has been successful, you’ll see a “success” message, which means that your DOI is now live and active (or your update to metadata for an existing DOI has worked).

Your "success" message will also contain a submission ID. If you need to, you can log in to our admin tool using your [account credentials](/documentation/member-setup/account-credentials/) and use this submission ID to [view your deposit](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00143).

<figure><img src='/images/documentation/Success-Metadata-Manager.png' alt='Metadata Manager success message' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image114">Show image</button>
<div id="image114" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Success-Metadata-Manager.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

If your submission hasn’t been successful, you’ll see a warning symbol - click on this to see the error message explaining the problem.

<figure><img src='/images/documentation/MM-error.png' alt='Error message in Metadata Manager' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image117">Show image</button>
<div id="image117" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-error.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Learn more about [submitting a deposit](/documentation/register-maintain-records/metadata-manager/review-and-submit#00487), and [reviewing deposit results](/documentation/register-maintain-records/metadata-manager/deposit-results) in Metadata Manager.

## Verify your registration - direct deposit of XML using our admin tool <a id='00526' href='#00526'><i class='fas fa-link'></i></a>

Submissions using our admin tool are sent to a submission queue. Once your submission has been accepted into the queue we display a *SUCCESS - Your batch submission was successfully received* message. This means that your deposit has been submitted to our processing queue, but it has not yet been processed.

<figure><img src='/images/documentation/Success-admin-tool.png' alt='Admin tool success message' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image115">Show image</button>
<div id="image115" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Success-admin-tool.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Registration of your content only occurs after your submission has worked its way through the queue, when you will receive an email with the subject line *Crossref Submission ID*, which includes your submission ID, tells you if your deposit has been successful, and provides the reason for any failure.

If your deposit has been successful, then your new DOI is live and active (or your update to metadata for an existing DOI has worked).

If your submission failed, please address the errors flagged in the email, and resubmit. Not sure what the error messages mean and what you need to do? Learn more about [error messages](/documentation/register-maintain-records/verify-your-registration/troubleshooting-submissions/).

If you don’t receive your submission log email immediately, it’s probably because your submission is still in the queue. It can stay in the queue between several minutes and several hours depending on how large your submission file is, and how busy our submission queue is at that time. Learn more about [how to view the submission queue](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00639).

If you don’t receive your submission log email and you can’t see your submission in the queue, it may be that your access to register content has been suspended due to unpaid invoices. If this is the case, please [contact us](/contact/).

## Verify your registration - XML deposit using HTTPS POST <a id='00527' href='#00527'><i class='fas fa-link'></i></a>

Most items registered with us are submitted via HTTPS POST. When files are POSTed to our system,  you’ll receive a 200 status message to confirm that we’ve received it. Your files are then added to a submission queue to await processing, and once your submission has been processed, you’ll receive a submission log (either by email or through the [notification callback service](/documentation/register-maintain-records/verify-your-registration/notification-callback-service/) if you have that enabled).

If your submission log shows a success, then your DOI is live and active (or your update to metadata for an existing DOI has worked).

If your submission log shows a failure, please address the errors flagged in the email, and resubmit. Not sure what the error messages mean and what you need to do? Learn more about [error messages](/documentation/register-maintain-records/verify-your-registration/troubleshooting-submissions/).

There may be a delay between your submission being received by the queue and completing processing. It can stay in the queue between several minutes and several hours depending on how large your submission file is, and how busy our submission queue is at that time. Learn more about [how to view the submission queue](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00639).

## Verify your registration - Crossref XML plugin for OJS <a id='00146' href='#00146'><i class='fas fa-link'></i></a>

If you are using the Crossref XML plugin for OJS to create an XML file that you upload through our admin tool, please follow [Verify your registration - direct deposit of XML using our admin tool](#00526).

If you are using the Crossref XML plugin for OJS to send your submission to us directly, check the status of your deposit by clicking the *Articles* tab at the top of the plugin settings page.
