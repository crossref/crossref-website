+++
title = "Creating and managing DOIs"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "creating-and-managing-dois"]
identifier = "documentation/register-maintain-records/creating-and-managing-dois"
rank = 4
weight = 60800
aliases = [
    "/education/content-registration/creating-and-managing-dois",
    "/education/content-registration/creating-and-managing-dois/",
    "/education/member-setup/metadata-manager/transferring-titles",
    "/education/member-setup/metadata-manager/transferring-titles/",
    "/education/content-registration/creating-and-managing-dois/managing-existing-dois",
    "/education/content-registration/creating-and-managing-dois/managing-existing-dois/",
    "/documentation/content-registration/creating-and-managing-dois/managing-existing-dois",
    "/documentation/content-registration/creating-and-managing-dois/managing-existing-dois/",
    "/documentation/content-registration/creating-and-managing-dois/",
    "/documentation/content-registration/creating-and-managing-dois"
]
+++

### Creating DOIs
A DOI is registered for each new content item by its owner as it's published. This single DOI would then remain associated with the content item forever. DOIs become active once they and their associated metadata are registered with us. Find out more about:
* [Constructing your DOIs](/documentation/member-setup/constructing-your-dois/)
* [Ways to register your content](/documentation/member-setup/choose-content-registration-method/)

### Managing and updating the metadata for your existing DOIs
Once you have registered your DOIs, you can update the metadata associated with them at any time, free of charge. Here are some [examples of metadata maintenance tasks](/documentation/register-maintain-records/maintaining-your-metadata).

### Changing or deleting DOIs
Because DOIs are designed to be persistent, a DOI string can’t be changed once registered, and DOIs can’t be fully deleted. You can always update the metadata associated with a DOI, but the DOI string itself can’t change. [Find out more](/documentation/content-registration/creating-and-managing-dois/changing-or-deleting-dois).

### Transferring titles or prefixes between members
[Find out what to do](/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois) if a title with existing DOIs is acquired by a member with a different DOI prefix.

### Multiple resolution
[Multiple resolution](/documentation/content-registration/creating-and-managing-dois/multiple-resolution/) is used where many members host the same content.
