+++
title = "Multiple resolution"
date = "2024-04-03"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "creating-and-managing-dois"]
identifier = "documentation/content-registration/register-maintain-records"
weight = 60804
aliases = [
    "/education/content-registration/creating-and-managing-dois/multiple-resolution",
    "/education/content-registration/creating-and-managing-dois/multiple-resolution/",
    "/faqs/multiple-resolution/",
    "/faqs/multiple-resolution",
    "/services/content-registration/multiple-resolution/",
    "/services/content-registration/multiple-resolution",
    "/documentation/content-registration/creating-and-managing-dois/multiple-resolution",
    "/documentation/content-registration/creating-and-managing-dois/multiple-resolution/",
    "/get-started/multiple-resolution/",
    "/get-started/multiple-resolution/"

]
+++

Ideally, a DOI is registered for each new content item by its owner prior to or at the time it is published. This single DOI would then remain associated with the content item forever. However, because content can travel from place to place online, and it can live in multiple locations, a content item may exist at more than one URL. With multiple resolution, you can assign multiple URLs to a single metadata record. Members often use multiple resolution for co-hosted content or content in transition from one platform to another. Instead of resolving directly to a single page, a multiple resolution-enabled link will instead land on an interim page. The interim page presents a list of link choices to the end-user.

A single member may register multiple URLs for their content, but multiple resolution usually involves coordination between several members. One member needs to deposit the DOIs and metadata as the primary depositor. The primary depositor is typically the DOI prefix owner of the content being registered, and will commit to maintaining the metadata record. If second (or third) parties are involved, they will only be able to add and update secondary URLs for existing records.

Multiple resolution interim pages can be set up for an entire DOI prefix, or for individual titles. 

There are no fees associated with multiple resolution. To get started, please [let us know](/contact/) who and what content is involved in your multiple resolution project, and send us your additional URLs. Learn more about [how to set up multiple resolution](/documentation/content-registration/creating-and-managing-dois/multiple-resolution#00118).

If the content you are working with does not already have DOIs and is not published by you, please [contact us](/contact/).

On this page, learn more about:

{{% row %}}
{{% column %}}

* [How to set up multiple resolution](#00118)
  * [Unlocking DOIs for multiple resolution](#00123)
  * [Registering secondary URLs](#00126)
* [How to update multiple resolution URLs](#00128)
* [Reversing multiple resolution](#00129)
* [DOI resolution by country code](#00130)

{{% /column %}}
{{% column %}}

* [The role of the DOI proxy in multiple resolution](#00133)
* [What if I want to do multiple resolution but sometimes want to send people directly to my site?](#00135)
* [How does multiple resolution affect my resolution statistics?](#00117)

{{% /column %}}
{{% /row %}}

## How to set up multiple resolution <a id='00118' href='#00118'><i class='fas fa-link'></i></a>

Multiple resolution typically involves two (or more) members involved in a co-hosting agreement. For the purposes of multiple resolution, the *primary depositor* is the member responsible for the prefix of the multiple resolution content being registered. The *secondary depositor* has been authorized by the content owner to also host content and assign additional URLs (called *secondary URLs*) to DOIs. We’ll always defer to the primary depositor’s instructions regarding changes to a metadata record including all assigned URLs.

Follow these steps to coordinate and implement multiple resolution:

1. Establish permissions - [contact us](/contact/) to let us know what organizations and content will be involved in your multiple resolution project and we’ll adjust permissions as needed.
You can skip this step if you are implementing multiple resolution without a secondary depositor or intend to supply the secondary URLs yourself (as you are by default enabled to register multiple resolution URLs for your own content).
The primary depositor must notify us of the intention to implement multiple resolution for their metadata records, as well as all titles and/or prefixes involved. The secondary depositor may coordinate multiple resolution activity with permission from the primary depositor - this can be an [email](/contact/) stating, for example: *XYZ Publishing has permission to coordinate multiple resolution activity on our behalf for titles (...)*.
Primary depositors can create metadata records and deposit primary and secondary URLs. Secondary depositors may only register secondary URLs for existing records. The secondary depositor will be assigned a new system account to be used for multiple resolution deposits only.
2. [Unlock your DOIs](#00123) - you must enable each metadata record for multiple resolution by sending us an ‘unlock’ flag for each DOI. This can be included in your files or sent separately as a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/), like this [example file](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_mr_unlock-only_4.8.0.xml).
3. [Register your secondary URLs](#00126). Secondary URLs are usually added to an existing metadata record using a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/). The secondary URL registration file contains the DOI(s) being updated, the secondary URL, and a label. The label value is case-sensitive and must be a minimum of 6 characters (no spaces).

### Create an interim page template <a id='00119' href='#00119'><i class='fas fa-link'></i></a>

We provide a standard interim page when a reader clicks on a DOI that is in a multiple resolution relationship. This means that users can be confident that they’ll see consistent behaviour across all Crossref DOIs that are in multiple resolution. Depositors don’t need to do anything to create this interim page, it will generate automatically for a DOI when the multiple resolution relationship is created. 

Clicking on these DOIs will take you to examples of the interim pages: 

[https://doi.org/10.1007/978-94-6209-116-0](https://doi.org/10.1007/978-94-6209-116-0)

[https://doi.org/10.1049/cp.2018.1305](https://doi.org/10.1049/cp.2018.1305)

[https://doi.org/10.18574/nyu/9781479845309.003.0004](https://doi.org/10.18574/nyu/9781479845309.003.0004)

Note that the logos are pulled from a service called [Clearbit](https://clearbit.com/) who curate company logo and other information. These are not hosted and curated by Crossref. If your logo isn’t appearing and you would like it to, or you'd like to update your logo, you can contact [support@clearbit.com](mailto:support@clearbit.com) so that they can assess your request.

### Unlock DOIs for multiple resolution <a id='00123' href='#00123'><i class='fas fa-link'></i></a>

The primary depositor must enable (or *unlock*) each multiple resolution DOI before secondary URLs can be deposited. You can do this using either a metadata deposit or a resource-only deposit (details below). It is expected that once a content owner gives permission for multiple resolution to be attached to DOIs of a given title, or to all their content, that the content owner will routinely enable multiple resolution when creating or updating their DOIs.

Unlocking a DOI does not change the linking behavior of a DOI - an unlocked DOI will continue to resolve to the URL supplied during registration until a secondary URL has been registered.

#### Unlock DOIs using the main deposit schema <a id='00124' href='#00124'><i class='fas fa-link'></i></a>

This mode should be used for all new DOIs created after the content owner has recognized that secondary deposits will be taking place. It allows the primary content owner to enable the DOI multiple resolution permission at the same time as the DOI is initially created.

The XML used by the content owner to create (or update) the DOI must include an a <collection> element with the multi-resolution attribute set to *unlock*.

```
<?xml version="1.0" encoding="UTF-8"?>
<doi_batch version="4.3.0" xmlns="http://www.crossref.org/schema/4.3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/schema/4.3.0 http://www.crossref.org/schemas/crossref4.3.0.xsd">
<head>
  <doi_batch_id>123456</doi_batch_id>
  <timestamp>19990628123304</timestamp>
  <depositor>
	<name>xyz</name>
	<email_address>support@crossref.org</email_address>
  </depositor>
  <registrant>Crossref Test</registrant>
</head>
<body>
  <journal>
	<journal_metadata language="en">
    	<full_title>Sample Journal</full_title>
    	<abbrev_title>SJ</abbrev_title>
     	<issn media_type="print">55555555</issn>
	</journal_metadata>
   <journal_issue>
 	<publication_date media_type="print">
    	<year>2008</year>
 	</publication_date>
 	<journal_volume>
    	<volume>10</volume>
 	</journal_volume>
	<issue>10</issue>
   </journal_issue>
   <journal_article publication_type="full_text">
	<titles>
  	<title>Sample Article</title>
	</titles>
	<contributors>
 	<person_name sequence="first" contributor_role="author">
    	<given_name>Firstname</given_name>
    	<surname>Surname</surname>
  	</person_name>
	</contributors>
	<publication_date media_type="print">
               	<year>2008</year>
	</publication_date>
 	<pages>
    	<first_page>1</first_page>
	</pages>
 	<doi_data>
    	<doi>10.50505/mrtest</doi>
     	<resource>http://www.crossref.org/hello/</resource>
     	<collection property="list-based" multi-resolution="unlock" />
 	</doi_data>
	</journal_article>
  </journal>
</body>
</doi_batch>
```

#### Unlock DOIs using the DOI resources schema <a id='00125' href='#00125'><i class='fas fa-link'></i></a>

This approach can be used for all existing records or can be used for new records if the content owner does not wish to include this metadata in their main metadata deposit. Resource-only deposits should be uploaded as 'DOI Resources' when using the [admin tool](https://doi.crossref.org/) or `operation=doDOICitUpload` when doing a [programmed HTTPS transaction](/documentation/member-setup/direct-deposit-xml/https-post/).

```
<?xml version="1.0" encoding="UTF-8"?>
<doi_batch version="4.3.0" xmlns="http://www.crossref.org/doi_resources_schema/4.3.0">
   <head>
 	<doi_batch_id>123456</doi_batch_id>
  	<depositor>
     	<name>xyz</name>
     	<email_address>support@crossref.org</email_address>
  	</depositor>
	</head>
  <body>
 	<doi_resources>
    	<doi>10.50505/mrtest</doi>
    	<collection property="list-based" multi-resolution="unlock" />
 	</doi_resources>
 	<doi_resources>
    	<doi>10.50505/mrtest2</doi>
    	<collection property="list-based" multi-resolution="unlock" />
 	</doi_resources>
 	<doi_resources>
    	<doi>10.50505/mrtest3</doi>
    	<collection property="list-based" multi-resolution="unlock" />
 	</doi_resources>
  </body>
</doi_batch>
```
* [Example of a multiple-resolution unlock resource-only deposit](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_mr_unlock-only_4.8.0.xml)

### Register secondary URLs <a id='00126' href='#00126'><i class='fas fa-link'></i></a>

When more than one URL is registered for a DOI, the DOI becomes a multiple resolution DOI. The primary URL is registered through a primary metadata deposit, but secondary URLs are typically submitted as a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/) by a secondary depositor. The secondary URL deposit consists of the DOI being updated, the secondary URL(s), and a label. No item-level metadata is required:

```
<doi_resources>
  <doi>10.50505/mrtest</doi>
     <collection property="list-based">
        <item label="SECONDARY_X">
            <resource>https://www.crossref.org/test1</resource>
        </item>
     </collection>
</doi_resources>
```

The *label* value is case-sensitive, the label must be a minimum of 6 characters (no spaces).

* [Example of a secondary URL resource-only deposit](/xml-samples/mr_secondary.xml)
* [Example of secondary URLs as part of a primary metadata deposit](/xml-samples/mr_full.xml)

#### Upload secondary URLs <a id='00127' href='#00127'><i class='fas fa-link'></i></a>

A secondary URL resource-only deposit must be uploaded with type *doDOICitUpload* for HTTPS POST (or *DOI Resources* when using the [admin tool](https://doi.crossref.org/). The secondary depositor must have permission to add URLs to the primary depositor's DOIs.

## How to update multiple resolution URLs <a id='00128' href='#00128'><i class='fas fa-link'></i></a>

If you are the primary depositor, the primary URL may be [updated in the standard way](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/). If you need to update a secondary URL you’ll need to re-send the secondary XML file to us with the updated URLs included. When updating, please note that the item *label* value and the depositor role must be consistent with those used in the previous update - this is how we know what URL to update.

## Reverse multiple resolution <a id='00129' href='#00129'><i class='fas fa-link'></i></a>

Multiple resolution can be reversed if the service is no longer needed for a DOI. When multiple resolution is reversed, the content owner should also *lock* the multiple resolution DOIs, preventing any future multiple resolution deposits.

To remove secondary URLs and lock DOIs, submit a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/) with a closed collection element, for example:

```
<?xml version="1.0" encoding="UTF-8"?>
<doi_batch version="4.3.0" xmlns="http://www.crossref.org/doi_resources_schema/4.3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/doi_resources_schema/4.3.0 http://www.crossref.org/schemas/doi_resources4.3.0.xsd">
<head>
 <doi_batch_id>123456</doi_batch_id>
 <depositor>
  <name>xyz</name>
  <email_address>xyz@crossref.org</email_address>
 </depositor>
</head>
<body>
 <doi_resources>
  <doi>10.50505/mrtest1</doi>
  <collection property="list-based" multi-resolution="lock" />
 </doi_resources>
</body>
</doi_batch>
```

## DOI resolution by country code <a id='00130' href='#00130'><i class='fas fa-link'></i></a>

Crossref's implementation of multiple resolution supports a form of appropriate copy based on the country of origin of the user requesting the resolution service. This service allows a content owner to deposit multiple URLs for a single DOI, each of which is intended to service users from a particular country. The DOI resolver will determine the resolution request's country of origin and select the appropriate URL target based on country codes (see [list](https://data.crossref.org/reports/help/schema_doc/4.4.2/JATS1.html#country).

The country code and URL information are supplied within `<collection>` (learn more about the [collection element](https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html)), and can be deposited as part of a primary metadata deposit or as a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/). If a country code is not supplied, the DOI will resolve to the URL supplied in the top level resource element.

### Metadata deposit example for multiple resolution <a id='00131' href='#00131'><i class='fas fa-link'></i></a>

```
<doi_data>
   <doi>10.5555/ilovedois</doi>
      <resource>https://www.crossref.org/hello</resource> default URL
      <collection property="country-based">
           <item country="US">
               <resource>https://www.crossref.org/howdy</resource> USA URL
            </item>
            <item country="SE">
               <resource>https://www.crossref.org/hej</resource> Sweden URL
            </item>
            <item country="KE">
               <resource>https://www.crossref.org/hujambo</resource> Kenya URL
            </item>
      </collection>
</doi_data>
```

### Resource-only deposit example for multiple resolution <a id='00132' href='#00132'><i class='fas fa-link'></i></a>

```
<doi_resources>
   <doi>10.5555/ilovedois</doi>
   <collection property="country-based">
     <item country="US">
       <resource>https://www.crossref.org/howdy</resource> USA URL
     </item>
     <item country="SE">
       <resource>https://www.crossref.org/hej</resource> Sweden URL
     </item>
     <item country="KE">
       <resource>https://www.crossref.org/hujambo</resource> Kenya URL
     </item>
   </collection>
</doi_resources>
```

## Role of the DOI proxy in multiple resolution <a id='00133' href='#00133'><i class='fas fa-link'></i></a>

The [DOI proxy](http://dx.doi.org/) is maintained by [CNRI](http://www.cnri.reston.va.us/) on behalf of the [IDF](https://www.doi.org/). Multiple resolution required the introduction of an additional Handle property for DOIs, called `10320/loc`, which is itself a Handle.

### Example Handle record <a id='00134' href='#00134'><i class='fas fa-link'></i></a>

<figure><img src='/images/documentation/Example-Handle-record.png' alt='Example Handle record' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image6">Show image</button>
<div id="image6" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Example-Handle-record.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


In the sample handle record the default URL is set to represent the content's primary location. This is typically the platform of the content owner, or its primary publisher. The presence of property `10320/loc`, containing an XML snippet, indicates to the proxy that multiple resolution is enabled for this DOI. The XML is interpreted as follows:

* `<locations>` element, `chooseby`: specifies the order of rules to be applied by the proxy when selecting from the `<location>` elements.
    * `locatt`: used if the DOI request specifies a specific location item
    * `country`: used if any location item specifies a specific country which must match the country of the requester
    * `weight`: a weighted random selection from those `<location>` elements having weight values
* `<location>` element identifies a specific location
    * `id`: a unique ID given to each location element
    * `cr_type`: a Crossref property that specifies the type of multiple resolution to support
    * `cr_src`: a Crossref property that identifies which user deposited the location value
    * `label`: used by us to identify the co-host
    * `href:` the URL of the location
    * `weight`: the weighted value to use when applying the weighted-random selection process

The presence or absence of a rule in the `chooseby` property will enable or disable that type of selection process by the proxy.

## What if I want to do multiple resolution but sometimes want to send people directly to my site? <a id='00135' href='#00135'><i class='fas fa-link'></i></a>

DOI resolution requests may be structured to bypass our interim page using features built into the proxy's multiple resolution capabilities.

You can bypass the interim page by appending a label parameter to your DOI link. To force the DOI to resolve to the primary (original) host location, add the `locatt=mode:legacy` parameter to the end of the URL, for example:

```
https://doi.org/10.50505/200806091300?locatt=mode:legacy
```

To force the DOI to resolve to a secondary URL, add `locatt=label:HOST-XYZ` to the end of the URL, where `HOST-XYZ` is the label supplied in the secondary URL deposit, for example:

```
https://doi.org/10.50505/200806091300?locatt=label:HOST-XYZ
```

Learn more about [the role of the DOI proxy in multiple resolution](#00133).

## How does multiple resolution affect my resolution statistics? <a id='00117' href='#00117'><i class='fas fa-link'></i></a>

A click on a multiple resolution DOI is still a single click, it’s just that the clicks will be coming from an interim page instead of the DOI resolver, and your resolution reports will reflect that.
