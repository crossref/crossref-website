+++
title = "Record registration form"
date = "2024-12-18"
draft = false
author = "Lena Stoll"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "record-registration-form"]
identifier = "documentation/register-maintain-records/record-registration-form"
aliases = [
    "/documentation/register-maintain-records/record-registration-form",
    "/documentation/register-maintain-records/record-management-tool",
    "/documentation/register-maintain-records/grant-registration-form"
]
rank = 4
weight = 50450
+++

The record registration form can be used to deposit metadata for your records. You do not need any knowledge of XML to use it. You can download your records to your local machine and re-upload them to the form later to make edits to the metadata. You can also save partial records to be used as templates in the future.

The form currently supports journal articles and grants, but we are planning to add support for additional record types in future.

## How to use the record registration form <a id='00100' href='#00100'><i class='fas fa-link'></i></a>

Start at the [record registration form](https://manage.crossref.org/records) and enter your [Crossref account credentials](/documentation/member-setup/account-credentials/). You can choose to create a new record or upload a record you’ve already created using this form. If this is the first time you’ve used this form, you’ll choose New Record.

<figure><img src='/images/documentation/record-form-start-submission.png' alt='Start a new record submission page' title='' width='85%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image117">Show image</button>
<div id="image117" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/record-form-start-submission.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

### Create a new record <a id='00200' href='#00200'><i class='fas fa-link'></i></a>

Select the type of record you wish to create, then add the metadata associated with your record. Some fields are required to be filled out in order to submit your record, while others are optional. If you are submitting a journal article, you can find links to our documentation in the form for more information on what each field means.

At any point while filling out the form, you can use the download button to save your record to your local computer for future edits. The record will download as a .json file, which is named automatically: for grant records, it will be named the funder name and award number; for journal article records, it will be named after the journal's e-ISSN (or p-ISSN if no e-ISSN is available) and article title. This file can be [loaded back into the form](/documentation/register-maintain-records/record-registration-form/#00400) at a later date to make changes to your record.

### Submit your record  <a id='00300' href='#00300'><i class='fas fa-link'></i></a>

Click Submit at the bottom of the form once you have filled out the required fields, as well as any [optional metadata](/documentation/principles-practices/best-practices/) you want to deposit. The submission will be made immediately and a success message will appear on the screen. You can also download the record from this page, or choose to start another submission. If you have submitted a journal article record, you can choose to repeat the process for another article in the same journal and/or journal issue, which will pre-fill the appropriate metadata for you so you don't have to re-enter it.

<figure><img src='/images/documentation/record-form-success.png' alt='Grant submission success page' title='' width='85%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image117">Show image</button>
<div id="image117" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/record-form-success.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

If there is a problem with your submission, you will see an error message appear instead. Go to [the documentation](/documentation/register-maintain-records/verify-your-registration/troubleshooting-submissions/) for tips on how to troubleshoot common errors from our deposit system.

### Load a saved record  <a id='00400' href='#00400'><i class='fas fa-link'></i></a>

If you’ve used the record registration form before to create a record, you can upload your saved copy to make edits and re-deposit the metadata. Start at the [record registration form](https://manage.crossref.org/records) and choose Load Record. Select the appropriate .json file from your computer and click Open. **Note**: the record you load must be a .json file previously downloaded from the record registration form.

Once the form is loaded, you can make edits to your record and submit your record to update the metadata. You can also download a new version to your local machine to repeat the process later.

### Create a template  <a id='00500' href='#00500'><i class='fas fa-link'></i></a>

You can partially complete a form and download it for use as a template in the future. For example, if you register multiple grants, your depositor information (name, email address) and funder information (funder name, funder ID) are likely to be the same across all submissions. So you might complete just those parts of the form, download the record, and upload it each time you need to submit a new grant record.
