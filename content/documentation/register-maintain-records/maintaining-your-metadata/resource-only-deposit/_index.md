+++
title = "Resource-only deposit"
date = "2022-08-01"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "maintaining-your-metadata", "resource-only-deposit"]
identifier = "documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit"
rank = 4
weight = 140103
aliases = [
  "/education/metadata-stewardship/maintaining-your-metadata/resource-only-deposit",
  "/education/metadata-stewardship/maintaining-your-metadata/resource-only-deposit/",
  "/education/metadata-stewardship/maintaining-your-metadata/resource-only-deposit/",
  "/education/metadata-stewardship/maintaining-your-metadata/resource-only-deposit"
]
+++

A *resource-only deposit* is a way of adding or updating certain elements in an existing metadata deposit without having to do a full metadata redeposit. Resource-only deposits use the [resource-only section of the schema](/documentation/schema-library/resource-only-deposit-schema-4-4-2/) (with the exception of stand-alone components which use the main [deposit section of the schema](/documentation/schema-library/schema-versions/)).

Whether you use a helper tool or submit your own XML to Crossref, you may find a resource-only deposit useful for adding the following:

* [References](/documentation/schema-library/markup-guide-metadata-segments/references/)
* [Funder information](/documentation/funder-registry/)
* [Crossmark](/documentation/crossmark/)
* [License information](/documentation/principles-practices/best-practices/license/)
* [Relationships](/documentation/principles-practices/best-practices/relationships/) between different research objects
* A [resolution URL](/documentation/schema-library/markup-guide-metadata-segments/full-text-urls/) must be included in all metadata records and cannot be updated using a resource-only deposit. However, the following additional URLs may be added using a resource-only deposit:
    * [Similarity Check full-text URLs](/documentation/similarity-check/participate#00040)
    * [multiple resolution secondary URLs](/documentation/content-registration/creating-and-managing-dois/multiple-resolution#00126)
    * [text and data mining URLs](/documentation/retrieve-metadata/rest-api/text-and-data-mining)

## Uploading resource-only deposits <a id='00174' href='#00174'><i class='fas fa-link'></i></a>

Resource-only deposits may only be submitted for existing Crossref DOIs. Deposits using the resource-only section of the schema must be uploaded with type `doDOICitUpload` for [HTTPS POST](/documentation/content-registration/direct-deposit-xml/https-post/), or *DOI Resources* if you are using the [admin tool](/documentation/content-registration/direct-deposit-xml/admin-tool/).
