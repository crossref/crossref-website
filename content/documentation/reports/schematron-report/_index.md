+++
title = "Schematron report"
date = "2024-07-19"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["reports", "schematron-report"]
identifier = "documentation/reports/schematron-report"
rank = 4
weight = 140210
aliases = [
  "/education/reports/schematron-report/",
  "/education/reports/schematron-report",
  "/education/metadata-stewardship/reports/schematron-report",
  "/education/metadata-stewardship/reports/schematron-report/",
  "/schematron/data"
]
+++

> #### A Schematron report tells you if there’s a metadata quality issue with your records.

[Schematron](http://www.schematron.com/) is a pattern-based XML validation language. We try to stop the deposit of metadata with obvious issues, but we can’t catch everything because publication practices are so varied. For example, most family names in our database that end with *jr* are the result of a publisher including a suffix (Jr) in a family name, but there are of course surnames ending with ‘jr’.

We do a weekly post-registration metadata quality check on all journal, book, and conference proceedings submissions, and record the results in the schematron report. If we spot a problem we’ll alert your technical contact **via email**. Any identified errors may affect overall metadata quality and negatively affect queries for your content. Errors are aggregated and sent out weekly via email in the schematron report.

## What should I do with my schematron report? <a id='00251' href='#00251'><i class='fas fa-link'></i></a>  

The report contains links (organized by title) to .xml files containing error details. The XML files can be downloaded and processed programmatically, or viewed in a web browser:

<figure><img src='/images/documentation/Schematron-report.png' alt='Schematron report' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image14">Show image</button>
<div id="image14" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Schematron-report.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Download Crossref's [Schematron rules for batch processing](https://gitlab.com/crossref/schema/raw/master/schematron/Crossref_Schematron.zip?inline=false)
