+++
title = "DOI error report"
date = "2024-07-19"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["reports", "doi-error-report"]
identifier = "documentation/reports/doi-error-report"
rank = 4
weight = 140205
aliases = [
  "/education/metadata-stewardship/reports/doi-error-report",
  "/education/metadata-stewardship/reports/doi-error-report/",
  "/education/reports/doi-error-report/",
  "/education/reports/doi-error-report"
]
+++

> #### The DOI error report is sent immediately when a user informs us that they’ve seen a DOI somewhere which doesn’t resolve to a website.

The DOI error report is used for making sure your DOI links go where they’re supposed to. When a user clicks on a DOI that has not been registered, they are sent to a form that collects the DOI, the user’s email address, and any comments the user wants to share. We compile the DOI error report daily using those reports and comments, and send it **via email** to the technical contact at the member responsible for the DOI prefix as a .csv attachment.

<figure><img src='/images/documentation/DOI-not-found.png' alt='DOI not found' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image8">Show image</button>
<div id="image8" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/DOI-not-found.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


 If you would like the DOI error report to be sent to a different person, please [contact us](/contact/).

The DOI error report .csv file contains (where provided by the user):

* DOI - the DOI being reported  
* URL - the referring URL  
* REPORTED-DATE - date the DOI was initially reported  
* USER-EMAIL - email of the user reporting the error  
* COMMENTS

We find that approximately 2/3 of reported errors are ‘real’ problems. Common reasons why you might get this report include:

* you’ve published/distributed a DOI but haven’t registered it
* the DOI you published doesn’t match the registered DOI
* a link was formatted incorrectly (a . at the end of a DOI, for example)
* a user has made a mistake (confusing *1* for *l* or *0* for *O*, or cut-and-paste errors)

## What should I do with my DOI error report? <a id='00236' href='#00236'><i class='fas fa-link'></i></a>

Review the .csv file attached to your emailed report, and make sure that no legitimate DOIs are listed. Any legitimate DOIs found in this report should be registered immediately. When a DOI reported via the form is registered, we’ll send out an alert to the reporting user (if they’ve shared their email address with us).

## I keep getting DOI error reports for DOIs that I have not published, what do I do about this? <a id='00237' href='#00237'><i class='fas fa-link'></i></a>

It’s possible that someone is trying to link to your content with the wrong DOI. If you do a web search for the reported DOI you may find the source of your problem - we often find incorrect linking from user-provided content like Wikipedia, or from DOIs inadvertently distributed by members to PubMed. If it’s still a mystery, please [contact us](/contact/).
