+++
title = "Datasets"
date = "2021-11-10"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "datasets"]
identifier = "documentation/principles-practices/datasets"
rank = 4
weight = 10104
aliases = [

]
+++

Dataset records capture information about one or more database records or collections.

When registering datasets, DO:
* register a DOI (or include a registered DOI) for a parent database - datasets must be registered as part of a collection
* include all relevant [funding](/documentation/principles-practices/best-practices/funding/), [license](/documentation/principles-practices/best-practices/license/), and [relationship](/documentation/principles-practices/best-practices/relationships/) metadata
* include all [contributors](/documentation/principles-practices/best-practices#contributors)
* include relevant dates (supported date types are creation, publication, and update dates)
* provide description, format, and citation metadata

Review our [Datasets Markup Guide](/documentation/schema-library/markup-guide/datasets) for XML and metadata help.
