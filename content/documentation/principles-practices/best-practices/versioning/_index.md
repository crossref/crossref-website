+++
title = "Version control, corrections, and retractions"
date = "2023-04-15"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "versioning"]
identifier = "documentation/principles-practices/versioning"
rank = 4
weight = 110107
aliases = [
	"/education/crossmark/version-control-corrections-and-retractions",
	"/education/crossmark/version-control-corrections-and-retractions/",
	"/documentation/crossmark/version-control-corrections-and-retractions/"
]
+++

Version control is the management of changes to a document, file, or dataset. Versions may include: draft, preprint, ending publication, author accepted manuscript (AAM), version of record (VOR), updated or corrected, and even retracted.

* Draft
* Preprint - early draft or manuscript shared by researcher in a preprint repository or dedicated channel (outside of a specific journal)
* Pending publication (PP) - a manuscript which has been accepted but has not yet been published online
* Advanced online publication or ahead of print (AOP) - early release of publication which publisher makes available to readers on their platform (prior to typesetting or before final published form)
* Author accepted manuscript (AAM) - accepted version which has been peer reviewed but not typeset or copyedited
* Version of record (VoR) - typeset, copyedited, and published version
* Updated - adding supplementary data or making corrections to the file, or its retraction.

Version control is important for:

* traceability (following the development of the document),
* identifiability (connecting documents to decisions, contributions, contributors, and time),
* clarity (distinguishing between multiple versions of documents, and identifying the latest version),
* reduced duplication (removing out-of-date versions), and
* reduced errors (clearly indicating to readers which is the current version).

## Reading list <a id='00326' href='#00326'><i class='fas fa-link'></i></a>

* [COPE](https://publicationethics.org/) Guidelines for retracting articles - part of the [COPE guidelines](https://publicationethics.org/resources/guidelines)
* [Journal Article Versions (JAV): Recommendations of the NISO/ALPSP JAV Technical Working Group](https://www.niso.org/publications/niso-rp-8-2008-jav) (opens .pdf file)
* [Errata, Retractions, and Other Linked Citations in PubMed](https://www.nlm.nih.gov/bsd/policy/errata.html)

## Publication stages and DOIs <a id='00327' href='#00327'><i class='fas fa-link'></i></a>

How do I decide if I should assign a DOI to a work, and at what stage? This table sets out seven publication stages of a research object (a publication such as a journal article, book, or dataset). A work may not go through all of these seven stages, so you only need to consider the stages relevant to your publication.

| Publication stage                                    | Eligible for a DOI?   | Which DOI? |
|------------------------------------------------------|-----------------------|------------|
| 1 Draft                                              | No DOI for draft item | n/a        |
| 2 Preprint                                           | Yes                   | DOI A      |
| 3 Pending publication \(PP\)                         | Yes                   | DOI B      |
| 4 Advanced online publication/ahead of print \(AOP\) | Yes                   | DOI B      |
| 5 Author accepted manuscript \(AAM\)                 | Yes                   | DOI B      |
| 6 Version of record \(VoR\)                          | Yes                   | DOI B      |
| 7 Updated                                            | Yes                   | DOI C      |

* A DOI should not be assigned to a draft (unpublished) work.
* A preprint should have its own DOI (DOI A).
* Accepted versions (including PP, AOP, AAM, and VoR) should have a separate DOI (DOI B). Establish a [relationship](/documentation/content-registration/structural-metadata/relationships/) between DOI B and DOI A to show the connection between them, such as DOI B "hasPreprint" DOI A.
* In the case of a significant change to the published version, a notice should be published explaining the correction/update/retraction. The updated version should have a new DOI (DOI C). Updates should only be deposited for changes that are likely to affect *the interpretation or crediting of the work* (editorially significant changes), and instead of simply asserting a relationship, these should be [recorded as updates](/documentation/register-maintain-records/maintaining-your-metadata/registering-updates/).

The metadata for the update is part of the Crossmark section of the metadata, and should include a link to the item being updated, and the type of update:

```
<updates>
<update type="retraction" label="Retraction" date="2009-09-14">10.5555/12345678</update>
</updates>
```

Note that you don't need to use all aspects of [Crossmark](/documentation/crossmark/) to register updates.
