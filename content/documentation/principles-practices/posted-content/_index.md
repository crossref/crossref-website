+++
title = "Posted content"
date = "2021-11-10"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "posted-content"]
identifier = "documentation/principles-practices/posted-content"
rank = 4
weight = 10112
aliases = [

]
+++
You can register records for [preprints and other posted material](/documentation/research-nexus/posted-content-includes-preprints/) using our posted content metadata model.

When registering posted content, DO:

* include the appropriate sub-type using the `type` attribute (preprint, working paper, letter, dissertation, report, other) - it's vital that preprints be identified as preprints
* include abstracts
* connect posted content to related items via [relationships](/documentation/principles-practices/best-practices/relationships/) - this is required for preprints and important for other posted content as well

Do not:
* register non-preprints with the "preprint" type

Other things to know:
* our [Preprint Advisory Group](/working-groups/preprints/) is actively creating new recommendations and best practices for preprint metadata records

Review our [Posted Content Markup Guide](/documentation/schema-library/markup-guide-record-types/posted-content-includes-preprints/) for XML and metadata help.
