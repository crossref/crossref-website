+++
title = "Metadata Plus keys"
date = "2023-08-29"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["metadata-plus", "metadata-plus-keys"]
identifier = "documentation/metadata-plus/metadata-plus-keys"
rank = 4
weight = 120000
+++

To access all of the features of Metadata Plus, you’ll need to create an API Key. Once you’ve subscribed to Plus, your technical contact will receive an email to set a password to access key manager. This is where you’ll create and manage API keys for Metadata Plus access. If you subscribed to Plus before August 2023, you'll be using a token.

### Create a new API Key

1. Login to [key manager](https://manage.crossref.org/keys)
2. Under API Keys, click “Add New”
3. Give the key a name (description)
4. Copy the API Key **(Note: The key will only be displayed once, so you must copy and paste it somewhere safe)**

### Delete an API Key

**Note: a deleted key cannot be recovered.**

1. Under API Keys, find the correct key and click the three dots to its right
2. Choose “Delete” from the drop down menu
3. Type DELETE in the modal and click ok. The key now cannot be used to access Plus services. The key cannot be recovered. It may take up to 60 seconds for this to propagate through the system.

### Edit a key’s description

1. Under API Keys, find the correct key and click the three dots to its right
2. Choose “Edit” from the drop down menu
3. Edit the key’s description and click ok. 

### Use an API Key

API Keys can be used to access Metadata Plus services. When making requests to the REST API (including for snapshots) or OAI-PMH, put your API Key (or other token) in the `Crossref-Plus-API-Token` HTTPS header of all your requests. The example below shows how this should be formatted, with XXX replaced by your key:

`Crossref-Plus-API-Token: Bearer XXX`

For full information on how to use the REST API, see the documentation at [api.crossref.org](https://api.crossref.org/).