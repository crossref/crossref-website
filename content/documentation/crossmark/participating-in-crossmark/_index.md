+++
title = "Participating in Crossmark"
date = "2024-08-09"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["crossmark", "participating-in-crossmark"]
identifier = "documentation/crossmark/participating-in-crossmark"
rank = 4
weight = 110200
aliases = [
    "/get-started/crossmark",
    "/education/crossmark/participating-in-crossmark",
    "/education/crossmark/participating-in-crossmark/"
]
+++

Implementing Crossmark includes several stages, some of which require technical knowledge to modify websites or PDFs.

Full implementation means that you'll need to include Crossmark-specific metadata when registering content and add the Crossmark button to your website and PDFs. If you are not able to finish the process, that's ok, make a start and continue when you have the expertise to do so.

On this page, learn more about:

* [Step one: Designate an update policy page and assign it a DOI](#00277)
* [Step two: Include the policy page in all your registered content](#00278)
* [Step three: Register published updates](#00279)
* [Step four: Implement Crossmark on your HTML pages](#00280)
* [Step five: Apply Crossmark to your PDF content](#00328)
* [Further options: adding more Crossmark information](#00329)

## Step one: Designate an update policy page and assign it a DOI <a id='00277' href='#00277'><i class='fa fa-link'></i></a>

You will need to explain to your readers how your content is updated after publication and indicate when this has happened. The first step is to have a single page on your website explaining these processes. This page should be registered and have a DOI to enable persistent linking. It must include your policies on corrections, retractions, withdrawals and other updates. It can include links to other relevant policies such as author submission guidelines and peer review guidelines, and may contain definitions and explanations of additional custom metadata fields you have used. You may already have a suitable page on your website, but don't forget to assign it a DOI and register the metadata with us.

Learn more about [creating an update policy page](/documentation/crossmark/crossmark-policy-page).

## Step two: Add the policy page DOI to all of your content <a id='00278' href='#00278'><i class='fa fa-link'></i></a>

It’s important to apply Crossmark to all of your current content, not just content that has updates. When an item is published, you don’t know if it will be updated in the future. Therefore, a researcher may download a PDF article today without a Crossmark button, and if the article is subsequently updated they have no way of knowing if their locally-saved version is still current. If you're using the Crossmark service, we expect you to display the Crossmark button on all your content, whether it has an update or not.

At a minimum, you will need to include the update policy page in each metadata record that you register. Here's how to do that via several registration methods:

### XML deposit

If you register content with us in XML format using either the admin tool or HTTPS POST, you can include Crossmark metadata in your initial deposit. You can also add Crossmark metadata to existing DOIs using a [resource-only-XML deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/). 

We provide sample a [sample XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_crossmark_4.8.0.xml) file with the fields you need to include.

### Using the web deposit form

Using the web deposit form you can register Crossmark metadata for journal articles.

<figure><img src='/images/documentation/Crossmark-web-deposit-form.png' alt='The web deposit form has an add Crossmark metadata button. Click it to open up fields to add the metadata.' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image44">Show image</button>
<div id="image44" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
   <div class="modal-dialog" data-dismiss="modal">
       <div class="modal-content">
           <div class="modal-body">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <img src="/images/documentation/Crossmark-web-deposit-form.png" class="img-responsive" style="width: 100%;">
           </div>
       </div>
   </div>
</div>

Fill in the 'Policy page DOI' field (and other fields if they are relevant). Note that Crossmark metadata for types other than journal articles (such as books or preprints) is not supported by the web deposit form.

### Metadata manager (deprecated)

Registering the update policy page is also possible using the [Metadata Manager](register-maintain-records/metadata-manager/) by adding the update policy page up at journal level. Go to the [journal-level record](/documentation/content-registration/metadata-manager/setting-up-a-new-journal-in-your-workspace) for your publication, add your Crossmark policy page DOI, and click *Save*.

<figure><img src='/images/documentation/Crossmark-Metadata-Manager.png' alt='Add Crossmark policy page DOI in Metadata Manager' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image44">Show image</button>
<div id="image44" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
   <div class="modal-dialog" data-dismiss="modal">
       <div class="modal-content">
           <div class="modal-body">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <img src="/images/documentation/Crossmark-Metadata-Manager.png" class="img-responsive" style="width: 100%;">
           </div>
       </div>
   </div>
</div>

The metadata will automatically be added to all content registered to this journal.

## Step three: Add metadata that reflects any updates to specific items <a id='00279' href='#00279'><i class='fa fa-link'></i></a>

If a registered item is updated, you need to register a different metadata record for the update. This is only necessary for editorially significant changes---those that are likely to affect the interpretation or crediting of the work, and where a separate update notice is usually published. Minor changes can be made directly to the content without notifying Crossref, including cases such as minor spelling corrections or formatting changes that don't affect the metadata.

There are 12 defined types of *update* accepted in our schema:

* addendum
* clarification
* correction
* corrigendum
* erratum
* expression_of_concern
* new_edition
* new_version
* partial_retraction
* removal
* retraction
* withdrawal

If an update does not fall into one of these categories, it should instead be placed in the *more information* section of the pop-up box in the web deposit form by being deposited as an *assertion*.

When deposited content corrects or updates earlier content, the DOI(s) of the corrected content must be supplied in the Crossmark metadata. See the [Crossref unixref documentation](https://data.crossref.org/reports/help/schema_doc/unixref1.0/unixref.html) section on *updates* for examples of how this is recorded in the Crossmark metadata.
When a correction is made in situ (that is, it replaces the earlier version completely), then the DOI of the corrected content will be the same as the DOI for the original Crossref deposit. In situ updates are not considered best practice as they obscure the scholarly record.

## Step four: Apply the Crossmark button to your HTML pages <a id='00280' href='#00280'><i class='fa fa-link'></i></a>

There are two options for applying Crossmark to your website.

1. Add the Crossmark logo with a link.
1. Install a JavaScript widget that creates a Crossmark popup.

These are explained in the following sections.

### Add a logo with a link

This is the simplest way to implement Crossmark on websites. Simply add a version of the Crossmark logo to the landing page for each of your registered items (usually the page where the abstract is shown) and link the logo to the Crossmark page of the relevant DOI.

There are several variations of the [Crossmark logo](http://crossmark.crossref.org/widget/v2.0/readme.html), for example you can use:

* [CROSSMARK_Color_square.eps](https://assets.crossref.org/logo/CROSSMARK_Color_square.eps)
* [CROSSMARK_Color_horizontal.eps](https://assets.crossref.org/logo/CROSSMARK_Color_horizontal.eps)

The link needs to be specific to each landing page. Here is an example:

[https://crossmark.crossref.org/dialog?doi=10.5555/abcdef&amp;domain=html&amp;date_stamp=2008-08-14](https://crossmark.crossref.org/dialog?doi=10.5555/abcdef&amp;domain=html&amp;date_stamp=2008-08-14)

* *doi* is the DOI of the content item
* *domain* tells the Crossmark system what kind of static content the link is coming from, and will change for different static formats (such as html, pdf, epub)
* *date_stamp* tells the Crossmark system the date on which a last *Major Version* of the PDF was generated. In most cases, this will be the date the article was published. However, when a member makes significant corrections to a PDF *in-situ* (no notice issued, and no new version of the work with a new DOI) then the *date_stamp* should reflect when the PDF was regenerated with the corrections. The system will then use the *date_stamp* in order to tell whether the reader needs to be alerted to updates or not. The *date_stamp* argument should be recorded in the form YYYY-MM-DD (learn more about [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html)).

The final result will look like this:

[<figure><img src='/images/documentation/Crossmark-check-for-updates.png' alt='Check for updates' title='' width='30%'></figure>](https://crossmark.crossref.org/dialog?doi=10.5555/abcdef&amp;domain=html&amp;date_stamp=2008-08-14)

Clicking the logo will open a new tab or window displaying the Crossmark information.

### A Crossmark popup

A different solution that is more technical to implement enables a popup containing Crossmark information. It has the advantage that readers do not leave your website when clicking the button.

We supply a templated HTML/JavaScript code widget which will embed the Crossmark button and functionality into your web pages. The latest version of the widget (v2.0) is below. Ensure you are using the latest version and that it points to our production server. Do not alter the script or host the button locally.

```
<!-- Start Crossmark Snippet v2.0 -->
<script src="https://crossmark-cdn.crossref.org/widget/v2.0/widget.js"></script>
<a data-target="crossmark"><img src="https://crossmark-cdn.crossref.org/widget/v2.0/logos/CROSSMARK_BW_horizontal.svg" width="150" /></a>
<!-- End Crossmark Snippet -->

```

Select one of the variations of the [Crossmark button](http://crossmark.crossref.org/widget/v2.0/readme.html) available. You can change the Crossmark button that is used simply by changing the `src` attribute of the `img` element to point to one of the following, for example:

* [CROSSMARK_Color_square.eps](https://assets.crossref.org/logo/CROSSMARK_Color_square.eps)
* [CROSSMARK_Color_horizontal.eps](https://assets.crossref.org/logo/CROSSMARK_Color_horizontal.eps)

Alternatively, check the source on [this page](http://crossmark.crossref.org/widget/v2.0/readme.html) to see the correct link for each style of button.

The button can be resized according to your design needs by changing the image width in the image tag but do follow the [Crossmark button guidelines](/documentation/crossmark/crossmark-button-guidelines).

The Crossmark popup needs to have a DOI to reference in order to pull in the relevant information. This needs to be embedded in the head of the HTML metadata for all content to which Crossmark buttons are being applied as follows:

`<meta name="dc.identifier" content="doi:10.5555/12345678"/>`

## Step five: Apply the Crossmark button to your PDF content <a id='00328' href='#00328'><i class='fa fa-link'></i></a>

To implement Crossmark in PDF files, the solution is very similar to that for the first website solution above: 

1. Select a [suitable Crossmark logo](http://crossmark.crossref.org/widget/v2.0/readme.html). 
1. Add the logo to your PDF files with a link to the correct Crossmark link, for example [https://crossmark.crossref.org/dialog?doi=10.5555/abcdef&amp;domain=pdf&amp;date_stamp=2008-08-14](https://crossmark.crossref.org/dialog?doi=10.5555/abcdef&amp;domain=pdf&amp;date_stamp=2008-08-14).

### Optional updates to PDF metadata

For additional transparency and to enable easier machine-reading of Crossmark metadata, you can modify the metadata of your PDFs. This is best done during production before the final PDF has been created and any security has been added to the document.

A minimal XMP file for the above PDF would look like this:

```
<?xml version="1.0" encoding="UTF-8"?>
<?xpacket begin="?" id="W5M0MpCehiHzreSzNTczkc9d"?>
<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 4.0-c316 44.253921, Sun Oct 01 2006 17:14:39">
<rdf:RDF xmlns:rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:pdfx = "http://ns.adobe.com/pdfx/1.3/"
xmlns:pdfaid = "http://www.aiim.org/pdfa/ns/id/"
xmlns:xap = "http://ns.adobe.com/xap/1.0/"
xmlns:xapRights = "http://ns.adobe.com/xap/1.0/rights/"
xmlns:dc = "http://purl.org/dc/elements/1.1/"
xmlns:dcterms = "http://purl.org/dc/terms/"
xmlns:prism = "http://prismstandard.org/namespaces/basic/2.0/"
xmlns:crossmark = "http://crossref.org/crossmark/2.0/">
<rdf:Description rdf:about="">
<dc:identifier>doi:10.5555/12345678</dc:identifier>
<prism:doi>10.5555/12345678</prism:doi>
<prism:url>http://dx.doi.org/10.5555/12345678</prism:url>
<crossmark:MajorVersionDate>2015-08-14</crossmark:MajorVersionDate>
<crossmark:DOI>10.5555/12345678</crossmark:DOI>
<pdfx:doi>10.5555/12345678</pdfx:doi>
<pdfx:CrossmarkMajorVersionDate>2015-08-14</pdfx:CrossmarkMajorVersionDate>
</rdf:Description>
</rdf:RDF>
</x:xmpmeta>
<?xpacket end="w"?>
```

It may appear redundant to apply Crossmark elements both in their own *Crossmark* namespace as well in the *pdfx* namespace, but the latter is necessary to ensure the Crossmark elements appear in the PDF dictionary, a specific requirement for some search engines. Any metadata found in the *pdfx* namespace will be copied over to the document info dictionary. Simply make sure that Crossmark metadata is in the *pdfx* namespace in the XMP provided to the tool.

## Further options: adding more information to the Crossmark button <a id='00329' href='#00329'><i class='fas fa-link'></i></a>

The Crossmark box has a section for you to show any additional non-bibliographic information about the content. You decide what to include here, and you are not required to add anything. In this section, Crossmark participants often include publication history dates, details of the peer review process used, and links to supporting information.

Use Metadata Manager to add custom metadata, or use the [assertion element](http://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html#assertion) in your XML.

Several metadata elements will automatically display in the Crossmark box if you are registering them:

* Author names and their ORCID iDs (learn more about [contributors](/documentation/content-registration/descriptive-metadata/contributors/))
* Funding information (learn more about [funding information](/documentation/schema-library/markup-guide-metadata-segments/funding-information/))
* License URLs (learn more about [license information](/documentation/content-registration/administrative-metadata/license-information/))

If you are already registering this additional metadata at the time you implement Crossmark, there is nothing more you need to do. If you start to register these metadata elements after you have set up Crossmark, they will automatically be put into the Crossmark box.

Please note that @order is an optional attribute. If @order is absent, it will return results in the order in which you list them in your deposit, but this is not guaranteed. If you want to be *sure* of the order, then you can use @order. Learn more about the Crossmark deposit elements (including what is optional) in the [schema](https://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html).
