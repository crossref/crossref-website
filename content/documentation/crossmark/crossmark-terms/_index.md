+++
title = "Crossmark terms"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["crossmark", "crossmark-terms"]
identifier = "documentation/crossmark/crossmark-terms"
rank = 4
weight = 110800
aliases = [
  "/education/crossmark/crossmark-terms",
  "/education/crossmark/crossmark-terms/"
]
+++

1. CROSSMARK® SERVICE. Crossmark is an optional service for Crossref members in good standing. Anything assigned a Crossref DOI that the publisher is taking responsibility for and stewarding can be registered in the Crossmark system.
2. OBLIGATIONS OF CROSSMARK PARTICIPANTS. Crossref members participating in the Crossmark Service ("Participating Publishers") will be obligated to:

   1. Include the Crossmark button as a clickable link in digital formats (HTML, PDF, and, at the Participating Publishers option, ePub) of the abstract and full text of all current content deposited at Crossref by the Participating Publisher. For purposes of these terms and conditions, "current content" is defined as content published after the date that the Participating Publisher begins to participate in the CrossMark system. The Participating Publisher may include the Crossmark button in content, published prior to that date. In such event, the button must be included in the HTML version of the abstract and full text (and the ePub file if relevant). Inclusion in the PDF versions of previously published research outputs is encouraged but not required.
   2. Maintain the content and register as promptly as reasonably possible any major updates in the Crossmark system, which updates shall include at a minimum corrections, retractions and withdrawals and other updates that have an impact upon the crediting or interpretation of the work.
   3. Comply with the guidelines for the use of the Crossmark button issued from time to time by Crossref. Learn more about the [Crossmark button guidelines](/documentation/crossmark/crossmark-button-guidelines)
3. METADATA. For each content item registered in the Crossmark system, Participating Publishers must deposit the minimum Crossmark metadata and keep it up-to-date. Participating Publishers may deposit additional optional metadata and must keep it up-to-date. Crossref will make all Crossmark metadata deposited by Participating Publishers openly available from time-to-time in standard formats for harvesting at no charge, and the Participating Publisher hereby gives Crossref permission to release the data for such purposes.
4. OBLIGATIONS AFTER PARTICIPATION IN CROSSMARK CEASES. If a Participating Publisher stops participating in the Crossmark service for any reason use of the Crossmark logo must stop and the logo must be removed from all content to the extent practicable. All metadata deposited through participation in the Crossmark system will remain in the databases maintained by Crossref and may be used by Crossref as provided in Section 3 above. In the event that either the cessation of participation in Crossmark by a Participating Publisher or the transfer by the Participating Publisher of ownership of a content item to a non-participating publisher results in the failure to maintain the Crossmark metadata, Crossref may add language to the Crossmark status message indicating that the data provided may no longer be up-to-date.
5. PROMOTION OF CROSSMARK SERVICE. The Participating Publisher agrees to use reasonable commercial efforts to promote awareness of the Crossmark Service within the scholarly community (i.e. among scholars, researchers and librarians), and Crossref will, upon request, provide the Participating Publisher with digital and print marketing materials for use in such promotional activities.
6. LICENSE TO USE THE CROSSMARK BUTTON.

   4. In consideration of its agreement to abide by these Terms and Conditions, the Participating Publisher is hereby granted a limited, non-exclusive, non-transferable license to include the Crossmark button with its content, on the terms and conditions and for the purposes set forth herein. The Participating Publisher may give permission to a third party to display the Crossmark button in connection with the content provided that the Participating Publisher conditions such permission on the third-party’s agreement that it will not remove or alter the button.
   5. The Participating Publisher must display the Crossmark button on all formats of the abstract and full text of at least one publisher-maintained copy of current Crossmark registered content.
7. BILLING. There is no annual service fee for Crossmark.
