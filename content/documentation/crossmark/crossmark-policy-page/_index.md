+++
title = "Update policy page"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["crossmark", "update-policy-page"]
identifier = "documentation/crossmark/udpate-policy-page"
rank = 4
weight = 110300
aliases = [
	"/education/crossmark/crossmark-policy-page",
	"/education/crossmark/crossmark-policy-page/",
	"documentation/crossmark/crossmark-policy-page"
]
+++

To participate in Crossmark, you must create an update policy page on your website that has been assigned a DOI and registered with us. You may choose to have one policy page for all of your titles, or a separate policy page for each title.

We recommend that the following appear on the update policy page. These are guidelines only, there may be variations due to common practice in your field or region:

* A link to, or description of, editorial policies. How is content reviewed prior to publication? Are there guidelines for reviewers or editors?
* Link to any ethical guidelines or standards authors should adhere to.
* How can readers report potential issues with published content?
* Under what circumstances works might be updated or retracted after publication?
* What happens to retracted or corrected works? Are updated publications replaced, does the original remain available?

You may add the information directly to the registered page or link to additional pages that contain the details, for example you may already have a separate page about how retractions are handled.

If you have fully implemented Crossmark, you may also include (or adapt) the following text:

<blockquote><a href='https://crossref.org/documentation/crossmark'>Crossmark</a>, from Crossref, provides a standard way for readers to locate the current version of a piece of content. By clicking the Crossmark button, readers can determine whether changes have been made after publication.</blockquote>

## Depositing Crossmark policy page(s) <a id='00333' href='#00333'><i class='fas fa-link'></i></a>

Crossmark policy pages should be deposited as datasets with a "database" called "PublisherName Crossmark Policy Statement". If you have multiple policy pages (for example, different policy pages for different journals) you should include them in the database deposit as multiple datasets.

See an example of [a member’s Crossmark policy page](https://doi.org/10.12688/f1000research.crossmark-policy) (section *10 Permanency of content*).