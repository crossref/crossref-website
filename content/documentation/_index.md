+++
title = "Documentation"
date = "2023-02-07"
draft = false
author = "Amanda Bartell"
image = "/images/banner-images/documentation-pipes.jpg"
maskcolor = "crossref-darkblue"
type = "documentation"
educationfront = true
aliases =[
    "/education",
    "/education/",
    "/help",
    "/help/",
    "/docs",
    "/docs/",
    "/support",
    "/support/", 
    "/faq/",
    "/faq",   
    "/faqs",
    "/faqs/"
    ]
[menu.main]
weight = 3
+++

Take a look at the topics on the right to browse and page through our documentation, or search using the box above the topic list {{< icon name="fa-arrow-right" color="crossref-darkgrey" size="small" >}}

---

## Common questions; how do I...?

### {{< icon style="fas" name="fa-link" color="crossref-yellow" >}}&nbsp;[Construct DOI suffixes](/documentation/member-setup/constructing-your-dois)

### {{< icon style="fas" name="fa-check" color="crossref-blue"  >}}&nbsp;[Verify a metadata registration](/documentation/content-registration/verify-your-registration)

### {{< icon style="fas" name="fa-pencil" color="crossref-red" >}}&nbsp;[Update an existing metadata record](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/)

### {{< icon style="fas" name="fa-align-left" color="crossref-green"  >}}&nbsp;[Interpret and act on reports](/documentation/reports/)

### {{< icon style="fas" name="fa-exchange" color="crossref-orange"  >}}&nbsp;[Query the API to retrieve metadata](/documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-api/)

### {{< icon style="fas" name="fa-exclamation-circle" color="crossref-darkgrey" >}}&nbsp;[See system status and maintenance](https://status.crossref.org)

<br>

---
If you have questions please consult other users on our forum at [community.crossref.org](https://community.crossref.org) or [open a ticket with our technical support team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) where we'll reply within a few days. Please also visit our [status page](https://status.crossref.org/) to find out about scheduled (and unscheduled) maintenance and subscribe to updates.
