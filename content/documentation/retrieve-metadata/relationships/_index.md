+++
title = "Relationships"
date = "2024-04-11"
draft = true
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata"]
aliases = [
	"/education/event-data",
	"/education/event-data/"
]
[menu.documentation]
identifier = "documentation/retrieve-metadata/event-data"
parent = "Documentation"
rank = 4
weight = 550000
+++


{{< snippet "/_snippet-sources/relationships.md" >}}

## How does the relationships endpoint work? <a id='00151' href='#00151'><i class='fas fa-link'></i></a>

We collect metadata from our members, Event Data, and selected other sources. When we find a connection between two items (a research output, organization, or person), we represent it as a relationship with the following information:
 - Subject (source of the relationship)
 - Object (target of the relationship)
 - Relationship type
 
We also provide metadata about when the relationship was last updated and which organizations deposited metadata about the relationship.

## How to use the relationships endpoint <a id='00152' href='#00152'><i class='fas fa-link'></i></a>

You can access the results in JSON format [from our REST API](https://api.crossref.org/beta/relationships).

Query filters are available to look for specific items, types of items, types or relationships, time ranges, and source organizations. For a full range of queries see the beta endpoint [Swagger documentation](http://api.crossref.org/beta/swagger-ui/index.html).

The relationships endpoint is optimized for specific types of queries. This means that certain combinations of query filters will return results quickly whereas others (particularly when combining more than one filter) will take much longer.

The following cases are currently supported:

| Query parameter combinations | Example use cases |
|--- |--- |
| `relationship-type`, `object.registration-agency`, `from-update-time`, `until-update-time` | Collect citations to DOIs from a different registration agency, e.g. DataCite. |
| `relationship-type`, `asserted-by`, `object.type`, `from-update-time`, `until-update-time` | Data citations deposited by a Crossref member. |
| `relationship-type`, `from-update-time`, `until-update-time` | Find all relationships of some type in some timeframe, e.g. citations, preprint to article links, members of Crossref. |
| `object.steward`, `asserted-by`, `object.type`, `from-update-time`, `until-update-time` | Find information added by Crossref or another organization to a member’s metadata records. |
| `subject.steward`, `asserted-by`, `object.type`, `from-update-time`, `until-update-time` | Find information added by Crossref or another organization to a member’s metadata records. |
| `subject.steward`, `relationship-type`, `object.type`, `from-update-time`, `until-update-time` | Data citations from a Crossref member’s works (deposited by the member or another organization). |

Relationships are provided via a freely available public API. This makes it suitable for post-processing and creating downstream tools such as search interfaces or dashboards. We do not curate the metadata we receive or provide plugins and data analytics.

Note that counts of references are based on our member metadata and differ from other services; see [this Community Forum post](https://community.crossref.org/t/did-someone-miss-a-citation/3164) for further background. 