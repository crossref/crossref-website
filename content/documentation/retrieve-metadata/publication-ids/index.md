+++
title = "Publication IDs"
date = "2022-01-19"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "publication-ids"]
identifier = "documentation/retrieve-metadata/publication-ids"
rank = 4
weight = 31101
aliases = [

]
+++

## Publication IDs <a id='00033' href='#00033'><i class='fas fa-link'></i></a>

Every publication in our system is assigned a unique publication ID. These are used mostly for internal purposes, but may be useful when retrieving data in bulk or identifying a specific title. Publication IDs may be retrieved using OAI-PMH, or from the browsable title list.

### Find publication IDs using an OAI-PMH request <a id='00034' href='#00034'><i class='fas fa-link'></i></a>

An OAI-PMH ListSets request will return titles and publication IDs for journals, books, conference proceedings, and series-level data:

* Journal data [http://oai.crossref.org/OAIHandler?verb=ListSets](https://oai.crossref.org/OAIHandler?verb=ListSets)
* Non-journal data [http://oai.crossref.org/OAIHandler?verb=ListSets&set=B](https://oai.crossref.org/OAIHandler?verb=ListSets&set=B)

*J* (journal) is the default set, *set=B* must be specified to retrieve book or conference proceeding titles, and *S* for series-level titles. Sets may be further limited by member prefix. Learn more about [OAI-PMH](/documentation/retrieve-metadata/oai-pmh).

The publication ID is listed within the `<setspec>` element, after the set and member prefix. For example, within the following set, `24` is the publication ID for *Journal of Clinical Psychology*:

``` XML
<set>
   <setSpec>J:10.1002:24</setSpec>
   <setName>Journal of Clinical Psychology</setName>
</set>
```

### Find publication IDs using the browsable title list <a id='00035' href='#00035'><i class='fas fa-link'></i></a>
The [browsable title list](https://www.crossref.org/titleList/) includes the publication ID next to each title in the search results. Select the <img src="/images/documentation/Icon-ID.png" alt="Purple id icon" height="23" > icon to reveal the ID. For most purposes, publication IDs are always preceded by the publication type (*J*, *B*, or *S* for journal, book, or series).
