+++
title = "OAI-PMH"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "oai-pmh"]
identifier = "documentation/retrieve-metadata/oai-pmh"
rank = 4
weight = 30500
aliases = [
  "/education/retrieve-metadata/oai-pmh",
  "/education/retrieve-metadata/oai-pmh/"
]
+++

We operate an OAI-PMH service for the distribution of metadata in XML. This system is based on the OAI-PMH version 2 repository framework and implements the interface [as documented here](http://www.openarchives.org/OAI/openarchivesprotocol.html).

The service interface can be used in different ways by public metadata users, Metadata Plus subscribers, and Crossref members.

* Public metadata users: we allow public access to two OAI verbs, *ListSets* and *ListIdentifiers*, which allow for discovery of available information.
* Metadata Plus subscribers: access to OAI verbs *GetRecord* and *ListRecords* require a subscription to our [Metadata Plus](/documentation/metadata-plus/) service. Users of this service are provided with tokens to identify them. Tokens are placed in HTTPS Authorization headers as:

```
Crossref-Plus-API-Token: Bearer FullTokenHere
```

* Crossref members may also use OAI-PMH to retrieve their deposited metadata using our [deposit harvester](/documentation/retrieve-metadata/deposit-harvester) using their member account.

## Set hierarchy <a id='00395' href='#00395'><i class='fas fa-link'></i></a>

We support selective harvesting according to sets defined by the hierarchy of publisher and title. Setspecs are formatted as follows:

* *record type*:*prefix*:*pubID* (learn more about [publication IDs](/documentation/retrieve-metadata/xml-api/retrieving-publication-ids/)) (for example: J:10.1002:4 = Journal content by the publisher Wiley, journal title *Applied Organometallic Chemistry*)
* record type:prefix (for example: J:10.1002, journals owned by publisher Wiley)

The *from* and *until* dates in a request capture when a record was deposited or updated, not the published date of the item. This means a request for records from yesterday through today will return all records added or changed between then and now, regardless of the publication dates included in the records.

Set record types are:
* *J* for journals
* *B* for books, conference proceedings, dissertations, reports, and datasets
* *S* for series

The default set for both *ListIdentifiers* and *ListRecords* is *J* (journals). A set (*B* for books or conference proceedings, *S* for series) must be specified to retrieve non-journal data.

With the *ListSets* request the *set* parameter is optional. Leaving off the *set* parameter will return only journal data which includes a list of publishers, their journal titles, and each year of publication for which we have metadata records.

With the *ListIdentifiers* request the *set*, *from*, and *until* parameters are optional. The *from* and *until* parameters are used to specify dates when the DOIs were registered with us and not the publication date.

## Examples of requests <a id='00396' href='#00396'><i class='fas fa-link'></i></a>

Request a list of DOIs registered since 2010-08-11:

```
https://oai.crossref.org/oai?verb=ListIdentifiers&metadataPrefix=cr_unixsd&from=2010-08-11
```

Request all journal sets:

```
https://oai.crossref.org/oai?verb=ListSets&set=J
```

Request all sets with record type 'B':

```
https://oai.crossref.org/oai?verb=ListSets&set=B
```

Request records for title '98765' with prefix 10.1234 registered or updated on 2017-07-06:

```
https://oai.crossref.org/oai?verb=ListRecords&metadataPrefix=cr_unixsd&set=J:10.1234:98765&from=2017-07-06&until=2017-07-06
```

## Best practice for performance <a id='00397' href='#00397'><i class='fas fa-link'></i></a>

We allow 3 concurrent initial OAI-PMH requests per user. There is no concurrency limit for follow-on requests (requests made with a [resumption token)](/documentation/retrieve-metadata/oai-pmh#00398). Due to the size of the repository, it is highly discouraged to perform a *ListRecords* action for the entire collection.

The best possible performance is had by requesting the changes made to one publication on a given date, such as:

```
https://oai.crossref.org/oai?verb=ListRecords&metadataPrefix=cr_unixsd&set=J:10.1234:98765&from=2017-07-06&until=2017-07-06
```

If you are harvesting a large amount of data and run up against our 3 concurrent initial request limitation, it is recommended that you request data by prefix for a short time-frame (days to a week). For example, this request will give you all journal records owned by prefix 10.1234 registered or updated between 2017-07-06 and 2017-07-09 :

```
https://oai.crossref.org/oai?verb=ListRecords&metadataPrefix=cr_unixsd&set=J:10.1234&from=2017-07-06&until=2017-07-09
```

## Using resumption tokens with OAI-PMH <a id='00398' href='#00398'><i class='fas fa-link'></i></a>

Many OAI requests are too big to be retrieved in a single transaction. If a given response contains a resumption token, you must make an additional request to retrieve the rest of the data. Resumption tokens remain viable for 48 hours.

The resumption token includes an expiry date of 48 hours:

```
<resumptionToken expirationDate="2015-10-28T00:00:00">c6cafedc-ef48-42a3-847c-b682dc58b617</resumptionToken>
```

The token should be appended to the end of the next request:

```
https://oai.crossref.org/oai?verb=ListSets&set=J:10.1007&resumptionToken=c6cafedc-ef48-42a3-847c-b682dc58b617
```

## Snapshots - part of our Metadata Plus service <a id='00399' href='#00399'><i class='fas fa-link'></i></a>

[Metadata Plus snapshots](/documentation/metadata-plus/metadata-plus-snapshots) provide access to our `160,104,382`-plus metadata records in a single file, providing an easy way to retrieve an up-to-date copy of our records. Snapshots are available for [Metadata Plus](/documentation/metadata-plus/) service users.

The files are made available via a */snapshots* route in the REST API which offers a compressed .tar file (tar.gz) containing the full extract of the metadata corpus in either JSON or XML formats.

## OAI-PMH example files <a id='00400' href='#00400'><i class='fas fa-link'></i></a>

An example application for harvesting Crossref OAI data

* [HTTPClient.jar](/documentation-files/oai-pmh-request/HTTPClient.jar)
* [oaipmhRequest.class](/documentation-files/oai-pmh-request/oaipmhRequest.class)
* [oaipmhRequest.java](/documentation-files/oai-pmh-request/oaipmhRequest.java)
* [oaipmhRequest$FullParser.class](/documentation-files/oai-pmh-request/oaipmhRequest$FullParser.class)
* [oaipmhRequest$SAXError.class](/documentation-files/oai-pmh-request/oaipmhRequest$SAXError.class)

Example ListIdentifiers, ListSets, and ListIdentifiers responses

* [cmsEnhanced_ListIdentifiers.xml](/documentation-files/oai-pmh-samples/cmsEnhanced_ListIdentifiers.xml)
* [cmsEnhanced_ListRecords.xml](/documentation-files/oai-pmh-samples/cmsEnhanced_ListRecords.xml)
* [cmsEnhanced_ListRecords.xml](/documentation-files/oai-pmh-samples/cmsEnhanced_ListRecords.xml)
