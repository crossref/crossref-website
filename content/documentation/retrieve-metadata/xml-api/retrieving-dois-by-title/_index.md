+++
title = "Retrieving DOIs by title"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "retrieving-dois-by-title"]
identifier = "documentation/retrieve-metadata/xml-api/retrieving-dois-by-title"
rank = 4
weight = 31012
aliases = [
  "/education/retrieve-metadata/xml-api/retrieving-dois-by-title",
  "/education/retrieve-metadata/xml-api/retrieving-dois-by-title/"
]
+++

A list of DOIs by title (top-level title, such as journal or book title, not article- or chapter-level title) may be retrieved using the following format:
```
https://doi.crossref.org/search/doi?pid={email@address.com}&format=doilist&pubid=record type pubid
```

where:

* `pid` = your email address
* `record type` = *J* for journals, *B* for books or conference proceedings, *S* for series
* `pubid` = [publication ID](/documentation/retrieve-metadata/xml-api/retrieving-publication-ids/)

For example:
```
https://doi.crossref.org/search/doi?pid={email@address.com}&format=doilist&pubid=J173705
```

The results returned match the title detail results from the [depositor report](/documentation/reports/depositor-report), and include a list of all DOIs for the title, the owner prefix for each DOI, the timestamp used in the most recent deposit, and the data the DOI was last updated.
