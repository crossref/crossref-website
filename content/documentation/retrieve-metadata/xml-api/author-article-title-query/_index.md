+++
title = "Author/article title query"
date = "2020-04-08"
draft = false
type = "documentation"
author = "Martyn Rittman"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "author-article-title-query"]
identifier = "documentation/retrieve-metadata/xml-api/author-article-title-query"
rank = 4
weight = 31007
aliases = [
  "/education/retrieve-metadata/xml-api/author-article-title-query",
  "/education/retrieve-metadata/xml-api/author-article-title-query/"
]
+++

We support a query mode where only article title and/or first author surname are required. These queries can be performed using an XML query.

## Example: XML author/article-title query <a id='00445' href='#00445'><i class='fas fa-link'></i></a>

```
<query enable-multiple-hits="false" secondary-query="author-title" key="key1">
  <article_title match="fuzzy">Concluding remarks</article_title>
   <author search-all-authors="true">Somiari</author>
</query>
<query enable-multiple-hits="false" secondary-query="author-title" key="key1">
  <article_title match="fuzzy">Off-line Approaches</article_title>
   <author search-all-authors="true">Gustafsson</author>
</query>
```

## Author/article title query tips <a id='00446' href='#00446'><i class='fas fa-link'></i></a>

* As with other metadata queries, if the system finds more than one possible match, the results are considered ambiguous, and no results are returned. You can override the one result rule and [request that multiple hits may be returned](/documentation/retrieve-metadata/xml-api/allowing-multiple-hits).
* When performing XML queries, an author-title query may be submitted as a [secondary query](/documentation/retrieve-metadata/xml-api/secondary-queries) if the initial full metadata query is unsuccessful.
* Only the `<author>` and `<article_title>` elements should appear in an author/article title query - if other elements are present, the query will be performed as a full metadata query.
