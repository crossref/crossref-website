+++
title = "Querying for books"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "querying-for-books"]
identifier = "documentation/retrieve-metadata/xml-api/querying-for-books"
rank = 4
weight = 31005
aliases = [
  "/education/retrieve-metadata/xml-api/querying-for-books",
  "/education/retrieve-metadata/xml-api/querying-for-books/"
]
+++

Querying for a *book* rather than a *book chapter* sometimes needs extra attention. Here are some examples.

## Book chapter queries <a id='00442' href='#00442'><i class='fas fa-link'></i></a>

Searching for individual chapters within a book may be done by using just the author name and chapter title (author name is optional but should be included for better results). This method is less exact than including book-level metadata in a query:

```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch version="2.0" xmlns = "https://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<head>
<email_address>someone@crossref.org</email_address>
<doi_batch_id>SomeTrackingID2</doi_batch_id>
</head>
<body>
<query key="MyKey1" enable-multiple-hits="false">
<author>Casteilla</author>
<article_title>Choosing an Adipose Tissue Depot for Sampling </article_title>
</query>
</body>
</query_batch>
```

This query contains a page number and author of a specific chapter as well as book metadata. It will return the chapter with author *Casteilla* and page *1*:

```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch version="2.0" xmlns = "https://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<head>
<email_address>someone@crossref.org</email_address>
<doi_batch_id>SomeTrackingID2</doi_batch_id>
</head>
<body>
<query key="MyKey1" enable-multiple-hits="false">
<author>Casteilla</author>
<volume>155</volume>
<first_page>1</first_page>
<year>2001</year>
<isbn>1-59259-231-7</isbn>
<volume_title>Adipose Tissue Protocol</volume_title>
</query>
</body>
</query_batch>
```

## Book title queries <a id='00443' href='#00443'><i class='fas fa-link'></i></a>

The deposit from the previous example also created a DOI for the book itself which can be found with a query containing the book's editor (in the `<author>` element):

```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch version="2.0" xmlns = "https://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<head>
<email_address>someone@crossref.org</email_address>
<doi_batch_id>SomeTrackingID2</doi_batch_id>
</head>
<body>
<query key="MyKey1" enable-multiple-hits="false">
<author> Ailhaud</author>
<volume>155</volume>
<first_page>1</first_page>
<year>2001</year>
<isbn>1-59259-231-7</isbn>
<volume_title>Adipose Tissue Protocol</volume_title>
</query>
</body>
</query_batch>
```

Many title-level book DOIs do not have author information deposited. If you do not have author information to include in your query or you are querying for an authorless book, for best results your query should instruct the system to ignore author by setting the `author match` attribute to *null*.

```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0" xmlns="https://www.crossref.org/qschema/2.0"xsi:schemaLocation="https://www.crossref.org/qschema/2.0 https://www.crossref.org/qschema/crossref_query_input2.0.xsd">
<head>
<email_address>test@crossref.org</email_address>
<doi_batch_id>test</doi_batch_id>
</head>
<body>
<query key="555-555" >
<isbn>9780387791456</isbn>
<journal_title>Ordinary and Partial Differential Equations</journal_title>
<year>2009</year>
<author match="null"/>
</query>
</body>
</query_batch>
```
