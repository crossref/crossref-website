+++
title = "Retrieving DOI info-metadata"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "retrieving-doi-info-metadata"]
identifier = "documentation/retrieve-metadata/xml-api/retrieving-doi-info-metadata"
rank = 4
weight = 31011
aliases = [
  "/education/retrieve-metadata/xml-api/retrieving-doi-info-metadata",
  "/education/retrieve-metadata/xml-api/retrieving-doi-info-metadata/"
]
+++

Crossref-specific information about a DOI may be retrieved by including the appropriate parameters in the following URL:

```
https://doi.crossref.org/search/doi?pid={email@address.com}&format=info&doi=doi
```

This data is distinct from item metadata deposited for each DOI, and includes information such as timestamp, owner prefix, and primary/alias status.

## Example info-metadata <a id='00451' href='#00451'><i class='fas fa-link'></i></a>

DOI: 10.2353/jmoldx.2009.090037
CITATION-ID: 39306481
JOURNAL-TITLE: The Journal of Molecular Diagnostics
JOURNAL-CITE-ID: 58207
BOOK-CITE-ID:
SERIES-ID:
DEPOSIT-TIMESTAMP: 20110701073812000
OWNER: 10.1016
LAST-UPDATE: 2011-07-04 21:13:59
PRIME-DOI: none
