+++
title = "Simple Text Query"
date = "2020-10-06"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "simple-text-query"]
identifier = "documentation/retrieve-metadata/simple-text-query"
rank = 4
weight = 30300
aliases = [
  "/education/retrieve-metadata/simple-text-query",
  "/education/retrieve-metadata/simple-text-query/"
]
+++

The Simple Text Query form allows you to retrieve DOI names for journal articles, books, and chapters by cutting and pasting a reference or reference list into the query box. References are entered as a standard bibliographic entry, such as:

```
Clow GD, McKay CP, Simmons Jr. GM, and Wharton RA, Jr. 1988. Climatological observations and predicted sublimation rates at Lake Hoare, Antarctica. Journal of Climate 1:715-728.
```

For best results, each reference should appear on a single line. When submitting multiple references, you can enter them in alphabetical order, or as a numbered list. Separate each reference using a blank line.

## Using Simple Text Query to match references with DOIs <a id='00392' href='#00392'><i class='fas fa-link'></i></a>

If you are a member and want to match and deposit references, please see [using Simple Text Query to add references](/documentation/register-maintain-records/maintaining-your-metadata/add-references#00176). If you just want to match references to DOIs, follow these instructions.

1. Go to the [Simple Text Query form](https://apps.crossref.org/SimpleTextQuery) and enter a reference or list of references into the search box.
2. Optional:
    * select *List all possible DOIs per reference* to return multiple results
    * select *Include PubMed IDs in results* to include PubMed IDs
3. Click *Submit*

The system attempts to find exactly one DOI for each reference. For some citations, multiple DOIs may be deposited for an item, or the metadata in either the reference or record registered with us is not sufficient to make a single match. Selecting *List All Possible DOIs* will return multiple results which will need to be evaluated to select the appropriate DOI.

We want our members to match and register as many references as possible, so there are no limits on the use of this service. We provide space for 1,000 references per submission.
