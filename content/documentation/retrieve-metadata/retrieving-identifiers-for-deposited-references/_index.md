+++
title = "Retrieving identifiers for deposited references"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "retrieving-identifiers-for-deposited-references"]
identifier = "documentation/retrieve-metadata/retrieving-identifiers-for-deposited-references"
rank = 4
weight = 30400
aliases = [
	"/education/retrieve-metadata/retrieving-identifiers-for-deposited-references",
	"/education/retrieve-metadata/retrieving-identifiers-for-deposited-references/"
]
+++

Including a content item's references as a citation list in the item’s metadata is encouraged, especially if you participate in our [Cited-by](/documentation/cited-by/) service. During the deposit process, these references are matched and the resulting DOIs are returned in the deposit log.

References that do not match at the time of deposit will be remembered internally and periodically re-run. These subsequent attempts to match references contribute to Cited-by data. The member who deposited the article containing these references can use the following API call to retrieve an updated list of the article's matched references, using role credentials:

```
https://doi.crossref.org/getResolvedRefs?doi=DOI&usr=role&pwd=password
```

Or using user credentials:
```
https://doi.crossref.org/getResolvedRefs?doi=DOI&usr=email@address.com/role&pwd=password
```

Note that the **role** used in the query must have permission to view references for the deposited DOI.

## Examples of retrieving identifiers for references <a id='00394' href='#00394'><i class='fas fa-link'></i></a>

The `key` returned in the matches is the same key supplied for the corresponding citation in the reference deposit. These results contain both journal articles and a book series:

```
{
	doi: "10.1103/PhysRevE.91.062714",
	matched-references: [
		{
			key: "PhysRevE.91.062714Cc1R1",
			doi: "10.1038/nature01609",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc2R1",
			doi: "10.1016/S0301-4622(02)00177-1",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc3R1",
			doi: "10.1073/pnas.1214051110",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc4R1",
			doi: "10.1529/biophysj.106.093062",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc5R1",
			doi: "10.1073/pnas.1833310100",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc6R1",
			doi: "10.1073/pnas.0802484105",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc7R1",
			doi: "10.1021/jz301537t",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc8R1",
			doi: "10.1103/PhysRevX.2.031012",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc9R1",
			doi: "10.1016/j.bpj.2011.03.067",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc10R1",
			doi: "10.1529/biophysj.104.045344",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc11R1",
			doi: "10.1021/ja970640a",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc12R1",
			doi: "10.1073/pnas.0509011103",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc13R1",
			doi: "10.1126/science.1057886",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc14R1",
			doi: "10.1103/PhysRevA.18.255",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc15R1",
			doi: "10.1103/PhysRevE.89.012144",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc16R1",
			doi: "10.1140/epje/e2003-00019-8",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc17R1",
			doi: "10.1021/jp061840o",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc18R1",
			doi: "10.1021/jp073413w",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc20R1",
			doi: "10.1142/2012",
			type: "book_title"
		},
		{
			key: "PhysRevE.91.062714Cc23R1",
			doi: "10.1103/PhysRev.91.1505",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc24R1",
			doi: "10.1039/b918607g",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc25R1",
			doi: "10.1021/jp311028e",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc26R1",
			doi: "10.1016/S0006-3495(03)74892-9",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc27R1",
			doi: "10.1209/epl/i2003-10158-3",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc28R1",
			doi: "10.1016/j.physa.2004.12.005",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc29R1",
			doi: "10.1529/biophysj.106.094052",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc30R1",
			doi: "10.1529/biophysj.106.094243",
			type: "journal_article"
		},
		{
			key: "PhysRevE.91.062714Cc31R1",
			doi: "10.1007/978-3-642-61544-3",
			type: "book_title"
		}
	]
}
```

These results contain a match made to a DataCite DOI (via `reg-agency`):

```
{
	doi: "10.50505/doi_2006042312",
	matched-references: [
		{
			key: "ref1",
			doi: "10.50505/test_20051231320",
			type: "journal_article"
		},
		{
			key: "ref2",
			doi: "10.50505/test_200611161351",
			type: "journal_article"
		},
		{
			key: "ref3",
			doi: "10.5167/UZH-29884",
			reg-agency: "DataCite"
		}
	]
}
```
