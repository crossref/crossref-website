+++
title = "Upgrading from iThenticate v1 to iThenticate v2"
date = "2022-07-15"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "upgrading"]
identifier = "documentation/similarity-check/upgrading"
rank = 4
weight = 100500
+++

## Background
The Similarity Check service gives Crossref members reduced rate access to the iThenticate tool from Turnitin, and there's now a new version of iThenticate available for some subscribers. iThenticate v2 has lots of new and improved features, including:
* The ability to identify content on preprint servers more easily
* A “red flag” feature that signals the detection of hidden text such as text/quotation marks in white font, or suspicious character replacement
* A faster, more user-friendly and responsive Similarity Report interface

For those members who integrate their iThenticate account with a Manuscript Tracking Service (MTS) there are even more benefits - you can now submit your manuscripts and view your Similarity Report from within the MTS, and you can also manage your exclusions from within your MTS too. There are also some important changes in how you manage users.

You can find out more about the benefits of iThenticate v2 [on our blog](/blog/similarity-check-whats-new-with-ithenticate-v2/).

## Who can upgrade from iThenticate v1 to iThenticate v2?
Most existing subscribers to Similarity Check are currently using iThenticate v1, and over the next year or so we'll be inviting these members to upgrade to iThenticate v2. Not everyone can upgrade immediately. This is because many members integrate their iThenticate account with their Manuscript Tracking Service (MTS), and not all the MTSs are able to integrate with iThenticate v2 just yet.

An upgrade is currently available to members of Crossref:
* Using iThenticate through the browser
* Integrating iThenticate with eJournal Press
* Integrating iThenticate with ScholarOne Manuscripts
* Integrating iThenticate with Scholastica Peer Review
* Integrating iThenticate with Editorial Manager

We'll be letting members know as the other Manuscript Tracking Systems are able to integrate with iThenticate v2. Brand new subscribers who fall into the categories above will be set up directly on iThenticate v2.

Not sure if you're currently using iThenticate v1 or iThenticate v2? [Here's how to check](documentation/similarity-check/upgrading/v1-or-v2/).

## The upgrade process
When you are able to upgrade from iThenticate v1 to iThenticate v2, we will contact the Similarity Check editorial contact on your account and ask you to complete a form. We will then check that you are still eligible for Similarity Check. This means that we will check that you are still providing Similarity Check URLs for at least 90% of the content you have registered with Crossref, and that the team at Turnitin can continue to index this content into the iThenticate database.

Once this is done, the team at Turnitin will send an email to the Similarity Check editorial contact on your account, so they can set up administrator credentials on your new iThenticate v2 account. The email will look like this:
<figure><img src='/images/documentation/SC-email-login.png' alt='email login' title='' width='50%'></figure>

Click on the blue ‘Set up my account’ button at the bottom of the email. This will bring you to a page which looks something like this:
<figure><img src='/images/documentation/SC-v2-initial-login-screen.png' alt='v2 initial login screen' title='' width='50%'></figure>

Fill out your username and password, and don’t forget to tick to agree to the terms and conditions. You will then arrive at your new iThenticate v2 account.
<figure><img src='/images/documentation/SC-v2-welcome-screen.png' alt='v2 welcome screen' title='' width='75%'></figure>

Once you have administrator access to your new iThenticate v2 account, you can follow the instructions for getting set up. There are a different set of instructions depending on whether:

* You will be using iThenticate directly in the browser - [more here](/documentation/similarity-check/ithenticatev2-account-setup/administrator-checklist/).
* You will be integrating iThenticate with your Manuscript Tracking System (MTS) - [more here](/documentation/similarity-check/ithenticatev2-mts-account-setup/administrator-checklist/).

If you have any follow up questions after your upgrade, do read our [upgrade FAQs](/documentation/similarity-check/upgrading/faqs/).
