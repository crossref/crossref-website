+++
title = "Folders"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-use", "folders"]
identifier = "documentation/similarity-check/ithenticate-account-use/folders"
rank = 4
weight = 100301
aliases = [
  "/education/similarity-check/ithenticate-account-use/folders",
  "/education/similarity-check/ithenticate-account-use/folders/"
]
+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}

**[v2 Folders](https://help.turnitin.com/crossref-similarity-check/user.htm#Managingfilesandfolders)**

**v1 Folders, keep reading:**


The *Folders* page contains the main functionality of iThenticate, the service which powers Crossref Similarity Check. It is where folders are created, browsed and shared with other users, where documents are submitted within a folder to be checked against the iThenticate database for similarity, and where documents can be deleted or moved from one folder to another.

Start from [iThenticate](https://www.ithenticate.com), and [log in](/documentation/similarity-check/ithenticate-account-use#00580).

On this page, learn more about how to:

{{% row %}}
{{% column %}}

* [Create a new folder](#00585)
  * [Exclusions](#00586)
  * [Repositories to search against](#00587)
  * [Save your new folder](#00588)
* [Create a new folder group](#00589)
* [Organize folders](#00590)
* [Move folders](#00591)

{{% /column %}}
{{% column %}}

* [Delete folders](#00592)
* [Share folders](#00593)
* [Edit folder settings](#00594)
  * [Automatic exclusion of bibliography sections](#00595)
  * [Automatic exclusion of quotations](#00596)

{{% /column %}}
{{% /row %}}

## Create a new folder (v1) <a id='00585' href='#00585'><i class='fas fa-link'></i></a>

Look for the *New Folder* section on the right of the screen, and click *New Folder*

<figure><img src='/images/documentation/SC-folders-new.png' alt='Create a new folder' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image66">Show image</button>
<div id="image66" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-new.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

On the *Create A New Folder* page, use *Folder Groups* to specify the group where you’d like to save your new folder, then enter a name in *Folder Name*.

<figure><img src='/images/documentation/SC-folders-new-name.png' alt='Add folder name' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image67">Show image</button>
<div id="image67" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-new-name.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

### Exclusions (v1) <a id='00586' href='#00586'><i class='fas fa-link'></i></a>

You can choose to exclude certain text from the Similarity Check for all documents uploaded into this folder. Learn more about [filters and exclusions within the Similarity Report](/documentation/similarity-check/ithenticate-account-use/similarity-report-use#00604), and [URL filters](/documentation/similarity-check/ithenticate-account-setup/account-info#00572) and [phrase exclusions](/documentation/similarity-check/ithenticate-account-setup/account-info#00573) for account administrators.

Use the relevant tick boxes to exclude quotes, bibliography, certain phrases (set these under [Account Info](/documentation/similarity-check/ithenticate-account-setup/account-info#00573)), small matches, and small sources.

<figure><img src='/images/documentation/SC-folders-exclude.png' alt='Exclusion options' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image68">Show image</button>
<div id="image68" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-exclude.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

To exclude small matches, you set an exclusion threshold. Any match with fewer words than the threshold will be excluded from the Similarity Check. This affects the *Match Overview* view in Document Viewer. Modify this option from within *Document Viewer*.

<figure><img src='/images/documentation/SC-folders-exclude-small-matches.png' alt='Exclude small matches' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image69">Show image</button>
<div id="image69" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-exclude-small-matches.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

To exclude small sources, you set a word count *or* a percentage exclusion threshold. Any matches with fewer words, or lower than a certain percentage matched will be excluded from the Similarity Check. This affects the *All Sources* view in Document Viewer. Modify this option from within *Document Viewer*.

<figure><img src='/images/documentation/SC-folders-exclude-small-sources.png' alt='Exclude small sources' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image70">Show image</button>
<div id="image70" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-exclude-small-sources.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Think carefully about using percentage thresholds if you are working with large documents, where a set percentage of 1% may exclude very large matches/sources. For example, 1% of a 100-page document is one full page.

The *exclude sections* option allows you to exclude longer abstracts or methods and materials sections from being picked up by the Similarity Check.

<figure><img src='/images/documentation/SC-folders-exclude-sections.png' alt='Exclude sections' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image71">Show image</button>
<div id="image71" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-exclude-sections.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Please be aware that section exclusion may not work properly if documents contain:
* Watermarks
* Unevenly spaced line numbering
* Sub-headings that are indistinguishable from the Methods and Materials heading
* Abstract or Methods and Materials section appearing within a table
* Section headings and body text using the same font, font size, and font treatment

### Repositories to search against (v1) <a id='00587' href='#00587'><i class='fas fa-link'></i></a>

Choose which collections to include in the Similarity Check. Here are the currently available repositories:

* Crossref - research articles, books, and conference proceedings provided by publishers of scholarly content all over the world
* Crossref posted content - preprints, eprints, working papers, reports, dissertations, and many other types of content that has not been formally published but has been registered with Crossref
* Internet - a database of archived and live publicly-available web pages, including billions of pages of existing content, and with tens of thousands of new pages added each day
* Publications - third-party periodical, journal, and publication content including many major professional journals, periodicals, and business publications

<figure><img src='/images/documentation/SC-folders-repositories.png' alt='Limit searches to specific repositories' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image72">Show image</button>
<div id="image72" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-repositories.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

To buy the option to create a customizable database source with your own content to submit to and search against, please contact [sales@ithenticate.com](mailto:sales@ithenticate.com).

### Save your new folder (v1) <a id='00588' href='#00588'><i class='fas fa-link'></i></a>

Once you are satisfied with the changes you’ve made, click *Create* at the bottom of the form to create your new folder.

## Create a new folder group (v1) <a id='00589' href='#00589'><i class='fas fa-link'></i></a>

Start from the *New folder* section to the right of the page, and click *New Folder Group*.

<figure><img src='/images/documentation/SC-folders-new-folder-group.png' alt='New folder group' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image73">Show image</button>
<div id="image73" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-new-folder-group.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

On the *Create A New Folder Group* screen, name your new folder group, and click *Create*.

Now you have an empty folder group. To add a folder to this folder group, click *Create a folder*. To delete an empty folder group, click *Remove this empty group*.

## Organize folders (v1) <a id='00590' href='#00590'><i class='fas fa-link'></i></a>

Folders in the folder group are shown in alphabetical order. To see a folder group’s content, go to the *My Folders* section on the left, and click *My Folders*.

<figure><img src='/images/documentation/SC-folders-my-folders.png' alt='My folders' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image74">Show image</button>
<div id="image74" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-my-folders.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

You can choose to organize the folders within a folder group by title, or by date processed:

* To sort the folders by title, click the *Title* header in the title column. A down arrow shows that the folders have been arranged in alphabetical order. Click the down arrow again to put the folders in reverse alphabetical order.
* To sort the folders by date created, click the *Date Created* header in the date created column. A down arrow shows that the folders have been arranged by date created, with the most recent first (reverse chronological order). Click the down arrow again to put the folders in chronological order.

<figure><img src='/images/documentation/SC-folders-organize.png' alt='Organize folders' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image75">Show image</button>
<div id="image75" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-organize.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## Move folders (v1) <a id='00591' href='#00591'><i class='fas fa-link'></i></a>

To move folders to another folder group, go to the folder group containing the folders you wish to move. Click the tick box beside the folder you want to move. From the drop-down menu, use *Move selected to...* to choose the destination folder group, and click *Move*.

<figure><img src='/images/documentation/SC-folders-move.png' alt='Move folder' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image76">Show image</button>
<div id="image76" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-move.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

The drop-down menu will not show unless you have created other folders to make it possible to move a document.

## Delete folders (v1) <a id='00592' href='#00592'><i class='fas fa-link'></i></a>

Start from the *My Folders* side menu, and hover over the folder you wish to delete. Click the *trash can* icon to move the folder to the *Trash* folder group.

<figure><img src='/images/documentation/SC-folders-delete.png' alt='Delete folder' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image77">Show image</button>
<div id="image77" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-delete.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

To delete multiple folders, go to the folder group, and check the tick boxes for each folder you wish to delete. Click *Trash* in the menu bar above to move the folders to the *Trash* folder group.

Once a folder has been moved to the trash, you can review it before you delete it permanently. From the *My Folders* menu on the left, click the *Trash* folder group. In the trash, you can see all the folders you have moved here. To remove a folder from the trash, check its tick box, and use *Move selected to...* to move the folder to another location.

To permanently delete a folder, check its tick box, and click *Delete* in the menu bar above. Once you have permanently deleted a folder from *Trash*, you will not be able to get it back.

## Share folders (v1) <a id='00593' href='#00593'><i class='fas fa-link'></i></a>

Depending on how your account administrator has set up sharing permissions, you may be able to (a) view only folders shared by other users, (b) view all users' folders, or (c) view folders of selected users. If you cannot automatically view others’ folders, use the sharing feature to share folders with other users within the same account.

Start from the folder you want to share, and click the *Sharing* tab.

<figure><img src='/images/documentation/SC-folders-share.png' alt='Share folder' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image78">Show image</button>
<div id="image78" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-share.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

You will see a list of users with whom you can share the folder. Check the box next to the users’ names, and click ​*Update Sharing​*.
Sharing a folder with another user allows them to view the Similarity Report only. It does not allow them to submit a document to the folder.

Once a folder has been shared, there are two ways to unshare the folder:
* by the user who shared it: uncheck the box next to the user’s name, and click ​*Update Sharing*
* by the user with whom it is being shared: in the user’s directory, hover the cursor over the folder name, and an *X* icon will appear to the right of the folder name. Click the *X* icon to remove the shared folder.

Account administrators can enable or disable sharing access based on the organization’s internal guidelines. If the sharing feature is disabled, users will not be able to view previously shared documents.

## Edit folder settings (v1) <a id='00594' href='#00594'><i class='fas fa-link'></i></a>

To customize a folder’s settings, use the *Settings* tab within the folder. Folder settings includes three tabs: Folder Options, Report Filters, and Phrase Exclusions.

<figure><img src='/images/documentation/SC-folders-settings.png' alt='Folder settings' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image79">Show image</button>
<div id="image79" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-settings.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

* Use *Folder Options* to view and modify the options you chose when you created the folder.
* Use *Report Filters* to manage the list of URLs that are filtered out from comparison checking for that folder.
* Use *Add URL* to add a URL to be filtered, and click Add URL. The URL you add may be as specific or general as you wish, for example:
  * http\://example.com/ (don’t forget to include the trailing “/”) - to exclude an entire site
  * http\://example.com/docs/ - to exclude a specific directory
  * http\://example.com/docs/paper.pdf - to exclude a specific document
To remove a URL, click the *X* icon to the right of the URL.

<figure><img src='/images/documentation/SC-folders-settings-report-filters.png' alt='Report filters' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image80">Show image</button>
<div id="image80" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-settings-report-filters.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

* Use *Phrase Exclusions* to add and remove phrases to exclude from comparison checking for every submission in this folder. To add a new phrase, click *Add a new phrase*, enter the phrase you wish to exclude in the *Phrase* text box, and click *Create*. If you don’t want to create a phrase to exclude, click *Back to list* to return to the *Phrase Exclusions* tab, or *Back to folder* to return to the folder view.

<figure><img src='/images/documentation/SC-folders-settings-phrase-exclusions.png' alt='Phrase exclusions' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image81">Show image</button>
<div id="image81" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-folders-settings-phrase-exclusions.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

### Automatic exclusion of bibliography sections (v1) <a id='00595' href='#00595'><i class='fas fa-link'></i></a>

iThenticate detects the following keywords and ignores any matches after the keyword:

* reference, references
* reference list
* reference cited, references cited
* reference and note, reference and notes
* references and note, references and notes
* reference & note, references & note
* reference & notes, references & notes
* references and further reading
* resource, resources
* resources directory
* bibliography
* bibliographic information
* works cited, work cited
* citations
* literature
* literature cited

When it reaches any of the following words in the paper, it resumes the Similarity Check:

* appendix
* appendices
* glossary
* table
* tables
* acknowledgement, acknowledgements
* exhibits
* figure
* figures
* chart
* charts

### Automatic exclusion of quotations (v1) <a id='00596' href='#00596'><i class='fas fa-link'></i></a>

Supported marks: iThenticate recognizes these quotation marks and will ignore any matches that use them:

* "..."
* «...»
* »...«
* „…“
* 《...》
* 〈...〉
* 『...』

Unsupported marks: iThenticate does not recognize these quotation marks and will flag any matches that use them:

* '...'

This applies even when (single) 'quotes' appear within (double) "quotes". For example:

> "This text would be excluded 'but this text would not be excluded' "then this text would also be excluded."

iThenticate will also exclude formatted block quotations (indented blocks of text) in .doc or .docx files.
