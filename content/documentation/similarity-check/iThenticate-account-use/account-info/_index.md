+++
title = "Your Similarity Check user account"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-use", "account-info"]
identifier = "documentation/similarity-check/ithenticate-account-use/account-info"
rank = 4
weight = 100308
aliases = [
  "/education/similarity-check/ithenticate-account-use/account-info",
  "/education/similarity-check/ithenticate-account-use/account-info/"
]
+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}

**[v2 Similarity Check user account](https://help.turnitin.com/crossref-similarity-check/user.htm)**

**v1 Similarity Check user account, keep reading:**


Manage your user account using the *Account Information* tab.

<figure><img src='/images/documentation/SC-admins-account-info.png' alt='Account information' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image60">Show image</button>
<div id="image60" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-account-info.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## Your user account profile (v1) <a id='00611' href='#00611'><i class='fas fa-link'></i></a>

{{< snippet "/_snippet-sources/similarity-check-account-info.md" >}}
