+++
title = "Administrator checklist"
date = "2022-07-15"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "ithenticatev2-MTS-account-setup", "similarity-check-administrator-checklist"]
identifier = "documentation/similarity-check/ithenticatev2-mts-account-setup/administrator-checklist"
rank = 4
weight = 100277

+++
This section of our documentation is for Similarity Check account administrators who are integrating iThenticate v2 with their Manuscript Submission System (MTS).

* Using iThenticate v1 instead? Go to the [v1 account administrators section](/documentation/similarity-check/ithenticate-account-setup/administrator-checklist).
* Using iThenticate v2 through a browser instead? Go to [setting up iThenticate v2 for browser users](/documentation/similarity-check/ithenticatev2-account-setup/administrator-checklist).
* Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).
* Not sure whether you're an account administrator? [Find out here](/documentation/similarity-check/ithenticatev2-account-setup/).

As an administrator, you decide how your organization uses the iThenticate tool. You’ll find the system easier to use if you set it up correctly to start with, so do read through the checklist below carefully and make sure you've set up your account as you want it before you start checking your manuscripts.

1. [Set up your integration with your MTS](#66614)
2. [Decide on your exclusions](#66615)
3. [DO NOT set up users or folders](#66616)
4. [Decide if you want to use the Submitted Works repository (or Private Repository)](#66617)
5. [Decide how you'll budget for your document checking fees](#66618)
6. [Make sure you stay eligible for the Similarity Check service](#66619)

### 1. Set up your integration with your MTS <a id='66614' href='#66614'><i class='fas fa-link'></i></a>
To set up your integration, you need to create an API key by logging into iThenticate through the browser. You will then share this API key and the URL of your iThenticate v2 account with your MTS. [Find out more](/documentation/similarity-check/ithenticatev2-mts-account-setup/api-key).

If you are using OJS and want to use the iThenticate plugin, you'll need to ensure that you're on OJS version 3.3, 3.4, or 3.5. For instructions on how to upgrade your OJS instance, please visit PKP's documentation [here](https://docs.pkp.sfu.ca/dev/upgrade-guide/en/) or [here](https://docs.pkp.sfu.ca/upgrading-ojs-2-to-3/en/), depending on which version you're currently running.

### 2. Decide on your exclusions <a id='66615' href='#66615'><i class='fas fa-link'></i></a>
You can decide to exclude preprints, certain websites, or even specific sections of text. We recommend starting without any exclusions to avoid excluding anything important. Once your users are experienced enough to identify words and phrases that appear frequently but are not potentially problematic matches (and can therefore be ignored) in a Similarity Report, you can start carefully making use of this feature.

Find out more in the [Exclusions](/documentation/similarity-check/ithenticatev2-mts-account-setup/exclusions/) section.

### 3. DO NOT set up users or folders <a id='66616' href='#66616'><i class='fas fa-link'></i></a>
If you are integrating iThenticate v2 with your MTS, you don't need to set up more users or folders in your iThenticate - everything will be managed from your MTS. [Find out more](/documentation/similarity-check/ithenticatev2-mts-account-setup/manage-users/).

### 4. Decide if you want to use the Submitted Works repository (or Private Repository) <a id='66617' href='#66617'><i class='fas fa-link'></i></a>
The Submitted Works repository (or Private Repository) allows users to find similarity not just across Turnitin’s extensive Content Database but also across all previous manuscripts submitted to your iThenticate account for all the journals you work on. This would allow you to find collusion between authors or potential cases of duplicate submissions but it also means you could share sensitive data between users, so you need to think very carefully about how you will use this feature. This feature is currently only available to those integrating with ScholarOne Manuscripts. [Find out more](/documentation/similarity-check/ithenticatev2-mts-account-setup/private-repository/).

### 5. Decide how you'll budget for your document checking fees <a id='66618' href='#66618'><i class='fas fa-link'></i></a>
There’s a [charge for each document checked](/fees#similarity-check-per-document-fees), and you’ll receive an invoice in January each year for the documents you’ve checked in the previous year. If you're a member of Crossref through a Sponsor, your Sponsor will receive this invoice.

As well as setting a Similarity Check document fees budget for your account each year, it’s useful to monitor document checking and see if you’re on track. You can monitor your usage in your [Statistics section](https://help.turnitin.com/crossref-similarity-check/administrator/statistics/viewing-submission-details.htm). Ask yourself:

* How many documents do you plan to check?
* How often do you want to monitor usage? Set yourself a reminder to check your Statistics periodically.

It’s a good idea to come back to these questions periodically, consider how your use of the tool is evolving, and make changes accordingly.

### 6. Make sure you can stay eligible for the Similarity Check service  <a id='66619' href='#66619'><i class='fas fa-link'></i></a>
Your organization gets reduced rate access to the iThenticate tool through the Similarity Check service because you make your own published content avaialble to be indexed into the iThenticate database. You do this by providing full text URLs specifically for this service in the metadata that you register with Crossref. Talk to your colleagues who are responsible for registering your DOIs with Crossref, and make sure that they continue to include full text URLs for Similarity Check in the metadata they register with us.
