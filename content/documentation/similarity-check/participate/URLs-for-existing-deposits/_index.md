+++
title = "Adding full-text URLs to existing deposits"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "participate", "URLs-for-existing-deposits"]
identifier = "documentation/similarity-check/participate/URLs-for-existing-deposits"
rank = 4
weight = 100103
aliases = [
  "/education/similarity-check/participate/urls-for-existing-deposits",
  "/education/similarity-check/participate/urls-for-existing-deposits/",
  "/education/similarity-check/adding-full-text-urls-for-similarity-check-for-existing-deposits",
  "/education/similarity-check/adding-full-text-urls-for-similarity-check-for-existing-deposits/"

]
+++

If you've previously registered content without including your full-text URLs for Similarity Check, don't worry - you can still add them later. Here are the options for adding full-text URLs for Similarity Check for existing deposits:

## Use the web deposit form’s *supplemental metadata upload using a .csv file* option <a id='00317' href='#00317'><i class='fas fa-link'></i></a>

If you have a large number of DOIs to update, it's easiest to upload a .csv file of the DOIs and their Similarity Check full-text URLs using the web deposit form's [supplemental metadata upload using a .csv file](/documentation/content-registration/web-deposit-form#00318) option.  

## Upload a resource-only deposit <a id='00319' href='#00319'><i class='fas fa-link'></i></a>

If you deposit Crossref metadata by sending us the XML directly, you may wish to update your existing XML using a resource-only deposit, as in this [example XML file](https://gitlab.com/crossref/schema/raw/master/examples/resource_simcheck_4.4.2.xml). Learn how to [upload resource-only deposits](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/) to add metadata to an existing record.

## A full redeposit (update) <a id='00320' href='#00320'><i class='fas fa-link'></i></a>

Run a standard metadata deposit by adding the Similarity Check URLs, as in [this example](/xml-samples/sc-ascrawled.xml). Don't forget to update your timestamp!

Use our widget to check the [percentage of Similarity Check URLs included in your metadata](/documentation/similarity-check/participate/eligibility/).
