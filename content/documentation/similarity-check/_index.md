+++
title = "Similarity Check"
date = "2020-04-08"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["other-services", "similarity-check"]
aliases = [
  "/education/similarity-check",
  "/education/similarity-check/",
  "/get-started/similarity-check",
  "/get-started/similarity-check/"
]
[menu.main]
parent = "Documentation"
identifier = "documentation/similarity-check"
weight = 400000
[menu.documentation]
identifier = "documentation/similarity-check"
parent = "Documentation"
rank = 4
weight = 400000
+++

{{< snippet "/_snippet-sources/similarity-check.md" >}}

*Update 2024: We are no longer able to offer the Similarity Check service to members based in Russia. [Find out more](/operations-and-sustainability/membership-operations/sanctions/)*.
