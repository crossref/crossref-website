+++
title = "Get help with Similarity Check"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "get-help"]
identifier = "documentation/similarity-check/get-help"
rank = 4
weight = 100600
alases = [
  "/education/similarity-check/ithenticate-account-use/help",
  "/education/similarity-check/ithenticate-account-use/help/",
  "/documentation/similarity-check/ithenticate-account-use/help",
    "/documentation/similarity-check/ithenticate-account-use/help"
]
+++

Do start by reading the documentation on this website.

### Help with initial set up for iThenticate administrators
* [Setting up your iThenticate v1 account](/documentation/similarity-check/ithenticate-account-setup/)
* [Setting up your iThenticate v2 account to use directly in the browser](/documentation/similarity-check/ithenticatev2-account-setup/)
* [Setting up your iThenticate v2 account to use with your Manuscript Tracking System](documentation/similarity-check/ithenticatev2-mts-account-setup/)

### Help using the iThenticate tool
* [Using your iThenticate account](documentation/similarity-check/ithenticate-account-use/)
* [Understanding your Similarity Report](documentation/similarity-check/similarity-report-understand/)

### Help with upgrading from iThenticate v1 to iThenticate v2
* [Upgrading from iThenticate v1 to iThenticate v2](documentation/similarity-check/upgrading/)

If you still don't understand how to use the iThenticate tool, the team at Crossref can help - just [contact us](/contact).

If you encounter a technical issue (such as an error message or a bug), please contact the Turnitin team directly at [tiisupport@turnitin.com](mailto:tiisupport@turnitin.com).

* Email: [tiisupport@turnitin.com](mailto:tiisupport@turnitin.com)
* Phone: [+1 866 816 5046](tel:+18668165046), extension 241

To find out about outages or planned maintenance that affect the iThenticate system, check the [Turnitin status page](https://turnitin.statuspage.io) or [Turnitin's X feed](https://x.com/TurnitinStatus). These pages display information on service outages, maintenance alerts, or incidents related to the performance of iThenticate. You can also subscribe to the notifications feed to automatically receive updates. For known bugs in the iThenticate software, please visit [Turnitin's known issues page](https://help.turnitin.com/known-issues.htm#ithTab).
