+++
title = "Thank you for completing the Metadata Plus subscriber form"
date = "2019-07-10"
type = 'join-thank-you'
draft = false
image = "/images/banner-images/speed-boat.jpg"
slug = "metadataplus-thank-you"
+++

## Thanks for completing the Metadata Plus subscriber form

### Here's what happens next:

We’ll send you a pro-rated invoice for your Metadata Plus subscriber fee for the remainder of this current calendar year. Once we confirm the invoice is paid, we’ll give you access to the key manager where you will be able to create your API keys.


---
Please contact our [Metadata Plus support](mailto:plus@crossref.org) team if you have any questions in the meantime.
