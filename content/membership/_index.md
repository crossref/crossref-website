+++
title = "Become a member"
date = "2023-09-01"
author = "Amanda Bartell"
draft = false
image = "/images/banner-images/join-hello-sand.jpg"
maskcolor = "crossref-blue"
rank = 5
aliases = [
    "/01company/join_crossref.html",
    "/02publishers/00success.html",
    "/02publishers/22request_mem.html",
    "/02publishers/22request_mem_test.html",
    "/join_crossref.html",
    "/education/become-a-member",
    "/education/become-a-member/",
    "/education/become-a-member/what-it-means-to-be-a-member/",
    "/education/become-a-member/what-it-means-to-be-a-member",
    "/education/become-a-member/join-as-a-member",
    "/education/become-a-member/join-as-a-member/",
    "/education/metadata-stewardship/understanding-your-member-obligations",
    "/education/metadata-stewardship/understanding-your-member-obligations/",
    "/documentation/metadata-stewardship/understanding-your-member-obligations",
    "/documentation/metadata-stewardship/understanding-your-member-obligations/",
    "/membership/benefits/",
    "/membership/benefits",
    "/education/why-crossref",
    "/education/why-crossref/",
    "/education/become-a-member/why-choose-crossref",
    "/education/become-a-member/why-choose-crossref/",
    "/education/why-crossref/the-problem-crossref-is-here-to-solve/",
    "/education/why-crossref/the-problem-crossref-is-here-to-solve"
    ]

[menu.main]
parent = "Get involved"
weight = 5
+++

{{% divwrap blue-highlight %}}

[If you’re Funder, learn more here.](/services/grant-linking-system/)

{{% /divwrap %}}


Organisations need to be members of Crossref to create metadata records that identify, describe, and locate their work. You don't need to be a member to retrieve metadata (read about our open [metadata retrieval](/services/metadata-retrieval) tools).

## Benefits of membership

- You can create and steward rich metadata records, adding relationships, provenance, contributor, and funding information, ensuring accuracy and persistence over time. By doing so, you are adding to and benefiting from reciprocal connections among a global network of research objects, co-creating a Research Nexus for the benefit of society including future generations.

- Works are more likely to be discovered if they have a Crossref record. Your organisation will be joining the world's largest registry of Digital Object Identifiers (DOIs) and metadata for scholarly research; your work is connected with 160 million other records from >20,000 other members from ~155 countries. Crossref facilitates an average of 1.1 billion DOI resolutions (clicks of a DOI link) every month, which is 95% of all DOI activity.

- Your metadata is freely and openly shared in a consistent, machine-readable way. You don't have to duplicate the information for multiple parties as your metadata is discoverable by thousands of other systems in the global research ecosystem; there are over 1 billion calls to our API every month.

- Your organisation can have access to unique tools that support research integrity, such as incorporating Crossmark status buttons on your landing pages and PDFs, and using Similarity Check to screen for text-based plagiarism.

- You can vote in our board elections and/or stand for a seat, participating in the governance of Crossref. There are also many opportunities to get involved with the whole community, to co-create solutions to shared problems, help shape and influence our roadmap, and to join the discussions.

It's so much more than just getting a DOI.

## Are you eligible?

{{< snippet "/_snippet-sources/eligibility.md" >}}

<a id=‘member-obligations’ href=‘#member-obligations’></a>
## Member obligations

{{< snippet "/_snippet-sources/benefits-obligations.md" >}}

## What are the fees?

All our fees are set out on the [fees](/fees) page.

### Independent membership

Members pay an [annual membership fee](/fees/#annual-membership-fees/), which is tiered depending on your organisation's revenue or expenses. After you apply, we will send you a pro-rated membership invoice for the remainder of the current year and this is due to be paid before your membership can be activated. You then pay the same annual membership fee each year, every January. Membership renews automatically unless you actively cancel.

There are also [one-off registration fees](/fees/#content-registration-fees) for each new metadata record and DOI that you create. There are never any fees to add to and update the metadata. We send you this invoice quarterly, after calculating the quantity and type of records you have registered that quarter.

Organisations located in the least economically-advantaged countries in the world do not pay an annual membership fee and do not pay registration fees - find out more on our [Global Equitable Membership (GEM) program](/gem) page.

If you have further questions about billing, visit our [fees page](/fees) and our [billing FAQs page](/members-area/all-about-invoicing-and-payment/).

### Membership via a Sponsor

The Sponsor Program is for members who do not have the resources or capabilities to work directly with and pay Crossref. More than half of our members have joined via a Sponsor, which means they don't pay Crossref a membership fee, and they receive technical support and expertise locally, through their sponsor.

If a sponsored member is eligible for the [Global Equitable Membership (GEM) program](/gem), we do not charge the sponsor any membership or registration fees. [Find out more about working with a sponsor](/membership/about-sponsors).

_(NB: some Sponsors may charge you for their services, so it's important that you clarify their terms before joining)._

## Ready to apply?

{{% row %}}
{{% column %}}

### Option 1: [Apply as an independent member](/membership/_apply)

{{% /column %}}

{{% column %}}

### Option 2: [Find a Sponsor to join through](/membership/about-sponsors)

{{% /column %}}
{{% /row %}}


---

If you have questions, please consult our forum at [community.crossref.org](https://community.crossref.org) or [open a ticket with our membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) where we'll reply within a few days.
