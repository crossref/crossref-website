+++
title = "Membership terms"
date = "2022-06-03"
draft = false
author = "Ginny Hendricks"
Rank = 5
aliases = [
    "/member-obligations",
    "/02publishers/59pub_rules.html"
]
[menu.main]
parent = "Become a member"
weight = 3
+++

{{< member-terms >}}

---

If you would like to [apply to join](/membership) please visit our membership page which describes the obligations and leads to an application form. Please contact our [membership specialist](mailto:member@crossref.org) with any questions.
