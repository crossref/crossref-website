+++
title = "Brand & logos"
date = "2025-02-07"
draft = false
author = "Rosa Morais Clark"
parent = "About us"
rank = 3
weight = 10
aliases = [
    "/06members/49logos.html"
]
+++


## Typeface

We use the [Inter](https://rsms.me/inter) typeface family. It is free and open source, and the fonts are optimised for accessibility on computer screens. They are also great for our global community as they feature over 2000 glyphs covering 147 languages.

## Logos

Please reference our logos on your site using the snippets below, rather than copying locally. This will ensure that if/when the logos change in future, it will automatically be updated across the web. Contact our [community team](mailto:feedback@crossref.org) if you’d like a variation or to request special use.

### Stacked organisational logo

<img float=left src="https://assets.crossref.org/logo/crossref-logo-200.svg" width="200" alt="Crossref logo stacked">

Please reference this default logo to represent Crossref, the organisation and use this snippet:
`<img src="https://assets.crossref.org/logo/crossref-logo-200.svg" width="200" alt="Crossref logo stacked"/>`.

### Landscape logo

<img float=left src="https://assets.crossref.org/logo/crossref-logo-landscape-200.svg" width="200" alt="Crossref landscape logo">

The landscape version of our logo can be used when space is tight, with the snippet
`<img src="https://assets.crossref.org/logo/crossref-logo-landscape-200.svg" width="200" alt="Crossref landscape logo">`.

### 'Metadata-from' logo

<img float=left src="https://assets.crossref.org/logo/metadata-from-crossref-logo-200.svg" width="200" alt="Metadata from Crossref logo"> 

If you use our metadata, the research community might like to know your sources, so here's a logo that you can use, with the snippet `<img src="https://assets.crossref.org/logo/metadata-from-crossref-logo-200.svg" width="200" alt="Metadata from Crossref logo">`.


### Displaying links

Crossref identifiers are also persistent links and should be displayed as full HTTPS links within reference lists and anywhere else on your website that you use Crossref DOIs. Optionally, you can add the Crossref icon (the 'zigzag') prepending the link, like this: 

{{% logo-inline %}} https://doi.org/10.13003/5jchdy

Please review the [official Crossref display guidelines](https://doi.org/10.13003/5jchdy) for full details.


### Badges

We know that many people like to reference Crossref in your communications, promoting how you are contributing to scholarly infrastructure. To support this we have created a set of [Account badges](/brand/badges/). If you currently use the Crossref logo on your website, you might like to replace it with or add your Account Type badge.

## Colour palette

{{% divwrap blue-highlight %}} Blue - Hex #3eb1c8 {{% /divwrap %}}  
{{% divwrap lightgrey-highlight %}} Sand - Hex #d8d2c4 {{% /divwrap %}}  
{{% divwrap darkgrey-highlight %}} Grey - Hex #4f5858  {{% /divwrap %}}  
{{% divwrap red-highlight %}} Red - Hex #ef3340 {{% /divwrap %}}  
{{% divwrap darkblue-highlight %}} Dark Blue - Hex #005f83  {{% /divwrap %}}  


### Secondary palette - use sparingly as an accent only

{{% divwrap yellow-highlight %}} Yellow - Hex #ffc72c {{% /divwrap %}}  
{{% divwrap green-highlight %}} Green - Hex #00ab84  {{% /divwrap %}}  
{{% divwrap darkbeige-highlight %}} Mocha - Hex #a39382  {{% /divwrap %}} 
{{% divwrap darkyellow-highlight %}} Gold - Hex #ffa300  {{% /divwrap %}}  
{{% divwrap orange-highlight %}} Orange - Hex #fd8332  {{% /divwrap %}}  
{{% divwrap darkred-highlight %}} Dark Red - Hex #a6192e  {{% /divwrap %}}  

## Brand guide

This brand guide was produced in 2016 and is somewhat out of date e.g. we changed typefaces to Inter. But the general rules about the logo still apply!

<p align="center"><embed src="/pdfs/brand-guide-members-march-2016.pdf" width="600px" height="400px" type="application/pdf">  </p>

---
Get in touch with the [Community team](mailto:feedback@crossref.org) with any questions.