---
title: 'Annual call for board nominations'
author: Lucy Ofiesh
draft: false
authors:
  - Lucy Ofiesh
date: 2022-05-31
categories:
  - Board
  - Member Briefing
  - Governance
  - Elections
  - Crossref Live
  - Annual Meeting
archives:
    - 2022
---

The Crossref Nominating Committee is inviting expressions of interest to join the Board of Directors of Crossref for the term starting in March 2023. The committee will gather responses from those interested and create the slate of candidates that our membership will vote on in an election in September.

Expressions of interest will be due Friday, June 24th, 2022.

### About the our board elections
The board is elected through the “one member, one vote” policy wherein every member organization of Crossref has a single vote to elect representatives to the Crossref board. Board terms are for three years, and this year there are five seats open for election.

The board maintains a balance of seats, with eight seats for smaller members and eight seats for larger members (based on total revenue to Crossref). This is in an effort to ensure that the diversity of experiences and perspectives of the scholarly community are represented in decisions made at Crossref.

This year we will elect four of the larger member seats (membership tiers $3,900 and above) and one of the smaller member seats (membership tiers $1,650 and below). You don’t need to specify which seat you are applying for. We will provide that information to the Nominating Committee.

The election takes place online and voting will open in September. Election results will be shared at the annual meeting in October. New members will commence their term in March 2023.

### About the Nominating Committee
The Nominating Committee reviews the expressions of interest and selects a slate of candidates for election. The slate put forward will exceed the total number of open seats. The committee considers the statements of interest, organizational size, geography, gender, and experience.

2022 Nominating Committee:
* Abel Packer, SciELO, Brazil, chair*
* Patrick Alexander, Penn State University Press, US
* Nisha Doshi, Cambridge University Press, UK
* Marc Hurlbert, Melanoma Research Alliance	, US*
* Kihong Kim, Korean Council of Science Editors, South Korea*

(*) indicates Crossref board member

### What does the committee look for
The committee looks for skills and experience that will complement the rest of the board. Candidates from countries and regions that are not currently reflected on the board are strongly encouraged to apply. Successful candidates often demonstrate a commitment to or understanding of our [strategic agenda](https://www.crossref.org/strategy/) or the [Principles of Open Scholarly Infrastructure](https://openscholarlyinfrastructure.org/); hold positions within their organizations that may be underrepresented on the board currently; and/or have experience with governance or community involvement. The Nominating Committee will also review the member organization's [participation report](https://www.crossref.org/members/prep/).

### Who can apply to join the board?
Any active member of Crossref can apply to join the board. Crossref membership is open to organizations that produce content, such as academic presses, commercial publishers, standards organizations, and research funders.

### Board roles and responsibilities
Crossref’s services provide central infrastructure to scholarly communications. Crossref’s board helps shape the future of our services, and by extension, impacts the broader scholarly ecosystem. We are looking for board members to contribute their experience and perspective.

The role of the board at Crossref is to provide strategic and financial oversight of the organization, as well as guidance to the Executive Director and the staff leadership team, with the key responsibilities being:
* Setting the strategic direction for the organization;
* Providing financial oversight; and
* Approving new policies and services.

The board is representative of our membership base and guides the staff leadership team on trends affecting scholarly communications. The board sets strategic directions for the organization while also providing oversight into policy changes and implementation. Board members have a fiduciary responsibility to ensure sound operations. Board members do this by attending board meetings, as well as joining more specific board committees.

### What is expected of board members?
Board members attend three meetings each year that typically take place in March, July, and November. Meetings have taken place in a variety of international locations and travel support is provided when needed. Following travel restrictions as a result of COVID-19, the board adopted a plan to convene at least one of the board meetings virtually each year and all committee meetings take place virtually. Most board members sit on at least one Crossref committee. Care is taken to accommodate the wide range of timezones in which our board members live.

While individuals apply to join the board, the seat that is elected to the board ultimately belongs to the member organization. The primary board member also names an alternate who may attend meetings in the event that the primary board member is unable to. There is no personal financial obligation to sit on the board. The member organization must remain in good standing.

Board members are expected to be comfortable assuming the responsibilities listed above and to prepare and participate in board meeting discussions.

### How to apply
Please [click here to submit your expression of interest](https://docs.google.com/forms/d/e/1FAIpQLSeh_paZyposW2HNSbwodAtxkdwseELsrJ91bpMfC3w_XfNDbg/viewform). We ask for a brief statement about how your organization could enhance the Crossref board and a brief personal statement about your interest and experience with Crossref.

Please contact me with any questions at [lofiesh@crossref.org](mailto:lofiesh@crossref.org)
