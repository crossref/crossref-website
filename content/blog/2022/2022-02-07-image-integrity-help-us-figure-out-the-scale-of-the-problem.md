---
title: 'Image integrity: Help us figure out the scale of the problem'
author: Fabienne Michaud
draft: false
authors:
  - Fabienne Michaud
date: 2022-02-07
categories:
  - Similarity Check
  - Community
  - Enrich services
archives:
    - 2022
---

## Some context

The [Similarity Check Advisory Group](https://www.crossref.org/working-groups/similarity-check/) met a number of times last year to discuss current and emerging originality issues with text-based content. During those meetings, the topic of image integrity was highlighted as an area of growing concern in scholarly communications, particularly in the life sciences.

Over the last few months, we have also read with interest the [recommendations for handling image integrity issues](https://osf.io/kgyc6/) by the STM Working Group on Image Alteration and Duplication Detection, followed closely image integrity sleuths such as [Elizabeth Bik](https://www.nature.com/articles/d41586-020-01363-z) and have, like many of you, noticed that image manipulation is increasingly given as [the reason for retractions](https://retractionwatch.com/2021/11/11/exclusive-university-of-glasgow-seeking-retraction-of-multiple-papers-after-findings-of-image-manipulation/).

Image integrity issues are often associated with [paper mill activity](https://publicationethics.org/resources/forum-discussions/publishing-manipulation-paper-mills) but can also originate from an individual’s intentional or unintentional unethical behaviour. Currently, such issues with figures and images are being identified manually or by using an image integrity tool, comparing images within the same article and/or the publisher’s past publications only - and we know that this is a source of frustration for the Crossref members we have spoken to.

## What next ?

As reported [in Nature last December](https://www.nature.com/articles/d41586-021-03807-6), we believe Crossref is in a unique position to spearhead a cross-publisher solution, similar to what we do for text-based originality checking, as part of our Similarity Check service.

Before we start exploring potential software options, [we need your help](https://docs.google.com/forms/d/e/1FAIpQLSduZO93xDjTFwSD6HmDNvaJnNbt_OGmHDH_72kD-OCZSNgKEw/viewform) to understand:

- the scale of the issues and whether these are focused on specific disciplines
- the type of issues we should prioritise e.g. duplication, beautification, rotation, plagiarism, GAN-generated images/deep-fakes, etc.
- what software (if any) members are using or trialling
- whether a cross-publisher service with the collective benefit of shared images would be of sufficient interest to the community

 ✏️  Let us know what your experience and thoughts are on image integrity by completing [this survey](https://docs.google.com/forms/d/e/1FAIpQLSduZO93xDjTFwSD6HmDNvaJnNbt_OGmHDH_72kD-OCZSNgKEw/viewform).

We’re planning to complete our research and share with you the results along with our proposed next steps soon. 
