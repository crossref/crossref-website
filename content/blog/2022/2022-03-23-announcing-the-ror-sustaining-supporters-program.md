---
title: 'Announcing the ROR Sustaining Supporters program'
author: Ed Pentz
draft: false
authors:
  - Ed Pentz
  - Rosa Morais Clark
  - Ginny Hendricks
date: 2022-03-23
categories:
  - Community
  - Infrastructure
  - Collaboration
  - ROR
archives:
    - 2022
---

In collaboration with California Digital Library and DataCite, Crossref guides the operations of the [Research Organization Registry (ROR)](https://ror.org). ROR is community-driven and has an independent sustainability plan involving grants, donations, and in-kind support from our staff.

ROR is a vital component of the Research Nexus, our vision of a fully connected open research ecosystem. It helps people identify, connect, and analyze the affiliations of those contributing to, producing, and publishing all kinds of research objects. Crossref added support for ROR to its schema and REST API in 2021 and we are [asking Crossref members](/blog/some-rip-roring-news-for-affiliation-metadata/) to use ROR IDs for author affiliations in the metadata they deposit with Crossref. But this post is about how the Crossref community can support ROR in another way.

All three lead organizations---as well as the ROR initiative---have publicly committed to the [POSI Principles](http://openscholarlyinfrastructure.org/) and we know that our diverse and global community is increasingly interested in showing its support for open scholarly infrastructure too. Now there's an opportunity to show that support; the following blog by Maria Gould, cross-posted from the [ROR blog](https://ror.org/blog/2022-02-28-help-sustain-ror/), explains how. 

---

## ROR begins a new round of community fundraising

Since ROR launched in 2019, we have been charting a path to sustainability that leverages our broad community network and diversifies our funding sources. ROR is currently funded through a combination of in-kind support from its three operating organizations, project-based grant funds, and financial contributions from community members.

While ROR aims to minimize overhead and contain costs, it still requires resources to build and maintain the registry's infrastructure, especially as adoption continues to grow. ROR has been working to establish independent revenue streams that complement ROR's in-kind support, avoid dependence on grant funds, and ensure the registry data remains openly available.

This year, ROR is initiating a new round of community fundraising. Building on the [community fundraising campaign](https://ror.org/blog/2019-10-16-help-sustain-ror) we ran during 2019-2021, we are renewing a call for organizations to commit to supporting ROR financially. We are launching a Sustaining Supporters program that opens up new ways for organizations to participate in the collective funding of ROR.

## ROR Sustaining Supporters program

With the Sustaining Supporters program, organizations are encouraged to support ROR's operating expenses on a recurring annual basis. Any organization that signs up to support ROR through the end of 2022 will be recognized as a Founding Supporter and receive a supporter badge that can be displayed on their website.

We want to make the process of contributing to ROR as easy as possible. To ensure this is the case, organizations can support ROR at any amount that works for their budget and capacity. Also, to simplify the invoicing process, organizations that are already members of [Crossref](https://crossref.org/) or [DataCite](https://datacite.org/) can choose to receive an invoice directly from Crossref and DataCite for their ROR contributions. However, if organizations prefer, they can also be invoiced directly from ROR.

## Why support ROR

ROR aims to be an example of the power and potential of community-funded open infrastructure. ROR is committed to providing open, stakeholder-governed infrastructure for research organization identifiers and associated metadata. Implementation of ROR IDs in scholarly infrastructure and metadata enables more efficient discovery and tracking of research outputs across institutions and funding bodies.

The Sustaining Supporters program is the next step in ROR's sustainability journey. ROR is continuing to explore future potential paid service tiers designed for those organizations and companies that rely heavily on our infrastructure, which would complement the supporters program. However, rest assured that any paid services will not impact the availability of ROR data or our commitment to supporting our community, in line with our commitment to the [Principles of Open Scholarly Infrastructure (POSI)](https://openscholarlyinfrastructure.org/).

We've all seen key infrastructure components disappear, be enclosed, or get acquired. We are also realistic about how much effort and cost is involved in sustaining key components of open infrastructure that the scholarly community depends on. And we are committed to doing this right. That means not just sustaining core infrastructures, but investing in them so that they can evolve alongside community needs.

ROR is a free resource for the research community. However, this shared infrastructure does require a collective funding approach that can sustain it as a common good.

## Join us!

This is an exciting moment to be part of ROR's growth. Let's fund open infrastructure together!  

If your organization is interested in supporting ROR and helping to fund open, community-led infrastructure, [sign up here](https://ror.org/sustain/).   
