---
title: 'Rethinking staff travel, meetings, and events'
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
  - Rosa Morais Clark
  - Susan Collins
  - Vanessa Fairhurst
  - Ed Pentz
date: 2022-06-07
categories:
- Community
- Collaboration
- Staff
archives:
    - 2022
---

As a distributed, global, and community-led organisation, sharing information and listening to our members both online and in person has always been integral to what we do.  

For many years Crossref has held both in-person and online meetings and events, which involved a fair amount of travel by our staff, board, and community. This changed drastically in March 2020, when we had to stop traveling and stop having in-person meetings and events. Due to the hard work and creativity of our team and the support of our Ambassadors and Sponsors, we were able to [move to exclusively online meetings and events](/blog/community-outreach-in-2020/) and maintain connections with colleagues, members, and much of the scholarly research community.

Online meetings have benefits compared to in-person ones; they have a much lower carbon footprint, and they can be more inclusive because people don’t have to find the time and money to travel. But there are limitations to online meetings; individual connections made in person do become harder to maintain, and new connections are more difficult to make and grow online. Sometimes just by sitting with someone, meeting their team and drinking their tea, free-flowing conversation leads to real progress.

But with over 17,000 members in 150 countries, our small staff can’t be everywhere, and we need to consider the personal as well as the environmental impacts.

When we started work on the 2022 budget last year, our staff and board took the opportunity to think about our approach, with the goal of not going back to ‘normal’. So we asked ourselves, now that we have a better sense of what works and what doesn't, how can we make our travel and in-person meetings have a greater impact on our goals, while also traveling less and reducing our impact on the environment?

We decided that in the context of our mission and values, we had to take into account three key areas:

1. The environment and climate change
2. Inclusion
3. Work/life balance.

We developed an updated strategy for in-person and online meetings from 2022 onwards along with a set of recommendations and commitments to reduce our carbon footprint. The commitments were approved by the board at its November 2021 meeting.

## Our plan for online and in-person meetings

Online events will generally be aimed at broad groups, in multiple timezones, to inform, update, and test general ideas and assumptions at scale. In contrast, in-person events will be smaller, focusing on deep learning, co-creation, and collaborating through various formats such as workshops, roundtables, or sprints, ideally working toward a specific outcome. These smaller in-person meetings will be scheduled alongside other community events so there will be fewer trips on the whole but each trip more consolidated.

Each in-person meeting will have stated goals such as recruiting and onboarding a new Sponsor, bringing our Ambassadors together to build relationships and share best practices, or getting experts together in a room to help decide important polices, improve some code, or plan new initiatives. At the moment, we are not planning 'hybrid' events as we don't believe they will help meet our goals.

While online meetings and webinars provide a _breadth_ of interactions, in-person meetings can provide greater _depth_ and opportunities for more meaningful engagement and purposeful discussion, and it is this depth that we have missed over the last two and a half years. Therefore, we are identifying focus countries where we plan on engaging more with local community groups. Each country-level engagement plan includes outreach and communications activities and some in-person meetings.  

### Factors and aims for selecting focus countries

Inclusion is important for us and we are committed to supporting the needs of our community members worldwide. We aim to combine meaningful conversations with informational activities. We want to provide time in the day for technical problem solving and/or a more strategically focused session, both of which have worked well in the past. We hope to learn more about trends in our selected focus countries, including the challenges our members face, local publishing norms, barriers to participation in Crossref, and understand and help to adapt government policies.

We consider a number of factors when selecting countries with which to focus our activities:

- Where we have a relatively large number of members.
- Where we are seeing an increase in new members joining.
- Where we have not undertaken engagement activities in at least 3 years.
- Where we have good contacts to collaborate with, i.e., a national funder, a sponsor or ambassador, a government body, or another organization aligned with our mission.
- Where we have very few members but where research output is high according to other sources, in order to understand and overcome barriers to participating in Crossref.
- Where we can consolidate multiple engagement activities in one trip, for example run a LIVE (informational) meeting or workshop, develop relationships with a key Sponsor, or discuss national research policy with government representatives.
- Where we can coordinate our engagement efforts alongside other local community events.

## Our environmental commitments

In line with rethinking how we engage with our members and making sure we do so in the most sustainable, inclusive, and impactful way, we are making the following commitments:

- Crossref staff will think strategically and consider environmental, inclusion, and work/life balance issues when they plan travel. We will make the most of in-person events by focusing on those that involve interaction, such as listening and learning from our members and users, deepening relationships, co-creating, and forming new alliances
- We will travel less and have fewer face-to-face meetings going forward compared with 2019 as a baseline year. The 2022 travel and events budget was reduced by 40% and set at 60% of the 2019 budget. Travel and in-person events for the first half of 2022 have been limited so we will make this same commitment for 2023 still using 2019 as the baseline.
- Crossref will track the carbon footprint of staff travel to meetings and events. We will regularly review the data and find ways to reduce the environmental impact.  

- Combine stakeholder visits with event trips and vice versa whenever possible (if you do 1 plane trip to a location 1000 miles away instead of 2 trips, you reduce your impact by 0.5t)

- As previously planned before the COVID-19 pandemic, the Crossref LIVE Annual Meetings will remain online only and will be held in different time zones. Having them in different time zones will enable global sharing of updates with a lower environmental impact.

- Crossref board meetings will be reduced from three in-person meetings per year to one face-to-face and two online meetings per year.

- Fewer staff will attend fewer in-person conferences and will combine them with other travel.

- For Crossref staff meetings, it is important for our distributed staff to meet face-to-face as a whole organization and as teams. We will plan for one all-staff in person meeting per year (at which there can also be team meetings). Additional team meetings will be based on the reduced travel and meetings budget. Where possible, team meetings will be combined with other meetings (e.g. conferences or other community events).

- While trips that combine meetings may mean longer time away from home, we will still try to avoid staff having to travel or be away on weekends. We will also:

  - Avoid short-haul flights (under 2.5 to 3 hours) where trains are available.
  - Book hotels within walking distance of the event locations (if safe) in order to reduce taxi use.
  - Use public transport and trains (if efficient and safe).
  - Select hotels that have good sustainability plans in place, seeking out ‘green’ hotels where  (if available and within budget).
  - Prioritize locations where the fewest number of staff have to travel or travel the shortest distances.

### Reporting

From now on, we will:

- Track staff travel incl. the number of trips, miles flown, and the carbon impact.
- Estimate the carbon footprint of our two offices, staff home working, our data center, and our cloud infrastructure.
- Track all Crossref-hosted events - in-person and online and review annually (what went well, what can be improved, how to further reduce carbon footprint) as part of the budgeting process.  

Many organizations are now rethinking how to go about travel, conferences, meetings, and work in general. The pandemic may have been the trigger for a big shift in the ways we work and interact, and not all of it was welcome or should continue; however, sometimes it takes a big event to give us the space to sit back, reflect, and change things for the better going forward. As always, we'll evaluate these approaches over time.

All of this means we may be declining some in-person meetings (and when we do, please don’t take it personally) but we still look forward to engaging with our community in a purposeful way.

This feels like a good time to give a shout-out to all our [Ambassadors](/community/our-ambassadors/) and [Sponsors](/membership/about-sponsors/) around the world who are very important for insight and engagement, and we will continue to partner with them for both online and in-person meetings.
