---
title: 'Amendments to membership terms to open reference distribution and include UK jurisdiction'
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
  - Martyn Rittman
  - Amanda Bartell
date: 2022-04-04
categories:
  - References
  - Metadata
  - Board
archives:
    - 2022
---

## Tl;dr

Forthcoming amendments to Crossref's membership terms will include:

1. Removal of 'reference distribution preference' policy: **all references in Crossref will be treated as open metadata from 3rd June 2022.**

2. An addition to sanctions jurisdictions: **the United Kingdom will be added to sanctions jurisdictions that Crossref needs to comply with.**

<br>
Sponsors and members have been emailed today with the 60-day notice needed for changes in terms.

### Reference distribution preferences

In 2017, when we consolidated our metadata services under Metadata Plus, we made it possible for members to set a preference for the [distribution of references](/documentation/content-registration/descriptive-metadata/references/#00564) to Open, Limited, or Closed. Prior to the 2017 change, we acted as a broker of 1:1 feeds of parts of metadata for parts of our community - clearly a role that was not scalable.

We are well underway to pay back technical debt on our 20-year-old metadata system and effectively rearchitect it. We therefore recently needed to decide whether to rewrite code for a capability that hardly any member was using. Just one member has chosen Closed, and Limited was the default for a while, but the vast majority of our members now prefer Open distribution. Additionally, bringing references in line with other metadata significantly simplifies this work and will speed up the technical development.

The Crossref Board discussed the issue in our meeting on 10th March 2022, and voted to remove the reference distribution policy set in 2017. [All board motions](/board-and-governance/#motions) go on our website, and the wording of this particular motion is:

> Resolve that, based on a technical assessment, we will change the reference distribution policy so that all references registered with Crossref are treated the same as other metadata, following a planned transition.

This motion means that 60 days from today---3rd June 2022---all references in Crossref will be open and after that available through our API. As with all other metadata, if members cannot make references available, or do not want them openly distributed, they can choose not to deposit them. However, depositing references is necessary in order to retrieve citation links from our members-only Cited-by API.

Check the documentation for information on how to [deposit references](/documentation/content-registration/descriptive-metadata/references/) and use [Cited-by](/documentation/cited-by/). Also look up your [participation dashboard](https://www.crossref.org/members/prep/) to see if you are already registering references and your current distribution setting.


### Sanctions jurisdictions

Following the UK departing from the European Union, we needed to add the United Kingdom as a separate jurisdiction that we must comply with, alongside the United Nations, the United States of America, and the European Union.

Where there are either relevant financial or governance-based sanctions against individuals, organisations, geographic regions, or whole countries, Crossref is legally bound to comply with these four different jurisdictions. These laws supersede our own governing bylaws.

We have launched a new [operations and sustainability](/operations-and-sustainability) section of our website, which includes [a sanctions page](/operations-and-sustainability/membership-operations/sanctions/) which we will keep updated with any changes and actions we're taking.

## The specific terms that will change

The complete membership terms are [online here](/membership/terms/). In the text below, any text to be removed is shown in 'strike-through' text and any additions are in bold. These new terms will be in effect from 3rd June 2022.

---

**5. Distribution of Metadata by Crossref.** Without limiting the provisions of Section 4 above, the Member acknowledges and agrees that<del>, subject to the Member's reference distribution preference,</del>all Metadata and Identifiers registered with Crossref are made available for reuse without restriction through (but not limited to) public APIs and search interfaces, which enhances discoverability of Content. Metadata and Identifiers may also be licensed to third party subscribers along with an agreement for Crossref to provide third parties with certain higher levels of support and service. <del>For the avoidance of doubt, the scope of Crossref's distribution (if any) of a Member's references is based on such Member's reference distribution preference, as established by the Member in accordance with the "Reference Distribution" page on the Website.</del>

**20. Compliance.** Each of the Member and Crossref shall perform under this Agreement in compliance with all laws, rules, and regulations of any jurisdiction which is or may be applicable to its business and activities, including anti-corruption, copyright, privacy, and data protection laws, rules, and regulations.

The Member warrants that neither it nor any of its affiliates, officers, directors, employees, or members is (i) a person whose name appears on the list of Specially Designated Nationals and Blocked Persons published by the Office of Foreign Assets Control, U.S. Department of Treasury (“OFAC”), (ii) a department, agency or instrumentality of, or is otherwise controlled by or acting on behalf of, directly or indirectly, any such person; (iii) a department, agency, or instrumentality of the government of a country subject to comprehensive U.S. economic sanctions administered by OFAC; or (iv) is subject to sanctions by the United Nations, **the United Kingdom,** or the European Union.

---

As always, please get in touch with us via member@crossref.org with any questions.
