---
title: Content Negotiation for Crossref DOIs
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2011-04-19

categories:
  - DataCite
  - Identifiers
  - Linked Data
  - Metadata
  - Programming
  - Standards
archives:
  - 2011

---
<span >So does anybody remember the posting <a href="/blog/dois-and-linked-data-some-concrete-proposals/">DOIs and Linked Data: Some Concrete Proposals</a>?</span>

<span >Well, we went with option &#8220;D.&#8221;</span>

<span >From now on, DOIs, <i>expressed as <a href="http://en.wikipedia.org/wiki/Uniform_Resource_Identifier">HTTP URI</a>s</i>, can be used with <a href="http://en.wikipedia.org/wiki/Content_negotiation">content-negotiation</a>.</span>

<span >Let’s get straight to the point. If you have <a href="http://curl.haxx.se/">curl</a> installed, you can start playing with content-negotiation and Crossref DOIs right away:</span>

> <span >curl -D - -L -H   &#8220;Accept: application/rdf+xml&#8221; &#8220;`http://dx.doi.org/10.1126/science.1157784`&#8221; </span>
>
> <span >curl -D - -L -H   &#8220;Accept: text/turtle&#8221; &#8220;`http://dx.doi.org/10.1126/science.1157784`&#8221;<br /> </span>
>
> <span >curl -D - -L -H   &#8220;Accept: application/atom+xml&#8221; &#8220;`http://dx.doi.org/10.1126/science.1157784`&#8221; </span>

<span >Or if you are already using Crossref’s &#8220;[unixref](https://www.crossref.org/schema/unixref1.1.xsd)&#8221; format:</span>

> <span >curl -D - -L -H &#8220;Accept: application/unixref+xml&#8221; &#8220;`http://dx.doi.org/10.1126/science.1157784&`#8221; </span>

<span >This will work with over 46 million Crossref DOIs as of today, but the beauty of the setup is that from now on, any <a href="http://www.doi.org/registration_agencies.html">DOI registration agency</a> can enable content negotiation for their constituencies as well. <a href="http://datacite.org/">DataCite</a>- we’re looking at you 😉 .</span>

<span >It also means that, as registration agency members (Crossref publishers, for instance) start providing more complete and richer representations of their content, we can simply redirect content-negotiated requests directly to them.</span>

<span >We expect that that this development will round-out Crossref’s efforts to support standard APIs including <a href="https://support.crossref.org/hc/en-us/articles/214880143">OpenURL</a> and <a href="https://support.crossref.org/hc/en-us/articles/213679866">OAI_PMH</a> and we look forward to seeing DOIs increasingly used in <a href="http://en.wikipedia.org/wiki/Linked_Data">linked data</a> applications.</span>

<span >Finally, Crossref would just like to thank the <a href="http://www.doi.org/foundation/bios.html">IDF</a> and <a href="http://www.cnri.reston.va.us/">CNRI</a> for their hard work on this as well as <a href="http://www.linkedin.com/in/tonyhammond">Tony Hammond</a> and <a href="http://www.ldodds.com/">Leigh Dodds</a> for their valuable advice and persistent goading.</span>
