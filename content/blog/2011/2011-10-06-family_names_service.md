---
title: Family Names Service
author: Karl Ward
authors:
  - Karl Ward
date: 2011-10-06

categories:
  - APIs
  - Family Names
archives:
  - 2011

---
Today I’m announcing [a small web API][1] that wraps a family name database here at Crossref R&D. The database, built from Crossref’s metadata, lists all unique family names that appear as contributors to articles, books, datasets and so on that are known to Crossref. As such the database likely accounts for the majority of family names represented in the scholarly record.

The web API comes with two services: a family name detector that will pick out potential family names from chunks of text and a family name autocompletion system.

Very brief documentation can be found&nbsp;[here][2]&nbsp;along with a jQuery example of autocompletion.

The database is still in development so there may be some oddities and inaccuracies in there. Right now one obvious omission from the name list that I hope to address soon are double-worded names such as &#8220;von Neumann&#8221;. We’re not proposing this database as an authority but rather something that backs a practical service for family name detection and autocompletion.

 [1]: https://web.archive.org/web/20120113212842/http://names.crrd.dyndns.org/
 [2]: https://web.archive.org/web/20120113212842/http://names.crrd.dyndns.org/
