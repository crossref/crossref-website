---
title: 'The GEM program - Year Two 2024'
author: Susan Collins
authors:
  - Susan Collins
  - Johanssen Obanda
draft: false 
date: 2025-02-27
doi: "https://doi.org/10.13003/357841"
categories:
  - GEM
  - Community
  - Membership
  - Equity
archives:
    - 2025
---
  
We began our [Global Equitable Membership (GEM) Program](/gem/) to provide greater membership equitability and accessibility to organizations in the world's least economically advantaged countries. Eligibility for the program is based on a member's country; our list of countries is predominantly based on the [International Development Association (IDA)](https://datahelpdesk.worldbank.org/knowledgebase/articles/906519-world-bank-country-and-lending-groups). Eligible members pay no membership or content registration fees. The list undergoes periodic reviews, as countries may be added or removed over time as economic situations change.

The program began in January 2023 with 214 existing members; and 131 more joined throughout the year. In 2024, we saw 127 organisations joining via the GEM program, bringing the total number of participants to 458.  We welcomed our first-ever members from Sierra Leone and Honduras, as well as our first Sponsor in Bangladesh ([Sponsors](/membership/about-sponsors/) are organizations that work with us to provide administrative, billing, technical, and local language support to the members they work with). 

Of 458 organisations participating in the GEM program, 380 are independent members, 77 are sponsored, and there is one sponsoring organisation. To date, these members have contributed over 279,000 works to the [Research Nexus](/documentation/research-nexus/), our concept of a fully connected global scholarly ecosystem.

Though we have Sponsors based elsewhere, working with members who are in GEM countries (e.g. PKP), we will continue to consult with [our ambassadors](/community/our-ambassadors/) and other partners to identify potential new sponsors that are based in GEM countries.

## Number of Crossref GEM members by country: 

{{% row %}}
{{% column %}}

| GEM Country (Alphabetically) | Total No. <br> of Members |
|-----------------------------|----------------------|
| Afghanistan                | 17                   |
| Bangladesh                 | 120                  |
| Benin                      | 5                    |
| Bhutan                     | 6                    |
| Burkina Faso               | 4                    |
| Burundi                    | 2                    |
| Cambodia                   | 8                    |
| Central African Republic   | 1                    |
| Congo, Democratic Republic | 15                   |
| Ethiopia                   | 13                   |
| Ghana                      | 27                   |
| Guyana                     | 2                    |
| Haiti                      | 1                    |
| Honduras                   | 1                    |
| Kosovo                     | 8                    |
| Kyrgyz Republic            | 23                   |
| Lao, People's Democratic Republic | 2            |
| Madagascar                 | 4                    |
| Malawi                     | 2                    |

{{% /column %}}

{{% column %}}

| GEM Country (Alphabetically)       | Total No. <br>of Members |
|------------------------------------|----------------------|
| Maldives                          | 3                    |
| Mali                              | 3                    |
| Mauritania                        | 1                    |
| Mozambique                        | 2                    |
| Myanmar                           | 1                    |
| Nepal                             | 50                   |
| Nicaragua                         | 2                    |
| Rwanda                            | 7                    |
| Senegal                           | 7                    |
| Sierra Leone                      | 1                    |
| Somalia                           | 9                    |
| Sri Lanka                         | 14                   |
| Sudan                             | 19                   |
| Tajikistan                        | 4                    |
| Tanzania, United Republic of      | 21                   |
| Togo                              | 1                    |
| Uganda                            | 17                   |
| Yemen                             | 30                   |
| Zambia                            | 5                    |


{{% /column %}}
{{% /row %}}

## Number of Crossref members in GEM Program Countries

{{% imagewrap center %}}  
{{< figure src="/images/blog/2025/V2map-gem-program-countries-2025.png" alt="screenshot of mapy showing membership density in GEM Program countries." width="75%" class="img-responsive" >}}  
{{% /imagewrap %}}

We are excited about our in-person event taking place in a few weeks in Accra, Ghana, as a direct result of the increasing participation and interest in Crossref from the region. 

We can see a clear connection between outreach activities conducted by us and our Ambassadors and the increase in awareness and the number of members joining from related countries. These were Bangladesh, Nepal, Uganda, and Tanzania in 2023, and Ghana, Zambia, Sri Lanka, and Tanzania in 2024. 

From our Ambassadors’ activities in the GEM countries, some recurring questions emerged highlighting barriers to joining Crossref. It’s important to recognise that many institutions struggle with funding and technical expertise. It’s no surprise that they are often concerned with the maintenance of their membership over the long term. We emphasize that GEM is a sustained measure to accommodate knowledge-sharing organisations from the regions of financial strain. Whilst the program addresses the costs of membership and content registration, our Ambassadors can assist further, offering technical support with record registration, metadata best practices, and integrating Crossref services with existing systems, including Open Journal Systems (OJS); and discuss how registering metadata improves research visibility. 

We are grateful to our Ambassadors for directly supporting the GEM program within their countries through webinars and presenting in person at conferences: Shaharima Parvin and MD Jahangir in Bangladesh, Richard Bruce Lamptey in Ghana, Niranjan Koirala in Nepal, Oumy Ndiaye in Senegal, Lasith Gunawardena in Sri Lanka, and Baraka Manjale Ngussa in Tanzania.
