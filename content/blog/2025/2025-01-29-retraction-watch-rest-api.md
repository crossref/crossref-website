---
title: 'Retraction Watch retractions now in the Crossref API'
author: Martyn Rittman
authors:
  - Martyn Rittman
draft: false 
date: 2025-01-29
doi: "https://doi.org/10.13003/692016"
categories:
  - REST API
  - Retraction watch
  - Research integrity
archives:
    - 2025
---
  
Retractions and corrections from Retraction Watch are now available in Crossref’s REST API. Back in September 2023, we announced the acquisition of the Retraction Watch database with an ongoing shared service. Since then, they have sent us regular updates, which are publicly available as a [csv file](https://gitlab.com/crossref/retraction-watch-data). Our aim has always been to better integrate these retractions with our existing metadata, and today we’ve met that goal.

This is the first time we have supplemented our metadata with a third-party data source. Until now, our APIs have included metadata provided by Crossref members along with outputs from our internal enrichment workflows, such as matches found for bibliographic reference matching and funders. Third party metadata has been gathered in Event Data, but this has been stored and delivered separately. 

Knowing when work has been retracted is critical for assessing the integrity of research, and this enhancement of the data will be a great benefit to the community.

## Where does the data come from?

Retraction Watch carefully curates retractions, pulling them from several non-Crossref sources, including PubMed and publisher websites. Each entry is manually checked and annotated before being added to the database. The high level of curation and broad coverage is what made a partnership between Crossref and Retraction Watch attractive, and our shared goal of making changes to metadata more visible.

> "Our goal with the Retraction Watch Database has always been for it to be as useful to as many people as possible, and available from as many sources as possible,” says Ivan Oransky, co-founder of Retraction Watch and executive director of The Center For Scientific Integrity, its parent nonprofit organization. “Integration with Crossref’s REST API is a huge step in that direction.”

## Where can I see the retractions?

If you use a service that collects Crossref metadata, you will start to see the Retraction Watch retractions as they are picked up. To access the data directly, you can find retractions from both Crossref members and Retraction Watch in our REST API, for example with the following request for all retractions:

[https://api.crossref.org/v1/works?filter=update-type:retraction](https://api.crossref.org/v1/works?filter=update-type:retraction)

Or for an individual record:

[https://api.crossref.org/v1/works/10.1177/17588359231172420](https://api.crossref.org/v1/works/10.1177/17588359231172420)

In the results here you will see an `update-to` field:

```
"update-to": [
  {
    "updated": {
      "date-parts": [
        [2023,4,22]
      ],
      "date-time": "2023-04-22T00:00:00Z",
      "timestamp": 1682121600000
    },
    "DOI": "10.1177/1758835920922055",
    "type": "retraction",
    "source": "publisher",
    "label": "Retraction"
  },
  {
    "updated": {
      "date-parts": [
        [2023,4,22]
      ],
      "date-time": "2023-04-22T00:00:00Z",
      "timestamp": 1682121600000
    },
    "DOI": 10.1177/17588359231172420",
    "type": "retraction",
    "source": "retraction-watch",
    "label": "Retraction",
    "record-id": 44124
  }
]
```

The `source` field states where the retraction came from. Currently, it can have two values: `publisher` or `retraction-watch`. Note that the same retraction may be included multiple times from different sources. 

Retraction Watch retractions will remain available [on Gitlab in csv format](https://gitlab.com/crossref/retraction-watch-data) and be updated on working days. The `record-id` refers to the entry in the csv file with further details, such as the reason for retraction. 

There is [full documentation available for the Crossref REST API](https://api.crossref.org/swagger-ui/index.html) and if you are new to REST APIs, see our [learning hub](https://www.crossref.org/learning/) to get started which includes [a tutorial](https://crossref.gitlab.io/tutorials/get-rw-metadata/) about accessing retractions.

## What can I do with the retractions?

Like the rest of our metadata, the retractions are freely available. If you use or operate a tool that ingests retractions, the new entries will start to be picked up immediately. The Retraction Watch database includes a larger number of retractions than the Crossref database, so you should see an increase in the total.

We have heard from organisations that would like to build new research integrity tools based on this data. We look forward to seeing the benefits brought by wider availability of the Retraction Watch retractions, and how they can provide better context to research outputs.

While Crossref metadata is freely available to reuse without a license, if you make use of the Retraction Watch retraction metadata in a published work, we kindly request that you provide a citation to the source.

If you have questions or comments, please head over to the [section of our forum](https://community.crossref.org/c/strategy/research-integrity/46) dedicated to integrity of the scholarly record.

