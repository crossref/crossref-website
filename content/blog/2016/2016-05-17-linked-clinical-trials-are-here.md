---
title: Clinical trial data and articles linked for the first time
author: Daniel Shanahan
slug: "/linked-clinical-trials-are-here"
authors:
  - Daniel Shanahan
  - Kirsty Meddings
date: 2016-05-17

categories:
  - Clinical Trials
  - Crossmark
  - Interoperability
  - Linked Data
archives:
  - 2016

---
<span >It’s here. After years of hard work and with a huge cast of characters involved, I am delighted to announce that you will now be able to instantly link to all published articles related to an individual clinical trial through the Crossmark dialogue box. Linked Clinical Trials are here!</span>

<span >In practice, this means that anyone reading an article will be able to pull a list of both clinical trials relating to that article and all other articles related to those clinical trials – be it the protocol, statistical analysis plan, results articles or others – all at the click of a button.</span> <figure id="attachment_1644"  class="wp-caption aligncenter">

[<img class="wp-image-1644 size-medium" src="/wp/blog/uploads/2016/05/crossmark_example-2_720-300x286.jpg" width="300" height="286" srcset="/wp/blog/uploads/2016/05/crossmark_example-2_720-300x286.jpg 300w, /wp/blog/uploads/2016/05/crossmark_example-2_720.jpg 720w" sizes="(max-width: 300px) 85vw, 300px" />][1]<figcaption class="wp-caption-text">Linked Clinical Trials interface</figcaption></figure>

<span >Now I’m sure you’ll agree that this sounds nifty. It’s definitely a ‘nice-to-have’. But why was it worth all the effort? Well, simply put: “to move a mountain, you begin by carrying away the small stones”.</span>

<span >Science communication in its current form is an anachronism, or at the very least somewhat redundant.</span>

<span >You may have read about the <a href="http://www.apa.org/monitor/2015/10/share-reproducibility.aspx">‘crisis in reproducibility’</a>. Good science, at its heart, should be testable, falsifiable and reproducible, but an historical over-emphasis on results has led to a huge number of problems that seriously undermine the integrity of the scientific literature.</span>

<span >Issues such as publication bias, selective reporting of outcome and analyses, hypothesising after the results are known (HARKing) and p-hacking are widespread, and can seriously distort the literature base (unless anyone seriously considers <a href="http://tylervigen.com/spurious-correlations">Nicholas Cage to be causally related to people drowning in swimming pools</a>).</span>

<span >This is, of course, nothing new. Calls for prospective registration of clinical trials <a href="http://www.ncbi.nlm.nih.gov/pubmed/3760920">date back to the 1980s</a> and it is now becoming increasingly commonplace, recognising that the quality of research lies in the questions it asks and the methods it uses, not the results observed.</span><figure id="attachment_1581"  class="wp-caption aligncenter">

[<img class="wp-image-1581" src="/wp/blog/uploads/2016/04/Trial-registration.jpg" alt="Uptake of trial registration since 2000" width="600" height="350" srcset="/wp/blog/uploads/2016/04/Trial-registration.jpg 868w, /wp/blog/uploads/2016/04/Trial-registration-300x175.jpg 300w, /wp/blog/uploads/2016/04/Trial-registration-768x448.jpg 768w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px" />][2]<figcaption class="wp-caption-text">Uptake of trial registration year-on-year since 2000</figcaption></figure>

<span >Building on this, a number of journals and funders – starting with BioMed Central’s <em>Trials</em> <a href="http://trialsjournal.biomedcentral.com/articles/10.1186/1468-6708-6-15">over 10 years ago</a> – have also pushed for the prospective publication of a study’s protocol and, more recently, statistical analysis plan. The idea that null and non-confirmatory results have value and should be published has also gained increasing support.</span>

<span >Over the last ten years, there has been a general trend towards increasing transparency. So what is the problem? Well, to borrow an analogy from Jeremy Grimshaw, co-Editor-in-Chief of <a href="http://trialsjournal.biomedcentral.com/"><em>Trials</em></a> – we’ve gone from <a href="http://blogs.biomedcentral.com/on-medicine/2014/05/30/the-consort-statement-in-2014/">Miró to Pollock</a>.</span>

<span >Although a results paper may reference a published study protocol, there is nothing to link that report to subsequent published articles; and no link from the protocol itself to the results article.</span>

<span >A <a href="http://trialsjournal.biomedcentral.com/articles/10.1186/1745-6215-15-369">single clinical trial can result in multiple publications</a>: the study protocol and traditional results paper or papers, as well as commentaries, secondary analyses and, eventually, systematic reviews, among others, many published in different journals, years apart. This situation is further complicated by an ever-growing body of literature.</span>

<span >Researchers need access to all of these articles if they are to reliably evaluate bias or selective reporting in a research object, but – as any systematic reviewer can tell you – actually finding them all is like looking for a needle in a haystack. When you don’t know how many needles there are. With the haystack still growing.</span>

<span >That’s where we come in. The advent of trial registration means that there is a unique identifier associated with every clinical trial, at the study-level, rather than the article level. Building on this, the <a href="http://blogs.biomedcentral.com/on-medicine/2014/01/31/threaded-publications-one-step-closer/">Linked Clinical Trials project</a> set out to connect all articles relating to an individual trial together using its trial registration number (TRN).</span>

<span >By adapting the existing Crossmark standard, we have captured additional metadata about an article, namely the TRN and the trial registry, with this information then associated with the article’s DOI on publication. This means that you will be able to pull all articles related to an individual clinical trial from the Crossmark dialogue box on any relevant article. </span>

<span >This obviously has huge implications for the way science is reported and used. By quickly and easily linking to related published articles, it will enable editors, reviewers and researchers to evaluate any selective reporting in the study, and help to provide far greater context for the results.</span>

<span >As all the metadata will be open access (CC0), with no copyright, it will also be possible to access this article ‘thread’ through the Crossref Metadata Search, or independently through an application programming interface (API). This provides a platform for others to build on, with many already looking to take the next step, such as Ben Goldacre’s new <a href="http://trialsjournal.biomedcentral.com/articles/10.1186/s13063-016-1290-8">Open Trials initiative</a>.</span>

<span >However, in order for this to work, we must capture as many articles and trials as possible to create a truly comprehensive thread of publications. We currently have data from the NIHR Libraries, PLoS and, of course, BioMed Central, but need more publishers and journals to join us in depositing clinical trial metadata. After all, without metadata, this is all merely wishful thinking.</span>

<span >Let’s hope we’re the pebble that starts the landslide.</span>

 [1]: /wp/blog/uploads/2016/05/crossmark_example-2_720.jpg
 [2]: /wp/blog/uploads/2016/04/Trial-registration.jpg
