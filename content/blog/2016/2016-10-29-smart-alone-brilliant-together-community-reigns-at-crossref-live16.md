---
title: Smart alone; brilliant together. Community reigns at Crossref LIVE16
author: Ginny Hendricks
authors:
  - Ginny Hendricks
date: 2016-10-29
categories:
  - Crossref LIVE
  - Annual Meeting
  - Member Briefing
  - Community
  - Election
archives:
  - 2016

---
<span >A bit different from our traditional meetings, <a href="https://www.eventbrite.com/e/crossref-live16-registration-25928526922#">Crossref LIVE16</a> next week is the first of a totally new annual event for the scholarly communications community.  Our theme is <span ><strong>Smart alone; brilliant together</strong></span>.  We have a broad program of both informal and plenary talks across two days. There will be stations to visit, conversation starters, and entertainment, that highlight what our community can achieve if it works together. </span>

> _<span ><a href="https://crossreflive16.sched.com/">Check out the final program</a>.</span>_

<!--more--><span >We’re now opening the doors to all parties&#8212;our 5,000+ members of all shapes and sizes&#8212;as well as the technology providers, funders, libraries, and researchers that we work with.  </span><span >Our aim is to gather the ‘metadata-curious’ and have more opportunities to talk face-to-face to share ideas and information, see live demos, and get to know one another.</span>

<p >
  <span ><strong><span >Mashup Day - Tuesday 1st November 12-5pm.</span></strong>  An &#8216;open house’ vibe, we’ll have several stations to visit each Crossref team, a LIVE Lounge, good food, and guest areas run by our friends at <span >DataCite</span>, <span >ORCID</span>, and <span >Turnitin</span>.  We’ll have some special programming too, on-the-hour lightning talks, including </span><span >a wild talk at 2pm from a primatologist who speaks baboon! </span>
</p>

<p >
  <span ><strong><span >Conference Day - Wednesday 2nd November 9am-5pm.</span></strong>  There is more of a formal plenary agenda this day, with keynote speakers from across the scholarly communications landscape.  Our primary goal is to share Crossref strategy and plans, alongside thought-provoking perspectives from our guest speakers.  We’ll hear from many corners of our community including:</span>
</p>

<li >
  <span >Funder program officer, Carly Strasser (Moore Foundation) on &#8220;<span >Publishers and funders as agents of change</span>&#8220;, </span>
</li>
<li >
  <span >Data scientist, Ian Calvert (Digital Science) on &#8220;<span >You don’t have metadata</span>&#8220;, </span>
</li>
<li >
  <span >Open knowledge advocate, Dario Taraborelli (The Wikimedia Foundation) on &#8220;<span >Citations for the sum of all human knowledge</span>&#8220;, and</span>
</li>
<li >
  <span >Scholarly communications librarian, April Hathcock (New York University) on &#8220;<span >Opening up the margins</span>&#8220;. </span>
</li>

<p >
  <span ><span ><span >For our part, we will set out Crossref’s &#8220;<span >strategy and key priorities</span>&#8221; (Ed Pentz), &#8220;<span >A vision for membership</span>&#8221; (me, Ginny Hendricks), &#8220;<span >The meaning of governance</span>&#8221; (Lisa Hart Martin), &#8220;<span >The case of the missing leg</span>&#8221; (Geoffrey Bilder),&#8221;<span >New territories in the scholarly research map</span>&#8221; (Jennifer Lin), and &#8220;<span >Relationships and other notable things</span>&#8221; (Chuck Koscher).  </span></span></span>
</p>

<p >
  <span ><span ><span >We will also set aside thirty minutes fo</span>r the important Crossref annual business meeting, when we will announce the results of the <span ><a href="/blog/one-member-one-vote-crossref-board-election-opens-today-september-30th/">membership’s vote</a>, and welcome new board members.</span></span></span>
</p>

<span >I can’t wait to welcome you all.</span>

# <span >Have you voted?</span>

<span >If you’re a voting member of Crossref you’ll have cast your vote already I hope! I’m so happy to see that people have voted in record numbers although it’s under 7% of our eligible members which is not high&#8230; more on member participation next week.</span>
