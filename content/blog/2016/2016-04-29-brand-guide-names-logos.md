---
title: 'Crossref Brand update: new names, logos, guidelines, + video'
slug: 'crossref-brand-update-new-names-logos-guidelines-and-video'
author: Ginny Hendricks
authors:
  - Ginny Hendricks
date: 2016-04-29

categories:
  - Brand
  - Member Briefing
archives:
  - 2016

---


It can be a pain when companies rebrand as it usually requires some coordinated updating of wording and logos on websites, handouts, and slides. Nevermind changing habits and remembering to use the new names verbally in presentations.  

## Why bother?

As our infrastructure and services expanded, we sometimes branded services with no reference to Crossref. As explained in our [The Logo Has Landed post](/blog/the-logo-has-landed/)  last November, this has led to confusion, and it was not scalable nor sustainable.   

With a cohesive approach to naming and branding, the benefits of changing to (some) new names and logos should help everyone. Our aim is to stem confusion and be in a much better position to provide clear messages and useful resources so that people don’t have to try hard to understand what Crossref enables them to do. 

So while it may be a bit of a pain short-term, it will be worth it!  

## What are the new names?

As a handy reference, here is a slide-shaped image giving an overview of our services with their new names:  

{{< figure src="/wp/blog/uploads/2016/04/Overview-of-brand-name-changes-April-2016.png" caption="Overview of brand name changes, April 2016" width="100%" >}}

## It’s a lowercase &#8216;r’ in Crossref

That’s right, you’ve spent fifteen years learning to capitalize the second R in Crossref, and now we’re asking you to lowercase it! Please say hello to and start to embrace the more natural and contemporary **Crossref**.  

## Reference logos from our new CDN via [assets.crossref.org](http://assets.crossref.org)

I’m hoping we can count on our community to update logos and names on your end, keeping consistent with new brand guidelines. And I hope we can make it as easy as possible to do: 

- This Content Delivery Network (CDN) at [assets.crossref.org](http://assets.crossref.org) allows you to reference logos using a snippet of code. Please do not copy/download the logos.

- This [set of brand guidelines for members](http://outreach.crossref.org/acton/ct/16781/s-0038-1604/Bct/l-001d/l-001d:282/ct2_0/1?sid=xd9u0mOai).  

We also have a new website in development which will put support and resources front and center of the user experience. More on that in the next month or two.  

By using the snippets of code provided via our new CDN at [assets.crossref.org](http://assets.crossref.org), these kind of manual updates should never be a problem in the future if the logo changes again (no plans anytime soon!).

Of course, we don’t expect people to update new logos and names immediately, there is always a period of transition. Please [let us know](mailto:feedback@crossref.org) let us know if we can help you to update your sites and materials in the coming weeks.  

Also, check out [the launch video](https://www.youtube.com/watch_popup?v=_Bm2r59TG1I), which presents five key Crossref brand messages:  
