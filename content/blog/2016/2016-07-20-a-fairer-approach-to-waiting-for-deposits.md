---
title: A fairer approach to waiting for deposits
author: Chuck Koscher
authors:
  - Chuck Koscher
date: 2016-07-20

categories:
  - Content Registration
  - Crossref System
  - Metadata
  - News Release
archives:
  - 2016

---
If you ever see me in the checkout line at some store do not _ever_ get in the line I’m in. It is always the absolute slowest.

Crossref’s metadata system has a sort of checkout line, when members send in their data they got processed essentially in a first come first served basis. It’s called the deposit queue. We had controls to prevent anyone from monopolizing the queue and ways to jump forward in the queue but our primary goal was to give everyone a fair shot at getting processed as soon as possible. With many different behaviors by our members this could often be a challenge and at times some folks were not 100% happy.   

[<img class="alignleft wp-image-1903 size-medium" src="/wp/blog/uploads/2016/07/depositwars-300x75.png" width="300" height="75" srcset="/wp/blog/uploads/2016/07/depositwars-300x75.png 300w, /wp/blog/uploads/2016/07/depositwars-768x192.png 768w, /wp/blog/uploads/2016/07/depositwars-1024x256.png 1024w, /wp/blog/uploads/2016/07/depositwars.png 1200w" sizes="(max-width: 300px) 85vw, 300px" />][1]We recently made a change where the queue now cycles through all waiting users and selects a job from each. This means that low-frequency users will always get a pretty fast service even if there are a lot of unique users waiting. Everyone gets one bite of the apple on each cycle through the waiting list. Of course, we still have some special controls to help deal with large quantities of files from a single user and ways to jump the queue under really special circumstances.

We believe this will, on average,  yield a better experience and minimize the backups that formerly required administrator attention to resolve.

 [1]: /wp/blog/uploads/2016/07/depositwars.png
