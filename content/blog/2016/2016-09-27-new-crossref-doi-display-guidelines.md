---
title: New Crossref DOI display guidelines are on the way
author: Ed Pentz
authors:
  - Ed Pentz
date: 2016-09-27

categories:
  - DOIs
  - Identifiers
  - Linking
  - Publishing
  - Member Briefing
archives:
  - 2016

---
### <span >TL;DR</span>

<span >Crossref will be updating its DOI Display Guidelines within the next couple of weeks.  This is a big deal.  We last made a change in 2011 so it’s not something that happens often or that we take lightly.  In short, the changes are to drop “dx” from DOI links and to use “http<span ><strong>s</strong></span>:” rather than “http:”.  An example of the new best practice in displaying a Crossref DOI link is: <a href="https://doi.org/10.1629/22161">https://doi.org/10.1629/22161</a></span><!--more-->

### <span >Hey Ho, &#8220;doi:&#8221; and &#8220;dx&#8221; have got to go</span>

<span >The updated Crossref DOI Display guidelines recommend that https://doi.org/ be used and not http://dx.doi.org/ in DOI links.  Originally the &#8220;dx&#8221; separated the DOI resolver from the International DOI Foundation (IDF) website but this has changed and the IDF has already updated its recommendations so we are bringing ours in line with theirs.</span>

<span >We are also recommending the use of HTTP<span ><strong>S</strong></span> because it makes for more sec</span>ure browsing.  When you use an HTTPS link, the connection between the person who clicks the DOI and the DOI resolver is secure.  This means it can’t be tampered with or eavesdropped on.  The DOI resolver will redirect to both HTTP and HTTPS URLs.

### <span >Timing and backwards compatibility</span>

<span ><span >We are requesting all Crossref member publishers and anyone using Crossref DOIs to start following the updated guidelines as soon as possible.  But realistically we are setting a goal of <span ><strong>six months</strong></span> for implementation; we realize that updating systems and websites can take time.  We at Crossref will also be updating our systems within six months - </span><span >we already use HTTPS for some of our services and our new website (coming very soon!) will use HTTPS. </span></span>

<span ><span >An important point about backwards compatibility is that “</span><a href="http://dx.doi.org/"><span >http://dx.doi.org/</span></a><span >” and “</span><a href=http://doi.org/><span >http://doi.org/</span></a><span >” are valid and will continue to work forever-or as long as Crossref DOIs continue to work-and we plan to be around a long time.</span></span>

### <span >We need to do better</span>

<span >Reflecting on the 2011 update to the display guidelines it’s fair to say that we have been disappointed.  It is still much too common to see unlinked DOIs in the form doi:10.1063/1.3599050 or DOI: 10.1629/22161 or even unlinked in this form: http://dx.doi.org/10.1002/poc.3551 </span>

<span ><span >What’s so wrong with this approach?  To demonstrate, please click on this DOI doi:10.1063/1.3599050 - oh, you can’t click on it?  How about I send you to a real example of a publisher page.  What I’d like you to do is click the following link and then copy the DOI you find there and come back - </span><a href="http://dx.doi.org/10.1002/poc.3551"><span >http://dx.doi.org/10.1002/poc.3551</span></a><span >. </span></span>

<span ><span >Are you back? I expect you had to carefully highlight the “10.1063/1.3599050” and then do “edit”, “copy”.  That wasn’t too bad but the next step is to put the DOI into an email and send it to someone.  But wait - what are they going to do with “10.1063/1.3599050”?  It’s useless.  If you want it to be useful you’ll have to add “</span><a href="http://doi.org"><span >http://doi.org</span></a><span >” or </span><a href="https://doi.org/"><span >https://doi.org/</span></a><span > in the front. </span></span>

<span ><span >When publishers follow the guidelines it makes things easier - if you go to </span><a href="https://doi.org/10.1063/1.3599050"><span >https://doi.org/10.1063/1.3599050</span></a><span > you’ll note that you can just right click on the full DOI link on the page and get a full menu of options of what to do with it.  One of which is to copy the link and then you can easily paste into an email or anywhere else.</span></span>

<span >However-putting a positive spin on the spotty adherence to the 2011 update to the DOI display guidelines-everyone has another chance with the latest set of updates to make all the changes at once! </span>

### <span >More on HTTPS (future-proofing scholarly linking)</span>

<span >We take providing the central linking infrastructure for scholarly publishing seriously.  Because we form the link between publisher sites all over the web, it’s important that we do our bit to enable secure browsing from start to finish.  In addition, HTTPS is now a ranking signal for Google <a href="https://webmasters.googleblog.com/2014/08/https-as-ranking-signal.html">who gives sites using HTTPS a small ranking boost</a>.</span>

<span >The process of enabling HTTPS on publisher sites will be a long one and, given the number of members we have, it may a while before everyone’s made the transition.  But by using HTTPS we are future-proofing scholarly linking on the web.</span>

<span >Some years ago we started the process of making our new services available exclusively over HTTPS.  The Crossref Metadata API is HTTPS enabled, and Crossmark and our Assets CDN use HTTPS exclusively. Last year we collaborated with Wikipedia to make all of their DOI links HTTPS.  We hope that we’ll start to see more of the scholarly publishing industry doing the same.</span>

<span >So-it’s simple-always make the DOI a full link - <a href="https://doi.org/10.1006/jmbi.1995.0238">https://doi.org/10.1006/jmbi.1995.0238</a> - even when it’s on the abstract or full text page of the content that the DOI identifies - and use “<a href="https://doi.org/">https://doi.org/</a>”. </span>
