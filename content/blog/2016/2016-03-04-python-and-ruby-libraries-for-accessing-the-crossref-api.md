---
title: Python and Ruby Libraries for accessing the Crossref API
author: Scott Chamberlain
authors:
  - Scott Chamberlain
date: 2016-03-04

categories:
  - APIs
  - Metadata
  - Programming
archives:
  - 2016

---
<span >I’m a co-founder with <a href="https://ropensci.org/">rOpenSci</a>, a non-profit that focuses on making software to facilitate reproducible and open science. <a href="https://github.com/ropensci/rcrossref/commit/a264da3177d2bdbdfce289a4fdccc43c8df36da1">Back in 2013</a> we started to make an R client working with various Crossref web services. I was lucky enough to attend <a href="/crossref-annual-meeting/archive/">last year’s Crossref annual meeting in Boston</a>, and gave one talk on <a href="https://youtu.be/_2iRjK5QjKU?si=qzAvJ70n_kaMJpmU">details of the programmatic clients</a>, and another higher level talk on <a href="https://youtu.be/j8qlHw7UqlI?si=qWY4NXls4w4jwZ3I">text mining and use of metadata for research</a>.</span>

<span >Crossref has a newish API encompassing works, journals, members, funders and more (check out <a href="https://github.com/Crossref/rest-api-doc/blob/master/rest_api.md">the API docs</a>), as well as a few other services. Essential to making the Crossref APIs easily accessible&#8212;and facilitating easy tool/app creation and exploration&#8212;are programmatic clients for popular languages. I’ve maintained an R client for a while now, and have been working on Python and Ruby clients for the past four months or so.</span>

<span >The R client falls squarely into the analytics/research use cases, while the Python and Ruby clients are ideal for general data access and use in web applications (the Javascript library below as well).</span>

<span >I’ve strived to make each client in idiomatic fashion according to the language. Due to this fact, there is not generally correspondence between the different clients with respect to data outputs. However, I’ve tried to make method names similar across Ruby and Python; although the R client is quite a bit older, so method names differ from the other clients and I’m resistant to changing them so as not to break current users’ projects. In addition, R users are likely to want a data.frame (i.e., table) of results, so we give back that - whereas with Python and Ruby we give back dictionaries and hashes, respectively.</span>

### <span ><strong>Crossref clients</strong></span>

  * <span >Python:</span>
      * <span >Source: <a href="https://github.com/sckott/habanero">https://github.com/sckott/habanero</a></span>
      * <span >Pypi: <a href="https://pypi.python.org/pypi/habanero">https://pypi.python.org/pypi/habanero</a></span>
  * <span >Ruby:</span>
      * <span >Source: <a href="https://github.com/sckott/serrano">https://github.com/sckott/serrano</a></span>
      * <span >Rubygems: <a href="https://rubygems.org/gems/serrano">https://rubygems.org/gems/serrano</a></span>
      * <span >`serrano` also comes with a command line tool of the same name that’s installed when you install `serrano` (examples below)</span>
  * <span >R:</span>
      * <span >Source: <a href="https://github.com/ropensci/rcrossref">https://github.com/ropensci/rcrossref</a></span>
      * <span >CRAN: <a href="https://cran.rstudio.com/web/packages/rcrossref/">https://cran.rstudio.com/web/packages/rcrossref/</a></span>
  * <span >Javascript:</span>
      * <span >Source: <a href="https://github.com/scienceai/crossref">https://github.com/scienceai/crossref</a></span>
      * <span >NPM: <a href="https://www.npmjs.com/package/crossref">https://www.npmjs.com/package/crossref</a></span>

<span >I’ll cover the Python, Ruby, and R libraries below.</span>

### <span ><strong>Installation</strong></span>

<span ><strong><em>Python</em></strong></span>

<span >on the command line</span>

<pre class="theme:solarized-light lang:sh decode:true"><span >pip install habanero</span></pre>

<span ><em><strong>Ruby</strong></em></span>

<span >on the command line</span>

<pre class="theme:solarized-light lang:sh decode:true"><span >gem install serrano</span></pre>

<span ><em><strong>R</strong></em></span>

<span >in an R session</span>

<pre class="theme:solarized-light lang:r decode:true"><span >install.packages("rcrossref")</span></pre>

&nbsp;

### <span ><strong>Examples</strong></span>

<span >Output is indicated by the syntax `#>` in all examples below.</span>

<span ><em><strong>Python</strong></em></span>

<span >in a Python REPL (e.g. <em>iPython</em>)</span>

<span >Import the <em>Crossref</em> module from within <em>habanero</em>, and initialize a client</span>

<pre class="theme:solarized-light lang:python decode:true"><span >from habanero import Crossref
cr = Crossref()</span></pre>

<span >Query for the phrase &#8220;ecology&#8221;</span>

<pre class="theme:solarized-light lang:python decode:true"><span >x = cr.works(query = "ecology", limit = 5)</span></pre>

<span >Index to various parts of the output</span>

<pre class="theme:solarized-light lang:python decode:true"><span >x['message']['total-results']
#&gt; 276188</span></pre>

<span >Extract similar data items from each result. The records are in the &#8220;items&#8221; slot</span>

<pre class="theme:solarized-light lang:python decode:true"><span >[ z['DOI'] for z in x['message']['items'] ]
#&gt; [u'10.1002/(issn)1939-9170',
#&gt;  u'10.4996/fireecology',
#&gt;  u'10.5402/ecology',
#&gt;  u'10.1155/8641',
#&gt;  u'10.1111/(issn)1439-0485']</span></pre>

<span >In <em>habanero</em> for some methods we require you to instantiate a client.</span>

<span >You can set a base URL and API key. This is a future looking feature</span>

 <span >as Crossref API does not require an API key.</span>

<span >Note: I’ve tried to make sure habanero is Python 2 and 3 compatible. Hopefully you’ll find that’s true.</span>

<span ><em><strong>Ruby</strong></em></span>

<span >in a Ruby repl (e.g., <a href="https://web.archive.org/web/20160312125404/http://pryrepl.org//">pry</a>), load <em>serrano</em></span>

<pre class="theme:solarized-light lang:ruby decode:true "><span >require 'serrano'</span></pre>

<span >Query for &#8220;peerj&#8221; on the journals route</span>

<pre class="theme:solarized-light lang:ruby decode:true"><span >x = Serrano.journals(query: "peerj")</span></pre>

<span >Collect just ISSN’s from each result</span>

<pre class="theme:solarized-light lang:ruby decode:true"><span >x['message']['items'].collect { |z| z['ISSN'] }
#&gt; =&gt; [["2376-5992"], ["2167-8359"]]</span></pre>

&nbsp;

<span ><em><strong>Shell</strong></em></span>

<span >The `serrano` command line tool is quite powerful if you are used to doing things there.</span>

<span >Here, search for one article; summary data is shown.</span>

<pre class="theme:solarized-light lang:sh decode:true"><span >serrano works 10.1371/journal.pone.0033693
#&gt; DOI: 10.1371/journal.pone.0033693
#&gt; type: journal-article
#&gt; title: Methylphenidate Exposure Induces Dopamine Neuron Loss and Activation of Microglia in the Basal Ganglia of Mice</span></pre>

<span >There’s also a `-json` flag to give back JSON data, which can be parsed with the command line tool <a href="https://stedolan.github.io/jq/">jq</a>.</span>

<pre class="theme:solarized-light lang:sh decode:true"><span >serrano works --filter=has_full_text:true --json --limit=5 | jq '.message.items[].link[].URL'
#&gt; "http://api.wiley.com/onlinelibrary/tdm/v1/articles/10.1002%2F9781119208082.ch9"
#&gt; "http://api.wiley.com/onlinelibrary/tdm/v1/articles/10.1002%2F9781119208082.index"
#&gt; "http://api.wiley.com/onlinelibrary/tdm/v1/articles/10.1002%2F9781119208082.ch11"
#&gt; "http://api.wiley.com/onlinelibrary/tdm/v1/articles/10.1002%2F9781119208082.ch15"
#&gt; "http://api.wiley.com/onlinelibrary/tdm/v1/articles/10.1002%2F9781119208082.ch4"</span></pre>

&nbsp;

<span ><em><strong>R</strong></em></span>

<span >In an R session, load `rcrossref`</span>

<pre class="theme:solarized-light lang:r decode:true "><span >library("rcrossref")</span></pre>

<span >Search the `works` route for the phrase &#8220;science&#8221;</span>

<pre class="theme:solarized-light lang:r decode:true"><span >res &lt;- cr_works(query = "science", limit = 5)
#&gt; $meta
#&gt;   total_results search_terms start_index items_per_page
#&gt; 1       4333827      science           0              5
#&gt;
#&gt; $data
#&gt; Source: local data frame [5 x 23]
#&gt;
#&gt;   alternative.id container.title    created  deposited                        DOI funder    indexed
#&gt;            (chr)           (chr)      (chr)      (chr)                      (chr)  (chr)      (chr)
#&gt; 1                                2013-11-21 2013-11-21            10.1126/science &lt;NULL&gt; 2015-12-27
#&gt; 2                  Science Askew 2004-11-26 2013-12-16 10.1887/0750307145/b426c18 &lt;NULL&gt; 2015-12-24
#&gt; 3                                2006-04-10 2010-07-30    10.1002/(issn)1557-6833 &lt;NULL&gt; 2015-12-25
#&gt; 4                                2013-08-27 2013-08-27    10.1002/(issn)1469-896x &lt;NULL&gt; 2015-12-27
#&gt; 5                                2013-12-19 2013-12-19                10.5152/bs. &lt;NULL&gt; 2015-12-28
#&gt; Variables not shown: ISBN (chr), ISSN (chr), issued (chr), link (chr), member (chr), prefix (chr), publisher
#&gt;   (chr), reference.count (chr), score (chr), source (chr), subject (chr), title (chr), type (chr), URL
#&gt;   (chr), assertion (chr), author (chr)
#&gt;
#&gt; $facets
#&gt; NULL</span></pre>

<span >Index through to get the DOIs</span>

<pre class="theme:solarized-light lang:r decode:true"><span >res$data$DOI
#&gt; [1] "10.1126/science"            "10.1887/0750307145/b426c18" "10.1002/(issn)1557-6833"
#&gt; [4] "10.1002/(issn)1469-896x"    "10.5152/bs."</span></pre>

> <span >rcrossref also has faster versions of most functions with an underscore at the end (`_`) which only do the http request and give back json (e.g., `cr_works_()`)</span>

### <span ><strong>Comparison of Crossref Client Methods</strong></span>

<span >After installation and loading the libraries above, the below methods are available</span>

<table style="width: 100%;">
  <tr>
    <th>
      <span >API Route</span>
    </th>

    <th>
      <span >Python</span>
    </th>

    <th>
      <span >Ruby</span>
    </th>

    <th>
      <span >R</span>
    </th>
  </tr>

  <tr>
    <td>
      <span ><strong>works</strong></span>
    </td>

    <td>
      <span ><code>cr.works()</code></span>
    </td>

    <td>
      <span ><code>Serrano.works()</code></span>
    </td>

    <td>
      <span ><code>cr_works()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>members</strong></span>
    </td>

    <td>
      <span ><code>cr.members()</code></span>
    </td>

    <td>
      <span ><code>Serrano.members()</code></span>
    </td>

    <td>
      <span ><code>cr_members()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>funders</strong></span>
    </td>

    <td>
      <span ><code>cr.funders()</code></span>
    </td>

    <td>
      <span ><code>Serrano.funders()</code></span>
    </td>

    <td>
      <span ><code>cr_funders()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>types</strong></span>
    </td>

    <td>
      <span ><code>cr.types()</code></span>
    </td>

    <td>
      <span ><code>Serrano.types()</code></span>
    </td>

    <td>
      <span ><code>cr_types()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>licenses</strong></span>
    </td>

    <td>
      <span ><code>cr.licenses()</code></span>
    </td>

    <td>
      <span ><code>Serrano.licenses()</code></span>
    </td>

    <td>
      <span ><code>cr_licenses()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>journals</strong></span>
    </td>

    <td>
      <span ><code>cr.journals()</code></span>
    </td>

    <td>
      <span ><code>Serrano.journals()</code></span>
    </td>

    <td>
      <span ><code>cr_journals()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>members</strong></span>
    </td>

    <td>
      <span ><code>cr.members()</code></span>
    </td>

    <td>
      <span ><code>Serrano.members()</code></span>
    </td>

    <td>
      <span ><code>cr_members()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>registration agency</strong></span>
    </td>

    <td>
      <span ><code>cr.registration_agency()</code></span>
    </td>

    <td>
      <span ><code>Serrano.registration_agency()</code></span>
    </td>

    <td>
      <span ><code>cr_agency()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>random DOIs</strong></span>
    </td>

    <td>
      <span ><code>cr.random_dois()</code></span>
    </td>

    <td>
      <span ><code>Serrano.random_dois()</code></span>
    </td>

    <td>
      <span ><code>cr_r()</code></span>
    </td>
  </tr>
</table>

### <span ><strong>Other Crossref Services</strong></span>

<table style="width: 100%;">
  <tr>
    <th>
      <span >Service</span>
    </th>

    <th>
      <span >Python</span>
    </th>

    <th>
      <span >Ruby</span>
    </th>

    <th>
      <span >R</span>
    </th>
  </tr>

  <tr>
    <td>
      <span ><strong>content negotiation</strong></span>
    </td>

    <td>
      <span ><code>cn.content_negotiation()</code><a href="#footnote-1">[1]</a></span>
    </td>

    <td>
      <span ><code>Serrano.content_negotiation()</code></span>
    </td>

    <td>
      <span ><code>cr_cn()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>CSL styles</strong></span>
    </td>

    <td>
      <span ><code>cn.csl_styles()</code><a href="#footnote-1">[1]</a></span>
    </td>

    <td>
      <span ><code>Serrano.csl_styles()</code></span>
    </td>

    <td>
      <span ><code>get_styles()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>citation count</strong></span>
    </td>

    <td>
      <span ><code>counts.citation_count()</code><a href="#footnote-2">[2]</a></span>
    </td>

    <td>
      <span ><code>Serrano.citation_count()</code></span>
    </td>

    <td>
      <span ><code>cr_citation_count()</code></span>
    </td>
  </tr>
</table>

<p id="footnote-1">
  <span >[1] <code>from habanero import cn</code></span>
</p>

<p id="footnote-2">
  <span >[2] <code>from habanero import counts</code></span>
</p>

&nbsp;

### <span >Features</span>

<span >These are supported in all 3 libraries:</span>

  * <span >Filters (see below)</span>
  * <span >Deep paging (see below)</span>
  * <span >Pagination</span>
  * <span >Verbose curl output</span>

### <span >Filters</span>

<span >Filters (see <a href="https://github.com/Crossref/rest-api-doc/blob/master/rest_api.md#filter-names">API docs</a> for details) are a powerful way to get closer to exactly what you want in your queries. In the Crossref API filters are passed as query parameters, and are comma-separated like <span class="lang:default decode:true crayon-inline ">filter=has-orcid:true,is-update:true</span> . In the client libraries, filters are passed in idiomatic fashion according to the language.</span>

<span ><em><strong>Python</strong></em></span>

<pre class="theme:solarized-light lang:python decode:true"><span >from habanero import Crossref
cr = Crossref()
cr.works(filter = {'award_number': 'CBET-0756451', 'award_funder': '10.13039/100000001'})</span></pre>

<span ><em><strong>Ruby</strong></em></span>

<pre class="theme:solarized-light lang:ruby decode:true"><span >require 'serrano'
Serrano.works(filter: {award_number: 'CBET-0756451', award_funder: '10.13039/100000001'})</span></pre>

<span ><em><strong>R</strong></em></span>

<pre class="theme:solarized-light lang:r decode:true"><span >library("rcrossref")
cr_works(filter=c(award_number=TRUE, award_funder='10.13039/100000001'))
</span></pre>

<span >Note how syntax is quite similar among languages, though keys don’t have to be quoted in Ruby and R, and in R you pass in a vector or list instead of a hash as in the other two.</span>

<span >All 3 clients have helper functions to show you what filters are available and what the options are for each filter.</span>

<table style="width: 100%;">
  <tr>
    <th>
      <span >Action</span>
    </th>

    <th>
      <span >Python</span>
    </th>

    <th>
      <span >Ruby</span>
    </th>

    <th>
      <span >R</span>
    </th>
  </tr>

  <tr>
    <td>
      <span ><strong>Filter names</strong></span>
    </td>

    <td>
      <span ><code>filters.filter_names</code><a href="#footnote-3">[3]</a></span>
    </td>

    <td>
      <span ><code>Serrano::Filters.names</code></span>
    </td>

    <td>
      <span ><code>filter_names()</code></span>
    </td>
  </tr>

  <tr>
    <td>
      <span ><strong>Filter details</strong></span>
    </td>

    <td>
      <span ><code>filters.filter_details</code><a href="#footnote-3">[3]</a></span>
    </td>

    <td>
      <span ><code>Serrano::Filters.filters</code></span>
    </td>

    <td>
      <span ><code>filter_details()</code></span>
    </td>
  </tr>
</table>

<p id="footnote-3">
  <span >[3] <code>from habanero import filters</code></span>
</p>

&nbsp;

### <span >Deep paging</span>

<span >Sometimes you want a lot of data. The Crossref API has parameters for paging (see <a href="https://github.com/Crossref/rest-api-doc/blob/master/rest_api.md#rows">rows</a> and <a href="https://github.com/Crossref/rest-api-doc/blob/master/rest_api.md#offset">offset</a>), but large values of either can lead to long response times and potentially timeouts (i.e., request failure). The API has a deep paging feature that can be used when large data volumes are desired. This is made possible via Solr’s cursor feature (e.g., <a href="http://solr.pl/en/2014/03/10/solr-4-7-efficient-deep-paging/">blog post on it</a>). Here’s a run down of how to use it:</span>

  * <span >`cursor`: each method in each client library that allows deep paging has a `cursor` parameter that if you set to `*` will tell the Crossref API you want deep paging.</span>
  * <span >`cursor_max`: for boring reasons we need to have feedback from the user when they want to stop, since each request comes back with a cursor value that we can make the next request with, thus, an additional parameter `cursor_max` is used to indicate the number of results you want back.</span>
  * <span >`limit`: this parameter when not using deep paging determines number of results to get back. however, when deep paging, this parameter sets the chunk size. (note that the max. value for this parameter is 1000)</span>

<span >For example, `cursor=&#8221;*&#8221;` states that you want deep paging, `cursor_max` states maximum results you want back, and `limit` determines how many results per request to fetch.</span>

<span ><em><strong>Python</strong></em></span>

<pre class="theme:solarized-light lang:python decode:true "><span >from habanero import Crossref
cr = Crossref()
cr.works(query = "widget", cursor = "*", cursor_max = 500)</span></pre>

<span ><em><strong>Ruby</strong></em></span>

<pre class="theme:solarized-light lang:ruby decode:true"><span >require 'serrano'
Serrano.works(query: "widget", cursor: "*", cursor_max: 500)</span></pre>

<span ><em><strong>R</strong></em></span>

<pre class="theme:solarized-light lang:r decode:true"><span >library("rcrossref")
cr_works(query = "widget", cursor = "*", cursor_max = 500)
</span></pre>

&nbsp;

### <span >Text mining clients</span>

<span >Just a quick note that I’ve begun a few text-mining clients for Python and Ruby, focused on using the low level clients discussed above.</span>

  * <span >Python: <a href="https://github.com/sckott/pyminer">https://github.com/sckott/pyminer</a></span>
  * <span >Ruby: <a href="https://github.com/sckott/textminer">https://github.com/sckott/textminer</a></span>

<span >Do try them out!</span>
