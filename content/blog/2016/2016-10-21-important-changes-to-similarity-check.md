---
title: Important changes to Similarity Check
author: Madeleine Watson
authors:
  - Madeleine Watson
date: 2016-10-21

categories:
  - Full-text Links
  - Member Briefing
  - Metadata
  - Similarity Check
archives:
  - 2016

---


## New features, new indexing, new name - oh my!

{{% imagewrap right %}} <img src="https://assets.crossref.org/logo/crossref-similarity-check-logo-200.svg" width="200" height="98" alt="Crossref Similarity Check logo"> {{% /imagewrap %}}

**TL;DR** The indexing of Similarity Check users’ content into the shared full-text database is about to get a lot faster. Now we need members assistance in helping Turnitin (the company who own and operate the iThenticate plagiarism checking tool) to transition to a new method of indexing content. <!--more-->

<span >For existing Similarity Check users: please check that your metadata includes full-text URLs so that Turnitin can quickly and easily locate and index your content. Full-text URLs need to be included in 90% of journal article metadata by 31st December 2016.</span>

## <span >2016 has seen some exciting new developments</span>

<span >(And there are plenty more in store as we strive towards 2017). But first: i</span><span ><span >n April we renamed the service from CrossCheck to Similarity Check and we now have a new service logo available to reference via our </span><a href="/brand"><span >logo CDN</span></a><span > using the following code.</span></span>

```<img src="https://assets.crossref.org/logo/crossref-similarity-check-logo-200.svg" width="200" height="98" alt="Crossref Similarity Check logo">```

<span >Earlier this year Crossref also signed a new contract with Turnitin. As part of this, we negotiated the inclusion of dedicated development time each year from Turnitin’s engineering and product teams to focus on developments in the iThenticate tool that will specifically support Similarity Check users and their needs.  Many of our members will have been contacted recently by Turnitin and asked to complete a survey regarding how they use the tool and what improvements they would like to see made in the future. The results of this survey are currently being analyzed and will be used by Turnitin to inform a development plan.</span>

<span >Finally, throughout 2016 we have also been working with Turnitin to help them develop a new Content Intake System that provides a faster, more reliable and robust method for collecting data from Crossref and indexing users’ content into the Similarity Check full-text database.  Previously Turnitin was only able to collect prefix data from Crossref’s system on a monthly basis whereas today, with the new Content Intake System up and running, they are able to pull full-text content links from deposited metadata on a daily basis. This means that if you are a Similarity Check user currently depositing full-text URLs with Crossref, your content is being indexed by Turnitin faster than ever before.</span>

> <span >There are plenty of other benefits this new method provides. This is why we have agreed with Turnitin that from 1st January 2017 onwards, indexing via full-text URLs will be the only method supported for Similarity Check.</span>

<span >Not convinced? Let me share my top four reasons for advocating Turnitin’s exclusive use of the full-text URL indexing method for Similarity Check:</span>

<p >
  <span ><b>1. Reduced traffic to publisher servers.</b><span > Indexing via full-text URLs means that the crawl is targeted specifically to the location of the full-text PDF or HTML content, thereby reducing the amount of traffic Turnitin puts through publisher’s servers.</span></span>
</p>

<p >
  <span ><b>2. Lower margin for error and simplified issue recovery.</b><span > Turnitin will no longer need to make multiple fetches for any content item, meaning there are now fewer steps in the process. This means there will be fewer places for indexing errors to occur and also reduces the reliance on users setting meta tags or span tags correctly in their markup. Furthermore, if problems do arise, using the one method of indexing for all users will mean that Turnitin is able to pinpoint the issue faster and work with members to resolve it quickly. </span></span>
</p>

<p >
  <span ><b>3. Quicker turnaround on indexing with fewer delays.</b><span > Turnitin will no longer need to investigate and set up bespoke indexing methods for different Similarity Check users and they will be able to access the location of full-text content from the one place (ie. within the specific <iparadigms> resource tag in member’s metadata deposits). More accurate data from only one location will result in a quicker turnaround on indexing, meaning newly published content will be added into the Similarity Check content database sooner for all members to check other new manuscripts against.</span></span>
</p>

<p >
  <span ><b>4. Daily ingest is better than monthly!</b> Full-text links can be collected daily from Crossref-rather than monthly for other methods-meaning a more regular ingest of content.</span>
</p>

<span >The presence of full-text URLs within the metadata is critical to the functioning of Turnitin’s new indexing system. All new Similarly Check participants are now asked to ensure they have these links in place within their deposited metadata before they participate in the service.</span>

## <span >Already a user of Similarity Check? </span>

<span ><span >If you’re an existing Similarity Check participant who joined the service before 2016, your content is likely to be currently indexed via different methods, such as following links contained in your page meta tags. If you’re not currently depositing full-text links with Crossref for Similarity Check, you will have received an email from us about this in August. If you’re unsure though, you can check your XML to see if you have included the full-text link in the <iparadigms> field or you can send us an email at </span><a href="mailto:similaritycheck@crossref.org"><span >similaritycheck@crossref.org</span></a><span > as we’d be happy to check for you. </span></span>

### <span >Help, don’t leave me behind!</span>

<span ><span >Us? Never! We’re here to help. But we really do need those full-text links… Everything existing Similarity Check publishers need to know about adding full-text links into new or existing metadata can be found on our </span><a href="http://help.crossref.org/similaritycheck"><span >help site</span></a><span >. These URLs should be included as part of all standard metadata deposits going forward and can be easily added into existing files in bulk. So there’s no need to redeposit the full metadata, unless of course you would prefer to do so!</span></span>

## <span >That’s a wrap</span>

<span >Looking back, it really has been a busy year for Similarity Check and it will continue to be so as we persevere in laying the groundwork for a more streamlined, robust and scalable service for 2017 and beyond. Remember, we need Similarity Check users to ensure they have full-text URLs in at least 90% of their journal article metadata by 31st December 2016 in order to continue using Similarity Check from 2017 onwards.</span>

<span ><span >And please keep us updated!  With over 1,200 publishers using Similarity Check, we’ll need a little nudge to know when metadata has been updated to include these links. So once updates have been deposited, please email </span><a href="mailto:similaritycheck@crossref.org"><span >similaritycheck@crossref.org</span></a><span > to confirm. And of course, as always, if there are any questions or if some advice would help, we’re just an [email](mailto:similaritycheck@crossref.org) away.  </span></span>

&nbsp;
