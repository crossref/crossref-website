---
title: 'Perspectives: Luis Montilla on making science fiction concepts a reality in the scholarly ecosystem'
slug: "perspectives-luis-montilla-sci-fi-concepts-reality-scholarly-ecosystem"
author: Luis Montilla
authors:
  - Luis Montilla
draft: false
date: 2023-11-20
categories:
  - Perspectives 
  - Community
archives:
    - 2023
aliases: "/blog/perspectives-luis-montilla-joscifi-concepts-reality-scholarly-ecosystem/"

---

{{% imagewrap left %}} <img src="/images/blog/2022/perspectives.png" alt="sound bar logo" width="150px" class="img-responsive" /> {{% /imagewrap %}} 

<br>

Hello, readers! My name is Luis, and I've recently started a new role as the Technical Community Manager at Crossref, where I aim to bridge the gap between some of our services and our community awareness to enhance the Research Nexus. I'm excited to share my thoughts with you.

My journey from research to science communications infrastructure has been a gradual transition. As a Masters student in Biological Sciences, I often felt curious about the behind-the-scenes after a paper is submitted and published. For example, the fate of data being stored in the drawer or copied and forgotten in the hard drive after the paper is online. I come from a university that shares its name with at least [three completely different universities](https://en.wikipedia.org/wiki/Sim%C3%B3n_Bol%C3%ADvar_University) in Latin America, and that also is pretty similar to another one with multiple offices across the [region](https://es.wikipedia.org/wiki/Universidad_Andina_Sim%C3%B3n_Bol%C3%ADvar), which made me wonder if there was a standard way of identifying our affiliations. And then we have the topic of our names in hispanoamerica. We use two family names, and more often than not, we have a middle name (and then I could tell you stories about multiple-word middle names), which inevitably leads to authors having many combinations of full names and hyphenations.

This curiosity led me to volunteer in the Journal of the Venezuelan Society of Ecology. This role has been a transformative experience because my goal was to learn more about the publishing aspect of science. Still, today I realize that this is a fraction of what the scholarly ecosystem represents. The experience allowed me to grasp the importance of having a community with a sense of belonging, the relevance of multilingualism, and the importance of having access to an open infrastructure that allows smaller communities to be participants in the global dynamics. Moreover, it seemed to me that a research paper is more than the capstone of a building that we place and then move on to the next project or the next experiment; instead, it is a node in the vast network of human knowledge, connected to other papers through references, but also to all the other elements that are produced as part of the research, namely datasets, protocols, code, presentations, posters, preprints, peer-review reports and more. In short, the research *metadata* extends the life of the research output and makes it visible to the rest of the community.

This brings us to my onboarding to the Crossref team. At Crossref, I became part of a team and a driving force whose idea of the [Research Nexus](/documentation/research-nexus/) [^1]  aligns perfectly with my aspirations. And to explain myself better, I'll draw an analogy using one of my favorite authors. In Isaac Asimov's Second Foundation, a character shows to another a wall covered to the last millimeter with equations and writings. He describes his contribution to "The Plan" as follows: "...Every red mark you see on the wall is the contribution of a man among us who lived since Seldon".[^2] This idea sounded fascinating to me and only possible in a sci-fi book; a massive integrated research ecosystem where scientists focused more on how their contributions fit in the big picture. Today I have come to think that metadata helps materialize this idea by interconnecting all knowledge, and more importantly, in stark contrast to Asimov's plan developed and guarded by a secret society, Crossref's research nexus is a "reusable open network," "a scholarly record that the global community can build on forever." In a world with undeniably unequal access to resources, providing open access and fostering community efforts to contribute to this growing collective effort is a fundamental condition to empower and visualize underrepresented voices.

We make available a series of tools to access and probe this data, including our REST API, but we know its potential is far from being realized. As Technical Community Manager at Crossref, my primary responsibility is to understand the needs of our community members who interact with our REST API. I aim to build and maintain relationships with new and existing metadata users to promote the effective usage of our API. I will also be working closely with organizations such as hosting platforms, manuscript submission systems, and general publisher services. In essence, I want to ensure that our community across the globe is aware of the vast possibilities that imply using and contributing to the Research Nexus.

I am committed to fostering an engaged and collaborative technical community. As we move forward, I look forward to sharing insights, experiences, and knowledge with all of you. Stay tuned for more updates, and let's explore the world of APIs, metadata, and scholarly communities together!

[^1]: [Crossref (2021) The research nexus. Accessed on 20 October 2023.](https://www.crossref.org/documentation/research-nexus/)

[^2]: Asimov, I. (1953) Second Foundation. Gnome Press.
