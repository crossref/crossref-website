---
title: 'The PLACE for new publishers – a one-stop-shop for information and a friendly community'
author: Kornelia Korzec
authors:
  - Kornelia Korzec
date: 2023-04-17
draft: false
categories:
  - Accessibility
  - Community
  - Collaboration
archives:
      - 2023
---



The Publishers Learning And Community Exchange (PLACE) at [theplace.discourse.group](https://theplace.discourse.group) is a new online public forum created for organisations interested in adopting best practices in scholarly publishing. New scholarly publishers can access information from multiple agencies in one place, ask questions of the experts and join conversations with each other.

Scholarly publishing is an interesting niche of an industry – it appears at the same time ancillary and necessary to the practice and development of scholarship itself. The sooner and more easily a piece of academic work is shared, the greater the chance that others will find and build upon it. Many practices of the publishing industry have been developed to support discovery and integrity of the scholarship that produces shareable works, and as the landscape of scholarly communications constantly evolves, a number of agencies arose to promote and continuously update the standards and best practices within it. 

We realise that the sheer number of agencies involved in regulating and preserving scholarly content is in itself a challenge and can be confusing. Newer publishers may find it difficult to know where to go to find the right information, what policies they need to follow or international criteria they need to meet and how to go about doing so. When time or finances are tight, it’s not easy to try to reinvent the wheel. 

Following the long-established practice of signposting organisations between us, we’ve worked together with the [Committee on Publication Ethics (COPE)](https://publicationethics.org), [the Directory of Open Access Journals (DOAJ)](https://doaj.org), and the [Open Access Scholarly Publishers Association (OASPA)](https://oaspa.org) to establish the PLACE. We share values and goals to work more effectively to better support the needs of our communities. Each organisation is taking actions to lower barriers to participation and provide greater support for the organisations that publish scholarly and professional content that we work with. 

Hence, we envisaged the PLACE as a ‘one stop shop’ for access to more consolidated and plainly put information, to support publishers in adopting best practices the industry developed. We also hope that by setting the information service as a forum, we will encourage open exchange with publishers who aspire to do things right, as well as between them.  