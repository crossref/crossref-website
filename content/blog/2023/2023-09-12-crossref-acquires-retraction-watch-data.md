---
title: 'News: Crossref and Retraction Watch'
author: Ginny Hendricks 
authors:
 - Ginny Hendricks
 - Rachael Lammey
 - Lucy Ofiesh
 - Geoffrey Bilder
 - Ed Pentz
date: 2023-09-12
draft: false
categories:
 - Research Integrity
 - Retractions
 - Research Nexus
 - News Release
archives:
 - 2023
---
{{% logo-inline %}} https://doi.org/10.13003/c23rw1d9

## Crossref acquires Retraction Watch data and opens it for the scientific community
_**Agreement to combine and publicly distribute data about tens of thousands of retracted research papers, and grow the service together**_

_12th September 2023_ —-- The Center for Scientific Integrity, the organisation behind the Retraction Watch blog and database, and Crossref, the global infrastructure underpinning research communications, both not-for-profits, announced today that the Retraction Watch database has been acquired by Crossref and made a public resource. An agreement between the two organisations will allow Retraction Watch to keep the data populated on an ongoing basis and always open, alongside publishers registering their retraction notices directly with Crossref.

{{% divwrap align-left %}}{{< figure src="/images/blog/2023/rw-cr-announcement.png" alt="crossref-acquires-retraction-watch-data" width="100%">}}{{% /divwrap %}}

Both organisations have a shared mission to make it easier to assess the trustworthiness of scholarly outputs. Retractions are an important part of science and scholarship regulating themselves and are a sign that academic publishing is doing its job. But there are more journals and papers than ever, so identifying and tracking retracted papers has become much harder for publishers and readers. That, in turn, makes it difficult for readers and authors to know whether they are reading or citing work that has been retracted. Combining efforts to create the largest single open-source database of retractions reduces duplication, making it more efficient, transparent, and accessible for all.

Product Director Rachael Lammey says, “Crossref is focused on documenting and clarifying the scholarly record in an open and scalable form. For a decade, our members have been recording corrections and retractions through our infrastructure, and incorporating the Crossmark button to alert readers. Collaborating with Retraction Watch augments publisher efforts by filling in critical gaps in our coverage, helps the downstream services that rely on high-quality, open data about retractions, and ultimately directly benefits the research community.” 

The Center for Scientific Integrity and the Retraction Watch blog will remain separate from Crossref and will continue their journalistic work investigating retractions and related issues; the agreement with Crossref is confined to the database only and Crossref itself remains a neutral facilitator in efforts to assess the quality of scientific works. Both organisations consider publishers to be the primary stewards of the scholarly record and they are encouraged to continue to add retractions to their Crossref metadata as a priority.

“Retraction Watch has always worked to make our highly comprehensive and accurate retraction data available to as many people as possible. We are deeply grateful to the foundations, individuals, and members of the publishing services industry who have supported our efforts and laid the groundwork for this development,” said Ivan Oransky, executive director of the Center for Scientific Integrity and co-founder of Retraction Watch. “This agreement means that the Retraction Watch Database has sustainable funding to allow its work to continue and improve.” 

Please join Crossref and Retraction Watch leadership, among other special guests, for a community call on 27th September at 1 p.m. UTC to discuss this new development in the pursuit of research integrity.

***

### Supporting details

- Crossref retractions number 14k, and the Retraction Watch database currently numbers 43k. There is some overlap, making a total of around 50k retractions.
- ~~The full dataset has been released through Crossref’s Labs API, initially as a .csv file to download directly: [https://api.labs.crossref.org/data/retractionwatch?name@email.org](https://api.labs.crossref.org/data/retractionwatch?ginny@crossref.org) (add your ‘mailto’).~~ _Edit: 2024-10-10:_ The full dataset is available in a git repository at [https://gitlab.com/crossref/retraction-watch-data](https://gitlab.com/crossref/retraction-watch-data).
- The Crossref Labs API also displays information about retractions in the `/works/` route when metadata is available, such as [https://api.labs.crossref.org/works/10.2147/CMAR.S324920?name@email.org](https://api.labs.crossref.org/works/10.2147/CMAR.S324920?mailto=ginny@crossref.org) (add your ‘mailto’). If you don't have a .json viewer, please see below for screenshot.
- Crossref is paying an initial acquisition fee of USD $175,000 and will pay Retraction Watch USD $120,000 each year, increasing by 5% each year. 
- The initial term of the contract is five years. ~~The full text of the contract will be made public in the coming fortnight.~~ _EDIT 2023-09-26:_ [Here is the signed agreement](/pdfs/retraction-watch-crossref-fully-executed-23-08-2023.pdf). 
- There will be a community call on 27th September at 1 p.m. UTC (your time zone [here](https://dateful.com/eventlink/3093150191)). Please [register](https://crossref.zoom.us/webinar/register/WN_U0naDJTCQIS_sQECv8Aa4Q).
- An open [FAQ document](https://docs.google.com/document/d/1GabgCP_sUwvW2XEtOfWmFwNIpizagWvlreALWvbZY8Y/edit) is available to collect questions to be answered at the webinar.
- This announcement will always be accessible via Crossref DOI [https://doi.org/10.13003/c23rw1d9](https://doi.org/10.13003/c23rw1d9); please use this persistent link for sharing.

##### About Retraction Watch and The Center for Scientific Integrity
The Center for Scientific Integrity is a U.S. 501(c)3 non-profit whose mission is to promote transparency and integrity in science and scientific publishing, and to disseminate best practices and increase efficiency in science. In addition to maintaining and curating the Retraction Watch Database, the Center is the home of [Retraction Watch](http://retractionwatch.com), a blog founded in 2010 that reports on scholarly retractions and related issues in research integrity.

##### About Crossref
[Crossref](https://www.crossref.org) is a global community infrastructure that makes all kinds of research objects easy to find, assess, and reuse through a number of services critical to research communications, including an open metadata API that sees over 1.1 billion queries every month. Crossref’s >19,000 members come from 151 countries and are predominantly university-based. Their ~150 million DOI records contribute to the collective vision of a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society.

### Enquiries

- For Retraction Watch/Center for Scientific Integrity: Ivan Oransky, [ivan@retractionwatch.com](mailto:ivan@retractionwatch.com?subject=Crossref%20and%20Retraction%20Watch)
- For Crossref: Ginny Hendricks, [ginny@crossref.org](mailto:ginny@crossref.org?subject=Retraction%20Watch%20and%20Crossref)

{{< figure src="/images/blog/2023/sample-record-retraction-watch-border.png" alt="crossref-acquires-retraction-watch-data" caption="A screenshot of an example Labs API metadata record with a Retraction Watch-asserted retraction" alt="A screenshot of an example Labs API metadata record with a Retraction Watch-asserted retraction" width="100%">}}