---
title: 'Joint Statement on Research Data'
slug: "joint-statement-on-research-data"
author: STM
authors:
  - Crossref
  - DataCite
  - STM
draft: false
date: 2023-11-28
categories:
  - DataCite
  - Linked Data
archives:
    - 2023
---


<p>{{% imagewrap right %}}
  <a href="/pdfs/stm-research-data-infographic-FINAL-4.pdf" title="Download PDF">
    <img src="/images/blog/2023/stm-research-data-infographic-FINAL-4.jpg" width="400"/>
  </a>{{% /imagewrap %}}

</p>

STM, DataCite, and Crossref are pleased to announce an updated joint statement on research data.

In 2012, DataCite and STM drafted an [initial joint statement](https://www.stm-assoc.org/2012_06_14_STM_DataCite_Joint_Statement.pdf) on the linkability and citability of research data. With nearly 10 million data citations tracked, thousands of repositories adopting data citation best practices, thousands of journals adopting data policies, data availability statements and establishing persistent links between articles and datasets, and the introduction of data policies by an increasing number of funders, there has been significant progress since. It now seems appropriate to focus on providing updated recommendations for the various stakeholders involved in research data sharing.

The premise of the original joint statement still stands: most stakeholders across the spectrum of researchers, funders, librarians and publishers agree about the benefits of making research data available and findable for reuse by others. This improves utility and rigor of the scholarly record. Still, research data sharing is not yet a self-evident step in the research lifecycle. We now have sufficient scholarly communication infrastructure in place to bring about widespread change and believe momentum is building for collective action.

It is in this context that DataCite, a global membership community working with over 2800 repositories around the world, and STM, whose membership consists of over 140 scientific, technical, and medical publishing organizations, are issuing this joint statement. Crossref, a nonprofit open infrastructure with over 18,000 institutional members from 150 countries, joins this call, recognising the need for an amplified focus on data citation. The aim of this statement is to accelerate adoption of best practices and policies, and encourage further development of critical policies in collaboration with a wide group of stakeholders. 

Signatories of this statement recommend the following as best practice in research data sharing:

1. When publishing their results, researchers deposit related research data and outputs in a trustworthy data repository that assigns persistent identifiers (DOIs where available). Researchers link to research data using persistent identifiers.
2. When using research data created by others, researchers provide attribution by citing the datasets in the reference section using persistent identifiers.
3. Data repositories enable sharing of research outputs in a FAIR way, including support for metadata quality and completeness.
4. Publishers set appropriate journal data policies, describing the way in which data is to be shared alongside the published article. 
5. Publishers set instructions for authors to include Data Citations with persistent identifiers in the references section of articles.
6. Publishers include Data Citations and links to data in Data Availability Statements with persistent identifiers (DOIs where available) in the article metadata registered with Crossref. 
7. In addition to Data Citations, Data Availability Statements (human- and machine-readable) are included in published articles where appropriate. 
8. Repositories and publishers connect articles and datasets through persistent identifier connections in the metadata and reference lists.
9. Funders and research organizations provide researchers with guidance on open science practices, track compliance with open science policies where possible, and promote and incentivize researchers to openly share, cite and link research data.
10. Funders, policymaking institutions, publishers and research organizations collaborate towards aligning FAIR research data policies and guidelines.
11. All stakeholders collaborate in the development of tools, processes, and incentives throughout the research cycle to enable sharing of high-quality research data, making all steps in the process clear, easy and efficient for researchers by providing support and guidance.
12. Stakeholders responsible for research assessment take into account data sharing and data citation in their reward and recognition system structures.

We, the following signatories shall adopt and promote the relevant best practices laid out above. We hope that our action inspires the community, including researchers, research funders, research institutions, data repositories and publishers, to join us in making it easy for researchers to share, link and cite research data.

[Endorse the statement here.](https://docs.google.com/forms/d/1Anc6EAs02YTveU3PqELJU2jnTK1R84hMWgB9kNX01p0/edit)

