---
title: 'We’re hiring! New technical, community, and membership roles at Crossref'
author: Michelle Cancel
authors:
  - Michelle Cancel
date: 2023-04-21
draft: false
categories:
  - Jobs
  - Community
  - Membership
archives:
      - 2023
---

Do you want to help make research communications better in all corners of the globe? Come and join the world of nonprofit open infrastructure and be part of improving the creation and sharing of knowledge.

We are recruiting for three new staff positions, all new roles and all fully remote and flexible. See below for more about our ethos and what it's like working at Crossref.

🚀 **[Technical Community Manager](/jobs/2023-04-20-technical-community-manager)**, working with our 'integrators' so all repository/publishing platforms and plugins, all API users incl. managing contracts with subscribers, and generally helping a very nice bunch of RESTful API dabblers, both novice and intermediate. The goal is to offer more interactive engagement such as sprints, and more technical consultation to help the community with things like query efficiency, public data dump ingestion, etc. Thousands of users exist, from individual researchers and small academic tools to giant technology companies. Researching and analysing usage and building tools to meet their needs is key, so this role works closely with Product and R&D colleagues and likely needs a developer or developer-advocacy background.

🎯 **[Member Experience Manager](/jobs/2023-04-13-member-experience-manager/)**, ramping up to handle the mammoth operation that is... membership, currently 18,000 members from 150 countries, and onboarding the ~180 new joiners we welcome monthly, mostly from Africa and Asia. This role involves lots of education and relationship management, but because of the scale, we also need someone with a real business process/analysis approach, improving how our systems function so that the operation flows seamlessly and isn't a pain for people (both members and staff). This role manages two full-time Member Support Specialists (UK and Indonesia) and three part-time contractors (USA, France, and one other as yet unknown).

🎈 **[Community Engagement Manager](/jobs/2023-04-19-community-engagement-manager/)**, working with the global community of scholarly editors at a time when research integrity is top of mind for our entire ecosystem. This is a classic community role for someone keen to cross over from managing or editing journals or books and perhaps make your volunteer work official. Activities will include program and project management, event and working group facilitation, communications and content creation. You'd be interacting with groups like the Asian Council of Science Editors, the European Association of Science Editors, and the Council of Science Editors, plus many more that you'd identify. It's all about helping editors, who work hand-in-hand with authors, to think about metadata as signals of trust and better use available services, such as those for retraction management or plagiarism checking, and helping to define needs for emerging activity too, such as machine-generated content.

### Working at Crossref

We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.

Crossref sits at the heart of the global exchange of research information, and our job is to make it possible—and easier—to find, cite, link, assess, and reuse research, from journals and books, to preprints, data, and grants. Through partnerships and collaborations we engage with members in 150 countries (and counting) and it’s very important to us to nurture that community.

We’re about [45 staff](/people/) and remote-first. This means that we support our teams working asynchronously and with flexible hours. We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organisation. Check out the [organisation chart](/people/org-chart/).

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

Bookmark our [jobs page](/jobs) to watch for future opportunities!
