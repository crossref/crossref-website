---
title: 'Perspectives: Madhura Amdekar on meeting the community and pursuing passion for research integrity'
slug: "perspectives-madhura-amdekar-meeting-community-pursuing-research-integrity"
author: Madhura Amdekar
authors:
  - Madhura Amdekar
draft: false
date: 2023-12-05
categories:
  - Perspectives
  - Community
archives:
    - 2023
---

{{% imagewrap left %}} <img src="/images/blog/2022/perspectives.png" alt="sound bar logo" width="150px" class="img-responsive" /> {{% /imagewrap %}} 

The second half of 2023 brought with itself a couple of big life changes for me: not only did I move to the Netherlands from India, I also started a new and exciting job at Crossref as the newest Community Engagement Manager. In this role, I am a part of the Community Engagement and Communications team, and my key responsibility is to engage with the global community of scholarly editors, publishers, and editorial organisations to develop sustained programs that help editors to leverage rich metadata. 

This represents an exciting phase in my professional journey, as I now have the chance to learn and develop new skills, broaden my understanding of the publishing landscape, and at the same time be able to leverage the experience I gained so far. I originally trained as an ecologist, obtaining a PhD studying colour change in a tropical agamid lizard in India at the [Indian Institute of Science](https://iisc.ac.in/) (Bengaluru, India). Having immensely enjoyed the process of writing manuscripts based on the data that resulted from my PhD thesis, I was drawn to working in the scholarly publishing industry. I worked for 3.5 years as a Senior Associate at Wiley, overseeing an editor support service by devising strategic scale-up planning and process improvement initiatives. 

I then moved countries as well as jobs and joined Crossref. The world of scholarly communications is a rapidly changing ecosystem, that is ably supported by scholarly infrastructure - the sets of tools and services that support this industry. Being a part of Crossref, a global organisation that provides open scholarly infrastructure, allows me to work with and make an impact on the broad scholarly community that ranges from publishers of all shapes and sizes, funders, to academic institutions, and researchers.  

So far, the [integrity of the scholarly record (ISR)](/categories/research-integrity/) has been the focus of my work. Now more than ever, the community is cognizant of the need to uphold the integrity of the scholarly output. Metadata and relationships between research outputs can support this endeavour in a substantial manner because information such as who contributed to a research output, who funded it, who cites it, whether it was updated after publication, aids provenance and provides signals about whether the output is trustworthy. 

Most of Crossref’s tools and services play a key role here: be it reference linking to allow researchers to increase discoverability of their work, tracking post-publication updates to research outputs via Crossmark, or detecting text plagiarism via Similarity Check. We noticed that not all editors and editorial teams associate metadata as signals of integrity, and might be unaware of the benefits of rich metadata. Therefore, my priority is to utilise opportunities to engage with editors about how metadata can provide trust indicators about a research output. I aim to empower editors to collect and leverage rich metadata.

While I am no stranger to the world of scholarly communications, engaging with the broader Crossref community has been a new experience for me. In my day to day work, I employ a range of different skills such as program design and management, content planning and outreach, networking, and meeting facilitation. I have also been participating in trainings to enhance my skill set – I recently completed a training course on [Community Engagement Fundamentals](https://www.cscce.org/trainings/cef/), which has equipped me with a better understanding of the concepts and strategies that I will need as a community manager. Additionally, I also underwent the Group Facilitation Methods training course led by the Institute of Cultural Affairs (ICA) where I learnt a couple of effective methods for group facilitation and leading workshops.

Equipped with these skills, I have moderated a few community events already – most prominently [the community call about Crossref and Retraction Watch](https://www.youtube.com/watch?v=b5VBUWz8KZY) to discuss Crossref’s acquisition and opening up of the Retraction Watch database. It was a valuable experience to contribute to the planning of an online event and host a panel of distinguished guests. 

I was also fortunate to be able to meet our community members in-person: I supported the organisation of the Frankfurt roundtable event that was held as part of Crossref’s Integrity of the Scholarly Record (ISR) program, where we engaged with community members to get their perspectives on how to work together towards preserving the integrity of the scholarly record (keep watching this space for a forthcoming blog summarising the outcomes from this event!). Additionally, I attended the Frankfurt Book Fair – the experience of getting to meet our members and to hear from them first-hand about all things Crossref, was unparalleled! I used this opportunity to meet several of our publisher members and discuss their view points about engaging with editors on ISR. The idea was received positively: we heard specific suggestions of metadata that would be of interest to readers of scientific manuscripts, and our members also expressed interest in finding out more about how metadata can act as markers of trust for a research output. I plan to use the insights from these meetings for the development of the ISR editor engagement program. 

As I reflect on the past three months, there are a few things that have stood out to me. In terms of work, no two days are the same. My work plan for the day can range from making presentations for outreach activities, creating content such as this blogpost, working on an engagement strategy, to planning events, attending online or offline community meetings, facilitating or moderating some of those events, and networking with community members. This variety in work keeps me motivated to give my best each day. I am also grateful that I have the ability to make an impact with my work in an area that I am passionate about. In my previous job, I had developed a good understanding of research integrity and publication ethics. As a community manager now, I’m looking to work with editorial teams on the integrity of the scholarly record. This role gives me an opportunity to further nurture this interest of mine. 

At times, working from home remotely has been a challenge. However, I have enjoyed attending in-person events as they are not just a chance to meet our community members, but also a chance to meet my colleagues and connect with them. 

I feel privileged to be able to connect with research communities all over the world and make a meaningful contribution towards supporting the discoverability and impact of their work. I am particularly excited to work at the forefront of shaping the future of preserving the integrity of the scholarly record, in tandem with our community. If this is a topic that excites you as well, I am keen to hear from you. It has been a wonderful three months at Crossref so far and I look forward to future collaborations with our community to develop effective ways of supporting and empowering editors to make the most of metadata for their publications. 


