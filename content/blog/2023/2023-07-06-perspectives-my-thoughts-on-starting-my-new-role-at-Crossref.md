---
title: 'Perspectives: My thoughts on starting my new role at Crossref'
author: Johanssen Obanda
authors:
  - Johanssen Obanda
date: 2023-07-06
draft: false
categories:
  - Outreach
  - Staff
  - Perspectives
archives:
      - 2023
---

My name is Johanssen Obanda. I joined Crossref in February 2023 as a Community Engagement Manager to look after the Ambassadors program and help with other outreach activities. I work remotely from Kenya, where there is an increasing interest in improving the exposure of scholarship by Kenyan researchers and ultimately by the wider community of African researchers. In this blog, I’m sharing the experience and insights of my first 4 months in this role.  

Right before joining Crossref, I was working as Stakeholder Manager with [AfricArXiv](http://africarxiv.org/), a community-led digital archive for African research communication. I transitioned to working with Crossref to take up a more challenging role, so I can apply the community-building and social innovation skills I gained over the last five years in my profession.    

What surprised me the most here is realising that such a robust infrastructure is being administered by a relatively small team. I wondered how the team keeps the services running and builds new solutions for the community. However, I am impressed by the collaborative culture, positive and healthy work environment, and great systems.  

I work within the Community Engagement and Communications team, where we collaboratively address members’ questions and challenges, plan events, create helpful content for our community and keep in touch with them. We help grow our community and create a better experience using our products and services. 

My main focus has been the Ambassador programme, which started in 2018 and currently comprises 48 Ambassadors globally. The Ambassadors are our trusted contacts who support and engage our communities locally to make scholarly communications better. Through one-on-one virtual interaction with most of them, I noted that there was little interaction among the Ambassadors. Most of our Ambassadors want to connect more, both face-to-face and online. In the coming months, we aim to design our meetings together with the Ambassadors to encourage better exchange and relationships. 

I value Crossref’s insistence on diversity, equity and inclusion, and I enjoy contributing to those activities. Working with my colleagues in the outreach team to organise webinars and activities for the [Global Equitable Membership (GEM) programme](https://www.crossref.org/gem/) has been an exciting experience. I particularly enjoyed engaging with our Ambassadors Shaharima Parvin and Jahangir Alam from Bangladesh, and Binayak Pandey from Nepal, in organising the initial webinars for the GEM program in their countries. I feel it is one of the ways of creating more in-depth connections between our communities and our Ambassadors while making it possible for more institutions to be part of Crossref and contribute to scholarly communication. 

I have made a few webinar presentations online and recently did one in-person [poster](/pdfs/SRI2023-conference-Crossref-Poster.pdf) presentation in South Africa at the Sustainability, Research and Innovation conference. I gained more confidence interacting with the wider Crossref community and a deeper understanding of Crossref’s services. I look forward to more opportunities to discuss Crossref’s mission with the community and to collaborate with like-minded organisations, contributing to joint initiatives, such as the upcoming Better Together webinar series with ORCID and DataCite, and the [Forum for Open Research in MENA](https://forumforopenresearch.com/) events. 

I experienced the challenges of working remotely in many ways. A couple of days, there was no power, other days the internet connection was painfully slow, and hopping from one restaurant to another was something I had to deal with from time to time, with the hopes of finding quiet most times to have a good meeting with my colleagues, until I had more dependable work station. On the positive side,  coordinating meeting times with colleagues, taking on tasks asynchronously and collaborating in real-time across different tools are making me more agile, patient and empathetic with myself and my colleagues. 

I am driven by the impact I want to contribute in my career working with Crossref, which is to build an inclusive research ecosystem where researchers across the globe can easily access scientific knowledge and make meaningful connections. And I feel confident about my colleagues, our systems and infrastructure and my capabilities to be part of a thriving community and organisation. 

