---
title: New Crossref DOI presentation guidelines for accessibility are landing soon
author: Patrick Vale
authors:
  - Patrick Vale
date: 2023-05-06
draft: true

categories:
  - DOIs
  - Identifiers
  - Linking
  - Publishing
  - Member Briefing
  - Accessibility
  - A11y
archives:
  - 2023

---
### TL;DR

We announced our intention to update our DOI Display Guidelines to make them work better for users of assitive technology in 2022. This is an update to that announcement.

Crossref will be updating its DOI Display Guidelines within the next couple of weeks - the first change is to the name: they are now the *Crossref DOI Presentation Guidelines*. This reflects the intention of these changes - to make DOI links more accessible to all users, whether they use a keyboard, screen reader, mouse, voice command or any combination of the above and more. This is [still](https://www.crossref.org/blog/new-crossref-doi-display-guidelines-are-on-the-way/) a big deal.  We last made a change in 2017 so it’s not something that happens often or that we take lightly. In short, the changes are to add accessible labels to the DOI links, so that screen readers no longer announce a string of meaningless characters (the full doi.org link), but instead say "DOI for .. " and the title of the paper/article/book/chapter that the DOI links to.

### H T T P Colon Slash Slash ...

The 2017 DOI Display Guidelines mandated that all dois be displayed as active, 'clickable' links. This brought a lot of benefits, but still presents users of screen readers with a stream of meaningless characters when the DOI link gets announced. DOIs are intentionally opaque, or they [should be](https://www.crossref.org/documentation/member-setup/constructing-your-dois/#00007]) - so having to listen to this stream of characters is a waste of time and not helpful to anyone. If it helps you - please [tell us](mailto:feedback@crossref.org).

We announced our intention to update the guidelines back in 2022, and called for feedback on our proposals. We recevied substantial and detailed responses, which caused us to look deeper into the world of aria techniques and the WCAG guidelines. A discussion with the JATS4R accessibility subcommittee helped to crystalise our thinking, getting some very on-point notes about our approach and the day to day experience of using a screen reader and other technologies to interact with DOI links. We are very grateful for the time and attention these proposals got from the community - both from our membership and beyond.

### An example

The updated recommendations are best demonstrated with an exmaple. We've updated our website, and our go-to test platform, the venerable [Journal of Psychoceramics](http://psychoceramics.labs.crossref.org/) to meet the new guidelines. Go and check them out with a screen reader and/or by inspecting the links with your favourite browser developer tools.

Alternatively, here is a short snippet demonstrating a DOI that conforms to the new guidelines.
```html
<a href="https://doi.org/10.5555/12345678" aria-label="DOI for Toward a Unified Theory of High-Energy Metaphysics: Silly String Theory">https://doi.org/10.5555/12345678</a>
```

That's one way of achieving the desired outcome, you could cause the same effect by using a combination of ARIA attributes.

```html
<a href="https://doi.org/10.5555/12345678" aria-label="DOI for" aria-description="Toward a Unified Theory of High-Energy Metaphysics: Silly String Theory">https://doi.org/10.5555/12345678</a>
```

Some implementations may reference elements already on a page.

```html
<h1 id="screen-reader-main-title">Toward a Unified Theory of High-Energy Metaphysics: Silly String Theory</h1>
<a href="https://doi.org/10.5555/12345678" aria-label="DOI for" aria-describedby="screen-reader-main-title">https://doi.org/10.5555/12345678</a>
```

The specific applicability of the different aria attributes in the case of DOI links could spark a lengthy debate - feel free to come join in that [conversation](link_to_post_on_community_forum) - but for implementations, we are more interested in getting a good result for users, than mandating a specific technical implementation.

### Timing and backwards compatibility

We are going to release these guidelines in Q2 2023, and give an implementation window of 2 years. That means that by Q2 2025, all DOI links should have accessible labels, and more importantly - people using screen readers should have a better experience of using them.

ARIA attributes don't affect the behaviour of the link when you 'click' on it, or copy the link URL or text - and existing DOI links that don't include them will continue to work as they do now.

We received limited feedback on the related topic of the effects this might present for voice command users. At present, we don't foresee that it will cause any detriment to their experience. However, we're open to more views on that topic, so if you're using such functionality and are concerned that our new guidelines may affect your usage of DOI links, please [get in touch](mailto:feedback@crossref.org).

### We can do better

This has been our first foray into improving DOI link accessbility, and it is an area we are serious about improving. We hope that these updated guidelines go a long way towards improving the experience of using DOI links for users of screen readers. However, we are aware that there are many other assistive technologies, and welcome any feedback about how we can make the experience even better.
