---
title: '2023 board election slate'
author: Lucy Ofiesh
draft: false
authors:
  - Lucy Ofiesh
date: 2023-09-27
categories:
  - Board
  - Member Briefing
  - Governance
  - Elections
  - Crossref Live
  - Annual Meeting
archives:
    - 2023
---

I’m pleased to share the 2023 board election slate. Crossref’s [Nominating Committee](/committees/nominating) received 87 submissions from members worldwide to fill seven open board seats.

We maintain a balance of eight large member seats and eight small member seats. A member’s size is determined based on the membership fee tier they pay. We look at how our total revenue is generated across the membership tiers and split it down the middle. Like last year, about half of our revenue came from members in the tiers $0 - $1,650, and the other half came from members in tiers $3,900 - $50,000. We have two large member seats and five small member seats open for election in 2023.

The Nominating Committee presents the following slate.

## The 2023 slate

### Tier 1 candidates (electing five seats):
* **Beilstein-Institut**, Wendy Patterson
* **Korean Council of Science Editors**, Kihong Kim
* **Lujosh Ventures Limited**, Olu Joshua
* **NISC Ltd**, Mike Schramm
* **OpenEdition**, Marin Dacos
* **Universidad Autónoma de Chile**, Dr. Ivan Suazo
* **Vilnius University**, Vincas Grigas


### Tier 2 candidates (electing two seats):
* **Association for Computing Machinery (ACM)**, Scott Delman
* **Oxford University Press**, James Phillpotts
* **Public Library of Science (PLOS)**, Dan Shanahan
* **University of Chicago Press**, Ashley Towne


{{% divwrap blue-highlight %}}

### [Here are the candidates' organizational and personal statements](/board-and-governance/elections/2023-slate/)

{{% /divwrap %}}

## You can be part of this important process by voting in the election

If your organization is a voting member in good standing of Crossref as of September 10th, 2023, you are eligible to vote when voting opens on September 27th, 2023.  


## How can you vote?

Your organization’s designated voting contact will receive an email from eBallot the week of September 25th with the Formal Notice of Meeting and Proxy Form with concise instructions on how to vote. The email will include a username and password with a link to our voting platform.

The election results will be announced at the [LIVE23 online meeting](/crossref-live-annual/) on October 31st, 2023. Save the date! Incoming members will take their seats at the March 2024 board meeting.
