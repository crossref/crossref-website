---
title: 'Perspectives: Audrey Kenni-Nemaleu on scholarly communications in Cameroon'
author: Audrey Kenni-Nemaleu
draft: false
authors:
  - Audrey Kenni-Nemaleu
  - Kora Korzec
  - Johanssen Obanda
  - Rosa Morais Clark
date: 2023-10-05
categories:
  - Community
  - Perspectives
archives:
    - 2023
---

{{% imagewrap left %}} <img src="/images/blog/2022/perspectives.png" alt="sound bar logo" width="150px" class="img-responsive" /> {{% /imagewrap %}}  

Our Perspectives blog series highlights different members of our diverse, global community at Crossref. We learn more about their lives and how they came to know and work with us, and we hear insights about the scholarly research landscape in their country, the challenges they face, and their plans for the future.

Notre série de blogs Perspectives met en lumière différents membres de la communauté internationale de Crossref. Nous en apprenons davantage sur leur vie et sur la manière dont ils ont appris à nous connaître et à travailler avec nous, et nous entendons parler du paysage de la recherche universitaire dans leur pays, des défis auxquels ils sont confrontés et de leurs projets pour l'avenir.

<!--more-->

Today, we meet Audrey Kenni-Nemaleu, Crossref Ambassador in Cameroon and Assistant Editor of the Pan-African Medical Journal (PAMJ). Audrey is excited about engaging Crossref's community in French West Africa. Please take a moment to read and listen to Audrey's perspective. 

Aujourd'hui, nous rencontrons Audrey Kenni-Nemaleu, ambassadrice Crossref au Cameroun et rédactrice adjointe du Pan-African Medical Journal (PAMJ). Audrey est enthousiaste à l'idée d'impliquer la communauté Crossref en Afrique occidentale française. Veuillez prendre un moment pour lire et écouter le point de vue d'Audrey.

{{% row %}} {{% column %}}

**<p align="center">English</p>**

<div style="padding:133.33% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/895601167?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Audrey's Perspective - Crossref Ambassador - English"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


{{% /column %}}
{{% column %}}

**<p align="center">Français</p>**

<div style="padding:133.33% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/895601175?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Audrey's Perspective - Crossref Ambassador - French"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

{{% /column %}}{{% /row %}}

<br><br>

>Tell us a bit about your organization, your objectives, and your role  
Pouvez-vous nous parler de votre organisation, vos objectifs et votre rôle ?

My name is Audrey Kenni Nganmeni-Nemaleu, assistant editor for [the Pan-African Medical Journal](https://www.panafrican-med-journal.com/). I am specifically responsible for editing the articles in terms of form, ensuring that they meet the journal's standards. Furthermore, I am the focal point of my journal for Crossref, that is to say I am responsible for managing all the problems that all publishers may encounter with DOIs and the various Crossref services to which our journal has subscribed. My role is also to manage all the conflicts that we may encounter with the DOIs submitted to Crossref. I train our journal staff in using Crossref services. I am also the focal point of my journal for COPE (Committee of Publications Ethics) which is an organization that helps to regulate ethical publishing practices. It is in this capacity that I participate COPE's webinars on behalf of our journal.  

Je m’appelle Audrey Kenni Nganmeni Nemaleu, éditrice assistante pour le Pan African Medical Journal. Je m’occupe précisément de traiter les articles sur le plan de la forme en m’assurant qu’ils respectent les normes du journal. Par ailleurs je suis point focal de mon journal pour Crossref c’est-à-dire je suis chargée de gérer tous les problèmes que l’ensemble des éditeurs peuvent rencontrer avec les DOIs et les différents services de Crossref auxquels notre journal a souscrit. Mon rôle également c’est de gérer tous les conflits qu’on peut rencontrer avec les DOIs soumis à Crossref. Je forme également le personnel de notre journal à l’utilisation des services de Crossref. Je suis aussi point focal de mon journal pour COPE (Committee of Publications ethics) qui est un organisme qui aide dans la régulation des pratiques éthiques en matière de publication. C’est dans ce cadre que je participe à tous les webinaires de cette organisation afin qu’il y ait toujours au moins une personne qui participe à ces webinaires pour le compte de notre journal.  


>What is one thing that others should know about your country and its research activity?  
Que doivent savoir les autres sur les activités de recherche dans votre pays ?

In my country, Cameroon, the research activity is still young. There are few scientific journals and we are actually the most influential journal in our country and subregion. There are also few schools or institutions that focus especially on research. For the time being, research activities in my country mainly revolve around congresses and conferences where researchers can exhibit their works. There is very little support for scientific research in my country.



Dans mon pays, le Cameroun, la recherche scientifique est encore jeune. Il existe peu de revues scientifiques et nous sommes en fait le journal le plus influent de notre pays et de notre sous-région. Il existe également peu d'écoles ou d'nstitutions qui spécialisées sur la recherche. Pour l'instant, les activités de recherche dans mon pays s'articulent principalement autour de congrès
et de conférences où les chercheurs peuvent exposer leurs travaux. Il y a très peu de soutien à la recherche scientifique dans mon pays.


>Are there trends in scholarly communications that are unique to your part of the world?  
Existe-t-il des tendances particulières en matière de recherche scientifique dans votre région ?

In this part of the world, we do our best to follow the code of ethics of the various organizations in which we are a member: Committee of publication ethics (COPE), World Association of Medical Editors (WAME), Open Access Scholarly Publishing Association (OASPA). What we have seen emerging recently is the organization, by professional scientific societies, of small conferences, workshops and meetings to exchange information. These small events are less costly to organize, hence their gain in popularity. We support these activities through sponsorship, and use them as opportunities to strengthen young researchers' capacities in areas such as scientific writing, publication ethics. We also use those opportunities to introduce to young researchers concepts such as Open Access, Open Science, DOIs and other modern publishing services.

Dans notre pays, nous nous efforçons de suivre le code de déontologie des différentes organisations dont nous sommes membre : Committee of publication ethics (COPE), World Association of Medical Editors (WAME), Open Access Scholarly Publishing Association (OASPA). Ce que l'on a vu émerger récemment, c'est l'organisation, par des sociétés scientifiques professionnelles, de petits colloques, ateliers et réunions d'échange d'informations. Ces petits événements sont moins coûteux à organiser, d'où leur gain en popularité. Nous soutenons ces activités par le sponsoring et les utilisons comme des opportunités pour renforcer les capacités des jeunes chercheurs dans des domaines tels que l'écriture scientifique, l'éthique de la publication. Nous utilisons également ces opportunités pour leur présenter des concepts tels que le libre accès, la science ouverte, les DOIs et d'autres services d'édition modernes.


>What about any political policies, challenges, or mandates that you have to consider in your work?  
Quels sont les politiques, défis ou mandats auxquels vous faites face dans votre travail ?

Operating a journal in our context is challenging. The critical challenges are as basic as constant availability of electricity or stable and fast internet connectivity. How to maintain a stable stream revenue to support the journal is also a critical challenge. Most of our authors are young, self-funded and with limited resources. Most cannot afford the amount we charge for article publishing fees, which in comparison, is very limited. So we have to be extremely creative to operate.



Faire fonctionner une revue dans notre contexte est difficile. Les défis critiques sont aussi fondamentaux que la disponibilité constante de l'électricité ou une connexion Internet stable et rapide. Comment maintenir un flux stable des revenus pour soutenir la revue constitue également un défi crucial. La plupart de nos auteurs sont jeunes, autofinancés, avec des ressources limitées et par conséquent n’arrivent pas à payer les frais de publication d'articles pourtant très bas. Nous devons donc être extrêmement créatifs pour gérer nos charges.


>How would you describe the value of being part of the Crossref community; what impact has your participation had on your goals?  
Comment décririez-vous la valeur de faire partie de la communauté Crossref ? Quel est l’impact de votre participation sur vos objectifs ?  

As a Crossref ambassador, I talk about Crossref around me, among my colleagues whether they are in Kenya or Cameroon. I shared the links to participate in Crossref webinars with my colleagues. I invited them to become ambassadors by sharing with them the links to join the community. I participated in several ambassador training webinars on different themes including: how to submit DOI to Crossref, ORCID. I participated in a Crossref event in Nairobi, Kenya. It was a memorable moment where I was able to meet other ambassadors. We were able to have a small meeting on the difficulties we encountered in growing the Crossref community in Africa. We produced a document to this effect which we submitted to Crossref in 2022. For the moment, I have not yet been able to organize an event as an ambassador, but I would like to with the help of Crossref. But being an ambassador is not the easiest thing because sometimes in our context people do not understand the use of Crossref's services because we are in an environment where the DOI is not yet very well known, and where even publishers know nothing about this. A question I am often asked is whether this work is paid and are discouraged when they learn that it is voluntary work.

Comme ambassadrice de Crossref, je parle autour de moi de Crossref, parmi mes collègues qu’ils soient au Kenya ou au Cameroun. J’ai partagé les liens pour participer à des webinaires de Crossref à mes collègues. Je les ai invités à devenir des ambassadeurs en partageant avec eux les liens pour rejoindre la communauté. J’ai participé à plusieurs webinaires de formation des ambassadeurs sur différents thèmes notamment ORCID. J’ai également participe à un évènement de Crossref à Nairobi au Kenya. C’était un moment mémorable ou j’ai pu rencontrer d’autres ambassadeurs. Nous avons pu faire une petite réunion sur les difficultés que nous rencontrons pour faire grandir la communauté Crossref en Afrique. Nous avons d’ailleurs produit un document à cet effet que nous avons soumis à Crossref en 2022. Pour l’instant, je n’ai pas encore pu organiser d’évènement dans le cadre d’ambassadeur, mais j’aimerais avec l’aide de Crossref voir comment le faire. Etre ambassadrice n’est pas la chose la plus facile car parfois dans notre contexte les gens ne comprennent pas le bien-fondé des services de Crossref car on est dans un environnement ou le DOI n’est pas encore très connu, et où beaucoup de journaux et même d’editeurs ne savent rien de cela. Une question qu’on me pose souvent est celle savoir si ce travail est remunere et se découragent quand ils apprennent que c’est du bénévolat. 


>For you, what would be the most important thing Crossref could change (do more of/do better in)?  
Pour vous, quelle serait la chose la plus importante que Crossref pourrait changer (faire plus/faire mieux) ?  

Crossref could invest in more capacity building, events, and communications in this part of the world. Why not localize Crossref in the francophone part of Africa? Crossref could offer continuing educational activities to professionals in order to improve their skills or acquire new knowledge in metadata and correlative disciplines. Crossref could also sponsor/support journal publishing and scholarship in Africa.

Crossref pourrait investir dans davantage de renforcement des capacités, d'événements et de communications dans cette partie du monde. Pourquoi ne pas localiser Crossref dans la partie francophone de l’Afrique ? Crossref pourrait proposer des activités de formation continue aux professionnels afin d'améliorer leurs compétences ou d'acquérir de nouvelles connaissances dans les métadonnées et les disciplines corrélatives. Crossref pourrait également sponsoriser/soutenir la publication de revues et les bourses d’études en Afrique.


>Which other organizations do you collaborate with or are pivotal to your work in open science?  
Avec quelles autres organisations collaborez-vous ou alors quelles sont les organismes pivot au cœur de votre travail en science ouverte ?  

I collaborate with various institutions such as [COPE (Committee on Publication Ethics)](https://publicationethics.org/), [AJOL African Journals Online](https://www.ajol.info/), and [OASPA (Open Access Scholarly Publishing Association)](https://oaspa.org/). I attend webinars of these organizations on behalf of my journal.

Je collabore avec diverses institutions telles que [COPE (Committee on Publication Ethics)](https://publicationethics.org/), [AJOL African Journals Online](https://www.ajol.info/), et [OASPA (Open Access Scholarly Publishing Association)](https://oaspa.org/). J'assiste à des webinaires de ceux-ci organisations au nom de ma revue.


>What are your plans for the future?  
Quels sont vos plans pour l'avenir ?  

My plan for the future is to continue working in science communication with different other organizations, and more within my community.

Mon plan pour l'avenir est de continuer à travailler dans le domaine de la communication scientifique avec différentes autres organisations, et davantage au sein de ma communauté.

Thank you, Audrey!  

Merci, Audrey !  
