---
title: Revised Crossref DOI display guidelines are now active
author: Ed Pentz
draft: false
authors:
  - Ed Pentz
date: 2017-03-15
categories:
  - DOIs
  - linking
  - Member Briefing
  - Identifiers
  - Publishing
archives:
  - 2017
---

{{% imagewrap right %}} <img src="/images/blog/crossref-doi-display-march-2017.jpg
" alt="Crossref DOI Display" width="300px" /> {{% /imagewrap %}}

We have updated our DOI display guidelines as of March 2017, this month!  I described the what and the why in my previous blog post [New Crossref DOI display guidelines are on the way](/blog/new-crossref-doi-display-guidelines-are-on-the-way) and in an email I wrote to all our members in September 2016. I’m pleased to say that the updated Crossref [DOI display guidelines are available via this fantastic new website](https://doi.org/10.13003/5jchdy) and are now active. Here is the URL of the full set of guidelines in case you want to bookmark it (https://doi.org/10.13003/5jchdy) and a shareable image to spread the word on social media.<!--more-->

This blog is a quick reminder that all Crossref members should now be displaying DOIs in the [recommended new format](https://doi.org/10.13003/5jchdy) from this month, on any new content you publish online. Please note these guidelines are for Crossref DOIs only, we have nearly 90 million registered but there are others, and [not all DOIs are made equal](/membership/#member-obligations-and-benefits/).

The main changes are to display the DOI as a full, linked URL using HTTPS:

```https://doi.org/10.xxxx/xxxxx```

For background on the HTTPS issue please read Geoffrey Bilder’s blog post, [Linking DOIs using HTTPS](/blog/linking-dois-using-https-the-background-to-our-new-guidelines).

## What will happen if you don’t update your Crossref DOI display?

We tell members that they should be working towards making the change even if they can’t do it until later - we recognize that it is not always an easy change to make.

However, if members don’t make the change, nothing immediate will happen (Crossref won’t fine you!) although as more members make the change your display will look odd and out of place compared with other members’ content.  

### If you have any questions please do not hesitate to [contact us](mailto:feedback@crossref.org).
