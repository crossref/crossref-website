---
title: 'Taking the "con" out of conferences'
slug: taking-the-con-out-of-conferences
author: Geoffrey Bilder
draft: false
authors:
  - Geoffrey Bilder
date: 2017-02-15
categories:
  - DOIs
  - Identifiers
  - Conferences
  - DataCite
archives:
  - 2017
---



TL;DR

Crossref and DataCite are forming a working group to explore conference identifiers and project identifiers. If you are interested in joining this working group *and* in doing some actual work for it, please contact us at `community@crossref.org`  and include the text `conference identifiers WG` in the subject heading. <br> <!--more-->

<p align="center">
<img src="/images/blog/mouse-ears.png" alt= "Mouse ears"/>
</p>


## All the times I could have gone to Walt Disney World...  <br> <br>

Back around 2010 I added a filter to my email settings that automatically flagged and binned any email that contained the word "Orlando." Back then this was a remarkably effective way of detecting and ignoring spam from the numerous fake technology conferences that all seemed to advertise the city of Orlando, Florida as the location for their non-events. I suspected they all chose Orlando as it would provide the [punter](http://dictionary.cambridge.org/dictionary/english/punter) that little bit of extra motivation to pay and register for the conference as they simultaneously plotted how they could tag-on some holiday time at Walt Disney World. I finally had to remove the filter last year when I realised that the scammers had moved on to advertising more realistically gritty cities in their calls for submissions and that meanwhile I had managed to miss all the mail informing me of the [ALA's summer 2016 meeting](http://2016.alaannual.org/) held in, you guessed it... Orlando.  <br> <br>

Clearly we need better mechanisms to flag dubious conferences.  <br> <br>

Late last year Crossref's Strategic initiatives group was approached by “CounterMock,” a group of Crossref members (including major proceedings publishers like  Springer Nature, Elsevier, IEEE, ACM, IET, etc) who were actively exploring the establishment of an identifier system and registry for scholarly conferences.  <br> <br>

The long term goal of the group is to make it easier for publishers, researchers and other stakeholders to identify fraudulent and/or low-quality conferences. There has recently been a proliferation of conferences that seem to have been developed specifically to dupe international and early-career researchers into paying substantial conference and publication fees. Sometimes these conferences are intentionally named after long-standing and well-respected conferences. At worst these conferences are entirely fake - no meetings are held and no publications are issued. At best they produce subpar publications of questionable academic integrity. Members of the group are concerned that these "mock conferences" (Hence "COUNTERMOCK") will:  <br> <br>

- Waste researcher time.<br> <br>
- Waste publisher time.<br> <br>
- Undermine academic trust in conferences and conference proceedings as a trustworthy means of scholarly communication.<br> <br>

The group understands that the "evaluation of a conference quality" and the "unambiguous identification of conferences" are separate concerns (as they are with publications, contributors, etc). But they also realise that it will be hard to address the quality issue without an infrastructure for unambiguously identifying conferences and providing meaningful provenance metadata about those conferences. Moreover, having unique identifiers for conference series would enable a number of other applications. Examples include conference-level metrics, better and more structured info about forthcoming conferences on a certain topic, and more visibility of conferences in research evaluation.  <br> <br>

Springer Nature has built a [POC prototype of a conference identifier system](http://lod.springer.com/data/search) and shown it to a number of other parties. The feedback has been that there is interest in the project, but that the consensus is that it should be managed a run by a neutral industry group. They have approached us to form a working group and explore how this project can be advanced.  <br> <br>

This is all good. Crossref itself doesn't make value judgements on the quality of content registered with us. [Crossref DOIs are not quality marks](/blog/dois-unambiguously-and-persistently-identify-published-trustworthy-citable-online-scholarly-literature-right/). But we do believe that unambiguous identification of research artifacts is a perquisite to building effective trust and reputation tools.<br> <br>

It is possible that the issue of conference identifiers can be folded into [the work we are doing with DataCite and ORCID on organization identifiers](/blog/the-organization-identifier-project-a-way-forward/). For example, some have argued that organization identifiers should include identifiers for projects or other less formal and more ephemeral corporate entities that are often included in affiliation and/or bibliographic data. It is possible to make the similar arguments in the case of conferences.<br> <br>

On the other hand we have also been interested in the issue of "project identifiers." [Martin Fenner](http://orcid.org/0000-0003-1419-2405) and [Tom Demeranville](http://orcid.org/0000-0003-0902-4386) have [made a strong argument](https://doi.org/10.6084/m9.figshare.4216323.v2) that 'projects' can be thought of as containers for collections of project outputs, project members and project funders. Again, it seems plausible that one could make the same case for conferences.<br> <br>

At the very least it is important to coordinate any work that is done on conference, project and organization identifiers. This why we have decided to form a joint Crossref/DataCite working group to specifically explore conference and project identifiers and determine how they relate both to each other and to our already ongoing work with ORCID on organization identifiers.
<br> <br>
Additionally, it is likely that the working group will discuss and explore how conference/project identifiers might be used for increasing the transparency of peer review at conferences, better attribution for programme chairs and program committee members, and how they might be incorporated into other services like [Crossref Metadata Search](https://search.crossref.org), [DataCite search](https://search.datacite.org/), [CrossMark](/services/crossmark/), etc.<br> <br>

If you are interested in doing some work on this- then please indicate your interest in joining a working group by sending email to `community@crossref.org` and include the text `conference identifiers WG` in the subject heading.<br> <br>

We will update this blog as the group convenes and makes progress.<br> <br>

<p align="center">
<img src="/images/blog/florida.png" alt= "Florida"/>
</p>
