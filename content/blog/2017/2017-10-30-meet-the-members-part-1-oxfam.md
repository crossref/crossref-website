---
title: 'Meet the members, Part 1 (with Oxfam)'
author: Christine Cormack Wood
draft: false
authors:
  - Christine Cormack Wood
  - Liam Finnis
date: 2017-10-30

categories:
  - Identifiers
  - Meet The Members
archives:
  - 2017

---
Introducing our new blog series *Meet the members;* where we talk to some of our members and find out a little bit more about them, ask them to share how they use our services, and discuss what their plans for the future are. To start the series we talk to Liam Finnis of Oxfam.

<!--more-->

{{% imagewrap right %}} <img src="/images/blog/oxfam.jpg" alt=“Oxfam logo" height="250px" width="250px" class="img-responsive" /> {{% /imagewrap %}}

## Can you tell us a little bit about Oxfam?

Around the globe, Oxfam works to find practical, innovative ways for people to lift themselves out of poverty and thrive. We save lives and help rebuild livelihoods when crisis strikes. And we campaign so that the voices of the poor influence the local and global decisions that affect them.

Oxfam’s Policy & Practice platform is the gateway to Oxfam’s knowledge, experience, and thinking. Policy & Practice aims to influence, enable and learn from others by sharing and collaborating online with professionals and practitioners.

## What’s your role within Oxfam?

My name is Liam Finnis and I am the Website Manager for Oxfam GB’s [Policy & Practice](http://policy-practice.oxfam.org.uk/) site and the [Oxfam Digital Repository](http://oxfamilibrary.openrepository.com/oxfam/). In addition to maintenance and development of our platforms, my role focuses on raising the visibility of our programme work including approach and methodology, while also ensuring the availability and accessibility of our publications and resources.

## What’s your participation level?

We joined Crossref in 2016, but only really began fully implementing DOIs this year. We have registered 139 content items as of October, with the majority assigned in 2017. While this only constitutes a small number of our total publications (roughly 6%), we’ve focused on current and future publications rather than retroactive application (with a handful of exceptions).

## Tell us a bit about what you publish and for whom

We produce roughly 220 publications each year, with a library of 4,450 spanning 40 years. Roughly half of this would be considered grey literature and includes: research reports; evaluations; briefing papers; technical briefings; case studies; guidelines and toolkits. We also publish the *Gender & Development* *Journal* with Routledge/Taylor & Francis.

While our organisational focus is on inequalities and the eradication of poverty, this isn’t something we can achieve by looking solely at economic models. Our publications span a range of subject areas including: climate change; food and livelihoods; economics; gender; conflicts and disasters; land rights; and Water, Sanitation and Hygiene (WASH).

Our audience ranges from humanitarian and development practitioners to policy makers to researchers and academics. We publish the research that underpins our campaigns advocacy work; the evaluations of our emergency response efforts; reports outlining the methodologies we’ve applied; briefings on policy and recommendations; and, toolkits and guidelines for research, programme quality and responsible data management.

##  What do you think makes your publications unique?

Oxfam is one of the only NGOs that is actively sharing an extensive body of knowledge and experience. With 75 years of experience working on a global scale, our publications help to share learning and encourage best practice. Further to that, they showcase the changes (gradual or sudden) that we’ve seen in how development and humanitarian aid is defined and approached through the decades.

_[The Oxfam Gender Training Manual](https://oxfamilibrary.openrepository.com/oxfam/handle/10546/141359)_, published in 1991, remains one of our most frequently accessed resources; still widely regarded as a relevant, unique and valuable resource within the sector. Another of our key publications, _[Wealth: Having it all and wanting more](https://oxfamilibrary.openrepository.com/oxfam/handle/10546/338125)_, was published in 2015, outlining the methodology and data sources for Oxfam’s frequently cited fact ‘85 billionaires have the same wealth as the bottom half of the world’s population’.

The diversity in subject and format of our publications isn’t necessarily unique, but I’m reasonably confident that there is something in our publications that will relate to everyone.

## What trends are you seeing in your part of the scholarly publishing community?

This is difficult to quantify as, while we have been lightly engaged with the scholarly publishing community in the past, we’ve been significantly more active in the past year. In addition to more actively applying DOIs, early in 2017 we were included in EBSCO Discovery and in March we made efforts to improve the visibility of our Digital Repository. Previously, the key route was through the Policy & Practice website, which brought together publications with blogs and pages focused on programmes, projects, approaches and methodology.

Since making these two changes we’ve seen a significant increase in access of our resources directly from the repository. This has come in addition to the general usage through Policy & Practice. We are also working with Research4Life, INASP and TEEAL to improve visibility and accessibility of our publications more widely.

## How would you describe the value of being a Crossref member?

In the past two years, we’ve been looking into how we can ensure that our publications are visible and accessible to a wider audience. Becoming a member of Crossref and registering content with Crossref is a big part of that. It helps to give us a place in the discussions and events as well as enabling us to better understand and meeting scholarly publishing standards and implement best practice.

## What are Oxfam's plans for the future?

In terms of our work with Crossref and an active role in the scholarly publishing community, we’re still fairly new to it and we’re starting to see some of the benefits of our efforts. In the future, we’re looking to get a better idea of the opportunities available and build on our recent work.

Personally, I’m really interested in exploring Crossref Event Data in greater detail and seeing how it can help us map the impact of our work more effectively.

<br>
Thanks, Liam!
