---
title: 'Linking DOIs using HTTPs: the background to our new guidelines'
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2017-01-17
categories:
  - DOIs
  - Standards
  - Web
archives:
  - 2017

---

Recently we announced that we were making some new recommendations in our DOI display guidelines. One of them was to use the secure HTTPS protocol to link Crossref DOIs, instead of the insecure HTTP.
<!--more-->

<span style="font-weight: 400;">Some people asked whether the move to HTTPS might affect their ability to measure referrals (i.e. where the people who visit your site come from).</span>
<h2><span style="font-weight: 400;">TL;DR: Yes</span></h2>
<span style="font-weight: 400;">Yes. If you do </span><b>not</b><span style="font-weight: 400;"> move your DOI links to HTTPS, Crossref, its members and the members of </span><a href="http://www.doi.org/registration_agencies.html"><span style="font-weight: 400;">other DOI registration agencies</span></a><span style="font-weight: 400;"> (e.g. DataCite, JLC, CNKI)  will find it increasingly difficult to accurately measure referrals. You should link DOIs using HTTPS.</span>

<span style="font-weight: 400;">In fact, if you do not support HTTPS on your site </span><b>now</b><span style="font-weight: 400;">, it is likely that your ability to measure referrals is already impaired. If you do not already have a plan to move your site to HTTPS, you should develop one.</span>

<span style="font-weight: 400;">If you have already transitioned your site to HTTPS, you should follow the new guidelines and link DOIs via HTTPS as soon as possible. As it stands, you are not sending any referrer information when DOIs are clicked on and followed from your site. You should also make sure that the URLs you have registered with Crossref are HTTPS URLs, otherwise *you* will not get referrer information on your site when they are followed.</span>

<span style="font-weight: 400;">Read on if you want some grody details. We'll try to keep it as non-technical as possible.</span>
<h2><span style="font-weight: 400;">Two protocols, one web</span></h2>
<span style="font-weight: 400;">To start with your web browser supports two closely related protocols, HTTP and HTTPS.</span>

<span style="font-weight: 400;">The first, HTTP, is the protocol that the web started out with. It is an unencrypted protocol and it is also easy to intercept and modify. It is also very easy and inexpensive to implement.</span>

<span style="font-weight: 400;">The second protocol, HTTPS, is a secure version of the first protocol. It is very difficult to intercept and modify. It has historically been more complex and expensive to implement. </span>

<span style="font-weight: 400;">Here you might say - "Great, but HTTPS has been around for a long time. We've used it for sensitive transactions like authentication and credit card transactions. Why do we want to use DOI links with HTTPS?" Why are you suggesting that we should even consider moving our entire site to HTTPS? </span>
<h2><span style="font-weight: 400;">The pressure to move to HTTPS</span></h2>
<span style="font-weight: 400;">The insecure HTTP protocol has become a major vector for a lot of security issues on the web. It allows user web pages to be intercepted and modified between the server and the browser. This flaw is being abused for everything from spying, to inserting unwanted advertisements into web pages, to distributing viruses, ransomware and botnets. </span>

<span style="font-weight: 400;">As such, there has been a steady drumbeat of industry encouragement to move to the more secure HTTPS protocol for all website functions.</span>

<span style="font-weight: 400;">We are not going to argue all the points here. Instead we will mention the major constituencies that are advocating for a move to HTTPS and provide you with some pointers. We apologise that these are all so US-centric, but a lot of the web's global direction does seem to be presaged by US adoption trends.</span>
<h2><span style="font-weight: 400;">Google</span></h2>
<span style="font-weight: 400;">It is probably easiest to start with Google, since its practices tend to focus the attention of those managing websites.</span>

<span style="font-weight: 400;">Back in 2014 </span><a href="https://webmasters.googleblog.com/2014/08/https-as-ranking-signal.html"><span style="font-weight: 400;">Google announced that they would slowly move toward including the use of HTTPS as a ranking signal</span></a><span style="font-weight: 400;">. In 2015 they upped the ante by announcing that </span><a href="https://security.googleblog.com/2015/12/indexing-https-pages-by-default.html"><span style="font-weight: 400;">they would start indexing HTTPS versions of pages by default</span></a><span style="font-weight: 400;">. It looks like in early 2017 they will really start to take the gloves off as they </span><a href="https://www.chromium.org/Home/chromium-security/marking-http-as-non-secure"><span style="font-weight: 400;">modify their Chrome browser to flag sites that do not use HTTPS as being `insecure`</span></a><span style="font-weight: 400;">.</span>
<h2><span style="font-weight: 400;">Every top website, </span><a href="http://www.urbandictionary.com/define.php?term=evah"><span style="font-weight: 400;">evah</span></a></h2>
<span style="font-weight: 400;">It looks like Google's plan is working too. Their </span><a href="https://www.google.com/transparencyreport/https/grid/?hl=en"><span style="font-weight: 400;">2016 transparency report</span></a><span style="font-weight: 400;"> shows that most top websites have already transitioned to HTTPS and that this translates to approximately 25% of all web traffic worldwide taking place using HTTPS. Indeed, over 50% of all web pages viewed by desktop users are delivered via HTTPS.</span>
<h2><span style="font-weight: 400;">Government agencies</span></h2>
<span style="font-weight: 400;">The USA’s Whitehouse issued [</span><a href="https://www.whitehouse.gov/blog/2015/06/08/https-everywhere-government"><span style="font-weight: 400;">a directive instructing all Federal websites to adopt HTTPS</span></a><span style="font-weight: 400;">]. As of December 2016 </span><a href="https://pulse.cio.gov/"><span style="font-weight: 400;">64%</span></a><span style="font-weight: 400;"> of federal websites have made the transition.</span>
<h2><span style="font-weight: 400;">Libraries</span></h2>
<span style="font-weight: 400;">Much of the pressure to move to HTTPS is coming from the library community who have a historical tradition of protecting patron privacy and resisting efforts to censor content. The third principle of the American Library Association's code of ethics reads:</span>

<span style="font-weight: 400;">We protect each library user's right to privacy and confidentiality with respect to information sought or received and resources consulted, borrowed, acquired or transmitted.</span>

<span style="font-weight: 400;">Recently there has been </span><a href="https://www.eff.org/deeplinks/2016/12/librarians-act-now-protect-your-users-its-too-late"><span style="font-weight: 400;">a major push by the Electronic Frontier Foundation</span></a><span style="font-weight: 400;"> to get libraries to adopt a number of security and privacy practices, including the use of HTTPS by all library systems as well as those used by library vendors.</span>
<h2><span style="font-weight: 400;">What are Crossref members doing about HTTPS?</span></h2>
<span style="font-weight: 400;">How big an issue is this? How many of our members have moved to HTTPS? How many plan to? Well, we looked at the URLs that are registered with Crossref and we tested them with both protocols. Eventually we will write a blog post detailing our findings - but the highlights are:</span>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Slightly fewer than half of the member domains tested only support HTTP.</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Slightly fewer than half of the member domains tested support both HTTP and HTTPS.</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">About 370 of the member domains tested only support HTTPS.</span></li>
</ul>
<h2><span style="font-weight: 400;">The transition to HTTPS and the issue of DOI referrals</span></h2>
<span style="font-weight: 400;">The HTTP referrer is a piece of information passed on by a browser that indicates the site from which the user navigated.</span>

<span style="font-weight: 400;">So, for example, if a user visiting site `A` clicks on a link which takes them to site `B`, site `B` will then record in its logs that a user visited them from site A. Obviously, this is important information for understanding where your web site traffic comes from. </span>

<span style="font-weight: 400;">The default rules for referrals are<sup><a href="#fn1">1</a></sup></span><span style="font-weight: 400;">:</span>
<ol>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">If you link between two sites with the same level of security, all referral information is retained.</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">When you follow a link from an insecure (HTTP) web site to a secure (HTTPS) site, referral data is passed on to the secure web site. </span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">If you follow a link from a secure (HTTPS) web site to an insecure (HTTP) site, referral data is not passed on to the insecure web site.</span></li>
</ol>
<span style="font-weight: 400;">So let's see what the situation would look like with normal links. If we had two sites, `A` &amp; `B`, the following table maps the possible combinations of protocols that can be used to link from `A` to `B`. So, for example, row #2 reads:</span>
<blockquote><span style="font-weight: 400;">A user browses site A using HTTP and clicks on a HTTPS link to publisher B who hosts their site using HTTPS. </span></blockquote>
<span style="font-weight: 400;">The last column indicates if the referrer information is passed along by the browser. In the case of row #2, the answer is “yes”. The user has navigated from a less secure site to a more secure site.</span>
<table>
<tbody>
<tr>
<td><b>User views site A using</b></td>
<td><b>Site A links to site B using</b></td>
<td><b>Browser reports referrer to site B</b></td>
</tr>
<tr>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">No</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
</tr>
</tbody>
</table>
<span style="font-weight: 400;">
But this gets a little more complicated with DOIs. In this case publisher `A` links to publisher `B` through the DOI system. This means there are two parts to the link. The first `(A-&gt;doi.org)` results in a redirect (A-&gt;B). Again we use the last columns to indicate when referrer information is passed along to site B. Again, let’s look at row #2. It reads:</span>

<span style="font-weight: 400;">A user browses the site of member A using HTTP and clicks on a HTTP DOI link. The DOI system redirects the browser to member B using an HTTPS link registered with Crossref by member B. The middle column and the last column records whether Crossref and the publisher were able to see referrer information. The answer in both cases is “yes”. In the first case (A-&gt;DOI) because the link was from a less secure site (HTTP on A) to a more secure site (HTTPS at DOI). The second case because the link is between two sites at the same security level (HTTP).</span>
<table>
<tbody>
<tr>
<td></td>
<td><b>User views site A using</b></td>
<td><b>Site A links DOI using</b></td>
<td><b>Browser reports referrer to Crossref<sup><a href="#fn2">2</a></sup></b></td>
<td><b>Crossref redirects to site B using<sup><a href="#fn3">3</a></sup></b></td>
<td><b>Browser reports referrer to site B</b></td>
</tr>
<tr>
<td><span style="font-weight: 400;">1</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">2</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">3</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">4</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">5</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">No</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">No</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">6</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">No</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">No</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">7</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
<td><span style="font-weight: 400;">HTTP</span></td>
<td><span style="font-weight: 400;">No</span></td>
</tr>
<tr>
<td><span style="font-weight: 400;">8</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
<td><span style="font-weight: 400;">HTTPS</span></td>
<td><span style="font-weight: 400;">Yes</span></td>
</tr>
</tbody>
</table>
<h2><span style="font-weight: 400;">
So what does this mean?</span></h2>
<span style="font-weight: 400;">Our old display guidelines recommended linking DOIs using HTTP. Rows #1, #2, #5, #6 represent the status quo.</span>

<span style="font-weight: 400;">About half of our members support HTTPS. A few support it exclusively and it seems, given the industry pressures mentioned above, those who support both protocols are likely doing so as a transition stage to HTTPS-only sites.</span>

<span style="font-weight: 400;">This means that </span><b>the scenarios represented in row #5 &amp; #6 are already happening</b><span style="font-weight: 400;">. The referral information for any user viewing one of our member sites using HTTPS is being lost when they click on DOIs that use the HTTP protocol. Crossref doesn't get the referral data and neither does the member whose DOI has been clicked on.</span>

<span style="font-weight: 400;">Of course this applies to non-member sites that link to DOIs as well. Wikipedia is the largest referrer of DOIs from outside the industry. In 2015 The Wikimedia Foundation </span><a href="https://blog.wikimedia.org/2015/06/12/securing-wikimedia-sites-with-https/"><span style="font-weight: 400;">made a highly publicised transition</span></a><span style="font-weight: 400;"> to HTTPS on all of their sites. This means that any of our members who are running HTTP sites have already lost the ability to see any referral information from Wikipedia on their own sites. However, Crossref </span><a href="http://blog.crossref.org/2016/05/https-and-wikipedia.html"><span style="font-weight: 400;">worked closely with Wikimedia</span></a><span style="font-weight: 400;"> to ensure that, at the very least, Crossref was still able to record Wikimedia referral data on behalf of our members.</span>
<h2><span style="font-weight: 400;">A solution</span></h2>
<span style="font-weight: 400;">It is largely this work with Wikimedia that has helped us to understand just how important it is for Crossref to get ahead of the curve in helping our community to transition to HTTPS.</span>

<span style="font-weight: 400;">As long as our members are running a combination of HTTP and HTTPS sites, there is no way for our community to avoid some disruption in the flow of referral data. And we certainly would never entertain the notion of asking our members to keep using HTTP.The best we can do is recommend a practice that will help smooth the transition to HTTPS. That is what we are doing.Our new recommendation is to move to linking DOIs using HTTPS. This is represented in rows #3, #4, #7 and #8 in the table above. </span>

<span style="font-weight: 400;">This is a particularly important step for our members who have already moved to hosting their sites on HTTPS. As long as they are using HTTP DOIs on their site, they will be sending no referral traffic to Crossref, other Crossref members or other users of the DOI infrastructure. This is captured in scenarios #5 and #6.</span>

<span style="font-weight: 400;">If our linking guidelines are followed during the industry’s transition to HTTPS, then scenario #5 and #6 will eventually be replaced with scenario #7. It is still not perfect, but at least it means that, during the transition, publishers who are still running HTTP sites will be able to get some DOI referral data via Crossref. And of course, once our members have widely transitioned to HTTPS, everything will go back to normal and they will be able to see referral data on their own sites as well (i.e.they will have moved from the state represented in row #1 to state represented in row #8.)</span>

<span style="font-weight: 400;">In summary, please change your sites to use HTTPS to link DOIs. They should look like this:</span>

<a href="https://doi.org/10.7554/eLife.20320"><span style="font-weight: 400;">https://doi.org/10.7554/eLife.20320</span></a>
<h2><span style="font-weight: 400;">FAQ</span></h2>
<span style="font-weight: 400;"><strong>Q:</strong> If I have moved my site to HTTPS, do I need to redeposit my URLs to that they use the HTTPS protocol instead?</span>

<span style="font-weight: 400;"><strong>A:</strong> Yes. If you want to be able to still collect referrer information on your site (scenario #8) as opposed to via Crossref (scenario #7).</span>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> But can’t I avoid redepositing my URLs and get referrer data again if I simply redirect HTTP URLs to HTTPS on my own site?</span>

<span style="font-weight: 400;"><strong>A:</strong> No. The browser will strip referrer information if there is any HTTP step in the redirects. Even if the redirect is done on your own site.</span>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> Can I avoid having to redeposit all my URLs? Can’t Crossref just update the protocol on our existing DOIs for us?</span>

<span style="font-weight: 400;"><strong>A:</strong> Contact </span><a href="mailto:support@crossref.org"><span style="font-weight: 400;">support@crossref.org</span></a><span style="font-weight: 400;">. We’ll see what we can do.</span>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> What about all the old PDFs that are are there? They link to DOIs using HTTP. </span>

<span style="font-weight: 400;"><strong>A:</strong> That is true. But links followed from PDFs don’t send referrer information anyway.</span>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> And what about my new PDFs? Should I start linking DOIs from them using HTTPS.</span>

<span style="font-weight: 400;"><strong>A:</strong> Probably. But not because of the DOI referrer problem. Simply because HTTPS is a more secure, private, and future-proof protocol.</span>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> Don’t some countries block HTTPS?</span>

<span style="font-weight: 400;"><strong>A:</strong> Typically countries block specific sites and/or services. We do not know of any countries that have a blanket block on the HTTPS protocol.</span>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> I use a link resolver that uses OpenURL + a  cookie pusher to redirect my users to local resources. What do I need to do?</span>

<span style="font-weight: 400;"><strong>A:</strong> You need to change your cookie pusher script to enable the `Secure` attribute for cookies for HTTPS-linked DOIs.   </span>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> Can I use protocol-relative URLs (e.g. </span><a href="https://doi.org/10.7554/eLife.20320"><span style="font-weight: 400;">//doi.org/10.7554/eLife.20320</span></a><span style="font-weight: 400;">)?</span>

<span style="font-weight: 400;"><strong>A:</strong> Protocol-relative URLs can be used in HTML HREFs to help ease the transition from HTTP to HTTPS, but use the full protocol in the text of the DOI link itself. So, for example, the following is fine:</span><span style="font-weight: 400;">
</span><span style="font-weight: 400;">
</span>
<pre><a href="//doi.org/10.7554/eLife.20320">https://doi.org/10.7554/eLife.20320</a></pre>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> I hear that HTTP and HTTPS versions of URI identifiers are considered to be different identifiers. Doesn’t this mean that by moving to HTTPS we are essentially doubling the number of DOI-based identifiers out there?</span>

<span style="font-weight: 400;"><strong>A:</strong> Yes. It isn’t a problem that is only being faced by DOIs. Basically all HTTP-URI based identifiers face the same issue. We will put in place appropriate same-as assertions in our metadata and HTTP headers to allow people to understand that the HTTP and HTTPS representations of the DOI point to the same thing. </span>

<em>On a personal note (@gbilder speaking- don’t blame @CrossrefOrg) - it breaks my brain that the official line is that the protocol difference means they are different identifiers. As a practical matter (a concept the W3C seems to be increasingly alienated from), it would be insane for anybody to follow this policy to the letter. You can probably be pretty safe swapping the protocols on DOIs and being sure you will get the same thing.</em>

<hr />

<span style="font-weight: 400;"><strong>Q:</strong> I see that the Crossref site isn’t running on HTTPS. Are you just a bunch of hypocrites?</span>

<span style="font-weight: 400;"><strong>A:</strong> ~~Yes. The site will be moving to HTTPS-only very soon. Then we won’t be.~~ We do now.</span>

&nbsp;
<h2><span style="font-weight: 400;">References</span></h2>
<ol>
 	<li id="fn1">These rules can be tweaked using meta referrer tags (https://www.w3.org/TR/referrer-policy/), but not in any way that both avoids the fundamental problems outlined here <b>and</b> that preserves the security/privacy characteristics that are the very reason to implement HTTPS in the first place.</li>
 	<li id="fn2">To be pedantic- it actually passes referrer information to the DOI proxy (https://doi.org/), which in turn is reported to Crossref.</li>
 	<li id="fn3">To continue with the pedantry- the DOI proxy does the redirect based on the URL member B has deposited with Crossref.</li>
</ol>
