---
title: Add linked images to PDFs
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2010-08-16

categories:
  - Crossref Labs
  - PDF
archives:
  - 2010

---
While working on an internal project, we developed &#8220;[pdfstamp][1]&#8220;, a command-line tool that allows one to easily apply linked images to PDFs. We thought some in our community might find it useful and have [released it on github.][2] Some more PDF-related tools will follow soon.

 [1]: https://www.crossref.org/labs/pdfstamp/
 [2]: http://github.com/Crossref/pdfstamp