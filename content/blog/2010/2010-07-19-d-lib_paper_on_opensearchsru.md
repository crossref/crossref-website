---
title: OpenSearch/SRU Integration Paper
author: Tony Hammond
authors:
  - Tony Hammond
date: 2010-07-19
categories:
  - Interoperability
  - Search
  - Standards
aliases: "/blog/opensearch/"

Archives:
  - 2010

---
Since I’ve already blogged about this a number of times before here, I thought I ought to include a link to a fuller writeup in this month’s [D-Lib Magazine][1] of our [nature.com OpenSearch][2] service which serves as a case study in OpenSearch and SRU integration:

[<img border="0" src="/wp/blog/images/dlib-page.png" height="320" width="450" />][3]

[doi:10.1045/july2010-hammond][3]

 [1]: http://dlib.org
 [2]: https://www.nature.com/opensearch/
 [3]: http://dlib.org/dlib/july10/hammond/07hammond.html
