---
title: 'DOI: What Do We Got?'
author: Tony Hammond
authors:
  - Tony Hammond
date: 2010-02-09

categories:
  - Linked Data
archives:
  - 2010

---
[<img border=0 alt="doi-what-do-we-got.png" src="/wp/blog/images/doi-what-do-we-got.png" width="467" height="323" />][1]

(Click image for full size graphic.)

Following the JISC seminar last week on persistent identifiers (`#jiscpid` on Twitter) there was some discussion about DOI and its role within a [Linked Data][2] context. John Erickson has responded with a very thoughtful post [DOIs, URIs and Cool Resolution][3], which ably summarizes how the current problem with DOI in that the way the DOI is is implemented by the handle HTTP proxy may not have kept pace with actual HTTP developments. (For example, John notes that the proxy is not capable of dealing with &#8216;Accept’ headers.) He has proposed a solution, and the post has attracted several comments.

I just wanted to offer here the above diagram in an attempt to corral some of the various facets relating to DOI that I am aware of. I realize that this may seem like an open invitation to flame on - and this is a very preliminary draft - but &#8230; be kind!

So, this may be totally off the wall but it represents my best understanding of DOI as used by Crossref.

I have distinguished three main contexts:

  1. **Generic Data** - A generalized information context where the an object is identified with a DOI, an identifier system that is currently being ratified through the ISO process. This is the raw DOI number. (This definitely is not a first class object on the Web as it has no URI.)
      * **Web Data** - An online information context (here I use the term &#8216;Web’ in its widest sense) where resources are identified by URI (not necessarily an HTTP URI). Here DOI is represented under two URI schemes: &#8216;doi:’ (unregistered but preferred by Crossref), and &#8216;info:’ (registered and available for general URI use). Also it has a presence on the Web via an HTTP proxy (dx.doi.org) URL where it is used as a slug to create a permalink (as listed at &#8216;A’). A simple HTTP redirect is used (with status code 302) to turn this permalink into the publisher response page http://example/1. (Note that typically a second redirect will occur on the publisher platform, here shown by the redirect to http://example/2.)
          * **Linked Data** - An online information context where resources are identified by HTTP URI and conform to Linked Data principles. Now this is where there is a tension arises between the common publisher perspective and the strict semantic viewpoint. Implicit in the general Web context given above was the notion that the permalink (&#8216;A’) was somehow related to the abstract object and the redirection service applied to it associated the abstract resource with concrete representations of the object.</ol>
            So how do we relate the DOI HTTP URI with the abstract (&#8216;work’) identifier listed at &#8216;D’ in the diagram?

            Well the [Architecture of the World Wide Web][4] recognizes two distinct classes of resources: Information Resources (IR) and Non-Information Resources (NR). (Note: Only the term &#8216;information resource’ is used in AWWW.) IR are those that can be directly retrieved using HTTP, whereas NR are not directly retrievable but have an associated description which is retrievable and is itself a proxy for the real world object.

            So either the HTTP URI denotes an IR (as listed at &#8216;B’) and is resolved (through HTTP status code &#8216;302 Found’) to a default representation, which is the view that the Linked Data community would currently have of DOI. But this is at odds with what the Crossref position which regards DOI as identifying the abstract work. Alternately to fit better the Crossref model of DOI the HTTP URI would denote an NR (as listed at &#8216;A’) which would be resolved (through HTTP status code &#8216;303 See Other’) to an associated description - a publisher response page.

            There will be those self-appointed URI czars who will bemoan the fact of there being multiple URIs. But frankly there is nothing inherently wrong with that. Just as in the real world there are many languages so in the online world there are multiple contexts and histories. We can attempt to make some sense of this by making use of the well-known semantic properties `owl:sameAs` and `ore:similarTo` and declare (as also shown in the diagram) the following assertions:

            > ``[<img border=0 alt="doi-what-do-we-got.png" src="/wp/blog/images/doi-what-do-we-got.png" width="467" height="323" />][1]

(Click image for full size graphic.)

Following the JISC seminar last week on persistent identifiers (`#jiscpid` on Twitter) there was some discussion about DOI and its role within a [Linked Data][2] context. John Erickson has responded with a very thoughtful post [DOIs, URIs and Cool Resolution][3], which ably summarizes how the current problem with DOI in that the way the DOI is is implemented by the handle HTTP proxy may not have kept pace with actual HTTP developments. (For example, John notes that the proxy is not capable of dealing with &#8216;Accept’ headers.) He has proposed a solution, and the post has attracted several comments.

I just wanted to offer here the above diagram in an attempt to corral some of the various facets relating to DOI that I am aware of. I realize that this may seem like an open invitation to flame on - and this is a very preliminary draft - but &#8230; be kind!

So, this may be totally off the wall but it represents my best understanding of DOI as used by Crossref.

I have distinguished three main contexts:

  1. **Generic Data** - A generalized information context where the an object is identified with a DOI, an identifier system that is currently being ratified through the ISO process. This is the raw DOI number. (This definitely is not a first class object on the Web as it has no URI.)
      * **Web Data** - An online information context (here I use the term &#8216;Web’ in its widest sense) where resources are identified by URI (not necessarily an HTTP URI). Here DOI is represented under two URI schemes: &#8216;doi:’ (unregistered but preferred by Crossref), and &#8216;info:’ (registered and available for general URI use). Also it has a presence on the Web via an HTTP proxy (dx.doi.org) URL where it is used as a slug to create a permalink (as listed at &#8216;A’). A simple HTTP redirect is used (with status code 302) to turn this permalink into the publisher response page http://example/1. (Note that typically a second redirect will occur on the publisher platform, here shown by the redirect to http://example/2.)
          * **Linked Data** - An online information context where resources are identified by HTTP URI and conform to Linked Data principles. Now this is where there is a tension arises between the common publisher perspective and the strict semantic viewpoint. Implicit in the general Web context given above was the notion that the permalink (&#8216;A’) was somehow related to the abstract object and the redirection service applied to it associated the abstract resource with concrete representations of the object.</ol>
            So how do we relate the DOI HTTP URI with the abstract (&#8216;work’) identifier listed at &#8216;D’ in the diagram?

            Well the [Architecture of the World Wide Web][4] recognizes two distinct classes of resources: Information Resources (IR) and Non-Information Resources (NR). (Note: Only the term &#8216;information resource’ is used in AWWW.) IR are those that can be directly retrieved using HTTP, whereas NR are not directly retrievable but have an associated description which is retrievable and is itself a proxy for the real world object.

            So either the HTTP URI denotes an IR (as listed at &#8216;B’) and is resolved (through HTTP status code &#8216;302 Found’) to a default representation, which is the view that the Linked Data community would currently have of DOI. But this is at odds with what the Crossref position which regards DOI as identifying the abstract work. Alternately to fit better the Crossref model of DOI the HTTP URI would denote an NR (as listed at &#8216;A’) which would be resolved (through HTTP status code &#8216;303 See Other’) to an associated description - a publisher response page.

            There will be those self-appointed URI czars who will bemoan the fact of there being multiple URIs. But frankly there is nothing inherently wrong with that. Just as in the real world there are many languages so in the online world there are multiple contexts and histories. We can attempt to make some sense of this by making use of the well-known semantic properties `owl:sameAs` and `ore:similarTo` and declare (as also shown in the diagram) the following assertions:

            >``

            Note that ore:similarTo (stemming from the [OAI-ORE][5] work) is a weaker kind of relationship than owl:sameAs (which comes from [OWL][6]) and may be appropriate in this usage.

            In sum, scenario &#8216;A’ is what we have currently implemented, scenario &#8216;B’ is what might be commonly perceived as being implemented, and scenario &#8216;C’ may be a more correct semantic position.

            Your comments (and not unkind comments, please;) are more than welcome.

 [1]: /wp/blog/images/doi-what-do-we-got.png
 [2]: https://web.archive.org/web/20100413223720/http://linkeddata.org/
 [3]: http://bitwacker.wordpress.com/2010/02/04/dois-uris-and-cool-resolution
 [4]: http://www.w3.org/TR/webarch/
 [5]: http://www.openarchives.org/ore/
 [6]: http://www.w3.org/TR/owl-ref/
