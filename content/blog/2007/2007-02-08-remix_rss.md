---
title: Remixing RSS
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-02-08

categories:
  - RSS
archives:
  - 2007

---
Niall Kennedy has a [post][1] about the newly released [Yahoo! Pipes][2]. As he says:

> _&#8220;Yahoo! Pipes lets any Yahoo! registered user enter a set of data inputs and filter their results. You might splice a feed of your latest bookmarks on del.icio.us with the latest posts from your blog and your latest photographs posted to Flickr.&#8221;_

He also warns about possible implications for web publishers:

> _&#8220;Yahoo! Pipes makes it easy to remove advertising from feeds or otherwise reformat your content.&#8221;_

Note: As yet, I have not been able to access the site. Interested to learn if anybody else has and what their experiences have been.

 [1]: http://www.niallkennedy.com/blog/archives/2007/02/yahoo-pipes.html
 [2]: https://www.pipes.digital/
