---
title: BioNLP 2007
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-07-10

categories:
  - Meetings
archives:
  - 2007

---
Just posted on Nascent a [brief account][1] of a presentation I gave recently on [OTMI][2] at [BioNLP 2007][3]. The post lists some of the feedback I received. We are very interested to get further comments so do feel free to contribute comments either directly to the post, privately to <otmi@nature.com>, or publicly to <otmi-discuss@crossref.org>. And then there’s always the OTMI wiki available for comment at <http://opentextmining.org/>.

It is important to note that OTMI is not a universal panacea but rather an attempt at bridging the gap between publisher and researcher. We are attempting to provide a framework to enable scholarly publishers to disclose full text for machine processing purposes without compromising their normal publishing obligations.


 [1]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2007/07/otmi_at_bionlp_2007.html
 [2]: http://opentextmining.org/
 [3]: http://ufal.mff.cuni.cz/acl2007/workshops/program/index.php/ws05
