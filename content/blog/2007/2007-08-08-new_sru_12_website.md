---
title: New SRU (1.2) Website
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-08-08

categories:
  - Search
archives:
  - 2007

---
From Ray Denenberg’s post to the [SRU Listserv][1] yesterday:

> _&#8220;The new SRU web site is now up: <http://www.loc.gov/sru/>

> It is completely reorganized and reflects the version 1.2 specifications.

> (It also includes version 1.1 specifications, but is oriented to version

> 1.2.)

> &#8230;

> There is an official 1.1 archive under the new site,

> <https://web.archive.org/web/20080724063403/http://www.loc.gov/sru/sru1-1archive/>. And note also, that the new spec incorporates both version 1.1 and 1.2 (anything specific to version 1.1 is annotated as such).&#8221;_

Interested to learn if any Crossref publishers are currently implementing SRU.

 [1]: https://web.archive.org/web/20070813010703/http://www.loc.gov/standards/sru/community/listserv.html
