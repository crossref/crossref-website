---
title: Stop Press
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-08-28

categories:
  - Metadata
archives:
  - 2007

---
Boy, was I ever so wrong! Contrary to what I said in yesterday’s post, the new PRISM 2.0 spec **_does_** support XMP value type mappings for its terms. See the table below which lists the PRISM basic vocabulary terms and the XMP value types.

Many thanks to Dianne Kennedy and the rest of the PRISM Working Group for having added this support to PRISM 2.0.

<!--more-->

<table border="1" cellpadding="3">
  <tr>
    <th>
      Section
    </th>

    <th>
      PRISM Term
    </th>

    <th>
      XMP Value Type
    </th>
  </tr>

  <tr>
    <td>
      4.2.1
    </td>

    <td>
      prism:alternateTitle
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.2
    </td>

    <td>
      prism:byteCount
    </td>

    <td>
      <b>Integer</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.3
    </td>

    <td>
      prism:channel
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.4
    </td>

    <td>
      prism:complianceProfile
    </td>

    <td>
      <b>Choice: &#8220;one&#8221;, &#8220;two&#8221;, &#8220;three&#8221;</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.5
    </td>

    <td>
      prism:copyright
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.6
    </td>

    <td>
      prism:corporateEntity
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.7
    </td>

    <td>
      prism:coverDate
    </td>

    <td>
      <b>Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.8
    </td>

    <td>
      prism:coverDisplayDate
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.9
    </td>

    <td>
      prism:creationDate
    </td>

    <td>
      <b>Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.10
    </td>

    <td>
      prism:distributor
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.11
    </td>

    <td>
      prism:edition
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.12
    </td>

    <td>
      prism:eIssn
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.13
    </td>

    <td>
      prism:embargoDate
    </td>

    <td>
      <b>bag Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.14
    </td>

    <td>
      prism:endingPage
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.15
    </td>

    <td>
      prism:event
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.16
    </td>

    <td>
      prism:expirationDate
    </td>

    <td>
      <b>bag Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.17
    </td>

    <td>
      prism:hasAlternative
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.18
    </td>

    <td>
      prism:hasCorrection
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.19
    </td>

    <td>
      prism:hasTranslation
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.20
    </td>

    <td>
      prism:industry
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.21
    </td>

    <td>
      prism:isCorrectionOf
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.22
    </td>

    <td>
      prism:issn
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.23
    </td>

    <td>
      prism:issueIdentifier
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.24
    </td>

    <td>
      prism:issueName
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.25
    </td>

    <td>
      prism:isTranslationOf
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.26
    </td>

    <td>
      prism:killDate
    </td>

    <td>
      <b>Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.27
    </td>

    <td>
      prism:location
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.28
    </td>

    <td>
      prism:modificationDate
    </td>

    <td>
      <b>Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.29
    </td>

    <td>
      prism:number
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.30
    </td>

    <td>
      prism:object
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.31
    </td>

    <td>
      prism:origin
    </td>

    <td>
      <b>Choice: &#8220;email&#8221;, &#8220;mobile&#8221;, &#8220;broadcast&#8221;, &#8220;web&#8221;, &#8220;print&#8221;, &#8220;recordableMedia&#8221;, &#8220;other&#8221;</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.32
    </td>

    <td>
      prism:organization
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.33
    </td>

    <td>
      prism:pageRange
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.34
    </td>

    <td>
      prism:person
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.35
    </td>

    <td>
      prism:postDate
    </td>

    <td>
      <b>Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.36.
    </td>

    <td>
      prism:publicationDate
    </td>

    <td>
      <b>Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.37
    </td>

    <td>
      prism:publicationName
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.38
    </td>

    <td>
      prism:receptionDate
    </td>

    <td>
      <b>Date</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.39
    </td>

    <td>
      prism:rightsAgent
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.40
    </td>

    <td>
      prism:section
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.41
    </td>

    <td>
      prism:startingPage
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.42
    </td>

    <td>
      prism:subsection1
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.43
    </td>

    <td>
      prism:subsection2
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.44
    </td>

    <td>
      prism:subsection3
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.45
    </td>

    <td>
      prism:subsection4
    </td>

    <td>
      <b>bag Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.46
    </td>

    <td>
      prism:teaser
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.47
    </td>

    <td>
      prism:versionIdentifier
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.48
    </td>

    <td>
      prism:volume
    </td>

    <td>
      <b>Text</b>
    </td>
  </tr>

  <tr>
    <td>
      4.2.49
    </td>

    <td>
      prism:wordCount
    </td>

    <td>
      <b>Integer</b>
    </td>
  </tr>
</table>
