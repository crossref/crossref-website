---
title: Resource Maps
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-06-05

categories:
  - Publishing
archives:
  - 2007

---
<img alt="nyc1.jpg" src="/wp/blog/images/nyc1.jpg" width="272" height="204" />

Last week we had a second face-to-face of the [OAI-ORE][1] (Open Archives Initiative – Object Reuse and Exchange) Technical Committee in New York, the meeting being hosted courtesy of Google. (Hence the snap here taken from the terrace of Google’s canteen with its gorgeous view of midtown Manhattan. And the food’s not too shabby either. ;~)

The main input to the meeting was this discussion document: [Compound Information Objects: The OAI-ORE Perspective][2]. This document we feel has now reached a level of maturity that we wanted to share with a wider audience. We invite feedback either directly at [ore@openarchives.org][3] or indirectly via [yours truly][4].

The document attempts to describe the problem domain - that of describing a scholarly publication as an aggregation of resources on the Web - and to put that squarely into the Web architecture context. What the initiative is seeking to provide is machine descriptions of those resources and their relationships, something that we are inclining to call &#8220;resource maps&#8221; and as underpinning we are making use of the notion of &#8220;named graphs&#8221; from ongoing semantic web research. Essentially these resource maps are machine-readable descriptions of participating resources (in a scholarly object - both core resources and related resources) and the relationships between those resources, the whole set of assertions about those resources being named (i.e. having a URI as identifier) and having provenance information attached, e.g. publisher, date of publication, version information (still under discussion). It is envisaged that these compound object descriptions may be available in a variety of serializations from a published, object-specific URL (i.e. a good old-fashioned Web address) but some honest-to-goodness XML serialization is a likely to be one of the candidates. No surprises here, then.

Below is a schematic from the paper which shows the publication of a resource map (or named graph) corresponding to the compound object which logically represents a scholarly publication. For those objects of immediate interest to Crossref these would likely be identified with DOI’s although there is no restriction in OAI-ORE on the identifier to be used - other than it be a URI.

<img alt="named_graph.png" src="/wp/blog/images/named_graph.png" width="460" height="160" />

**Update:** For a couple posts from some other members of the ORE TC see [here][5] (Peter Murray, OhioLINK) and [here][6] (Pete Johnston, Eduserv).

 [1]: http://www.openarchives.org/ore/
 [2]: http://www.openarchives.org/ore/documents/CompoundObjects-200705.html
 [3]: mailto:ore@openarchives.org.
 [4]: mailto:t.hammond@nature.com
 [5]: https://web.archive.org/web/20160404104320/http://dltj.org/article/thoughts-on-compound-documents/
 [6]: https://web.archive.org/web/20160410040938/http://efoundations.typepad.com/efoundations/2007/06/refining_ore.html
