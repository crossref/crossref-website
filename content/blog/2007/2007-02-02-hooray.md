---
title: Hooray!
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-02-02

categories:
  - Blog
archives:
  - 2007

---
Somebody is both reading (and recommending) this blog - see Lorcan’s post [here][1]. Just my opinion but would be really good to see more librarians following this in order to arrive at better consensus.

 [1]: http://orweblog.oclc.org/archives/001257.html
