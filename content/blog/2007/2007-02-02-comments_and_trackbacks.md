---
title: comments and trackbacks
author: Ed Pentz
authors:
  - Ed Pentz
date: 2007-02-02

categories:
  - Blog
archives:
  - 2007

---
Due to spam the comments and trackbacks were turned off on the blog since last week. Comments can be moderated so they have now been turned back on. Glad to see postings picking up.
