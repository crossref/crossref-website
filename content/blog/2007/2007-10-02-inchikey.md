---
title: InChIKey
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-10-02

categories:
  - Identifiers
  - InChI
archives:
  - 2007

---
The [InChI][1] (International Chemical Identifier from IUPAC) has been blogged earlier [here][2]. RSC have especially taken this on board in their Project Prospect and now routinely syndicate InChI identifiers in their RSS feeds as blogged [here][3].

As reported variously last month (see [here][4] for one such review) IUPAC have now [released][5] a new (1.02beta) version of their software which allows hashed versions (fixed length 25-character) of the InChI, so-called InChIKey’s, to be generated which are much more search engine friendly. Compare a regular InChI identifier:

<tt><br /> InChI=1/C49H70N14O11/c1-26(2)39(61-42(67)33(12-8-18-55<br /> -49(52)53)57-41(66)32(50)23-38(51)65)45(70)58-34(20-29-1<br /> 4-16-31(64)17-15-29)43(68)62-40(27(3)4)46(71)59-35(22-30<br /> -24-54-25-56-30)47(72)63-19-9-13-37(63)44(69)60-36(48(7<br /> 3)74)21-28-10-6-5-7-11-28/h5-7,10-11,14-17,24-27,32-3<br /> 7,39-40,64H,8-9,12-13,18-23,50H2,1-4H3,(H2,51,65)(H,54,56<br /> )(H,57,66)(H,58,70)(H,59,71)(H,60,69)(H,61,67)(H,62,68)(H,73,74)<br /> (H4,52,53,55)/f/h56-62,73H,51-53H2<br /> </tt>

with its InChIKey counterpart:

<tt><br /> InChIKey=JYPVVOOBQVVUQV-UHFFFAOYAR<br /> </tt>

That’s some saving.

 [1]: http://www.iupac.org/inchi/
 [2]: /categories/inchi
 [3]: /blog/rscs-project-prospect-v1.1/
 [4]: https://web.archive.org/web/20071014205908/http://fiehnlab.ucdavis.edu/staff/kind/InChIKey
 [5]: https://web.archive.org/web/20071030202540/http://www.iupac.org/inchi/release102.html
