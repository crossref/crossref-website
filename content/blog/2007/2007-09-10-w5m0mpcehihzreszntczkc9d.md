---
title: W5M0MpCehiHzreSzNTczkc9d
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-09-10

categories:
  - XMP

---
What on earth can this string mean: &#8216;W5M0MpCehiHzreSzNTczkc9d’? This occurs in the XMP packet header:

> <span ><tt><?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d'?></tt></span>

Well from the XMP Specification (September 2005) which is available <a href="https://web.archive.org/web/20170830043306/http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/xmp/pdfs/XMPSpecificationPart3.pdf">here</a> (PDF) there is this text:

> <span ><i>&#8220;The required id attribute must follow begin. For all packets defined by this version of the syntax, the value of id is the following string: W5M0MpCehiHzreSzNTczkc9d&#8221;</i></span>
>
> <span >(See: 3 XMP Storage Model / XMP Packet Wrapper / Header / Attribute: id)</span>

<span >OK, so it’s no big deal to cut and paste that string, it’s just mighty curious why this cryptic key is needed in an open specification, especially since (contrary to what might be implied by the text) it doesn’t seem to vary with version. (Or hasn’t yet, at any rate - more below.)</span>

<span ><!--more--></span>



 <span >Right, so now we get down to it. Just what is the version number of the current XMP Specification anyways? I couldn’t for the life of me find one. (Note that I am talking about the XMP Specification itself and not the XMP Toolkit which is versioned at 4.1.1.) I am assuming that I have the latest version, else I really don’t know where else to look. This link</span>

> <span ><a href="http://www.adobe.com/products/xmp/">http://www.adobe.com/products/xmp/</a></span>

<span >leads me to</span>

> <span ><a href="http://www.adobe.com/devnet/xmp/">http://www.adobe.com/devnet/xmp/</a></span>

<span >which leads me to</span>

> <span ><a href=" https://web.archive.org/web/20210811233806/https://www.adobe.com/devnet/xmp.html">https://web.archive.org/web/20210811233806/https://www.adobe.com/devnet/xmp.html</a></span>

<span >which by the way is also the same version that ships with the SDK.</span>

 <span >I do know that there was a Version 1.5 published in September 14, 2001. (You can see that this is a fairly slow changing technology - the published spec is from 2 years back, and an earlier - the earlier? - version is from 6 years back). Note that this version has a version number (1.5) but still uses the same XMP packer header &#8216;id’ attribute.</span>

 <span >No good, by the way, peeking inside the XMP of the XMP Spec either. Here’s a dump (using the DumpMainXMP utility with the SDK):</span>

<pre><span >% xmpd xmp_spec.xmp
 
 
// -----------------------------------
// Dumping main XMP for xmp_spec.xmp :
 
File info : format = "    ", handler flags = 00000260
Packet info : offset = 0, length = 4051
 
Initial XMP from xmp_spec.xmp
Dumping XMPMeta object ""  (0x0)
 
http://ns.adobe.com/pdf/1.3/  pdf:  (0x80000000 : schema)
pdf:Producer = "Acrobat Distiller 7.0 (Windows)"
pdf:Copyright = "2005 Adobe Systems Inc."
pdf:Keywords = "XMP  metadata  schema XML RDF"
 
http://ns.adobe.com/xap/1.0/  xap:  (0x80000000 : schema)
xap:CreateDate = "2005-09-23T15:19:07Z"
xap:ModifyDate = "2005-09-23T15:19:07Z"
xap:CreatorTool = "FrameMaker 7.1"
 
http://purl.org/dc/elements/1.1/  dc:  (0x80000000 : schema)
dc:description  (0x1E00 : isLangAlt isAlt isOrdered isArray)
[1] = "XMP metadata specification"  (0x50 : hasLang hasQual)
? xml:lang = "x-default"  (0x20 : isQual)
dc:creator  (0x600 : isOrdered isArray)
[1] = "Adobe Developer Technologies"
dc:title  (0x1E00 : isLangAlt isAlt isOrdered isArray)
[1] = "Extensible Metadata Platform (XMP) Specification"  (0x50 : hasLang hasQual)
? xml:lang = "x-default"  (0x20 : isQual)
dc:format = "application/pdf"
 
http://ns.adobe.com/pdfx/1.3/  pdfx:  (0x80000000 : schema)
pdfx:Copyright = "2005 Adobe Systems Inc."
 
http://ns.adobe.com/xap/1.0/mm/  xapMM:  (0x80000000 : schema)
xapMM:InstanceID = "uuid:99b91701-a78b-4652-84e5-6bccaeb7534e"
xapMM:DocumentID = "uuid:374ea24b-3931-4b83-944d-5b9daa42277e"
</span></pre>

<span >or in more readable form (courtesy of &#8216;<a href="http://www.w3.org/2000/10/swap/doc/cwm">cwm</a>&#8216;):</span>

<pre><span >% xmp2n3q  docs/XMP-Specification.pdf
#Processed by Id: cwm.py,v 1.164 2004/10/28 17:41:59 timbl Exp
#    using base file:/Users/tony/Sources/Build/XMP-SDK/
 
#  Notation3 generation by
#       notation3.py,v 1.166 2004/10/28 17:41:59 timbl Exp
 
#   Base was: file:/Users/tony/Sources/Build/XMP-SDK/
 
@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
@prefix pdf: &lt;http://ns.adobe.com/pdf/1.3/&gt; .
@prefix pdfx: &lt;http://ns.adobe.com/pdfx/1.3/&gt; .
@prefix xmp: &lt;http://ns.adobe.com/xap/1.0/&gt; .
@prefix xmpMM: &lt;http://ns.adobe.com/xap/1.0/mm/&gt; .
 
&lt;&gt;   pdf:Copyright "2005 Adobe Systems Inc.";
pdf:Keywords "XMP  metadata  schema XML RDF";
pdf:Producer "Acrobat Distiller 7.0 (Windows)";
pdfx:Copyright "2005 Adobe Systems Inc.";
xmp:CreateDate "2005-09-23T15:19:07Z";
xmp:CreatorTool "FrameMaker 7.1";
xmp:ModifyDate "2005-09-23T15:19:07Z";
xmpMM:DocumentID "uuid:374ea24b-3931-4b83-944d-5b9daa42277e";
xmpMM:InstanceID "uuid:99b91701-a78b-4652-84e5-6bccaeb7534e";
dc:creator  [
a rdf:Seq;
rdf:_1 "Adobe Developer Technologies" ];
dc:description  [
a rdf:Alt;
rdf:_1 "XMP metadata specification"@x-default ];
dc:format "application/pdf";
dc:title  [
a rdf:Alt;
rdf:_1 "Extensible Metadata Platform (XMP) Specification"@x-default ] .
 
#ENDS
</span></pre>

<span >So, just what then is the version number of the XMP Specification which the id string &#8216;W5M0MpCehiHzreSzNTczkc9d’ is marking?</span>
