---
title: 'OTMI Applied - Means More Search Hits'
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-10-09

categories:
  - OTMI
archives:
  - 2007

---
[<img alt="otmi-twease-window-alpha.png" src="/wp/blog/images/otmi-twease.png/otmi-twease-window-alpha.png" width="214" height="217" />][1]

(Click image to enlarge.)

Following up on previous posts on [OTMI][2] (the proposal from NPG for scholarly publishers to syndicate their full text to drive text-mining applications), [Fabien Campagne][3] from Cornell, a long-time OTMI supporter, has created an [OTMI-driven search engine][4] (based on his [Twease][5] work). This may be the first publicly accessible OTMI-based service. It currently only contains NPG content from the OTMI archive online - some 2 years worth of _Nature_ and four other titles. (When will we begin to see other publishers on board?)

What’s happening here? Well, [Twease][6] is a web-based front-end to searching Medline abstracts. As such, a search will retrieve a set of results labeled by PMID and list all lines in the abstract where a match occurs. By contrast, with [Twease-OTMI][5] a search is run over the article full text and a will retrieve **_all_** text &#8220;snippets&#8221; (for Nature we use sentences, although other units of text are possible) which match. See the figure above where the top three results are all labeled by the same DOI and show text matches from various points **_within_** the document.

This shows that a far superior search match rate is possible using the article full text (as distributed in OTMI format) where text integrity as publishable asset is not compromised.

 [1]: /wp/blog/images/otmi-twease.png/otmi-twease-window-alpha.png
 [2]: http://opentextmining.org/
 [3]: https://web.archive.org/web/20071022163355/http://physiology.med.cornell.edu/faculty/campagne/
 [4]: https://web.archive.org/web/20080703194218/http://otmi.twease.org/otmi/app
 [5]: https://web.archive.org/web/20080213031456/http://www.twease.org/medline/app
 [2]: https://www.crossref.org/blog
 [3]: http://opentextmining.org/
 [4]: https://web.archive.org/web/20071022163355/http://physiology.med.cornell.edu/faculty/campagne/
 [5]: https://web.archive.org/web/20080703194218/http://otmi.twease.org/otmi/app
 [6]: https://web.archive.org/web/20080213031456/http://www.twease.org/medline/app
