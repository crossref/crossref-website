---
title: Authors in Context?
author: Tony Hammond
authors:
  - Tony Hammond
date: 2007-09-30

categories:
  - ORCID
archives:
  - 2007

---
On the subject of author IDs (a subject Crossref is interested in and on which held a meeting earlier this year, as blogged about [here][1]), this post by Karen Coyle &#8220;[Name authority control, aka name identification][2]&#8221; may be worth a read. She starts off with this:

> _&#8220;Libraries do something they call &#8220;name authority control&#8221;. For most people in IT, this would be called &#8220;assigning unique identifiers to names.&#8221; Identifying authors is considered one of the essential aspects of library cataloging, and it isn’t done in any other bibliographic environment, as far as I know.&#8221;_

and concludes thus:

> _&#8220;Perhaps the days of looking at lists of authors’ names is over. Maybe users need to see a cloud of authors connected to topic areas in which they have published, or related to books titles or institutional affiliations. In this time of author abundance, names are not meaningful without some context.&#8221;_

 [1]: /blog/crossref-author-id-meeting/
 [2]: http://kcoyle.blogspot.com/2007/09/name-authority-control-aka-name.html