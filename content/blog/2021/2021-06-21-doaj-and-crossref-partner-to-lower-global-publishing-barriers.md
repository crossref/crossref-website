---
title: 'DOAJ and Crossref sign agreement to remove barriers to scholarly publishing for all'
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
date: 2021-06-21
categories:
  - News Release
  - Collaboration
  - DOAJ
  - Community
Archives:
  - 2021
---

_22 June 2021, London, UK and Boston, MA, USA_ — The future of global open access publishing received a boost today with the signing of a Memorandum of Understanding between the Directory of Open Access Journals (DOAJ) and Crossref. The MOU formalizes an already strong partnership between the two organisations and furthers their shared pursuit of an open scholarly communications ecosystem that is inclusive of emerging publishing communities.

Both organisations aim to encourage the dissemination and use of scholarly research using open infrastructure, online technologies, regional and international networks, and community partners - all supporting local institutional capacity and sustainability around the world.

> “DOAJ is delighted to be formalizing today’s agreement with Crossref, an organization we are already closely aligned with. Together we stand a greater chance of encouraging an open, fair, and fully inclusive future for scholarly publishing,” said [Lars Bjørnshauge](https://doaj.org/about/team), DOAJ Founder and Managing Director.

The agreement will enable content from journals indexed on DOAJ to be more easily identified through the use of Crossref metadata. The MOU also covers the exchange of a variety of services and information and greater coordination of technical and strategic requirements between DOAJ and Crossref. Included too is the development of outreach and training materials, coordination of service and feature development, as well as research studies to explore the overlaps and gaps in the journals and metadata covered by each organisation.

> “As academic-led journals continue to grow in number and geographic reach, it’s important we support this community more effectively. Our partnership with DOAJ means we can share strategies, data, and resources in order to lower barriers for emerging publishers around the world,” said [Ginny Hendricks](/people), Crossref’s Director of Member & Community Outreach.

### About DOAJ
DOAJ is a community curated online directory that indexes and provides access to high quality, open access, peer reviewed journals.  DOAJ deploys more than one hundred carefully selected volunteers from among the community of library and other academic disciplines to assist in the curation of open access journals. This independent database contains over 15,000 peer-reviewed open access journals covering all areas of science, technology, medicine, social sciences, arts and humanities. DOAJ is financially supported worldwide by libraries, publishers and other like-minded organisations. DOAJ services (including the evaluation of journals) are free for all, and all data provided by DOAJ are harvestable via OAI/PMH and the API. See [doaj.org](https://doaj.org) for more information.

### About Crossref
Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organisation that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. Visit [crossref.org](https://www.crossref.org) for further information.


Please contact [louise@doaj.org](mailto:louise@doaj.org) or [feedback@crossref.org](mailto:feedback@crossref.org) with any questions.

{{< figure src="/images/blog/2021/doaj-crossref-twitter-post-new.png" width="80%" >}}
