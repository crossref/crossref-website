---
title: 'Doing more with relationships - via Event Data'
author: Martyn Rittman
draft: false
authors:
  - Martyn Rittman
  - Rachael Lammey
date: 2021-05-14
categories:
  - Metadata
  - Relationships
  - Data Citation
  - Event Data
  - Research Nexus

Archives:
  - 2021
---

Crossref aims to [link research together](https://www.crossref.org/blog/the-research-nexus-better-research-through-better-metadata/), making related items more findable, increasing transparency, and showing how ideas spread and develop. There are a number of moving parts in this effort: some related to capturing and storing linking information, others to making it available.

By including relationship metadata in Event Data, we are taking a big step to improve the visibility of a large number of links between metadata. We know this is long-promised and we’re pleased that making this valuable metadata available supports a number of important initiatives. We will also be backfilling, so all previously deposited relationships will eventually become available as events. The first step will be to add relationships between items that have DOIs, such as between a research article and a related review report or dataset.

## What are relationships?

When members register metadata with us, they have the possibility to identify other works, items, and websites that they know are related. This might be supplementary material or previous versions of a work (especially for preprints and working papers). Equally, identifiers for a protein, gene, or organism used in the research can be included. These are recorded as ‘relationships’ and can be [accessed in the same way as the rest of the metadata](https://crossref.org/services/metadata-retrieval/) we hold about registered content.


## Some examples

#### Relationships in the metadata show links to the published article from [this bioRxiv preprint](https://doi.org/10.1101/2020.05.21.109546). In the [Crossref Rest API](https://api.crossref.org/works/10.1101/2020.05.21.109546):

``` JSON
"relation": {
  "is-preprint-of": [
    {
      "id-type": "doi",
      "id": "10.1038/s41467-020-17892-0",
      "asserted-by": "subject"
    }
  ],
  "cites": []
},
```

#### And now in [Event Data](http://api.eventdata.crossref.org/v1/events?mailto=mrittman@crossref.org&subj-id=10.1101/2020.05.21.109546):

``` JSON
"subj": {
  "pid": "https://doi.org/10.1101/2020.05.21.109546",
  "url": "https://doi.org/10.1101/2020.05.21.109546",
  "work_type_id": "posted-content"
},
"obj": {
  "pid": "https://doi.org/10.1038/s41467-020-17892-0",
  "url": "https://doi.org/10.1038/s41467-020-17892-0",
  "method": "doi-literal",
  "verification": "literal",
  "work-type-id": "journal-article"
},
```

#### Linking to a dataset in the Dryad Digital Repository by [a recent eLife article](https://doi.org/10.7554/elife.19920). In the Crossref metadata:

``` JSON
"relation": {
  "is-supplemented-by": [
    {
      "id-type": "doi",
      "id": "10.5061/dryad.s58qh",
      "asserted-by": "subject"
    }
  ],
  "references": [
    {
      "id-type": "doi",
      "id": "10.5061/dryad.s58qh",
      "asserted-by": "subject"
    }
  ],
  "cites": []
},
```

#### And now in Event Data:

``` JSON
"subj": {
  "pid": "https://doi.org/10.7554/elife.19920",
  "url": "https://doi.org/10.7554/elife.19920",
  "work_type_id": "journal-article"
},
"obj": {
  "pid": "https://doi.org/10.5061/dryad.s58qh",
  "url": "https://doi.org/10.5061/dryad.s58qh",
  "method": "doi-literal",
  "verification": "literal",
  "work-type-id": "Dataset"
},
```

If you are interested in relationships for a single DOI, we still recommend checking the metadata of that record, however Event Data is a great option for looking across multiple records. For example, to check for relationships across a prefix, in a given time period, or for a specific type of relationship.

## Data citation

Data citations can be included in data deposits in relationship metadata, usually using the ‘is-supplemented-by’ relationship. By creating an event from each relationship, the links between journal articles and books, and the data they rely on are more visible. This makes the data much easier to locate.

Many datasets have DOIs which are usually recorded with [DataCite](https://datacite.org/), meaning you are unlikely to find them via searches of Crossref metadata. Making data citation relationship metadata available in Event Data means it will be available in the same format as citations from datasets to articles (which DataCite sends to Event Data) and citations from articles to datasets from Crossref reference metadata (more to come on this later this year). It also means we will convert this information into [Scholix](https://documentation.ardc.edu.au/cpg/scholix) format so that it can be harvested and combined with other sets of Scholix-compliant article/data links. Data citations will therefore be available for the community to identify, share, link and recognise research data. We’re working with initiatives like [Make Data Count](https://makedatacount.org/) and [STM’s research data program](https://www.stm-researchdata.org/) to support the growing uptake of good data citation practices. This is a big step forward in making data citation happen for the community; we have more to do, but Crossref is committed to completing this work as a strategic priority.

## What’s next?

In this first stage we are adding relationships that link two objects with a DOI, and later this year we will bring in relationships using other identifiers such as accession numbers and URIs. That will make it more straightforward to ask questions of Event Data such as which organisms have relationships to which works with a DOI.

## More info and staying in touch

- Find out more about Event Data in our [support documentation](/education/event-data/) or check out tickets in the [GitLab repo](https://gitlab.com/crossref/issues/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Service%3A%3AEvent%20Data).
- Keep informed and ask us anything via our [community forum for Event Data discussion](https://community.crossref.org/c/crossref-services/event-data/17)
