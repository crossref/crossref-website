---
title: 'The road ahead: our strategy through 2025'
author: Ginny Hendricks
draft: false
image: "/images/banner-images/strategy-road-ahead-blog-banner.png"
authors:
  - Ginny Hendricks
date: 2021-06-03
categories:
  - Strategy
Archives:
  - 2021
---
This announcement has been in the works for some time, but everything seems to take longer when there is a pandemic going on, including finding time and headspace to plan out our strategy for the next few years.

Over the last year or so we have had our heads down addressing how to scale our 20-yr-old system and operation -- and adapting to new ways of working. But we've also spent time talking to people, forging alliances, looking ahead, and making plans. So we're happy to now let everyone know exactly what we've been up to lately, what we are heading towards in 2025, and what projects and programs are prioritised on our near-term agenda.

## Tl;dr
- Introducing the [new Crossref strategy through 2025](/strategy), extending the one we published in 2018
- There are now two additional strategic goals, to make six: bolstering our team; living up to POSI
- Good progress has been made in reducing operational and technical debt - a lot of learning too
- We're unblocking stuff to get more done, including expanding R&D (more on that next week)
- We have [a new public roadmap](http://bit.ly/crossref-roadmap) 🎉
- Come to next week's mid-year update webinar to hear what's happening and up next.


## The emergence of a strategic agenda

2018 seems like a decade ago, doesn't it? Back then we set out a [2018-2021 strategic direction---now archived](/strategy/archive-2018)---that described four goals: adapt to expanding constituencies; simplify and enrich services; selectively collaborate and partner with others; and improve our metadata quality and comprehensiveness. These themes were formed from the output of a [planning exercise](/blog/scenario-planning-for-our-future) with our board in mid-2017 which tackled scenarios that remain true today, including: the increasing diversity in scholarly publishing (library-publishing, academic-led journals, shifting geographic dominance, etc.); the growth in preprints and other content formats; the sustainability of scholarly publishing (who is funding it and whether that is an expanding or shrinking pool); and the increase in policy and regulation in this space.

That meeting was the catalyst for embracing openness and a broader set of constituents. It was also decisive about Crossref’s role in this evolving community to focus on our core competencies, defined as:

> 1. A reputation as a trusted, neutral one-stop source of metadata and services
> 2. Managing scholarly infrastructure with technical knowledge and innovation
> 3. Convening and facilitating scholarly community collaboration.

So you can see how we got to focusing on metadata, services, infrastructure, and broad community collaboration.

## Ahh, 2019, such an innocent time

When we wrote our post at the end of 2019 [A turning point is a time for reflection](/blog/a-turning-point-is-a-time-for-reflection/) we highlighted---with data---how different the Crossref community is nowadays. The post also linked to [the results of our 'value' research project](https://docs.google.com/presentation/d/1RsqtnHssBkaFNphdWoq20_ewruYP04n8j_dYB9wvphM/edit#slide=id.g65af51c04a_1_238) and a [fact file](https://doi.org/10.13003/y8ygwm5) which had even more hard data and posed the question **Which Crossref initiatives should be top or bottom priorities?**. To answer that, the LIVE19 annual meeting group voted (using betting chips) on priority initiatives, with the following results:

1. Support and implement ROR &nbsp; {{< icon name="fa fa-trophy" color="crossref-yellow" size="medium" >}}
2. Metadata best practices and principles
3. Support for multiple languages
4. Address technical and operational debt
5. Schema updates such as JATS and CRediT
6. Engagement with funders

We all know what happened next: the collective health and social trauma of the COVID-19 pandemic. All of us struggled. You all did too. Homeschooling, homeworking, homestaying. Caring for---and even saying goodbye to---sick friends and family. Also [beloved colleagues](/blog/a-tribute-to-our-kirsty/). Alongside these unfamiliar new stresses, members were joining in growing numbers, funders kept joining to register grants, conferences went online and we loved them (before then hating them), the number of records we hosted kept going up, and publishing (especially preprints) skyrocketed.

The plan hasn't actually changed much. Those charts in the 2019 fact file still make for remarkable reading as those same trends continue. We simply haven't had time to update people on where we are with plans. So it's high time we give an update on these priorities as well as contextualise them in longer-term goals.

## But first, some framing

The chart below shows the approach we took to organise our thinking. A lot of it isn't new; we have had the current [mission statement, key messages](/commuinity/about/) (rally, tag, run, play, make), and [truths](/truths/) since the rebranding work in 2015/2016. More recently, we have added POSI to our values, describing the principles and rules by which we operate as a committed open scholarly infrastructure organization.

{{< figure src="/images/blog/2021/crossref-strategic-framework.png" width="100%" >}}

<br>
We already have a lot of 'words'. So why do we also need a vision statement and where do the goals fit in? In order to prioritize the things we will work on first, we need to be able to track everything to a higher vision, ensuring that everything we do is working toward an agreed destination. When we have organization-wide goals, it means that everyone is clear on the direction, is able to prioritize individual and team work, and can see how their contribution fits in. This, in turn, instills confidence, and motivation - amongst staff as well as members and users.

Our working vision statement (feedback needed!) is:

> We envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society.

A vision is, of course, shared. It isn't Crossref-specific but describes the world in which we all want to work together in future.

## Now for those contextual six goals

Full details are on the new [strategy](/strategy) page but here's a summary below.

{{% row %}}
{{% column %}}
<a href="/strategy/#bolster-the-team">  <img src="/images/community-images/bolster.svg" alt="Bolster the team" width="100%" align="left" /> </a>
<br>
This goal is all about people, support, culture, and resilience. Not just because we're coming through a panedmic, but also because we're growing and we need to be able to scale and manage growth more purposefully, with appropriate policies, fees, and resources.

<a href="/strategy/#live-up-to-posi">  <img src="/images/community-images/live.svg" alt="Live up to POSI" width ="100%" align="left"/> </a>
<br>
We published a [POSI self-assessment](/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure/) earlier this year and like-minded initiatives are [following suit](http://openscholarlyinfrastructure.org/posse/). This is a stated goal because we want to be held publicly accountable to the Principles of Scholarly Infrastructure standards of governance, insurance, and sustainability.

<a href="/strategy/#engage-with-expanding-communities">  <img src="/images/community-images/engage.svg" alt="Engage with expanding communities" width="100%" align="left"/> </a>
<br>
This goal centres on growth, strengthening relationships, community facilitation, and content. Working with a growing number of Sponsors helps us lower barriers to participation around the world, including in languages other than English. Expanding the support we offer for research funders and institutions are priorities.

{{% /column %}}
{{% column %}}

<a href="/strategy/#improve-our-metadata">  <img src="/images/community-images/improve.svg" alt="Improve our metadata" width ="100%" align="left" /> </a>
<br>
This goal involves researching and communicating the value of richer, connected, and reusable, open metadata, and incentivising people to meet best practices, while also making it possible (and easier) to do so.

<a href="/strategy/#collaborate-and-partner">  <img src="/images/community-images/collab.svg" alt="Collaborate & partner" width ="100%" align="left"/> </a>  
<br>
We've always collaborated but we want to work even more closely with like-minded organisations to solve problems together. Perhaps in future we could also partner with others to find operating efficiencies for our overlapping stakeholders.

<a href="/strategy/#simplify-and-enrich-services"> <img src="/images/community-images/simplify.svg" alt="Simplify & enrich services" width ="100%" align="left"/>  </a>  
<br>
This goal is all about focus. And about delivering easy-to-use tools that are critically important for our community. A lot of invisible work has been happening behind the scenes; we've been strengthening (and will continue to strengthen) our code-base (while opening up all code) in order to unblock some of the initiatives we know people have been waiting for.

{{% /column %}}
{{% /row %}}

Read more about what projects are included in the above goals in our full [2025 strategic agenda](/strategy).


## You're invited to a mid-year update webinar

Rather than saving everything for our annual---usually November---meeting, we'll also do a mid-year update and plan to do so in May or June every year from now on, in addition to the November updates which include the board election and governance and budget information.

This year, we're covering some of the main product development work we have completed, underway, and planned for the next quarter. We'll run it live twice - once for those nearby The Americas timezones ([June 8th 3pm UTC](https://bit.ly/theroadahead-June8)) and once for those nearby Asia Pacific timezones ([June 9th 6am UTC](https://bit.ly/theroadahead-June9)). We have a lot to cover in 90 minutes---including unveiling [our public roadmap[(http://bit.ly/crossref-roadmap)]---but we're going to try really hard to have a few minutes to discuss questions too.

> In the meantime, or indeed anytime, join the discussion over on our community forum - see the discussion below and join in on our [forum](https://community.crossref.org/t/the-road-ahead-our-strategy-through-2025-crossref).

We want to be held accountable to these goals so we’re reliant on you, as a community, to let us know what you think of our [2025 strategic agenda](/strategy). As always; we’re grateful for your support and advice.
