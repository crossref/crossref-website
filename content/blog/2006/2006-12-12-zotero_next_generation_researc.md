---
title: 'Zotero - next generation research tool?'
author: Ed Pentz
authors:
  - Ed Pentz
date: 2006-12-12

categories:
  - News Release
archives:
  - 2006

---
[1] was mentioned at the STM Innovations talk in London and it’s worth taking a look. It’s billed as the next generation of bibliographic management software - End Note but a lot more included. DOIs should be incorporated into this tool - I couldn’t find any mention of Crossref or DOIs.

 [1]: http://www.zotero.org/
