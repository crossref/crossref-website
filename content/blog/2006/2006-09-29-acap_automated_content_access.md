---
title: 'ACAP - (Automated Content Access Protocol)'
slug: "acap-automated-content-access"
author: Ed Pentz
authors:
  - Ed Pentz
date: 2006-09-29

categories:
  - News Release
archives:
  - 2006

---
The World Association of Newspapers is developing ACAP - see the [press release][1] which will be machine readable rights information that search engines would read and act on in an automated way. Rightscom is working on the project and the IPA and EPC (European Publishers Council) are involved.

Publishers presenting a united front to search engines is a good thing but I’m somewhat skeptical about how such a system would work without being overly complicated. However, the idea of getting more information to the search engines when they are crawling sites is a good idea but what will the publishers say to the search engines? If you get much above crawl/don’t crawl then you need a bilateral agreement that has to be negotiated.

 [1]: https://web.archive.org/web/20070612185117/http://www.wan-press.org/article11943.html
