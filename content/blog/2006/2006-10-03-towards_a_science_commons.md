---
title: Science Commons
slug: towards-a-science-commons

author: Tony Hammond
authors:
  - Tony Hammond
date: 2006-10-03

categories:
  - Meetings
archives:
  - 2006

---
Peter Murray-Rust posts on the SPARC-OpenData mailing list about a Commons for Science Conference (Oct. 3/4 in DC). The meeting is invitation-only but the papers are online (see [here][1]) and there should be public reports. The meeting underlines the importance of Open Data. There’s a brief abstract below.

<!--more-->



_&#8220;The sciences depend on access to and use of factual data. Powered by

developments in electronic storage and computational capability,

scientific inquiry today is becoming more data-intensive in almost

every discipline. Whether the field is meteorology, genomics,

medicine, ecology, or high-energy physics, modern research depends on

the availability of multiple databases, drawn from multiple public

and private sources; and the ability of those diverse databases to be

searched, recombined, and processed.&#8221;_

 [1]: https://web.archive.org/web/20061121055622/http://www.spatial.maine.edu/icfs
