---
title: Ruby Makes A-List
slug: ruby-makes-alist

author: Tony Hammond
authors:
  - Tony Hammond
date: 2006-10-12

categories:
  - Programming
archives:
  - 2006

---
Um, well. Seems according to [O’Reilly Ruby][1] that Ruby is now a mainstream language.

_&#8220;The [Ruby programming language][2] just made the A-list on the [TIOBE Programming Community Index][3], and Ruby is now listed as a mainstream programming language. For the past three or four years Ruby has consistently placed in the high 20’s in this index, but is now placed as the 13th most popular programming language!&#8221;_

(No language wars, but I am, I will confess, a big admirer - for some time.)

 [1]: http://www.oreillynet.com/ruby/blog/2006/10/ruby_declared_mainstream.html
 [2]: http://www.ruby-lang.org/
 [3]: http://www.tiobe.com
