---
title: Exhibit A
date: 2006-12-12

categories:
  - News Release
archives:
  - 2006

---
MIT’s Simile project has just released [Exhibit][1], a &#8221; lightweight structured data publishing framework.&#8221; Read that as &#8220;an easy-to-use mashup creation tool.&#8221; I have heard that [Leigh][2] has already started experimenting with it. I look forward to a writeup soon&#8230;

 [1]: http://simile.mit.edu/exhibit/
 [2]: http://www.ldodds.com/blog/