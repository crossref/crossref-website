---
title: PRISM Press Release
author: Tony Hammond
authors:
  - Tony Hammond
date: 2008-07-09

categories:
  - Metadata
archives:
  - 2008

---
The [PRISM][1] metadata standards group issued a [press release][2] yesterday which covered three points:

PRISM Cookbook</p>
:   The Cookbook provides _&#8220;a set of practical implementation steps for a chosen set of use cases and provides insights into more sophisticated PRISM capabilities. While PRISM has 3 profiles, the cookbook only addresses the most commonly used profile #1, the well-formed XML profile. All recipes begin with a basic description of the business purpose it fulfills, followed by ingredients (typically a set of PRISM metadata fields or elements), and, closes with a step-by-step implementation method with sample XMLs and illustrative images.&#8221;_</p>

    PRISM 2.0 Errata </p>

    :   The Errata _&#8220;addresses a range of issues, from editorial to technical, that have been reported by the PRISM user community.&#8221;_</p>

        PRISM 2.1</p>

        :   The next version of the PRISM Specification, PRISM 2.1, is slated for release in late 2008. _&#8220;This release will address complex rights for multi-platform and global distribution channels.&#8221;_</dl>

 [1]: https://web.archive.org/web/20081019002715/http://www.prismstandard.org//
 [2]: https://web.archive.org/web/20160326011637/http://prismstandard.org/news/2008/PRISM_%20PR070808.pdf
