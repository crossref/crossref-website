---
title: Multiple Resolution
author: Tony Hammond
authors:
  - Tony Hammond
date: 2008-08-22

categories:
  - Multiple Resolution
archives:
  - 2008

---
I’ve been meaning for some time to write something about DOI and so-called &#8220;Multiple Resolution&#8221;, which to be honest is the only technology feature of any real interest as concerns DOI. (DOI as a business and social compact for guaranteeing name persistence of Web resources has been an extraordinarily successful venture in the academic publishing world with more than 32m items registered and maintained over eight years of operation but that may not have required any specialized technology. More a consensus to adopt a single location service in the [DOI proxy][1].)

Multiple resolution, though. Now, that’s something else. Seems like it should be able to offer a lot of general funkiness and yet it has not been much used up to now. And I have to wonder why.

(Continues below.)

<!--more-->



I guess we should start out with some definitions: the DOI Handbook, the (draft) ISO standard, and Crossref:

  * [DOI Handbook][2] - From [Sect. 3.3 Multiple resolution][3]:

    > _&#8220;Multiple resolution allows one entity to be resolved to multiple other entities; it can be used to embody e.g a parent-children relationship, or any other relationship. &#8230; A DOI name can be resolved to an arbitrary number of different points on the Internet: multiple URLs, other DOI names, and other data types.&#8221;_

      * [ISO CD 26324][4] - I’ve blogged [here][5] before about the ISO standardization of DOI which is now [available][4] as a Committee Draft. Multiple resolution is specifically mentioned in Sects. 3.2 and 6.2 and discussed in Sect. 6.1. From Sect. 3 &#8220;Terms and definitions&#8221; we have this definition:

        > _&#8220;Multiple resolution is the simultaneous return as output of several pieces of current information related to the object, in defined data structures.&#8221;_

        And then Section 6 &#8220;Resolution of DOI name&#8221; goes on to say this:

        > _&#8220;DOI resolution records may include one or more URLs, where the object may be located, and other

        > information provided about the entity to which a DOI name has been assigned, optionally including but not restricted to: names, identifiers, descriptions, types, classifications, locations, times, measurements, and relationships to other entities.&#8221;_

          * [Crossref][6] - In the help page [Multiple Resolution Intro][7] there is this:

            > _&#8220;As of May 2008 the Crossref main system will support assigning more than one URL to a single DOI, a concept known as multiple resolution (MR). &#8220;_

            The intro goes on to talk about the two pilot forms of multiple resolution service that have been trialled: a) interim page, and b) menu pop-up. The pop-up service is no longer supported. Only the interim page is currently offered as a production service. The help page [Interim Page multiple resolution overview][8] leads off thus:

            > _&#8220;Crossref’s MR service provides an interim page solution which presents a list of link choices to the end user. Each choice represents a location at which the item may be obtained and are commonly services that are co-hosting the content under agreement with the content’s Copyright holder.&#8221;_</ul>

            So, there it is. That’s DOI multiple resolution. The real important thing to note is that the official DOI position (IDF, ISO) is invitingly open while both the Crossref implementation and the description of multiple resolution itself is unduly restrictive. Multiple resolution as described by Crossref is essentially the deposition of additional URLs (pointing to copies of the same resource) for alternate routing (for geographical reasons, co-hosting arrangements, etc.) with a service presentation of alternate locations for user selection.

            Multiple resolution proper (as per the DOI Handbook and ISO draft) is the deposition of arbitrary data values and return of same with _no particular services implied_. Use cases for multiple resolution include the addition of URLs for referencing different (but related) network objects, e.g. a metadata record, or other resources such as supplementary information, datasets, etc. Deposit of arbitrary data types is not yet catered for by Crossref. There could, I would suggest, at least be some rudimentary provision for depositing vanilla type/value pairs (subject to policy constraints). (There is currently some work under way in defining handle data types but this need not be any showstopper to depositing new data types as any type management system will likely need to evolve over time.)

            An obvious use case for multiple resolution (to me, anyway) would be the registration of a second URL which would point not onto a copy of the resource but onto a public metadata record. (I have earlier posted here about [architectures][9] and [options][10] for exposing public data.)

            With more than one data value in a resolution record, the process of resolving such a record is potentially complicated. As the ISO CD says in Sect. 62f:

            > _&#8220;Resolution requests should be capable of returning all associated values of current information, individual values, or all values of one data type.&#8221;_

            The DOI Handbook itself recognizes the problems that multiple resolution may present.

            > _&#8220;If the DOI name can point to many different possible &#8220;resolutions&#8221;, how is the choice made between different options? At its simplest, the user may be provided with a list from which to make a manual choice. However, this is not a scalable solution for an increasingly complex and automated environment. The DOI name will increasingly depend on automation of &#8220;service requests&#8221;, through which users (and, more importantly, users’ application software) can be passed seamlessly from a DOI name to the specific service that they require.&#8221;_

            Indeed, services like [OpenHandle][11] will make it much easier to programmatically access data stored in the handle record associated with a DOI name. (I have blogged previously about the OpenHandle [project][12] and its [languages support][13].) Note that presentation of data values to a human user may be a non-issue for mediated services.

            And talking of computer languages it may be amusing to ruminate briefly on their own built-in support for multiple return values. Perhaps unsurprisingly, of the dominant languages Java has no such support as this [recent post][14] addresses:

            > _&#8220;Today was one of those days when I wished Java would support multiple return values&#8230; but Java allows you to return only one value either an object or a primitive type.&#8221;_

            By contrast, languages such as Common Lisp do have support for multiple return values. See [this post][15] for some gory details and insights. Interesting also to reflect that as in the world of computing languages where there is a decided tilt towards a mainstream family of languages based on (or derived from) C, there may be dominant protocols at large on the Internet but no single &#8220;winner takes all&#8221;.

 [1]: http://dx.doi.org/
 [2]: https://web.archive.org/web/20090301014307/http://www.doi.org/hb.html
 [3]: https://web.archive.org/web/20080704170549/http://www.doi.org/handbook_2000/resolution.html
 [4]: https://www.iso.org/standard/81599.html
 [5]: https://www.crossref.org/blog/iso/cd-26324-doi/
 [6]: http://www.crossref.org/
 [7]: /education/content-registration/creating-and-managing-dois/multiple-resolution/
 [8]: /education/content-registration/creating-and-managing-dois/multiple-resolution/
 [9]: /blog/exposing-public-data
 [10]: /blog/exposing-public-data-options/
 [11]: http://code.google.com/p/openhandle/
 [12]: /blog/openhandle-google-code-project/
 [13]: /blog/openhandle-languages-support/
 [14]: http://www.osnews.com/story/20076/Multiple_Return_Values_in_Java
 [15]: http://lambda-the-ultimate.org/node/2833
