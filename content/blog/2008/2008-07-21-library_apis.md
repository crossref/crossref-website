---
title: Library APIs
author: Tony Hammond
authors:
  - Tony Hammond
date: 2008-07-21

categories:
  - Linking
archives:
  - 2008

---
Roy Tennant in a [post][1] to XML4Lib announces a new list of library APIs hosted at

> <https://web.archive.org/web/20080730080413/http://techessence.info/apis//>

A useful rough guide for us publishers to consider as we begin cultivating the multiple access routes into our own content platforms and tending to the &#8220;alphabet soup&#8221; that taken together comprises our public interfaces.

 [1]: https://web.archive.org/web/20081201160108/http://lists.webjunction.org/wjlists/xml4lib/2008-July/006059.html
