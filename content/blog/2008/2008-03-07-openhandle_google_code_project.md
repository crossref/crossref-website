---
title: 'OpenHandle: Google Code Project'
author: Tony Hammond
authors:
  - Tony Hammond
date: 2008-03-07

categories:
  - Handle
archives:
  - 2008

---
Just announced on the [handle-info][1] and [semantic-web][2] mailing lists is the [OpenHandle][3] project on Google Code. This may be of some interest to the DOI community as it allows the handle record underpinning the DOI to be exposed in various common text-based serializations to make the data stored within the records more accessible to Web applications. Initial serializations include RDF/XML, RDF/N3, and JSON.

We’d be very interested in receiving feedback on this project - either on this blog or over on the [project wiki][4].

 [1]: http://www.handle.net/mail-archive/handle-info/msg00254.html
 [2]: http://lists.w3.org/Archives/Public/semantic-web/2008Mar/0054.html
 [3]: http://code.google.com/p/openhandle/
 [4]: https://code.google.com/archive/p/openhandle/
