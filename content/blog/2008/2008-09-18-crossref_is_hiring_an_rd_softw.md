---
title: 'Crossref is hiring an R&D software engineer'
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2008-09-18

categories:
  - News Release
archives:
  - 2008

---
Crossref is hiring an R&D software engineer to work in our Oxford office. This is a fantastic opportunity to work on wide range of projects that promise to revolutionize scholarly publishing.