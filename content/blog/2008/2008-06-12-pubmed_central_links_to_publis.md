---
title: PubMed Central Links to Publisher Full Text
author: Ed Pentz
authors:
  - Ed Pentz
date: 2008-06-12

categories:
  - Member Briefing
archives:
  - 2008

---
A Crossref Member Briefing is available that explains how [PubMed Central (PMC)][1] links to publisher full text, how PMC uses DOIs and how PMC _should_ be using DOIs. The briefing is entitled [&#8220;Linking to Publisher Full Text from PubMed Central&#8221; (PDF 85k)][2].

Crossref considers it very important the PMC uses DOIs as the main means to link to the publisher version of record for an article and we are recommending that publishers try to convince PMC to use DOIs in an automated way. Almost all of the PMC articles contain DOIs but they aren’t linked. This seems like a waste considering that publishers have invested a lot in Crossref and DOIs as unique identifiers and persistent links.

This issue will be of interest to anyone who publishers journal articles that are the result of NIH funding and fall under the [NIH Public Access Policy][3].

 [1]: http://www.pubmedcentral.nih.gov/
 [2]: http://www.crossref.org/pdfs/pmc-briefing-june2008.pdf
 [3]: http://publicaccess.nih.gov/
