---
title: CLADDIER Final Report
author: admin
authors:
  - admin
date: 2008-01-15

categories:
  - Citation Formats
  - Linking
archives:
  - 2008

---
I just ran across the final report from the [CLADDIER project.][1] CLADDIER comes from the [JISC][2] and stands for &#8220;CITATION, LOCATION, And DEPOSITION IN DISCIPLINE &#038; INSTITUTIONAL REPOSITORIES&#8221;. I suspect JISC has an entire department dedicated to creating impossible acronyms (the JISC Acronym Preparation Executive?)

Anyhoo- the report describes a distributed citation location and updating service based on the [linkback][3] mechanism that is widely used in the blogging community.

I think this is an interesting approach and is one that I talked about [briefly][4] (PDF) at the [UKSG’s Measure for Measure seminar][5] last June. I think that, like most proponents of p2p distributed architectures, they massively underestimate the problem of trust in the network. They fully knowledge the problem of linkback spam, but their hand-wavy-solution(tm) of using whitelists just means the system effectively becomes semi-centralized again (you have to have trusted keepers of the whitelists).

And of course I was mildly exasperated by the report’s characterization of one of the perceived &#8220;disadvantages&#8221; of the Crossref architectural model being a :

> &#8220;Centralised service hosting a large persistent store – with the need for a (possibly commercial) business model to justify providing the service.&#8221;

Though DOI registries like [Bowker][6] and [Nielsen Bookdata][7] are commercial, Crossref, the organization that services the industry that the JISC is concerned with, is \*not\* a commercial service.

Also if you replaced the phrase &#8220;justify providing&#8221; with the word &#8220;sustain&#8221;, the sentence wouldn’t sound like such a &#8220;disadvantage.&#8221;

But aside from these quibbles, the report makes an interesting (if technical) read.

 [1]: http://www.ukoln.ac.uk/repositories/digirep/index/CLADDIER
 [2]: http://www.jisc.ac.uk/
 [3]: http://en.wikipedia.org/wiki/Linkback
 [4]: http://www.uksg.org/sites/uksg.org/files/PresentationBilder.pdf
 [5]: https://web.archive.org/web/20080512153431/http://www.uksg.org/events/measure
 [6]: http://www.bowker.com/
 [7]: http://www.doi.nielsenbookdata.co.uk
