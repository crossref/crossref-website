---
title: 'Open Abstracts: Where are we?'
author: Ludo Waltman
draft: false
authors:
  - Ludo Waltman
  - Bianca Kramer
  - Ginny Hendricks
  - Bryan Vickery
date: 2020-09-25
categories:
  - Metadata
  - Content Registration
  - Collaboration
  - Community

Archives:
  - 2020
---
The [Initiative for Open Abstracts (I4OA)](https://i4oa.org) launched this week. The initiative calls on scholarly publishers to make the abstracts of their publications openly available. More specifically, publishers that work with Crossref to register DOIs for their publications are requested to include abstracts in the metadata they deposit in Crossref. These abstracts will then be made openly available by Crossref. 39 publishers have already agreed to join I4OA and to open their abstracts.  

<!--more-->
Where are we at the moment in terms of openness of abstracts? For an individual publisher working with Crossref, the percentage of the publisher’s content for which an abstract is available in Crossref can be found in Crossref’s [Participation Reports](https://www.crossref.org/members/prep). The chart presented below gives the overall picture (as of September 1, 2020) for medium-sized and large publishers working with Crossref. The vertical axis shows the number of journal articles of a publisher in the period 2018-2020. Because of the large differences between publishers in the number of articles they publish, this axis has a logarithmic scale. The horizontal axis shows the percentage of the articles of a publisher for which an abstract is available in Crossref. The orange dots represent publishers that have agreed to join I4OA. The publishers colored in blue have not yet agreed to join the initiative.

{{% imagewrap center %}}{{< figure src="/images/blog/i4oa-chart.png" alt="Publishers with abstracts in Crossref" width="100%" >}}{{% /imagewrap %}}

A similar chart was published a few months ago in [this blog post on the importance of open abstracts](https://medium.com/@aarontay/why-openly-available-abstracts-are-important-overview-of-the-current-state-of-affairs-bb7bde1ed751). Comparing the above chart with the one published a few months ago, the first effects of I4OA are already visible. While for most publishers the percentage of abstracts available in Crossref has hardly changed, it has increased from 11% to 95% for the Royal Society, one of the founding publishers of I4OA. This reflects the efforts the Royal Society has made over the past months to improve the availability of abstracts in Crossref for its content, not only for new content but also for existing content. For SAGE, another founding publisher of I4OA, the percentage of abstracts available in Crossref has increased from 38% to 50%. A further increase can be expected to take place in the coming months. The third founding publisher of I4OA, Hindawi, has remained at a stable level, with abstracts being available for 97% of its content.

The above chart shows that many publishers supporting I4OA are already making abstracts available in Crossref. Other publishers do not yet make abstracts available in Crossref but have nevertheless decided to join I4OA. This is the case for Frontiers, PLOS, and Karger, and also for several smaller publishers not visible in the above chart, such as EMBO and Ubiquity Press. These publishers are currently adjusting their workflows and will start submitting abstracts to Crossref soon.

Of the publishers that have not yet joined I4OA, some may not yet be aware of I4OA, while others may need more time to decide whether they will join the initiative. As can be seen in the above chart, most publishers that have not yet joined I4OA do not make abstracts available in Crossref at the moment. However, some publishers have not yet joined I4OA even though they do make abstracts available in Crossref. We hope these publishers will join I4OA soon. By joining the initiative, these publishers would formalize their commitment to openness of abstracts.

None of the publishers in the above chart makes abstracts available in Crossref for 100% of its journal content. Some publishers, such as Copernicus and Hindawi, are close to 100%, but even these publishers have some content for which no abstract is available. Importantly, this does not necessarily mean that publishers have failed to submit abstracts to Crossref for some of their content. Instead, it may simply mean that some of their journal content does not have an abstract. Research articles usually have an abstract, but many other types of content published in journals, such as book reviews, letters, editorials, and corrections, often do not have an abstract. For most publishers, it is therefore impossible to make abstracts available for 100% of their content. Moreover, since Crossref does not distinguish between different types of content published in journals, we cannot provide separate statistics on the availability of abstracts for different types of journal content.

As an example, let’s consider Brill, a publisher that has joined I4OA and that mainly focuses on the humanities and social sciences. Abstracts are available in Crossref for 57% of Brill’s content in the period 2018-2020. This may suggest that Brill has failed to submit abstracts to Crossref for a significant share of its content. However, when we look up journal publications of Brill in 2018 and 2019 in the Web of Science database, abstracts turn out to be available for only 68% of these publications. Assuming that Web of Science has more or less complete coverage of abstracts, this seems to indicate that Brill has already submitted most of its abstracts to Crossref. In fact, Web of Science shows that about a quarter of the publications of Brill are book reviews and that hardly any of these book reviews has an abstract. This illustrates why some publishers, for instance those that publish many book reviews, cannot be expected to get close to 100% availability of abstracts.

Despite the above caveats, it is clear that there is still a long way to go in improving the availability of abstracts in Crossref. As of September 1, 2020, abstracts were available for 21% of all journal articles in Crossref in the period 2018-2020. In Web of Science (Science Citation Index Expanded, Social Sciences Citation Index, and Arts & Humanities Citation Index), 86% of all journal publications in 2018 and 2019 that have a DOI also have an abstract.

Publishers who wish to distribute their abstracts openly through Crossref can include them in the normal content registration process. They can send XML to Crossref (using Crossref’s metadata deposit schema), either directly via HTTPS POST or via the Crossref admin system. For back-content, a resubmission of the full XML is required. In addition, various tools can be used to deposit abstracts. Open Journal Systems (OJS) has a plugin that supports the depositing of abstracts. Metadata Manager also facilitates this, but only for journal articles. Crossref’s web deposit form does not yet support abstracts, but Crossref is working on this.

To keep track of the progress publishers are making in depositing abstracts in Crossref, we plan to publish regular updates of the chart presented above on the I4OA website. We look forward to witnessing the impact of I4OA in the coming months!

---
_Thank you to guest authors Bianca Kramer and Ludo Waltman, as well as the other founding members of I4OA._
