---
title: 'A tribute to our Kirsty'
author: Crossref
draft: false
image: "/images/banner-images/red-kite-for-kirsty-banner.png"
authors:
  - Crossref
date: 2020-12-16
categories:
  - Staff

Archives:
  - 2020
---

Our colleague and friend, [Kirsty Meddings](/people/kirsty-meddings), passed away peacefully on 10th December at home with her family, after a sudden and aggressive cancer. She was a huge part of Crossref, our culture, and our lives for the last twelve years.

Kirsty Meddings is a name that almost everyone in scholarly publishing knows; she was part of a generation of Oxford women in publishing technology who have progressed through the industry, adapted to its changes, spotted new opportunities, and supported each other throughout. We hope this post will do justice to her memory in our profession.

## Kirsty's early career

After completing her degree in English and Spanish American Literature at Warwick University, Kirsty started her career in scholarly communications and publishing at Blackwell’s Information Services. She was there for a year before joining CatchWord, an online journal start-up, in 1998, as Electronic Publisher and Account Manager and in 1999 was promoted to the new role of Library Relations Manager.

CatchWord was acquired by Ingenta and Kirsty moved into product management working on integrating the CatchWord and Ingenta platforms and launching IngentaConnect in 2004. Ingenta became Publishing Technology in 2005 and Kirsty was Product Development Manager working with engineering, business development, and users on developing online products and services. She was also involved in a range of community initiatives including COUNTER, KBART, and ICEDIS.   

## Joining Crossref

{{< figure src="/images/staff/kirsty_720px.jpg" caption="Kirsty's professional headshot" link="/people/kirsty-meddings/" width="50%" >}}

She was an early pioneer in electronic and online publishing - an innovator who understood scholarly publishing, technology, libraries, and people - a powerful combination. And Crossref was quick to offer her a role.

In Kirsty’s introduction to Crossref she was described by the recruiter as:

> An experienced and highly capable individual with a solid background in product development, marketing and customer service issues related to the supply of scholarly electronic content from publishers to library and end user audiences. A good communicator and team worker with sound technical understanding and an excellent grasp of publishing industry issues.

This adequately captures Kirsty’s impressive professional achievements, but not her personality. Kirsty was a Product Manager at Crossref for 12 years and was a valued and loved friend and colleague. Committed to Crossref---its values and people---she was funny, human, and always asked tough questions.

She joined us on October 27th, 2008 as our first Product Manager and the third UK employee. In her time at Crossref, Kirsty made a major impact, working on a range of important projects and services - particularly new, innovative services. Not long after she started at Crossref, she wrote a “day in the life” profile for the journal Serials that perfectly captures what it was like in 2009 at Crossref Oxford (there were three of us in Oxford and only ten total staff at Crossref): _Meddings, K., 2009. Mini-profile: a day in the life of a product manager. Serials, 22(1), pp.5–6. DOI: [http://doi.org/10.1629/225](http://doi.org/10.1629/225)_

Her own biography, on her staff [page](/people/kirsty-meddings/), states:

> Kirsty Meddings has been involved in a diverse set of initiatives that have kept her busy since 2008. She has spent most of her career in scholarly communications, in a variety of marketing and product development roles for intermediaries and technology suppliers. She speaks conversational geek and competent publishing, and is working towards fluency in both.

See? Funny!

## Professional achievements

Kirsty started out working on CrossCheck, now Similarity Check, the plagiarism screening service that launched in 2008. The service was in need of some attention and better organization - Kirsty got stuck in, whipped it into shape and it has gone on to be one of Crossref’s most widely-adopted services. [This article](https://cdn.ymaws.com/www.ismte.org/resource/resmgr/eon/august_2011.pdf) that Kirsty wrote for ISMTE’s publication, EON, remains useful nearly 10 years after it was written! Kirsty successfully managed the partnership with Turnitin (starting as iParadigms), the technical provider for Similarity Check, for many years. Colleagues there are mourning her loss too.

Kirsty was instrumental in launching Crossmark, which became a production service in 2012. After a few changes of hands, she resumed work on the service in recent years, and announced [the removal of Crossmark fees](/blog/encouraging-even-greater-reporting-of-corrections-and-retractions/) to better support uptake in 2020.

[The addition of clinical trial information to the Crossref metadata](/news/2016-05-17-crossref-publishers-deliver-win-for-clinical-trial-openness/) was a community-driven initiative, developed from the concept of [threaded publications](https://blogs.biomedcentral.com/on-medicine/2014/01/31/threaded-publications-one-step-closer/). There were/are lots of moving parts in this initiative, and in many ways it was one of the precursors to the idea of the Research Nexus: linking via metadata and relationships to provide a clearer picture of the ecosystem that exists around a research object.

What was once FundRef (ahh, those logos!) has matured into the Open Funder Registry under Kirsty’s stewardship. In collaboration with Elsevier, the registry has grown from [an initial 4,000 funders](https://www.crossref.org/news/2013-05-28-crossrefs-fundref-launches-publishers-and-funders-track-scholarly-output/), to over 25,000 and we can see [over 5 million works](http://api.crossref.org/works?filter=has-funder:t&rows=0) registered with Crossref that are linked to at least one funder. More recently, Kirsty was the Product Manager for the registration of research grants with Crossref, working with our Funder Advisory Group, and she was starting to work with CDL and DataCite to absorb the Funder Registry into ROR.

In 2018, Kirsty launched our first ever dashboard for member best practice. She led the development and design of Participation Reports and the decision of which checks would be most important for the scholarly community to assess. This has quickly become one of Crossref’s most valuable and used tools.

## Public speaking

Kirsty always spoke with authority across a range of topics, appearing totally calm even if she was nervous. Among many talks, she spoke at the STM seminar on [Publication Ethics and Research Integrity](https://www.stm-assoc.org/events/publication-ethics-and-research-integrity/), ISMTE, UKSG, ALPSP seminars, the COPE Forum, ran numerous CrossCheck, CrossMark, FundRef and TDM webinars, and a recent online LIVE event.

She was a frequent presenter at many of Crossref annual meetings, and enjoyed the opportunity to meet and catch up with our members, the board, and the community (many of whom always ask after her). Checking in after conferences on who said what, who’s moving where, what feedback we had, and picking up on opportunities for further collaboration were all things that we looked forward to sharing.

To use UKSG’s own words, Kirsty was always a staunch supporter of the organisation - attending, exhibiting, and speaking at many UKSG conferences and events over her whole career. She was also a legend at the dinners, on the dance floor, and in the bar. At the 2019 conference she tallied the votes at the quiz night - Kirsty loved a quiz! We had an all-staff end-of-year quiz via zoom last week and it was just not the same without her.

Here are [Kirsty's slides on SlideShare](https://www2.slideshare.net/search/slideshow?searchfrom=header&q=Kirsty+meddings), some [videos of Kirsty's talks on YouTube](https://www.google.com/search?q=kirsty+meddings+Crossref&source=lnms&tbm=vid&sa=X&ved=2ahUKEwjf78Soq9DtAhVTolwKHXzhBOoQ_AUoA3oECAYQBQ&biw=1440&bih=707), and her [ORCID record](https://orcid.org/0000-0001-9205-2956) which lists her published works.

## Strong friendships

One of the most rewarding experiences of working at Crossref is meeting up with the whole team and with our members. Jetlag, hunting out coeliac-friendly food, staying up far too late chatting, trying to fit in exploring bits of cities around board and other meetings, presenting, organizing, thinking, laughing (I’m sure to the annoyance of other plane passengers)---these experiences were all part and parcel of working with Kirsty, and where many of us cemented connections with her.

We started a [message board](https://www.kudoboard.com/boards/DY47xRTo) and within days it was populated with numerous stories, poems, and photos from so many friends and colleagues on whom Kirsty made such a lasting and loving impression.

{{< figure src="/images/blog/kirsty-messages.jpg" caption="Kirsty's message board" link="https://www.kudoboard.com/boards/DY47xRTo" width="25%" >}}

It’s impossible to capture someone’s character in a blog, but some of the words that carry across the messages that people have shared are empathy, compassion, honesty, intelligence, brilliance, sincerity, laughter, human, passion, openness, and fun. We’ll miss her immensely.

Kirsty was somewhat of an expert in grief. She lost her first husband, James Culling, to leukemia in December 2012, leaving her a widow with two sons, Dan, 7 at the time, and Luke, just 6-months old. A few years later, through the charity Widowed And Young (WAY), she met Martin Eggleston. Martin and his daughter Amy joined Kirsty, Dan, and Luke, and they created a very happy blended family. Some of us went to their wedding and it was an incredible event full of love and laughter - and of course music. Always music.

Kirsty represented us, along with Rachael, at the funeral of another colleague last year, [Christine Hone](/blog/rest-in-peace-christine-hone/), in Amsterdam. Kirsty helped all of us get through the grief then. And because she made it okay to grieve and to talk about grief, it is heartbreaking and also comforting that she is indirectly helping us all now to be better able to handle her own death.

### How we can honour Kirsty’s memory

We heard that Kirsty’s last words were “I’m listening”. Which is just so fitting. She was always ready with an ear, a shoulder to support us all, and indeed she demanded that we express ourselves honestly.

If you want to share memories of Kirsty, you can join others who have done so [on the message board](https://www.kudoboard.com/boards/DY47xRTo) or just take a few minutes to read through.

And there is a [justgiving page](https://www.justgiving.com/fundraising/kirsty-meddings) in memory of Kirsty for Maggie's Oxford, a branch of a cancer support charity who helped her and her family through James's death and is now helping her family again.

Professionally, Kirsty made major contributions at Crossref and in scholarly communications in general. More importantly, she had a profound impact on a personal level with many people. Our thoughts are with Martin, Dan, Amy, and Luke, and also with Kirsty’s mum Val, her brother Colin, her in-laws, her close friends, and all the people who---like the rest of us---are better for knowing her, and will never forget her.
