---
title: 'Calling all prospective board members'
author: Lucy Ofiesh
draft: false
authors:
  - Lucy Ofiesh
date: 2020-05-21
categories:
  - Member Briefing
  - Community
  - Board
  - Governance
  - Election

archives:
    - 2020
---

_**English version** ––  [Información en español](#spanishversion) –– [Version Française](#frenchversion)_

The Crossref Nominating Committee is inviting expressions of interest to join the Board of Directors of Crossref for the term starting in 2021. The committee will gather responses from those interested and create the slate of candidates that our membership will vote on in an election in September. Expressions of interest will be due Friday, June 19, 2020.   

The role of the board at Crossref is to provide strategic and financial oversight of the organization, as well as guidance to the Executive Director and the staff leadership team, with the key responsibilities being:

-   Setting the strategic direction for the organization;

-   Providing financial oversight; and

-   Approving new policies and services.

The board is representative of our membership base and guides the staff leadership team on trends affecting scholarly communications. The board sets strategic directions for the organization while also providing oversight into policy changes and implementation. Board members have a fiduciary responsibility to ensure sound operations. Board members do this by attending board meetings, as well as joining more specific board committees.

As an example, in 2019 the board decided to remove fees for the Crossmark service. This involved a strategic review of the service and its alignment with the mission by the Membership & Fees committee; followed by a review of the financial implications of removing the fee; and ultimately, a vote by the full board to remove the fee starting in 2020.

Crossref's services provide central infrastructure to scholarly communications. Crossref's board helps shape the future of our services, and by extension, impacts the broader scholarly ecosystem. We are looking for board members to contribute their experience and perspective. 

## I'm interested but busy! What is expected of board members?

Board members attend three meetings each year that typically take place in March, July, and November. Meetings have taken place in a variety of international locations and travel support is provided when needed.

Starting in 2020, following travel restrictions as a result of COVID-19, the board introduced a plan to convene at least one of the board meetings virtually each year and all committee meetings take place virtually. Most board members sit on at least one Crossref committee. Care is taken to accommodate the wide range of timezones in which our board members live.

While the expressions of interest are specific to an individual, the seat that is elected to the board belongs to the member organization. The primary board member also names an alternate who may attend meetings in the event that the primary board member is unable.

Board members are expected to be comfortable assuming the responsibilities listed above and to prepare and participate in board meeting discussions.

## About the election

The board is elected through the "one member, one vote" policy wherein every member of Crossref has a single vote to elect representatives to the Crossref board. Board terms are for three years, and this year there are six seats open for election.

The board maintains a balance of seats, with eight seats for smaller publishers and eight seats for larger publishers, in an effort to ensure that the diversity of experiences and perspectives of the publishing community is represented in decisions made at Crossref. This year we will elect two of the larger publisher seats and four of the smaller publisher seats.

The election takes place online and voting will open in September. Election results will be shared at the November board meeting and new members will commence their term in 2021.

## About the nominating committee

The nominating committee will review the expressions of interest and select a slate of candidates for election. The slate put forward will exceed the total number of open seats. The committee considers the statements of interest, organizational size, geography, gender, and experience.

**2020 Nominating Committee:**  
Melissa Harrison, eLife, Cambridge, UK, committee chair  
Scott Delman, ACM, New York, NY  
Susan Murray, AJOL, Grahamstown, South Africa  
Tanja Niemann, Erudit, Montreal, Canada  
Arley Soto, Biteca, Bogotá, Colombia  


{{% divwrap blue-highlight %}}

#### How to submit an expression of interest

Please [click here to submit your expression of interest](https://docs.google.com/forms/d/e/1FAIpQLSeJhKR34FmXVHELDXZjNYy0W4TnEpuYJMHfKAPPYjRIuDuoQg/viewform?usp=sf_link) or contact me with any questions at [lofiesh [at] crossref.org](mailto:lofiesh@crossref.org).  

{{% /divwrap %}}  

<br />

<a id="spanishversion"></a>
_**Versión en español**_

El Comité de Nominación de Crossref está invitando a expresiones de interés a unirse a la Junta Directiva de Crossref para el período que comienza en 2021. El comité recopilará las respuestas de los interesados ​​y creará la lista de candidatos que nuestra membresía votará en una elección en septiembre. Las expresiones de interés vencen el viernes 19 de junio de 2020.  

La función de la junta directiva de Crossref es proporcionar supervisión estratégica y financiera de la organización, así como orientación para el Director Ejecutivo y el equipo de liderazgo del personal, con responsabilidades importantes como:  

- Establecer la dirección estratégica para la organización;  

- Proporcionar supervisión financiera; y  

- Aprobar nuevas políticas y servicios.  

La junta es representativa de nuestra base de miembros y guía al equipo de liderazgo del personal sobre las tendencias que afectan las comunicaciones académicas. La junta establece direcciones estratégicas para la organización mientras supervisa los cambios e implementación de políticas. Los miembros de la junta tienen la responsabilidad fiduciaria de garantizar operaciones sólidas. Los miembros de la junta hacen esto asistiendo a las reuniones de la junta, además de unirse a comités de la junta más específicos.  

Como ejemplo, en 2019 la junta decidió eliminar las tarifas de servicio de Crossmark. Esto implicó una revisión estratégica del servicio y su alineación con la misión del comité de Membresía y Tarifas; seguido de una revisión de las implicaciones financieras de eliminar la tarifa; y, en última instancia, un voto de la junta completa para retirar la tarifa a partir de 2020.  

Los servicios Crossref proporcionan infraestructura central para las comunicaciones académicas. La junta directiva de Crossref ayuda a dar forma al futuro de nuestros servicios y, por extensión, impacta el ecosistema académico más amplio. Estamos buscando miembros de la junta para contribuir con su experiencia y perspectiva.  

## ¡Estoy interesado pero ocupado! ¿Qué se espera de los miembros de la junta?  

Los miembros de la junta asisten a tres reuniones cada año que generalmente tienen lugar en marzo, julio y noviembre. Las reuniones se han llevado en una variedad de ubicaciones internacionales y se brinda apoyo para viajes cuando es necesario.  

A partir de 2020, después de las restricciones de viaje como resultado de COVID-19, la junta introdujo un plan para convocar al menos una de las reuniones de la junta virtualmente todos los años, y todas las reuniones del comité tienen lugar virtualmente. La mayoría de los miembros de la junta formen parte del menos un comité Crossref. Se tiene cuidado de acomodar la amplia gama de zonas horarias en las que viven los miembros de nuestra junta.  

Aunque las expresiones de interés son específicas de un individuo, el asiento elegido para la junta pertenece a la organización miembro. El miembro primario de la junta también nombra a un suplente que puede asistir a las reuniones en caso de que el miembro de la junta principal no pueda.  

Se espera que los miembros de la junta se sientan cómodos asumiendo las responsabilidades anteriores y que se preparen y participen en las discusiones de la reunión de la junta.  

Las reuniones de la junta se llevarán a cabo en inglés, por lo que los posibles miembros de la junta deben sentirse cómodos leyendo material en inglés y en inglés conversacional.  

## Sobre las elecciones  

La junta se elige mediante la política de "un miembro, un voto" en la que cada miembro de Crossref tiene un voto para elegir representantes en la junta de Crossref. Los términos de la junta son de tres años, y este año hay seis asientos abiertos para la elección  

La junta mantiene un equilibrio de asientos, con ocho asientos para editoriales más pequeñas y ocho asientos para editoriales más grandes, en un esfuerzo por garantizar que la diversidad de experiencias y perspectivas de la comunidad editorial esté representada en las decisiones tomadas en Crossref. Este año elegiremos dos de los asientos de editor más grandes y cuatro de los asientos de editor más pequeños.  

La elección se realiza en línea y la votación se abrirá en septiembre. Los resultados de las elecciones se compartirán en la reunión de la junta de noviembre y los nuevos miembros comenzarán su mandato en 2021.  

## Sobre el comité de nominaciones  

El comité de nominaciones revisará las expresiones de interés y seleccionará una lista de candidatos para la elección. Esta lista presentada excederá el número total de asientos disponibles. El comité considera declaraciones de interés, tamaño organizacional, geografía, género y experiencia.  

**Comité de nominaciones 2020:**  
Melissa Harrison, eLife, Cambridge, UK, committee chair  
Scott Delman, ACM, New York, NY  
Susan Murray, AJOL, Grahamstown, South Africa  
Tanja Niemann, Erudit, Montreal, Canada  
Arley Soto, Biteca, Bogotá, Colombia  

{{% divwrap blue-highlight %}}

#### Cómo presentar una expresión de interés

Por favor [haga clic aquí para enviar su expresión de interés](https://docs.google.com/forms/d/e/1FAIpQLSeJhKR34FmXVHELDXZjNYy0W4TnEpuYJMHfKAPPYjRIuDuoQg/viewform?usp=sf_link) o contáctame si tiene alguna pregunta [lofiesh [at] crossref.org](mailto:lofiesh@crossref.org).  

{{% /divwrap %}}  

<br />

<a id="frenchversion"></a>
_**Version Française**_  

## Appel à tous les membres potentiels du conseil d'administration

Le comité de nomination de Crossref invite les personnes qui seraient intéressées à se porter candidates pour l'élection au conseil d'administration de Crossref, pour le mandat commençant en 2021. Le comité de nomination rassemblera les réponses des personnes candidates et élaborera une liste des candidats, pour lesquels nos membres pourront voter lors des élections au conseil d'administration, en septembre. Les candidatures doivent être déposées au plus tard le vendredi 19 juin 2020.  

Le rôle du conseil d'administration de Crossref est d'opérer une supervision stratégique et financière de l'organisation, et de conseiller le directeur exécutif ainsi que l'équipe de direction du personnel. Les principales responsabilités du conseil d’administration sont les suivantes :  

* Fixer l'orientation stratégique de l'organisation
* Assurer la surveillance financière
* Approuver de nouvelles politiques et de nouveaux services

Le conseil d'administration est représentatif de nos adhérents et guide l'équipe de direction du personnel en ce qui concerne les tendances affectant les communications savantes. Le conseil d'administration établit des orientations stratégiques pour l'organisation, tout en assurant le contrôle des changements et de la mise en œuvre des politiques. Les membres du conseil ont la responsabilité fiduciaire d'assurer son bon fonctionnement. Les membres du conseil d'administration s’acquittent de cette responsabilité en assistant aux réunions du conseil d'administration et en participant à des comités, plus spécifiques, du conseil d'administration.  

A titre d’exemple, en 2019, le conseil d'administration a décidé de supprimer les frais liés au service Crossmark. Ceci a impliqué un examen stratégique du service et de son alignement avec la mission de Crossref, par le comité des adhésions et frais, puis un examen des implications financières de la suppression des frais, et, finalement, un vote par l'ensemble du conseil d'administration pour supprimer les frais à partir de 2020.  

Les services de Crossref fournissent une infrastructure centralisée pour les communications savantes. Le conseil d'administration de Crossref aide à façonner l'avenir de nos services et, par extension, a un impact sur l'écosystème universitaire plus large. Les futurs membres du conseil d'administration sont recherchés particulièrement pour leur expérience et leur point de vue.  

## Je suis intéressé mais très occupé! Qu'attend-on des administrateurs?

Les membres du conseil d'administration assistent à trois réunions par an qui ont généralement lieu en mars, juillet et novembre. Les réunions se déroulent dans des lieux divers, à l'échelle internationale, et une assistance financière est octroyée, en cas de besoin, pour le voyage.   

À partir de 2020, à la suite des restrictions de voyage causées par la COVID-19, le conseil a présenté un plan pour convoquer au moins une des réunions du conseil en téléconférence chaque année, et toutes les réunions des comités auront lieu en téléconférence. La plupart des membres du conseil d'administration siègent à au moins un comité de Crossref. Nous souhaitons préciser que nous prenons soin de prendre en compte le large éventail de fuseaux horaires dans lesquels vivent les membres de notre conseil d'administration.  

Bien que les manifestations d'intérêt émanent d’une personne, le siège pourvu au conseil appartient à l'organisation membre dans son ensemble. Le membre titulaire du conseil d'administration nomme également un suppléant, qui pourra assister aux réunions en cas d'empêchement du membre titulaire du siège au conseil d'administration.  

Il est attendu que les membres du conseil d’administration puissent dédier aux responsabilités présentées ci-dessus le temps qui leur est raisonnablement dû, ainsi qu'à la préparation et à la participation aux discussions des réunions du conseil.  

## À propos de l'élection

Le conseil d'administration est élu selon une politique de «un membre, une voix» dans laquelle chaque membre de Crossref dispose d'une seule voix pour élire les représentants au conseil d'administration de Crossref. Le mandat du conseil d'administration est de trois ans et, cette année, six sièges sont à pourvoir lors de des élections de septembre prochain.  

Le conseil d'administration maintient un équilibre des sièges, avec huit sièges pour les petits éditeurs et huit sièges pour les grands éditeurs, afin de garantir que la diversité des expériences et des perspectives de la communauté de l'édition soit représentées dans les décisions prises à Crossref. Cette année, sont à pourvoir deux sièges de grands éditeurs et quatre sièges de petits éditeurs.  

Le vote aura lieu en ligne et s'ouvrira en septembre. Les résultats de ce scrutin seront communiqués lors de la réunion du conseil d'administration de novembre et les nouveaux membres commenceront leur mandat en 2021.  

## À propos du comité de nomination

Le comité des candidatures examinera les candidatures et sélectionnera une liste de candidats aux élections. Le nombre de candidats proposés dépassera le nombre total de sièges à pourvoir. Le comité prend en compte les déclarations d'intérêt, la taille de l'organisation, la géographie, le sexe et l'expérience des personnes pour sa sélection.  


{{% divwrap blue-highlight %}}

#### Comment exprimer une manifestation d'intérêt  
Veuillez cliquer ici pour envoyer votre candidature ou contactez-moi pour toute question à lofiesh [at] crossref.org.

{{% /divwrap %}}  
