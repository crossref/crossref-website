---
title: 'Events got the better of us'
author: Bryan Vickery
draft: false
authors:
  - Bryan Vickery
date: 2020-03-27
categories:
  - Identifiers
  - Metadata
  - Citation
  - Collaboration
  - Data
  - Event Data
archives:
    - 2020
---
Publisher metadata is one side of the story surrounding research outputs, but conversations, connections and activities that build further around scholarly research, takes place all over the web. We built [Event Data](/services/event-data/) to capture, record and make available these 'Events' –– providing open, transparent, and traceable information about the provenance and context of every Event. Events are comments, links, shares, bookmarks, references, etc.

<!--more-->

In September 2018 we said [Event Data](/blog/event-data-is-production-ready/) was 'production ready.' What we meant was development of the service had reached a point where we expected no further major changes to the code, and we encouraged you to use it. What normally would have followed was a detailed handover to our operations team, for monitoring and performance management, and for Product Management to expand Event Data by adding new Crossref member domains and evaluating additional event sources.

## Why so quiet?

But many things changed on the [staff front](/blog/rest-in-peace-christine-hone/), meaning 2019 was a year of reinvention for the Technical and Product teams and of critical knowledge sharing and learning –– Event Data had to take a back seat as we focused resources on other key projects (more on that later). From a technical perspective, we've found the Elasticsearch index is not performing well and the approach taken to specifically support data citations through [Scholix](https://documentation.ardc.edu.au/cpg/scholix) has not really scaled.

When things go wrong, whether in ways you can or can't anticipate, the most important thing is communication –– in dealing with the challenges we forgot to do that. We understand how frustrating that can be and we're extremely sorry to have gone so quiet.

## So, where are we today?

Event Data is important to us and clearly important to you too as you've contacted us about your use-cases and the reliability of the service. Event Data remains [available](https://www.eventdata.crossref.org/guide/) and you're welcome to use it, but you should expect instability to continue and be aware that it does not find events for [DOIs/domains of our newer members](https://www.eventdata.crossref.org/guide/data/ids-and-urls/#dois-for-objects) (who joined Crossref since 2019) –– so we're conscious it might be hard to say whether it's a good fit for your project at this point.

## What are we doing?

We have brought in additional expert Elasticsearch resources to assist with a separate project to migrate our REST API from SOLR to Elasticsearch. We're making fantastic progress on this. As soon as we're confident we can make this switch, we will move those same Elasticsearch resources to shoring up Event Data. The REST API takes priority over Event Data because we need to add support for important new record types (like research grants) that aren't yet available via the API.

We're also concluding the process of hiring two new Product Managers which means we'll be in a position to assign someone to head up the product management of Event Data. When we do return to Event Data in the coming months, our initial priority will be increased support for data citation and Scholix. If that means radical changes to the rest of the service, we'll let you know. 

## Opening up the discussion

We will have more news on Event Data in mid-2020. We'd love you to join the [Crossref Community Forum](https://community.crossref.org/c/event-data/17); we've created a new Category for Event Data where you can post details of how you are using, or plan to use Event Data; post questions to the group; suggestions for future development and provide general feedback on the Event Data service.
