---
title: 'EASE Council Post: Rachael Lammey on the Research Nexus'
author: Rachael Lammey
draft: false
authors:
  - Rachael Lammey
date: 2020-10-12
categories:
  - Citation
  - Metadata
  - Persistence
  - Research Nexus

archives:
    - 2020
---

This blog was initially posted on the [European Association of Science Editors (EASE)](https://ease.org.uk/) blog: ["EASE Council Post: Rachael Lammey on the Research Nexus"](https://ese-bookshelf.blogspot.com/2020/10/ease-council-post-rachael-lammey-on.html). EASE President Duncan Nicholas accurately introduces it as a whole lot of information and insights about metadata and communication standards into one post...

I was given a wide brief to decide on the topic of my EASE blog, so I thought I'd write one that tries to encompass *everything* - I'll explain what I mean by that.

In the past, Crossref has had the opportunity to talk to EASE members about the importance of registering content whose metadata contains important information related to the article. Richer metadata helps to connect the content to other key information such as who wrote it, who it was funded by, the relevant license, the research it cites, any updates to the work such as corrections and retractions, and [the data that underpin the research](https://doi.org/10.20316/ESE.2019.45.19010). The use of open persistent identifiers like DOIs, funder IDs, ORCID iDs and ROR IDs are always recommended.

Such rich and connected metadata also helps discoverability of the published research in a different way than just direct access; if you can find something based on looking at the publications related to a particular funder, author, or institution, then there are more ways to come across what you're looking for. Making links between objects underpinning the research also helps put the research in context and can help further research by making connections to other valuable information that may have been more difficult to make otherwise.

I've mentioned the Research Nexus in the title of this post. It's achieved by declaring relationships between publications and other associated research objects, and from those objects to related publications. The metadata that reveals relationships between research objects can be as informative as the objects themselves. These relationships can assert certain facts that may not be otherwise obvious: this is our goal with the Research Nexus. These relationships and assertions need to exist not just on the web pages of the outputs, but also reflected in a standard way in the metadata so that the information is computer-readable and can be used at scale. As Jennifer Lin, who coined the term, explains:

> "Researchers are adopting new tools that create consistency and shareability in their experimental methods. Increasingly, these are viewed as key components in driving reproducibility and replicability. They provide transparency in reporting key methodological and analytical information. They are also used for sharing the artefacts which make up a processing trail for the results: data, material, analytical code, and related software on which the conclusions of the paper rely. Where expert feedback was also shared, such reviews further enrich this record."

In [her Crossref blog](/blog/the-research-nexus-better-research-through-better-metadata), Jennifer goes on to give some examples, including:  

* Linking to an [entire collection of methods](https://doi.org/10.17504/protocols.io.r89d9z6) and [video protocols](https://doi.org/10.17504/protocols.io.itrcem6) via Protocols.io
* Linking to [software and peer reviews](https://doi.org/10.21105/joss.00384) in JOSS  
* Linking to [preprint, data, code, source code, peer reviews in Gigascience](https://doi.org/10.1093/gigascience/gix045)

I'd include an additional example of linking research to the grant using the grant identifier and associated metadata from the funding section of [this PLOS paper](https://doi.org/10.1371/journal.pone.0222922) (read more about the example from EuroPMC who [register grants with Crossref for Wellcome)](https://blog.europepmc.org/2020/06/global-grant-ids-in-europe-pmc.html).

These links can be established by adding them into the Crossref [relationship metadata](/education/content-registration/structural-metadata/relationships/) schema. The information is then made available to anyone via our open APIs, so that they can easily see and use the information.

In all of these, publishers and other parties are linking to associated research outputs to support the reproducibility and discoverability of content.

The reproducibility point is worth reiterating; EASE has always supported projects to maintain high standards around the review of research, publication standards and ethics, and the reduction of research waste. And connecting articles to data, preprints, protocols, and peer reviews, and making the relationships open for analysis will help achieve this.

{{% imagewrap center %}}{{< figure src="/images/blog/2020/DOI-network-diagram_v3_600x560px-1024x956.png" alt="Visualizing the Reseasrch Nexus image" width="50%" >}}{{% /imagewrap %}}

We also know that there are work and cost involved in establishing these links, and we're working on ways to lower the barriers in doing so by:

* Revisiting what we charge to encourage best practice. Starting in 2020, we have [removed fees](/blog/encouraging-even-greater-reporting-of-corrections-and-retractions/) for registering vital information on corrections, retractions and other Crossmark metadata. This is timely in light of the updates to the [EASE Standardised Retraction form.](https://ease.org.uk/publications/ease-statements-resources/ease-standard-retraction-form/)
* We're also working to remove fees for translations and versions that are linked together by the appropriate relationship metadata so that publishers posting translations or different versions of an article don't have to pay multiple times for these. Our [Membership & Fees Committee](/committees/membership-and-fees/) is currently reviewing other ways we can support publishers keen to make these connections.
* Finding ways to make it easier for publishers to collect this information from authors e.g. submission systems integrations with data repositories to collect robust information on article/data links.
* Allowing the registration of peer review metadata for content other than journal articles e.g. books, preprints (coming soon).
* Making it easier for publishers to register this information with us at Crossref via the provision of simple to use tools, interfaces and reporting.

The outputs of the research process, such as journal articles, don't exist in isolation - you only have to look at the interest in the corpus of COVID-19 publications, preprints and associated data to see this. This thinking is also supported by campaigns like [Metadata 2020](http://www.metadata2020.org/) advocating for "richer, connected, and reusable, open metadata will advance scholarly pursuits for the benefit of society." The relationships revealed by the Research Nexus may one day help progress research to realise benefits that help us all, providing we all make efforts to effectively support them. More to come...
