---
title: 'Similarity Check news: introducing the next generation iThenticate.'
author: Kirsty Meddings
draft: false
authors:
  - Kirsty Meddings
date: 2020-07-28
categories:
  - Member Briefing
  - Similarity Check
aliases: "/blog/similarity-check-news-introducing-the-next-generation-ithenticate/"
aliases: "/blog/similarity-check-news-introducing-the-next-generation-ithenticate"

Archives:
  - 2020
---


Crossref’s Similarity Check service is used by our members to detect text overlap with previously published work that may indicate plagiarism of scholarly or professional works. Manuscripts can be checked against millions of publications from other participating Crossref members and general web content using the iThenticate text comparison software from Turnitin.
<!--more-->
The 2000 members who already make use of Similarity Check upload almost 2,000,000 documents each month to look for matching text in other publications.

We have some great news for those 2000 members –– a completely new version of iThenticate is on its way, and will start to roll out to users in the coming months.

New functionality has been developed based on your feedback over the past few years and includes:

- An improved Document Viewer that makes PDFs searchable and accessible, with responsive design for ease of use on different screen sizes.  All of the functionality of the Viewer and the Text-only reports in the previous version  have been streamlined into just two views: Sources Overview and All Sources.
- Improved exclusion options to make refining matches even easier. Smarter citation detection now identifies probable citations both inline and in reference sections.
- A new “Content Portal” where you can see what percentage of your own content has been successfully indexed for the iThenticate comparison database, and download reports of indexing errors that need to be fixed.
- A new API for integration with manuscript submission systems allows display of the largest matching word count and the top 5 source matches alongside the Similarity Score.
- The maximum number of pages and file size per document has been doubled to 800 pages/200 MB.

{{< figure src="/images/blog/new-ithenticate-screen.png" title="The new document viewer in iThenticate v2.0" width="80%" >}}

{{< figure src="/images/blog/exclude-bibliography-ithenticate.png" title="Improved reference exclusion" width="80%" >}}

Crossref members can use Similarity Check directly by logging in, or via an integration with a submission/peer review system. We are working with many system providers to bring v2.0 to you as soon as possible. In the meantime, we are looking for members to help us test the new system directly in the iThenticate user interface. If you are interested and can spare a few hours some time in the next month [please let me know](https://docs.google.com/forms/d/e/1FAIpQLScaqCunNVfyTe7bk9RwNbtf48KPTetVnCtvd-l194wokQ5NCQ/viewform?usp=sf_link).

And if your organization is not yet using Similarity Check to assess the originality of the manuscripts you receive do take a look at the [many benefits](/services/similarity-check/) the service has to offer.
