---
title: Easily add publications to your ORCID profile
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2013-01-24

categories:
  - ORCID
  - Crossref Labs
  - Metadata-Search
  - ORCID
archives:
  - 2013

---
<span >You can now easily search for publications and add them to your <a href="http://www.orcid.org" target="_blank">ORCID</a> profile in the new beta of <a href="https://web.archive.org/web/20131229210637/http://search.crossref.org/" target="_blank">Crossref Metadata Search</a> (CRMDS). The user interface is pretty self-explanatory, but if you want to read about it before trying it, here is a summary of how it works.</span>

<span >When you go to to CRMDS, you will see that there is now a small ORCID sign-in button on the top right-hand side of the screen.</span><figure id="attachment_244"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" target="_blank" rel="attachment wp-att-244"><img class="size-medium wp-image-244 "  src="/wp/blog/uploads/2013/01/crmds_home-300x253.png" alt="crmds_home" width="300" height="253" srcset="/wp/blog/uploads/2013/01/crmds_home-300x253.png 300w, /wp/blog/uploads/2013/01/crmds_home-624x527.png 624w, /wp/blog/uploads/2013/01/crmds_home.png 859w" sizes="(max-width: 300px) 85vw, 300px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<span >Clicking on this button allows you to connect CRMDS to your ORCID profile and authorises CRMDS to add publications to your profile. First, if you are not already logged into ORCID, CRMDS will ask ORCID to log you in:</span><figure id="attachment_245"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" rel="attachment wp-att-245"><img class="size-medium wp-image-245 "  src="/wp/blog/uploads/2013/01/orcid_login_prompt-300x230.png" alt="orcid_login_prompt" width="300" height="230" srcset="/wp/blog/uploads/2013/01/orcid_login_prompt-300x230.png 300w, /wp/blog/uploads/2013/01/orcid_login_prompt-624x479.png 624w, /wp/blog/uploads/2013/01/orcid_login_prompt.png 915w" sizes="(max-width: 300px) 85vw, 300px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<span >Once you have logged in, ORCID will ask you if you want to allow CRMDS to be able to view and update your ORCID profile:</span><figure id="attachment_248"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" target="_blank" rel="attachment wp-att-248"><img class="size-medium wp-image-248 "  src="/wp/blog/uploads/2013/01/orcid_authorize-300x230.png" alt="orcid_authorize" width="300" height="230" srcset="/wp/blog/uploads/2013/01/orcid_authorize-300x230.png 300w, /wp/blog/uploads/2013/01/orcid_authorize-624x480.png 624w, /wp/blog/uploads/2013/01/orcid_authorize.png 925w" sizes="(max-width: 300px) 85vw, 300px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<span >After you authorise CRMDS to access your profile, you will be returned to the CRMDS screen and the top right corner of the CRMDS page will indicate that you have connected to your ORCID profile (note, you can always de-authorise CRMDS from accessing your ORCID profile in your ORCID settings):</span><figure id="attachment_249"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" target="_blank" rel="attachment wp-att-249"><img class="size-medium wp-image-249 "  src="/wp/blog/uploads/2013/01/orcid_logged_in-300x231.png" alt="orcid_logged_in" width="300" height="231" srcset="/wp/blog/uploads/2013/01/orcid_logged_in-300x231.png 300w, /wp/blog/uploads/2013/01/orcid_logged_in-624x481.png 624w, /wp/blog/uploads/2013/01/orcid_logged_in.png 915w" sizes="(max-width: 300px) 85vw, 300px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<span >Once you are logged in, you can enter search terms that are likely to return records of your publications:</span><figure id="attachment_250"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" target="_blank" rel="attachment wp-att-250"><img class="size-medium wp-image-250 "  src="/wp/blog/uploads/2013/01/crmds_search_terms-300x231.png" alt="crmds_search_terms" width="300" height="231" srcset="/wp/blog/uploads/2013/01/crmds_search_terms-300x231.png 300w, /wp/blog/uploads/2013/01/crmds_search_terms-624x481.png 624w, /wp/blog/uploads/2013/01/crmds_search_terms.png 915w" sizes="(max-width: 300px) 85vw, 300px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<span >Each search result will show an icon telling you whether that particular item is visible in your ORCID profile. If the item is not in your ORCID profile, you see an icon like this:</span>

<p >
  <a href="/blog/easily-add-publications-to-your-orcid-profile/" rel="attachment wp-att-251"><img class="size-full wp-image-251 aligncenter"  src="/wp/blog/uploads/2013/01/add_to_orcid_button.png" alt="add_to_orcid_button" width="113" height="30" /></a>
</p>

<span >And if the item is already in your ORCID profile, you will see an icon like this:</span>

<p >
  <a href="/blog/easily-add-publications-to-your-orcid-profile/" rel="attachment wp-att-252"><img class="size-full wp-image-252 aligncenter"  src="/wp/blog/uploads/2013/01/in_your_profile.png" alt="in_your_profile" width="133" height="27" /></a><span >In the following search results you can see that 1 item is already in Josiah Carberry’s profile, and 2 items are not:</span>
</p><figure id="attachment_254"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" target="_blank" rel="attachment wp-att-254"><img class=" wp-image-254 "  src="/wp/blog/uploads/2013/01/crmds_search_results.png" alt="crmds_search_results" width="329" height="254" srcset="/wp/blog/uploads/2013/01/crmds_search_results.png 915w, /wp/blog/uploads/2013/01/crmds_search_results-300x231.png 300w, /wp/blog/uploads/2013/01/crmds_search_results-624x481.png 624w" sizes="(max-width: 329px) 85vw, 329px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<span >Clicking on the &#8220;Add to Profile&#8221; button will confirm that you want to add the specified publication to your ORCID profile:</span><figure id="attachment_255"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" rel="attachment wp-att-255"><img class=" wp-image-255 "  src="/wp/blog/uploads/2013/01/crmds_add_work.png" alt="crmds_add_work" width="329" height="254" srcset="/wp/blog/uploads/2013/01/crmds_add_work.png 915w, /wp/blog/uploads/2013/01/crmds_add_work-300x231.png 300w, /wp/blog/uploads/2013/01/crmds_add_work-624x481.png 624w" sizes="(max-width: 329px) 85vw, 329px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<p >
  <span >After clicking on &#8220;Yes&#8221; to add the publication to your profile, the search results will refresh to reflect that the item has been added.</span>
</p><figure id="attachment_257"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" target="_blank" rel="attachment wp-att-257"><img class=" wp-image-257 "  src="/wp/blog/uploads/2013/01/crmds_work_added.png" alt="crmds_work_added" width="329" height="254" srcset="/wp/blog/uploads/2013/01/crmds_work_added.png 915w, /wp/blog/uploads/2013/01/crmds_work_added-300x231.png 300w, /wp/blog/uploads/2013/01/crmds_work_added-624x481.png 624w" sizes="(max-width: 329px) 85vw, 329px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<p >
  <span >You can then just continue searching for and adding any publications that are not in your ORCID profile.</span>
</p>

<p >
  <span >Note that, occasionally, you may see an orange icon that says that an item is &#8220;Not Visible&#8221;</span>
</p><figure id="attachment_258"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" target="_blank" rel="attachment wp-att-258"><img class="wp-image-258 "  src="/wp/blog/uploads/2013/01/not_visible.png" alt="not_visible" width="329" height="254" srcset="/wp/blog/uploads/2013/01/not_visible.png 915w, /wp/blog/uploads/2013/01/not_visible-300x231.png 300w, /wp/blog/uploads/2013/01/not_visible-624x481.png 624w" sizes="(max-width: 329px) 85vw, 329px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure> 

<p >
  <span >This only occurs when you have previously added an item to your profile using CRMDS and then either:</span>
</p>

  1. <span >Set the ORCID privacy for that particular work item to &#8220;Private&#8221; in your ORCID profile.</span>
  2. <span >Deleted the work from your ORCID profile.</span>

<span >Unfortunately, CRMDS has no way to determine which of these two events occurred  However, If you click on the &#8220;Not Visible&#8221; icon, you will be prompted with two ways to resolve this issue. Either you can:</span>

  1. <span >Reset the privacy settings on the specified work to &#8220;Public&#8221; or &#8220;Limited&#8221;</span>
  2. <span >Confirm to CRMDS that you have deleted the item from your profile.</span><figure id="attachment_259"  class="wp-caption aligncenter">

<a href="/blog/easily-add-publications-to-your-orcid-profile/" target="_blank" rel="attachment wp-att-259"><img class=" wp-image-259 "  src="/wp/blog/uploads/2013/01/not_visible_prompt.png" alt="not_visible_prompt" width="329" height="254" srcset="/wp/blog/uploads/2013/01/not_visible_prompt.png 915w, /wp/blog/uploads/2013/01/not_visible_prompt-300x231.png 300w, /wp/blog/uploads/2013/01/not_visible_prompt-624x481.png 624w" sizes="(max-width: 329px) 85vw, 329px" /></a><figcaption class="wp-caption-text">click on thumbnail to see larger image</figcaption></figure>

<p >
  <span >If the issue was your privacy settings, then once you have changed the privacy settings to public/limited you can simply click on the &#8220;Refresh&#8221; button and CRMDS will reflect the correct status of the work.</span>
</p>

<p >
  <span >The best way to avoid this kind of confusion is to go to your ORCID settings and set the default privacy level for &#8220;works&#8221; to either &#8220;limited&#8221; or &#8220;public.&#8221;</span>
</p>

<p >
  <span >Crossref Metadata Search is still a &#8220;<a title="Crossref Labs" href="https://www.crossref.org/labs/" target="_blank">Crossref Labs</a>&#8221; project and, as such, we are very interested to hear feedback on this new ORCID functionality for CRMDS. Please send comments, etc. to:</span>
</p>

<p >
  <span ><a href="/blog/easily-add-publications-to-your-orcid-profile/" rel="attachment wp-att-261"><img class="alignnone size-full wp-image-261" src="/wp/blog/uploads/2013/01/labs_email.png" alt="labs_email" width="233" height="42" /></a></span>
</p>
