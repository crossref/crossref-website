---
title: 'Status, I am new'
author: Isaac Farley
draft: false
authors:
  - Isaac Farley
  - Ginny Hendricks
  - Geoffrey Bilder
date: 2018-07-02
categories:
  - Support
  - Member Experience
  - User Experience
  - Status
archives:
  - 2018
---

Hi, I’m Isaac. I’m new here. What better way to get to know me than through a blog post? Well, maybe a cocktail party, but this will have to do. In addition to giving you some details about myself in this post, I’ll be introducing our [status page](http://status.crossref.org/), too.

<!--more-->

### A little about me

In mid-April, I began as the new Support Manager. My goal is to fill the very large shoes left by Patricia Feeney moving into the Head of Metadata role. I know Patricia knows Crossref and the rich community of members (and metadata!) inside and out. I’ll get there too. For now, I have immersed myself in tackling as many of your support questions as possible, so I may have already met some of you on a support ticket. If so, thanks for your patience; you likely have already taught me a thing or two!

<p align="center">
<img src="/images/blog/isaac.jpg" alt="Isaac, on the lookout to provide you excellent support" height="250px" width="250px" />
</p>
<p align="center">Isaac, on the lookout to provide you excellent support</p>


I came to this position from one of our members – the Society of Exploration Geophysicists, where I served as the Digital Publications Manager for the last five years. Like many of you, I was always impressed, intrigued, and excited by the work underway at Crossref and wanted to be a part of the team. So, here I am, very much looking forward to the challenge ahead.

I work remotely from Tulsa, Oklahoma, where I live with my wife and two daughters. Tulsa doesn’t have as many members as D.C., London, or Jakarta, but I hope to meet some of you during outreach trips, LIVE events, online in a webinar, or in our support community.

One of the things that attracts me to being a part of this community are our [truths](/truths/). As a quick reminder, the truths are:

 * Come one, come all
 * One member, one vote
 * Smart alone, brilliant together
 * Love metadata, love technology
 * What you see, what you get
 * Here today, here tomorrow

I am drawn to forward-thinking, action-oriented communities that value collaboration and openness. These truths, and the ten weeks I have been at Crossref, have confirmed that this is one of those communities. As your new support manager, I want to emphasize our commitment to transparency: Ask me anything; I’ll tell you what I know. In that spirit, I have the privilege of introducing our new status page—a key piece in furthering our own transparency and openness.

[status.crossref.org](http://status.crossref.org/)

Our new status page provides critical, real-time information about our services—it helps us tell our overall story. If you are looking for metrics on the performance of our APIs, websites, the deposit system, or new beta services, bookmark this page. The system metrics provide daily, weekly, and monthly overviews of each of our services’ response time (in milliseconds) and uptime, or percentage of time that service has been operational during your selected time span (daily, weekly, or monthly).

From this page, we’ll announce planned maintenance and keep you regularly updated when we have an incident. And, we’ll provide regular status updates for these incidents when in progress, updated, and completed.

<p align="center">
<img src="/images/blog/support .jpg" alt="Our new status page" height="750px" width="550px" />
</p>
<p align="center">Our new status page – status.crossref.org</p>

I encourage you to subscribe to the updates from the top-right corner of the page. While we’ll update this page with any service-related outages, subscribing for notifications will allow you to stay current on the latest. We’ll describe maintenance and incidents clearly, simply, and timely when we have them. And, if we don’t, call us on it.

If you have questions about the performance of our services, the status page is a great starting place. If you still have questions, ask us, we’ll tell you what we know.

---
