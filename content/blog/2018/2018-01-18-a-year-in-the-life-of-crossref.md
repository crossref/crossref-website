---
title: "A year in the life of Crossref"
author: Ed Pentz
draft: false
authors:
  - Ed Pentz
date: 2018-01-23
categories:
  - Members
  - Member Briefing
  - Community
  - Strategy
archives:
  - 2018
---
We are delighted to report that last year Crossref welcomed a record-breaking 1,939 new members and, because our member base is growing so rapidly in both headcount and geography---with the highest number of new members joining from Asia---we thought it was a good time to reiterate what Crossref is all about, as well as show off a little about the things we are proud to have achieved in 2017.

<!--more-->

> What is Crossref?

We are an organization that runs a registry of metadata and DOIs of course, but we are much more than that---staff, board, working groups, and committees as well as a broad range of collaborators, users, and supporters in the wider scholarly communications community. Increasingly, our community includes new contributors like scholars, funders, and universities. Together, we are all working toward the same goal---to enhance scholarly communications. Everything we do is designed to put scholarly content in context so that the content our members publish can be found, cited, used, and re-used.

Here's how we did that over the past year:

## We rallied the community

Rallying the community is all about working together to forge new relationships and pave the way for future generations of researchers---in 2017 we were closely involved with the launch of [Metadata 2020](http://www.metadata2020.org/); a collaboration that advocates richer, connected, and reusable metadata for all research outputs.

## We tagged and shared metadata

To make sure that our APIs continue to have real, genuine utility, we introduced a new service called [Metadata Plus](/news/2017-11-15-new-metadata-plus-service-launching/) in 2017 so that platforms and tools can leverage the power of our rich, immense database to increase the value and discoverability of content.

## We played with new technology

To keep pace with changes in the industry and stay true to [our mission](/about/), we often play with new technology with the goal of offering a bigger and better infrastructure. In 2017 we formed a working group and an advisory group for two new identifiers that will see this infrastructure increase; [Organization IDs](/blog/organization-identifier-working-group-update/) which became ROR, and [Grant IDs](/blog/global-persistent-identifiers-for-grants-awards-and-facilities/) which became the Crossref Grant Linking System.

## We made new tools and services

Combining our own knowledge and experience with input from the wider community, in 2017 we were able to launch in Beta a new and exciting tool called [Event Data](/blog/event-data-enters-beta/). Event Data provides a record of where research has been bookmarked, linked, recommended,  shared, referenced, commented on etc, beyond publisher platforms---which is a great example of putting scholarly research in a wider context.

---

So, while richer metadata (including more record and resource types) remains our focus 2018 and beyond, we also hope that as we become a bigger and more global community we can move beyond the basics and work together to make sure that DOIs, are not the be-all and end-all when they are, in fact, just the beginning.
