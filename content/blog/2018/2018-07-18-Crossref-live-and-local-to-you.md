---
title: 'Crossref LIVE and local (to you)'
author: Vanessa Fairhurst
draft: false
authors:
  - Vanessa Fairhurst
date: 2018-07-18
categories:
  - Education
  - Ambassadors
  - Community
  - Crossref LIVE
  - Outreach
  - Collaboration
archives:
  - 2018
---
The last few months have been busy for the Crossref community outreach team. We’ve been out and about from Cape Town to Ulyanovsk—and many places in between—talking at ‘LIVE locals’ to members about all things metadata.
Our [LIVE locals](/events/) are one-day events, held around the world—but local to you—that provide both deeper insight into Crossref, and information on our services and how to benefit from them. These events are always free to attend, and whether you are a long-established member, totally new, or not even a member at all, we welcome you all to join us. <!--more-->

At our most recent events we collaborated with some fantastic organizations and welcomed attendees from a variety of backgrounds including editors, publishers, service providers, researchers and other metadata users.

## South Africa

In April [Chuck Koscher](/people/chuck-koscher/), Director of Technology, and I travelled to South Africa for two LIVE locals, one in Pretoria and the other in Cape Town—and both in collaboration with the [Academy of Science of South Africa (ASSAf)](https://www.assaf.org.za/). ASSAf also provided two excellent speakers, Nadine Wubbeling (ASSAf) and Pierre de Villiers ([AOSIS](https://aosis.co.za/)), who shared their experiences with Crossref and presented valuable insights into the work that they do.

Delivering events for a varied audience like this means there are often differing levels of knowledge and experience. So, to make sure everyone benefited from our sessions, we covered the different ways you can work with the Crossref deposit system as an XML pro, or an absolute beginner. This included a live demonstration of our new deposit tool Metadata Manager (currently in beta) which should help those less technically-minded people (like myself), and be a big improvement upon our current web deposit form.

{{% imagewrap right %}}|<img src="/images/blog/dr-pierre.jpg" alt="Dr. Pierre de Villiers" height="250px" width="300px"/>|<img src="/images/blog/table-mountain2.jpg" alt="Table Mountain" height="250px" width="300px"/>|{{% /imagewrap %}}
The day ended with a technical session, where attendees discussed specific issues they needed help with, which mainly focussed on retrieving metadata in the Crossref system, interpreting reports, and support with XML.

_Images left to right: Dr. Pierre de Villiers talks about the Crossref Experience at AOSIS, and the stunning scenery of Table Mountain provided a nice backdrop to our Cape Town event._

## Russia

{{% imagewrap right %}}<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Just back from a few days in Russia 🇷🇺. We ran a <a href="https://twitter.com/CrossrefOrg?ref_src=twsrc%5Etfw">@CrossrefOrg</a> LIVE local in Ulyanovsk for 60 editors, made plans to do more education and outreach in the region and caught a <a href="https://twitter.com/hashtag/FifaWorldCup2018?src=hash&amp;ref_src=twsrc%5Etfw">#FifaWorldCup2018</a> game... <a href="https://t.co/GSdNEujJXa">pic.twitter.com/GSdNEujJXa</a></p>&mdash; Rachael Lammey (@rachaellammey) <a href="https://twitter.com/rachaellammey/status/1010040188406587393?ref_src=twsrc%5Etfw">June 22, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>{{% /imagewrap %}}

The World Cup wasn’t the only big event in Russia last month. That’s right, we were there too—with our very first Russian LIVE local! On the 19th June, 60 attendees from a range of academic and publishing institutions joined us at [The Ulyanovsk State Pedagogical University](http://www.ulspu.ru/).
[Rachael Lammey](/people/rachael-lammey/) and I introduced Crossref, the role of identifiers, and how to register different resource types with us. We also discussed the use and importance of providing accurate and comprehensive metadata, and shared some interesting use cases.

Guest speaker Professor Zinaida Kuznetsova talked about her experiences of working with Crossref and the benefits of being a member. This was complimented by a talk by fellow guest speaker Maxim Mitrofanov from Crossref sponsoring organisation, [NEICON](https://neicon.ru/). Maxim explained how NEICON works with Crossref, and provide services for the smaller members they support. Maxim is also one of our [Crossref Ambassadors](/community/our-ambassadors/) - and he will be running more Russian webinars on our services in the near future, so look out for those listed on our [webinar page](/webinars/)!

We’d like to say a big thank you to the team at Ulyanovsk State Pedagogical University for their support and help with the event. Also thanks to our fantastic interpreters who helped us immensely by relaying the information to the audience in Russian, as well as helping to translate and answer questions.

## Germany

{{% imagewrap right %}}<center><blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Najko Jahn from Göttingen State and University Library talks about how he uses <a href="https://twitter.com/CrossrefOrg?ref_src=twsrc%5Etfw">@CrossrefOrg</a> metadata in his work <a href="https://twitter.com/hashtag/CRLIVEGermany?src=hash&amp;ref_src=twsrc%5Etfw">#CRLIVEGermany</a> <a href="https://t.co/Y89ZkBMoSh">pic.twitter.com/Y89ZkBMoSh</a></p>&mdash; Vanessa Fairhurst (@NessaFairhurst) <a href="https://twitter.com/NessaFairhurst/status/1011902317828993024?ref_src=twsrc%5Etfw">June 27, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></center>{{% /imagewrap %}}

One week later and we were in Hannover, Germany. Crossref’s [Laura Wilkinson](/people/laura-j-wilkinson/), [Joe Wass](/people/joe-wass/) and [Jennifer Kemp](/people/jennifer-kemp/) joined me for this event, which was held in collaboration with the German National Library of Science and Technology ([Technische Informationsbibliothek - TIB](https://www.tib.eu/en/service/news/details/metadaten-unverzichtbarer-rohstoff-im-digitalen-zeitalter/) at their impressive venue in on the 27th June. ￼

The day focused on all things metadata - how it can be used and why good metadata is important. This included taking a look at our new [Participation Reports](https://www.crossref.org/members/prep/) tool and a fascinating talk from guest speaker Najko Jahn from [Göttingen State and University Library](https://www.sub.uni-goettingen.de/sub-aktuell/) on the benefits of using Crossref metadata for libraries and scientists.

[Datacite’s](https://www.datacite.org/) Britta Dreyer also spoke about how DataCite and Crossref support research data sharing, before Joe Wass and I presented updates to the collaborative [Org ID project](/categories/organization-identifier/) and [Event Data](/services/event-data/) service. The day concluded with us sharing more ways to participate in Crossref and other community initiatives.

## Questions? Вопросов? Fragen?

Over the course of these events we were asked many questions—and here are some of the more interesting/common ones posed to the team:  <br>

Q. Do I have to join Crossref directly, or can I join as part of a group of smaller organizations?  <br>
A. You don’t have to be a direct member, you can join via a Sponsor. See our [sponsors page](/community/sponsors/) for a list of Sponsors in your area, and for more information on becoming a Sponsor.<br>

Q. Can I link translations of works together?  <br>
A. Yes, a journal article published in two languages can each be assigned its own DOI, and then linked in the metadata using the [relationship type](https://support.crossref.org/hc/en-us/articles/214357426) TranslationOf from our schema.<br>

Q. Does the web deposit form support depositing abstracts and references?<br>
A. No, it doesn’t. However, our new Metadata Manager tool does and if you are in interested in trying it out in beta, [let us know](mailto:feedback@crossref.org).<br>

Q. Can I share your new Participation Report tool with my colleagues?<br>
A. Yes you can! It’s open and available for use, just come along and [search for a member](https://www.crossref.org/members/prep/).<br>

Q. Can I also register book chapters, dissertations and other record types under the same prefix?<br>
A. Yes you can. You can register any of the different [resource types we support](https://support.crossref.org/hc/en-us/articles/213123586-Metadata-and-content-type-overview) under one prefix.<br>

Q. Will you be doing more events in this region in future?<br>
A. We hope so, and we are always happy to hear from those who wish to collaborate on future events, so just [contact us](mailto:feedback@crossref.org) to get involved.

---
