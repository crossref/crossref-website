---
title: 2018 election slate
author: Lisa Hart Martin
draft: false
authors:
  - Lisa Hart Martin
date: 2018-08-17
categories:
  - Board
  - Member Briefing
  - Governance
  - Election
  - Crossref LIVE
  - Annual Meeting
archives:
  - 2018
---

With Crossref developing and extending its services for members and other constituents at a rapid pace, it’s an exciting time to be on our board. We recieved 26 expressions of interest this year, so it seems our members are also excited about what they could help us achieve.

<!--more-->

From these 26, the [Nominating Committee](/committees/nominating) has put forward the following slate.

## The 2018 slate: seven candidates for five available seats

* **African Journals OnLine (AJOL),** Susan Murray, South Africa
* **American Psychological Association (APA),** Jasper Simons, USA
* **Association for Computing Machinery (ACM),** Scott Delman, USA
* **California Digital Library (CDL),** Catherine Mitchell, USA
* **Hindawi,** Paul Peters, UK
* **Sage,** Richard Fidczuk, USA
* **Wiley,** Duncan Campbell, USA

{{% divwrap blue-highlight %}}

### [Read the candidates’ organizational and personal statements](/board-and-governance/elections/2018-slate)

{{% /divwrap %}}

Candidates were chosen based on the following criteria:

* Follow the guidance from the Board to provide a slate or seven or fewer.
* Maintain the current balance of the board with respect to size of organizations.
* Improve balance in other areas, with respect to gender and geography.
* Also consider types of organizations and sector, as well as engagement with Crossref and its services.    

## You can be part of this important process, by voting in the election

If your organization is a member of Crossref on September 14, 2018 you are eligible to vote when voting opens on September 28, 2018 (affiliates, however, are not eligible to vote).

## How can you vote?

On September 28, 2018, your organization’s designated voting contact will receive an email with a link to the formal Notice of Meeting and Proxy Form with concise instructions on how to vote. An additional email will be sent with a username and password along with a link to our online voting platform. It is important to make sure your voting contact is up-to-date.

## Want to add your voice?

We are accepting independent nominations until November 7, 2018. Organizations interested in standing as an independent candidate should contact me by this date with a list of ten other Crossref members that endorse their candidacy.

The election itself will be held at [LIVE18 Toronto](/crossref-annual-meeting/archive/#2018), our annual meeting, on 13 November 2018 in Canada. We hope you’ll be there to hear the results.
