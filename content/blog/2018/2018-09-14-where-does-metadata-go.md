---
title: 'Where does publisher metadata go and how is it used?'
author: Laura J Wilkinson
draft: false
authors:
  - Laura J Wilkinson
date: 2018-09-17
categories:
  - Participation
  - Content Registration
  - Metadata
  - Best practices
archives:
  - 2018
---

Earlier this week, colleagues from Crossref, ScienceOpen, and OPERAS/OpenEdition joined forces to run a webinar on “Where does publisher metadata go and how is it used?”.

<!--more-->

Stephanie Dawson explained how ScienceOpen’s freely-accessible, interactive search and discovery platform works by connecting and exposing metadata from Crossref. Her case study showed that articles with additional metadata had much higher average views than those without - depositing richer metadata helps you get the best value from your DOIs!

Pierre Mounier of OPERAS/OpenEdition showed us how a variety of persistent identifiers (PIDs) including DOIs, ORCID iDs, and Funder Registry IDs have been used on OA book platforms to improve citations, author attribution, and tracking of funding. He described a forthcoming annotations project with Hypothes.is, and explained how Crossref metadata is being used in both usage and alternative metrics.

## Five ways to register content with Crossref

My overview of Content Registration outlined the five ways to register content with Crossref:

- Via the manual [web deposit form](https://apps.crossref.org/webDeposit/)
- Through Crossref’s new [Metadata Manager](https://www.crossref.org/metadatamanager/) tool (beta)
- With OJS’s Crossref plugin - [more information here](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/config) ([see OJS downloads](https://pkp.sfu.ca/ojs/ojs_download/) Version 3.1.0 and above is the best option for supporting the fullest Crossref metadata)
- With a [manual  XML upload file](https://doi.crossref.org)
- Or, using HTTPS to POST XML

I also emphasized the importance of depositing, adding, and updating your metadata, and spoke about:

- Basic citation metadata: titles, author names, author affiliations, funding data, publication dates, issue numbers, page numbers, ISSNs, ISBNs...
- Non-bibliographic metadata: reference lists, ORCID iDs, license data, clinical trial information, abstracts, relationships...
- Crossmark: errata, retractions, updates, and more
- How important it is to have accurate, clean, and complete metadata
- The importance of registering your backfiles

## How to see the metadata you have

Anna Tolwinksa, Crossref’s Member Experience Manager, gave us an overview of the new [Participation Reports](https://www.crossref.org/members/prep/) tool. She  explained how Participation Reports allows anyone to see the metadata Crossref members have registered with us, and how you can see for yourself where the gaps in your metadata are, and—importantly—how you can improve your coverage.

## What we learnt

- There are [10 key metadata elements or checks](/participation/) in Participation Reports that aid in Crossref members’ content discoverability, reproducibility and research integrity:
    - References
    - ~Open References~ _[EDIT 6th June 2022 - all references are now open by default]._
    - ORCID iDs
    - Funder Registry IDs
    - Funding award numbers
    - Text mining URLs
    - License URLs
    - Similarity Check URLs
- Every day, research organizations around the world rely on metadata from Crossref, and use it in a variety of systems. Here are [a few examples](/categories/api-case-study/). Many organizations that enable research depend on Crossref’s metadata; we received over 650 million queries just last month
- Crossref members should check Participation Reports to see what percentage of their content includes rich metadata
If the percentages are low, Crossref is happy to work with you to help understand and improve your coverage
- Richer metadata helps research to be found, cited, linked to, assessed, and reused
- To make sure your work can be found!

Catch up with the [webinar recording](https://www.youtube.com/watch?v=RJhDHWhFFAs&feature=youtu.be), and slides from [Laura](https://www.crossref.org/pdfs/crossref-webinar-laura-wilkinson-where-does-publisher-metadata-go-and-how-is-it-used-sep11-2018.pdf), [Stephanie](https://www.slideshare.net/slideshow/crossref-webinar-stephanie-dawson-sciencopen-metadata-091118/114165046), [Pierre](https://www.crossref.org/pdfs/crossref-webinar-pierre-mounier-where-does-publisher-metadata-go-and-how-is-it-used-sep11-2018.pdf), and [Anna’s](https://www.slideshare.net/slideshow/crossref-webinar-anna-tolwinska-crossref-participation-reports-metadata-091118/114163162) presentations, and please [contact us](mailto:feedback@crossref.org) if you have any questions.

___
