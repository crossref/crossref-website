---
title: 'Join us in Toronto this November for LIVE18'
author: Christine Cormack Wood
draft: false
authors:
  - Christine Cormack Wood
date: 2018-09-25
categories:
  - Community
  - Crossref LIVE
  - Annual Meeting
archives:
    - 2018
---

LIVE18, your Crossref annual meeting, is fast approaching! We’re looking forward to welcoming everyone in Toronto, November 13-14.

<!--more-->

This year’s theme “How good is your metadata?” centers around the definition and benefits of metadata completeness, and each half day will cover some element of the theme:


* Day one, AM *Defining good metadata*
* Day one, PM *Improving metadata quality and completeness*
* Day two, AM *What does good metadata enable?*
* Day two, PM *Who is using our metadata and what are they doing with it?*

Both days will be packed with a mixture of plenary and interactive sessions. Speakers include:

* Patricia Cruse, DataCite
* Kristen Fisher Ratan, CoKo Foundation
* Stefanie Haustein, University of Ottawa
* Bianca Kramer, Utrecht University
* Shelley Stall, American Geophysical Union
* Ravit David, University of Toronto Libraries
* Graham Nott, Freelance developer of an eLife JATS conversion tool
* Paul Dlug, American Physical Society

A ‘meet and mingle’ drinks reception will be held directly after the election results on day one.


## About the theme—how good is your metadata?

The reach and usefulness of research outputs are only as good as how well they are described. Metadata is what is used to describe the story of research: its origin, its contributors, its attention, and its relationship with other objects.

The more machines start to do what humans cannot—parse millions of files through multiple views—the more we see what connections are missing, the more we start to understand the opportunities that better metadata can offer.

LIVE18 will focus this year entirely on the subject of metadata. It touches everything we do, and everything that publishers, hosting platforms, funders, researchers, and libraries do.


## Come and join the discussions

[Register to join](https://www.eventbrite.com/e/crossref-live18-toronto-nov-13-14-crlive18-registration-46284552342 ) us this 13 and 14 November, at the [Toronto Reference Library](https://www.torontopubliclibrary.ca/torontoreferencelibrary/), 789 Yonge Street, Toronto, Canada—we look forward to seeing you there.

___

[Read more about our annual events](/crossref-annual-meeting/archive/)
