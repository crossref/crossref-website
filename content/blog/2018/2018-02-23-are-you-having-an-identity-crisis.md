---
title: 'Are you having an identity crisis?'
author: Amanda Bartell
draft: false
authors:
  - Amanda Bartell
date: 2018-02-23

categories:
  - Members
  - Member Experience
  - Community
archives:
  - 2018

---

We work with a huge range of organizations in the scholarly communications world—publishers, libraries, universities, government agencies, funders, publishing service providers, and researcher services providers—and you each have different relationships with us.

Some of you are members who create and disseminate your own content, register it with us by depositing metadata, and help steer our future by voting in our annual board elections. Some of you don't vote in our board elections but do play a vital role by registering content on members' behalf.

And some of you make use of the metadata provided by our members and so perform a key service by getting their published works out into the world, but don't vote in our board elections.

After a recent review we realized our Member Types weren't completely clear, and may in fact have led to a bit of confusion. With this in mind, we put some thought into their revision and have now given them the clarity they were missing. Over the course of this year we'll be checking that everyone is in the right group and getting the appropriate support based on your Member Type.

| Former Member Type name  | New Member Type name  |
|:---|:---|
|Publisher |Member |
|Sponsoring Publisher |Sponsoring Member |
|Represented Member |Sponsored Member |
|Sponsoring Entity |Sponsoring Organization |
|Sponsored Member |Sponsored Organization |
|Affiliate |Metadata User |
|Service Provider |(No change to Member Type name) |
<br>

> So, what's different?

The changes we've made help to differentiate if you're a voting member (and therefore have a say in our future direction), or not. If you are a voting member, you'll now have the word "Member" in your title—and if you're not—you won't, as the diagram below indicates.<br>
<img src="/images/blog/Sugar-labels-2.png" alt="membership map" width="800px" /><br>
Where there are two organizations with a sponsorship arrangement in place (with a sponsoring party and a sponsored party), one of you will always be the voting party, and the other will be non-voting. These partnerships will therefore always contain one "Member" and one "Organization".

We've also stopped using the word "Publisher" in our Member Types as not all our members consider themselves to be publishers — sometimes you're libraries, funders, scholars, repositories, etc. As it says in one of our [truths](/truths) "Come one, come all: we define publishing broadly. If you communicate research and care about preserving the scholarly record, join us."

## How do you know if you are a voting member?

<br>Voting members fall into three Member Types: Members, Sponsoring Members and Sponsored Members.

This means you are Organizations who create and disseminate content, and therefore contribute to the scholarly record. Some of you register your content directly with us and some via a third party, but the key thing is that you're adding to our metadata records, and as such can have a say in the future direction of Crossref. Voting members can also  take metadata out of our system — and many of you do — however, your key relationship with us is as a member who is contributing to the scholarly record.

It also means you have [obligations](/membership/terms) to keep your records up-to-date, and maximize links with other Crossref members.

## What's the difference between the voting categories?

**Members**<br>
As a Member (formerly known as Publishers), you create and disseminate content, register your own content with us (usually under a single prefix), and are able to vote in our board elections. You pay an annual fee based on your publishing revenue, plus Content Registration fees for all new DOIs.

**Sponsoring Members**<br>
As a Sponsoring Member (formerly known as a Sponsoring Publisher), you do everything a standard member does, but as well as registering your own content under your own DOI prefix, you also register content on behalf of other, smaller publishers (ideally using separate DOI prefixes so the metadata is accurate and can be reported on separately and relied upon downstream).

When you vote, you vote on behalf of the organizations that you sponsor. You pay an annual fee based on your publishing revenue/expenses plus the publishing revenue of your sponsored organizations, and you also pay Content Registration fees for all new metadata records registered. You look after deposit billing for the organizations you sponsor, and provide technical and language support for them.

Some of our larger members may be thinking that you should be in this Member Type - and you're probably right! During the course of 2018 we'll be working with you to transition you over to Sponsoring Membership. If you are a Member who is thinking of becoming a Sponsoring Member, [please get in touch](mailto:member@crossref.org).

**Sponsored Members**<br>
As a Sponsored Member (formerly known as a Represented Member), you create and disseminate content, but you don't register your content directly with us—this is done by your Sponsoring Organization.  Because of this it's you, the one who creates and disseminates the content and thus contributes to the scholarly record, who can vote.

## How do you know if you are a non-voting member?

If you haven't spotted yourself yet, you may be one of the non-voting organizations we work with — these fall into four Member Types: Sponsoring Organizations, Sponsored Organizations, Service Providers and Metadata Users.

As a non-voting organization, you may still register content with us, but you either don't create and disseminate the content yourselves, or you're already represented by a voting organization. Non-voting organizations also include those whose only relationship with us is to make use of our metadata.  

## What's the difference between the non-voting categories?

**Sponsoring Organizations**<br>
As a Sponsoring Organization (formerly known as a Sponsoring Affiliate), you don't create and disseminate content yourself, but you do register content with us on behalf of your Sponsored Members — preferably using distinct DOI prefixes for each member. You also often look after their administrative, technical, billing and language support needs. You'll pay us an annual fee based on the publishing revenue of all your members, and Content Registration fees for all new DOIs. You might charge the members you work with for this service. You also provide support and promotion of our services and activities.

**Sponsored Organizations**<br>
As a Sponsored Organization (formerly known as a Sponsored Member), you do create and disseminate content yourself, but you don't register your own content. This is done by a Sponsoring Member, and as they have the member vote, you can't have one too. For this reason, we've removed the word "Member" from your title, to make your voting position clearer. Of course, your Sponsoring Member needs to represent your needs too when voting, so make sure you make them known!

**Service Providers**<br>
As a Service Provider you work closely with our members to collect and/or host and/or deposit metadata on their behalf. Unlike a Sponsoring Organization however you don't get involved with administrative, technical, billing or language support for the members you work with, but you're a key partner in helping them deposit quality metadata and contribute effectively to the scholarly record. During 2018 we'll be working more closely with you to help you collaborate with us more effectively.

**Metadata Users**<br>
Metadata Users (formerly known as Affiliates), you are the organizations who don't register content with us, but you do make use of it through our free and open APIs and search interfaces, or our paid-for Metadata Plus service, giving you access to a premium version of both the REST API and OAI-PMH. Of course all members can get metadata out of our systems as well, but if the only thing you do with us is get metadata out, then you're a Metadata User.

### Don't know which Member Type you are?

We're hoping these new names make it clearer, but if you're still confused, please get in touch with our [membership specialist](mailto:member@crossref.org)
