---
title: 'Crossref ambassador program'
author: Vanessa Fairhurst
draft: false
authors:
  - Vanessa Fairhurst
date: 2018-01-04
categories:
  - Education
  - Ambassadors
  - Community
  - Collaboration
archives:
  - 2018

---

We have listened to the feedback from you, our members, and you've told us of a need for local experts to provide support in your timezone and language, and to act as liaisons with the Crossref team. You've also asked for an increased number of training events both online and in person close to you, and for more representatives from Crossref at regional industry events.

We want to make sure we can reach members around the globe, and as such, a wide team of people is required who are knowledgeable in the languages, cultures, and member needs in a variety of countries. This is why we're launching our Ambassador Program.

<p align="center">
<img src="/images/blog/crossref-ambassadors-logo-rgb.jpg" alt="image of Crossref Ambassadors Logo" width="500px" />
</p>

> What are Crossref Ambassadors?

Crossref Ambassadors are volunteers who work within the international scholarly research community in a variety of different roles such as librarians, researchers or editors to name but a few. They are individuals who are well connected, value the work that Crossref does and are passionate about improving scholarly communication and the role Crossref plays within this system.

Some of the activities our ambassadors will undertake:

-   Staying up-to-speed with Crossref developments, for example, by attending webinars and maintaining regular check-ins with the Crossref team.
-   Engaging in the online community platform; providing feedback, joining in discussions and helping other members to resolve issues posted to the group.
-   Writing blog posts, or contributing to newsletters.
-   Participating in beta-testing of new products and services.
-   Helping with local LIVE events; for example, providing recommendations on speakers or venues, helping with logistics and presenting at the event.
-   Helping with the translation of Crossref material and content into local languages.
-   Running webinars on different Crossref services in local languages.
-   Running training sessions locally with Crossref members
-   Representing Crossref at relevant industry events

It is important that our ambassadors enjoy the work they are doing with Crossref by contributing in ways in which they feel comfortable, according to their interests, skills and the time they feel they want to contribute. For this reason, the role comes with a high degree of flexibility.

We see our ambassadors as valued members of the Crossref network and will provide them with:

-   A dedicated contact for any upcoming news, or to share ideas, queries or concerns.
-   Help with content for proposal calls, presentations, training and written articles.
-   Crossref materials and giveaways (plus ambassador-branded materials).
-   Personal endorsement via Crossref
-   Training on Crossref services and on wider relevant skills as necessary.
-   First look at new Crossref developments
-   Certification from Crossref on ambassador and training status.
-   Personal ambassador logo or badge for use on email, website and profile on the Crossref online community forum (launching later this year).

Crossref Ambassadors will become an increasingly key part of the Crossref community - the first port of call for updates or to test out new products or services, and the eyes and ears within the local academic community - working closely with Crossref to make scholarly communications better for all.

### Meet our first ambassadors!


{{% imagewrap right %}} <img src="/images/blog/jae-hwa-chang-sq.jpg" alt="image of Jae Hwa Chang" width="300px" /> {{% /imagewrap %}}

**Jae Hwa Chang** has been working at infoLumi as a manuscript editor in academic journals since 2010. Prior to joining infoLumi, she was a medical librarian at International Vaccine Institute and was engaged in medical information management and service. Her interests in information control and management started when she was doing work indexing newspaper articles at JoonAng Ilbo. She was fascinated by Crossref’s persistent efforts and contribution in developing new services to “make content easy to find, cite, link, and assess” and has been introducing them to Korean scholarly publishing communities. Jae earned her MA in Library and Information Science from Ewha Womans University, Korea. She serves as a vice chair of the Committee on Planning and Administration at the Korean Council of Science Editors. In her spare time, she enjoys traveling and experiencing new cultures.

장재화는 2010년부터 인포루미에서 의학학술지 원고편집을 담당하고 있다. 그전에는 국제백신연구소 도서관에서 사서로 일하면서 의학정보와 학술지논문 유통에 관심을 가졌으며, 그에 앞서서는 중앙일보에서 신문기사 DB 색인을 하면서 정보관리와 활용에 대해 연구하였다. 정보의 검색, 평가, 활용을 위해 꾸준히 새로운 서비스를 개발하는 Crossref에 매력을 느꼈고, 그 서비스들을 한국의 학술지 출판 관계자들에게 소개해왔다. 이화여자대학교에서 문헌정보학을 전공하였고, 한국과학학술지편집인협의회 기획운영위원회 부위원장을 맡고 있다. 여행과 다양한 문화 체험을 즐긴다.

<br>
{{% imagewrap right %}} <img src="/images/blog/edilson-demasio-sq.jpg" alt="image of Edilson Demasio" width="300px" /> {{% /imagewrap %}}

**Edilson Demasio** has been a librarian since 1995, with PhD. in Information Science at Federal University of Rio de Janeiro-UFRJ/IBICT. He works in the Department of Mathematics Library of State University of Maringá-UEM, Brazil. With 20 years' experience in scientific metadata and publishing. His expertise is various including knowledge in  scientific communication, Crossref services, research integrity, misconduct prevention in science, publishing on Latin America, biomedical information, OJS-Open Journal Systems, Open Access journals, scientific journals quality and indexing, and scientific bibliographical databases. He is enthusiastic about presenting and disseminating information about Crossref services to his community in Brazil and working within the community, exchanging ideas and experience.

Eu sou bibliotecário desde 1995, Doutor em Ciência da Informação pela Universidade Federal do Rio de Janeiro-UFRJ/convênio IBICT. Eu trabalho na Biblioteca do Departamento de Matemática da Universidade Estadual de Maringá-UEM. Com 20 anos de experiência em metadados científicos e editoração, entre outros. Meus conhecimentos são diversos sobre comunicação científica, cientometria, metadados XML, serviços Crossref, integridade em pesquisa, prevenção de más condutas na ciência, editoração, editoração na América Latina, informação biomédica, OJS-Open Journal Systems, revistas de Acesso Aberto, qualidade de periódicos científicos e indexação, bases de dados bibliográficas. Gosto de disseminar meu conhecimento a outras regiões e pessoas e de trabalhar em comunidade junto as instituições e outros países, de planejar novas apresentações, de trocar experiências como palestrante ou convidado e trabalhar na disseminação do conhecimento para todos.

<br>
{{% imagewrap right %}} <img src="/images/blog/lauren-lissaris-sq.jpg" alt="image of Lauren Lissaris" width="300px" /> {{% /imagewrap %}}

**Lauren Lissaris** has dedicated much of her career to the dissemination of valuable content on a robust platform. She takes pride in her achievements as the Digital Content Manager at JSTOR. [JSTOR](http://www.jstor.org/) provides access to more than 10 million academic journal articles, books, and primary sources in 75 disciplines. JSTOR is part of [ITHAKA](http://www.ithaka.org/), a not-for-profit organization helping the academic community use digital technologies to preserve the scholarly record and to advance research and teaching in sustainable ways.

Lauren successfully works with all aspects of journal content to effectively assist publishers with their digital content. This includes everything from XML markup, Content Registration/multiple resolution, and HTML website updates. Lauren has been involved in hosting current content on JSTOR since the program's launch in 2010. She continues to collaborate with organizations to successfully contribute to the evolution of digital content. The natural spread from journals to books has set Lauren up for developing and planning the book Content Registration program for JSTOR. She is a member of the Crossref Books Advisory Group and she helped successfully pilot Crossref’s new Co-access book deposit feature.

If you want to find out more information on the Ambassador Program, or you would like to express your interest in being an ambassador, you can either contact us at [feedback@crossref.org](mailto:feedback@crossref.org?subject=Ambassador Program) or complete our [online form](/community/ambassadors/).
