---
title: 'Metadata Manager: Members, represent!'
author: Jennifer Lin
draft: false
authors:
  - Jennifer Lin
date: 2018-10-15
categories:
  - Metadata
  - Content Registration
  - Citations
  - Identifiers
archives:
  - 2018
---
[Over 100 Million unique scholarly works](/blog/100000000-records-thank-you/) are distributed into systems across the research enterprise 24/7 via our APIs at a rate of around 633 Million queries a month. Crossref is broadcasting descriptions of these works (metadata) to all corners of the digital universe.

<!--more-->

{{% imagewrap right %}} <img src="/images/blog/broadcastmetadata.png" alt="broadcastmetadata" width="150px" /> {{% /imagewrap %}}
Whether you’re a publisher, institution, governmental agency, data repository, standards body, etc.: when you register and update your metadata with Crossref, you’re relaying it to the entire research enterprise. So make sure your publications are fully and accurately represented.

## Metadata Manager is here to help

This year, we’ve released a new tool aimed to make this easier and give you, members, full control over your metadata. Presenting: **[Metadata Manager](https://www.crossref.org/metadatamanager/)**. It helps to:

- Simplify and streamline the [Content Registration](/services/content-registration) service, with a user-friendly interface
- Give you greater flexibility and control of metadata deposits
- Support users who are less familiar with XML
- Boost metadata quality, encourage cleaner and more complete metadata records

Metadata Manager is available to all our members and the service providers they work with, providing assistance with a wide range of metadata-related tasks:

- Regular Content Registration conducted by journal staff, editors and service providers
- Registering corrections, retractions, or other editorial expressions of concern
- Matching references to their DOIs and registering them with the publication
- Adding metadata to existing records such as license and funding information, abstracts, or data citations
- Late-arriving editorial updates/corrections after initial publication
- Unexpected corrections to production hiccups
- Emergency editorial changes that affect publication record
- Accelerated registration for special pieces published outside of regular workflow
- Securely and efficiently transfer titles to another publisher as the authorized owner

Issues arise all the time in the dynamic and challenging work of scholarly communications. Metadata Manager provides a fast and easy way to meet these head-on when broadcasting new content or updating existing content. Submissions through this tool are processed immediately upon submission (i.e., no queues!).

This new tool empowers our members to “represent” in the exhilarating thrum of data reaching our API users. At this moment in time, it only supports journals, but our development team is currently working hard to include the remaining record types.

## Features

Here’s a smattering of highlights from the Metadata Manager feature list:

- All metadata: easily adds any and all metadata, allowing publishers to add richness and depth to their records.
- Prevents rejected submissions: it ensures you have satisfied all the basic Content Registration requirements and points out any input errors.
- Expedited deposit: the Content Registration system processes each submission immediately, bypassing the deposit queue.
- Historic log: easy to read archive of all previous submissions.
- Effortless review: provides a clean, condensed view of metadata (invariably complicated and lengthy) to support human review of the content before submission.
- Aids members to follow best practices: checks for completeness and reminds users of the full breadth of metadata available for the article, volume/issue, and the journal itself.
- Full control over title transfers: no need to make these requests through our support channels. Complete the transfer at your convenience, directly through the system.

For those of you that have looked at your own metadata contribution with the use of our new [Participation Reports](https://www.crossref.org/members/prep/), you’ll find using Metadata Manager a quick and useful way to help you level-up your records.

## Members, represent!

We invite you to register and update your publications with Metadata Manager, relay the metadata fully and accurately to the entire research enterprise. Check out the comprehensive [help documentation](/education/member-setup/metadata-manager/) to find out how to set up your workspace and get started right away with your usual Content Registration login details.

As mentioned, we are continuing development, adding support for all remaining record types as well as enhancing existing features. The webDeposit form will remain available throughout this time. For journal publishers, give us a whirl and [let us know](mailto:support@crossref.org) if you see something missing or there’s a function that would improve your Content Registration experience!
