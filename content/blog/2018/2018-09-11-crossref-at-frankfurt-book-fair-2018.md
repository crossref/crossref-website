---
title: 'Crossref at the Frankfurt Book Fair 2018'
author: Amanda Bartell
draft: false
authors:
  - Amanda Bartell
date: 2018-09-11
categories:
  - Meetings
  - Community
  - Participation Reports
archives:
  - 2018
---
### How good is your metadata? Find out at the Frankfurt Book Fair...

At the Frankfurt Book Fair this year (Hall 4.2, Stand M82), the Crossref team will be on hand to give you a personal tour of our new [Participation Reports](https://www.crossref.org/members/prep/) tool. Or join us at The Education Stage to hear about how this new tool can help you view, evaluate and improve your metadata participation.

<!--more-->

<center>

{{% divwrap blue-highlight %}}**How good is your metadata?**
Join us Thursday 11th October at 15.30
at the Education Stage in Hall 4.2 to find out
{{% /divwrap %}}

</center>

### Lots of reasons to visit our stand

We’ll be located in the same place as last year, Hall 4.2, Stand M82, and there are lots of reasons to visit us:

Get your metadata participation evaluated -  [Anna Tolwinska](/people/anna-tolwinska) and [Amanda Bartell](/people/amanda-bartell) will walk you through your own Participation Report and provide guidance on how to improve your results. Discover how complete your metadata is, where the gaps are, and how other publishers compare.

Discuss a technical issue that’s hindering your metadata participation (or any other technical issue) with [Isaac Farley](/people/isaac-farley) and [Paul Davis](/people/paul-davis) from our Technical Support team.

[Jennifer Kemp](/people/jennifer-kemp) will also be around to answer all your metadata use and reuse questions. She’s looking forward to chatting with all kinds of service providers and toolmakers.

On the strategy side, [Ginny Hendricks](/people/ginny-hendricks/) will be there on Wednesday 10th if you’d like to discuss any policy stuff, new ideas, or find out what Crossref is planning next.

### Ask us anything

Not just Participation Reports—you can ask us about anything. Perhaps about our newer record types such as [preprints](/blog/preprints-growth-rate-ten-times-higher-than-journal-articles/), pending publications (i.e. DOIs on acceptance), or [data citations](/blog/how-do-you-deposit-data-citations/). Or, ask us how you can:

* Advance scholarly pursuits for the benefit of society, through [Metadata 2020](http://www.metadata2020.org/)
* Check papers for originality, with our service for editorial rigour, through [Similarity Check](/services/similarity-check/)
* Discover where and how research is being discovered, through [Event Data](/services/event-data/)
* Reveal who is citing your published papers and how platforms can display this information, with our [Cited-by service](/services/cited-by/)
* Provide evidence of trust in published outputs, revealing updates, corrections and retractions, through our [Crossmark service](/services/crossmark/)

[Let us know](mailto:feedback@crossref.org) if you’d like to book in a meeting with one of us, or do just stop by the stand to say “Guten Tag”.

We look forward to seeing you there - bis dann!
