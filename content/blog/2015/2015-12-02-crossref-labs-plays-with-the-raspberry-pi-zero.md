---
title: Crossref Labs plays with the Raspberry Pi Zero
author: Joe Wass
authors:
  - Joe Wass
date: 2015-12-02

categories:
  - Crossref Labs
  - Event Data
  - Wikipedia
archives:
  - 2015

---
<span >If you’re anything like us at Crossref Labs (and we know some of you are) you would have been very excited about the launch of the <a href="https://www.raspberrypi.org/products/">Raspberry Pi Zero</a> a couple of days ago. In case you missed it, this is a new edition of the tiny low-priced Raspberry Pi computer. Very tiny and very low-priced. At $5 we just had to have one, and ordered one before we knew exactly what we want to do with it. You would have done the same. Bad luck if it was out of stock.</span><!--more-->

<img src="/wp/blog/uploads/2015/12/run.jpg" alt="run" class="img-responsive" />

<span >We love the way <a href="/blog/coming-to-you-live-from-wikipedia/">DOIs are being used in Wikipedia</a>, but you probably already <a href="/blog/real-time-stream-of-dois-being-cited-in-wikipedia/">know that by now</a>. Not only is it a brilliant source of information, mostly well cited, it’s also an organic living thing, with countless people and bots working together on countless articles. Our live stream of edits that cite (or uncite) DOIs shows new scholarly literature unfold, as it happens. From new articles to new references to improved citations to edit wars to bots cleaning up all the mess, it captivates everyone we show it to. The <a href="https://live.eventdata.crossref.org/live.html">latest version has a live chart</a> to show exactly how much activity is going on.</span>

<span >Crossref works in five ways: <a href="/blog/the-logo-has-landed/">Rally, Tag, Run, Play, and Make</a> and this definitely comes under &#8216;Play’. By the time our Raspberry Pi Zero arrived it was clear what we had to do. We ordered a <a href="https://en.wikipedia.org/wiki/Servo_(radio_control)">servo</a>, a driver board and a wireless adapter and got to work.</span>

<img src="/wp/blog/uploads/2015/12/servo.jpg" alt="servo" class="img-responsive" />

<span >We have some new neighbours in the basement. <a href="https://web.archive.org/web/20160305183505/http://oxhack.org/">Oxford Hackspace</a> is a community of people who want to work on projects from electronics to metalwork, hack things to improve them or find out how they work. A diverse bunch who at the last visit were working on squeezing unprecedented color capabilities from the 30 year old <a href="https://en.wikipedia.org/wiki/ZX_Spectrum">ZX Spectrum</a>, a nixie tube display, a smartphone controlled doorbell and a robotic glockenspiel. They let us use their soldering iron to solder a few header pins.</span>

<span >A bit of hacky Python, a pictureframe and lots of duck tape later, we have a live display of how many DOIs are cited and uncited per hour. It updates live every minute, fetches the latest numbers from the <a href="https://live.eventdata.crossref.org/live.html">Wikipedia DOI citation stream</a> and moves the hand.</span>

<img src="/wp/blog/uploads/2015/12/tape.jpg" alt="tape" class="img-responsive" />

<span >(For the worried engineers amongst you, rest assured that sufficient duck tape was added after this picture)</span>

<span >It’s extraordinary to think that a fully fledged computer with very capable specifications can be manufactured and sold for $5. Within the space of a lunchtime we had it up and running, all connected and fetching data over the internet via wireless. A generation ago you would have had to use <a href="https://en.wikipedia.org/wiki/Computer_programming_in_the_punched_card_era">punched cards</a>, send them by post and load them in by hand. The live stream would have been at least a month behind.</span>

<img class="alignnone size-full wp-image-1069" src="/wp/blog/uploads/2015/12/desk.jpg" alt="desk" class="img-responsive" />

<span >It now sits in our Oxford office reminding us that DOIs Aren’t Just for Traditional Bibliographies. Below <a href="http://twitter.com/gbilder">Geoff Bilder’s</a> reminder about what happens when you have too many standards (they’re telephone plugs from round the world).</span>

<img class="alignnone size-full wp-image-1073" src="/wp/blog/uploads/2015/12/wall.jpg" alt="wall" class="img-responsive"   />

<span >You can find <a href="https://github.com/Crossref/wiki.gauge">source code and instructions on the github repository</a> so you can make your own if you want.</span>
