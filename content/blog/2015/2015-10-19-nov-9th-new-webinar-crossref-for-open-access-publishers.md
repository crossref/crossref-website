---
title: 'Nov 9th - New Webinar: Crossref for Open Access Publishers'
author: April Ondis
authors:
  - April Ondis
date: 2015-10-19

categories:
  - Open Access
  - Webinars
tags:
  - Open Access
archives:
  - 2015

---
_<span ><a href="/wp/blog/uploads/2015/10/Nov-9-Crossref-Webinar-Open-Access-Publishers.jpg"><img class="alignleft size-medium wp-image-929" src="/wp/blog/uploads/2015/10/Nov-9-Crossref-Webinar-Open-Access-Publishers-300x300.jpg" alt="November 9 Crossref Webinar for Open Access Publishers" width="300" height="300" srcset="/wp/blog/uploads/2015/10/Nov-9-Crossref-Webinar-Open-Access-Publishers-300x300.jpg 300w, /wp/blog/uploads/2015/10/Nov-9-Crossref-Webinar-Open-Access-Publishers-150x150.jpg 150w, /wp/blog/uploads/2015/10/Nov-9-Crossref-Webinar-Open-Access-Publishers-624x624.jpg 624w, /wp/blog/uploads/2015/10/Nov-9-Crossref-Webinar-Open-Access-Publishers.jpg 693w" sizes="(max-width: 300px) 85vw, 300px" /></a><a href="https://attendee.gotowebinar.com/register/4198524003003451650" target="_blank">Register for our webinar</a></span>__<span > to learn best practices for</span>__ <span >depositing metadata and ways to help with the dissemination and discoverability of OA content.</span>_

<span style="font-weight: 400; color: #000000;">New Crossref services are being developed that have particular application to OA publishers. Did you know that our upcoming <a style="color: #000000;" href="/blog/det-poised-for-launch/">DOI Event Tracker service</a> was inspired by a group of OASPA publishers asking if there was a way to centrally support the gathering of data that could be analyzed as altmetrics?</span><!--more-->

<span style="font-weight: 400; color: #000000;">A large number of Crossref members classify their content as Open Access, and we’ve been thinking about how our infrastructure can support and communicate this.  In many ways, it already does:</span>

  * <span style="font-weight: 400; color: #000000;">Crossref supports the deposit of license and funding information in the DOI metadata.</span>
  * <span style="font-weight: 400; color: #000000;">Crossref’s Crossmark Service is useful to OA publishers who need to have the means to update info about their content, no matter where it sits.</span>
  * <span style="color: #000000;"><span >Crossref’s APIs allow</span> <span >publishers to make it easier for researchers to mine full-text content.</span></span>

<span style="color: #000000;"><b>Register for the Crossref Open Access Webinar</b></span>

<span style="color: #ff0000;"><b>Date:</b></span> <span style="font-weight: 400; color: #000000;">November 9, 2015</span>

<span style="color: #ff0000;"><b>Time:</b></span><span style="color: #000000;"><span > 8:00 am (San Francisco), </span><span >11:00 am (New York), 4:00 pm (London) </span></span>

<span style="color: #000000;"><b>Register:</b></span> [<span >https://attendee.gotowebinar.com/register/4198524003003451650</span>][1]

<span style="color: #000000;">Please join us for this new webinar that gives an overview of Crossref and its network of member publishers, along with information on Crossref services that have specific relevance to OA scholarly content.</span>

<span >Crossref will be joined by two guest speakers - <a href="http://www.frontiersin.org" target="_blank">Frontiers</a> </span><span ><span style="color: #000000;">will talk about their OA workflows and how Crossref services integrate with these, and James MacGregor from <a href="http://pkp.sfu.ca" target="_blank">PKP</a> will show </span></span><span >participants the Crossref Export/Registration Plugin which journals can enable to assign DOIs with Crossref and to help them participate in other Crossref services.</span>

<span style="font-weight: 400; color: #000000;">There will be time for questions and discussion during the webinar. The webinar will be recorded.</span>

 [1]: https://attendee.gotowebinar.com/register/4198524003003451650
