---
title: 'Summary of the environmental impact of Crossref'
author: Ed Pentz
image: "/images/banner-images/blog-emissions-moss.jpg"
authors:
  - Ed Pentz
  - Lucy Ofiesh
  - Rosa Morais Clark
  - Kornelia Korzec
draft: false 
date: 2024-12-05
categories:
  - Community 
  - Environment
archives:
    - 2024
---
 

In June 2022, we wrote a blog post “[Rethinking staff travel, meetings, and events](https://www.crossref.org/blog/rethinking-staff-travel-meetings-and-events/)” outlining our new approach to staff travel, meetings, and events with the goal of not going back to ‘normal’ after the pandemic. We took into account three key areas: 

1. The environment and climate change  
2. Inclusion  
3. Work/life balance

We are aware that many of our members are also interested in minimizing their impacts on the environment, and we are overdue for an update on meeting our own commitments, so here goes our summary for the year 2023! 

To be honest, the picture is mixed. On the positive side, we are traveling less and differently compared with 2019\. Most of our events have been online, with some regional in-person ones, reducing our carbon footprint and increasing inclusivity with more people attending Crossref events.  On the negative side, it hasn’t been easy to collect the data and figure out the best tools for calculating emissions, and we certainly haven’t captured all of our carbon emissions. Our approach has been to not let the perfect be the enemy of the good and we’ve focused on our largest source of carbon emissions \- air travel.  

### Some of the positive things: 

* We have maintained our strategic approach to consider environmental, inclusion, and work/life balance issues when we plan travel and to make the most of in-person events by focusing on those that involve interaction, such as listening and learning from our members and users, deepening relationships, co-creating, and forming new alliances  
* Crossref Annual Meetings and community updates have been online and in different time zones.  
* Crossref board meetings have been reduced from three in-person meetings per year to one face-to-face and two online meetings per year.  
* We had an optional all-staff in-person meeting in June 2023 (and this year too).  
* For the in-person board and staff meetings, we have selected locations that minimize the overall amount of travel and maximize direct flights.   
* We have maintained our country focus for in-person local meetings supported by regional Ambassadors.  
* We met our goal of keeping total travel and meeting expenses below 60% of 2019 costs even though we have more staff and membership growth has continued. The amount of money spent is a rough proxy for our carbon impact.    
* We no longer have an office in Oxford and will not renew the lease on our Lynnfield, MA office, so we will have no physical offices by the end of 2024\. This is not a large carbon emission reduction and is more a result of being a “distributed first” organization with staff in 11 different countries.    
* We recorded data on staff travel (flights, trains, cars, hotels) for 2023 to use as a baseline for comparison with future years. In 2023 the carbon emissions from travel and meetings was about 105 tCO2e.  
* We used tools provided by Amazon Web Services (AWS) and Zoom to estimate the impact of these services. In 2023 this was 0.266 tCO2e for AWS and .1 tCO2e for Zoom.  

### Some challenges

* Compiling data is difficult and time-consuming for a small organization  
* There are many different calculators and metrics to use and it’s difficult to decide which to use and how much detail to go into  
* We haven’t yet estimated the carbon footprint of staff home working  
* We were able to calculate the emissions from AWS but not our data center  
* We didn’t estimate the emissions from our offices. We had a small office in Oxford until November 2023, and we have an office near Boston \- we won’t be renewing the lease in 2025 so won’t have any offices.   

### Total travel and meetings spending

| Year          | Amount      | Percentage of 2019 |
|---------------|-------------|--------------------|
| 2019 actuals  | $585,482    | 100%              |
| 2020 actuals  | $91,700     | 16%               |
| 2021 actuals  | $19,066     | 3%                |
| 2022 actuals  | $74,416     | 13%               |
| 2023 actuals  | $305,737    | 52%               |
| 2024 budget   | $333,500    | 56%               | 


We have recorded carbon emissions from travel at about 105 tCO2e, so we will compare 2023 with future years. Now that we have started collecting travel data, it will be easier—staff can do it as they travel throughout the year. 

Our Executive Director, Ed Pentz, looked at his personal and work flights and the carbon emissions in 2019 were 18 tCO2e and in 2023 were 2.7 tCO2e so this is a big change in the right direction. 

### Hosting services

We use AWS for hosting our REST APIs, Crossref Metadata Search, the website, and Labs projects. Our main metadata registry is still in a data center, which is not included in this calculation. For 2023 Amazon reports Crossref’s carbon emissions were 0.216 tCO2e compared with 0.266 tCO2e in 2022\. Crossref is planning to move out of the data center and fully to AWS by the end of 2024 so this will increase our AWS usage and therefore our emissions from related activities will increase. Compared to travel, the footprint from AWS is minimal. 

### Online meetings

As a distributed, remote-first organization Crossref is a heavy Zoom user –– it’s essential for staff and for engaging with our community. However, Zoom doesn’t provide tools or estimates of the carbon impact of Zoom meetings. We used [a tool provided by Utility Bidder](https://www.utilitybidder.co.uk/business-electricity/zoom-emissions/), which makes a lot of estimates and assumptions. In 2023 Crossref had almost 800,000 meeting minutes. This translated into an average of 1.92 kg of CO2 emissions per week, or 100 kg per year. 

Some studies have estimated that turning off video reduces the carbon footprint of meetings. However, this can be a false savings since video is often important for creating a connection and having a productive meeting, and a Zoom meeting with video is still much, much better than traveling, particularly if flying is involved. 

### Tools we used

In order to calculate emissions for flights and train journeys, we chose to use [Carbon Calculator](https://www.carbonfootprint.com/calculator.aspx). We didn’t calculate emissions from hotel stays but looked at the [Hotel Footprinting tools](https://www.hotelfootprints.org/) and may add hotels to calculations in the future.  

### Offsetting

We don’t offset our emissions from travel or other operations and don’t have plans to do this. [Offsetting emissions is problematic in a number of different ways](https://en.wikipedia.org/wiki/Carbon_offsets_and_credits#Assuring_quality_and_determining_value) so we don’t feel confident in doing it. 

We did tree-planting as a “thank you” for the time of respondents in our metadata survey. Intended as an alternative to more commercial types of incentives rather than off-setting for our emissions, this resulted in 921 trees planted for the [Gewocha Forest, Ethiopia](https://ecologi.com/projects/restoring-degraded-land-in-ethiopia) via Ecologi.

### Wrapping up

Moving forward, we’ve learned a lot over the last couple of years. Collecting accurate data is challenging and time-consuming, especially for a small organization. For us, this has been a new lens for viewing our activities, and it remains a true learning journey and we have made permanent changes. In 2024 and beyond we are going to continue to follow our travel, meetings, and events policies that we announced in 2022. We will continue to capture our air travel emissions, and in 2025 we will more accurately capture train journeys and hotel stays. We will also continue calculating our Zoom and AWS emissions as best as we can. What we've learnt in the process of capturing and calculating our 2023 emissions helped us set things up to enable more prompt reporting on these impacts in the future. 

We expect that many of our members and our community at large assess their environmental impact or are embarking on similar projects, to understand and curb emissions. We’re keen to discuss this and learn together to reduce our environmental impact as an organization. 
