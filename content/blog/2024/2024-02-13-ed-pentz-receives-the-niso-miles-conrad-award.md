---
title: 'Ed Pentz accepts the 2024 NISO Miles Conrad Award'
author: Rosa Morais Clark
draft: false
authors:
  - Rosa Morais Clark
date: 2024-02-13
categories:
  - Community
  - Staff
  - Strategy
  - GEM
archives:
    - 2024
---
Great news to share: our Executive Director, Ed Pentz, has been selected as the 2024 recipient of the Miles Conrad Award from the USA's National Information Standards Organization (NISO). The award is testament to an individual's lifetime contribution to the information community, and we couldn't be more delighted that Ed was voted to be this year's well-deserved recipient.

{{% divwrap align-right %}}
{{< figure src="/images/staff/ed17-720px.png" width="100%">}}
{{% /divwrap %}}

During the [NISO Plus conference](https://niso.plus) this week in Baltimore, USA, Ed accepted his award and delivered the 2024 Miles Conrad lecture, reflecting on how far open scholarly infrastructure has come, and the part he has played in this at Crossref and through numerous other collaborative initiatives.

Established in 1965, the Miles Conrad Award gives recognition to those who've made substantial contributions to the information community over a lifetime. Named after the founder of the National Federation of Abstracting and Indexing Services (NFAIS)—an association that since merged with NISO—the award encourages innovation in content management and dissemination. Over the years, leaders and innovators who have significantly influenced the field of information exchange have been honored with the award. Ed has joined [an illustrious group](https://www.niso.org/awards/MCA)!

Ed’s leadership in collaboration and diplomacy has led to Crossref's success in making research objects more accessible and useful to a wide global audience, including publishers, researchers, funders, societies, libraries, and more. Crossref's founding purpose is stated as:

> “To promote the development and cooperative use of new and innovative technologies to speed and facilitate scientific and other scholarly research”.

Acknowledging his privilege as a Western, university-educated, white man, which he comments has helped his career, Ed prioritises collaboration, open communication, teamwork, and equity in creating a positive, trusted environment that has brought together a diverse team of 49 colleagues from 11 countries. The organisation’s culture allows everyone to grow and contribute to the mission of a connected [research nexus](/documentation/research-nexus) by including and developing solutions for community members across the globe.

Before his journey with Crossref, Ed held a number of roles at Harcourt Brace, including launching Academic Press's first online journal. This experience led to his involvement with the DOI-X pilot project, which became the foundation for Crossref. Since its launch in 2000, under his leadership, Crossref has become an important component of the research ecosystem, an open scholarly infrastructure with nearly 20,000 members across more than 150 countries. Crossref is now the main source of >155 million records about all kinds of research objects and this open metadata registry is relied upon by thousands of tools and services across the whole research system.

Ed’s influence is also evident throughout the wider world of open scholarly infrastructure; aside from establishing Crossref, he co-founded [ROR](https://ror.org/) and was a founding member of [ORCID](https://orcid.org/), where he also served as board Chair. Further, he has engaged with the community by holding various advisory positions, including the [DOI Foundation](https://www.doi.org/), the Digital Object Naming Authority (DONA), and the Coalition for Diversity in Scholarly Publishing (C4DISC).

Ed also emphasised that the long-term success of community initiatives lies in patience and the ability to agree on high-level principles of purpose and governance, which oil the wheels of collaboration, encourage participation, and enable more progressive change that builds and lasts over time. He says, "to solve collective problems it takes collaboration and diplomacy, bringing together a group of stakeholders, balancing their different concerns, building trust, and reaching consensus."

The adoption of the [Principles of Open Scholarly Infrastructure (POSI)](https://openscholarlyinfrastructure.org), along with (so far) 14 other organisations, was a key [turning point for Crossref](/categories/posi/), Ed said, and one which has already paved the way for more openness of key metadata for the community, including references and retractions, as well as closer partnerships with many of the other POSI adoptees, given their shared understanding and experience.

Referencing the current "peak hype" around artificial intelligence (AI), Ed points to the challenge of research integrity and the "growing field of science sleuthing" as a forthcoming area that Crossref and open metadata may help tackle at scale, including through Crossref's [Integrity of the Scholarly Record (ISR) Program](/categories/research-integrity) and---of course---community-wide collaboration.

In concluding his talk, Ed describes his hopes and dreams for scholarly communications in the future. He would like to see more balance in diversity in the leadership of open scholarly infrastructure, extended integrations among the various foundational infrastructures, and a fully connected system where the scholarly record is inclusive globally.

#### Ed, on behalf of all your proud colleagues at Crossref, thank you and congratulations!