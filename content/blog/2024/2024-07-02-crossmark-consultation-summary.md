---
title: 'Crossmark community consultation: What did we learn?'
author: Martyn Rittman
authors:
  - Martyn Rittman
  - Madhura Amdekar
draft: false
date: 2024-07-02
categories:
  - Crossmark
  - Community
archives:
    - 2024
---

In the first half of this year we’ve been talking to our community about post-publication changes and Crossmark. When a piece of research is published it isn’t the end of the journey—it is read, reused, and sometimes modified. That's why we run Crossmark, as a way to provide notifications of important changes to research made after publication. Readers can see if the research they are looking at has updates by clicking the Crossmark logo. They also see useful information about the editorial process, and links to things like funding and registered clinical trials. All of this contributes to what we call the [integrity of the scholarly record](https://www.crossref.org/blog/isr-part-one-what-is-our-role-in-preserving-the-integrity-of-the-scholarly-record/).

{{% imagewrap center %}}
{{< figure src="/images/blog/2024/Crossmark-popup-example.png" alt="The Crossmark popup provides information about whether a work is current and other metadata about integrity of the scholarly record." width="75%" class="img-responsive" >}}
{{% /imagewrap %}}

Crossmark has been around a long time and the context around it is constantly changing. It last had a major update in [2016](https://www.crossref.org/blog/get-ready-for-crossmark-2.0/) and in [2020](https://www.crossref.org/blog/encouraging-even-greater-reporting-of-corrections-and-retractions/) we removed fees for its use. 

The past few years have seen a more intense focus on research integrity among the scholarly communications community, leading to more retractions and calling out large-scale manipulation of editorial processes. At the same time, we haven’t seen an increase in the uptake of Crossmark, which is still used by only a minority of our members. We would like to know why the uptake is low and whether there is more we can do in this area. To dig into this, in the first part of 2024 we reached out to members of our community.

## What did we do?

We wanted to learn about attitudes towards Crossmark and related aspects of research integrity. This was done in several ways:

- Structured interviews with eight of our members.
- Round tables at Crossref LIVE events in Bogota and Nairobi
- Surveying a selection of our members, which led to 94 responses. 

The topics we asked about were related to how post-publication updates are made and communicated, and which metadata demonstrates good practice. 

We are extremely grateful to the members who contributed. They provided valuable feedback and have helped to shape the future of Crossmark and our approach to the integrity of the scholarly record.

## What did we find?

Across the various groups there were a few common themes, which fell into several areas.

1. Communication of updates is highly valued, and seen as the most important role that Crossmark can play. Some of those we spoke to would like readers to see if there is an update as soon as a page opens, without having to open a popup. This could be done by having a logo that changes colour, shape, or size.

1. Conversely, not as much enthusiasm was shown for the metadata assertions. These are additional fields that can be displayed to readers in the Crossmark popup. There wasn’t a strong consensus on which commonly-made assertions are the most important for research integrity. 

1. There is diversity in attitudes towards making updates to published works, what research integrity means, and approaches to workflows for updates. Even within a single organisation, a number of different workflows and multiple staff members might be called on to update published research. This makes things complex and means that it can be difficult to fit Crossmark in.

1. There are technical challenges to getting started with Crossmark. Those responsible for implementing Crossmark are often technical staff who struggle with the documentation we provide in English. There is also no plugin for OJS, a widely-used open source editorial software. It is more difficult to deposit Crossmark metadata for books than journal articles, and many article types don’t permit Crossmark metadata at all. On the other hand, those who successfully installed Crossmark found it easy to use and low-maintenance.

Overall, it seems that Crossmark still has an important role to play but there are changes and improvements we can make.

## What’s next?

Here are the main areas we intend to follow up on in the coming months. 

### Implementation

We need to look at how to make implementation more straight-forward. Can we provide multilingual documentation, plugins, run workshops or webinars, or make changes to Crossmark to lower the barrier to entry?

### Understanding workflows

Can we collaborate with our members and other organisations to reach a better understanding of how to update published works? Are there alternative workflows we need to support? Have we made it too difficult to understand and implement the options we currently have? 

While updates are always likely to be rare, we want to help members understand the benefits of making them. We talked to some members who were proud of never having published a retraction or correction, which left us wondering whether they are missing legitimate opportunities to correct the scholarly record. We also know that for some members and many work types (preprints, for example), updates are made without a separate published notification. Can we better understand the role that the published updates play and communicate updates even if there isn’t a published notice?

### Ongoing feedback

Clearly one size doesn’t fit all when it comes to implementing and communicating updates. We need to find ways of keeping in touch with the community to test new solutions with as broad a range of members as possible. We want to avoid catering to a minority and leaving others struggling to find ways to implement a solution.

### Custom metadata?

Is there an ongoing need for metadata assertions? Many of the assertions currently made are possible as standard metadata and others could be included in our deposit schema. We want to consider removing the option to add assertions. This needs more feedback from the community, especially those who currently make use of assertions.

### Redesign the UI

Crossmark doesn’t have the recognition with readers we would like. Is there a way we can redesign it to make it more associated with Crossref and accurate metadata? We intend to explore different designs, and test them with members and readers. 
