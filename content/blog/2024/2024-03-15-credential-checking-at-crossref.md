---
title: 'Credential Checking at Crossref'
author: Martin Eve
draft: false
authors:
  - Martin Eve
date: 2024-03-15
categories:
  - Tools
  - Authorization
  - Labs
archives:
    - 2024
---

{{% divwrap align-right %}}
{{< figure src="/images/blog/2024/credential-checking.png"  width="75%" >}}
{{% /divwrap %}}
It turns out that one of the things that is really difficult at Crossref is checking whether a set of Crossref credentials has permission to act on a specific DOI prefix. This is the result of many legacy systems storing various mappings in various different software components, from our Content System through to our CRM.  

To this end, I wrote a basic application, [credcheck](https://gitlab.com/crossref/labs/credcheck), that will allow you to test a Crossref credential against an API.

There are two modes of usage. First, a command-line interface that allows you to run a basic command and get feedback:

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Usage: cli.py [OPTIONS] USERNAME PASSWORD DOI

Second, you can use it as a programmatic library in Python:

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; import cred  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; credential = cred.Credential(username=username, password=password, doi=doi)

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; if not credential.is_authenticated():  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;     ...

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; if credential.is_authorised():  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;     ...

The tool splits down authentication (whether the given username and password are valid) and authorisation (whether the valid credentials are usable against a specific DOI/prefix).

For technical information, the way this works is by attempting to run a report on the specific DOI in question and then scraping the response page. We hope, at some future point, that there will be a real API for this, but for now this solves the problem as a bridge.  

