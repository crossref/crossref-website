---
title: 'Seeking consultancy: understanding joining obstacles for non-member journals'
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
date: 2024-05-01
categories:
  - Request for Information
  - Fees
  - Sustainability
  - Membership
archives:
    - 2024
---
Crossref is undertaking a large program, dubbed [&#39;RCFS&#39; (Resourcing Crossref for Future Sustainability)](/community/special-programs/resourcing-crossref/) that will initially tackle five specific issues with our fees. We haven’t increased any of our fees in nearly two decades, and while we’re still okay financially and do not have a revenue growth goal, we do have inclusion and simplification goals. [This report from Research Consulting](/pdfs/research-consulting-rcfs-report-public.pdf) helped to narrow down the five priority projects for 2024-2025 around these three core goals:

## Scope of the RCFS Program 2024-2025

{{% divwrap green-highlight %}}

#### GOAL: MORE EQUITABLE FEES

1. **Project 1**: Evaluate the USD $275 annual membership fee tier and propose a more equitable pricing structure, which might entail breaking this down into two or more different tiers.
2. **Project 2**: Define a new basis for sizing and tiering members for their capacity to pay

#### GOAL: SIMPLIFY COMPLEX FEES

3. **Project 3**: Address and adjust _volume_ discounts for Content Registration
4. **Project 4**: Address and adjust _backfile_ discounts for Content Registration

#### GOAL: REBALANCE REVENUE SOURCES

5. **Project 5**: Reflect the increasing value of Crossref as a metadata source, likely increasing Metadata Plus fees

{{% /divwrap %}}

## Work to date

As part of the RCFS program, we are working closely with our Membership & Fees Committee to discuss insights, gather feedback, and make recommendations to the Board. As a first step, we have surveyed and received responses from around 1000 of the current 8000 Crossref members in our lowest membership fee tier (USD $275). We are now starting to distill that data and will discuss it on our [community call on May 8th](/events/the-shape-of-things-to-come) and subsequently with the M&F Committee to inform recommendations for fee changes that may going into effect in 2025 or 2026.

## Request For Information (RFI) about community consultation project

While we have useful data from existing Crossref members, we know that there are many thousands of journals that are not (yet) members, and we need to understand this group better, in particular, to document and address the financial obstacles as well as the technical or social challenges.

> We are looking for community facilitation expertise, with multiple language skills, to conduct a series of focus groups with non-member journals, with a summary and insights report (in English) provided by the end of June 2024.

All the data and documentation will be available publicly on the dedicated [RCFS Program website](/community/special-programs/resourcing-crossref/)

As well as designing, conducting, and summarising the results of some focus groups (participants for which will be gathered via our own contacts and those of partners such as DOAJ, EIFL, and the Free Journal Network) we would like the consultant to review work such as the [DIAMAS institutional publishing report](https://diamasproject.eu/diamas-results-institutional-landscape-survey/), and identify data relevant to Crossref’s fee model.

If you would like to respond, please provide the following information and send it to Kora Korzec at [feedback@crossref.org](mailto:feedback@crossref.org) by **15th May**:

* Your consultancy organisation and your role within it
* Examples of similar market research undertaken
* Languages spoken within your team
* Confirmation that the timeline is workable
* Approximate fee, likely range, or structure/basis for your fee

Equally, if you represent a journal or group of journals, such as Diamond Open Access journals, and are not yet using Crossref, please get in touch and we can include your group in the research.

Thank you!
