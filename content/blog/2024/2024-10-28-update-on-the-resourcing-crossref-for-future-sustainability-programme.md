---
title: 'Update on the Resourcing Crossref for Future Sustainability research'
slug: "update-rcfs"
author: Kornelia Korzec
draft: false
authors:
  - Kornelia Korzec
  - Amanda Bartell
  - Ginny Hendricks
  - Lucy Ofiesh 
  - Ryan McFall 
date: 2024-10-28
categories:
  - Strategy
  - Fees
archives:
    - 2024
---

We’re in year two of the [Resourcing Crossref for Future Sustainability](/community/special-programs/resourcing-crossref/) (RCFS) research. This report provides an update on progress to date, specifically on research we’ve conducted to better understand the impact of our fees and possible changes. 

Crossref is in a good financial position with our [current fees](/fees/), which haven’t increased in 20 years. This project is seeking to future-proof our fees by: 
* Making fees more equitable
* Simplifying our complex fee schedule
* Rebalancing revenue sources

In order to review all aspects of our fees, we’ve planned five projects to look into specific aspects of our current fees that may need to change to achieve the goals above. This is an update on the research and discussions that have been underway with our [Membership & Fees Committee](/committees/membership-and-fees/) and our [Board](/board-and-governance/#officers), and what we’ve learned so far in each of these areas.

## Goal 1: More equitable fees. 

To ensure our fees going into the future are more equitable, we’re carrying out two parallel projects: evaluation of the lowest membership tier, and the review of the basis for deciding the membership tiers and distribution of membership across them.

### Project 1: Evaluate the lowest membership tier and propose a more equitable pricing structure.

All Crossref members pay an annual membership fee. These fees are tiered, and different members pay a different fee depending on the annual publishing revenue that their organisation receives (or publishing expenses if they don’t receive any publishing revenue).

We entered into this project recognising that we have too many membership tiers and the definition we use to size members is not consistent and can be confusing (e.g. different basis for funders than other organisations, and both are different still from subscribers to our Metadata Plus service). The idea of the membership tiers was to use publishing revenue as a proxy for “ability to pay”. We really want to develop proposals for a more equitable pricing structure. However we don’t know enough about our members’ capacity to pay to be able to model an alternative approach.

Our current lowest fee tier is $275 (USD) for any organisation with annual publishing revenue (or publishing expenses where the organisation doesn’t receive publishing revenue) of $0 to $1 million, and this is the tier where we focus our attention in our first project of the RCFS program. The difference between an organisation with revenue or expenses of US$0, and an organisation with revenue or expenses of US$1 million, is huge. Hardly any new members have joined in any other tier in the past several years. Of the 21,000 active members, more than 20,000 fall into the US$275 tier - either directly (as an independent member) or indirectly (through a sponsor, where their fees would be lower). A fee structure that would fit better with the realities of our community might entail breaking our current $275 fee tier down into two or more more granular tiers. 

At the moment, the majority of Crossref’s revenues come from the bottom membership tiers; 65% of membership revenues come from organisations in the $275 tier. We also know that many of those members (86%) are paying more in membership dues than in content registration, whereas other members have the inverse relationship between annual dues and content registration. Overall, the members in the $275 tier contributed 34% of Crossref’s revenue last year, and the members in the >$50mln tier – contributed 29%. 

### Members’ survey
Between April and May this year, we surveyed all independent members in the $275 tier. We asked questions about their operating size, how they’re funded, and how Crossref’s fees affect them. At the time of the survey, there were 8,027 members in this category. We received 1,054 responses; with a 13% response rate and broad representation globally, we are confident in the sample size. One-third of respondents said they were part of a larger organisation (such as a department or a library in a research institution). 

Chart 1: Organisation revenue or funding
The majority of respondents in this category (65%) have annual revenue or expense of less than 100,000 USD; with 48% operating with less than 10,000 USD.


{{% imagewrap center %}}
{{< figure src="/images/blog/2024/update-on-rcfs-2024-10-28/income.bik.png" alt="Responses to the question about the income or funding levels in the members survey" width="50%" class="img-responsive" >}}
{{% /imagewrap %}}


Chart 2: Sources of funding
When asked about the sources of funding (as an indicator of how stable these organisations might be and how readily accessible their funding is) the most frequent answer was public or government funding, and then article processing charges. If organisations relied on two sources of funding, the most common combination was public funding and article processing charges, and it was relatively rare for these organisations to have multiple sources of funding.

{{% imagewrap center %}}
{{< figure src="/images/blog/2024/update-on-rcfs-2024-10-28/sources.funding.top.20.png" alt="Responses to the question about the main sources of income for independent members in the $275 tier" width= "100%" class="img-responsive" >}}
{{% /imagewrap %}}

Chart 3: What percentage of expenses do you spend on Crossref fees?
{{% imagewrap center %}}
{{< figure src="/images/blog/2024/update-on-rcfs-2024-10-28/feestocrossrefwithregions.png" alt="Responses to the question about the proportion of overall expenses paid in Crossref fees by independent members in $275 tier"  width= "100%" class="img-responsive" >}}
{{% /imagewrap %}}

The majority (61%) of respondents spend less than 5% of their expenses on Crossref fees. However, we have also learnt that for some volunteer-run publications, Crossref fees might be some of the only expenses they incur. Interestingly, the percentage of expenses spent on Crossref is fairly consistently spread across the continents.

### Project 2: Review the basis and distribution of membership tiers

This project examines options for how we define the capacity to pay, how members are distributed across tiers, and the right levels of member fees. 

There are currently a range of prices for our annual fees, based on an organisation's ability to pay. We have used the metric of annual publishing expense or revenue as an indicator of that ability, but in some cases it doesn’t apply. As per our [fee principles](/operations-and-sustainability/#fee-principles), we have not differentiated between organisation types. Nonprofit and commercial entities pay the same price (caveat: research funders still have a separate fee schedule, but that was intended to be temporary). 

We conducted a review of other annual fee models to benchmark our approach against six like-minded organisations working in the context of scholarly communications and infrastructure. We looked at whether these organisations based their fees on one more more of the following:
* Volume: e.g., research output, # of journals
* Budget: e.g., total annual revenue or expenses
* Relevant budget: e.g. publishing revenue
* Organisation type: e.g. variance in fee based on publisher, institution, or funder
* Country-level economic data: e.g., discounting based on World Bank classification, discounting based on purchasing power calculation.

Chart 4: Annual fee schedules comparisons between Crossref and CORE, DOAJ, Dryad, OA Switch-board, OpenCitations and ORCID. 

{{% imagewrap center %}}
{{< figure src="/images/blog/2024/update-on-rcfs-2024-10-28/annual-fee-schedules-comparisons.png" alt="Annual fee schedules comparisons between Crossref and CORE, DOAJ, Dryad, OA Switch-board, OpenCitations and ORCID"  width= "100%" class="img-responsive" >}}
{{% /imagewrap %}}

There are three consistent themes among our peers: the total annual revenue and volume levels are the most common basis for membership fees among other organisations, and almost all offer discounted fees to accommodate country-based economic circumstances, utilising World Bank’s data (this is currently achieved at Crossref via the [GEM program](/gem/), which we have full intention of incorporating into our future fees whatever other decisions we might take). Only one other organisation uses publishing revenue or expenses as a basis for annual fees, while the potentially more transparent and less ambiguous data point of the total revenue factors in three other annual fee models. 

For subscribers to our [Metadata Plus service](/fees/#metadata-plus-subscriber-fees), the fee tier is selected based on whichever is the higher between their total annual revenue (including earned and fundraised, e.g. grants) or annual operating expenses (including staff and non-staff, e.g. occupancy, equipment, licences etc.). At present, we have limited understanding of the budgets of our members and how this may compare to their publishing revenues or expenses. We are looking to learn more about this as part of our annual membership data checking process, where we email all our members to ask them to confirm contact details for their organisation and the staff involved in managing their Crossref account. This year, we’re also asking all members about their organisation’s annual operating budget (or planned annual expenses) to help inform our discussions. In our case, the volume of outputs (in this case the number of items and associated metadata registered with Crossref) is recognised by the registration fees mechanism.

## Consulting with organisations outside Crossref membership
To help us inform how our fees can be more equitable, it’s important to invite voices of organisations that may currently be unable to join us - due to fees or technical barriers. We hope that learning more about their circumstances will help us make sure that we improve accessibility of Crossref membership to all organisations that publish scholarly and professional works. We commissioned Accucoms to carry out a consultation on our behalf. 

So far, from a handful of interviews with publishers from Nigeria, DRC, Canada and USA, we’ve learnt that while virtually all offer open access to their publications, the majority has no publishing income, and where the income is derived via APCs it’s modest and only applicable in rare circumstances. Through institutional funding and/or grants, these organisations have modest operational budgets, yet our respondents lacked clarity over the particulars. In terms of participation in professional networks and international publishing organisations, only one of the organisations we interviewed participates in DOAJ, and another is a member of OASPA, in both cases their participation is free. Among the interviewees, two organisations were interested in Crossref membership in the past but encountered technical barriers to joining.

With only five interviews to date, [the consultation is still open](https://survey.alchemer.com/s3/7879005/673ef7e88ae5) and we’re keen to hear from more organisations that are not Crossref members but have considered our membership at some point. 

## Goal 2: Simplify complex fees
### Projects 3 & 4: Review volume and backfile discounts for Content Registration

Along with our membership fees, our members also pay usage-based registration fees for records (scholarly works and grants) they register with us. Different content types render different costs for our members, and the fees are subject to discounts related to the age of publication and volume of registrations. Records for items older than two years have a lower fee associated with them, to help incentivise registration of such “backfile” materials with great gains for the [Research Nexus](/documentation/research-nexus/). There are also discounts related to the volume of transactions – which again depend on the content types. 

These discounts are intended to encourage certain behaviours, specifically encouraging members to register older records in large quantities to better complete the scholarly record. Not all content types have backfile or volume discounts, and the rate of discount varies. This creates quite a complex system of fees. To the extent that the discount is successful in encouraging this behaviour, we want to preserve it, but in many cases these discounts see little to no activity. 

Following the discussions of the [Membership and Fees Committee](/committees/membership-and-fees/), chaired by Vincas Grigas, Vilnius University, we are preparing to consult with the small number of members who currently receive volume discounts to discuss what the impact would be if we removed them. 

We plan to identify and preserve the well-used backfile discounts, which encourage registration of old content, such as books, journal articles, grants. However, there are types of discounts that are hardly ever used and we are considering removing these to simplify the fees. This work will focus on the technical implications of removing some of the underused backfile discounts from the billing code and consulting with members to understand any impact . 

## Goal 3: Rebalance revenue sources
### Project 5: Reflect increase in metadata usage and perceived shift of value toward metadata distribution

All Crossref metadata is made freely and openly available to everyone. However, some organisations may be looking for a service level agreement in delivery of the metadata, plus more regular snapshots and priority service/rate limits. For those organisations, we have an optional Metadata Plus service. 


The final project is looking at the fees for this service. We are interested in making sure that Crossref metadata is available and used by the community where it can contribute to their objectives – related to discovery, analysis, integrity, and more. The optional paid service we offer aims to support the external tools that facilitate business and scholarly processes for the community. We are heartened to see that the appetite for the use of metadata seems to be growing, and the value of open research information is increasingly and widely recognised. We want to ensure that the users of metadata contribute proportionally to the maintenance of the records created and curated by our members. 

## Conclusion

At this point, most projects generate a lot of questions and the work is underway to deliver answers related to capacity to pay, discounts as well as available metadata usage, and barriers faced by organisations in our community. 

What we have found so far is that two of our goals – simplification and equity –  are often at odds with each other, and this is especially true with the $275 tier. 

We welcome comments, suggestions and questions.
