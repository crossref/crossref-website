---
title: 'The Lammey Effect'
author: Crossref
draft: false
authors:
  - Crossref
date: 2024-02-16
categories:
  - Community
  - Staff
archives:
    - 2024
---
We’re equally sad and proud to report that [Rachael Lammey](/authors/rachael-lammey) is moving on in her career to the very lucky team at 67Bricks. Her last day at Crossref is today, Friday 16th February. Which is too soon for us, but very exciting for her!

It’s hard to overstate Rachael's impact on Crossref’s growth and success in her 12 years here. She started as a Product Manager where she developed that role into a broad and central function, and soon moved into the newly-formed community team as International Outreach Manager where she grew important programs such as Sponsors, Ambassadors, a series of ‘LIVE’ events around the world, and she went on to manage her own team and establish some of the most important strategic relationships that Crossref now feels fortunate to have.

{{% divwrap align-right %}}
{{< figure src="/images/blog/2024/rachael/rachael-b&w.jpeg"  width="100%" >}}
{{% /divwrap %}}
Rachael was a significant part of the growth and adoption of new initiatives such as Crossmark, Similarity Check, the REST API, preprints, grants, data citation, and ROR. She's contributed to numerous organisations such as EASE, ALPSP, SSP, ISMTE, STM, and most recently co-Chaired the NISO working group on retractions and corrections.

As Head of Strategic Initiatives, and most recently, Director of Product, Rachael has shown dedication and leadership, supporting and strengthening not just her own teams but all of us across the organisation, encouraging us to do better while being one of the easiest people to work with.

The '[butterfly effect](https://en.wikipedia.org/wiki/Butterfly_effect)' is the notion that the world is deeply interconnected and that one small occurrence can influence a much larger complex system. Rachael embodies that notion, having created positive ripples and waves---and certainly many connections---in the scholarly record, in our organisation, and across the community.

## Messages from colleagues

{{% quotecite "Ryan" %}}
Rachael, I was saddened when I first heard the news that you were moving on to another opportunity. Your professionalism, work ethic, and positive attitude have been inspirational to work around. I have enjoyed the opportunities we have had to collaborate. As you move on to a new experience I wish you success and happiness in your future endeavors. Your presence will be missed at Crossref! Best Wishes.
{{% /quotecite %}}

{{% quotecite "Obanda" %}}
I will miss you, Rachael. It has been great working with you for the few months that I have been at Crossref. I also cannot forget kayaking together with you and capsizing on the return to the shore, but almost professionally recovering. We would have made the best team this time around. I wish you all the best and many wins in your new role.
{{% /quotecite %}}

{{% quotecite "Sara" %}}
I feel like the luckiest human to have worked with Rachael over the last 4 years. She’s the perfect mix of smart and funny and knowing how to get things done. Rachael is a big part of what makes Crossref culture so special — I’ve never felt so supported in a role as when Rachael was my manager and for that I am very grateful. I will miss her wit and humor and her pragmatic approach to work and life!
{{% /quotecite %}}

{{% quotecite "Susan" %}}

{{% divwrap align-left %}}

{{< figure src="/images/blog/2024/rachael/rachael-live-brazil.jpg" alt="Team at Crossref LIVE event in Brazil, 2016" width="100%" >}}

{{% /divwrap %}}

One of my first ‘Crossref LIVE’ events was with Rachael in Brazil in 2016. At the time, my role mostly focused on membership, and we had just started working more closely with ABEC, a large organisation in Brazil that sponsored quite a few members. Rachael managed the sponsor program then and thought this would be a good opportunity to collaborate with a sponsor on an event, and she asked me to join her.  There is so much planning for these - venues, local partners, presentations, meetings - and she had all the details in order and made the event such a success. Rachael was supportive, encouraging, and I learned so much from working with her.  The Brazil trip was such a positive experience that I realised I wanted to focus more closely on community engagement. Rachael encouraged me to do so.

{{% divwrap align-right %}}
{{< figure src="/images/blog/2024/rachael/rachael-indonesia.jpg" width="100%">}}
{{% /divwrap %}}

She and I went on to partner on more LIVE events together. Our time in Indonesia was perhaps one of the most memorable for me - as well as our LIVE event, we had an unexpected tour of Yogyakarta with our Indonesian hosts, involving a tour of Prambanan Temple (see photo below), batik fabric shopping, visiting a few universities, and a stop at our hosts’ home. All the while trying not to let the winding car ride and traffic get the better of us.  Our event the next day went perfectly, and I told her, half-jokingly, that the whole experience renewed my faith in humanity. Of note, we also drank the only bottle of wine available in the hotel bar.

Rachael was also my Crossref running buddy, and we spent quite a few miles together - in Brazil, NH, Maine, Oxford, and Spain.  During our runs, topics ranged from Game of Thrones to Idris Elba to sportsing, but not so much about work.  The next time I find myself in England, we will run a  few more miles together, followed by a pint. Thank you for everything!
{{% /quotecite %}}

{{% quotecite "Jon" %}}
Many have pointed out how talented, wise, or skilled you are and I certainly will not contradict a single word of it but that's not what comes to mind first for me. Those traits, while true, pale in comparison to the person you are. Your positive, bright demeanor and the way everyone always feels better just being around you. I have dreaded some meetings from time to time. But whenever I've been involved in something with you, I've always left feeling better than when I started (no matter how grumpy I may have entered). You have been a consistent bright light in the Crossref constellation and you will truly be missed.
{{% /quotecite %}}

{{% quotecite "Shayn" %}}
Rachael! You are the best at cutting through all the bulls**t to get at what really needs to happen and why!  Your knowledge is broad and deep, as is your institutional memory for all things Crossref and scholarly publishing.  And your unflappability in pretty much any context is admirable and inspiring.  We’ll miss you big time!  Wishing you all the success at 67Bricks and otherwise.
{{% /quotecite %}}

{{% quotecite "Laura" %}}
Hey Rachael - I’m happy to be writing this note of Congratulations!! to you, particularly because it would be awkward to explain this bit of verklempt I’m feeling. Our interactions have been limited, but my impressions of you are of confidence, calm, capability, and collegiality. Thanks so much for your work with the Billing team. I’m sorry we are losing you, and am also so glad to know that you are out there at the forefront of inspiring others elsewhere, not only in the work you do, but also how you go about it.
{{% /quotecite %}}

{{% quotecite "Rosa" %}}
{{% divwrap align-right %}}
{{< figure src="/images/blog/2024/rachael/rachael-uksg.jpg"  width="100%" >}}
{{% /divwrap %}}
Hey Rachael, Just a big THANK YOU for helping me out all this time. I've had so many questions, and you've always been there to answer them. I always knew I could count on you. Thanks for those heartening chats when I needed a boost, and for including me in webinars and recordings - it really helped me improve. Remember that funny mistake I made on a recording when I called us 'Rochael'?  We sure had a good laugh! I'm gonna miss those times and working with you. Can't wait to catch up with you over a drink the next time I'm in town. Wishing you all the very best and once again, thanks for everything! 
{{% /quotecite %}}

{{% quotecite "Michelle" %}}
I am happy we got to enjoy some delicious vegetarian/vegan meals and wine together. I guess I should also mention that I enjoyed recruiting, HR and business fun with you too. Thank you for being such a big part of Crossref for 12 years! Have fun conquering your new chapter. Congratulations!
{{% /quotecite %}}

{{% quotecite "Esha" %}}
Rachael! You will be missed. I have really enjoyed our chats and work together. I will miss our wide ranging talks about food, books, and your descriptions of all the sportsing, which I would admire because I can barely manage a short run. :-) Thanks so much for being you and let’s stay in touch! Congratulations on your new endeavor, you’re going to be great.
{{% /quotecite %}}

{{% quotecite "Tim" %}}
When Rachel joined Crossref she brought a lot of enthusiasm and interest in learning about all that we were doing and also about what we could do. Her ideas and engaging leadership are wonderful for creating interest and drive to make projects happen. It has been wonderful to work with her over the 12 years here. I always look forward to seeing her and hearing what she has been doing outside of Crossref as well as inside. I will miss her but I know she will be doing great things wherever she may be.
{{% /quotecite %}}

{{% quotecite "Patrick P" %}}
We’ve had a number of opportunities to reminisce, gassing each other up about how great it has been to work together, so I won’t do too much more of that here. But we will continue building on all of your contributions at Crossref and will carry forward your truth-telling and problem-solving approach to the work we do here. Best of luck with all the future has to offer, and we will certainly miss having you on the team.
{{% /quotecite %}}

{{% quotecite "Ed" %}}
{{% divwrap align-left %}}
{{< figure src="/images/blog/2024/rachael/rachael-zoom.png"  width="100%" >}}
{{% /divwrap %}}
Rachael - I will miss you. I’ve really enjoyed working with you, hanging out while traveling, and getting recommendations on good books to read. Crossref won’t be the same without you. I think you have worked in the most different areas of Crossref and on the most projects of anybody, ever. Your commitment, professionalism and humour helped make Crossref what it is today. Your sportsing is also very impressive. All the best.
{{% /quotecite %}}

{{% quotecite "Paul" %}}
Not all heroes wear capes! Rachael defines that saying so much with her ethic of getting things DONE! I know she loves to get things done but the speed and quality in which that happens is second-to-none. Rachael will be massively missed at Crossref and 67Bricks don’t yet know what they have found. I enjoyed working with Rachael throughout my tenure at Crossref, she has helped me a huge amount in developing my programming skills and has always been encouraging throughout, especially with the 'toil-bashing’ which is substantial and overwhelming at times.

On a more personal note, she is a great drinking buddy and always motivates me to be more active… by making me feel lazy. The number of hours Rachael would work was crazy, but then I always thought that anyone who gets up that early to go for runs must be a little crazy!  AIl the best in your future endeavors and don’t be a stranger.
{{% /quotecite %}}

{{% quotecite "Ginny" %}}
{{% divwrap align-right %}}
{{< figure src="/images/blog/2024/rachael/rachael-park.jpeg"  width="100%" >}}
{{% /divwrap %}}
When I started at Crossref in March 2015—at the UKSG conference in Glasgow—Rachael was leading a workshop on text mining, showing off in full glory her ‘unicorn’ mix of skills from her technical knowledge of metadata and APIs to her facilitation techniques with a large group of people, clearly a community whose needs she knew inside out. Later that evening, Rachael took it upon herself to induct me in the ways of Crossref. One of the most important things she thought I should know was that we were all trusted and treated like adults - there was no micromanagement and I was to feel completely free to challenge the status quo. 
{{% divwrap align-left %}}
{{< figure src="/images/blog/2024/rachael/rachael-xmas.jpeg"  width="100%" >}}
{{% /divwrap %}}
After one of the first ‘LIVE’ events, in Vilnius, I realised that it was Rachael who had created and embodied that trusted vibe through her own approach. She has been entrusted with so many programs, projects, teams, and tricky situations. Almost every launch, release, announcement, or achievement at Crossref very likely had Rachael’s eye on it at some stage, certainly the ‘actually-getting-it-done’ stage. Our close working relationship over the last nine years grew into a great friendship and I’m not quite sure how I’ll feel when the reality sets in and she’s not here for a quick chat, always a reality check. Working with Rachael has been inspiring, exciting, reassuring, and hilarious (that dry 'Norn Iron' humour!). 67Bricks is so fortunate and I can’t wait to watch her help them go from strength to strength, just like she has done for Crossref. See you soon, Ranty Rachael, no doubt putting the world to rights over a bottle of Malbec and many eyerolls 🙄.
{{% /quotecite %}}

{{% quotecite "Adam" %}}
Although our time working together only overlapped the short span of two years, I appreciate how much of a champion you were for ROR and everything else you did at Crossref! I’m sure you’ll continue to do the same, among many other great things, in your new journey at 67 Bricks. You will be missed!
{{% /quotecite %}}

{{% quotecite "Amy" %}}
Rachael, It has been wonderful working with you!! You are truly a special person. I always looked forward to when we chatted over slack, had a call together, or got to spend time together in person. You are sure to do amazing things on your next adventure. You will truly be missed!! I hope we can stay in touch! Good luck, Rachael!!! Fondly, Amy.
{{% /quotecite %}}

{{% quotecite "Patience" %}}
I am happy I got to meet Rachael when I joined Crossref in December 2023. We spoke generally about the Products team at Crossref, the differences and similarities between the African and British culture and upcoming projects on automation. You were really patient towards explaining and providing great information on metadata and research. Thank you so much for  always responding swiftly to my requests pertaining to Finance issues. I have no doubt that you would be missed at Crossref and would keep doing great things into the future!!! Congratulations Rachael.
{{% /quotecite  %}}

{{% quotecite "Martin" %}}
I will greatly miss working with you, Rachael; you have been a stalwart of reliability and enthusiasm during my time at Crossref and the organization will not be the same without you. That said, of course, I wish you all the best of luck and success in your future endeavours!
{{% /quotecite %}}

{{% quotecite "Madhura" %}}
Rachael- Congratulations on this new opportunity, I am thrilled for you! I am also very sorry that our time at Crossref did not overlap much and I am grateful for all the chances I had of interacting with you (including being able to meet you in London recently)- you were always very helpful and kind to me. I am hopeful that our paths will cross again in the future. We will definitely miss you here, and I wish you all the best for all the exciting things ahead.
{{% /quotecite %}}

{{% quotecite "Amanda B" %}}
{{% divwrap align-left %}}
{{< figure src="/images/blog/2024/rachael/rachael-dog.jpg" width="75%">}}
{{% /divwrap %}}
My third week at Crossref back in 2017 was at the annual meeting in Singapore, and not getting into the timezone and not sleeping for 4 days was eased by our visit to a rooftop nightclub on the penultimate night - just before you headed off to Indonesia for a series of meetings with members and sponsors. I still don’t know where you get all your energy!

I’m so sorry you’re leaving - I’ll really miss your honesty, your approach to getting things done, and of course seeing Rosie on our zoom calls. Looking forward to seeing what’s next for 67 Bricks - exciting times!
{{% /quotecite %}}

{{% quotecite "Kora" %}}
Rachael, it’s been a pleasure to work with you. You’re always ready to help and ever full of information. We’ve only just got coordinated on the perennial challenge of timelines! You took things on and got them done, as you said. The world of schol comms won’t even know how much it has to thank you for, probably chiefly for seeing the Retraction Watch data acquisition through and opening it up for all. I will miss your honesty and energy, and the opportunity to challenge you again on the amount of food consumed in one sitting… I don’t think you’ll need luck in your next place, but I wish you that it is all you want it to be.
{{% /quotecite %}}

{{% quotecite "Maryna" %}}
I’m so glad to have met you in person over these couple of days in London shortly after I joined Crossref and it’s such a shame we didn’t have much time to work together more and spend more time (not working) together. Thanks for the introduction to the Scampi Fries - you’ve changed my life forever (for the best obviously)!
{{% /quotecite %}}

{{% quotecite "Joe" %}}
{{% divwrap align-right %}}
{{< figure src="/images/blog/2024/rachael/rachael-bonfire.jpg" width="100%">}}
{{% /divwrap %}}
Thank you for your collaboration and friendship over the past decade! You will be missed. We've worked on a long series of abbreviations, acronyms, and portmanteaus! Thanks for organizing countless things, from conference satellites to conference rooms. Your long record as fire warden was unblemished. 67bricks will benefit from your singular drive and attention to detail. All the best!
{{% /quotecite %}}

{{% quotecite "Amanda F" %}}
Rachael! One thing I admire most in a person is a facility with metaphor accompanied by the ability to see to the heart of a matter, and hoo boy do you have those qualities in spades. I remember so clearly your talk at the Crossref team meeting in Spain in 2023 in which you clarified the Big Picture for us all in an extremely enlightening way, and then, in a smaller but equally impressive achievement, casually mentioning in a Funder Registry meeting that funders should start "stretching and warming up" for the transition to ROR – boy did I latch on to that terrific image. I wish you all the best at 67 Bricks.
{{% /quotecite %}}

{{% quotecite "Dominika" %}}
{{% divwrap align-right %}}
{{< figure src="/images/blog/2024/rachael/rachael-force11-2019.jpeg"  width="75%" >}}
{{% /divwrap %}}
Rachael, thank you so much for all the support, patience, honesty, and determination. I will certainly miss our chats, work-related and non-work related. I wish you all the best in your new ventures!
{{% /quotecite %}}

{{% quotecite "Lena" %}}
Rachael - thank you for your boundless patience, generosity, and sense of humour. I’m very grateful I got to learn the Crossref ropes (cropes?) from you. Looking forward to randomly running into you on the Bristol karaoke circuit in 10 years’ time and performing an epic duet of Dancing in the Dark together. There’s a joke in there somewhere about you being the boss.
{{% /quotecite %}}

{{% quotecite "Maria" %}}
Rachael, Congrats on your new opportunity. You will be greatly missed here. Through the years we have only been at the same events in person a handful of times but I will always remember your amazing personality and sense of humor. I am thankful to have spent some time with you at 2020 PIDapalooza.
{{% /quotecite %}}

{{% quotecite "Isaac" %}}
Thank you, Rachael. Thank you. I know everyone is telling you that they’re sad to see you go (I am too; we all are). I keep thinking if I delay telling you that, maybe the day won’t come when you walk out the Crossref doors. But here it is. Just wanted to you to know that I appreciate you. I appreciate you pushing us forward. I appreciate you being an advocate for all things Crossref. We’ll all miss you. Best of luck at 67bricks!
{{% /quotecite %}}

{{% quotecite "Lucy" %}}
{{% divwrap align-right %}}
{{< figure src="/images/blog/2024/rachael/rachael-bar.jpeg"  width="75%" >}}
{{% /divwrap %}}
On one of our first meet ups together, I drove us from the Lynnfield office to Logan airport in rush hour, and we managed to survive the Bostonian road rage in one piece. We spent the ride talking through the intricacies of a sponsoring organization’s agreement. Rachael has been a safe set of hands and an encyclopedia of institutional memory for Crossref for 12 years.

Rachael is one of those people who’s as equally competent as she is a pleasure to work with. She’s an innate leader because people want to get behind her. She shows her depth of understanding while also inviting input from everyone in the room. I’ll miss our Zoom calls, our marathon Friday sessions, and our post-meeting pub visits.
{{% /quotecite %}}

{{% quotecite "Luis" %}}
Hello! Here's to hoping your new workplace appreciates you as much as you were here – they're lucky to have you. I only wish we had the chance to interact more. Many hugs!
{{% /quotecite %}}

{{% quotecite "Fabienne" %}}
{{% divwrap align-left %}}
{{< figure src="/images/blog/2024/rachael/rachael-pub.jpeg"  width="75%" >}}
{{% /divwrap %}}
Rachael, I will really really miss you, professionally and personally (but you know this already !). I'll miss all our work, dog, book and putting the world to rights chats. You'll be brilliant whatever you do and wherever you go (67Bricks have no idea how lucky they are !). Just keep 'getting stuff done' and have fun 😀
{{% /quotecite %}}

{{% quotecite "Martyn" %}}
You will be sorely missed but can be very proud of what you’ve done during your time at Crossref, I’m sure you’ll continue to have a big impact. You’ve always been a pleasure to work with: efficient, supportive, and always with a sense of fun and enjoyment. That’s probably one of the things that drew me to Crossref even before we worked together as colleagues. Thanks for the support and positivity you’ve brought on many, many occasions and best wishes for the future!
{{% /quotecite %}}

{{% quotecite "Panos" %}}
Hey Rachael! I might have not had the chance to meet with you much while still around but I’ll definitely miss your jokes and the good vibes you were bringing to each call! Looking forward to taking over your place for board games when around Bristol ;) Wishing you a great start in the new place!
{{% /quotecite %}}

{{% quotecite "Mike" %}}
ThunderCats are on the move. ThunderCats are loose. Says it all, really. Best of luck in your new endeavours.
{{% /quotecite %}}

## Crossref won't be the same without Rachael and we wish her well on her way to even greater things.

{{< figure src="/images/blog/2024/rachael/rachael-2024.jpeg"  width="100%" >}}

## Good luck, Lammey!

<br>
<br>
<br>
<br>
