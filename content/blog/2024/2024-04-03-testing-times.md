---
title: 'Testing times'
author: Martin Eve
draft: false
authors:
  - Martin Eve
date: 2024-04-03
categories:
  - Tools
  - Authorization
  - Labs
archives:
    - 2024
---


One of the challenges that we face in Labs and Research at Crossref is that, as we prototype various tools, we need the community to be able to test them. Often, this involves asking for deposit to a different endpoint or changing the way that a platform works to incorporate a prototype.

The problem is that our community is hugely varied in its technical capacity and level of ability when it comes to modifying their platform. Some mega-publishers, for instance, outsource their platforms and so are dependent on third party developers/organizations when they want to make a change. Many smaller publishers, by contrast, use systems such as OJS, which come with Crossref plugins that make life very easy… but that require hard code changes to accommodate prototypes. Such changes are way beyond the technical capacity of most journal editors.

So how can we prototype new ideas and test them? One way is by creating new interstitial interfaces that allow people to manually supplement metadata or register for prototype services. Of course, this requires additional work on behalf of the user. Every time they wish to participate they have to visit an extra web page and re-input details that, surely, were included in the original deposit. 

Another way would be for plugin developers to have an advanced option field that allowed end-users to change their deposit endpoint. It would be excellent to see this feature in OJS, Janeway, and also proprietary systems. This would allow us to work with the community to test new prototype mechanisms, without forcing anyone to edit code. Many systems already include the ability to switch between Crossref’s “test” system and our live deposit API. All I am really suggesting here is the logical next step: allow advanced users to specify a deposit endpoint of their own choosing so that we can give them access to prototype systems.

Of course, it’s not always that simple. Sometimes, prototype systems will require new data fields on submission, for example. In those cases, there is nothing for it except to modify the plugin or to provide a separate interface. But sometimes, as in the case of the Op Cit project (more on which soon), all the data is already in place; we just need to direct users to a different endpoint. Such changes would definitely make testing times less trying.
