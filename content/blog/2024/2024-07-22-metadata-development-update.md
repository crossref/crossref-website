---
title: 'Metadata schema development plans'
author: Patricia Feeney
authors:
  - Patricia Feeney
draft: false
date: 2024-07-22
categories:
  - Metadata 
archives:
    - 2024
---

## It’s been a while, here’s a metadata update and request for feedback

In Spring 2023 we sent out a [survey](https://community.crossref.org/t/take-our-metadata-priorities-survey-by-may-18/3498) to our community with a goal of assessing what our priorities for metadata development should be - what projects are our community ready to support? Where is the greatest need? What are the roadblocks? 

The intention was to help prioritize our metadata development work. There’s a lot we want to do, a lot our community needs from us, but we really want to make sure we’re focusing on the projects that will have the most immediate impact for now. 

Several projects were proposed, based on community demand over time. All are projects we intend to support long-term.

## Projects

The projects included in the survey were:

* **Alternate names** - We proposed adding a repeatable ‘name’ element to allow for names that aren’t separated by given/family/surname.  
* **Updates to funding data** -[this update](https://www.crossref.org/blog/roring-ahead-using-ror-in-place-of-the-open-funder-registry/) will be released in the near future and includes:
    * Expand ROR support - Allow members to supply ROR ID instead of funder ID in funding data and grant records.
    * Include Grant DOIs in funding metadata.
* **Publication typing in citations** - Support citation type in citation metadata (for example article, preprint, data, software, etc.).   
* **Expand contributor role support** - Allow multiple contributor roles to be provided per contributor and add support for external vocabularies (like CRediT)
* **Expand abstract support** - We currently require all abstracts to be formatted using JATS. We will be adding new abstract formats, including BITS and ONIX (which have been requested), as well as a generic abstract format (non-JATS).  
* **Statements** - Add support for free text statements such as data availability, acknowledgments, funding, and conflict of interest.
* **Contributor identifiers** - Accept contributor identifiers such as ISNI (in addition to ORCID, which is already supported).
* **Conference event IDs** - Identifiers for [conference events](https://www.crossref.org/blog/taking-the-con-out-of-conferences/).   

## What’s next?

There is a clear preference for publication types in citations and abstract markup, expanded support for multilingual metadata, followed by expanding contributor roles to support multiple roles and the CRediT taxonomy. The results have helped us prioritize our work and we’re advancing several projects soon based on our readiness to move forward.

First up is **publication typing in citations and statements** - we hope to be able to make this ready for registration in the coming months, but want to confirm a few things first, primarily the list of ‘types’ to apply to citations, so please review and comment: [Metadata updates in need of feedback July 2024](https://docs.google.com/document/u/0/d/1VPXhTPMZzfvAPmTOlNp-bZf9cTLkw0dPZFTuDtDIPls/edit)

We also have been discussing expansions to our **support for preprints metadata** with our [Preprints Advisory Group](https://www.crossref.org/working-groups/preprints/) and have a number of preprint-specific updates that will be rolled out in the coming months as well, including support for versions and status. These proposed changes are also available for comment.

And finally, we will be expanding **support for contributor roles** to include multiple roles per contributor, as well as adding support for the [CRediT](https://credit.niso.org/) taxonomy.  This update is yet to be scheduled but we do have the inputs and output planning done and welcome any comments on this as well.  

We will also be continuing work on other projects highlighted in the survey that aren’t quite ready to go:

- **Multilingual metadata**: Support for multilingual metadata in particular is very important and will require a fairly significant technical effort, so we want to be sure we get this right - at minimum we need to include repeatable fields flagged with language metadata for most items, there may be other considerations as well such as the scope of languages supported.

     As we develop new metadata segments we’re keeping language metadata in mind, but I’d like to form a short-term working group to help shape this update - this group will be focused on the details of supporting multilingual metadata in our inputs and outputs, so conversations will be very XML and JSON heavy. If you are interested and available please contact pfeeney@crossref.org. <p>

- **Abstract markup:** we are currently in the research phase of this project but will be proposing updates and asking for input this fall. At the moment support for BITS and ONIX abstracts have been requested, as well as an agnostic format.

- **Expansion of name and contributor ID support**: work is under way for this as well, and I should have inputs and outputs for feedback in the coming months.

We anticipate more developments and requests for feedback in the future as we still have other projects from the list above to get to. I’ve opened up a ‘[Metadata Development](https://community.crossref.org/c/metadata-development/15)’ section in our Community Forum to invite discussion and will be kicking off a renewed Metadata Interest Group in the fall.  
