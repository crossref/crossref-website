---
title: 'Meet the candidates and vote in our 2024 Board elections'
slug: "2024-board-election"
author: Lucy Ofiesh
draft: false
authors:
  - Lucy Ofiesh
date: 2024-09-24
categories:
  - Board
  - Member Briefing
  - Governance
  - Elections
  - Crossref Live
  - Annual Meeting
archives:
    - 2024
---

On behalf of the Nominating Committee, I’m pleased to share the slate of candidates for the 2024 board election. 

Each year we do an open call for board interest. This year, the [Nominating Committee](/committees/nominating) received 53 submissions from members worldwide to fill four open board seats. 

We maintain a balanced board of 8 large member seats and 8 small member seats. Size is determined based on the organization's membership tier (small members fall in the $0-$1,650 tiers and large members in the $3,900 - $50,000 tiers). We have two large member seats and two small member seats open for election in 2024. 

We were pleased to see the diversity in candidates, with applicants from 24 countries. We also received three applications from research funders, which we specifically identified as a priority in the committee’s remit for this year. The committee was keen to prepare a diverse slate of organization types, individual skills, and global representation. 

The Nominating Committee presents the following slate.

## The 2024 slate

### Tier 1 candidates (electing two seats):
* **Katharina Rieck**, Austrian Science Fund (FWF)
* **Lisa Schiff**, California Digital Library
* **Ejaz Khan**, Health Services Academy, Pakistan Journal of Public Health
* **Karthikeyan Ramalingam**, MM Publishers

### Tier 2 candidates (electing two seats):
* **Aaron Wood**, American Psychological Association
* **Dan Shanahan**, PLOS
* **Amanda Ward**, Taylor and Francis

{{% divwrap blue-highlight %}}

### [Please read the candidates' statements](/board-and-governance/elections/2024-slate/)

{{% /divwrap %}}

## Every member has a vote

If your organization is a voting member in good standing as of September 11th, 2024, you are eligible to vote.

The voting contact for your organization will receive a ballot from eBallot, a third party election platform. You should receive your ballot by Wednesday, September 25th, and you will have until 15:00 UTC on October 29th to submit your ballot. 

The election results will be announced at [Crossref2024](/crossref-annual-meeting/), our anual online meeting on October 29th, 2024. 

If you have any questions about our election process, please [contact me](mailto:lofiesh@crossref.org) 

Happy voting!