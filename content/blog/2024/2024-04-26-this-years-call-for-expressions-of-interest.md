---
title: 'This year''s call for expressions of interest to join our board'
author: Lucy Ofiesh
draft: false
authors:
  - Lucy Ofiesh
date: 2024-04-26
categories:
  - Board
  - Member Briefing
  - Governance
  - Elections
  - Crossref Live
  - Annual Meeting
archives:
    - 2024
---

The Crossref Nominating Committee is inviting expressions of interest to join the Board of Directors of Crossref for the term starting in January 2025. The committee will gather responses from those interested and create the slate of candidates that our membership will vote on in an election in September. 

**Expressions of interest will be due Monday, May  27th, 2024**

This is an exciting time to join the board, as we have a number of active projects underway: We are considering resourcing Crossref for a sustainable future and board members will be part of deciding any changes to our fees scheme and overseeing its implementation. We're focusing on how our community and metadata can contribute to ensuring the integrity of the scholarly record. We’re broadening our metadata record to capture richer funding and institutional affiliations. We're working towards a future where the scholarly record prioritizes relationships between research outputs to build a holistic research nexus. The board helps guide this work. 

### About the board elections

The board is elected through the “one member, one vote” policy wherein every member organization of Crossref has a single vote to elect representatives to the Crossref board. Board terms are for three years, and this year, there are four seats open for election. 

The board maintains a balance of seats, with eight seats for smaller members and eight seats for larger members (based on total revenue to Crossref). This is an effort to ensure that the scholarly community's diversity of experiences and perspectives is represented in decisions made at Crossref. 

This year, we will elect two of the larger member seats (membership tiers $3,900 and above) and two of the smaller member seats (membership tiers $1,650 and below). You don’t need to specify which seat you are applying for; we will provide that information to the nominating committee.

The online election will open in September, with results announced at the annual meeting on October 29th, 2024. New members will begin their term in January 2025.


### About the Nominating Committee
The Nominating Committee reviews the expressions of interest and selects a slate of candidates for election. The slate put forward will exceed the total number of open seats. The committee considers the statements of interest, organizational size, geography, and experience. 

2024 Nominating Committee
* James Phillpotts*, Director of Content Transformation and Standards, Oxford University Press, committee chair
* Oscar Donde*, Editor in Chief, Pan Africa Science Journal
* Rose L’Huillier*, Senior Vice President Researcher Products, Elsevier
* Ivy Mutambanengwe-Matanga, Chief Operating Officer, African Journals Online
* Adam Sewell, Chief Technology Officer, IOP Publishing

(*) indicates Crossref board member


### What is the committee looking for this year
The committee looks for skills and experience that will complement the rest of the board. Candidates from countries and regions not currently reflected on the board are strongly encouraged to apply. Successful candidates often have some or all of these characteristics:

* Demonstrate a commitment to or understanding of our [strategic agenda](https://www.crossref.org/strategy/) or the [Principles of Open Scholarly Infrastructure](https://openscholarlyinfrastructure.org/); 
* Have expertise that may be underrepresented on the board currently; 
* Hold senior/director-level positions in their organizations;
* Have experience with governance or community involvement;
* Represent member organizations that are active in the scholarly communications ecosystem; 
* Demonstrate metadata best practices as shown in the member’s [participation report](https://www.crossref.org/members/prep/)

The board is also encouraging Crossref members who are research funders to apply.

### Board roles and responsibilities
Crossref’s services provide a central infrastructure for scholarly communications. Crossref’s board helps shape the future of our services and by extension, impacts the broader scholarly ecosystem. We are looking for board members to contribute their experience and perspective. 

The role of the board at Crossref is to provide strategic and financial oversight of the organization, as well as guidance to the Executive Director and the staff leadership team, with the key responsibilities being:
* Setting the strategic direction for the organization;
* Providing financial oversight; and
* Approving new policies and services.

The board is representative of our membership base and guides the staff leadership team on trends affecting scholarly communications. The board sets strategic directions for the organization while also providing oversight into policy changes and implementation. Board members have a fiduciary responsibility to ensure sound operations. They do this by attending board meetings as well as joining more specific board committees. 

### Who can apply to join the board?
Any active member of Crossref can apply to join the board. Crossref membership is open to organizations that produce content, such as academic presses, commercial publishers, standards organizations, and research funders. 

### What is expected of board members?
Board members attend four meetings each year that typically take place in January, March, July, and November. Meetings have taken place in a variety of international locations and travel support is provided when needed. January, March, and November board meetings are held virtually, and all committee meetings take place virtually. Each board member should sit on at least one Crossref committee. Care is taken to accommodate the wide range of time zones in which our board members live.

While the expressions of interest are specific to an individual, the seat that is elected to the board belongs to the member organization. The primary board member also names an alternate who may attend meetings in the event that the primary board member is unable to. There is no personal financial obligation to sit on the board. The member organization must remain in good standing. 

Board members are expected to be comfortable assuming the responsibilities listed above and to prepare and participate in board meeting discussions.


### How to apply
Please [click here to submit your expression of interest](https://docs.google.com/forms/d/e/1FAIpQLSf5m9mXCYRGQgu_6qlo7xaIz0LyFgmzIXTeOC-UW8_2C20pmw/viewform). We ask for a brief statement about how your organization could enhance the our board and a brief personal statement about your interest and experience with Crossref. 

Please contact me with any questions at [lofiesh@crossref.org](mailto:lofiesh@crossref.org)
