---
title: 'I4OA Hall of Fame - 2023 edition'
author: Bianca Kramer
image: "/images/banner-images/blog_image_header.png"
draft: false
authors:
  - Bianca Kramer
date: 2024-01-09
categories:
  - 140A
  - Abstracts
  - Community
archives:
    - 2024
---


The [Initiative for Open Abstracts (I4OA)](https://i4oa.org/) was launched in September 2020 to advocate and promote the unrestricted availability of the abstracts of the world's scholarly publications, particularly journal articles and book chapters, in trusted repositories where they are open and machine-accessible. I4OA calls on all scholarly publishers to open the abstracts of their published works and, where possible, to submit them to Crossref.

<!--more-->

Since the launch of I4OA, we have been tracking the openness of abstracts for all Crossref members over time (for data and code, see this [GitHub repository](https://github.com/bmkramer/I4OA)). For a subset of 40+, mostly larger, publishers, the proportion of current journal articles (published in the current year and preceding two years) that have abstracts deposited in Crossref is shown in a [chart on the I4OA website](https://i4oa.org/#:~:text=are%20shown%20in-,orange,-.), which is updated quarterly (Figure 1).

<center>
{{< figure src="/images/blog/2024/I4OA_chart_current_2024_01_01.png" alt="An image of a dot graph titled 'selected publishers - abstracts in Crossref. Journal articles (2021-2023) per 2024-01-01'"  width="75%" >}}</center>

_Figure 1: Proportion of current journal articles from selected publishers that have open abstracts in Crossref. Data collected on January 1, 2024 for publication years 2021-2023. Publishers already supporting I4OA are shown in orange._

These longitudinal data and accompanying visualisations allow us to identify and highlight good examples from 2023: publishers (both large and small) who newly started to make abstracts openly available last year and/or who managed to get the proportion of their articles with open abstracts close to 100%<sup>1</sup>.

While we highlight some of these examples below in our 'Hall of Fame', it's important to also acknowledge all the publishers that already were depositing abstracts to Crossref for most or all of their journal articles prior to 2023, thereby contributing to the availability of abstracts as part of a rich ecosystem of open metadata, [for others to use and build upon](https://www.leidenmadtrics.nl/articles/why-openly-available-abstracts-are-important-overview-of-the-current-state-of-affairs). 

## Hall of Fame - Part 1: publishers included in I4OA visualisation

For the set of (mostly larger) publishers included in the visualisation on the I4OA website, Figure 2 shows the difference in the proportion of abstracts available in Crossref between January and December 2023 for journal articles published in 2021-2023.

A number of publishers stand out from this figure:

-   **Wiley** announced in [October 2022](https://www.wiley.com/en-us/network/publishing/research-publishing/open-access/wiley-expands-commitment-to-open-research) that it was joining I4OA and would be making abstracts available through Crossref. In August 2023, Wiley started to deposit abstracts to Crossref, and at the end of 2023, the proportion of current journal articles with open abstracts was 77%.

    This makes Wiley the first of the four largest traditional commercial publishers to deposit abstracts for the majority of journal articles they publish. Springer Nature does this only for their current open access articles, while Elsevier and Taylor & Francis<sup>2</sup> do not yet provide abstracts to Crossref at all. SAGE, the fifth largest traditional commercial publisher, was a founding member of I4OA and has open abstracts for 85% of current journal articles.  <br><br>

-   Among society publishers, the **American Geophysical Union (AGU)** went from 7% to 99% open abstracts for current journal articles last year, which is a great achievement. The publishing arm of the **American Institute of Physics (AIP Publishing)** joins them in reaching close to 100% open abstracts, going from 41% to 95% in 2023.

-----------------------

<sup>1</sup>Depending on the type of journal(s) of a given publisher, the maximal coverage of open abstracts will often be somewhat below 100%, as in Crossref, all journal content is assigned the type ‘journal article’. This includes e.g. editorials, letters to the editor and other publication types that are not always expected to have abstracts. 


<sup>2</sup>The numbers for Wiley and Taylor & Francis do not include Hindawi and F1000 Research, respectively, as these have separate Crossref member IDs. As most full open access publishers, both Hindawi and F1000 Research have high proportions of open abstracts (81% and 98%, respectively). 




-   **CAIRN** and **Project Muse**, two publishing platforms in the humanities and social sciences representing a number of individual publishers, both started including abstracts in the metadata they provide to Crossref in 2023. At the end of 2023, CAIRN had abstracts available for 41% of current journal articles, while Project Muse was just starting out at 5%. Both will hopefully increase further this coming year.

-   Returning to traditional commercial publishers, **Wolters Kluwer Health**, part of Wolters Kluwer, had seen a slow growth in the proportion of journal articles with open abstracts in the years prior to 2023, going from 2% to 10%. However, they showed a rapid increase in 2023, ending the year with 52% open abstracts.

While it is good to see publishers who have publicly committed their [support for I4OA](https://i4oa.org/#:~:text=Publishers%20supporting%20I4OA) follow through with opening their abstracts (like Wiley and AIP), it is also very encouraging to see publishers who are not (yet) listed as I4OA supporters do so. This shows a growing awareness and action on this issue beyond advocacy through I4OA alone. And of course, we would love to list these publishers on our website as official supporters of I4OA!

Figure 2 also shows some cases where the proportion of open abstracts has gone down during the year. This can be due to temporary technical issues in depositing abstracts (as was the case for Hindawi). Theoretically, the proportion of open abstracts can also go down when publishers stop providing abstracts altogether during the year, but we have not observed that to be the case.

<center>
{{< figure src="/images/blog/2024/I4OA_chart_diff.png" alt="An image of a dot graph titled 'selected publishers - abstracts in Crossref. January - December 2023 (journal articles 2021-2023'" width="75%" >}}</center>

_Figure 2: Development in the proportion of open abstracts in 2023 for current journal articles (publication years 2021-2023) from selected publishers. Publishers already supporting I4OA are shown in orange. Light orange/blue dots show the proportion of open abstracts in January 2023, and dark orange/blue dots in December 2023._

## Hall of Fame - Part 2: other publishers

Among the many publishers not included in the limited selection shown in the I4OA visualisation, there are also some interesting highlights of publishers either starting out to deposit abstracts (and reaching a sizeable proportion) or having deposited open abstracts for almost all their current journal articles in 2023. The examples below drew our attention in 2023; they include a number of medium-sized publishers as well as a group of smaller publishers that deserve special attention.

-   The **European Molecular Biology Organization (EMBO)** went from 0% to 42% open abstracts in 2023. However, from January 2024 onwards, several EMBO journals were transferred to Springer Nature, so EMBO can no longer be tracked at publisher level in Crossref. It will still be possible to look at the development of open abstracts for individual EMBO journals.

-   The **Institution of Engineering and Technology (IET)**, a medium-sized publisher, started to deposit abstracts in 2023, reaching 33% open abstracts for current journal articles at the end of the year.

-   The **Acoustical Society of America (ASA)** had open abstracts for almost all their current journal articles at the end of 2023, increasing from 50% to 97%.

-   Finally, in the second quarter of 2023, a group of **over 200 smaller Turkish publishers** saw large increases in their coverage of open abstracts, resulting in open abstracts for 95%-100% of their current journal articles. Consultation with Crossref pointed to the potential supporting role of [DergiPark](https://dergipark.org.tr/en/), one of the largest Crossref sponsors in Turkey. This is a great example of developments in open metadata at smaller publishers.

## Looking forward 

At the beginning of 2024, the proportion of current journal articles published by Crossref members with open abstracts has reached 49.7%, up from 20.7% when I4OA was launched in September 2020. This is thanks to a growing number of publishers who are depositing abstracts to Crossref, often depositing open abstracts for close to 100% of their journal articles.

This blog post has highlighted a number of publishers who contributed to this growth in the availability of open abstracts in 2023. We hope these examples will inspire other publishers to start doing the same and are looking forward to following the growth in the availability of open abstracts in 2024.

For publishers that started to deposit abstracts in recent years and are doing so for newly published articles only, our data on open abstracts for current journal articles will look better in 2024 than in 2023, as only articles published in the current year and two preceding years are taken into account.

However, the benefits of having abstracts openly available from a central location such as Crossref (both for direct usage and for integration in other open scholarly infrastructures) are not limited to recent publications only. Hopefully, publishers currently depositing abstracts to Crossref will continue to do so both for newly published articles as well as for the backfiles of journal articles already published.

Publishers who would like to be added to the list of I4OA supporters, or who would like more information on how to deposit abstracts for both new and existing journal articles, are very welcome to [reach out to I4OA](mailto:openabstracts@gmail.com). More information about open abstracts in general, and I4OA in particular, can also be found in the [FAQ](https://i4oa.org/faqs.html) on the I4OA website.

_The author would like to thank Ludo Waltman (CWTS) and Ginny Hendricks (Crossref) for useful feedback on an earlier draft of this post._  

_This blog post is published under a CC BY 4.0 license. The header image is an adaptation of an image by Adam Jones available from Wikimedia Commons (<https://commons.wikimedia.org/wiki/File:Interior_02_of_Rock_%26_Roll_Hall_of_Fame_and_Museum,_Cleveland_%28by_Adam_Jones%29.jpg>) and is shared under a CC BY-SA license._