---
title: 'Celebrating five years of Grant IDs: where are we with the Crossref Grant Linking System?'
author: Kornelia Korzec
authors:
  - Kornelia Korzec
  - Ginny Hendricks
draft: false
date: 2024-07-01
categories:
  - Research Funders 
  - Grants
  - Infrastructure
  - Metadata
  - Identifiers
archives:
    - 2024
---
We’re happy to note that this month, we are marking five years since Crossref launched its Grant Linking System. The Grant Linking System (GLS) started life as a joint community effort to create ‘grant identifiers’ and support the needs of funders in the scholarly communications infrastructure.

{{% divwrap align-right %}}<img src="/images/community-images/gls/gls-logo-stacked.png" alt="Crossref Grant Linking System logo" width="100%" >{{% /divwrap %}}
The system includes a funder-designed metadata schema and a unique link for each award which enables connections with millions of research outputs, better reporting on the research and outcomes of funding, and a contribution to open science infrastructure. Our first activity to highlight the moment was to host a community call last week where around 30 existing and potential funder members joined to discuss the benefits and the steps to take to participate in the Grant Linking System (GLS).

Some organisations at the forefront of adopting Crossref’s Grant Linking System presented their challenges and how they overcame them, shared the benefits they are reaping from participating, and provided some tips about their processes and workflows.

The funding organisations whose experiences were shared included [Wellcome](https://wellcome.org/), [FCT (Foundation for Science and Technology, Portugal)](https://www.fct.pt/en/), and [NWO (Dutch Research Council)](https://www.nwo.nl/en). They were joined by a new group of foundations, research councils, and private research funders from around the world---from Kenya to Singapore to Estonia---to have a first introduction to the GLS and connect them with colleagues who are further along on their journey.

We also heard about tools such as a new [open source Crossref plugin](https://github.com/oaworks/create-grant-doi-in-fluxx) for the Fluxx platform, grant management systems with in-built Crossref integrations such as [ProposalCentral](https://proposalcentral.com/), [Europe PMC GrantFinder](https://europepmc.org/grantfinder) which was first to implement the GLS on Wellcome’s behalf and hosts their grants, and one of the first publishers, [eLife](https://elifesciences.org/) to start referencing Crossref grant links in their publications both online and in the open metadata for others to retrieve.

Read on for further information or watch [the recording of the event](https://www.youtube.com/watch?v=LuM2eMOTmN8).

<iframe width="560" height="315" src="https://www.youtube.com/embed/LuM2eMOTmN8?si=GefNp773GN36XGTp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## What is the Crossref Grant Linking System?

The Crossref Grant Linking System, conceptualised in 2017, and launched in 2019, captures and helps clarify funding relationships for scholarly outputs. Thanks to interconnectedness with the 160 million metadata records collected and curated by Crossref members, it enables funders as well as scholars to track and analyse funding patterns and evaluate programmes, and it supports assertions about the integrity of scholarly records.

### Features of the GLS

- Globally unique persistent link and identifier for each grant
- Connected with 160 million published outputs
- Funder-designed metadata schema, including project, investigator, value, and award-type information
- Programmatic or no-code methods to send metadata
  - Thanks to the [Gordon and Betty Moore Foundation](https://www.moore.org/) who funded development of the [online grant registration form](https://manage.crossref.org/records)
- Open search and API for all to discover funding outcomes; all metadata is distributed openly to thousands of tools and services
- Crossref-hosted landing pages
- A global community of ~50 funder advisors and 35+ funders already in the Grant Linking System
- Membership of Crossref; influence the foundational infrastructure powering open research

The last five years has seen the GLS grow through membership, metadata, and community contributions.

{{< figure src="/images/community-images/gls/gls-5-years.png" width="100%" alt="graph showing the effects of specific funders joining that increase matches and relationships in the Crossref Grant Linking System">}}
<br>
The momentum for this programme is building - as illustrated by increasing numbers of metadata records (and related relationships we’re seeing). The 35 funder members represent over 100 funding programmes and have created 125,000 grant records already.

{{< figure src="/images/community-images/gls/gls-growth.png" width="100%" alt="timeline of the Crossref Grant Linking System from 2019 to 2024">}}

During last week's call, it was helpful to hear from the community what they see as key benefits of the Crossref Grant Linking System:

{{% row %}}
{{% column %}}

- Meaningfully delivering on and supporting Open Science policies and mandates, and contributing ‘their bit’ to the transparency of the evidence trail in the scholarly ecosystem.
- Reporting and evaluating the funding programmes, essential for the public funders who need to demonstrate the value for money in allocating their funds and other support.
- Supporting a more holistic assessment of scholarship and scholars, especially as and when metadata becomes included with a full array of outputs, not limited to books and articles.

{{% /column %}}

{{% column %}}

<img src="/images/community-images/gls/gls-benefits.png" alt="High-level benefits of the Crossref Grant Linking System (GLS)" width="100%" >

{{% /column %}}

{{% /row %}}

## How the Crossref Grant Linking System supports Open Science policy

Since 2020, all the grant records are openly available through our REST API which is queried more than 1.8 billion times every month so these metadata records are distributed to thousands of systems across the research enteprise. In a 2022 blog, Ed Pentz and Ginny Hendricks laid out [guidelines for research funders to meet open science guidelines](/blog/how-funding-agencies-can-meet-ostp-and-open-science-guidance-using-existing-open-infrastructure/) using existing open infrastructure such as Crossref, ORCID, and ROR. Syman Stevens, a grantmaking and private philanthropy consultant, highlighted on the call that the funders he works with are increasingly interested in ways to deliver on their open science policy and that participation in the GLS is a tangible thing they can do to meet this goal.

{{% quotecite "Hans de Jonge, Director of Open Science NL, part of the Dutch Research Council (NWO)" %}}
As part of its open science policy, NWO will start participating in the Crossref Grant Linking System from July 2025. Research funders are a part of the scholarly communications system; we not only provide the funding to do the actual research but can also be the authoritative source of data about the projects we have funded and the outputs arising from that funding. Increasingly, all these elements – grants, researchers, outputs - are linked with metadata and unique identifiers to ensure that research is findable and accessible.
{{% /quotecite %}}

## How funders leverage the Grant Linking System in their reporting and assessment

Looking back to the origins of the system, it’s important to recognise the work of the initial working groups. Through their contribution, funders helped design the initial metadata schema for grants as well as establish the governance and fees for this service, and our Advisory Group continues to inform further developments. In this way, the Grant Linking System enables the needs and wishes of funders to contribute and see their data as part of the wider ecosystem.

An excellent example of that synergy in action is the use case presented by Cátia Laranjeira, manager of the PTCRIS programme at the Foundation for Science and Technology, Portugal (FCT).  PTCRIS is the Foundation’s integrated national information ecosystem that supports scientific activity management. Cátia reflected on the relative fragmentation of spaces where the scientific outputs are found, and PTCRIS’s ambition for aggregating metadata in one place to be able to trace and evaluate programmes in light of the related outputs. At the start of the programme, they identified lack of a persistent identifier for grants as a major shortcoming of the system. Crossref GLS naturally fits in with their goals.

{{% quotecite "Cátia Laranjeira, PTCRIS Program Manager at Fundacao para a Ciencia e a Tecnologis (FCT Portugal)" %}} The initiative by FCT to assign unique DOIs to national public funding through Crossref is a game-changer for open science, linking funding directly to scientific outcomes and boosting transparency. Join us in this effort—let's make every grant count and ensure open access to research information!"
{{% /quotecite %}}

FCT initially piloted a small subset of their grants (approximately 6,000 recent awards) at the end of 2023. Cátia pointed to researchers’ keen participation in this programme as one of its successes – and thanks to the word of mouth, FCT has already been approached by researchers requesting unique Crossref links for their grants! This appetite for grant IDs will soon be more fully satisfied, as FCT is readying to register all of their grants with Crossref, to enable further insights into funding and outcome flows, supporting them in demonstrating the value for money for the public resources they manage. Via interfaces for grant management and standardised online CVs, the system is also enabling researchers to use the system in their own future reporting and career development.

In the ensuing discussion, Rachel Bruce of UKRI mentioned that she’s hopeful that GLS will help funders ‘close the loop’ on more holistic reward and recognition, allowing for inclusion of evidence for a broader set of outputs in those processes.

## How the community is working to integrate open infrastructure

Melissa Harrison, Team Leader at EMBL-EBI, manages Europe PMC and a complementary data science team, who were part of the initial FREYA project – supporting infrastructure delivery for unique identifiers for grants. The team has been adding grant records to Crossref on Wellcome’s behalf since 2019. Melissa highlighted the shortcomings of internal award numbers, which don’t tend to be understood outside of the ecosystem where they are produced (that is the funder’s administrative system), are almost certainly not unique, and don’t resolve to or connect with anything in the wider ecosystem. Therefore internal award numbers can’t signify relationships with other outputs or assets in the wider world. By contrast, Crossref’s Grant IDs are unique, persistent, resolvable, and interrelated with other Crossref metadata, whilst being retrievable for other systems to link to too.

{{% quotecite "Melissa Harrison, Team Leader, Literature Services at EMBL-EBI)" %}}
Persistent identifiers for grants was the next logical step after identifiers for funders - open metadata registered with a PID in a central service like Crossref is invaluable to build the full picture of the research enterprise.
{{% /quotecite %}}

Ease of execution is important for scaling the Grant Linking System, and enabling its use in a diverse set of circumstances in the open science ecosystem. Altum was the trailblazer, first integrating its grant management platform Proposal Central with GLS. It was good to hear that others are now joining the integration efforts. Syman Stevens talked about the recent work initiated by Joe McArthur at [OA Works](https://oa.works/), to develop a simple, open-source plug-in for any of the major grant management systems, to enable funders to deposit their grant metadata with Crossref GLS with a click of the button. Syman demonstrated the resulting interface in Fluxx, that allows for creating a record and sending grant metadata to Crossref as part of the regular grant management within the platform. He pointed out that, while this integration was developed for Fluxx, all code and documentation is openly available on [GitHub](https://github.com/oaworks/create-grant-doi-in-fluxx) and this can potentially be forked or adapted as necessary for reuse in other grant management systems.

It is heartening that others in the community are seeing such a need for this that they're funding and creating their own tools to advance participation and use of the GLS.

Finally, Fred Atherden, Head of Production Operations at eLife, presented how they include Crossref grant identifiers in publication metadata for the version of record of the works published on their platform. eLife is the first publisher to fully integrate Crossref grant identifiers both within the article display and in the metadata. Fred shared that in addition to collecting the data from the authors, eLife also attempts matching, albeit using very restrictive methodology, to enable more grant metadata in their publication records. They recognise that so far there are very few publishers including persistent links for grants in this way, and talked about plans to start collecting and including this data further upstream, and including them in the future for reviewed preprints.

## Acknowledgements and how to participate in the GLS

Reflecting on the last five years, thanks must go to the >35 funders who are already participating (see logo mashup below), to our [current volunteers](/working-groups/funders) and to those partners working to promote and make use of the Grant Linking System. We also acknowledge that the GLS would not have been possible without the Crossref board members at the time, our staff including alumni Josh Brown, Jennifer Kemp, Rachael Lammey, and Geoffrey Bilder, or without the early dedicated time and input from the following people and organisations on our working groups for governance and fees, and for metadata modelling:

- Yasushi Ogasaka and Ritsuko Nakajima, Japan Science & Technology Agency
- Neil Thakur and Brian Haugen, US National Institutes of Health
- Jo McEntyre and Michael Parkin, Europe PMC
- Robert Kiley and Nina Frentop, Wellcome
- Alexis-Michel Mugabushaka and Diego Chialva, European Research Council
- Lance Vowell and Carly Robinson, OSTI/US Dept of Energy
- Ashley Moore and Kevin Dolby, UKRI (Research Councils UK / Medical Research Council)
- Salvo da Rosa, Children's Tumor Foundation
- Trisha Cruse, DataCite

{{< figure src="/images/community-images/gls/gls-members.png" width="100%" alt="funding bodies participating in the Crossref Grant Linking System (GLS)">}}

To learn more about the [Crossref Grant Linking System](/services/grant-linking-system), the best place to start is our service page. And for the next step, please reach out to us for a conversation about any questions specific to your organisation and any questions that may need to be addressed in order to enable your full participation.

{{% quotecite "Kristin Eldon Whylly, Senior Grants Manager and Change Management Lead at Templeton World Charity Fund (TWCF)" %}}
Grant DOIs enhance the discovery and accessibility of funded project information and are one of the important links in a connected research ecosystem. I'm grateful and proud to contribute to the robustness and interconnectedness of the research infrastructure. Few funders are currently participating in the Crossref Grant Linking System, and I encourage others to consider doing so. This adoption follows the "network effect," where the value and utility increase as more people participate, encouraging even wider adoption.
{{% /quotecite %}}

You can email me via [feedback@crossref.org](mailto:feedback@crossref.org?subject=Grant%20Linking%20System) or [set up a call with me when it suits you](https://savvycal.com/kkorzec/68502be2) (you can overlay your own calendar using the toggle at the top right). We look forward to welcoming even more funders and to see those relationships in the open science infrastructure grow even further in the coming years.
