---
title: 'The GEM program - year one'
author: Susan Collins
draft: false
authors:
  - Susan Collins
date: 2024-01-24
categories:
  - GEM
  - Community
  - Membership
  - Equity
archives:
    - 2024
---

In January 2023, we began our[  Global Equitable Membership (GEM) Program](/gem/) to provide greater membership equitability and accessibility to organisations located in the least economically advantaged countries in the world. Eligibility for the program is based on a member's country; our list of countries is predominantly based on the [International Development Association (IDA)](https://datahelpdesk.worldbank.org/knowledgebase/articles/906519-world-bank-country-and-lending-groups). Eligible members pay no membership or content registration fees.

The list undergoes periodic reviews, as countries may be added or removed over time as economic situations change. Sri Lanka was added to the GEM program in March 2023 as they were recategorised to the IDA classification by the World Bank.

When the program launched, we had 214 existing members eligible for the program who then were no longer charged for membership or content registration. Since the program began, we have welcomed an additional 131 new members into the program, including our first members from Cambodia and Togo.

| Country                    | As of 1/1/2023<br> (start of GEM) | Additions in 2023 <br>(end of first year of GEM) | Total |
|----------------------------|-------------------------------|----------------------------------------------|-------|
| Afghanistan                | 6                             | 4                                            | 10    |
| Bangladesh                 | 56                            | 33                                           | 89    |
| Benin                      | 1                             | 1                                            | 2     |
| Bhutan                     | 4                             | 2                                            | 6     |
| Burkina Faso               | 2                             | 0                                            | 2     |
| Burundi                    | 1                             | 0                                            | 1     |
| Cambodia                   | 0                             | 2                                            | 2     |
| Central African Republic   | 1                             | 0                                            | 1     |
| Congo, Democratic Republic | 1                             | 11                                           | 12    |
| Ethiopia                   | 4                             | 6                                            | 10    |
| Ghana                      | 14                            | 7                                            | 21    |
| Guyana                     | 1                             | 1                                            | 2     |
| Haiti                      | 1                             | 0                                            | 1     |
| Kosovo                     | 2                             | 2                                            | 4     |
| Kyrgyz Republic            | 22                            | 3                                            | 25    |
| Laos                       | 1                             | 0                                            | 1     |
| Madagascar                 | 1                             | 1                                            | 2     |
| Malawi                     | 1                             | 0                                            | 1     |
| Maldives                   | 1                             | 0                                            | 1     |
| Mali                       | 2                             | 0                                            | 2     |
| Mauritania                 | 1                             | 0                                            | 1     |
| Myanmar                    | 1                             | 0                                            | 1     |
| Nepal                      | 20                            | 18                                           | 38    |
| Nicaragua                  | 1                             | 0                                            | 1     |
| Rwanda                     | 4                             | 1                                            | 5     |
| Senegal                    | 3                             | 3                                            | 6     |
| Somalia                    | 2                             | 2                                            | 4     |
| Sri Lanka                  | 13                            | 5                                            | 18    |
| Sudan                      | 9                             | 2                                            | 11    |
| Tajikistan                 | 5                             | 1                                            | 6     |
| Tanzania                   | 9                             | 7                                            | 16    |
| Togo                       | 0                             | 1                                            | 1     |
| Uganda                     | 3                             | 6                                            | 9     |
| Yemen                      | 16                            | 12                                           | 28    |
| Zambia                     | 5                             | 0                                            | 5     |



With help from our ambassadors based in GEM countries, we organised and co-hosted several webinars to introduce the program, along with an introduction to Crossref, and the benefits of including all kinds of research objects in the [Research Nexus](/documentation/research-nexus/).

-   In April, our team, together with ambassador Binayak Raj Pandey, provided an overview of Crossref for members and organisations in Nepal. 

-   Our team and ambassadors, Dr Md Jahangir Alam and Shaharima Parvin hosted two webinars in May for members and organisations in Bangladesh. The first webinar provided an introduction to Crossref, our services, and the GEM Program. The second webinar focused on the methods to register content and how to add and update metadata. 

-   In September, ambassador Baraka Manjale Ngussa joined us for an introductory webinar aimed at organisations in Tanzania

-   In November, CARLIGH (the Consortium of Academic and Research Libraries in Ghana), Crossref, and EIFL co-hosted a webinar for librarians and journal editors in Ghana with a discussion on the GEM program and Crossref services.

In 2024, we will continue to collaborate with our ambassadors and other members of the community to offer more opportunities for organisations in GEM-eligible countries to learn about the program and the benefits of membership for content discovery.

The program was initially met with scepticism by some organisations in GEM-eligible countries, who wanted to be certain that it wasn't a free trial, that there are no hidden fees, or that they would be required to pay later for other services. Others expressed concern that Crossref would introduce fees after a year or two. Though we were able to clarify these aspects of the program, we understand the concerns and are working to ensure we provide clarity and transparency about the program. Additionally, we will be conducting a complete review of our fees in 2024, and we will ensure that GEM-eligible members will have input.

Although the program offers relief from fees, many organisations require technical assistance and language support. The GEM program would benefit from an increase in local [Sponsors](/community/sponsors/) to facilitate membership and provide support, particularly In countries with the highest growth, such as Bangladesh, Nepal, Yemen, Kyrgyz Republic, and Ghana. Though we have Sponsors working with members who are in GEM countries (e.g. PKP), we do not yet have any Sponsors who are based in a GEM country.

We will be working with relevant like-minded organisations, such as PKP, DOAJ, INASP, OASPA, EIFL, and others, to help identify suitable candidates for new Sponsors in underserved regions and engage them proactively. Additionally, we will consult with our ambassadors in GEM countries to help identify potential Sponsors. We are beginning the year by making the most of the momentum created in African countries (Uganda, Ghana, Tanzania) and looking to develop new networks in other parts of the world in Q2-Q4 of this year.