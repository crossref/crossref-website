---
title: 'DOAJ and Crossref renew their partnership to support the least-resourced journals'
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
date: 2024-03-06
categories:
  - Community
  - Collaboration
archives:
    - 2024
---

Crossref and DOAJ share the aim to encourage the dissemination and use of scholarly research using online technologies and to work with and through regional and international networks, partners, and user communities for the achievement of their aims to build local institutional capacity and sustainability.
Both organisations agreed to work together in 2021 in a variety of ways, but primarily to ‘encourage the dissemination and use of scholarly research using online technologies, and regional and international networks, partners and communities, helping to build local institutional capacity and sustainability around the world.’ Some of the fruits of this labour are:
* DOAJ added support for Crossref XML to make it easier for publishers to upload metadata
* Closer collaboration between customer/member support at both organisations, making it easier for publishers and journal editors to navigate both service’s technologies
* the launch of PLACE: ‘a ‘one-stop shop’ for information to support publishers in adopting best practices the industry developed’ (together with other partners)
* a pilot gap analysis of the journals in DOAJ with the possibility of helping them start to use and resolve DOIs.

The new agreement, signed earlier this month, will slightly shift focus to build upon existing collaborations, particularly around metadata. One of the primary sections of the MOU is enhancing support for the least-resourced journals by:

* Assigning DOIs and depositing the metadata with Crossref
* Finding ways to improve their DOAJ application experience to help them become indexed
* Collect and ingest their Crossref metadata into DOAJ
* Help them to get preserved via JASPER or similar initiatives
* Help identify other local partners, such as Crossref Sponsoring Organisations, to support their use of Crossref services

{{% quotecite "Joanna Ball, Managing Director of DOAJ" %}}
It’s great that we can further underpin what is already a good working relationship. Both Crossref and DOAJ are central to discovery so it’s a natural partnership. Helping journals meet better standards and become indexed to make them more discoverable on a global scale is at the heart of our strategy. This agreement opens up a new avenue that allows the community to really focus on supporting those journals and the research they publish.’
{{% /quotecite %}}

{{% quotecite "Ginny Hendricks, Director of Member and Community Outreach at Crossref" %}}
‘The collaborations with DOAJ so far only reconfirmed our shared goal to help make the global scholarly communications system more equitable wherever we can. Our joint projects aim to seek out and devise support for resource-constrained journals in multiple ways. DOAJ’s work is essential in helping journals to develop good practice, while Crossref offers an open infrastructure to ensure all journals can be included and discoverable in the global scholarly record.’
{{% /quotecite %}}

-------- END ------

### About DOAJ
DOAJ is a community-curated online directory that indexes and provides access to high quality, open access, peer reviewed journals.  DOAJ deploys around one hundred carefully selected volunteers from the community of library and other academic disciplines to assist in curating open access journals. This independent database contains over 20,400 peer-reviewed open access journals covering all areas of science, technology, medicine, social sciences, arts and humanities. DOAJ is financially supported worldwide by libraries, publishers and other like-minded organizations. DOAJ services (including the evaluation of journals) are free for all, and all data provided by DOAJ are harvestable via OAI/PMH and the API. See https://doaj.org/ for more information.

### About Crossref
Crossref is a global community-governed open scholarly infrastructure that makes all kinds of research objects easy to find, assess, and reuse through a number of services critical to research communications, including an open metadata API that sees over 1.5 billion queries every month. Crossref’s ~20,000 members come from 155 countries and are made up of universities, publishers, funders, government bodies, libraries, and research groups. Their ~155 million DOI records contribute to the collective vision of a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society.


For more information please contact:
dominic@doaj.org and rclark@crossref.org  

{{< figure src="/images/blog/2024/doaj-crossref-twitter-post-new-2024-1.png" width="80%" >}}
