---
title: 'RORing ahead: using ROR in place of the Open Funder Registry'
author: Rachael Lammey
draft: false
authors:
  - Rachael Lammey
date: 2024-01-30
categories:
  - ROR
  - Metadata
  - Open Funder Registry
archives:
    - 2024
---

A few months ago we announced our plan to [deprecate our support for the Open Funder Registry](/blog/open-funder-registry-to-transition-into-research-organization-registry-ror/) in favour of using the ROR Registry to support both affiliation and funder use cases. The feedback we’ve had from the community has been positive and supports our members, service providers and metadata users who are already starting to move in this direction. 

We wanted to provide an update on work that’s underway to make this transition happen, and how you can get involved in working together with us on this. 

Overall, we are building more comprehensive support for ROR into Crossref’s services. Some of this work is specifically to support using ROR to identify funding organisations in place of funder registry IDs. We have a number of parallel, complementary projects underway to support different elements of this work:  

1. We are evolving our metadata schema so that we can collect ROR IDs in places where we currently support the collection of Funder IDs. 
2. We are analysing the coverage of Funder ID to ROR ID mappings and testing the way we expose them in our APIs. 
3. We are developing new matching strategies to match text strings to ROR IDs. 

## 1. Schema updates
Everything flows from being able to get ROR IDs into the Crossref metadata!
 
We are evolving our metadata schema so that we can collect ROR IDs in places where we already support the collection of Funder IDs – for instance, in [the funding section of the metadata for works](/documentation/funder-registry/funding-data-overview/) and in the funder section for grants. 

We’re working with members and service providers so that they can try sending us this data via a pipeline our Labs team has built to test schema updates before they go live. We are actively recruiting members to help us test our new pipeline by providing sample XML for registration. Planned metadata inputs and outputs are detailed in [Including ROR as a funder identifier in your metadata (metadata prototyping instructions)](https://docs.google.com/document/u/0/d/164h3UtBQ2mHf5lH5ZS6c_Oh8OuraoaQPvXhNNO3-Ko8/edit), we’d encourage you to provide feedback on these in the document, ideally in the next two weeks. 
We’re aiming to release an updated schema that supports these changes in Q1 2024. 

## 2. Modelling ROR ID/Funder ID mappings in our metadata model 
We have integrated the ROR registry into our evolving metadata model, and we have started work to integrate the Funder Registry. The aim is to create more flexibility in how Crossref’s metadata can be supplemented and queried, and give more clarity as to which party asserted or created a metadata element. 

We’re working on an early iteration of how the model handles ROR IDs, funder IDs and their equivalencies. Once we have something to share, we’ll welcome community feedback on this approach and on the metadata model in general. 

## 3. Developing new matching strategies to match text strings to ROR IDs
Ideally, everyone would always use persistent identifiers to exchange information about contributor and awardee affiliations, organisations related to works, as well as funders supporting the research. In practice, this information is often exchanged as data without identifiers, such as affiliation strings (e.g. “University of Virginia, Charlottesville, VA”), funder names, or even funding acknowledgements (e.g. “Funding and support generously provided by the Ford Foundation”). In such situations, a good metadata matching strategy can help map these to persistent identifiers.

Currently, we are focused on developing reliable strategies for matching affiliation strings to ROR IDs. In the future, we will adapt the strategies to support funder names and funding acknowledgements as well. All the strategies will be rigorously evaluated using real-life data. We will make the strategies, as well as the evaluation datasets and evaluation results, publicly available for anyone to use. If you are interested in collaborating on the development or the evaluation of the matching strategies, [please get in touch](mailto:labs@crossref.org)!

In the future, we might also apply some of the new matching strategies at Crossref, to the metadata our members send us. This would allow us to insert matched identifiers to the metadata to better connect organisations with other items in the scholarly record. We already have a process that matches the names of funders supporting research against the Funder Registry and enriches the metadata with matched Funder Registry IDs. Developing and evaluating reliable matching strategies will allow us to modify this process to use ROR IDs instead, and extend it to support other use cases, such as contributor affiliations.

## What will the transition mean for you?

We do recommend that you begin looking at what it will take to integrate ROR into your systems and workflows for identifying funders. Talk to your service providers about this to ready them for this change. 
To reiterate the point from the earlier post, in the short term, and even in the medium term, Funder IDs aren’t going away and the Funder IDs will continue to resolve – they are persistent, after all. Eventually, however, the Funder Registry will cease to be updated, so any new funders will only be registrable in Crossref metadata with ROR IDs. Legacy Funder IDs and their mapping to ROR IDs will be maintained, so if Crossref members submit a legacy Funder ID, it will get mapped to a ROR ID automatically. Note, too, that Crossref is committed to maintaining the current funder API endpoints until ROR IDs become the predominant identifier for newly registered content. We also know that there are questions that we’ll want to tackle with the community as we all make progress, some we know and some we don’t know. With that in mind: 

## Tell us what you need!

We want to hear from you! We have set up several channels of communication meant to ensure that you can tell both ROR and Crossref what will make this transition easier for you and that you can get answers to your questions. 

First, we are conducting a series of Open Funder Registry user interviews designed to deepen our understanding of where Funder IDs are being used in workflows and systems. Write [community@ror.org](mailto:community@ror.org) if you'd like to participate in these interviews to show and tell us how you're using Funder IDs. 

Second, in 2024, we will be running a follow-up to the funding data workshop we ran in June 2023. Please get in touch if your organisation would be interested in participating in the discussion. 






