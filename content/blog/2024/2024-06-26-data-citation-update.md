---
title: 'Data citation update'
author: Ginny Hendricks
authors:
  - Ginny Hendricks
draft: true
date: 2024-07-26
categories:
  - Product 
  - Strategy
archives:
    - 2024
---

We’ve spent three years and quite a lot of money building a new relationships database and technology stack (dubbed ‘manifold’) that has recently shown itself to be unreliable and unusable. Yesterday's post described product priorities and announced that we have paused this greenfield development project to catch up on the backlog of initiatives that most of our members are asking for.

Back in mid-2023, we needed a test case and we chose data citation and set about building a new service on this new tech stack. It did not deliver and we have disappointed users like STM and DataCite, and others in the community who were counting on being able to retrieve the linked metadata for their own projects they have committed to for their own communities. 


### Where are we with data citation?

Data citation is not necessarily deprioritised... but the promised new way of retrieving them unfortunately has to be for now. All relationships are critical to the scholarly (metadata) record, so that includes a long list of connections between things like data—also preprints, versions, translations, newer research objects like software and video, and of course, general references, retractions, and so on. But while we’ve focused on building that greenfield tech stack that included a new relationships API, we’ve neglected a backlog of work that thousands of our members have been asking for and are ready to send to us - including many of the same large publishers that are STM members. For example, we are being asked to prioritise things like expanding contributor roles and affiliations, expanding preprints such as including withdrawals and retractions, merging the Retraction Watch dataset with our own, revising the Crossmark button, speeding up the metadata deposit and notifications services, expanding support for grant records with more award types and project metadata, even arguably ‘simple’ things like more granular dates, timestamps, and accepting non-JATS-formatted abstracts.All of these things contribute to research integrity which I know is also an STM priority.

One thing to remind even ourselves of is that Crossref already supports data citation both on the input and the output sides. For many years, publishers have been able to express these in their Crossref metadata. There are two routes - 1) references and 2) relationships. On the output side, our main REST API distributes these openly. Very few publishers do and we should understand better why this is not happening. Is data citation a low priority for publishers in general (and colleagues haven’t succeeded in convincing each other internally), or are researchers simply not citing/posting data as much as we’d all like? Or both. 

Another thing to think about is the campaigning approach. Despite an apparent lack of enthusiasm in our membership, Crossref has spent thousands of hours and invested a huge amount of resources since 2017 in trying to help move the needle on data citation. We stand by the joint statement - that all remains true (and to reiterate, all possible today). We have neither sought nor received any funding for these efforts. It’s worth noting that campaigns like I4OC (journal citations) and I4OA (abstracts) were researcher-led and supported by bibliometricians, and have seen much more success in influencing large publishers to take action. The Barcelona Declaration is now picking up where these other metadata campaigns left off - data citation is not on the priority list, but we could talk about adding it.

So hopefully you can understand why we had no choice but to make the tricky decision to pause all work on a new relationships API and refocus on the other things that our members are asking for that we know we can deliver on. We’re also making some internal changes, for example, to the ways that we prioritise at all (I’m overseeing product work ad interim) so we can transparently measure and balance the various drivers our members have and come up with a new roadmap from 2025 and beyond.

Relatedly, the Event Data API and the Scholix endpoint also don't include the correct or complete data. Event Data itself is very unstable too and it costs us ~10k USD every month with very few users - so this will also be up for reprioritisation in the coming weeks. 

I’m certain we will overcome this setback, but I’m sorry that we’re in this situation now, which has knock-on effects for you. If we can find a way to compile data citation snapshots in a reliable and low-cost way, we will, but we haven’t found a way yet. Others have received millions in grant funding to build this sort of thing, so maybe a solution will be found elsewhere too. 

We can talk about this situation together on a call if that would be helpful. In general, it would be great to understand more about STM’s overall strategy and priorities (for example, why data citation is so critical for you and what the visualisations you're planning aim to do) and other areas of overlap or collaboration. I know that you’ve talked with others at Crossref many times so this may feel duplicative, but why don’t we have a general call in the coming month or so between you, me, Ed, and Caroline just to share information about what each organisation is working on and where we’re going?

Will anyone from STM be attending the Make Data Count summit in London in September? Ed and I will be there (on and off). It seems like a good opportunity to discuss in a broad group the state of the community in the data area. We’re putting out an announcement about manifold in the next couple of weeks, similar to the above.

## What's next?


We will be recruiting for a Director of Technology in July so if you have experience of metadata infrastructure transformation at scale, leading collaborative software development programs, and equipping a global team with the tools and techniques to deliver on this big audacious goal - keep an eye out!