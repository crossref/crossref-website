---
title: XMP Primer
author: Tony Hammond
authors:
  - Tony Hammond
date: 2009-06-10

categories:
  - XMP
archives:
  - 2009

---
There’s a new [XMP Primer][1] (PDF) by Ron Roskiewicz (ed. Dianne Kennedy) available from XMP-Open. This is copyrighted 2008 but I only just saw this now. This is a 43 page document which provides a very gentle introduction to metadata and labelling of media and then introduces XMP into the content lifecycle and talks to the business case for using XMP. The primer covers the following areas:

  * Introduction to Metadata
      * Introduction to XMP
          * XMP and the Content Lifecycle
              * XMP in Action; Use Cases
                  * Additional XMP Resources </ul>
                    One small gripe would be that this seems to have been prepared for US letter-sized pages and although is printable on A4 there is the slightest of clippings on the right-hand margin with no real loss of information but it does confer a sense of &#8220;incompleteness&#8221;. Really there can be little excuse these days for this parochialism. Also, for a document talking up the benefits of using XMP, it’s decidedly odd that it doesn’t make use of XMP itself - or rather there is a default XMP packet in the PDF with no real useful properties such as title, author, or date. Could have been a nice little object lesson in using XMP.

 [1]: https://web.archive.org/web/20101124111737/http://www.idealliance.org/filefolder/XMPPrimer.pdf
