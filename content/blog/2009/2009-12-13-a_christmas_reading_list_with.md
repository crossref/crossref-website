---
title: 'A Christmas Reading List&#8230; with DOIs'
slug: 'a-christmas-reading-list-with-dois'
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2009-12-13

categories:
  - Identifiers
  - Linking
archives:
  - 2009

---
Was outraged (outraged, I tell you) that one of my favorite online comics, [PhD][1], didn’t include DOIs in [their recent bibliography of Christmas-related citations.][2]. So I’ve compiled them below.

We care about these things so that you don’t have to. Bet you will sleep better at night knowing this.

Or perhaps not&#8230;

## A Christmas Reading List&#8230; with DOIs.

Citation:  Biggs, R, Douglas, A, Macfarlane, R, Dacie, J, Pitney, W, Merskey, C &#038; O’Brien, J, 1952, &#8216;Christmas Disease’, BMJ, vol. 2, no. 4799, pp. 1378-1382.

Crossref DOI:  <http://dx.doi.org/10.1136/bmj.2.4799.1378>

Title:  More Than a Labor of Love: Gender Roles and Christmas Gift Shopping

Citation:  Fischer, E &#038; Arnold, S, 1990, &#8216;More Than a Labor of Love: Gender Roles and Christmas Gift Shopping’, Journal of Consumer Research, vol. 17, no. 3, p. 333.

Crossref DOI:  <http://dx.doi.org/10.1086/208561>

Title:  Looking at Christmas trees in the nucleolus

Citation:  Scheer, U, Xia, B, Merkert, H &#038; Weisenberger, D, 1997, &#8216;Looking at Christmas trees in the nucleolus’, Chromosoma, vol. 105, no. 7-8, pp. 470-480.

Crossref DOI:  <http://dx.doi.org/10.1007/s004120050209>

Title:  The Vela glitch of Christmas 1988

Citation:  McCulloch, P, Hamilton, P, McConnell, D &#038; King, E, 1990, &#8216;The Vela glitch of Christmas 1988’, Nature, vol. 346, no. 6287, pp. 822-824.

Crossref DOI:  <http://dx.doi.org/10.1038/346822a0>

Title:  Cardiac Mortality Is Higher Around Christmas and New Year’s Than at Any Other Time: The Holidays as a Risk Factor for Death

Citation:  Phillips, D, 2004, &#8216;Cardiac Mortality Is Higher Around Christmas and New Year’s Than at Any Other Time: The Holidays as a Risk Factor for Death’, Circulation, vol. 110, no. 25, pp. 3781-3788.

Crossref DOI:  <http://dx.doi.org/10.1161/01.CIR.0000151424.02045.F7>

Title:  Red Crabs in Rain Forest, Christmas Island: Biotic Resistance to Invasion by an Exotic Snail

Citation:  Lake, P &#038; O’Dowd, D, 1991, &#8216;Red Crabs in Rain Forest, Christmas Island: Biotic Resistance to Invasion by an Exotic Snail’, Oikos, vol. 62, no. 1, p. 25.

Crossref DOI:  <http://dx.doi.org/10.2307/3545442>

Title:  The Carvedilol Hibernation Reversible Ischaemia Trial, Marker of Success (CHRISTMAS) study Methodology of a randomised, placebo controlled, multicentre study of carvedilol in hibernation and heart failure

Citation:  Pennell, D, 2000, &#8216;The Carvedilol Hibernation Reversible Ischaemia Trial, Marker of Success (CHRISTMAS) study Methodology of a randomised, placebo controlled, multicentre study of carvedilol in hibernation and heart failure’, International Journal of Cardiology, vol. 72, no. 3, pp. 265-274.

Crossref DOI:  <http://dx.doi.org/10.1016/S0167-5273(99)00198-9>

 [1]: http://www.phdcomics.com/comics.php
 [2]: http://www.phdcomics.com/comics/archive.php?comicid=1262
