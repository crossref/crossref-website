---
title: OpenSearch Formats for Review
author: Tony Hammond
authors:
  - Tony Hammond
date: 2009-07-23

categories:
  - Search
archives:
  - 2009

---
In an [earlier post][1] I talked about using the PAM (PRISM Aggregator Message) schema for an SRU result set. I have also noted in [another post][2] that a Search Web Service could support both [SRU][3] and [OpenSearch][4] interfaces. This does then beg the question of what a corresponding OpenSearch result set might look like for such a record.
  
Based on the [OpenSearch spec][5] and also on a new [Atom extension for SRU][6], I have contrived to show how a PAM record might be returned in a coomon OpenSearch format. Below I offer some mocked-up examples for each of the following formats for review purposes:

  * RSS 1.0 
      * ATOM 
          * JSON </ul> 
            Just click the relevant figure for a text rendering of each result format for the following phrase search:
            
            > cql.keywords adj &#8220;solar eclipse&#8221;
            
            In this example we imagine that two records have been requested. (The example formats also include navigational links as per the OpenSearch spec examples.)
  
            Note that the JSON example closely follows the ATOM schema with a couple of main deviations:
            
              * Repeated elements are gathered together in an array (e.g. &#8220;entry&#8221;, &#8220;dc:creator&#8221;) 
                  * Attributes are broken out alongside their parent elements (e.g. &#8220;rel&#8221;, &#8220;href&#8221;)</ul> 
                    It would be interesting to hear what readers think of these examples - especially the JSON format.
                    
                    <table cellpadding="10" border="0">
                      <tr>
                        <td>
                          <a href="http://nurture.nature.com/opensearch/demo/solar2-rss.txt"><img alt="solar2-rss.jpg" border="0" width="159" height="309" src="/wp/blog/images/solar2-rss.jpg" /></a>
                        </td>
                        
                        <td>
                          <a href="http://nurture.nature.com/opensearch/demo/solar2-atom.txt"><img alt="solar2-atom.jpg" border="0" width="159" height="309" src="/wp/blog/images/solar2-atom.jpg" /></a>
                        </td>
                        
                        <td>
                          <a href="http://nurture.nature.com/opensearch/demo/solar2-json.txt"><img alt="solar2-json.jpg" border="0" width="159" height="309" src="/wp/blog/images/solar2-json.jpg" /></a>
                        </td>
                      </tr>
                      
                      <tr>
                        <th>
                          RSS 1.0
                        </th>
                        
                        <th>
                          ATOM
                        </th>
                        
                        <th>
                          JSON
                        </th>
                      </tr>
                    </table>
                    
                    (Click image to get text format.)

 [1]: /blog/structured-search-using-prism-elements/
 [2]: /blog/search-web-service
 [3]: http://www.loc.gov/standards/sru/
 [4]: http://opensearch.org/
 [5]: http://www.opensearch.org/Specifications/OpenSearch/1.1
 [6]: http://www.oasis-open.org/committees/download.php/33410/atom-extension-for-sru.doc