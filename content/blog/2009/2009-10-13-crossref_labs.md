---
title: Crossref Labs
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2009-10-13

categories:
  - News Release
  - InChI
archives:
  - 2009

---
The other day [Noel O’Boyle][1] wrote to tell me that he had updated the Ubiquity plug-in that we had developed in order to to make it work with the latest version of Firefox. The problem was, I had &#42;also&#42; updated the Ubiquity plug-in, but I hadn’t really indicated to anybody how they could find updates to the plug-in. /me=embarrassed. So it seemed time to provide a home for some of the prototypes and experiments that we’ve been developing at Crossref. To that end, we have created a [Crossref Labs][2] site. Here you can find links to various tools and services that either make it easier to use Crossref services (e.g. [Blog][3]/[Ubiquity][4] plugins and [OpenSearch Description files][2]) or that serve to illustrate a concept that has been of interest to our members ([InChI lookup][5], [TOI-DOIs][6]). Oh, yeah- and when we update these experiments, you should be able to find the updates on their respective pages. Sorry about that Noel&#8230; Finally, I will quote from the [Crossref Labs home page][2]:

> &#8220;Most of the experiments linked to here are running on R&D equipment in a non-production environment. They may disappear without warning and/or perform erratically. If one of them isn’t working for some reason, come back later and try again.&#8221; Have fun.

 [1]: http://www.redbrick.dcu.ie/~noel/
 [2]: https://www.crossref.org/labs/
 [3]: https://www.crossref.org/labs/wordpress-moveable-type-plugins/
 [4]: https://www.crossref.org/labs/ubiquity-plugin/
 [5]: https://www.crossref.org/labs/inchi-lookup/
 [6]: https://www.crossref.org/labs/toi-doi-i-e-short-dois/
