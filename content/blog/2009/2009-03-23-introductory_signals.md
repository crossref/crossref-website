---
title: Introductory Signals
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2009-03-23

categories:
  - Legal citations
  - Citation
archives:
  - 2009

---
So while doing some background reading today I realized that legal citations already widely support a form of &#8220;[citation typing][1]&#8221; in the form of &#8220;[Introductory Signals][2]&#8220;. The 10 introductory signals break down as follows&#8230;

In support of an argument:

&nbsp;&nbsp;&nbsp;1) [no signal]. (NB that, apparently, this is increasingly deprecated.)

&nbsp;&nbsp;&nbsp;2) accord;

&nbsp;&nbsp;&nbsp;3) see;

&nbsp;&nbsp;&nbsp;4) see also;

&nbsp;&nbsp;&nbsp;5) cf.;

For Comparisons:

&nbsp;&nbsp;&nbsp;6) compare &#8230; with &#8230;;

For contradiction:

&nbsp;&nbsp;&nbsp;7) but see;

&nbsp;&nbsp;&nbsp;8) but cf.;

For background:

&nbsp;&nbsp;&nbsp;9) see generally;

And for examples:

&nbsp;&nbsp;&nbsp;10) e.g.

Clever lawyers.

 [1]: /blog/citation-typing-ontology/
 [2]: http://en.wikipedia.org/wiki/Citation_signal
