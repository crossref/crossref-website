---
title: Recommendations on RSS Feeds for Scholarly Publishers
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2009-10-19

categories:
  - Interoperability
  - Metadata
  - News Release
  - RSS
archives:
  - 2009

---
We’re pleased to announce that a Crossref working group has released a set of [best practice recommendations][1] for scholarly publishers producing RSS feeds.
  
Variations in practice amongst publisher feeds can be irritating for end-users, but they can be insurmountable for automated processes. RSS feeds are increasingly being consumed by knowledge discovery and data mining services. In these cases, variations in date formats, the practice of lumping all authors together in one <font color="#3eb1c8">\<dc:creator\> </font> element, or generating invalid XML can render the RSS feed useless to the service accessing it.
  
The recommendations intended to facilitate good practice in the production and provision of TOC RSS Feeds. The guidelines include general recommendations for good practice, specific recommendations on the use of RSS Modules and an example RSS TOC feed. Ultimately, we expect that industry wide adoption of these best practices will help drive more traffic to publisher web sites. Note that most of these recommendation can also be applied to non-TOC RSS feeds such as thematic feeds, automated search result feeds, etc.

 [1]: http://oxford.crossref.org/best_practice/rss/