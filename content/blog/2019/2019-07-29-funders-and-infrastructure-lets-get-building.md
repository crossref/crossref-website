---
title: 'Funders and infrastructure: let’s get building'
author: Josh Brown
draft: false
authors:
  - Josh Brown
date: 2019-07-29
categories:
  - Collaboration
  - Research Funders
  - Infrastructure
  - Metadata
  - Grants
archives:
    - 2019
---

Human intelligence and curiosity are the lifeblood of the scholarly world, but not many people can afford to pursue research out of their own pocket. We all have bills to pay. Also, compute time, buildings, lab equipment, administration, and [giant underground thingumatrons do not come cheap](https://web.archive.org/web/20190729155623/https://fap-dep.web.cern.ch/rpc/2019-annual-contributions-cern-budget). In 2017, according to statistics from [UNESCO](https://en.unesco.org/), [$1.7 trillion dollars](http://uis.unesco.org/apps/visualisations/research-and-development-spending/) were invested globally in Research and Development. A lot of this money comes from the public - [22c in every dollar ](http://data.uis.unesco.org/Index.aspx?DataSetCode=SCN_DS&lang=en)spent on R&D in the USA comes from government funds, for example. Funders really do support a LOT of research.

For that research to count, it needs to be communicated. For us to interpret those research communications critically, we need to understand how the research was done and [who paid](https://www.ucsusa.org/disguising-corporate-influence-science-about-sugar-and-health) for it.

At Crossref, we’ve been [working with funders](/community/funders) for many years. The [Open Funder Registry](/services/funder-registry/) was launched (with donated support from Elsevier) in 2012, and provides a taxonomy of funders, each uniquely identified, which has grown to cover 20,000 funders around the world. This resource has helped to connect the organizations that provide research funds to resources, projects, and publications. Some are also members and have been registering content with us. This is a growing trend as more funders start to launch their own [open platforms](https://amrcopenresearch.org/). Funders also consume metadata from Crossref members, using it to track and report on the published outputs of the researchers they support.

More recently, we have been exploring the ways that we can do more in partnership with the funding community. As our [board](/board-and-governance/) concluded in 2017,

> Crossref requires increased emphasis on funders, understanding their needs and requirements and increasingly including funders in the scholarly communication dialogue.

In response, we have explored new services and practical enhancements to our existing portfolio, such as the [new grants registration system](/education/content-registration/content-types-intro/grants/), which will also power search and lookup tools.

This new initiative will link [structured information about grants](https://github.com/CrossRef/grantID-schema) with DOIs, and enable us to provide open tools to help institutions, publishers, and research supporting organizations to re-use that data and make long-lasting connections between specific funding (and [other kinds of research support](/blog/global-persistent-identifiers-for-grants-awards-and-facilities/)) and research activities and outcomes. The value of this was beautifully explained by our friends at [Wellcome](https://wellcome.ac.uk/) (now members) in this [blog post](/blog/wellcome-explains-the-benefits-of-developing-an-open-and-global-grant-identifier/), and was reinforced by a recent survey undertaken by ORCID in which linking grants to outputs was cited as one of the major challenges facing funders. The Crossref Grant Linking System launched this July with a group of early adopter funders, ably supported by the team at [Europe PMC](https://europepmc.org/).

We’re not stopping there though: we are lucky to have a dedicated and engaged [funder advisory group](/working-groups/funders), and we will continue to work with them to understand how our interactions with funders can benefit the wider ecosystem that we support, and help funders to achieve their goals.

There are many platforms providing vital intelligence to funders, from [Dimensions](https://www.dimensions.ai/) to [OpenAIRE](https://www.openaire.eu/), which rely on Crossref data. Last month, I was at the [OAI11 workshop](https://indico.cern.ch/event/786048/) in [Geneva](https://www.geneve.com/), and it was striking how many presentations included a slide that mentioned using Crossref data. There were 200 people from the open science community there, and they clearly rely on Crossref as a [foundational infrastructure](https://cameronneylon.net/blog/where-are-the-pipes-building-foundational-infrastructures-for-future-services/) to build their ecosystem. That community is also just a subset of the more than 2,500 registered consumers of Crossref metadata. We need to keep asking how this metadata can improve the information available to funders, to their partners and to service providers. Adding grants to the mix will help all of these parties provide an even richer picture of research.

As we move forward with our engagement with the global funding community, new opportunities are becoming visible, and not just for funders. Better experiences for authors, reduced overhead for publishers and easier benchmarking for institutions are a selection of benefits that this work can help us realize.

When we really start to get to grips with opening up information about the inputs to research in the way we already have with its outputs, truly exciting things can happen. The really great thing about this is that, quite literally, everyone benefits: from Crossref members to everyone touched by advances in our understanding of the world. Let’s get building!
