---
title: 'A simpler text query form'
author: Isaac Farley
draft: false
authors:
  - Isaac Farley
date: 2019-04-30
categories:
  - Content Registration
  - Membership
  - Metadata
  - References
  - Linking
archives:
    - 2019
---

The [Simple Text Query form](https://apps.crossref.org/SimpleTextQuery) (STQ) allows users to retrieve existing DOIs for journal articles, books, and chapters by cutting and pasting a reference or reference list into a simple query box. For years the service has been heavily used by students, editors, researchers, and publishers eager to match and link references.

We had changes to the service planned for the first half of this year - an upgraded reference matching algorithm, a more modern interface, etc. In the spirit of openness and transparency, part of our project plan was to communicate these pending changes to STQ users well in advance of our 30 April completion date. What would users think? Could they help us improve upon our plans?

<!--more-->
About a month ago, I reached out to the 21,000 plus users we had on record of using STQ since January 2018. We received nearly 85 responses from the messages we sent. Questions ranged from: if we were making changes, would PubMed ID matching be supported? To: What about the reliability of the returned reference links? And: Could we better accommodate larger reference lists?

Many of the users we heard from told us how STQ was critical to their work. I read all these messages. The concerns raised by users were legitimate and much appreciated. We reassessed our project timeline and plans, and decided to shift course. So, what *are* we doing?

### What’s changing?
* The previous hurdle of having to register your email address simply to return reference links was confusing and unnecessary. We removed it.
* We previously limited the number of monthly reference links to 5,000 per email address. Most didn’t reach the limit, but those who did were frustrated by it and/or found ways around it. We want you to match and register as many references as possible, so we removed the monthly limit too.
* Many of you with long reference lists found that you were occasionally reaching our limit of 30,000 characters per submission. Once again, we want you to match and register as many references as possible so we removed the character limit altogether and instead are just looking at the number of references per submission. We now provide space for 1,000 references per submission (We checked. The most references we have ever received via the STQ form in one submission was around 750. Thus, we rounded up.).
* We did make a change to the backend of the service. We updated [the algorithm](/blog/matchmaker-matchmaker-make-me-a-match/) we use to return reference links. We think it’s [an improvement](/blog/reference-matching-for-real-this-time/). Let us know how you find it.

### What’s remaining the same?
* Core functionality. It's all in the name. Retrieve DOIs for journal articles, books, and chapters by cutting and pasting a reference or reference list into a simple query box.  
* PubMed ID matching. You use it. You need it. We’re keeping it.
* Deposits. You’ll still need an email address for this, but we won’t ask for it until you’re at the deposit screen.
* The interface. We’re still eager to give the user interface a much-needed refresh, but, as many users pointed out to us, there’s still some core functionality that’s important that we need to retain with any interface update. For instance, you need to be able to easily copy and paste reference links into your reference list. That functionality isn’t going anywhere.
* Resetting reference links. Submit references, match, reset, and repeat. Many users like the reset button. It’s not going anywhere either.

### XML queries
The change to the backend of the service that I mentioned above is not confined to reference matching and depositing for STQ users. XML queries for reference matching are also now powered by that new backend. We think it’s a seamless transition, but if you find it is not, please let us know.

I’m excited for these changes and hope you are too. I invite you to try the simpler and improved STQ form, and [let us know what you think](mailto:support@crossref.org).  
