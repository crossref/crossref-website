---
title: 'Work through your PID problems on the PID Forum'
author: Rachael Lammey
draft: false
authors:
  - Helena Cousijn
  - Rachael Lammey
  - Alice Meadows
date: 2019-02-21
categories:
  - Identifiers
  - Infrastructure
  - Collaboration
  - Community
archives:
    - 2019
---
As self-confessed PID nerds, we’re big fans of a persistent identifier. However, we’re also conscious that the uptake and use of PIDs isn’t a done deal, and there are things that challenge how broadly these are adopted by the community.

At [PIDapalooza](https://pidapalooza.org/) (an annual festival of PIDs) in January, ORCID, DataCite and Crossref ran an interactive session to chat about the cool things that PIDs allow us to do, what’s working well and, just as importantly, what isn’t, so that we can find ways to improve and approaches that work.

{{% imagewrap right %}} <img src="/images/blog/yin_yang_board.jpg" alt=“the yin yang board" height="150px" width="400px" class="img-responsive" /> {{% /imagewrap %}}

We titled the session the Yin & Yang of PIDs and challenged attendees to put down on paper (post-its) their thoughts about what’s good about PIDs (the Yin) and what’s not so good (the Yang). Attendees could also upvote other’s comments by adding a smiley face sticker to the concept(s) they supported.

So what came out of the session? [Lots of things](https://doi.org/10.5281/zenodo.2572718)!

- Limits to PID uptake are often more cultural than technical
- Yay for [auto-update](https://support.orcid.org/hc/en-us/articles/360006896394-Auto-updates-time-saving-and-trust-building)!
- Slow adoption of  new PID types
- Trust issues (I don’t want my information in another system)
- "I'm the only Erik, I don't need an ORCID"
- User stories work!  

{{% imagewrap left %}} <img src="/images/blog/what_are_pids.jpg" alt=“what are PIDs" height="100px" width="300px" class="img-responsive" /> {{% /imagewrap %}}

We know we only scratched the surface in the session, but fortunately PIDapalooza also brought a good way to continue the conversation: [pidforum.org](https://pidforum.org)! The PIDforum was [launched at PIDapalooza](https://doi.org/10.5281/zenodo.2548649)  and is a global discussion platform for all things PID-related. Many PID providers and PID users are already on there, so help us understand more about the yin and yang of PIDs by sharing your own PID problems!
