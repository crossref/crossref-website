+++
title = "Organisation chart"
date = "2025-02-09"
draft = false
author = "Ginny Hendricks"
[menu.main]
parent = "About us"
rank = 4
weight = 25
+++

Here is our organisational chart. Also take a look at our [people directory](/people) and click through to read about each of our roles and areas of responsibility. If you're using Firefox and can't view the interactive chart below, here is static image, correct as of January 2025.

{{< figure src="/images/staff/org-chart-jan-25.png" width="100%">}}

## Interactive org chart

<iframe src='https://app.organimi.com/embed/organizations/5673da03df38a203000e8795/charts/5673da1768da2f030041928c/chart/view?pId=79a83874fc074c79d785f64fe11e39edf96489ea049e2123bfd0cbdb2986b090' style='height:600px;width:100%;background:#fff;'></iframe>

