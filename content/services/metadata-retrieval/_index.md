+++
title = "Metadata Retrieval"
date = "2024-02-03"
draft = false
author = "Martyn Rittman"
image = "/images/banner-images/dois-not-the-same.jpg"
maskcolor = "crossref-darkblue"
aliases = [
    "/cms/cms_policies.html",
    "/cms/index.html",
    "/metadata_services.html",
    "/services/metadata-delivery",
    "/affiliates",
    "/affiliates/",
    "/services/metadata-delivery/"
]
[menu.main]
rank = 5
weight = 2
parent = "Find a service"
+++

{{< snippet "/_snippet-sources/metadata-retrieval-intro.md" >}}

Anyone can retrieve and use `160,104,382` records without restriction. So there are no fees to use the metadata but if you really rely on it then you might like to sign up for [Metadata Plus](/services/metadata-retrieval/metadata-plus) which offers greater predictability, higher rate limits, monthly data dumps in XML and JSON, and access to dedicated support from our team.

## Options for retrieving metadata

All Crossref metadata is completely open and available to all. Whatever your experience with metadata, there are several tools, techniques, and support guides to help---whether you're just beginning, exploring occasionally, or need an ongoing reliable integration.

{{% row %}}
{{% column-thirds %}}

### BEGINNING?

{{< icon style="fas" name="fa-angle-right" color="crossref-green" size="large" >}} **You've heard Crossref metadata might be useful and want to know where to start.**

We recommend you start with [metadata search](https://search.crossref.org/), [funder search](https://search.crossref.org/funding), or [simple text query](https://apps.crossref.org/SimpleTextQuery) for matching references to DOIs. Also take a look at the [tips for querying our REST API in a browser](/documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-api/) which only needs you to get a JSON plugin to view the results. We are also building up tutorials to demonstrate the possibilities, starting with a [funder metadata workbook](https://cr-funders-r-workbook-crossref-demos-835a6dbd3d62c9c760dc1d526b.gitlab.io/). If it's retractions and corrections that you need, check out the frequently-updated `csv` file of the [Retraction Watch dataset](/blog/news-crossref-and-retraction-watch#supporting-details) that we  acquired and opened in 2023.

{{% /column-thirds %}}

{{% column-thirds %}}

### EXPLORING?

{{< icon style="fas" name="fa-angle-double-right" color="crossref-yellow" size="large" >}} **You have some specific queries and want a lightweight  way to use Crossref metadata.**

Take a look at the in-depth interactive documentation for our REST API at [api.crossref.org](https://api.crossref.org). If you're comfortable with `tar` data files you can download our latest annual [public data file](/documentation/retrieve-metadata/rest-api/tips-for-using-public-data-files-and-plus-snapshots/). You may be interested in how relationships between research objects are represented, so have a look at the [Colab notebook](https://colab.research.google.com/drive/19_J5qzW9kIu_6PyrehV9Qded8SIduwiz#scrollTo=o01ZiHpEPQfq) of the upcoming [endpoint for relationship metadata, currently in beta at https://api.crossref.org/beta/relationships](https://community.crossref.org/t/relationships-endpoint-update-and-event-data-api-sunsetting/4214). Our [Crossref Learning Hub](/learning/) is also another helpful resource. 

{{% /column-thirds %}}

{{% column-thirds %}}

### INTEGRATING?

{{< icon style="far" name="fa-forward" color="crossref-orange" size="large" >}} **You rely on Crossref metadata and need to incorporate it into your product at scale.**

You might want to jump straight to subscribing to [Metadata Plus](/services/metadata-retrieval/metadata-plus), which is our premium service for the REST API that comes with monthly data dumps in `JSON` and `XML`, higher rate limits, and fast support. But we always recommend that you try out the public version first to make sure it will work for your product. If you're looking for a single DOI record in multiple formats (e.g. `RDF`, `BibTex`, `CSL`) you can use [content negotiation](/documentation/retrieve-metadata/content-negotiation/).

{{% /column-thirds %}}
{{% /row %}}


## Watch the animated introduction to metadata retrieval

{{< vimeoPlayer id="services-metadataretrieval">}} 

<button onclick="window.location.href='/documentation/retrieve-metadata'">READ THE DOCS</button>

## Learn more by visiting our learning hub.


<button onclick="window.location.href='/learning/'" style="background-color:#005f83; color: white; display: block; margin-left: 0;">
  {{< icon style="far" name="fa-book" color="white" >}} &nbsp;&nbsp;CROSSREF LEARNING HUB&nbsp;&nbsp;
</button>

