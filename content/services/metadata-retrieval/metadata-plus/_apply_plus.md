+++
title = "Apply for Metadata Plus"
date = "2024-08-05"
draft = false
author = "Luis Montilla"
parent = "Metadata Plus"
Rank = 4
weight = 3
+++

## Ready to apply? What you will need

Make sure you’re in the right place. This application form is for those of you who wish to subscribe to Crossref Metadata Plus and retrieve metadata benefiting from the features listed in the [service's description](https://www.crossref.org/services/metadata-retrieval/metadata-plus/#00343).

There are a few steps to joining and it usually takes a week or so to complete the process, depending on how many extra questions we have to ask you. Make sure you have all the information you need before you start. We’re going to ask you lots of important questions so we can get you set up as quickly as possible. It will save a lot of time if you can put the right information into the form now rather than having to change it later:

 - **Your organisation name** - you should apply to join in the name of the largest legal entity, except you are A) a University, then the subscriber is considered to be the university department(s), school(s), or faculty(ies) that is using the service e.g. Department of Earth Sciences, School of Law, or Faculty of Medicine. Or B) you are a government body, then the subscriber is considered to be the agency(ies), department(s), or ministry(ies) that is using the service, e.g. Agence de l’Innovation Industrielle, Department of Energy, or Ministry of Consumer Affairs.
 
 - **Your mailing address and your billing address** - some of you will have a different address for invoices, and it’s important that we collect the correct address now. The billing address you provide in this application form is what will appear on the invoice we send you for your first year of membership as part of this application process. Before you apply, check what this should be, and also whether your finance team needs other details quoted on the invoice (for example, your tax number).
 
 - **The publishing revenue of your organization** - we don’t need to know exact amounts, but we use this information to decide which tier of our annual membership fee you need to pay. There is an annual subscription fee which is payable each January or will be pro-rated for the remainder of the calendar year. The fee tier is selected based on whichever is the higher between: 
    A) your total annual revenue (including earned and fundraised, e.g. grants); or 
    B) your annual operating expenses (including staff and non-staff, e.g. occupancy, equipment, licenses etc.).

 - **Contact details for four separate people at your organization** - we ask you to provide contact details (full name and email address) for 
    A) The Signatory on the agreement
    B) The Primary contact for the day-to-day relationship
    C) The Technical contact for any troubleshooting or new features and documentation.
    D) The Billing contact to send invoices to.

## What happens next?

We’ll review your application within two working days. We may need to ask some clarifying questions. Where applicable, we'll send you a pro-rated membership order (invoice) for the remainder of the calendar year. Once you've paid, we’ll send you a welcome email with instructions for setting your Metadata Plus API key.

Our replies come from the email address noreply@crossref.org. Please add this email address to your contacts list, or add it to your safe senders list to make sure that you receive our replies.
{{< joinlink-retrieve >}}
---

Please contact our [support team](mailto:support@crossref.org) with any questions.
