+++
title = "Grant Linking System (GLS)"
date = "2024-09-04"
draft = false
author = "Kornelia Korzec"
image = "/images/banner-images/gls-bridge.jpg"
toc = true
aliases = [
    "/community/grants/",
    "/community/grants",
    "/grants/",
    "/grants",
    "/funders",
    "/funders/",
    "/community/special-programs/grant-linking-system/",
    "/community/special-programs/grant-linking-system"
    ]
[menu.main]
rank = 5
parent = "Find a service"
weight = 2
+++


{{% divwrap align-left %}}<img src="/images/community-images/gls/gls-logo-stacked.png" alt="Crossref Grant Linking System logo" width="100%" >{{% /divwrap %}} The Crossref Grant Linking System (GLS) is a service for research funders to contribute to open science infrastructure. As members of Crossref, funders create unique links and open metadata about their support of all kinds, from financial grants to prizes to use of facilities. This metadata is distributed at scale openly and globally and the unique links are acknowledged in any outputs of the funding, such as publications, preprints, data and code - in order to streamline the reporting process. 

## Background to the Grant Linking System

In 2017, our board agreed that connecting more intentionally with research funding should be a key strategic priority for the Crossref infrastructure which already supported all kinds of research outputs like articles, preprints, standards, datasets, etc. Whilst the scholarly community has long been linking persistently between articles and other objects (Crossref), people (ORCID), and institutions (ROR), the record of the award was not captured in a consistent way across funders worldwide. Researchers and publishers have long been acknowledging funders in their publication metadata but the grants themselves were not easily and persistently linked up with the literature or with researchers or with institutions.

After a board motion in 2017, we reconvened our [Funder Advisory Group](/working-groups/funders) and worked with other community partners such as Europe PMC to create a new strategic plan. Part of that work was to agree on a sustainability model and fee schedule, and to design a schema that would capture relevant metadata about grants and projects, and in 2019 we launched the Crossref Grant Linking System (GLS).

Now, over 35 funders have joined as members of Crossref created over 125,000 grants with metadata records and globally unique persistent links that can be connected with outputs, activities, people, and organizations.

## Features of the Grant Linking System

{{% row %}}
{{% column %}}

- Globally unique persistent link and identifier for each grant
- Connected with 160 million published outputs
- Funder-designed metadata schema, including project, investigator, value, and award-type information
- Programmatic or no-code methods to send metadata
    - Thanks to the [Gordon and Betty Moore Foundation](https://www.moore.org/) who funded development of the [online grant registration form](https://manage.crossref.org/records) in 2023.
- Distributed openly to thousands of tools and services
- Open search and API for all to discover funding outcomes
- Crossref-hosted landing pages
- A global community of ~50 funder advisors and >35+ funders already in the Grant Linking System
- Membership of Crossref; influence the foundational infrastructure powering open research

{{% /column %}}

{{% column %}}

<img src="/images/community-images/gls/gls-benefits.png" alt="High-level benefits of the Crossref Grant Linking System (GLS)" width="100%" >

{{% /column %}}
{{% /row %}}

### What our GLS funder members say

{{% quotecite "Hans de Jonge, Director of Open Science NL, part of the Dutch Research Council (NWO)" %}}
Research funders are a part of the scholarly communications system. We not only provide the funding to do the actual research but can also be the authoritative source of data about the projects we have funded and the outputs arising from that funding. Increasingly, all these elements – grants, researchers, outputs - are linked with persistent identifiers to ensure that research is findable and accessible. As part of its open science policy, NWO will start participating in the Crossref Grant Linking System from July 2025
{{% /quotecite %}}

{{% quotecite "Kristin Eldon Whylly, Senior Grants Manager and Change Management Lead at Templeton World Charity Fund (TWCF)" %}}
Grant DOIs enhance the discovery and accessibility of funded project information and are one of the important links in a connected research ecosystem. I'm grateful and proud to contribute to the robustness and interconnectedness of the research infrastructure. Few funders are currently participating in the Crossref Grant Linking System, and I encourage others to consider doing so. This adoption follows the "network effect," where the value and utility increase as more people participate, encouraging even wider adoption.
{{% /quotecite %}}

{{% quotecite "Cátia Laranjeira, PTCRIS Program Manager at Fundacao para a Ciencia e a Tecnologis (FCT Portugal)" %}} The initiative by FCT to assign unique DOIs to national public funding through Crossref is a game-changer for open science, linking funding directly to scientific outcomes and boosting transparency. Join us in this effort—let's make every grant count and ensure open access to research information!"
{{% /quotecite %}}

{{% accordion %}}
{{% accordion-section "Benefits for funders" %}}

**During research management (primarily coming from activity reporting):**

- Improved analytics and data quality
- More complete picture of outputs and impact
- Better value from investments in reporting services
- Improved timeliness, completeness and accuracy of reporting
- More complete information to support analysis and evaluation
- Streamlined discovery of funded content

**During reporting and evaluation (with a special component for policy compliance):**

- Better information about publication pathways and policy compliance
- Better/more comprehensive data about the impact and outcomes of their policies
- Improved data on policy compliance
- Improved data on policy progress and impact
- Streamlined discovery of funded content
- Better understanding of the effects of investments on the research landscape
- Clearer data on impact and ‘ROI’ for facility/infrastructure investments
- Improved analysis and evidence of outcomes of career support
- Improved publication ethics and research integrity (COIs, funding transparency etc.)
- Improved picture of long-term ROI and impact

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "Benefits for content hosts" %}}

Content hosts include publishers, data repositories, and hosting platforms

- Improved publication ethics and research integrity
- Improved services to authors
- Improved transparency on content access
- More connections within and between platforms and content
- New platform opportunities and value added services
- Reduced administrative and information management/verification overhead
- New value add services
- Greater ecosystem integration
- Improved user experiences

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "Benefits for research organizations" %}}

This includes benefits for research administrators and managers, resource managers, project managers, and institutional policy makers.

**Research administrators and managers benefit from:**

- Opportunities to provide additional effective and constructive support for proposal preparation (pre-award)
- Find it easier to perform due diligence (pre-award)
- Reduced overhead in data collection (research management)
- Reduced overhead in compliance and data checking (research management)
- Reduced time/effort and improved data quality (reporting and evaluation)
- Improved evidence for decision making (reporting and evaluation)
- Better evidence for career and organizational impact (reporting and evaluation)

**Resource managers benefit from:**

- Better intelligence on funding sources and dynamics (pre-award)
- Better understanding of who is using their facilities (research management)
- Clearer links to downstream benefits of their work and provision (reporting and evaluation)
- Improved reporting/analysis capacity (reporting and evaluation)
- Improved data quality (research management)
- Simplified data sharing (research management)

**Institutional policy makers and strategists benefit from:**

- Understanding funder portfolios to improve grant targeting (pre-award)
- Reduced data gathering overhead and improved intelligence about their portfolio of outputs (research management)
- Richer understandings of their research activity portfolio (research management)
- Better management of APC budgets (research management)
- Greater insight and evidence for stronger strategic planning (research management)
- More complete information to support analysis and evaluation (reporting and evaluation)
- Improved analytics and data quality (reporting and evaluation)
- Better understanding of outcomes of studentships and postdoctoral positions (reporting and evaluation)
- Improved connections to alumni (reporting and evaluation)
- Better data for benchmarking (reporting and evaluation)

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "Benefits for researchers" %}}

**In applying for funding, researchers benefit from:**

- Reduced data entry and improved reusability of information in applications
- Better tailored institutional support
- Improved targeting and design of career supporting interventions from funders
- Improved review
- Easier completion of applications

**In conducting research, researchers can benefit from:**

- Boosted current awareness
- Easier access to facilities
- Reduced administrative overhead

**In publishing researchers benefit as authors and as readers from:**

- Shorter publication delays
- Simplified acknowledgement processes
- Critical awareness of any potential bias
- Richer context and simplified discovery
- Reduced uncertainty and administration around policy compliance

**In reporting on their activities to funders:**

- Improved reporting experiences
- A shift from data collation/entry to verification
- Easier acknowledgement of support for their careers

**In building their careers:**

- Boosted impact and enhanced visibility
- As collaborators, from better understanding of the contributions of others and improved recognition for their own contributions
- Clearer, more complete and complex career records
- Enhanced career recognition and support
- More diverse data sources for recognition and reward

**At every stage, the core benefits for researchers include:**

- Better career representations and reputational enhancement
- Simplified administration, reporting and application processes with reduced overhead and duplication of effort
- Better intelligence about research support and future opportunities for funding and collaboration

{{% /accordion %}}
{{% /accordion-section %}}

### Funder-designed metadata model

One thing to note about Crossref grant records is that they can be registered for all sorts of support for research, such as awards, use of facilities, sponsorship, training, or salary awards. Essentially any form of support provided to a research group. The award type list (agreed by the Funder Advisory Group) is currently:

{{% row %}}
{{% column %}}

- **award**: a prize, award, or other type of general funding
- **contract**: agreement involving payment
- **crowdfunding**: funding raised via multiple sources, typically small amounts raised online
- **endowment**: gift of money that will provide an income
- **equipment**: use of or gift of equipment
- **facilities**: use of location, equipment, or other resources
- **fellowship**: grant given for research or study
- **grant**: a monetary award
  {{% /column %}}
  {{% column %}}
- **loan**: money or other resource given in anticipation of repayment
- **other**: award of undefined type
- **prize**: an award given for achievement
- **salary-award**: an award given as salary, includes intramural research funding
- **secondment**: detachment of a person or resource for temporary assignment elsewhere
- **seed-funding**: an investor invests capital in exchange for equity
- **training-grant**: grant given for training
  {{% /column %}}
  {{% /row %}}

Take a look at the metadata schema described in our [schema markup guide for grant metadata](/documentation/schema-library/markup-guide-record-types/grants/), to see the details of how to send (or retrieve) metadata including investigators, funding values and types, unlimited numbers of projects with titles and descriptions, and more.

## Outcomes: funding and outputs connected

### Matching and analyses

Over the years, since the Grant Linking System has evolved, we have been closely watching and analysing the effects of all funding data on the global Crossref infrastructure. This [round-up of some of the community&#39;s analyses](/blog/dont-take-it-from-us-funder-metadata-matters/) shows the breadth of applications.

In 2024, we updated some matching on award IDs in publications with grants registering in Crossref, really showing the linking system in effect.

{{< figure src="/images/community-images/gls/gls-growth.png" width="75%" alt="graph showing the effects of specific funders joining that increase matches and relationships in the Crossref Grant Linking System">}}

<br>
<br>

| number of                  | as of 2023-04-16 | as of 2024-04-16 |
| :------------------------- | :--------------- | ---------------- |
| grants                     | 76,621           | 120,819 (+58%)   |
| members registering grants | 28               | 34 (+21%)        |
| matched relationships      | 98,593           | 155,475 (+58%)   |
| matched grants             | 18,114           | 27,199  (+50%)   |

See earlier reports that show the same sort of analyses of grant<->output matching as the above table, and their results with more explanation, from 2023: [The more the merrier, or how more registered grants means more relationships with outputs](/blog/the-more-the-merrier-or-how-more-registered-grants-means-more-relationships-with-outputs/); and from 2022: [Follow the money, or how to link grants to research outputs](/blog/follow-the-money-or-how-to-link-grants-to-research-outputs).

### The role of publishers

Publishers have been including funding acknowledgements in their publication metadata at Crossref for over a decade. But they did not have a persistent link to allow seamless linking between article and grant. Now with the Crossref system they do - and as more and more funders join and register grants with us, more and more publishers will start to pick these up and include them in their article (and other) metadata. In fact, all 20,000 Crossref members have a responsibility to use Crossref links wherever they can, in reference lists, on interfaces, in search engines, etc.

#### Real life example from eLife<->Wellcome of the GLS in action

This unique Crossref link `https://doi.org/10.7554/eLife.90959.3` is for an eLife article that displays another unique Crossref link (`https://doi.org/10.35802/212242`) to the Wellcome grant on the Europe PMC page.

{{< figure src="/images/community-images/gls/gls-elife-example.png" width="100%" alt="screen shots of the eLife article showing their link to the unique link created by Wellcome as part of the Crossref Grant Linking System">}}

The same example now in the metadata using the funder-designed metadata schema for relationship `is-financed-by`.

{{< figure src="/images/community-images/gls/gls-elife-metadata-example.png" width="100%" alt="screen shots of the eLife article in the metadata showing the is-financed-by relationship to a Wellcome grant, as part of the funder-design schema for the Crossref Grant Linking System">}}

That's an example of funders and their grants and publishers and their publications connecting together, using the Grant Linking System within the global open science infrastructure.

### The role of Grant Management Systems

Following Europe PMC helping Wellcome and their other funders to create Crossref XML and host landing pages on their site, and Altum's ProposalCentral integrating with Crossref since 2021, in 2024, the GLS started to see increased interest from other systems in integrating with Crossref. One such example is an open-source plugin for Fluxx, which was kindly funded by [OA.Works](https://oa.works/): https://github.com/oaworks/create-grant-doi-in-fluxx.

We're currently reviewing and supporting Crossref integrations within a number of other Grant Management Systems and will add a list of those integrations here soon. Please get in touch if you are able to contribute to this work.

## Getting started

If you're reading this far you must be about ready to get going. You'll be joining Wellcome, European Research Council, NWO - Dutch Research Council, FWF - Austrian Science Fund, FCT - Foundation for Science and Technology, Portugal, JST - Japan Science and Technology Agency, CSIRO, Melanoma Research Alliance, OSTI at the US Department of Energy, and many other Crossref funder members.

You will need to be a Crossref member in order to participate in the Grant Linking System and register unique links for your grants.

Once you're a member (or in preparation for becoming one), take a look at [our documentation on registering grants](/documentation/content-registration/content-types-intro/grants) which will walk you through what you need to know and what information you can to send to Crossref (which makes it globally and openly distributed at scale through our APIs). Some things you will need:

- Unique landing page for each grant, which should always also display the unique link
- An Open Funder Registry ID (find yours by searching for your organisation at [search.crossref.org/funding](https://search.crossref.org/funding)) and noting the ID in the URL (or ask us)
- Ability to create and send XML metadata OR allocate staff to register using the [manual form](https://manage.crossref.org/records) funded by the Moore Foundation
- Map your internal award types to the Crossref award types
- Create communications for your awardees and/or include the Grant Linking System in your agreements. We've compiled a few examples below.

### Membership & fees

Funders who would like to participate in the Grant Linking System and register their research grants should apply to [join](/membership) as a member. In some cases, your organization may already be a member - so we'll check on that for you as you may be able to register grants under you existing membership. Membership comes with obligations to maintain the metadata record for the long term; our [membership terms](/membership/terms) sets these out. You will also be able to participate in Crossref [governance](/board-and-governance/) such as voting in or standing for our annual board elections - it's very much encouraged to maintain funder voices in the oversight of Crossref. Your first year's membership invoice needs to be settled before a DOI prefix is assigned and your grant registrations can begin.

We have an introductory fee structure for funders which includes for a much lower annual membership fee (from USD $200 to USD $1200 depending on annual award value) and a higher per record fee of USD $2.00 for current grants and USD $0.30 for older grants. This fee schedule was proposed by the original Advisory Group of funders and approved by the board in 2018. It allows the cost to be budgeted into the grant itself, rather than through the often non-existent administration or operations budgets. Please see our [fees](/fees) page for more information. Please note that fees may be changed as part of the [Resourcing Crossref for Future Sustainability (RCFS) Program](/community/special-programs/resourcing-crossref/) that started in 2024.

### Communicating with grantees

Some of our members have shared their standard notifications with the rest of the funder group. Some funders include mention of the Crossref unique link in their contracts, on acceptance, and some in emailed or online guidance. Some specify how the awardee should acknowledge their funding. We've noted a few examples that can be adapted depending on your process:

#### In an email notification

> _Dear [principal investigator],_
>
>  _Through Crossref, [funder name] has assigned a globally unique identifier to your grant: https://doi.org/10.#####/#####. We ask that this link is used in all instances when referencing our funding such as when submitting a journal article, or when posting other elements of your research (e.g. preprints, data). Please enter this unique identifier link in the Award Number field (or equivalent) if one is available, and in the funding acknowledgement section of your work (sample included). The publisher can then collect it and associate it with your work._
>
>   _This will help to accurately identify and recognize any funding you have received, connect your research outputs with the grant automatically, streamline the reporting process, track the outputs of the grant. It can also boost the impact of your research and demonstrate your accomplishments to other funders and the rest of the research community._
>
>  _Please use the following text to acknowledge the funding: "This publication is based on research supported by [funder name] (open funder registry ID ##########) under the grant https://doi.org/10.#####/#####”._
>
>  _Further details are available [...link to guidance as relevant]._

#### In an agreement

>   _**Communications clause**: Any publication based on or developed under the Grant must, unless otherwise requested by the Grantor, contain an acknowledgment in the following or similar language that includes the Grantor’s open funder registry identifier and the Grant digital object identifier (DOI): "This publication is based on research supported by [funder name] (open funder registry ID ##########) under the grant https://doi.org/10.#####/#####"._

## Acknowledgements

In mid-2024, we celebrated five years of grant linking! While thanks certainly go to our [current volunteers from the funding community](/working-groups/funders), we acknowledge that the GLS would not have been possible without early dedicated time and input from the following people and organisations on our working groups for governance and fees, and for metadata modelling:

- Yasushi Ogasaka and Ritsuko Nakajima, Japan Science & Technology Agency
- Neil Thakur and Brian Haugen, US National Institutes of Health
- Jo McEntyre and Michael Parkin, Europe PMC
- Robert Kiley and Nina Frentop, Wellcome
- Alexis-Michel Mugabushaka and Diego Chialva, European Research Council
- Lance Vowell and Carly Robinson, OSTI/US Dept of Energy
- Ashley Moore and Kevin Dolby, UKRI/Medical Research Council/Research Councils UK
- Salvo da Rosa, Children's Tumor Foundation
- Trisha Cruse, DataCite

---

Please [contact our membership specialists](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) with any questions about joining, or our [technical support specialists](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) with questions about the grants schema or how to register your grants.

