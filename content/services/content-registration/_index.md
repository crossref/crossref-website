+++
title = "Content Registration"
date = "2020-04-08"
draft = false
author = "Sara Bowman"
rank = 5
[menu.main]
parent = "Find a service"
weight = 1
+++

{{< snippet "/_snippet-sources/content-registration.md" >}}

## Getting started with content registration <a id='00665' href='#00665'><i class='fas fa-link'></i></a>

Learn more about [content registration in our documentation](/education/content-registration#snippet-end).

---
