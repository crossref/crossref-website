+++
title = "Cited-by"
date = "2021-02-09"
rank = 5
aliases = [
    "/02publishers/citedby_linking.html",
    "/06members/citedby_linking_signup.html",
    "/citedby.html",
    "/citedby/",
    "/citedby/index.html"
]
[menu.main]
author = "Martyn Rittman"
parent = "Find a service"
weight = 7
+++

{{< snippet "/_snippet-sources/cited-by.md" >}}

## Getting started with Cited-by <a id='00667' href='#00667'><i class='fas fa-link'></i></a>

Learn more about [Cited-by in our documentation](/education/cited-by/cited-by-participation/).

---
