+++
title = "Reference Linking"
date = "2020-04-08"
draft = false
author = "Amanda Bartell"
rank = 5
[menu.main]
parent = "Find a service"
weight = 5
+++

{{< snippet "/_snippet-sources/reference-linking.md" >}}

## Getting started with reference linking <a id='00666' href='#00666'><i class='fas fa-link'></i></a>

See how you can find other members DOIs for your reference list in [our documentation](/documentation/reference-linking/how-do-i-create-reference-links/).

Crossref members can look up the DOIs for their references, and add the links to their articles' reference lists. Our website provides a simple text tool for manual, low volume querying, and a form for uploading a small number of reference lists as .txt files to find their DOIs (if available). However, the preferred method for most members is via XML API for individual or batch query requests.
