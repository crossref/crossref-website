+++
title = "Crossmark update policy page"
date = "2020-10-21"
draft = false
author = "Martyn Rittman"
rank = 5
weight = 10
parent = "Find a service"
+++

Crossmark is a multi-publisher initiative from Crossref to provide a standard way for readers to locate the current version of a piece of content. By participating in Crossmark and adding the Crossmark button to their content, Crossref members are committing to maintaining the content they publish, and to alerting readers to changes if and when they occur.

You have arrived at this page because you clicked on a link in a publisher's Crossmark box.  This publisher has not yet registered an Update Policy Page on their own site. An Update Policy Page should give information on a publisher's correction and retraction policies.

If you want to find out about this publisher’s guidelines for authors please visit their website or contact them.

If you are the publisher of this article please [create and register](https://www.crossref.org/education/crossmark/crossmark-policy-page/) a Crossmark policy page, and include it in your [article or journal deposits](https://www.crossref.org/education/crossmark/participating-in-crossmark/).

For assistance please contact [Crossref support](mailto:support@crossref.org).
