+++
title = "Similarity Check terms"
date = "2019-06-30"
draft = false
author = "Ginny Hendricks"
[menu.main]
parent = "Similarity Check"
Rank = 3
weight = 1
+++

{{< simcheck-terms >}}

---

If you are already a Crossref member and would like to sign up for Similarity Check, please [check whether your metadata includes what you need for Similarity Check](/services/similarity-check/#how-to-participate). If it does, that will lead you to an application form. Please contact our [support team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) with any questions.
