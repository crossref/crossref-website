+++
title = "Similarity Check"
date = "2020-04-08"
draft = false
author = "Kathleen Luschek"
rank = 9
aliases = [
    "/crosscheck/",
    "/crosscheck/crosscheck_for_researchers.html",
    "/crosscheck/crosscheck_inthenews.html",
    "/crosscheck/index.html",
    "/crosscheck_terms.html",
    "/get-started/similarity-check/",
    "/08downloads/handouts/crosscheck.pdf/",
    "/crosscheck.html",
    "/services/similaritycheck"
]
[menu.main]
parent = "Find a service"
weight = 6
+++

{{< snippet "/_snippet-sources/similarity-check.md" >}}

## Getting started with Similarity Check <a id='00669' href='#00669'><i class='fas fa-link'></i></a>

Learn more about [Similarity Check in our documentation](/education/similarity-check/participate/).

*Update 2024: We are no longer able to offer the Similarity Check service to members based in Russia. [Find out more](/operations-and-sustainability/membership-operations/sanctions/)*.

---
