+++
date = "2024-07-07"
Title = "You are Crossref"
type = "index"
image = "/images/homepage-images/parchment.jpg"
Caption = "'Paper Parchment' by Mubera Boskov, Serbia"
aliases = [
    "/02publishers/18gallery.html"
]
+++

Crossref runs open infrastructure to link research objects, entities, and actions, creating a lasting and reusable scholarly record. As a not-for-profit with over 21,000 members in 160 countries, we drive metadata exchange and support nearly 2 billion monthly API queries, facilitating global research communication, for the benefit of society.