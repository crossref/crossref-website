+++
title = "API Learning Hub"
date = "2024-03-07"
lastmod = "2024-09-13"
draft = false
author = "Luis Montilla"
image = "images/banner-images/header-keys.jpg"
#maskcolor = "crossref-blue"
#educationfront = true
aliases =[
    "/demos",
    "/tutorials/"
    ]
[menu.main]
parent = "Get involved"
weight = 60
rank = 5
+++


We want everybody to have access to the metadata in our API but we also acknowledge that this is not a trivial task and some help does not hurt. Here we will collect some of the tools and resources that our team prepare and that you can freely use to start your metadata exploring adventure.

## Choose your path:

### First steps

- **API 101 for publishers, researchers, and librarians with Postman and Crossref**: Postman offers an friendly interface to build and modify your API queries. In this collection you will find templates to which you can add or modify the parameters of your choice.
    - {{< icon name="fa-window-maximize" color="crossref-yellow" size="small" >}}&nbsp;[Postman collection](https://www.postman.com/postman-student-programs/crossref-unified-resource-api/collection/uqp9uvy/crossref-unified-resource-api?action=share&creator=29447684) | {{< icon name="fa-play" color="crossref-red" size="small" >}}&nbsp;[Video tutorial](https://www.youtube.com/watch?v=DZjAMt4wojA) | {{< icon name="fa-image" color="crossref-blue" size="small" >}}&nbsp;[Slides](https://docs.google.com/presentation/d/1B3aijqC0Cx_zfYdJTZAX-_8WDQXDRn68MRYubAt1qUM/edit?usp=sharing)
- **Intro to Crossref API using code:** if your aim is to create workflows to download, analyze, and visualize data, you will probably want to create programs and scripts. We currently have available the following tutorials using R and/or Python that you can use and modify to your convenience.
    - {{< icon name="fa-terminal" color="crossref-yellow" size="small" >}}&nbsp;[Python notebook](https://gitlab.com/crossref/tutorials/intro-to-crossref-api-using-python) | {{< icon name="fa-terminal" color="crossref-red" size="small" >}}&nbsp;[R notebook](https://crossref.gitlab.io/tutorials/intro_to_cr_api/)

### Retrieve specific metadata

- **Crossref API for funding data:** how to query data from the funders endpoint and grant-type records.
    - {{< icon name="fa-terminal" color="crossref-yellow" size="small" >}}&nbsp;[R dashboard](https://crossref.gitlab.io/tutorials/cr-funders-r-workbook)

- **Get Crossref citations:** this project contains a Jupyter notebook that shows how to compare citation counts from different Crossref endpoints.
    - {{< icon name="fa-terminal" color="crossref-yellow" size="small" >}}&nbsp;[Python notebook](https://gitlab.com/crossref/tutorials/get-crossef-citations)

- **Get journal-level metadata from Crossref’s API using R:** how to retrieve journal-level metadata from a list of ISSN.
    - {{< icon name="fa-terminal" color="crossref-yellow" size="small" >}}&nbsp;[Notebook](https://crossref.gitlab.io/tutorials/get-journal-metadata/)

- **Get Retraction Watch metadata from Crossref’s API:** how to retrieve Retraction Watch and general updates.
    - {{< icon name="fa-terminal" color="crossref-yellow" size="small" >}}&nbsp;[R notebook](https://crossref.gitlab.io/tutorials/get-rw-metadata)

- **Cursor-based pagination using R:** how to retrieve long lists of records using cursors.
    - {{< icon name="fa-terminal" color="crossref-yellow" size="small" >}}&nbsp;[R notebook](https://crossref.gitlab.io/tutorials/cursor-based-pagination/)

### Retrieve all the metadata
You have an alternative to the REST API if your goal is to obtain the entire body of Crossref’s records. With the public data file you have access to every DOI ever registered with Crossref. Learn more about the [Public Data File](/public-data-file)   

## Some convenient tools:

- [API cheatsheet](https://doi.org/10.13003/7nry-h9tg): we prepared a quick-reference sheet that you can use to get started with building your queries.

- JSON-file viewer: When you make a request to the REST API you will get a [JSON file](https://en.wikipedia.org/wiki/JSON) in the output. If you are making requests from your web browser and depending on its version, perhaps you will need a JSON-viewer plugin. For example, click this [simple request](https://api.crossref.org/types). If you see a string of seemingly disorganized text, you will need to install a plugin. Alternatively, you can use other viewers such as [JSON hero](https://jsonhero.io/), which provides an extra layer of interactivity.

## Related resources

- [Crossref Unified Resource API](https://api.crossref.org/): Here you will find a detailed list of the REST API endpoints, and parameters that you can use.
- [Tips for using the Crossref REST API](https://www.crossref.org/documentation/retrieve-metadata/rest-api/): This section from our documentation should be one of the first resources you should check for your API journey. 
- [Crossref Gitlab tutorial repository](https://gitlab.com/crossref/tutorials): Download the source code of our selection of tutorials.

