+++
title = "Crossref Public Data File"
date = "2024-03-07"
lastmod = "2024-10-15"
draft = false
author = "Luis Montilla"
image = "images/banner-images/ambassador-banner-fish.jpeg"
#maskcolor = "crossref-blue"
#educationfront = true
aliases =[
    "/demos",
    "/tutorials/",
    "/public-data-file/"
    ]
+++

You have an alternative to the REST API if your goal is to obtain the entire body of Crossref’s records. With the public data file you have access to every DOI ever registered with Crossref.

### 2024 Public data file

- {{< icon name="fa-file-text" color="crossref-yellow" size="small" >}}&nbsp;[Release notes](https://www.crossref.org/blog/2024-public-data-file-now-available-featuring-new-experimental-formats/).
- {{< icon name="fa-database" color="crossref-red" size="small" >}}&nbsp;[Public data file](https://academictorrents.com/details/4426fa56a4f3d376ece9ac37ed088095a30de568).
- {{< icon name="fa-table" color="crossref-blue" size="small" >}}&nbsp;[Data file sample](https://academictorrents.com/details/4426fa56a4f3d376ece9ac37ed088095a30de568).

## [Tips for working with Crossref public data files and Plus snapshots](https://www.crossref.org/documentation/retrieve-metadata/rest-api/tips-for-using-public-data-files-and-plus-snapshots/)

## Important considerations:
- The records are in JSON.
- Metadata is supplied by our members and, as such, not all records have the same completeness or quality of metadata.
- Every year our metadata corpus grows. The first data file was 65GB and held 112 million records; this year the file weighs in at 212 GB and contains metadata for 156 million records, or all Crossref records registered up to and including April 2024.
- Decompressing the .tar.gz files will take you several hours.

## Additional convenient tools:

Given the size and the amount of files that the public data files comprises, we started experimenting with some additional tools to improve access to the data and repack the data into additional formats:

- {{< icon name="fa-wrench" color="crossref-yellow" size="small" >}}&nbsp;[Packer:](https://gitlab.com/crossref/labs/packer) A python application that allows you to repack the Crossref data dump into JSON-L.
- {{< icon name="fa-wrench" color="crossref-red" size="small" >}}&nbsp;[dois2SQLite:](https://gitlab.com/crossref/labs/dois2sqlite) This tool will help you load Crossref metadata into a SQLite database.
- {{< icon name="fa-wrench" color="crossref-blue" size="small" >}}&nbsp;[Crossref Data Dump Repacker: Rust Edition](https://gitlab.com/crossref/labs/rustsqlitepacker) A Rust application that allows you to repack the Crossref data dump into SQLite.
- {{< icon name="fa-wrench" color="crossref-green" size="small" >}}&nbsp;[API for Interacting with the Crossref Annual Data File:](https://gitlab.com/crossref/labs/labs-data-file-api) A python API for interacting with the Crossref Annual Data File dump, allowing you to build various indexes for working with the annual data dump from Crossref.

These tools are **experimental**, so please remember that we they are released without warranties and support, but are happy to hear about your experience using them. You can read more about them in [this blog post](https://www.crossref.org/blog/increasing-crossref-data-reusability-with-format-experiments/)

## Previous releases
(Click to expand)   

{{% accordion %}}
{{% accordion-section "2023" %}}

- {{< icon name="fa-file-text" color="crossref-yellow" size="small" >}}&nbsp;[Release notes](https://www.crossref.org/blog/2023-public-data-file-now-available-with-new-and-improved-retrieval-options/)
-  {{< icon name="fa-database" color="crossref-red" size="small" >}}&nbsp;[2023 March public data file (185GB)](https://academictorrents.com/details/d9e554f4f0c3047d9f49e448a7004f7aa1701b69)

{{% /accordion-section%}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "2022" %}}

- {{< icon name="fa-file-text" color="crossref-yellow" size="small" >}}&nbsp;[Release notes](https://www.crossref.org/blog/2022-public-data-file-of-more-than-134-million-metadata-records-now-available/)
-  {{< icon name="fa-database" color="crossref-red" size="small" >}}&nbsp;[2022 April public data file (167GB)](https://academictorrents.com/details/4dcfdf804775f2d92b7a030305fa0350ebef6f3e)

{{% /accordion-section%}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "2021" %}}
- {{< icon name="fa-file-text" color="crossref-yellow" size="small" >}}&nbsp;[Release notes](https://www.crossref.org/blog/new-public-data-file-120-million-metadata-records/)
-  {{< icon name="fa-database" color="crossref-red" size="small" >}}&nbsp;[2021 January public data file (102GB)](https://academictorrents.com/details/e4287cb7619999709f6e9db5c359dda17e93d515)
{{% /accordion-section%}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "2020" %}}
- {{< icon name="fa-file-text" color="crossref-yellow" size="small" >}}&nbsp;[Release notes](https://www.crossref.org/blog/free-public-data-file-of-112-million-crossref-records/)
-  {{< icon name="fa-database" color="crossref-red" size="small" >}}&nbsp;[2020 March public data file (67GB)](https://doi.org/10.13003/83B2GP)
{{% /accordion-section%}}
{{% /accordion %}}