+++
title = "Blocked IP"
date = "2019-02-14"
author = "Isaac Farley"
Weight = 15
rank = 2
+++

Hello. We're delighted that you're using our API and we hope you find it useful.

Unfortunately, your usage also seems to be causing some problems with the API, which, in turn, is preventing other users from accessing it as well.

Therefore we have temporarily blocked your access. Please contact us at [support@crossref.org](mailto:support@crossref.org). We’ll be happy to help you optimize your system to make the best, unobtrusive use of our API.

Also take a look at these [API tips](/documentation/retrieve-metadata/rest-api/tips-for-using-the-crossref-rest-api/) and the etiquette section of our [API documentation](https://api.crossref.org) for more tips on using the system 'politely'.
