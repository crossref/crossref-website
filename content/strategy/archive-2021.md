+++
title = "Strategic agenda 2021-2022"
date = "2021-05-28"
draft = false
image = "/images/blog/crossref-strategic-direction-star-go-kart.jpg"
author = "Ginny Hendricks"
parent = "About us"
weight = 15
rank = 1
+++


{{% divwrap darkgrey-highlight %}}
### This is our strategic agenda from 2020-2022 and it's now archived, along with the previous [2018-2022 version](/strategy/archive-2018), please visit the main [strategy page](/strategy) for the current version.
{{% /divwrap %}}

Welcome to our new strategic narrative---published June 2021---which sets out our agenda through 2025. It encompasses all our plans, from governance and sustainability, our work with different parts of the scholarly community, through to new and existing product development as well as strategic initiatives.

We have six overarching goals, two more than the [previous set, now archived](/strategy/archive-2018); 'bolster the team' and 'live up to POSI' are the 2021 additions. Historic [strategy information](/categories/strategy/) can be found on our blog.

Read on to learn more about where the Crossref community is heading and let us have your thoughts by [starting or joining a discussion](https://community.crossref.org/c/strategy) in the strategy section of our community forum.



{{% row %}}
{{% column %}}
<a href="/strategy/#bolster-the-team">  <img src="/images/community-images/bolster.svg" alt="Bolster the team" width="100%" align="left" /> </a>
<br>

<a href="/strategy/#live-up-to-posi">  <img src="/images/community-images/live.svg" alt="Live up to POSI" width ="100%" align="left"/> </a>
<br>

<a href="/strategy/#engage-with-expanding-communities">  <img src="/images/community-images/engage.svg" alt="Engage with expanding communities" width="100%" align="left"/> </a>


{{% /column %}}
{{% column %}}

<a href="/strategy/#improve-our-metadata">  <img src="/images/community-images/improve.svg" alt="Improve our metadata" width ="100%" align="left" /> </a>
<br>

<a href="/strategy/#collaborate-and-partner">  <img src="/images/community-images/collab.svg" alt="Collaborate & partner" width ="100%" align="left"/> </a>  
<br>
<a href="/strategy/#simplify-and-enrich-services"> <img src="/images/community-images/simplify.svg" alt="Simplify & enrich services" width ="100%" align="left"/>  </a>  

{{% /column %}}
{{% /row %}}

---

<br>
{{% divwrap darkgrey-highlight %}}
## Bolster the team
###  This theme is all about people, support, culture, and resilience.
{{% /divwrap %}}

<br>

In recent years, Crossref has grown in every way: members, users, registered content, and staff. We see no indication of things slowing down which is usually seen as a positive, but we need to get into a position where we can **project and manage growth** more purposefully, and catch up with our **technical debt** which will support our staff and community in improving scholarly communications.

All teams have manual processes that need to be automated, and a continued priority is to ‘**reduce toil**’ for our staff, and therefore for our members and users. Support queries have grown as we’ve introduced new services and tools (along with the volume of content and members). Having needed to recruit contractors for technical support and membership, we need to carefully consider what **resourcing** Crossref will require if we continue to grow as we have been before we achieve our other goal to radically simplify our services. We won’t be able to meet our community members in their own (now numbering 140) countries, and while we can reach a breadth of people online, we miss the depth of understanding that meeting face-to-face can give. So we need **new ways to hear from and understand many varied needs**. We will organise our communications and engagement activities under a new **community playbook** where we have a programming and co-creation approach to content, events, and supporting **languages other than English**.

As our staff have become more distributed, and our organization is **adapting to changes in work culture**, we want to improve the way we communicate across teams and timezones, and improve how we make decisions and track progress towards the goals set out here. Our **recruitment practices are being honed** to better reflect our inclusive values, and we are **improving the new hire ‘welcome’ experience**.

Strong leadership and **organizational resilience** are priorities and activities such as knowledge transfer, documentation, succession planning, risk management, and identification of critical functions, will all help to **keep Crossref thriving**. {{% logo-inline %}}

---

<br>

{{% divwrap orange-highlight %}}
## Live up to POSI
### This theme is stated because we want to be held publicly accountable to the Principles of Scholarly Infrastructure standards of governance, insurance, and sustainability.
{{% /divwrap %}}

<br>

Our board adopted [POSI](https://openscholarlyinfrastructure.org) in November 2020 and we published a [self-assessment](/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure/) soon after. We meet most of the 16 principles and this goal keeps us working towards the others as well as maintaining those we are doing well in.

The one principle we meet least well is broad stakeholder governance. A stakeholder is any person or organisation that would be affected by decisions made by Crossref. That includes anyone who has ever clicked on a Crossref DOI. So---starting with research funders---we will actively work to **broaden the governance** of the organisation. In 2021, the board gave guidance to the nominating committee to add at least one funder to the election slate.

A number of projects in this section center around **transparent operations**. While we have a lot of information here on our website, some of it is not easy to find and some of it we've never thought to publish. So we will work to digitise things such as our financial updates and we've introduced a **[public product roadmap](http://bit.ly/crossref-roadmap)** where we are updating information more frequently throughout the year. Governance and sustainability information will be added to the website, the practices we follow to onboard as well as 'off-board' accounts, as well as recruitment policies and staff handbooks.

On the technical side we will continue to release public data files periodically, we’ll **clarify licenses** of the different kinds of metadata, services, and tools that we have. And we will aim to continue to **open-source** the majority of our code, bug reporting, and issue tracking. Over time, support will be lifted out of a 1:1 setting and into the open, via our **community forum**. All of these activities---and the mindset that goes along with them---will ensure that Crossref can be 'forked' if anything goes badly wrong.

---

<br>

{{% divwrap blue-highlight %}}
## Engage with expanding communities
### This theme centers on scale, strengthening relationships, community facilitation, and communications/content.
{{% /divwrap %}}

<br>

Crossref has enjoyed incredible growth for many years and we need to be able to predict what’s coming next and **project the scale** of Crossref some years down the line. We know that not all content has a DOI (even with other agencies) and we know that many countries and organizations still do not find Crossref accessible. The priority activities under this goal include **gap analyses** work with organizations such as DOAJ to predict **future growth areas**. We will also be conducting a **review of our revenue**, its current distribution, historical trends, and **capacities for growth across our services**, all in line with our mission and the POSI sustainability definition.

Crossref currently has members in 140 countries and we interact with people in 160. With that comes the need to increasingly and proactively involve emerging markets as they begin sharing research outputs globally. We are regularly reassessing the ways that we manage the long tail of new members, our relationship with and management of **[Sponsors](/community/sponsors)** (also growing in number), and how we have structured our teams to support this growth.

We include emerging markets in our training and events, and we will develop as much **content in languages other than English** as we can. We are also exploring **removing barriers to participation for US-sanctioned countries**. We work with government education/science ministries and local **Sponsors** and **Ambassadors**, and are increasing our efforts to do more **public support** via the **[community forum](https://community.crossref.org)**. We are also creating more bitesize and visual/audio resources and taking a **programming approach to our online events**.

We will re-establish **a practice of community consultation** where possible, involving people in our priorities and plans, refreshing or revising community groups, starting with one for **metadata users** as a key and growing group of stakeholders.

Furthermore, **funders** and **research institutions** are increasingly interested in supporting open scholarly infrastructure and have increased their engagement with Crossref in recent years. **Funder’s service providers** are already supporting the registration and use of grant metadata and our outreach activities with them will expand. **Library-publishing** infrastructure is a growing interest and activity. Our overarching objective is to extend our value proposition to convince these new constituents of Crossref’s relevance, getting them into our system and using our infrastructure.

---

<br>

{{% divwrap red-highlight %}}
## Improve our metadata
### This theme involves researching and communicating the value of richer, connected, and reusable, open metadata, and incentivising people to meet best practices, while also making it possible (and easier) to do so.
{{% /divwrap %}}

<br>

Increased **metadata quality** is an ongoing effort and will also be the result of meeting other goals, particularly engaging with new communities (to understand new workflows and record types) and simplifying our services (to provide the data and tools to make better metadata creation easier and reduce manual interventions by our staff).

We will commission a **metadata ‘reach and return’** study to understand and analyse the outcomes of investing in quality open metadata, and the benefits to members and others. We will convene **metadata users in a new user group** and we are actively involved in the follow-on phase **Metadata 20/20: Your Turn**. We will set out the Crossref metadata strategy and create new **best practice documentation**, incentivising good practice through **appropriate fees** and **actionable participation reports**. We have also started a **partnership program** to work 1:1 with select organisations whose improvement would see the greatest improvement for everyone.  

We are improving support for standards including **JATS** for Content Registration, and will provide clear **guidelines for service providers**. We need to ensure members (and their Service Providers and Sponsors) can register metadata more easily, and we will create a new process to **crowdsource corrections and error reporting** and make it easier for members to act on those reports.

By working to understand new workflows, and applying this knowledge in a **revised community data model**, we will further develop the **research nexus** to expand the links between more research objects and plan for new relationships and record types such as protocols, videos, and conferences. We will **implement schema 5.0**—for both input and output—to enable members to include **CRediT** and **ROR** for affiliations.

Crossref is now a recognized source of scholarly metadata for thousands of services across multiple communities so we will **maintain a clear provenance trail of every metadata assertion**, assisted by **Event Data**. And we will continue to work on end-to-end metadata distribution through our public REST API, moving from solr to elasticsearch, while **maintaining and growing the Metadata Plus service**.

---

<br>

{{% divwrap green-highlight %}}
## Collaborate and partner
### We've always collaborated but we want to work even more closely with like-minded organisations to solve problems together.
{{% /divwrap %}}

<br>

This theme cuts across all of our other goals like engaging with new and existing communities, simplifying services, and improving our metadata.

Crossref did not develop its mission alone, and we cannot meet and exceed these goals alone. The essence of our vision is shared by many organizations and initiatives:

> We envision a rich and reusable nexus of metadata and relationships connecting all research objects, organizations, people, and actions; an open scholarly infrastructure that the global community can build on forever, for the benefit of society.

To **realize this vision**, we must deepen and cement partnerships with like-minded initiatives and organisations such as DataCite, ORCID, and ROR, to name but a few, ideally reducing the burden of supporting us or our mutual stakeholders. We want to **co-create and co-develop** and **encourage integrations** that solve problems for the whole community.

We will collaborate on outreach and technical development and improve and extend integrations such as **ORCID Auto-update**. We will support for **DataCite’s ‘commons’** work for a joint discovery experience, our **shared Event Data infrastructure**, and in promoting **data citation**. We will continue to spend time and effort **co-governing ROR and ensuring its sustainability**, and we will continue our partnership with the **Public Knowledge Project** to build plugins for the thousands of Crossref members who use OJS. Our new partnership with **DOAJ** means we can **lower barriers to participation** for small and emerging constituencies.

In order to invest in new and upcoming community initiatives, we are also **expanding our R&D focus** (Labs is back!).

---

<br>

{{% divwrap yellow-highlight %}}
##  Simplify and enrich services
### This goal is all about focus. And about delivering easy-to-use tools that are critically important for our community.
{{% /divwrap %}}

<br>

If we are to reduce ‘toil’ for our staff and users, handle growth and engage with new communities, expand our support for the research nexus, improve metadata, and collaborate with others... we need to radically simplify our services, starting with **unifying Content Registration tools** and **deprecating old tools and APIs** as we go.

In order that we can plan, develop, test, deliver, maintain, and update our services faster and more reliably, we are **untangling years of code** and rules. We have and will continue focusing on **knowledge transfer**, **documentation**, and creating a **robust test environment**. We will extract and refactor major subsystems (Content Registration, matching, billing, monitoring, reporting) and we will build this modern and extensible architecture around a set of capabilities...

Under a concept known as **‘My Crossref’**, we will create my.crossref.org. We need to better understand our users, starting with **a new authentication module** for all tools and for different layers of permissions to reflect all the ways our users work with Crossref on behalf of each other. We will develop **linked and actionable reports** to help both users and ourselves to make decisions, improve metadata, and increase participation. We will add to the **research nexus** of relationships, bringing together the technologies we have for **matching and linking all objects** in the research nexus and extending our support for additional record types and metadata, including additional **provenance tracking**. We are working on creating **an open crowdsourced facility for updates/corrections** for metadata enrichment, an **activity and notification capability**, and we'll **unify designs of all Crossref UIs** based on usability, accessibility, responsiveness, and internationalization.

My Crossref will also allow **self-service** for toilsome tasks such as **title transfers** and **bulk URL updates**, reducing the pressure on our teams and our community. Our new elasticsearch-backed REST API will be more closely aligned with our metadata input, and we will **disseminate all metadata, starting with grant records**.

Apart from Content Registration and all the related activities above, we have work to do to introduce and support **Similarity Check's iThenticate V2** including training materials, member migration, revenue planning, and a future needs analysis.

Event Data is an important part of our strategy as that exposes multiple relationships and extends the research nexus beyond member-defined connections. We have **stabilised and relaunched Event Data**, and we'll also add to its sources in time. We will **monitor and grow our Metadata Plus service**, which includes high rate limits and monthly snapshot downloads of all metadata, and with Crossmark we will start to conceptualise a more **accessible widget to combine with DOI display guidelines**.
