+++
title = "Strategic agenda and roadmap"
date = "2024-10-16"
draft = false
author = "Ginny Hendricks"
image = "/images/banner-images/forum-starlings.jpg"
[menu.main]
categories = ["Strategy"]
tags = ["Strategy"]
parent = "About us"
weight = 15
rank = 5
+++

Welcome to our strategic roadmap---finalised January 2023---which sets out Crossref's priorities through 2025 so everyone can see our focus areas and upcoming projects.

Like others, we envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society.

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better.  We **rally** the community; **tag** and share metadata; **run** an open infrastructure; **play** with technology; and **make** tools and services---all to help put scholarly content in context.

 This page presents all our high-level activities, from open governance and sustainability, collaborative projects with different parts of the ever-diversifying scholarly community, work to expand metadata, and delivery of tools and APIs to retrieve works, entities, and their relationships---all while fostering a strong global team.

Read on to learn more about where Crossref is heading and let us have your thoughts by [starting or joining a discussion](https://community.crossref.org/c/strategy) in the strategy section of our community forum. Review the archived strategic narratives for [2020-2022](/strategy/archive-2021) and [2018-2020](/strategy/archive-2018), and read background on our [strategic planning approach](/blog/the-road-ahead-our-strategy-through-2025) on our blog. 

<br>

> View our live and detailed product roadmap at the [bottom of this page](#live-product-roadmap)

{{% strategy-section wrapper %}}

{{% strategy-section %}}

## Crossref's role in the scholarly landscape

Governments, funders, institutions, and researchers---groups who once had tangential involvement in scholarly publishing---are taking a more direct role in shaping how research is recorded, shared, contextualised, and assessed.

We now have more members that self-identify as universities or research institutions than as publishers, and we have seen a rise in library- and academic-led publishing. Many research funders are playing their part by supporting open infrastructure, registering their records with Crossref as members, and seeing this as a direct way of measuring reach and return on their grants and awards.

As more people contribute to an evolving scholarly record, Crossref must capture provenance and relationships through metadata, and broaden it's scope even further to collect, clean, and deliver metadata in context.

We are scaling our systems, tools, and resources to manage this and we are doing it in the open, to demonstrate our committment to [POSI](https://openscholarlyinfrastructure.org/) and add some assurance that research is being properly supported, and so we can more easily integrate and co-create with other open infrastructures, whilst making it easy for members to participate and share as much metadata as they have.

With a more complete picture of the scholarly record available in the open, everyone will be able to examine the integrity, impact, and outcomes of our collective efforts to progress science and society.

{{% strategy-table table landscape %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### [Contribute to an environment where the community identifies and co-creates solutions for broad benefit](#we-want-to-contribute-to-an-environment-in-which-the-scholarly-research-community-identifies-shared-problems-and-co-creates-solutions-for-broad-benefit)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### [A sustainable source of complete, open, and global scholarly metadata and relationships](#we-want-to-be-a-sustainable-source-of-complete-open-and-global-scholarly-metadata-and-relationships)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### [Manage Crossref openly and sustainably, modernising and making transparent all operations so that we are accountable to the communities that govern us](#we-want-to-manage-crossref-openly-and-sustainably-modernising-and-making-transparent-all-operations-so-that-we-are-accountable-to-the-communities-that-govern-us)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### [Foster a strong team---because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it](#we-want-to-foster-a-strong-team---because-reliable-infrastructure-needs-committed-people-who-contribute-to-and-realise-the-vision-and-thrive-doing-it)

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="create" %}}

{{% divwrap service-blue %}}

## We want to contribute to an environment in which the scholarly research community identifies shared problems and co-creates solutions for broad benefit

> _We do this in all teams through research and engagement with our expanding global community, responding to and leading trends in scholarly communications._

{{% /divwrap %}}

Some problems benefit from collective action. As scholarly communications evolves, we need to be proactive within our community to understand how we can help solve shared problems. We continue to focus on collaboration with new and long standing partners, developing programs through cross-team working groups to allow us to move nimbly and involve the community as we do.

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

* Announced plans for [merging Funder Registry into ROR](/blog/open-funder-registry-to-transition-into-research-organization-registry-ror/)
* [Community consultation](https://doi.org/10.31222/osf.io/6z7s3) about Crossmark
* Explored [preservation coverage](https://gitlab.com/crossref/labs/preservation-database) among Crossref membership and partnering with preservation services
* Released [Participation Reports v1.1](/blog/re-introducing-participation-reports-to-encourage-best-practices-in-open-metadata/) together with CWTS at Leiden University

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus

* Extend engagement around our [Integrity of the Scholarly Record (ISR)](/categories/research-integrity/) program with editors, institutions, and research integrity sleuths
* [Develop Similarity Check](/categories/similarity-check/) to improve the user experience and add features
* Grow [adoption](https://api.crossref.org/types/grant/works?rows=0&facet=funder-name:*) of the [Grant Linking System (GLS)](/services/grant-linking-system/) with more funders becoming members
* [Align the Open Funder Registry with ROR](/categories/open-funder-registry/)
* Work with the [Barcelona Declaration](https://barcelona-declaration.org/) signatories to prioritise open metadata efforts 
* Expand and relaunch our [Service Provider](/community/service-providers/) program

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Up next

* [Extend support for grant metadata](https://docs.google.com/document/d/1CVB9UzsRVj5Hr_yDj7LEz3jlivSopeZTcn3AMJzGipo/edit?usp=sharing)
* Run a series of API sprints/hackathons to encourage the community to build tools with Crossref metadata.

{{% /strategy-table %}}

{{% strategy-table cell %}}

### Under consideration

* Exploring: How do we more efficiently gather metadata corrections and notify the community?
* Exploring: Monitoring research integrity community developments and tools we could align with

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="nexus" %}}

{{% divwrap service-red %}}

## We want to be a sustainable source of complete, open, and global scholarly metadata and relationships

> _We are working towards this vision of a ‘Research Nexus’ by demonstrating the value of richer and connected open metadata, incentivising people to meet best practices, while making it easier to do so._

{{% /divwrap %}}

Building a more complete picture of the scholarly record means thinking about our metadata outside the more rigid structures once provided by content containers. In line with community needs, we are building flexibile, clearer assertions of metadata provenance, and charge ourselves with improving the accuracy, transparency, and downstream usage of the metadata we collect and ingest from a range of sources. We will support our members in improving the provision of key metadata fields so that they can easily contribute to the growing network of metadata and relationships. 

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

* [Celebrated five years of the Crossref Grant Linking System (GLS)](/blog/celebrating-five-years-of-grant-ids-where-are-we-with-the-crossref-grant-linking-system/)
* Launched Crossref-hosted interim pages for grants (see [example](https://record.crossref.org/10.46936/cpcy.proj.2019.50733/60006578))
* Shared [metadata development plans](/blog/metadata-schema-development-plans/) in response to community feedback
* [Retired the inconsistent and unreliable subject codes](/blog/subject-codes-incomplete-and-unreliable-have-got-to-go/) and removed them from the REST API



{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus

* Broad adoption of the [GEM Program](/gem) to include more of the world's metadata
* [Surveying small members and consulting with non-member journals](https://community.crossref.org/c/strategy/rcfs/47) to better understand fee accessibility
* Extending [record registration form](https://crossref.atlassian.net/browse/CR-1333) to accept articles and other research objects; planning to retire Metadata Manager tool as a result
* Developing clear metadata development strategy, priorities, and [roadmap](https://trello.com/b/JaB7xxgw/crossref-metadata)
* Hosting a series of [webinars](/events/metadata-health-check-webinars/) to support members’ metadata quality improvements with Participation Reports
* Co-creating interactive resources for using metadata, e.g. [interviews](/categories/api-case-study/), demos, and [tutorials](https://community.crossref.org/tag/tutorial) for working with our API
* Publishing a [blog series on metadata matching](/categories/metadata-matching/)
* Investigating strategies for matching affiliation strings to ROR IDs
* Developing a proof-of-concept work for [DOI assignment and management with static site generators](https://crossref.atlassian.net/browse/RD-15)
* Ongoing bug fixes and metadata completeness checks for our REST API

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Up next

* Metadata schema updates to include citation types, versions, and contributor roles - [share your input and feedback!](https://community.crossref.org/t/request-for-feedback/6610)
* Improvements to the design and functionality of the [Crossmark button](/services/crossmark/) following community consultation
* [Merge the Open Funder Registry into ROR](/blog/open-funder-registry-to-transition-into-research-organization-registry-ror/)
* [Incorporate new preprint matching approach in our API](/blog/discovering-relationships-between-preprints-and-journal-articles/)
* Incorporate Retraction Watch data into our API


{{% /strategy-table %}}

{{% strategy-table cell %}}

### Under consideration

* Develop a training program and resources on metadata registration best practices
* Extend the list of metadata elements that can be registered using our helper tools

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="posi" %}}

{{% divwrap service-yellow %}}

## We want to manage Crossref openly and sustainably, modernising and making transparent all operations so that we are accountable to the communities that govern us.

> _We do this by actively broadening board representation, managing the finances responsibly and openly, and improving organisational resilience through modernising systems, processes, and policies._

{{% /divwrap %}}

We invest time and resources in embedding open practices in our organisation so that you know what we know. We continuously address technical debt and work to make the infrastructure robust and resilient for the future. We use POSI as a decision framework to help improve ways of working and together with the other adoptees, we uncover more ways to be open and accountable to the community as we evolve.

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

* Improved 'broad stakeholder governance', and progressed 'open data' (See the [self-assessments](/categories/posi/))
* Released the [2024 public data file](/blog/2024-public-data-file-now-available-featuring-new-experimental-formats/)
* Reached our 12-month contingency goal (see: [Sustainability](https://openscholarlyinfrastructure.org/))
* Worked with the other [POSI-adopting infrastructures](https://openscholarlyinfrastructure.org/posse/) to release [v1.1 of the Principles](https://upstream.force11.org/principles-of-open-scholarly-infrastructure-posi-version-1-1-reflections-from-adopters/) together
* [Published our employee manuals and policies publicly](/operations-and-sustainability/working-at-crossref/) for transparency

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus

* Compiling latest self-assessment update on POSI compliance
* Drafting patent non-assertion policy
* Updating [our by-laws](/board-and-governance/bylaws/) to create a new category of membership to broaden participation
* Increasing transparency of more of our employment practices and general operations
* Migrate all databases to open-source database management systems
* Share data, software, and methods used in recent [metadata matching explorations](/categories/metadata-matching/) for reuse and reproducibility by the community 

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Up next

* Move from data center to cloud
* Establish test environments in AWS
* Reconsider the structure of our REST API [to gain efficiencies](/blog/rebalancing-our-rest-api-traffic/)
* Decouple and open up additional parts of our legacy, closed codebases
* Conduct penetration tests in 2025 and publish a report on the findings

{{% /strategy-table %}}

{{% strategy-table cell %}}

### Under consideration

* Exploring: Is our governance structure representative and are the processes efficient? (see: [Governance](https://openscholarlyinfrastructure.org/))
* Continue opening up our [technical support conversations and processes](/categories/open-support/) 

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="team" %}}

{{% divwrap service-green %}}

## We want to foster a strong team---because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it

> _We do this through fair policies and working practices, a balanced approach to resourcing, and accountability to each other._

{{% /divwrap %}}

Our commitment to collaboration and transparency in the community is reflected in how we operate as a team as well. By making our people operations more transparent, we can ensure that our approach is applied consistently and equitably; potential candidates can get a sense of how we operate; and other organisations can adapt and reuse policies if they wish.

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

* Reviewed and revised our [leadership structure](/people/org-chart/) to set up our teams to reflect and accommodate Crossref's growth and expansion
* Agreed and published our new [travel and events](/blog/rethinking-staff-travel-meetings-and-events/) policy
* Reviewed recruitment and compensation practices

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus

* [Hiring for two key leadership positions](/jobs/) to strengthen our Operations and Programs groups
* Pursuing a program to better understand how resourcing supports [Crossref's sustainability](/community/special-programs/resourcing-crossref/)
* Setting up cross-functional program teams to manage our work more collaboratively
* Continuously (re-)prioritizing our [product roadmap](https://roadmap.productboard.com/e6fdeba8-a5b3-4aef-8104-d48863ba975e) using a rubric of 12 prioritization drivers
* Automating membership processes to support continued growth
* Tracking staff carbon emissions and considering ways to reduce our environmental impact

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Up next

* Plan 2025 in-person all-staff event
* Evolve the R&D function into a new Data Science function whose focus will be to analyse and improve how we collect, match, and deliver scholarly metadata
* Close our last remaining physical office in Lynnfield, MA (USA)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### Under consideration

* Exploring: how do we adapt to working in a fully remote environment as a growing organization?

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% /strategy-section %}}

<br>

## Completed activities

Recently completed activities are listed on our strategic roadmap above. The following projects and initiatives have been completed since [we last revisited our high-level strategy](/blog/the-road-ahead-our-strategy-through-2025) but are no longer considered recent:

* [Acquired and opened Retraction Watch database](https://www.crossref.org/blog/news-crossref-and-retraction-watch/); entered an agreement to grow the service together with Center for Scientific Integrity 
* [Brought operational management of ROR in-house](https://ror.org/blog/2022-10-10-strengthening-sustainability/), shared across DataCite, CDL, and Crossref
* Added metadata records with [ROR IDs to our API](/blog/a-ror-some-update-to-our-api/)
* Launched [Global Equitable Membership (GEM) program](/blog/introducing-our-new-global-equitable-membership-gem-program/)
* [Opened references by default](/blog/amendments-to-membership-terms-to-open-reference-distribution-and-include-uk-jurisdiction/#reference-distribution-preferences)
* Released [new form](/documentation/register-maintain-records/grant-registration-form/) for funders to register grant records
* [New recommendations from the Preprints Working Group](https://community.crossref.org/t/share-your-thoughts-on-preprint-metadata/2800)
* Discovery work on [metadata matching and relationships](https://gitlab.com/crossref/labs/marple)
* Investigated [patterns in and matching of article and preprint metadata](https://crossref.atlassian.net/browse/RD-14)
* Explored [preservation coverage](https://gitlab.com/crossref/labs/preservation-database) among Crossref membership
* Developed a framework to better track our carbon emissions
* Developed v2 of the experimental [Labs participation reports](https://prep.labs.crossref.org/)

## Paused activities

We are not currently actively pursuing the following projects, either because other things have taken priority or because they have been taken as far as they can for now:

* Implement 'item graph' to reflect nuanced relationships and to ensure future schema flexibility
* Further work on a new [API endpoint for relationships](https://community.crossref.org/t/relationships-are-here/3523) including providing a dataset of data citations
* [Build a bridge API](https://crossref.atlassian.net/browse/CR-1204) to surface information about billing and other internal operations
* Build a [test environment](https://gitlab.com/crossref/labs/lambda-api-proxy/-/blob/main/crapiproxy/docs/DEPOSIT.md) for metadata schema updates


## Live product roadmap

See the full link at [Productboard](https://roadmap.productboard.com/e6fdeba8-a5b3-4aef-8104-d48863ba975e).

<iframe src="https://roadmap.productboard.com/e6fdeba8-a5b3-4aef-8104-d48863ba975e" width="100%" height="1000" frameborder="0"></iframe>
