+++
title = "Web Style Guide"
name = "Web Style Guide"
date = "2022-11-09T12:00:00-00:00"
draft = false

+++

## Web Style Guide: matching the style of the new Crossref website in external applications

Note (as of Feb 2021) that this was created in 2017 and needs to be reviewed by UI/frontend colleagues.

This page provides information on how to style external applications to match the visual style of the new Crossref website using HTML snippets and existing stylesheets.

All pages are compiled from templates that are contained within the [/themes/crossref/layouts](https://github.com/CrossRef/cr-site/tree/master/themes/crossref/layouts) directory of the GitHub repository for the site. Although they include proprietary tags (look for the curly brackets) used by Hugo the static site generator, Hugo, for the most part layout HTML should be clear. **Do not make changes to the template files contained here.**

You may also wish to simply use a browser to view source on any page in the site with a template that is suitable for your use, and pull whatever you need from there.  

### Stylesheets, fonts, layout
The new site uses Bootstrap for layout, and stylesheets are written in SASS. The font used was Helvetica Neue but was changed to [Inter](https://rsms.me/inter/) in 2023. An icon font from Font Awesome is also used. Links to compiled stylesheets and the icon font are as follows:

```
<link href="/css/bootstrap.min.css" rel="stylesheet">

<link href="/css/crossref.css" rel="stylesheet">

<link href="/css/partials/_fonts.css" rel="stylesheet">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
```

The original SASS files can be found in the GitHub repo, in the [/static](https://github.com/CrossRef/cr-site/tree/master/static) folder.  **Do not make changes to any of the files contained in the ../static directory.**

### Layout HTML
Pre-compiled templates containing HTML for Common page elements like the header (including navigation) and the footer can be found in in [/themes/crossref/layouts/partials](https://github.com/CrossRef/cr-site/tree/master/themes/crossref/layouts/partials).

### Elements not covered by existing stylesheets or templates
If you have an element that isn't covered by existing styles or layout HTML, please contact [Ginny Hendricks](mailto:ghendricks@crossref.org).

