+++
title = "Aliases How To"
name = "Aliases How To"
date = "2020-08-11"
draft = false
author = "Rosa Morais Clark"
aliases = [
    "/_editors/redirects-how-to"
]

+++

## Aliases How To

Let's say that Crossref launches a service called "dipsyxology." and we create a web page for the site at the following url:

```
https://crossref.org/services/dipsyxology
```

Later we conclude that "dipsyxology" was a really stupid name and we decide to rebrand it to "snall" instead. The new page for the re-branded service lives at:

```
https://crossref.org/services/snall
```

But we want anybody who follows the old url to be automatically redirected to the new url.

To to this, we can add the following to the metadata of the *new* snall page.

```
aliases = [
    "/services/dipsyxology"
]
```

And then when hugo builds the site, it will automatically create the redirect for the old "dipsyxology" page. Note that you can add any number of aliases to the list. So, if before the service was called "dipsyxology", the beta name was "wastone", you could have that redirect as well.

```
aliases = [
    "/services/wastone",
    "/services/dipsyxology"
]
```

The frontmatter format for blogs is different than other pages.  To add an alias to a blog, add the alias above "Archives:" as in the example below.

```
aliases: "/blog/similarity-check-news-introducing-the-next-generation-ithenticate/"
aliases: "/blog/planning-platform-move/"
```

Cool, right?

You can see how this works if you edit the markdown for this very document you are reading.

I originally called this "redirects-how-to." But you can get to this page with either of these URLS.

- [https://crossref.org/_editors/redirects-how-to](/_editors/redirects-how-to)
- [https://crossref.org/_editors/aliases-how-to](/_editors/aliases-how-to)

The aliases directive _must_ be included in the main metadata section of the header. It will not work if placed under a menu (e.g., below `[menu.main]`). Ensure the aliases directive is added to the front matter above `[menu.main]`.

Additionally, you cannot redirect a page that still exists—it takes precedence (e.g., an _index.md page). To enable redirection, you could either delete the page or rename it (e.g., change fee-assistance to fee-assistance-old) and set draft=true.

By setting draft=true, the page remains indexable, meaning it can still appear in search engines, even though you are redirecting it. However, note that if the page is later un-drafted with the same name/location, it will break the alias.

Have fun.
