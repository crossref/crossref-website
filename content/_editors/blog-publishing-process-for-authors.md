+++
title = "Blog publishing process for authors"
name = "Blog publishing process for authors"
date = "2023-05-02T12:00:00-00:00"
draft = false
tags = ["help", "template", "blog"]
+++


**Purpose**: To provide a clear guide for authors to have their blogs reviewed, approved, scheduled, and published on the website and the community forum.

It is important for authors to allocate a minimum of two weeks before their desired publication date when submitting a blog for review. (There are always exceptions!) This time frame allows the comms team to review the content, ensure the markdown is correct and make any necessary fixes, and coordinate with other ongoing communications. Providing sufficient lead time helps maintain a smooth and efficient publishing process.

In addition, authors should familiarize themselves with the blog publishing process as displayed on the Trello board. Every blog should start as a card in the “Idea” column or the “In Draft” column, and move through the columns until it is published, adding any helpful details to the card notes along the way. By adding your card to the planner, automations are triggered, alerting the comms team that a blog is in the works. Working together and following the established process helps us manage the communications so they are timely and aligned with our engagement strategy for the coming months.


{{< figure src="/images/_editors/blog-planner-trello.png" width="75%">}}

### Steps

1. Authors, add your blog information to [Blog Planner](https://trello.com/b/IJAYsTQ8/blog-planner) in the appropriate column, including the link to your working document. Also, add yourself and Rosa as members of the card. This helps others easily find the work that needs to be reviewed.
2. Consider other [blogs in the queue](https://trello.com/w/crossrefstaff/views/calendar?idBoards=02zsQaeA,IJAYsTQ8,9xNteg94,1C3rqTWX,IYa0WwSw,hxKnJW8c,jxzZw3fW), topic relevance, community events, and ongoing projects to determine an optimal publication date.
3. Check with Rosa/Kora/Ginny to confirm if your desired publication date will work, as there may be other events or information not yet public that could affect the blog’s publication.
4. Once the blog is written, get feedback from your manager and colleagues to ensure content accuracy.
5. After the review, set up your blog, [push it to staging](/_editors/how-to-publish-content/), and review it again for grammar, formatting, and typos.
6. Schedule a publication date with Rosa/Kora/Ginny, add the date to the card, and move the card to column “5 - Scheduled” in the [Blog Planner.](https://trello.com/b/IJAYsTQ8/blog-planner). This triggers an automation that will send an alert a blog is scheduled.
7. Rosa/Ginny will publish the blog.
8. Once the blog is published, move the card to the 'Published’ column in the Blog Planner and it will automatically get added to the Content Calendar.
9. It is encouraged the author seed the post with a question to foster engagement, particularly if the blog contains a call to action.

## Listing blogs on the community forum

1. Blogs are automatically published to the #community-news channel in Slack and saved under the ‘Blog’ category on community.forum.org, which is hidden from public view.
2. To make the blog public, locate it and mark it as “listed.” Then, recategorize it by moving it to the appropriate category using the dropdown menu, for example, selecting ‘Content Registration’ or ‘Crossref services’.

{{< figure src="/images/_editors/forum-listing-menu.png" width="50%">}}

3. Add tags such as POSI, ROR, or metadata, and always include the tag 'blog.'
4. You'll notice a 'reply' appearing with your name. You may optionally delete the "listed" reply by clicking the trash can icon to the right. After deletion, it will show as "VIEW 1 HIDDEN REPLY."
5. When sharing on social media, always use the actual blog link instead of the forum post link. Keep in mind that forum posts do not include images, etc.

### How to list and categorize the blog and make it public

<img src="https://gitlab.com/crossref/crossref-website/-/raw/main/static/images/_editors/forum-list-blog.gif " width="50%" height="75%" />


