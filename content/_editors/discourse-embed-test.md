+++
title = "Discourse embed test"
date = "2024-01-19"
draft = false
author = "Zach Anthony"
+++

This is a paragraph in the page, and everything below the horizontal line is being pulled in using a bit of javascript from Discourse. 

---

<script src="https://community.crossref.org/javascripts/embed-topics.js"></script>

## Join the discussion
--
Version 1

<d-topics-list discourse-url="https://community.crossref.org" tags="rest-api" per-page="5"></d-topics-list>

## Join the discussion
--
Version 2, with full template, "top" posts in the period "monthly"

<d-topics-list discourse-url="https://community.crossref.org" tags="rest-api" period="monthly" template="complete" per-page="5"></d-topics-list>

<d-topics-list discourse-url="https://community.crossref.org" tags="rest-api" per-page="5"></d-topics-list>


