+++
title = "Embedding videos"
name = "Embedding videos"
date = "2020-11-18"
draft = false
author = "Rosa Morais Clark"

+++


We've switched from Wistia to Vimeo due to changes in Wistia's pricing. Now, we use two platforms for embedding video on our web pages: Vimeo and YouTube.  

- **Vimeo** is used for embedding videos on our service pages.  
- **YouTube** hosts all other videos. (Refer to sample screenshots below.)  

{{< figure src="/images/documentation/embed-videos.png" alt="screenshot of video player" width="70%" >}}

### Instructions for Embedding Service Videos

1. **Locate the JSON file**: Go to `/data/videos/vimeo.json`.
2. **Create an “id” name**: Add an entry with a unique ID for each video you want to embed on a specific page.
3. **Vimeo video id**: The vimeo video id is the numbers after the second `/`https://vimeo.com/`988561984` 

   **Example format**:
   ```json
   {
     "blog-perspectives-mohamad-mostafa": [
       {
         "id": "895597454",
         "lang": "English"
       },
       {
         "id": "895597913",
         "lang": "Arabic"
       }
     ]
   }
   ```

3. **Embed the video**: On the page where you’d like to embed the video, use the shortcode with the “id” as shown:

   ```html
   {{< vimeoPlayer id="blog-perspectives-mohamad-mostafa">}}
   ```

You can see an example of this in `/blog/2023/2023-02-27-perspectives-mohamad-mostafa-on-scholarly-communications-in-UAE.md` (line 43).