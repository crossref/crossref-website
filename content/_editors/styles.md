+++
title = "Illustrated Styles"
name = "Illustrated Styles"
date = "2019-12-11T12:00:00-00:00"
draft = false

+++

## Link styles

---

## Season 2

The variations below are based on S01E02, "The Easy One". All use the preferred dark(er) blue colour. In all of these options the underline has been moved a bit further away from text, which brings some of the benefits of hiding it on hover.

{{% row %}}
{{% column-thirds %}}
### Episode 1: "The One Where They Go Through Thick & Thin"
<style>
	a.options02e01 			{ color: #017698; text-decoration: underline; text-underline-offset: .3em; text-decoration-thickness: .04rem; }
	a.options02e01:hover	{ color: #017698; text-decoration: underline; text-underline-offset: .3em; text-decoration-thickness: .1600rem; }
</style>
**Underline is finer to start, and is retained on hover but goes twice as thick. Colour remains the same.** 

Lorem ipsum <a href="#" class="options02e01">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#" class="options02e01">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#" class="options02e01">Lorem ipsum dolor sit amet</a>
- <a href="#" class="options02e01">Consecteteur adipiscing elit</a>
- <a href="#" class="options02e01">Sed do eiusmod tempor incididunt</a>
- <a href="#" class="options02e01">Ut labore et dolore magna aliqua</a>

**Analysis:** The slightly finer underline is more subtle but also less accessible.
{{% /column-thirds %}}
{{% column-thirds %}}
### Episode 2: "The One Where Things Get a Bit Dark"
<style>
	a.options02e02 			{ color: #017698; text-decoration: underline; text-underline-offset: .3em; text-decoration-thickness: 1px; }
	a.options02e02:hover	{ color: black; text-decoration: underline; text-underline-offset: .3em; text-decoration-thickness: 1px; }
	a.options02e02a 		{ color: #017698; text-decoration: underline; text-underline-offset: .3em; text-decoration-thickness: 1px; }
	a.options02e02a:hover	{ color: #016082; text-decoration: underline; text-underline-offset: .3em; text-decoration-thickness: 1px; }
</style>
**Underline normal thickness to start, and is retained on hover at the same thickness. Colour goes black on hover.** 

Lorem ipsum <a href="#" class="options02e02">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#" class="options02e02">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#" class="options02e02">Lorem ipsum dolor sit amet</a>
- <a href="#" class="options02e02">Consecteteur adipiscing elit</a>
- <a href="#" class="options02e02">Sed do eiusmod tempor incididunt</a>
- <a href="#" class="options02e02">Ut labore et dolore magna aliqua</a>

**Analysis:** This is based on the previously discussed [GOV.UK article](https://designnotes.blog.gov.uk/2021/07/07/making-links-easier-to-see-and-read-on-gov-uk/). The dark(er) blue used here is already ao dark and close enough to text colour that changing to an even darker blue on hover that isn't nearly black is almost imperceptible (<a href="#" class="options02e02a">here's an example</a> - it really does go darker). So, we've used black instead, which has more contrast from both original link colour and text. 
{{% /column-thirds %}}
{{% column-thirds %}}
### Episode 3: "The One Where We Try Both"
<style>
	a.options02e03 			{ color: #017698; text-decoration: underline; text-underline-offset: .3em; text-decoration-thickness: .0625rem; }
	a.options02e03:hover	{ color: black; text-decoration: underline; text-underline-offset: .3em; text-decoration-thickness: .1600rem; }
</style>
**Underline is normal thickness to start, and goes twice as thick on hover. Colour goes black on hover.** 

Lorem ipsum <a href="#" class="options02e03">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#" class="options02e03">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#" class="options02e03">Lorem ipsum dolor sit amet</a>
- <a href="#" class="options02e03">Consecteteur adipiscing elit</a>
- <a href="#" class="options02e03">Sed do eiusmod tempor incididunt</a>
- <a href="#" class="options02e03">Ut labore et dolore magna aliqua</a>

**Analysis:** This is combination of the previous 2 options: thicker underline on hover, colour goes darker.
{{% /column-thirds %}}
{{% /row %}}

---

## Season 1

See below for various alternative link styles. The various versions below all follow some common guidelines:

1. Links are underlined. This is the most common indicator of a link, making links easily understandable by users. 
2. Underlines disappear on hover. By removing the underline on hover, link text becomes more easily readable. 

These links styles would be used in text and most other places where links appear over a white background (note that in the evolving design proposals, we've tried to keep links in areas over a white background wherever possible). 

In some cases links will appear over a non-white background however, and in these cases we would propose to (mostly) stick to the "Color Dos and Do Nots" from page 26 of the Brand Guidelines. 

In some specific cases, for example arrows that indicate a block is clickable, other colours might be used. These elements are usualy more presentational than informational, so accessible colours are less of an issue.  

### Episode 1: "The Inaccessible One" (Current Link Styling)
**Crossref Blue, no underline, Crossref red on hover.** Lorem ipsum <a href="#">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#">Lorem ipsum dolor sit amet</a>
- <a href="#">Consecteteur adipiscing elit</a>
- <a href="#">Sed do eiusmod tempor incididunt</a>
- <a href="#">Ut labore et dolore magna aliqua</a>

**Analysis:** This combination uses Crossref Blue and Crossref Red on hover, and has no underline in either state. <span style="color: red;">Neither colour is accessible.</span>

### Episode 2: "The Easy One"

<style>
	a.optionEasy 			{ color: #017698; border-bottom: 1px solid #017698; }
	a.optionEasy:hover		{ color: #017698; border-bottom: 0; }
</style>
**Dark(er) Blue text and underline that disappears on hover.** Lorem ipsum <a href="#" class="optionEasy">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#" class="optionEasy">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#" class="optionEasy">Lorem ipsum dolor sit amet</a>
- <a href="#" class="optionEasy">Consecteteur adipiscing elit</a>
- <a href="#" class="optionEasy">Sed do eiusmod tempor incididunt</a>
- <a href="#" class="optionEasy">Ut labore et dolore magna aliqua</a>

**Analysis:** This is a minimally corrected (made darker and more saturated) version of Crossref Blue, very slightly lighter and brighter than the dark blue used below. It provides sufficient contrast (AA). 

### Episode 2: "The Dark Blue One"
<style>
	a.option1 			{ color: #005f83; border-bottom: 1px solid #005f83; }
	a.option1:hover		{ color: #005f83; border-bottom: 0; }
</style>
**Secondary Palette Dark Blue text and underline that disappears on hover.** Lorem ipsum <a href="#" class="option1">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#" class="option1">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#" class="option1">Lorem ipsum dolor sit amet</a>
- <a href="#" class="option1">Consecteteur adipiscing elit</a>
- <a href="#" class="option1">Sed do eiusmod tempor incididunt</a>
- <a href="#" class="option1">Ut labore et dolore magna aliqua</a>

**Analysis:** This uses the Dark Blue from the official Secondary Palette, and provides AAA accessibility but relatively low contrast to the text colour. The underline mitigates this somewhat. 

### Episode 2a: "The One With the Subtle Cobalt Hue" 
<style>
	a.option1a 			{ color: black; border-bottom: 1px solid #005f83; }
	a.option1a:hover	{ color: #005f83; border-bottom: 0; }
</style>
**Black text, Secondary Palette Dark Blue underline that disappears on hover.** Lorem ipsum <a href="#" class="option1a">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#" class="option1a">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#" class="option1a">Lorem ipsum dolor sit amet</a>
- <a href="#" class="option1a">Consecteteur adipiscing elit</a>
- <a href="#" class="option1a">Sed do eiusmod tempor incididunt</a>
- <a href="#" class="option1a">Ut labore et dolore magna aliqua</a>

**Analysis:** An alternative version using the Secondary Palette Dark Blue that is a bit more subtle. The text is black here, offering even less contrast to text, but the underline again mitigates this somewhat. This option is shown mostly to give an idea of something a bit more subtle (but is it too subtle?). 

### Episode 3: "The Green One"
<style>
	a.option2 			{ color: #006740; border-bottom: 2px solid #006740; }
	a.option2:hover		{ color: #006740; border-bottom: 0; }
</style>
**Corrected version of Crossref Green, heavy underline that disappears on hover.** Lorem ipsum <a href="#" class="option2">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#" class="option2">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#" class="option2">Lorem ipsum dolor sit amet</a>
- <a href="#" class="option2">Consecteteur adipiscing elit</a>
- <a href="#" class="option2">Sed do eiusmod tempor incididunt</a>
- <a href="#" class="option2">Ut labore et dolore magna aliqua</a>

**Analysis:** This uses a corrected version of the Secondary Palette Crossref Green that has been made darker and more saturated, resulting in AAA accessibility. A heavier underline has also been used, just as an illustration of another option.

### Episode 4: "The Red One"
<style>
	a.option3 			{ color: #A6192E; border-bottom: 1px solid #A6192E; }
	a.option3:hover		{ color: #A6192E; border-bottom: 0; }
</style>
**Secondary Crossref Red, underline that disappears on hover.** Lorem ipsum <a href="#" class="option3">dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#" class="option3">quis nostrud exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat.

- <a href="#" class="option3">Lorem ipsum dolor sit amet</a>
- <a href="#" class="option3">Consecteteur adipiscing elit</a>
- <a href="#" class="option3">Sed do eiusmod tempor incididunt</a>
- <a href="#" class="option3">Ut labore et dolore magna aliqua</a>

**Analysis:** This uses the Secondary Palette Crossref Red, resulting in AAA accessibility. 