+++
title = "Deep linking"
name = "Deep linking"
author = "Isaac Farley"
date = "2019-12-03T12:00:00-00:00"
draft = false
tags = ["help", "structure"]
+++

HTML supports a mechanism that allows one to link directly to a specific part of a document. For example, I can link directly to [the conclusion](#500) of this document. This kind of linking is often used within a document's TOC to link to specific sections of the document. Deep linking _within_ a document like this is fine and rarely presents problems.

But the same mechanism can also be used to create "deep link" from one document to a specific part of another document. This kind of usage can cause problems due to the way in which these kinds of links work.

## How does deep linking work?

To deep link to a specific section of a document, you create an "anchor" in the part of the document you want to be able to directly link to. For example, in Markdown this would look like this:

```markdown
## Conclusion <a id="500">
```

Then, to link to that specific section **within** the document, you would write markdown like this

```markdown
...as you will see in our [conclusion](#500), we recommend that...
```

And if you wanted to link to that specific section **from another document**, you would simply prepend the URL to the fragment identifier. For example:

```markdown
...as Crossref point out in the [conclusion](/education/some_document#500), of their display guidelines, the structure of a DOI...
```

In the above examples, the `500` is used to identify the specific anchor point in the document to link to. And, because we used an opaque identifier (e.g. "500" instead of "conclusion"), if we then decided to rename the section to "Final thoughts", we could just change the header to this:

```markdown
## Final thoughts <a id="500">
```

and the direct link would **still work** because it is based on the anchor identifier, not on the text of the heading itself. Pretty cool, right?

Unfortunately, deep linking isn't as cool as it looks at first glance.

## Problems with deep links

The problem with deep links is the way in which they work. Whereas normal links between documents are resolved by the HTTP requests to server, deep links involve the client (e.g. the web browser) as well. In the following URL:

`<span style="color:blue">https://www.crossref.org/education/some_document``<span style="color:red">#500`

The part of the URL before the # is resolved by the server, but the part after the # (the fragment identifier) is interpreted by the web browser _after_ the url has been resolved.

In practice this means that, if I try to link to a non-existent anchor id, I will not get a normal HTTP 404 error. The link will just silently fail. For example, [this link to a non-existent section on the Crossref home page](https://www.crossref.org/#foobarbaz), will not result in a 404. Instead, it will just take me to the home page.

In other words, using deep links like this makes it very difficult to check for broken links. Our server logs will never see them. Link checkers won't see them. This isn't a huge problem if you are linking **within** a document where it is fairly easy to see if a link from a TOC entry is not working. But this is a much bigger problem if you are linking between documents.

There are also implications for tracking usage. Presumably, if we have a section called "DOI Display Guidelines", it would be useful to know how many times that section is linked to and accessed. But if links to the section are deep links using fragment identifiers, then we will only see links to the parent document the section is in, we won't be able to measure links to the "DOI Display Guidelines section itself.

## Conclusion`<a id="500"></a>`

Deep linking using fragment identifiers is useful and relatively innocuous if they are used \*_within_ a document as a navigation aid (e.g. A linked TOC), but they should probably not be used to link between documents.

## The specific case of the new education section

The new documentation section of the web site currently uses deep links heavily. This is to support the information architecture of the site which is structured to handle two distinct ways of navigating and understanding the content whilst minimizing duplicate copy. The section is made up of discrete, reusable entries which can either be:

- read in a predefined linear sequence that is also suitable for exporting or printing out.
- read in a custom sequence chosen for specific use cases (e.g. a seminar on a specific service) or chosen by the reader for their specific use-case.

It is considered vital that these entries could be recombined in different sequences _without_ needing to duplicate the content. This is to minimise the inevitable content drift and synchronisation issues that would occur when trying to maintain multiple documents that say essentially the same thing.

The current implementation collects all the discrete entries into one document with each section individually addressable via deep links using fragment identifiers. This accomplishes the three goals of:

- minimising duplicate content
- allowing for a guided, linear narrative path through the entries which can also be printed, exported.
- allowing for alternative paths through the entries which are optimised for particular users or use-cases.

The problem with this approach is that it relies so heavily on deep links. Using deep links like this will make it very difficult for us to make sure links don't break and it will also make it hard for us to understand the usage of the site at a granular level.

## Decompose and transclude

We can support the information architecture by inverting the implementation model. Rather than build a section document with individually addressable subsections , we can create a section document by transcluding the individual entries from their own files. This sounds weird and arcane, but we already do it elsewhere on the site.

The most obvious example is on the [blog page](/blog/) where individual blog entries (each in their own file and individually addressable) are combined into one page. In this case, the page is simply a collection of the most recent blog posts. But the pages can combine the entries and provide customized views of them. For example, [this page](/categories/orcid/) just shows all the blog entries related to ORCID.

If we follow the same pattern, we would break out every documentation section entry into its own file. Then every subsection of the documentation section would simply be composed of the relevant individual entries. For example:

The current page for section 1.3 has three individually addressable sections. The overview (1.3), "The Structure of a DOI" (1.3.1) and "DOI Display Guidelines" (1.3.2). These are all contained in **one** file, `content/_education_sources/section1.md`.

We would instead break this out into three separate files:

- persistent-identifiers.md
- the-structure-of-a-doi.md
- doi-display-guidelines.md

And then the `_index.md` for the "persistent identifiers" section would simply include the above three files in the correct order. Any links to a specific section could then be the link to the subfile.

This approach has many benefits, including that we can then create special paths through the content simply by adding categories and keywords to individual entries. For example, if we labeled entries "beginner", "intermediate" and "advanced" we could create custom pages for users with different skill levels.
