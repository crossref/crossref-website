+++
title = "Homepage How To"
name = "Homepage How To"
date = "2017-08-17T12:00:00-00:00"
author = "Zach Anthony"
draft = false
type = "Help"
thumbnail = "/images/slots/slot1.jpg"

+++

## How to control the Homepage 'highlight' boxes

The 6 boxes at the bottom of the homepage, aka the Highlight Boxes, are easily controlled via a datafile that lives in  /data/features/homepage.toml. For more info on the contents of the data directory and data files in general see the [Data How To page](../data-how-to "go to this page").

Here is a sample of the content of ```homepage.toml```:

```
[[slots]]
	[[slots.slot]]
		class = 'home-slot col-lg-offset-1 col-md-3 col-md-offset-1'
		pagetitle = 'Crossref LIVE17 in Singapore'
		type = 'Event'
		thumbnail = "/images/homepage-images/homepage-slot-images/live17-singapore-homepage.jpg"
	[[slots.slot]]
		class = 'home-slot home-slot-col2 col-md-3'
		pagetitle = 'Crossref now accepts preprints'
		type = 'News'
		thumbnail = "/images/homepage-images/homepage-slot-images/highlight-box-bikes-fist-bump-preprints.png"
	[[slots.slot]]
		class = 'home-slot col-md-3'
		pagetitle = 'Working for you'
		type = 'Community'
		thumbnail = "/images/homepage-images/homepage-slot-images/content-registration-feature.jpg"
	[[slots.slot]]
		class = 'home-slot col-lg-offset-1 col-md-3 col-md-offset-1'
		pagetitle = 'FAQs'
		type = 'Help'
		thumbnail = "/images/homepage-images/homepage-slot-images/highlight-box-bikes-wrenches-spanners-faqs.png"
	[[slots.slot]]
		class = 'home-slot home-slot-col2 col-md-3'
		pagetitle = 'Cited-by'
		type = 'Services'
		thumbnail = "/images/homepage-images/homepage-slot-images/feature-bike-rack-yarn.jpg"
	[[slots.slot]]
		class = 'home-slot col-md-3'
		pagetitle = 'Our truths'
		type = 'Feature'
		thumbnail = "/images/homepage-images/homepage-slot-images/highlight-box-bikes-camera-view-truths.png"
```

To change the content of a Highlight Box, simply update the ```pagetitle```, ```type``` and ```thumbnail``` fields. **JSON files must be exactly formatted, so be sure not to miss out a ' or " (either is fine).** More on these fields below.

### How to add external URL to feature box on website

```
[[slots.slot]]

		class = 'home-slot home-slot-col2 col-md-3'

		alttitle = 'ROR Annual Meeting and 5th Anniversary Celebration'

		type = 'Register for the ROR Annual Meeting'

		thumbnail = "/images/homepage-images/featured/lion-ror-feature.jpg"

		url = "https://bbc.com"

```

If the total number of boxes ever needs to change from 6, either by adding or removing boxes, please consult with Cazinc, as this will require changes in the underlying template code and can't be controlled in homepage.toml. 

Field | Definition/Functionality
--- | ---
```class``` | Used for layout purposes. **Do not change this field.**
```pagetitle``` | The contents of this field must **exactly** match the frontmatter ```title``` of the page you wish to link to. This text is also shown beneath the image in the Highlight Box. 
```url``` | Used to link to a specific URL (eg an external website.) ```pagetitle``` must not be present for this to work.
```type``` | Shown at the top left of the Highlight Box; technically can be anything but should ideally stick to top level site categories like Help or Services.
```thumbnail``` | The thumbnail image for the Highlight Box. **Images should be 765px wide by 240px high and should be placed in the ```/static/images/homepage-images/homepage-slot-images/``` directory.** Because the thumbnail images are wide but quite short, abstract images or portrait orientation images will likely work best. Taller images can be used but the top and bottom will be cropped automatically. Ideally images should be JPGs at 60% compression.



### Ordering
Boxes are ordered according to their placement in homepage.toml from top to bottom. To change their order you must select, cut and then paste an entire block, where an individual block looks like this:

```
	[[slots.slot]]
		class = 'home-slot col-md-3'
		pagetitle = 'Our truths'
		type = 'Feature'
		thumbnail = "/images/homepage-images/homepage-slot-images/highlight-box-bikes-camera-view-truths.png"

```

Be very careful to preserve the formatting of the block when you move it.

Boxes are displayed on the homepage in order like so:

<table border="1">
<tbody>
<tr>
<td width="100" align="center">first</td>
<td width="100" align="center">second</td>
<td width="100" align="center">third</td>
</tr>
<tr>
<td width="100" align="center">fourth</td>
<td width="100" align="center">fifth</td>
<td width="100" align="center">sixth</td>
</tr>
</tbody>
</table>
*Desktop resolution*

<table border="1">
<tbody>
<tr>
<td width="100" align="center">first</td>
<td width="100" align="center">second</td>
</tr>
<tr>
<td width="100" align="center">third</td>
<td width="100" align="center">fourth</td>
</tr>
<tr>
<td width="100" align="center">fifth</td>
<td width="100" align="center">sixth</td>
</tr>
</tbody>
</table>
*Large tablet/landscape tablet resolution*

<table border="1">
<tbody>
<tr>
<td width="100" align="center">first</td>
</tr>
<tr>
<td width="100" align="center">second</td>
</tr>
<tr>
<td width="100" align="center">third</td>
</tr>
<tr>
<td width="100" align="center">fourth</td>
</tr>
<tr>
<td width="100" align="center">fifth</td>
</tr>
<tr>
<td width="100" align="center">sixth</td>
</tr>
</tbody>
</table>
*Small tablet/portrait tablet/mobile resolution*

## How to configure the example queries in the homepage How Can We Help box

The How Can We Help search box in the banner area of the homepage features a number of example queries that appear automatically after a few seconds of viewing the page. These can be used way to draw users' attention to especially current, key or common topics. 

The example queries exist in a file within ```/data/algolia/examplequeries.json``` because they relate to the site search, which is enabled by the Algolia search engine.

This is a JSON file, which means that formatting rules must be followed exactly. **Invalid formatting can break the site build process, so after any edits to this file you should definitely monitor the build process in Slack and look for errors.**

The file itself looks like this:

```
{ "examplequeries":
	[
		{"query": "Am I eligible for Similarity Check?"},
		{"query": "Help with multiple DOI resolution"},
		{"query": "How do I deposit metadata?"}
	]
}
```

To edit an existing query just change the text within the "" after ```"query":```. **Optimal length of a query is about 25 characters, including punctuation.** Be sure to check that your query is displaying OK on Staging before merging into live. 

To add a new query just copy and paste one of the lines. **Ensure that each line has a comma after it except for the very last one** (you can see this in the example above).

Queries will appear to site visitors in the order they are found within the file. Note that although there is no limit on the number of queries that can be added, each successive one after the first is less and less likely to be seen.

