+++
title = "Research Organization Registry (ROR)"
date = "2023-07-31"
draft = false
author = "Amanda French"
[menu.main]
rank = 2
parent = "Initiatives"
weight = 10
+++


{{% divwrap green-highlight %}}

ROR IDs and Affiliations of authors can now be tracked in [Participation Reports](https://www.crossref.org/members/prep/)! Check your own Participation Report to see how many of your publications have author affiliations and ROR IDs in Crossref metadata. If you deposit metadata via XML, see our guide on [Affiliations and ROR](https://www.crossref.org/documentation/schema-library/markup-guide-metadata-segments/affiliations/) for instructions on how to include affiliations and ROR IDs in your metadata. 

{{% /divwrap %}}  

---

Crossref encourages our members to include ROR IDs in metadata in order to help make research organization information clear and consistent as it is shared between systems. ROR IDs are essential to realize a rich and complete [Research Nexus](https://www.crossref.org/documentation/research-nexus/) because they enable connections between research outputs and the organizations that support researchers.

 > "At Scholastica, we care about taking steps to enrich metadata – like adding ROR IDs, for example, on behalf of our customers, so they don’t have to worry about the technical aspects of metadata collection or creation and can instead focus on maximizing the discovery benefits." -- Cory Schires, Co-founder and Chief Technology Officer, [Scholastica](https://scholasticahq.com/) 

Read the [case study on ROR in Scholastica publishing products](https://ror.org/blog/2023-03-30-scholastica-case-study/).

 > "If we’re talking about misconduct, then you might need to be able to contact the institution that the author is from. On an individual manuscript, it doesn’t matter if there’s no identifier – an address will do. But if you find some signal that is on manuscripts at scale, and you’ve got thousands of them, well, you need an identifier. You can’t go through them and try and search for every single one of those institutions." -- Adam Day, CEO, [Clear Skies Ltd.](https://clear-skies.co.uk/)

Read the [case study on why Clear Skies uses ROR to help detect paper mills](https://ror.org/blog/2023-11-27-clear-skies-case-study/). 

## What is ROR?

ROR is the [Research Organization Registry](https://ror.org) -- a **global, community-led registry of open persistent identifiers for research organizations**. The registry currently includes globally unique persistent identifiers and associated metadata for [more than 110,000 research organizations](https://ror.org/search).

ROR IDs are specifically designed to be implemented in any system that captures institutional affiliations and to enable a richer networked research infrastructure. ROR IDs are interoperable with other organization identifiers, including GRID (which provided the seed data that ROR launched with), the [Open Funder Registry](https://www.crossref.org/services/funder-registry/), ISNI, and Wikidata. ROR data is available under a CC0 Public Domain waiver and can be accessed at no cost via [a public API](https://api.ror.org/organizations) and a [data dump](https://doi.org/10.5281/zenodo.6347574).  

{{% imagewrap center %}}
{{< figure src="/images/services-images/ror-record.png" alt="The ROR record for the University of St Andrews." width="75%" class="img-responsive" caption="[ROR record for the University of St. Andrews](https://ror.org/02wn5qz54)">}}
{{% /imagewrap %}}


## Who is ROR?

ROR is operated as a joint initiative by Crossref, [DataCite](https://datacite.org/), and the [California Digital Library](https://cdlib.org/), and was launched with seed data from GRID in collaboration with [Digital Science](https://www.digital-science.com/). These organizations have invested resources into building an open registry of research organization identifiers that can be embedded in scholarly infrastructure to effectively link research to organizations. ROR is not a membership organization (or an organization at all!) and charges no fees for use of the registry or the API. [Read more about ROR's sustainability model.](https://ror.org/blog/2022-10-10-strengthening-sustainability/) 

## Why ROR IDs are an important element of Crossref metadata

For a long time, Crossref only collected affiliation metadata as free-text strings, which made for ambiguity and incomplete data. An author affiliated with the University of California at Berkeley might give the name of the university in any of several common ways: 

* University of California, Berkeley  
* University of California at Berkeley  
* University of California Berkeley  
* UC Berkeley  
* Berkeley
* And likely more …  

While it isn’t too difficult for a human to guess that “UC Berkeley,” “University of California, Berkeley,” and “University of California at Berkeley” are all referring to the same university, a machine interpreting this information wouldn’t necessarily make the same inference. If you are trying to easily find all of the publications associated with UC Berkeley, you would need to run and reconcile multiple searches at best, or, at worst, miss some data completely. 

This is where an organization identifier comes in: a single, unambiguous, standardized identifier that will always stay the same. For UC Berkeley, that would be [https://ror.org/01an7q238](https://ror.org/01an7q238).

In 2019, Crossref members indicated that the ability to associate research outputs with organizations in a clean and consistent fashion was one of their most desired improvements to Crossref metadata. In January of 2022, therefore, Crossref [added support for ROR IDs](/blog/a-ror-some-update-to-our-api/) in its metadata schema and APIs. Since then, more and more Crossref members have been including ROR IDs in DOI metadata. 

<iframe width="912" height="715" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQU_zvKDRXOFY56Zq6gAZyWBXWivQxPyDgAVP18bYDpNTgrM2pQFz4EzNc6I44kgTmhoVqkTJfef-HN/pubchart?oid=1526035463&amp;format=interactive"></iframe>

Publishers and service providers can implement ROR in their systems so that submitting authors and co-authors can [easily choose their affiliation from a ROR-powered list](https://ror.readme.io/docs/create-ror-powered-typeaheads) instead of typing in free text. Authors themselves do not  have to provide a ROR ID or even know that a ROR ID is being collected. This affiliation information can then be sent to Crossref alongside other publication information. 

{{% imagewrap center %}}
{{< figure src="/images/community-images/FAU-ror-typeahead.gif" alt="Demo of collecting ROR IDs in a typeahead field" width="75%" class="img-responsive" caption="Demo of collecting ROR IDs in a typeahead field">}}

{{% /imagewrap %}}

If the submission system you use does not yet support ROR, or if you don't use a submission system, you'll still be able to provide ROR IDs in your Crossref metadata. ROR IDs can be [added to JATS XML](https://ror.readme.io/docs/include-ror-ids-in-jats-xml), and [Crossref helper tools](https://www.crossref.org/documentation/register-maintain-records/choose-content-registration-method/#00479) will start to support the deposit of ROR IDs. There's also an [OpenRefine reconciler](https://github.com/ror-community/ror-reconciler) that can map your internal identifiers to ROR identifiers.

ROR IDs for affiliations stand to transform the usability of Crossref metadata. While it’s crucial to have IDs for affiliations, it’s equally important that the affiliation data can be easily used. The ROR dataset is CC0, so ROR IDs and associated affiliation data can be freely and openly used and reused without any restrictions.

The ROR IDs registered by members in their Crossref metadata are available via [Crossref’s open APIs](/services/metadata-retrieval/) so that they can be detected, analyzed, and reused by anyone interested in linking research outputs to research organizations. Examples include 

* Institutions who want to monitor and measure their research output by the articles their researchers have published
* Funders who want to be able to discover and track the research and researchers they have supported 
* Academic librarians who want to find all of the publications associated with their campus 
* Journals who want to know where authors are affiliated so they can determine eligibility for institutionally sponsored publishing agreements 

The inclusion of ROR IDs in Crossref metadata will eventually help all these entities make all these connections much more easily.

## Get ready to ROR 🦁!

ROR is already working with publishers, funders and service providers who are integrating ROR in their systems, mapping their affiliation data to ROR IDs, and/or including ROR IDs in publication metadata. Libraries and institutional repositories are also beginning to build ROR into their systems and to send ROR IDs to Crossref in their metadata. See the growing list of [active and in-progress ROR integrations](https://bit.ly/ror-integrations) for more stakeholders who are supporting ROR.

If you deposit metadata with Crossref via XML, see our guide on [Affiliations and ROR](https://www.crossref.org/documentation/schema-library/markup-guide-metadata-segments/affiliations/) for instructions on how to include author affiliations and ROR IDs. 

For further information on how ROR IDs are supported in the Crossref metadata, you can take a look at this [.xsd file](https://gitlab.com/crossref/schema/-/blob/5.0/schemas/common5.0.xsd) (under the ‘institution’ element) or in this [journal article example XML](https://gitlab.com/crossref/schema/-/blob/5.0/examples/journal_article_5.0.xml). ROR also has some great [help documentation](https://ror.readme.io/) for publishers and anyone else working with the ROR Registry.

---
[Get in touch with ROR](mailto:support@ror.org) if you have questions or want to be more involved in the project. If you have questions about adding ROR IDs to your Crossref metadata, get in touch with our [support specialists](mailto:support@crossref.org) or ask on the [Crossref Community Forum](https://community.crossref.org/tags/c/content-registration/24/ror). 
