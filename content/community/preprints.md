+++
title = "Preprints"
date = "2020-11-09"
draft = false
parent = "Get involved"
author = "Martyn Rittman"
rank = 4
weight = 2
+++

Our members asked for the flexibility to [register content at different points in the publishing lifecycle](/education/crossmark/version-control-corrections-and-retractions), so we extended our infrastructure to support members who want to register early versions such as preprints or working papers.

> Our custom support for preprints ensures that links to these outputs persist over time, that they are connected to the full history of the shared research results, and that the citation record is clear and up-to-date.

{{< snippet "/_snippet-sources/preprints-label.md" >}}

We have designed a schema together with a working group that included preprint advisors from bioRxiv and arXiv, along with members including PLOS, Elsevier, AIP, IOP, ACM. The schema lays out what metadata is specifically important for preprint content. We also developed a notification feature to alert preprint creators of any matches with journal articles, so they can link to future versions from the preprint.

Since November 2016, members have been [registering hundreds of thousands of preprints with us](http://api.crossref.org/v1.0/types/posted-content/works?rows=0), and thousands of those in turn already have [matches with journal articles](https://api.crossref.org/v1.0/works?filter=relation.type:is-preprint-of) too (requires a JSON viewer). These relationships in the Crossref metadata, available through our APIs, are relied upon by many parties - from researchers to funders - to discover, track and evaluate the preprint journey.

{{< snippet "/_snippet-sources/preprints-benefits.md" >}}

<img src="/images/services-images/preprints-nexus.png" alt="Preprints as part of the article nexus; as content evolves, connections persist and new links are added" width="60%">

## What to be aware of when registering preprints

Members registering preprints need to make sure they:

1. Register content using the posted content metadata schema (see [examples in the posted content markup guide](/documentation/schema-library/markup-guide-record-types/posted-content-includes-preprints/))
2. Respond to our match notifications that an accepted manuscript (AM) or version of record (VOR) has been registered, and link to that within seven days. You should designate a specific contact with us who will receive these alerts (it can be your existing technical contact)
3. Clearly label the manuscript as a preprint, above the fold on the preprint landing page, and ensure that any link to the AAM or VOR is also prominently displayed above the fold.

Other considerations:

* References will be flagged as belonging to a preprint in our [Cited-by](/education/cited-by/) service
* The preprint is treated as one item only without components for its constituent parts
* Each version should be assigned a new DOI, and associate the versions via a relationship with type *isVersionOf* - learn more about [relationships](/documentation/schema-library/markup-guide-metadata-segments/relationships/)
* Preprints are not currently able to participate in [Crossmark](/education/crossmark/).

## Registering preprints: joining as a member

Preprint owners who would like to use our preprint service should [apply to join](/membership) as a member. We have a dedicated fee structure for registering each preprint, and volume discounts offered for both backfile and current content. Learn more about our [fees](/fees).

## Registering preprints: existing members

Are you an existing Crossref member who wants to assign preprint DOIs? Let’s talk about getting started or migrating any existing mis-labelled content over to the dedicated preprint deposit schema. You can also give us a specific contact who will receive match notifications that an author's accepted manuscript or version of record (AAM or VOR) has been registered. [Get in touch with our membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) and they’ll be able to walk you through the process.

---

Learn more about registering preprints in our [Education documentation](/documentation/research-nexus/posted-content-includes-preprints/).
