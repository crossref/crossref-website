+++
title = "Special programs"
date = "2023-10-24"
draft = false
author = "Rosa Morais Clark"
image = "/images/banner-images/Register references landing page header (2).png"
maskcolor = "crossref-red"
[menu.main]
rank = 2
parent = "Get involved"
weight = 10
+++

# Discover our special programs

{{% imagewrap right %}}
{{< figure src="/images/documentation/research-nexus-2023-final.png" alt="Crossref Research Nexus vision image" width="350" align="right">}}
{{% /imagewrap %}}

At Crossref, our mission is not only about making research easier to find, cite, link, assess, and reuse. We are constantly widening our horizon to create special programs that focuses on different elements of the Research Nexus vision – a vision that aspires to create a rich, reusable open network of relationships connecting research elements for the benefit of society.

These programs focus on making connections between all kinds of research information, including journal articles, book chapters, grants, and preprints. We want to connect these different elements to make it easier for everyone involved in research to find and understand the information they need. We aim to involve everyone - like researchers, funding organizations, and governments - in building a better, more open network for sharing research.

Explore our programs designed to uphold our Research Nexus vision.

## Special Programs

#### [Resourcing Crossref for Future Sustainability (RCFS)](/community/special-programs/resourcing-crossref/)
A multi-year program to make participation more accessible through our fees becoming more equitable, less complex, and rebalanced to ensure they match our mission and future.

#### [Register references](/community/special-programs/register-references/) 
Registering your references with Crossref helps citation linking, creates a more interconnected scholarly network, and improves the discoverability and impact of your research.

#### [Integrity of the scholarly record (ISR)](/community/special-programs/research-integrity/)  
Our efforts ensuring that research is accurately documented and trustworthy by improving the quality and context of metadata, without directly judging the content itself. It supports the research community by providing reliable signals to assess the integrity of scholarly work.  


_More to come...watch this space._
