+++
title = "RCFS Program: Resourcing Crossref for Future Sustainability"
date = "2024-04-29"
draft = false
author = "Lucy Ofiesh"
image = "/images/homepage-images/ecosystem.jpg"
aliases = [
  "/rcfs",
  "/rcfs/"
]
[menu.main]
rank = 2
parent = "Special programs"
weight = 10
+++

## Background
Following discussions at our July 2023 board meeting, Crossref commenced a large-scale program, dubbed **RCFS - ‘Resourcing Crossref for Future Sustainability’** with the following stated purpose:

> Core to any discussion of resourcing Crossref is understanding what makes us sustainable long term. Organizational sustainability aligns the impact Crossref makes with the financial position required to support it. We want to conduct a thoughtful, comprehensive review that centers on our long-term [vision and plans](strategy), is guided by well-defined problems and principles, includes board, staff, and community input, and details an implementation plan.

Crossref is in a healthy financial position. We’ve experienced steady growth in our revenue and operating size over the past 20 years. We’re self-reliant on program revenue. Our growth has come from natural, broad adoption of membership, Content Registration, and the development of services like Metadata Plus and Similarity Check. Core revenue lines, like membership dues and Content Registration, have grown through volume rather than price increases. In fact, basic Content Registration and membership fees haven’t increased in over 20 years.

But our [fee schedules](/fees) have also strained under the evolution of our membership. They've grown complex over time with the addition of new fees, new record types, or new membership and user categories. The complexity in our fees is hard to manage programatically, and also means it is difficult for the community to reliably predict their costs.

The RCFS program is an umbrella for a few related goals:

* Making fees more equitable
* Simplifying our complex fee schedule
* Rebalancing revenue sources

Changes might result in fee increases for some, but only to the extent that fee increases are in service to those goals. The ideal outcome would be that all of these changes result in as close to an overall revenue-neutral position as possible while ensuring long term sustainability.

Throughout all the project discussions and decisions, we are being mindful of Crossref’s [fee principles](/operations-and-sustainability/#fee-principles), which the board adopted in 2019.

## Scope of the RCFS Program 2024-2025

{{% imagewrap align-right %}}
{{< figure src="/images/community-images/research-consulting-report.png" alt="Cover image of PDF 'Review of Crossref Organisational Stability by Research Consulting'" link="/pdfs/research-consulting-rcfs-report-public.pdf" width="200px" >}}
{{% /imagewrap %}}
We engaged Research Consulting in late 2023 to help us identify 11 potential changes that would have the highest positive impact and be most feasible.

Since then we have narrowed down the 11 projects to five that we will tackle in 2024-2025. In  February 2024, we started working with our expanded [Membership & Fees Committee](/committees/membership-and-fees) to progress the discussions. Their remit is to give and assess community input and data in order to make recommendations to the Board.

This page sets out the work for each of the five projects under the three core program goals.

{{% divwrap green-highlight %}}

#### GOAL: MORE EQUITABLE FEES

1. **Project 1**: Evaluate the USD $275 annual membership fee tier and propose a more equitable pricing structure, which might entail breaking this down into two or more different tiers.
2. **Project 2**: Define a new basis for sizing and tiering members for their capacity to pay

#### GOAL: SIMPLIFY COMPLEX FEES

3. **Project 3**: Address and adjust _volume_ discounts for Content Registration
4. **Project 4**: Address and adjust _backfile_ discounts for Content Registration

#### GOAL: REBALANCE REVENUE SOURCES

5. **Project 5**: Reflect the increasing value of Crossref as a metadata source, likely increasing Metadata Plus fees
{{% /divwrap %}}

read on for more about the goals and the five projects and what's happening with each.

## GOAL: MORE EQUITABLE FEES
Membership characteristics have evolved over time, with revenue concentrated on the furthest ends of the membership tiers. The goal of reviewing the tiers is to align them with how membership participation has changed, reduce the number of tiers, and examine how we apply the sliding scale of pricing to an organization’s capacity to pay.

### Project 1: Evaluate the lowest membership tier and propose a more equitable pricing structure, which might entail breaking this down into two or more different tiers.

The first project in focus is an analysis of the USD $275 [membership tier](/fees/#annual-membership-fee-tiers-for-organizations-registering-published-content). The vast majority of Crossref’s revenues come from the top and bottom membership tiers. 65% of membership revenues come from organisations in the USD $275 tier, which is for organisations sized at < USD $1 million, and membership fees account for a much greater share of these organisations’ total payments to Crossref, at 44%. There are currently over 8000 members in that USD $275 tier, so we need to understand them better.

#### Work to date

* In March 2024, we distributed a survey to all members in the USD $275 tier and received over 1000 responses.
* In May 2024, preliminary survey results were shared and discussed in our [community call](https://zenodo.org/records/12549191), and the final findings were considered by the Membership & Fees Committee, who highlighted a number of possible paths that need to be modelled, as well as the need for further research into barriers to Crossref membership.

#### What's next

* Modelling of future impacts of different approaches to fee changes.
* In August and September 2024, we are inviting feedback from organisations, who are not Crossref members to understand how our fees can be more accessible. We invite volunteers from publishing organisations to come forward and express their interest in sharing their feedback with this [form](https://survey.alchemer.com/s3/7879005/673ef7e88ae5).

### Project 2: Define a new basis for sizing and tiering members for their capacity to pay

We will review the basis for determining annual membership fee. Currently, tier is based on the higher of:

* Total annual publishing revenue from all the divisions of your organization (the member is considered to be the largest legal entity) for all types of activities (advertising, sales, subscriptions, databases, article charges, membership dues, etc). Or, if no publishing revenue then:
* Total annual publishing operations expenses including (but not limited to) staff costs, hosting, outsourcing, consulting, typesetting, etc.

This criterion has become limiting as it is based on the original premise that all Crossref members are publishers. However, we have government bodies and NGOs, funders, news agencies, museums, pharmaceutical companies, and more - who don’t measure publishing revenue or expenses. It should simply be a way to determine size or capacity to pay.

Additionally, even within traditional publishing, so many journals are volunteer-led, that it's been tricky for them to size themselves based on either revenue or expenses, since the volunteer group may be very broad but largely involve just a few snatched hours from many different researchers and editors.

#### Work to date

* Conducting market research of sliding scale fee models

## GOAL: SIMPLIFY COMPLEX FEES

[Content Registration fees](/fees/#content-registration-fees) can be broken down into 14 different record types, which can be further split into 42 different content fees, when accounting for current year (CY), back year (BY) discounts, and volume discounts.

Content registration fees have not changed substantially in 20 years. Fees have been added over time as new record types have been introduced for new communities such as preprint servers and funders.

The number of fee variants creates complexity in our code and billing processes; makes it difficult for members to predict their fees because of computation that can currently only be done at the close of the quarter; and inhibits our ability in the future to change our approach to billing or provide accurate running costs of content registration for members.

### Projects 3 & 4: Review volume and backfile discounts for Content Registration

A lot of the complexity in our billing can be attributed to underused content type provisions like volume and back year discounts. Discounted prices, like discounts for registering back year records, were introduced to encourage members to register as much content as they had, in order to better capture the scholarly record.

Many of these discount categories have very little to no activity in them, but a few are still in use and some conceptually encourage best practice in line with fee principles.

Removing or reducing discount types would have a significant impact on simplifying our fees without risking significant financial impact. Although encouraging previous archival content to be registered is an important incentive. In this review, we're considering to what extent the discounted price encourages registration of high volumes of content and/or back year records.

### Work to date

Summary of a recent Membership & Fees committee discussion of the usefulness of discounts:

{{< figure src="/images/community-images/m&f-discussion-content-reg-discounts.png" alt="Screen grab of Miro board with sticky notes discussing volume and backfile discounts" width="80%" >}}

### What's next
* Analysing recent use trends of the respective content types, volume and back year content types. Considering how these content types are used by members based on their membership tier,  join date, and country.


## GOAL: REBALANCE REVENUE SOURCES

### Project 5: Reflect the increasing value of Crossref as a metadata source, likely increasing Metadata Plus fees

Increasing Metadata Plus fees would help reflect the change in the value of Crossref to the community. Initially, Crossref was primarily an end-point role for members; we are (and will remain) custodians of the scholarly record through metadata. Over the last decade—with the growth of search and APIs and the vision of the Research Nexus—Crossref’s role has expanded to be a hub-point for all scholarly stakeholders, we are also now distributors of metadata between and among all parties in scholarly communications who curate or consume metadata.

In five years, Crossref has seen the following growth:
 | Data point                    | 2018        | 2023         |
|-------------------------------|-------------|--------------|
| Total annual DOI resolutions  | 6 billion   | 13.8 billion |
| Total annual API/Search calls | 7.6 billion | 14.8 billion |

Rebalancing revenue between metadata registration and metadata distribution will more accurately reflect Crossref’s purpose as perceived by the wider community we now serve.

### Work to date

- We have been in touch with a few Query Affiliate subscribers to understand why they use this service
- We have started analysing the costs to Crossref of supporting the Plus API
- We have started gathering usage data in case that is a fee model we could move to

### What's next

- Present the data to the Plus subscribers and discuss
- Present the data and subscriber feedback to the Membership & Fees Committee

---
We invite everyone to comment or ask questions about this program in the dedicated [RCFS Program category on our community forum](https://community.crossref.org/c/strategy/rcfs/47).
