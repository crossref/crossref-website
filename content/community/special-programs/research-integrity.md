+++
title = "Integrity of the scholarly record (ISR)"
date = "2024-07-16"
draft = false
author = "Amanda Bartell"
image = "/images/banner-images/Register references landing page header (2).png"
maskcolor = "crossref-blue"
[menu.main]
rank = 3
parent = "Special programs"
weight = 30
+++


The integrity of the scholarly record is an essential aspect of research integrity. Every initiative and service that we’ve launched since our founding has been focused on documenting and clarifying the scholarly record in an open, machine-actionable and scalable form, and all of this has been done to make it easier for the research community to assess the trustworthiness of scholarly outputs. 

We want to ensure that we are definitely collecting and distributing the right metadata “trust signals” that the community needs to preserve the integrity of the scholarly record, while ensuring that we are really clear on the role that Crossref can and cannot play in this. With this in mind, we’ve created a focused program around Integrity of the Scholarly Record - the “ISR Program”.

This page lists the rationale behind this program, and our ongoing and planned initiatives under this program. 

# Background

The outputs of the research and publishing process create a “scholarly record”. This scholarly record is more than just the published outputs - it’s also a network of inputs, relationships, and contexts. At Crossref, we talk about the “[Research Nexus](https://www.crossref.org/documentation/research-nexus/)”, 

> a rich and reusable open network of relationships connecting research organisations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society.

{{% imagewrap center %}}
{{< figure src="/images/documentation/research-nexus-2023-final.png" alt="Crossref Research Nexus vision image" width="350" align="right">}}
{{% /imagewrap %}}


When published outputs are tied to a persistent and unique identifier, they get a persistent record. Maintaining this record for the long term and having a layer of context associated with the record, establishes the integrity of the scholarly record. The context comes from the metadata associated with the work. In simpler words, scholarly metadata tells us: 

- Who are the authors of a work? 
- What are the affiliations of the authors?
- Which funding programs supported the work?
- Which datasets arose out of the work, OR how is dataset A connected to paper A?
- Was the work updated after its publication, i.e. was it retracted or corrected?
- And more..

> By providing this important context, scholarly metadata can act as a signal of trustworthiness. 

Crossref is focused on providing infrastructure which enables those who produce scholarly outputs to provide metadata and relationships (evidence and context) about how they ensure the quality of their content, and how their outputs fit into the wider scholarly record. We do not assess the quality of our members’ content- the presence of a DOI record is not a signal of quality, but the community can use the presence (or lack of) rich metadata associated with a DOI record as evidence of integrity. 

Read more: [ISR part one: What is our role in preserving the integrity of the scholarly record](https://www.crossref.org/blog/isr-part-one-what-is-our-role-in-preserving-the-integrity-of-the-scholarly-record/)
