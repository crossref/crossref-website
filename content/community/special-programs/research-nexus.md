+++
title = "Research Nexus"
date = "2023-10-24"
draft = true
author = "Kornelia Korzec"
image = "/images/banner-images/Register references landing page header (2).png"
maskcolor = "crossref-blue"
[menu.main]
rank = 2
parent = "Special programs"
weight = 20
+++
