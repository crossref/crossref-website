+++
title = "Register references"
date = "2023-10-24"
draft = false
author = "Rosa Morais Clark"
image = "/images/banner-images/Register references landing page header (2).png"
maskcolor = "crossref-blue"
[menu.main]
rank = 2
parent = "Special programs"
weight = 100
+++

# Make your references count: register today

Improving the discoverability and impact of research across the globe is central to the future of open research. We understand that your time is limited. That’s why we have a clear process for registering your references with Crossref using our Simple Text Query, built with you in mind.

## Give your content the recognition it deserves

We know how hard it is to find time. Once your content is registered through the web deposit form, use our Simple Text Query to add your references. Learn more about how to use our Simple Text Query using either of our handy guides:

### Step-by-step instructions  
<center>
<div style="position: relative; padding-top: 50%; box-shadow: 0 2px 8px 0 rgba(63,69,81,0.16); margin-top: 1.6em; margin-bottom: 0.9em; overflow: hidden; border-radius: 8px; will-change: transform; margin-left: auto; margin-right: auto; width: 50%;">
 <iframe loading="lazy" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; border: none; padding: 0;margin: 0; max-width: 100%;" src="https://www.canva.com/design/DAFYhLp4szo/watch?embed" allowfullscreen="allowfullscreen" allow="fullscreen"> </iframe>
</div>
</center>
<br>

### Instructional video
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/z2-5NzhvuQM?si=thN6RF-egcHGPb9-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</center>   


{{% accordion %}}
{{% accordion-section "Video transcript" %}}

**Transcript for the video "How to use the Crossref Simple Text Query form to register your references with Crossref”**

Hi there. This is a tutorial on how to use Crossref's simple text query form. This is a form that allows you to discover the DOIs for your references and add them to the metadata for a content item that has already been registered with CrossRef and already has a DOI registered for it.

What this does is create a relationship between the citing item and each of the various items that it cites. Be aware that this method will overwrite any references that have already been deposited for that content item. So we recommend that you deposit all of an item's references at once when you use the simple text query.

To start, let's head on over to Crossref's website, crossref.org. Here in the search box, start typing "simple text query." You'll want the second entry here: documentation, simple text query. This page has a whole lot of useful background and information about the simple text query form. For our purposes, we just want to access it right now. So we'll head down here to number one and click this link. Voilà, the simple text query form. It is indeed pretty simple.

You'll want to paste your list of references here in the text field. A couple of things to note about this: 

1. It doesn't matter which citation style you're using. What's important is that you're using a consistent citation style for all of your references. 
2. Each reference should be on its own line, ideally in a numbered list form, although you won't break anything by not having the numbers. 
3. You want to avoid line breaks within individual references as this can result in poorer citation matching from the form. So watch out for that if you're copying the references over from a .doc or a PDF file.

When all your references are in here, click Submit, and the simple text query will match them in a couple of seconds to a minute or two, depending on how many references you have, and it can handle up to 1000 at a time.

And there you have it. It looks like it's found DOIs for seven of the eight references that we included here. Of course, not every cited item has a DOI, but the great feature of the simple text query is that even if something doesn't have a DOI, the form will save it and periodically run checks to see if a DOI has been added for this item.

Every so often, it's a good practice to spot check a few of the DOIs that the simple text query finds to make sure they correspond to the article. When you're happy with the results, click Deposit down here at the bottom. In the Email Address field, you'll enter your email address because you'll be receiving a report after you finish the deposit. 

The parent DOI is the DOI for the content item whose references you're registering here—a reminder that you need to have registered a DOI for the content item first before using the simple text query. So if you haven't yet done that, go back and register the DOI and then come back to the simple text query to deposit its references.

The username is either going to be your organization's shared role credential, which will be a four, five, or six alphanumeric character string, or your individual user credential, which will be an email address followed by the role. And your password is your password. Click Deposit. Great. The simple text query has sent your references to CrossRef. 

You'll now receive two emails: one with the XML of the references you just deposited, and the second, which will confirm if the deposit was a success or a failure. That's it. That's the simple text query. We hope that you'll find that it's easier than ever to deposit references with CrossRef and that you'll start doing it today if you haven't already. Thanks so much.

{{% /accordion-section %}}
{{% /accordion %}}


Make your research more visible, evaluated, and likely to be cited. By registering your references you improve the discoverability of your work, facilitate evaluation, and assist with citation counts and accuracy.

References matter:

- Used by thousands of organisations
- Guide researchers to your content
- Contribute to the evaluation of research
- Progress your metadata strategy

**Start registering references today.**

1. Already submitted your article metadata? Now register your article references to improve discoverability of your article: https://apps.crossref.org/SimpleTextQuery/
2. Ready to submit your article metadata? Add your metadata using our [Web Deposit form](https://apps.crossref.org/webDeposit/) and then register your references with our [Simple Text Query](https://apps.crossref.org/SimpleTextQuery/).

Need more help? Explore our [step by step instructions.](https://www.crossref.org/documentation/register-maintain-records/maintaining-your-metadata/add-references/#00176)

### Why is registering references important?

By registering your references, you provide context and help readers understand the methodology and sources in your content, supporting the scholars to build upon it. Make your content more accessible and discoverable to a wider audience and increase the chances of it being cited.

Help with research assessment. Your authors will benefit from accurate citations as it contributes to the impact of their research. Many citation-counting services use references as a way of tracking citations.

Thanks to the recent changes to our Citedby service, which is now open for all members to use, your references not only support your readers in their research, but now more than ever before – they can help guide others to your content as well.

## Top tips for submitting your references:

- Work with authors to make sure that your references are complete and accurate. This includes each source's author, title, publisher, publication date, and –– if available –– the DOIs.
- Use a consistent citation style. This will make it easier for readers to find your references and understand how to cite them in their own work.
- Submit your references straight after you register your content DOIs so they are included in citation databases, and citations are counted accurately.

Looking for support? Use our [community forum](https://community.crossref.org/c/content-registration/24) or email <a href="mailto:support@crossref.org?subject=Registering references email support">`support@crossref.org`</a>.

## Helpful resources:

{{% divwrap darkblue-highlight %}}

#### [Our blogs about references](/categories/references/)

{{% /divwrap %}}

{{% divwrap darkgrey-highlight %}}

#### [Amendments to membership terms to open reference distribution and include UK jurisdiction](/blog/amendments-to-membership-terms-to-open-reference-distribution-and-include-uk-jurisdiction/)

{{% /divwrap %}}

{{% divwrap green-highlight %}}

#### [Linking references is different from depositing references](/blog/linking-references-is-different-from-depositing-references/)

{{% /divwrap %}}
