+++
title = "For librarians"
date = "2017-03-02"
draft = false
rank = 5
weight = 24
aliases = [
    "/01company/07libraries.html",
    "/01company/free_services_agreement.html",
    "/03libraries/16lib_how_to.html",
    "/03libraries/index.html",
    "/03libraries/27forlibraries.html"
]
+++


Libraries and Crossref are a winning combination. Our shared goal is to improve discoverability of content for researchers. For our part we look after our members' metadata and run a registry of persistent links. We also offer services that help systems and people to make connections.

We currently look after over `160,104,382` records from theses, dissertations, preprints, grants, and reports-- through to journal articles and books/book chapters. Enhance your metadata and connect your discovery and linking services with these records, they're all available through [open APIs](https://api.crossref.org/) and [search] (https://search.crossref.org/). 

## Retrieve metadata into your library discovery system using our OpenURL service

If you are a librarian and you need to use OpenURL with your library link resolver e.g. Alma, an email address should be supplied in queries that the link resolver sends to Crossref. This will be configured in your link resolver. You can find more information in our [OpenURL documentation](/documentation/retrieve-metadata/openurl/).

## The metadata we take in

Our members register records with us including as much metadata as possible, they then use each others' metadata to link their references and commit to maintaining these links for the long-term. 

Increasingly we are being asked to take in other scholarly outputs such as grants, peer review reports, videos, blogs, and other. Watch our [blog](/blog) for news of these additions or sign up to our newsletter via the link in the footer of this website.

## Other ways to get metadata

One of our responsibilities is to make metadata available to the systems and people who need it. The benefits to libraries in using our metadata are that you can ensure persistent links, which lead to increased discoverability of your resources. And it also means you don't have to sign individual linking agreements with each publisher or keep track of their different ways of linking.

We have a number of [metadata retrieval](/services/metadata-retrieval) options, in addition to OpenURL. The most visible is [search.crossref.org](https://search.crossref.org) (for humans!) which uses our [REST API](/education/retrieve-metadata/rest-api/) (for machines!). Both interfaces are open and free to use without registering.

From [search.crossref.org](https://search.crossref.org) you can search for a DOI, an article, an author, an ORCID iD, etc. You can search right from our [homepage](/) on the second tab 'search metadata' or go directly to search.crossref.org.

We have a browsable [title list](https://apps.crossref.org/titleList/) of all titles registered with us, by journal, book series, or conference proceeding.

## Library publishers

A growing number of libraries are publishers themselves. Many of the approximately 180 members that join every month are, for example, a scholar-publisher or a library-publisher. If you'd like to register content and contribute metadata please take a look at the [member terms](/membership/terms) and if you can meet them, [apply to join](/membership).

---
Contact our [outreach team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) to set up an account or ask any questions. [Technical support](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) is also available from our metadata experts.
