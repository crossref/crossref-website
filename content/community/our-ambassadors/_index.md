+++
title = "Meet our ambassadors"
date = "2021-03-10"
draft = false
author = "Johanssen Obanda"
[menu.main]
parent = "Ambassadors"
rank = 4
weight = 10
+++




The Crossref Ambassador Program is an exciting and important program initiated in early 2018, and one which fully embraces a key strategic focus---to adapt to expanding constituencies.

Our Ambassadors are enthusiastic volunteers who work within the global academic community in a variety of ways---as librarians, researchers, publishers, and societies,---and all of whom share a strong belief in the mission-driven work we do to improve scholarly research communication. They support us by using their industry expertise, local knowledge, and translation skills to represent Crossref at regional events---providing training to our members in different languages, locations and time zones.

See who is based in your region: 

### [Africa](/community/our-ambassadors/africa/)  
### [Asia](/community/our-ambassadors/asia/)  
### [Americas](/community/our-ambassadors/americas/)  
### [Europe](/community/our-ambassadors/europe/)  
### [Oceania](/community/our-ambassadors/oceania/)  


Country groupings are based on the geographic regions defined under the [Standard Country or Area Codes for Statistical Use (known as M49)](https://unstats.un.org/unsd/methodology/m49) of the United Nations Statistics Division. The assignment of countries or areas to specific groupings does not imply any assumption regarding political or any other affiliation of countries or territories.   

## Apply to become an ambassador

If you are interested in finding out more about the Ambassador Program and working with us please fill out the [application form](/community/ambassadors/) to give us a little information about yourself. We'll then get back to you to follow-up and discuss your plans and ideas.

<a href="#top">Back to top</a>

---

Please contact our [outreach team](mailto:feedback@crossref.org) with any questions.
