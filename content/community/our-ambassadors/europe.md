+++
title = "Meet our ambassadors in Europe"
date = "2023-08-01"
draft = false
author = "Johanssen Obanda"
parent = "Our ambassadors"
rank = 4
weight = 10
+++



The Crossref Ambassador Program is an exciting and important program initiated in early 2018, and one which fully embraces a key strategic focus---to adapt to expanding constituencies.

Our Ambassadors are enthusiastic volunteers who work within the global academic community in a variety of ways---as librarians, researchers, publishers, and societies,---and all of whom share a strong belief in the mission-driven work we do to improve scholarly research communication. They support us by using their industry expertise, local knowledge, and translation skills to represent Crossref at regional events---providing training to our members in different languages, locations and time zones.

See who is in Europe:  

### Belarus - Alexey Skalaban (currently inactive)
{{% imagewrap left %}} <img src="/images/ambassadors/Alexey-Skalaban.jpg" alt="Alexey Skalaban" width="200px" class="img-responsive" /> {{% /imagewrap %}}  

Alexey graduated from the Belarusian State University of Culture, specializing in automated library and information systems.
From 2009 to August 2017, he served as director of the Scientific Library of the Belarusian National Technical University. Prior to that, he was engaged in the acquisition of electronic resources in the National Library of Belarus. Specialist in the field of creating institutional open access repositories, managing and providing access to scientific electronic information resources, PIDs. Alexey is an author of more than 30 publications in professional journals and collections and trained in the largest libraries in the USA, Poland, Sweden, Germany.   

Окончил Белорусский государственный университет культуры, специализация — автоматизированные библиотечно-информационные системы.
С 2009 г. по август 2017 г. занимал должность директора Научной библиотеки Белорусского национального технического университета. До этого занимался комплектованием электронных ресурсов в Национальной библиотеке Беларуси. Специалист в области создания институциональных репозиториев открытого доступа, управлению и обеспечению доступа к научным электронным информационным ресурсам, постоянным идентификаторам.
Автор более 30 публикаций в профессиональных журналах и сборниках.
Стажировался в крупнейших библиотеках США, Польши, Швеции, Германии.

### France - Frédéric Lefrançois
{{% imagewrap left %}} <img src="/images/ambassadors/Frederic-Lefrancois.jpeg" alt="Frédéric Lefrançois" width="200px" class="img-responsive" /> {{% /imagewrap %}}  

Frédéric is a researcher/lecturer at the University of the French West Indies. While practising research and lecturing in English Studies and Visual/Performing Arts, he has developed a keen interest in cultural anthropology. After the obtention of his Ph.D. in English Studies, his research has focused on the relationship between aesthetics and social sculpture in a variety of Transamerican diasporic contexts: visual arts, cinema, drama, and performance. He has authored two books and edited 3 journal international issues. He believes that research accessibility and visibility are key to fostering excellent scientific cooperation, hence his engagement with Crossref.

Frédéric est chercheur/enseignant à l’Université des Antilles Françaises. Tout en pratiquant la recherche et en enseignant en études anglaises et en arts visuels/du spectacle, il a développé un vif intérêt pour l'anthropologie culturelle. Après l'obtention de son doctorat. en études anglaises, ses recherches se sont concentrées sur la relation entre l'esthétique et la sculpture sociale dans divers contextes diasporiques transaméricains : arts visuels, cinéma, théâtre et performance. Il est l'auteur de deux livres et a édité trois numéros internationaux de revues. Il estime que l’accessibilité et la visibilité de la recherche sont essentielles pour favoriser une excellente coopération scientifique, d’où son engagement auprès de Crossref.

### Italy - Eleonora Colangelo
{{% imagewrap left %}} <img src="/images/ambassadors/Eleonora-Colangelo.jpeg" alt="Eleonora Colangelo" width="200px" class="img-responsive" /> {{% /imagewrap %}}

An Open Science and academic publishing professional, she obtained her PhD in Classics in 2020. Since January 2023, she has been a Publishing Specialist at Frontiers, contributing to science outreach and research dissemination. Formerly a Project Manager at a leading Italian software house, she played a pioneering role in introducing Crossref services to Italy. Qualified as a Maître de conférences by the French National University Council, she mentors at STM, the University of Pisa, and the Society for Scholarly Publishing. Additionally, she collaborates with the Council of Science Editors as a member of the Education Committee.

Esperta nel campo della scienza aperta e dell'editoria scientifica, ha conseguito il dottorato in Storia Antica nel 2020. Dal gennaio 2023, ricopre il ruolo di Publishing Specialist presso Frontiers, contribuendo attivamente alla divulgazione scientifica e alla diffusione della ricerca. In precedenza, ha svolto un ruolo chiave nella promozione dei servizi di Crossref in Italia come Project Manager presso una nota software house, partner tecnologico globale di importanti università, centri di ricerca e university press italiane. Qualificata come Maître de conférences dal Consiglio Nazionale delle Università francese, è mentore presso STM, l'Università di Pisa e la Society for Scholarly Publishing. Inoltre, collabora con il Council of Science Editors come membro del suo Education Committee.

### Romania - Nicoleta-Roxana Dinu
{{% imagewrap left %}} <img src="/images/ambassadors/Nicoleta-Roxana-Dinu.jpg" alt="Nicoleta-Roxana Dinu" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Nicoleta-Roxana Dinu holds a PhD in Library and Information Science at the University of Bucharest. She works at the National Library of Romania, Institutional Development Department. She has been responsible for international relations for the Profesional de la información Journal and the webmaster of the mentioned journal. She is currently editor of the Infonomy Journal (Spain), Scientific advisor of the Journal of Creative Industries and Cultural Studies (JOCIS), Portugal and member of the Advisory Board of the Central European Library and Information Science Review (CeLISR) Journal, Hungary. She is editor of e-LIS for Romania and Moldova. She has published articles on digital information, metadata, trends in scientific journals, use of repositories and social networks.

Nicoleta-Roxana Dinu este doctor în Științele informării și documentării, în cadrul Universității din București. Își desfășoară activitatea la Biblioteca Națională a României, în serviciul Dezvoltare instituțională. A lucrat în Biroul de Relații internaționale al revistei Profesional de la información și a fost webmaster-ul aceleași reviste. În prezent, este editor al revistei Infonomy (Spania), consilier științific al revistei Journal of Creative Industries and Cultural Studies (JOCIS), Portugalia, și membru în Consiliul Consultativ al revistei Central European Library and Information Science Review (CeLISR), Ungaria. Este editor e-Lis pentru România și Republica Moldova. A publicat articole despre informații digitale, metadate, tendințe în revistele științifice, dar și despre utilizarea depozitelor digitale și rețelelor de socializare.

### Russia - Maxim Mitrofanov (currently inactive)
{{% imagewrap left %}} <img src="/images/ambassadors/maxim-mitrofanov.png" alt="Maxim Mitrofanov" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Current position -- Not-for-Profit Partnership National Electronic Information Consortium (NP NEICON). NEICON provides all kinds of services for the editors, universities, libraries, etc., such as publishing, consulting, ensuring access to international databases and many more. I was born in Moscow, Russia April 8, 1977, graduated from the University of Foreign Relations and then worked for the Ministry of Foreign Affairs of Russia for nine years. Within that term I spent six years on different positions in Russian Embassies to Canada and then Ghana.

After I left the Ministry in 2007 I worked for the Russian largest Exhibition company, Expocentre and then joined NEICON in 2014. Besides the regular activities in 2015 I was invited to become a DOAJ Ambassador and Associate Editor and I've held this position since then. Within my activities I was one of the first in Russia who started to promote Crossref services and advise editors about their importance to the editorial process and science information exchange at personal meetings, conferences and seminars. NEICON is a Crossref Sponsor in Russia as well. As a result of the work and the recognition of DOIs by the wide Russian editorial audience I receive a number of applications for Crossref services daily and provide the applicants with the necessary information. Since 2018 Crossref Ambassador in Russia.

Максим Митрофанов. В настоящее время Руководитель партнерских программ в Некоммерческом партнерстве Национальный электронно-информационный консорциум (НП НЭИКОН). Основная задача НЭИКОН -- предоставление полного спектра услуг для научных организаций России -- вузов, университетов, библиотек, издательств научной литературы, это и помощь в издании научной периодики, обеспечение подпиской на международные ресурсы, консультации и прочее. Родился в Москве 8 апреля 1977 г. Закончил Московский государственный институт международных отношений после чего девять лет работал в МИД России, включая различные позиции в Посольствах России в Канаде и Гане.

С 2007 г. работал в крупнейшей российской выставочной компании Экспоцентр. Пришел в НЭИКОН в 2014 г. Помимо основной работы с 2015 г. являюсь представителем и редактором DOAJ в России. В 2014 г. в рамках НЭИКОН одним из первых в России начал пропагандировать использование цифровых идентификаторов для научного контента, пояснять правила и технологии использования doi на встречах, в ходе конференций и семинаров.  C 2018 г. являюсь представителем Crossref в России.

### Russia - Angela P. Maltseva (currently inactive)
{{% imagewrap left %}} <img src="/images/ambassadors/Maltseva-Angela.jpg" alt="Angela Maltseva" width="200px" class="img-responsive" /> {{% /imagewrap %}}

As Doctor of Philosophy, Associate Professor, Chief Researcher and Professor of the Department of Philosophy and Culturology, Angela Maltseva conducts lectures and seminars in Political Science, Sociology, Sociology of Trust, Modern Political Structure of Western countries, History and Philosophy of Science. She has been Editor-In-Chief of the scientific journal "Volga Region Pedagogical Search" since 2017. The journal is interested in the ways and means of creating trusted environments, the social and educational influences on country development, citizen well-being and dignity, and articles about the role of educational institutions and children-adult communities in the accumulation of social capital. Since 2018 her University (UlSPU named after I.N. Ulyanov) is the first Crossref Sponsor in the Volga region.

Мальцева Анжела Петровна. Будучи доктором философских наук и главным научным сотрудником, Мальцева А.П. в настоящее время читает лекции по социологии, политологии, социологии доверия, современному политическому устройству стран Запада, истории и философии науки. С 2017 года Анжела Петровна является главным редактором научного журнала «Поволжский педагогический поиск», публикующего материалы о доверительных средах и путях их создания, о роли образовательных институтов и взросло-детских сообществ в накоплении социального капитала, о влиянии социальных и образовательных институтов на благополучие граждан, чувство их собственного достоинства. C 2018 года УлГПУ им. И.Н. Ульянова функционирует как первый в Поволжье представитель Crossref и спонсор организаций, осознающих всю важность цифровой идентификации своего контента.   

### Serbia -  Lazar Stošić
{{% imagewrap left %}} <img src="/images/ambassadors/Lazar.png" alt=" Lazar Stošić" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Prof. Dr. Lazar Stošić is a university professor at the Faculty of Informatics and Computer Science, University Union—Nikola Tesla, Belgrade, Serbia and the President of the Association for the Develop and the President of the Association for the Development of Science, Engineering and Education, in Serbia. He is also a leading researcher at the Center for Scientific Competence of DSTU, Department of Scientific and Technical Information and Scientific Publications Don State Technical University, Russia. Lazar has over ten years of experience in scholarly publishing. His expertise includes editorial workflow management, conference organization, web technologies, web design, indexing, XML production, SEO, digital marketing, and new media technologies. 

Lazar is Editor in Chief of the International Journal of Cognitive Research in Science, Engineering and Education and a member and reviewer of many international scientific journals, providing technical support for submitting metadata to Crossref and using Crossref tools. His main aim as an ambassador is to help organizations and researchers understand the benefits of Crossref membership and the services that Crossref provides.  

Prof. dr Lazar Stošić, je univerzitetski profesor na Fakultetu za informatiku i računarstvo, Univerzitet Union - Nikola Tesla, Beograd, Srbija i predsednik Udruženja za razvoj nauke, inženjerstva i obrazovanja u Srbiji. Takođe radi kao vodeći naučni istraživač u Centru za naučne kompetencije DSTU, Odeljenje za naučne i tehničke informacije i naučne publikacije, Donskog državnog tehničkog univerziteta, Rostov na Donu, Rusija. Lazar ima više od deset godina iskustva u naučnom izdavaštvu. Njegova stručnost obuhvata upravljanje procesom rada u izdavaštvu, organizovanje konferencija, veb tehnologije, veb dizajn, indeksiranje, XML produkciju, SEO, digitalni marketing, i nove medijske tehnologije.

Lazar je glavni i odgovorni urednik WoS i Scopus indeksiranog časopisa International Journal of Cognitive Research in Science, Engineering and Education, član i recenzent mnogih međunarodnih naučnih časopisa gde pruža tehničku podršku za dostavljanje metapodataka u Crossref-u kao i za korišćenje drugih alata koje pruža Crossref. Njegov glavni cilj kao ambassadora je da pomogne istraživačkim organizacijama i istraživačima da shvate prednosti članstva u Crossref-u i usluga koje Crossref nudi.


### Ukraine - Anna Danilova
{{% imagewrap left %}} <img src="/images/ambassadors/Anna-Danilova.jpg" alt="Anna Danilova" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Anna Danilova, assistant to the Head of subscription agency "Ukrinformnauka", is a leading specialist of Publishing House "Academperiodyka", founded under the National Academy of Sciences (NAS) of Ukraine. Her primary focus is maintaining online platforms for the scientific journals of NAS of Ukraine; since 2014 she has been providing technical support for submission of the scientific publication metadata to Crossref. Anna takes active part in the organization and holding of conferences, seminars and workshops throughout Ukraine, where all attendees have an opportunity to get detailed information about Digital Object Identifiers (DOIs), additional Crossref services and guidance about submission of scientific publications to the database. She is an author of a number of articles, educational and training materials.

Данілова Анна - заступник директора передплатного агентства «Укрінформнаука», провідний спеціаліст Видавничого дому «Академперіодика» Національної академії наук України. Сфера її діяльності включає в себе роботу з інтернет-ресурсами наукових періодичних видань НАН України, а з 2014 року – забезпечення технічної підтримки процесів депонування метаданих наукових статей і монографій в базу даних Crossref. Анна бере активну участь в підготовці й проведенні конференцій, семінарів та майстер-класів в різних містах України, на яких усі бажаючі мають можливість отримати детальну інформацію про цифрові ідентифікатори DOI, додаткові сервіси Crossref, підготовку ресурсів наукових видань до депонування. Окрім цього, вона є автором низки статей та навчально-методичних посібників.   

