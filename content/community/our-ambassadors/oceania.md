+++
title = "Meet our ambassadors in Oceania"
date = "2023-08-01"
draft = false
author = "Johanssen Obanda"
parent = "Our ambassadors"
rank = 4
weight = 10
+++




The Crossref Ambassador Program is an exciting and important program initiated in early 2018, and one which fully embraces a key strategic focus---to adapt to expanding constituencies.

Our Ambassadors are enthusiastic volunteers who work within the global academic community in a variety of ways---as librarians, researchers, publishers, and societies,---and all of whom share a strong belief in the mission-driven work we do to improve scholarly research communication. They support us by using their industry expertise, local knowledge, and translation skills to represent Crossref at regional events---providing training to our members in different languages, locations and time zones.

See who is in Oceania:  


### Australia - Melroy Almeida
{{% imagewrap left %}} <img src="/images/ambassadors/melroy-almeida-sq2.jpg" alt="Melroy Almeida" height="200px" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Melroy currently works at the Australian Access Federation (AAF) as their ORCID Technical Support Analyst. AAF is the consortium lead for the Australian ORCID Consortium and as part of his day to day work Melroy works with the Australian ORCID Consortium members on their ORCID implementations as well as assists them in planning their communication and engagement strategy. As part of his work with ORCID, Melroy occasionally gets questions about DOIs, metadata and discoverability. "My aim is to help research organisations and researchers understand the benefits of PIDs, why it is needed and how it helps within the scholarly research lifecycle". In addition to English, Melroy also speaks Hindi and Marathi.
In his spare time after work and family commitments, Melroy can be found playing/coaching football (soccer) or sitting on the couch reading a good book.


