+++
title = "Meet our ambassadors in Africa"
date = "2023-08-01"
draft = false
author = "Johanssen Obanda"
parent = "Our ambassadors"
rank = 4
weight = 10
+++



The Crossref Ambassador Program is an exciting and important program initiated in early 2018, and one which fully embraces a key strategic focus---to adapt to expanding constituencies.

Our Ambassadors are enthusiastic volunteers who work within the global academic community in a variety of ways---as librarians, researchers, publishers, and societies,---and all of whom share a strong belief in the mission-driven work we do to improve scholarly research communication. They support us by using their industry expertise, local knowledge, and translation skills to represent Crossref at regional events---providing training to our members in different languages, locations and time zones.

See who is in Africa:  

### Algeria - Younes Saaid
{{% imagewrap left %}} <img src="/images/ambassadors/Younes-Saaid.jpeg" alt="Younes Saaid" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Dr. Younes SAAID is in charge of Scientific and Technological Information at the Centre for Research in Social and Cultural Anthropology (CRASC) in Oran, Algeria. He is a skilled researcher holding a Ph.D. in Didactics of English for Specific Purposes (ESP). His expertise includes language contact, language teaching methodologies, and ESP. Younes actively contributes to the field by indexing academic journals and enhancing research visibility. He also plays a vital role in the Algerian Scientific Journals Platform (ASJP), promoting Algerian research and managing the Open Journal Systems (OJS) for various journals.

<div style="text-align: right">
الدكتور يونس سعيد مكلف بالإعلام العلمي والتكنولوجي بمركز البحث في الأنثروبولوجيا الاجتماعية والثقافية في وهران،
الجزائر. وهو باحث متمكن حاصل على دكتوراه في تعليمية الإنجليزية لأغراض خاصة. تشمل خبرته في اللسانيات
والتواصل اللغات، مناهج تدريس اللغات، والإنجليزية لأغراض خاصة. يساهم يونس بشكل فعال في المجال من خلال
فهرسة المجلات العلمية وتعزيز رؤية البحث العلمي. كما يلعب دورًا هامًا في منصة المجلات العلمية الجزائرية، حيث
يعمل على الترويج للبحث الجزائري وإدارة أنظمة المجلات المفتوحة لمجلات مختلفة.
</div>

### Cameroon - Audrey Kenni Nganmeni
{{% imagewrap left %}} <img src="/images/ambassadors/Audrey-Kenni.png" alt="Audrey Kenni" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

I am Audrey Kenni Nganmeni, editor at the Pan African Medical Journal in charge of legal affairs and focal person of the journal with Crossref, a journal based in Cameroon, Africa. I am very glad to be part of Crossref ambassadors where I will learn more about Crossref services, benefit of diverses training and help Crossref with my French skills.

Je suis Audrey Kenni Nganmeni, éditrice au Pan African Medical Journal en charge des affaires juridiques et point focal de la revue avec Crossref. Je suis basée au Cameroun en Afrique. Je suis très heureuse de faire partie des ambassadeurs Crossref où je pourrai en savoir plus sur les services Crossref, bénéficier de diverses formations et aider Crossref avec mes compétences en français.    
<br><br>

### Egypt - Ahmed Moustafa
{{% imagewrap left %}} <img src="/images/ambassadors/Ahmed-Moustafa.jpg" alt="Ahmed Moustafa" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

I’m an Academic Publishing professional who has wide expertise in the Scholarly Publishing industry. I have a B.Sc. in Chemistry/Geology from Cairo University in Egypt, and now I’m the Production Manager at Knowledge E in the UAE. I started my career by joining the leading open access publisher, Hindawi Publishing Corporation. Afterwards, I had an enriching journey at Andromeda Publishing and Academic Services as an Executive Director. I support openness and integrity of research, as well as integration and collaboration between the different societies of the publishing community. I’m a member of the Creative Commons Global Network and of the Industry Partnership Committee of the International Society of Managing and Technical Editors.  

<div style="text-align: right">
أحمد مصطفى، متخصص في النشر الأكاديمي ولديه خبرة واسعة في صناعة النشر أحمد مصطفى، متخصص في النشر الأكاديمي ولديه خبرة واسعة في صناعة النشر العلمي. حصل على درجة البكالوريوس في الكيمياء/الجيولوجيا من جامعة القاهرة في مصر وهو الآن مدير الإنتاج بنولدج اي في الإمارات العربية المتحدة. بدأ حياته المهنية بالانضمام إلى رائد النشر مفتوح الوصول "غير مقيد الوصول إليه"، مؤسسة هنداوي للنشر ثم خاض تجربة غنية بشركة اندروميدا للنشر والخدمات الأكاديمية في المملكة المتحدة كمديراً تنفيذياً. يدعم أحمد انفتاح ونزاهة البحث، فضلاً عن التكامل والتعاون بين مختلف مؤسسات مجتمع النشر. وهو عضو في شبكة المشاع الإبداعي العالمية ولجنة الشراكة الصناعية إحدي لجان الجمعية الدولية للمحررين الإداريين والتقنيين.  
</div>


### Ghana - Richard Bruce Lamptey
{{% imagewrap left %}} <img src="/images/ambassadors/Bruce-Richard.jpeg" alt="Richard Bruce Lamptey" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Richard Bruce Lamptey is the Librarian of the College of Science Library and a Deputy Librarian in the KNUST Library System. He is knowledgeable in digital libraries, data curation, digital repositories, information management, and open access / open data issues. Very much results-driven, go-getter, follow transformational leadership principles. Has supported national and institutional open access awareness raising and advocacy workshops that have resulted in several open access repositories in the country. Through his work, the first open access mandate in the country was introduced by Kwame Nkrumah University of Science and Technology. He holds PhD, MPhil, MA and Diploma (Library and Information Studies). He is very passionate about Knowledge sharing, interested in equity in scholarly communications and research, alternative metrics, grey literature and open access.

### Kenya - Mercury Shitindo
{{% imagewrap left %}} <img src="/images/ambassadors/Mercury-Shitindo.jpg" alt="Mercury Shitindo" width="200px" class="img-responsive" /> {{% /imagewrap %}}

With over 18 years of management experience, Ms. Shitindo is a researcher and bioethicist. She currently chairs the Africa Bioethics Network, is an editor at the African Journal of Bioethics, serves on the BCA-WA-ETHICS II Project Advisory Board, and is a Technical Expert for Global Impact. She is an alumnus of the WCG IRB International Fellows Program and trains Open Peer Reviewers. She promotes human rights and human dignity in African society through research, writing, and capacity-building. She volunteers at Africans Rising as a Regional Resource Mobilizer and is a Crossref Ambassador. Her research aims to promote equitable access to necessities, education, and health by endorsing ethical research conduct and upholding human rights and dignity.

Akiwa na uzoefu wa usimamizi wa zaidi ya miaka 18, Bi. Shitindo ni mtafiti na mtaalamu wa maadili. Kwa sasa ni mwenyekiti wa Mtandao wa Maadili ya Kibiolojia Afrika, ni mhariri katika Jarida la Afrika la Maadili ya Kibiolojia, anahudumu katika Bodi ya Ushauri ya Mradi wa BCA-WA-ETHICS II, na ni Mtaalam wa Kiufundi wa Athari za Kiulimwengu. Yeye ni mhitimu wa Mpango wa Kimataifa wa Wenzake wa WCG IRB na hufunza Wakaguzi Huria wa Rika. Anakuza haki za binadamu na utu katika jamii ya Kiafrika kupitia utafiti, uandishi, na kujenga uwezo. Anajitolea katika Africans Rising kama Mhamasishaji wa Rasilimali za Kanda na ni Balozi wa Crossref. Utafiti wake unalenga kukuza upatikanaji sawa wa mahitaji, elimu, na afya kwa kuidhinisha mwenendo wa utafiti wa kimaadili na kuzingatia haki za binadamu na utu. 

### Senegal - Oumy Ndiaye
{{% imagewrap left %}} <img src="/images/ambassadors/Oumy-Ndiaye.jpeg" alt="Oumy Ndiaye" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Dr. Oumy Ndiaye, a certified gender and ethics expert, is a health economist at Cheikh Anta Diop University, Dakar. Her areas of expertise include health financing, inequalities in access to care, sexual and reproductive health, and children’s health. She has led numerous projects and programs in this field, and has collaborated with the United Nations and the international NGO FIND as an international consultant. Dr. Ndiaye has published several articles and a significant work on demographic dividend and development. Currently, she is spearheading a project on violations of the rights of domestic workers in French-speaking West Africa.

Le Dr Oumy Ndiaye, experte certifiée en genre et en éthique, est économiste de la santé à l'Université Cheikh Anta Diop de Dakar. Ses domaines d’expertise incluent le financement de la santé, les inégalités d’accès aux soins, la santé sexuelle et reproductive et la santé des enfants. Elle a dirigé de nombreux projets et programmes dans ce domaine, et a collaboré avec les Nations Unies et l'ONG internationale FIND en tant que consultante internationale. Le Dr Ndiaye a publié plusieurs articles et un ouvrage important sur le dividende démographique et le développement. Actuellement, elle mène un projet sur les violations des droits des travailleuses domestiques en Afrique de l’Ouest francophone.

### South Africa - Mokheseng Richard Buti
{{% imagewrap left %}} <img src="/images/ambassadors/Mokheseng-Buti.jpg" alt="Mokheseng Richard Buti" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Mokheseng Richard Buti is a Portfolio Manager at Taylor & Francis, dedicated to advancing knowledge within Africa. With over seven years of experience, I manage relationships with co-publishing partners and stakeholders, ensuring the dissemination of high-quality research. Passionate about curating reliable information in an era flooded with misinformation, I strive to amplify African research and voices through impactful projects. My commitment to truth and accuracy has been recognized with prestigious awards, underscoring my role as a patriotic ambassador for scholarly excellence in Africa. I believe in the power of knowledge-sharing to transform lives and drive meaningful societal change.  

### South Africa - Sidney Engelbrecht
{{% imagewrap left %}} <img src="/images/ambassadors/Sidney-Engelbrecht.jpg" alt="Sidney Engelbrecht" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Sidney is a Senior Research Compliance Specialist at King Abdullah University of Science and Technology in Saudi Arabia with 15 years of  experience in research ethics and integrity. He is an accredited Research Management Professional by the International Professional Recognition Council. He is the recipient of the Award for Distinguished Contribution to the Research Management Profession and co-recipient of the Anderson-Kleinert Diversity Award. He is a Research Group Fellow (with distinction) of the Center for AI and Digital Policy (US) and participates in the EdSafe Catalyst Fellowship Programme. He is currently pursuing a PhD in AI Ethics.

Sidney is 'n Senior Navorsingsnakomingsspesialis by King Abdullah Universiteit van Wetenskap en Tegnologie in Saoedi-Arabië met 15 jaar ondervinding in navorsingsetiek en integriteit. Hy is 'n geakkrediteerde Navorsingsbestuursprofessiel deur die International Professional Recognition Council. Hy is die ontvanger van die Toekenning vir Uitnemende
Bydrae tot die Navorsingsbestuursberoep en mede-ontvanger van die Anderson-Kleinert Diversiteitstoekenning.
Hy is 'n Navorsingsgroepgenoot (met lof) van die Sentrum vir KI en Digitale Beleid (VS) en neem deel aan die EdSafe Catalyst Fellowship-program. Hy is tans besig met 'n PhD in KI-etiek.

### Tanzania - Baraka Manjale Ngussa
{{% imagewrap left %}} <img src="/images/ambassadors/Baraka-Ngussa.jpg" alt="Baraka Ngussa" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Baraka Manjale NGUSSA holds a PhD in Education (Curriculum and Teaching) from the University of Eastern Africa Baraton, Kenya. He is an experienced educator, researcher, and administrator in higher learning institutions particularly at the University of Arusha in Tanzania where he currently serves as the Director of Human Resources and Administration. He has been the President for Tanzania Adventist Authors and Writers Association (TAAWA) since 2019. He has a wide experience in teaching, publication and supervision and has authored over 60 publications including books, journal articles, encyclopedia sections, and book chapters. Baraka is the founder, CEO and Chief Editor of the East African Journal of Education and Social Sciences (EAJESS) which is indexed by African Journals Online (AJOL). His research areas include curriculum and teaching, educational management, and leadership. As Crossref Ambassador, Baraka’s passion is to provide his expertise in supporting academic journals in Africa to acquire and maintain high quality standards.

Baraka Manjale NGUSSA ana Shahada ya Uzamivu katika Elimu (Mitaala na Ufundishaji) ya Chuo Kikuu cha Afrika Mashariki, Baraton kilichoko Kenya. Baraka ana uzoefu mwingi katika ufundishaji, utafiti na uongozi katika Elimu ya Juu hasa Katika Chuo Kikuu cha Arusha ambako kwa sasa ni Mkurugenzi wa Rasilimali watu na Utawala. Amekuwa Rais wa Chama cha Watunzi wa Waandhishi wa Kiadventista nchini Tanzania tangu mwaka 2019. Ana uzoefu mpana katika kufundisha, uandhishi na usimamizi wa tafiti, na ana machapisho zaidi ya 60 ikiwa ni pamoja na vitabu, sura za vitabu pamoja na makala mbalimbali. Baraka ni muasisi na mhariri mkuu wa jarida la Afrika Mashariki la Elimu na Sayansi Jamii ambalo limewekwa katika African Journals Online (AJOL). Maeneo yake ya utafiti ni katika mitaala na ufundishaji, utawala wa elimu pamoja na uongozi. Akiwa Balozi wa Crossref, lengo lake ni kutoa uzoefu wa kitaalamu ili kuwezesha majarida yaliyoko Afrika Kushiriki katika mtandao wa Crossref wa kiulimwengu wa majarida  ya kitaaluma.   