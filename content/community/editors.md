+++
title = "For editors"
date = "2017-01-18"
draft = false
author = "Madhura Amdekar"
rank = 5
parent = "Working for you"
weight = 1
+++

Your decisions influence what research is communicated and how. Demonstrate your editorial integrity with tools that help you assess a paper’s originality, and properly label and connect updates, corrections, and retractions.

### Get discovered

Using our services provides a way for editors to maximize the discoverability of the content they publish.

Registering DOIs and metadata with us means that content can be found and used alongside that of our [members](/dashboard). We also ask our members to link their references using the DOI as this makes sure that content will be linked persistently to the work it cites, so that links to your publications won’t deprecate over time and readers can find them long-term. You can encourage your authors to add DOIs to the reference lists of the papers they submit to you, and [we have tools](/education/retrieve-metadata/simple-text-query/) to help with that.

Other things that will help your content get discovered include collecting information on who funded the research behind the content you publish and the ORCID iDs of your authors, so that it can be found by readers searching on those criteria too. If you collect that information, make sure it’s being deposited with Crossref so that it can be used. Not sure if that's the case? [Let us know](mailto:support@crossref.org).

### Integrity and ethics

As an editor, we know you’re on the front-line to ensure the quality and integrity of what you publish. We provide the [Similarity Check service](/services/similarity-check) which gives members a tool to help editors and publishers ensure that work is original (and references other work properly) by checking it against a growing database of academic publications and general web content. Questions about Similarity Check? [Get in touch](mailto:similaritycheck@crossref.org).

Even with thorough processes in place, changes may happen to a work after it has been published which may affect how it should be interpreted or credited. Perhaps some supplementary information has been added or the work needs to be corrected or retracted. It’s important that your readers know these changes have happened to research they want to read or cite. [Crossmark](/services/crossmark) provides a way to communicate this information in a standard way across publishers so that researchers don’t miss it (even on a PDF), and also gives you a way to showcase additional publication information e.g. funding, license and peer review details.

---

Want to find out more? Check out our [webinars](/webinars), [blog](/blog) or come and speak to us in person at an [event](/events) - we’re eager to hear from you!
