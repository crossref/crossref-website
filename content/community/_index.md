+++
title = "Get involved"
date = "2023-08-05"
draft = false
author = "Kornelia Korzec"
image = "/images/banner-images/village.jpg"
maskcolor = "crossref-darkgrey"
rank = 4
toc = true
aliases = [
    "/01company/06publishers.html",
    "/01company/17crossref_members.html"
]
[menu.main]
weight = 1
+++

It takes a village. Crossref only works as long as the scholarly research community wants to work together globally, across all disciplines, for integrity and openness of the scientific record. Our community includes tens of thousands of organisations and systems in over 160 countries. Over 20,000 organisations create identifiers for metadata records that describe and locate their research. They share them through Crossref so that they don't have to duplicate the information for the many thousands who consume and use it downstream throughout the research ecosystem.

Crossref was founded in 2000 by some established scientific societies and publishers. Now, our membership comprises just 35% publishers or societies, with the largest membership group (40%) self-identifying as research institutions and universities. The other 25% is made up of funders (who started joining to record grants, use of facilities, and other support) alongside hundreds of museums, government organisations, libraries, data and subject repositories, conference providers, standards bodies, individual scholars, and news outlets. Our members register any research object that might be part of the scholarly evidence trail - from grants, articles, books, preprints, and reports to data, software, video, and physical objects.

Crossref is the world's largest registry of Digital Object Identifiers (DOIs) and metadata for the scholarly research community. Unlike other DOI agencies, we encompass all research stakeholders and all geographies. We facilitate an average of 1.1 billion DOI resolutions (clicks of a DOI link) every month, which is 95% of all DOI activity. And our APIs see over 1 billion queries of our metadata every month.

> This global network effect means your work is more likely to be found, if it's in Crossref.

Everyone is welcome, and there are lots of opportunities to participate and help prioritise our [strategic agenda and roadmap](/strategy), to get together, to learn or advise, or to more formally contribute to open science communication through co-creating and using our [Research 'Nexus'](/documentation/research-nexus) of metadata and relationships, backed by our strong commitment to the [POSI](/categories/posi/) principles for broad governance, transparent and forkable operations, and financial [sustainability](/operations-and-sustainability/).

So get involved with Crossref to...

## {{< icon style="far" name="fa-link" color="crossref-yellow" size="medium" >}} Identify and describe any research object

You can maintain and enrich the scholarly record in perpetuity to contribute evidence and make your work discoverable globally. If you publish scholarly or professional materials, content, or any kind of research object, you are eligible to [become a member](/membership) of Crossref. Membership allows you to [create DOI records that persistently identify and describe your work](/services/content-registration).

Always use the DOI links wherever you communicate about your work, following the display guidelines available at [https://doi.org/10.13003/5jchdy](https://doi.org/10.13003/5jchdy), so that the community can refer to and build upon your work. You agree to certain [obligations](/membership/terms) that help safeguard the system for everyone, including future generations.

There are [fees](/fees) attached as part of our not-for-profit sustainability model, and we also have a number of ways to reduce barriers, such as the [Global Equitable Membership (GEM)](/gem) program, or joining via a [Sponsor](/community/sponsors) organisation. If you're just starting out in your journey, visit the [Publishers Learning And Community Exchange (The PLACE)](https://theplace.discourse.group/) for more general orientation including information about ethical publishing and open access, which is broader than just Crossref.

## {{< icon style="far" name="fa-plus" color="crossref-red" size="medium" >}} Add more metadata to make a difference

You can curate and steward the record and [include metadata](/documentation/principles-practices/) and [assert relationships](/documentation/principles-practices/best-practices/relationships/) between other objects so your work can be found, cited, linked, assessed, and reused by the whole community. You can add context and match research to [funding](/documentation/principles-practices/best-practices/funding/), [affiliations](/documentation/schema-library/markup-guide-metadata-segments/affiliations/), [translations](/documentation/principles-practices/best-practices/multi-language/), [references](/documentation/principles-practices/best-practices/references/), [contributors](/documentation/schema-library/markup-guide-metadata-segments/contributors/), and more.

Crucially, you can also [put updates, corrections, and retractions on the record](/services/crossmark) so these notices accompany the work to better inform users and readers in the future.

All of this added context helps co-create the 'Research Nexus', which is the vision of a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society

## {{< icon style="far" name="fa-exchange" color="crossref-blue" size="medium" >}} Analyse metadata to inform and understand research

Crossref is the [sustainable source of community-owned scholarly metadata](https://doi.org/10.1162/qss_a_00022) and is relied upon by thousands of systems across the research ecosystem and the globe.

Take a look at our metadata and search for anything at [search.crossref.org](https://search.crossref.org/) and specifically by funder at [search.crossref.org/funding](https://search.crossref.org/funding). Or explore [how to use our API with these sample queries](/documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-api/). 

People using Crossref metadata need it for all sorts of reasons including metaresearch (researchers studying research itself such as through bibliometric analyses), publishing trends (such as finding works from an individual author or reviewer), or incorporation into specific databases (such as for discovery and search or in subject-specific repositories), and [many more detailed use cases](/services/metadata-retrieval/user-stories/).

Anyone can retrieve and use over 150 million records without restriction. So there are no fees to use the metadata but if you really rely on it then you might like to sign up for [Metadata Plus](/services/metadata-retrieval/metadata-plus) which offers greater predictability and higher rate limits.

Additionally, we facilitate [screening of text-based research for originality](/services/similarity-check) through collective licensing terms with leading software tools such as iThenticate from Turnitin.

## {{< icon style="far" name="fa-seedling" color="crossref-green" size="medium" >}} Co-create solutions to shared problems

Crossref co-developed the initial system for what is now known as [ORCID](https://www.orcid.org), we co-founded and help operate the [ROR Registry](https://www.ror.org), and we run the [Scholix endpoint](https://documentation.ardc.edu.au/cpg/scholix) for data citation.

We are active on dozens of boards and working groups, such as [JATS](https://jats.nlm.nih.gov/), [Open Research Funders Group](https://orfg.org), [African Journals Online (AJOL)](https://www.ajol.info), [software citation](https://force11.org/groups/software-citation-implementation-working-group/), and [OA Switchboard](https://www.oaswitchboard.org/) - to list just a few. We collaborate with others on several community initiatives, such as co-founding [Metadata 20/20](https://www.metadata2020.org) and we support other campaigns for richer open metadata, such as [I4OC](https://i4oc.org) and [I4OA](https://i4oa.org).

We have especially close integrations and partnerships with other [POSI adopters](https://openscholarlyinfrastructure.org/posse/), such as [DOAJ](https://doaj.org/), the [Public Knowledge Project](https://pkp.sfu.ca/), [Europe PMC](https://europepmc.org/), and [DataCite](https://datacite.org/).

We’re keen for volunteers to join our [advisory and working groups](/working-groups) which help to support and give input on our different programs and initiatives. If you want to help test new metadata schema, suggest or join a working group, or have thoughts about what Crossref should prioritise, please get in touch to make a suggestion. We have a large [agenda and roadmap](/strategy) but we're always open to ideas.

## {{< icon style="far" name="fa-comments" color="crossref-orange" size="medium" >}} Join the discussions

We host [webinars and in-person events](/events) which are all open and free to attend. Through these we aim to inform and update people with what we’re working on, but we also want your feedback - what’s happening with you and what do you need from us? What shall we collaborate on next? Who in the community would you like to hear more from, and what about?

With an eye towards the future of our planet with a promise to [reduce travel](/blog/rethinking-staff-travel-meetings-and-events/), we're doing more events online than in-person so please come and chat with us over on [Mastodon](https://mastodon.online/@crossref), interact with the whole community and ask for support on our [forum](https://community.crossref.org/), and read and comment on [our blog](/blog) (or [volunteer a guest post](mailto:feedback@crossref.org)).

If you're passionate about open research infrastructure and your community doesn't feel represented in Crossref, please [tell us](mailto:feedback@crossref.org). Consider applying to become an [Ambassador](/community/ambassadors/). You'd be joining [many others](/community/our-ambassadors/) around the world in becoming a Crossref expert and helping us understand how to support your community better.

You can browse and contribute to our open code on [GitLab](https://gitlab.com/crossref) and in fact you can even update this website by [opening issues or merge requests in the public repository](https://gitlab.com/crossref/crossref-website).

Our [team](/people) is looking forward to hearing from you; thanks for getting involved.
