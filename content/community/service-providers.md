+++
title = "Service providers"
date = "2021-05-04"
author = "Amanda Bartell"
draft = false
[menu.main]
parent = "Get involved"
rank = 2
weight = 6
+++


Crossref Service Providers are a small group of organizations that work collaboratively with us to help support [member obligations](https://www.crossref.org/education/metadata-stewardship/understanding-your-member-obligations/) and best practices to the benefit of the wider scholarly community.

## Service Providers are defined as
Third party organizations such as hosting platforms, manuscript submission systems, XML/metadata providers and general publisher services organizations that are selected by Crossref members to retrieve Cited-by matches, create, register, and/or display metadata on their behalf.

## The Service Providers/Crossref collaboration
Communication is the focus of the Service Provider program. We work together to support the availability of new services and updates for members, identify trends, encourage best practices for everyone involved and prevent and minimize problems in the metadata supply chain.

## Service Providers commit to:
+ Support best practice metadata represented in [Participation Reports](https://www.crossref.org/members/prep/)
+ Sharing their relevant workflows with Crossref
+ Make reasonable efforts to accommodate changes to Crossref schema and services
+ Provide Crossref with client services information, including metadata delivery options
+ Staying up to date with Crossref services and policies, including participation in ongoing meetings and communications
+ Providing feedback to Crossref on our services  

## Crossref commits to:
+ Providing advance communication of changes and new services requiring Service Provider awareness and development
+ Provide Service Providers with member services information
+ Helping Service Providers stay up to date with Crossref services and policies, including providing regular meeting opportunities
+ Recognizing registered Service Providers by keeping this page up to date
+ Developing Service Providers participation reports

## Current Service Providers



{{% accordion %}}
{{% accordion-section "Allen Press" %}}  

Allen Press aims to inspire your audience with an innovative blend of printing, marketing, publishing and distribution services crafted with the latest technology. We also serve a unique role in assisting the STEM community share ideas that improve the world. Combined with our state-of-the-art equipment and a team of seasoned pros, we provide rapid production and unmatched quality at competitive prices. Our commitment to excellence and attention to detail are more than just goals—they define how we help our customers.

{{< figure src="/images/service-providers/allenpress-logo.png" alt="Allen Press logo" width="20%" >}}

Website: [https://www.allenpress.com/](https://www.allenpress.com/)

{{% /accordion-section %}}
{{% accordion-section "Aptara Inc." %}}  

As the STM publishing landscape evolves, so do Aptara’s service offerings. Our OA and publishing fee collection service, SciPris, continues to grow in clientele and functionality, keeping pace with evolving publishing agreement types. With PE/PM, copyediting, typesetting, and conversion services still at our core, the expansion of our peer review management, accessibility compliance/tagging, AI/ML, AR/VR, data mining, and content enrichment service lines allow publishers to source end-to-end workflows with Aptara. See our revamped web site for more.

{{< figure src="/images/service-providers/aptaracorp-logo.png" alt="Aptara Inc. logo" width="20%" >}}

Website: [https://www.aptaracorp.com/](https://www.aptaracorp.com/)

{{% /accordion-section %}}
{{% accordion-section "Aries Systems" %}}  

Aries Systems transforms the way scholarly publishers bring high-value content to the world. Our innovative workflow solutions manage the complexities of modern print and electronic publishing—from submission to editorial management and peer review, to production tracking and publishing channel distribution. As the publishing environment evolves, Aries Systems is committed to delivering solutions that help publishers and scholars enhance the discovery and dissemination of human knowledge on a global scale. Publish faster, publish smarter, with Aries Systems.

{{< figure src="/images/service-providers/aries-logo.png" alt="Aries Systems logo" width="20%" >}}

Website: [https://www.ariessys.com/](https://www.ariessys.com/)

{{% /accordion-section %}}
{{% accordion-section "Atypon" %}}  

Atypon was founded in 1996, driven by the desire to democratize scientific research by expanding its availability, which meant giving scholarly publishers the software that they needed to excel in a new—and often intimidating—digital environment.

Atypon’s initial development efforts resulted in Literatum, our online publishing and website development platform, first released in 1999. What was a five-person technology startup in Silicon Valley, is now an influential global organization, with a team of more than 480 in nine offices across the United States, the UK, Jordan, the Czech Republic, and Georgios’s native Greece.

{{< figure src="/images/service-providers/Atypon-logo.png" alt="Atypon logo" width="20%" >}}

Website: [https://www.atypon.com/](https://www.atypon.com/)

{{% /accordion-section %}}
{{% accordion-section "Cadmore Media Inc." %}}    

Our vision for founding Cadmore Media was born out of a broad ambition to transform the dissemination of video and audio content in the scholarly and professional world through expert technology. In addition to creating our own products, we aim to facilitate industry-wide innovation by leading industry efforts to shape and promote best practices and standards, thus benefiting any organization or service whose purpose is to publish or structure research and professional information.

{{< figure src="/images/service-providers/cadmore-logo.jpg" alt="Cadmore Media logo" width="20%" >}}

Website: [https://cadmore.media/](https://cadmore.media/)   

{{% /accordion-section %}}
{{% accordion-section "eJournalPress" %}}  

 eJournalPress is focused on providing web-based technology solutions for the scholarly publishing community. The company was initially founded as a software consulting service assisting companies in designing, programming, and deploying software and mission critical systems. In 1999 eJournalPress utilized this skill set to work with journals and publishers creating a new generation of web-based software tools to support manuscript submission, tracking, and peer review. The result of this engineering effort is EJPress.

 {{< figure src="/images/service-providers/ejp_logo.png" alt="eJournalPress logo" width="20%" >}}

Website: [https://www.ejournalpress.com/](https://www.ejournalpress.com/)

{{% /accordion-section %}}
{{% accordion-section "Ex Libris Ltd." %}}

At Ex Libris, we believe in the value of education and research. Our mission is to allow academic institutions to create, manage, and share knowledge. With better tools, our customers achieve their goals and further academic initiatives.

 {{< figure src="/images/service-providers/exlibris-logo.png" alt="Ex Libris logo" width="20%" >}}

Website:[https://exlibrisgroup.com/](https://exlibrisgroup.com/)   

{{% /accordion-section %}}
{{% accordion-section "Ingenta" %}}

Ingenta was formed in 1998 and floated on the AIM market of the London Stock Exchange in April 2000. After a number of smaller acquisitions, the Group expanded through a merger with Vista.

Ingenta headquarters are in Oxford, UK with another office in New Jersey, USA. With industry experience going back nearly 40 years and more than 150 employees, Ingenta serves over 400 trade and scholarly publishers.

{{< figure src="/images/service-providers/Ingenta-logo.svg" alt="Ingenta logo" width="20%" >}}

Website: [https://www.ingenta.com/](https://www.ingenta.com/)

{{% /accordion-section %}}
{{% accordion-section "JSTOR" %}}

JSTOR is a digital library for the intellectually curious. We help everyone discover, share, and connect valuable ideas.

{{< figure src="/images/service-providers/jstor-logo.png" alt="Ingenta logo" width="10%" >}}

Website: [https://www.jstor.org/](https://www.jstor.org/)

{{% /accordion-section %}}
{{% accordion-section "Knowledge Works Global Ltd." %}}

KnowledgeWorks Global Ltd. (KGL) combines the content and technology expertise of Cenveo Publisher Services and Cenveo Learning with Sheridan Journal Services and Sheridan PubFactory under the umbrella of the CJK Group.

{{< figure src="/images/service-providers/KGL_logo.jpg" alt="Knowledge Works Global Ltd. logo" width="20%" >}}

Website: [https://www.kwglobal.com/](https://www.kwglobal.com/)

{{% /accordion-section %}}
{{% accordion-section "MPS Limited" %}}  

MPS, a leading global provider of platforms, and content, and learning solutions for the digital world, was established as an Indian subsidiary of Macmillan (Holdings) Limited in 1970. The long service history as a captive business allowed MPS to build unique capabilities and talents through strategic partner programs. MPS is now a global partner to the world’s leading enterprises, learning companies, publishers, libraries, and content aggregators.

{{< figure src="/images/service-providers/mps-logo.png" alt="MPS Ltd. logo" width="30%" >}}

Website: [https://www.mpslimited.com/](https://www.mpslimited.com/)

{{% /accordion-section %}}
{{% accordion-section "Nova Techset Ltd." %}}

Nova Techset is a leading supplier of prepress services to the STM and academic publishing world. We provide pre-editing, copyediting, composition and ePub solutions as well as the full range of project management services for books and journals. With delivery centers in Bengaluru and Chennai, and offices in the UK and the US, we employ a staff of over 800 skilled and experienced personnel and produce over 1,000,000 book and journal pages a year.

{{< figure src="/images/service-providers/nova-techset-logo.png" alt="Nova Techset logo" width="15%" >}}

Website: [https://www.novatechset.com/](https://www.novatechset.com/)  

{{% /accordion-section %}}
{{% accordion-section "PubPub" %}}  

PubPub gives research communities of all stripes and sizes a simple, affordable, and nonprofit alternative to existing publishing models and tools.

{{< figure src="/images/service-providers/pubpub-logo.svg" alt="PubPub logo" width="20%" >}}

Website: [https://www.pubpub.org/](https://www.pubpub.org/)    

{{% /accordion-section %}}
{{% accordion-section "Scholastica" %}}

Scholastica was founded in 2012 in response to a growing need in academia for an easier, more modern way to peer review research articles and publish high-quality open access journals online.

{{< figure src="/images/service-providers/scholastica-logo.png" alt="Scholastica logo" width="20%" >}}

Website: [https://scholasticahq.com/](https://scholasticahq.com/)  

{{% /accordion-section %}}
{{% accordion-section "Sheridan" %}}  

At Sheridan, we have deep roots in the book, journal, magazine, and catalog markets we serve, and our seasoned experts are not only knowledgeable about the conventions of these industries, but also their exciting new frontiers.  As part of The CJK Group, under the brand of Sheridan, we’re organized by five locations, each providing specific services to our key markets.

{{< figure src="/images/service-providers/Sheridan-logo.jpg" alt="Sheridan logo" width="20%" >}}

Website: [https://www.sheridan.com/](https://www.sheridan.com/)

{{% /accordion-section %}}
{{% accordion-section "Silverchair" %}}

Silverchair believes that innovation can fulfill the greatest promises of scholarship. As the leading independent platform partner for scholarly and professional publishers, we serve our growing community through flexible technology and unparalleled services. We build and host websites, online products, and digital libraries for our clients’ content, enabling researchers and professionals to maximize their contributions to our world. Our vision is to help publishers thrive, evolve, and fulfill their missions.

{{< figure src="/images/service-providers/silverchair-logo.jpg" alt="Silverchair logo" width="20%" >}}

Website: [https://www.silverchair.com/](https://www.silverchair.com/)   

{{% /accordion-section %}}
{{% accordion-section "Walter de Gruyter GmbH" %}}

De Gruyter publishes first-class scholarship and has done so for more than 270 years. An international, independent publisher headquartered in Berlin - and with further offices in Boston, Beijing, Basel, Vienna, Warsaw and Munich - it publishes over 1,300 new book titles each year and more than 900 journals in the humanities, social sciences, medicine, mathematics, engineering, computer sciences, natural sciences, and law. The publishing house also offers a wide range of digital media, including open access journals and books.

{{< figure src="/images/service-providers/dg-logo.svg" alt="Walter de Gruyter logo" width="20%" >}}

Website: [https://www.degruyter.com/](https://www.degruyter.com/)

---
{{% /accordion-section %}}
{{% /accordion %}}

You can find more information on working with a service provider [here](/education/member-setup/working-with-a-service-provider/) or you may contact [our community team](mailto:feedback@crossref.org).
