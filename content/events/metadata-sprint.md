+++
title = "Crossref Metadata Sprint 2025"
date = "2024-09-25"
draft = false 
author = "Crossref"
image = "/images/banner-images/api-sprint-landing-page-header-grey.png"
maskcolor = "crossref-darkblue"
aliases = [
    "/events/api-sprint/",
    "/events/api-sprint"
]
[menu.main]
rank = 8
parent = "Webinars and events"
weight = 40
+++

## Overview

Crossref makes research objects easy to find, cite, link, assess, and reuse. We exist to make scholarly communications better. We emphasise the community's role in contributing to this goal and next Spring, we’re opening a new option to collaborate and co-create new research, tools and initiatives – at our first Metadata Sprint. Read on to learn what we’re hoping to achieve and how you can take part.

Studying and using the ever-growing magnitude of the scholarly record requires collaboration and joint thinking. In the case of Crossref’s metadata, we are referring to a body of over 162 million records provided by 21,000 members across the globe. This is actively used, on one hand, to power all sorts of scholarly tools, such as [reference managers](https://www.zotero.org/), [open catalogues](https://openalex.org/), [information dashboards](https://retraction-dashboard.netlify.app/) and many more, but also as the raw data for [meta-research](https://www.crossref.org/blog/dont-take-it-from-us-funder-metadata-matters/).

We offer open access to this metadata via our REST API, which supports a wide range of queries, facets and filters. The REST API can be used, for example, to look up the metadata for specific records, search for works mentioning an author’s name or find retractions registered with us. It also allows users to filter on several elements, including funder IDs, ORCIDs, dates and more. Despite API being commonly defined as interfaces for machines, the number of people engaging directly with APIs is [accelerating and diversifying today](https://www.postman.com/state-of-api/api-global-growth/#api-global-growth). Additionally, we also provide [annual public data files](https://www.crossref.org/blog/2024-public-data-file-now-available-featuring-new-experimental-formats/), which you can download via BitTorrent protocol and get your hands on the ~212 GB file containing all of Crossref metadata. 

Our upcoming 25th anniversary lends itself as an opportunity for doing something new and increasing support for community-led initiatives. We want to bring together community members to engage with our REST API and the scholarly metadata therein, and foster innovation and creativity, solve real-world problems, and promote networking and collaboration. Some examples of projects that we have co-created with the community include the [JSON Forms release that supported Vuetify](https://www.crossref.org/blog/forming-new-relationships-contributing-to-open-source/), the Vue.js user interface library we use and, more recently, the [update to our Participation Reports](https://www.crossref.org/blog/re-introducing-participation-reports-to-encourage-best-practices-in-open-metadata/). We welcome librarians, research integrity experts, scientometricians, meta-scientists, data scientists, coders, engineers, and other open scholarly infrastructure enthusiasts. We invite the participants to suggest and join projects within the following themes:

- **Integrity of the scholarly record** - what signals and patterns of lack of integrity can be detected using scholarly metadata?
- **Quality and completeness** - tools and strategies to assess and enhance the quality and completeness of the scholarly metadata.
- **Metadata for everyone** - making interacting with our REST API easier and more accessible.
- **Metadata by everyone** - tools to help the community contribute to the scholarly record in various ways.
- **Outside the box** - projects that don’t fit any of the above themes.

You can pitch your project or support one proposed by others in the community. This event is not limited to technical-oriented participants. If you are interested in open infrastructure and don’t have a technical or code-oriented background, we encourage you to register your interest too.

The Metadata Sprint will take place in the [National Museum of Natural Sciences](https://www.mncn.csic.es/es) of Madrid, Spain, on the 8 and 9 of April 2025. There is a limited number of spaces – we hope to welcome 30 participants at the event. We will encourage accepted participants to involve others in the community to collaborate ahead of time via our Community Forum, crowdsourcing ideas and comments on their initiative to support productivity on the day. 

If you need a refresher on our API and what you can access through it, you can visit the [REST API section of our documentation](https://www.crossref.org/documentation/retrieve-metadata/rest-api/). We also encourage you to explore the [API documentation](https://api.crossref.org/) and the latest news about our [public data file](https://www.crossref.org/blog/2024-public-data-file-now-available-featuring-new-experimental-formats/).

We're committed to inclusivity and diversity at our Metadata Sprint and will look to offer assistance for accepted applicants for whom travel costs might be a barrier to participation.

Finally, we encourage you to read [our Code of Conduct](https://www.crossref.org/code-of-conduct/), which also applies to this event. 

We are looking forward to meeting you in Madrid! 

---

## Registration and submission form

<!-- **Submissions after 10 January, 2025 will not be accepted.** --> 


<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSe4yIroyt3EATt4VaMK11ZCkw392yCn5dqp3lnrH-yyxuHgwA/viewform?embedded=true" width="100%" height="200" frameborder="0" marginheight="0" marginwidth="0">Your browser does not support iframes.</iframe>
<br><br>



[Direct access to the registration and submission form.](https://docs.google.com/forms/d/e/1FAIpQLSe4yIroyt3EATt4VaMK11ZCkw392yCn5dqp3lnrH-yyxuHgwA/viewform)


{{% accordion %}}
{{% accordion-section "Program" %}}

Agenda coming soon...watch this space. 

{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "FAQS" %}}

**What should I bring?**

Bring your laptop with you. The venue will have wifi access and plenty of power sockets. If you are visiting us from outside the EU, make sure you pack your power adapters.  

**Is there a fee to participate?**

Registration is free, but we do have limited seats.  

**Can I join if I don’t have a team?**

Absolutely. We will have slack channels and host group calls to let the participants start interacting with each other in advance. Ultimately, you can always choose to develop your project on your own.   

**Can I propose a different theme or topics?**

Sure! Be sure to include your proposal in the registration form.  

**What are the entry requirements for Spain?**

You can check if you need an entry Visa  [in this link](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32018R1806#d1e32-51-1)  

**Do you have any hotel suggestions?**

Here’s a selection of hotels near the [Museo Nacional de Ciencias Naturales](https://www.mncn.csic.es/es), with options for every budget. All hotels are within walking distance or a short transport ride, making it convenient for your visit. See the map below to see the proximity of the hotels to the venue.

[Map view](https://www.google.com/maps/dir/Museo+Nacional+de+Ciencias+Naturales,+C.+de+Jos%C3%A9+Guti%C3%A9rrez+Abascal,+2,+Chamart%C3%ADn,+28006+Madrid,+Spain/Hotel+NH+Madrid+Chamber%C3%AD,+Calle+de+Bret%C3%B3n+de+los+Herreros,+Madrid,+Spain/Hotel+Suites+Barrio+De+Salamanca,+Calle+del+General+Or%C3%A1a,+Madrid,+Spain/Hotel+NH+Madrid+Zurbano,+Calle+de+Zurbano,+Madrid,+Spain/Barcel%C3%B3+Emperatriz,+Calle+de+L%C3%B3pez+de+Hoyos,+Madrid,+Spain/@40.4384011,-3.7015462,15z/data=!3m2!4b1!5s0xd4228eefdac1ae3:0x639f47bf9a33d635!4m32!4m31!1m5!1m1!1s0xd4228eef8a087c5:0x141d274e9dbb7a8e!2m2!1d-3.6901921!2d40.4412381!1m5!1m1!1s0xd4228f6b70fcec1:0xa413eda0342bc1c8!2m2!1d-3.69827!2d40.439586!1m5!1m1!1s0xd4228eb07f30e4b:0xdfce28be65770ec9!2m2!1d-3.6857229!2d40.4363629!1m5!1m1!1s0xd4228f1ffb40517:0xfaa3c9101eb4e0b5!2m2!1d-3.692878!2d40.439051!1m5!1m1!1s0xd4228ec628bcf19:0xca052388d2889590!2m2!1d-3.6885047!2d40.4363686!3e0?entry=ttu&g_ep=EgoyMDI0MTExMi4wIKXMDSoASAFQAw%3D%3D) of all locations shared below.


- [NH Collection Madrid Abascal](https://www.booking.com/Share-ZSnq0M)
  - *Distance*: approx. 15-minute walk  
  - *Price*: €260+ a night  
  - [Google Maps](https://www.google.com/maps/dir/Museo+Nacional+de+Ciencias+Naturales,+C.+de+Jos%C3%A9+Guti%C3%A9rrez+Abascal,+2,+Chamart%C3%ADn,+28006+Madrid,+Spain/Hotel+NH+Collection+Madrid+Abascal,+Calle+de+Jos%C3%A9+Abascal,+47,+Chamber%C3%AD,+28003+Madrid,+Spain/@40.4400649,-3.6972441,17z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0xd4228eef8a087c5:0x141d274e9dbb7a8e!2m2!1d-3.6901921!2d40.4412381!1m5!1m1!1s0xd4228f22b01bd89:0x7f382b9151270982!2m2!1d-3.695182!2d40.438314!3e0?entry=ttu&g_ep=EgoyMDI1MDEyNy4wIKXMDSoASAFQAw%3D%3D)

- [NH Madrid Chamberí](https://www.booking.com/Share-tygdLDL)
  - *Distance*: approx. 15-minute walk  
  - *Price*: €210+ a night  
  - [Google Maps](https://www.google.com/maps/dir/Museo+Nacional+de+Ciencias+Naturales,+C.+de+Jos%C3%A9+Guti%C3%A9rrez+Abascal,+2,+Chamart%C3%ADn,+28006+Madrid,+Spain/Hotel+NH+Madrid+Chamber%C3%AD,+Calle+de+Bret%C3%B3n+de+los+Herreros,+29,+Chamber%C3%AD,+28003+Madrid,+Spain/@40.4405085,-3.6965649,17z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0xd4228eef8a087c5:0x141d274e9dbb7a8e!2m2!1d-3.6901921!2d40.4412381!1m5!1m1!1s0xd4228f6b70fcec1:0xa413eda0342bc1c8!2m2!1d-3.69827!2d40.439586!3e2?entry=ttu&g_ep=EgoyMDI1MDEyNy4wIKXMDSoASAFQAw%3D%3D)


- [Exclusive Rooms in the Heart of the City, Madrid, Spain](https://www.booking.com/hotel/es/hosteria-balmoral-madrid.html?aid=304142&checkin=2025-04-07&checkout=2025-04-10&group_adults=1&group_children=0&label=gen173nr-1FCAEoggI46AdIM1gEaEaIAQGYATG4AQfIAQ_YAQHoAQH4AQKIAgGoAgO4AuvDorwGwAIB0gIkMGFkMTU0YmUtMDZjNC00Nzc2LTgzODUtYmI2NzU1NzY1OWQ52AIF4AIB-Share-wx05L3W%401737450111&no_rooms=1&req_adults=1&req_children=0)
  - *Distance*: approx. 15-minute walk  
  - *Price*: €110+ a night 
  - [Google Maps](https://www.google.com/maps/dir/Museo+Nacional+de+Ciencias+Naturales,+C.+de+Jos%C3%A9+Guti%C3%A9rrez+Abascal,+2,+Chamart%C3%ADn,+28006+Madrid,+Spain/Hostal+en+Chamberi,+C.+del+Robledillo,+6,+bajo,+Chamber%C3%AD,+28003+Madrid,+Spain/@40.44221,-3.6965008,17z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0xd4228eef8a087c5:0x141d274e9dbb7a8e!2m2!1d-3.6901921!2d40.4412381!1m5!1m1!1s0xd422949d5be12fb:0x18e33acc989beada!2m2!1d-3.6981714!2d40.4434408!3e2?entry=ttu&g_ep=EgoyMDI1MDEyNy4wIKXMDSoASAFQAw%3D%3D)


- [Hotel NH Madrid Chamberí](https://www.nh-hotels.com/en/hotel/nh-madrid-chamberi?utm_campaign=local-gmb&utm_medium=organic_search&utm_source=google_gmb)   
  - *Distance*: approx. 15-minute walk  
  - *Price*: €200-€250 a night  
  - [Google Maps](https://www.google.com/maps/dir/Hotel+NH+Madrid+Chamber%C3%AD,+Calle+de+Bret%C3%B3n+de+los+Herreros,+29,+Chamber%C3%AD,+28003+Madrid,+Spain/C.+de+Jos%C3%A9+Guti%C3%A9rrez+Abascal,+2,+Chamart%C3%ADn,+28006+Madrid,+Spain/@40.4405358,-3.6965598,17z/data=!3m2!4b1!5s0xd4228eefdac1ae3:0x639f47bf9a33d635!4m13!4m12!1m5!1m1!1s0xd4228f6b70fcec1:0xa413eda0342bc1c8!2m2!1d-3.69827!2d40.439586!1m5!1m1!1s0xd4228eef8a087c5:0x141d274e9dbb7a8e!2m2!1d-3.6901921!2d40.4412381?entry=ttu&g_ep=EgoyMDI0MTExMC4wIKXMDSoASAFQAw%3D%3D)

- [Hotel Suites Barrio De Salamanca](https://www.hotelbarriodesalamanca.es/en/)  
  - *Distance*: approx. 15-minute walk  
  - *Price*: €250-€300 a night  
  - [Google Maps](https://www.google.com/maps/dir/Hotel+Suites+Barrio+De+Salamanca,+C.+del+Gral.+Or%C3%A1a,+17,+Salamanca,+28006+Madrid,+Spain/C.+de+Jos%C3%A9+Guti%C3%A9rrez+Abascal,+2,+Chamart%C3%ADn,+28006+Madrid,+Spain/@40.4383992,-3.6936714,16z/data=!3m2!4b1!5s0xd4228eefdac1ae3:0x639f47bf9a33d635!4m13!4m12!1m5!1m1!1s0xd4228eb07f30e4b:0xdfce28be65770ec9!2m2!1d-3.6857229!2d40.4363629!1m5!1m1!1s0xd4228eef8a087c5:0x141d274e9dbb7a8e!2m2!1d-3.6901921!2d40.4412381?entry=ttu&g_ep=EgoyMDI0MTExMC4wIKXMDSoASAFQAw%3D%3D)

- [Hotel NH Madrid Zurbano](https://www.nh-hotels.com/en/hotel/nh-madrid-zurbano?utm_campaign=local-gmb&utm_medium=organic_search&utm_source=google_gmb)  
  - *Distance*: approx. 9-minute walk  
  - *Price*: €100-€150 a night  
  - [Google Maps](https://www.google.com/maps/dir/Hotel+NH+Madrid+Zurbano,+Calle+de+Zurbano,+79-81,+Chamber%C3%AD,+28003+Madrid,+Spain/C.+de+Jos%C3%A9+Guti%C3%A9rrez+Abascal,+2,+Chamart%C3%ADn,+28006+Madrid,+Spain/@40.4403155,-3.6925615,18z/data=!3m2!4b1!5s0xd4228eefdac1ae3:0x639f47bf9a33d635!4m13!4m12!1m5!1m1!1s0xd4228f1ffb40517:0xfaa3c9101eb4e0b5!2m2!1d-3.692878!2d40.439051!1m5!1m1!1s0xd4228eef8a087c5:0x141d274e9dbb7a8e!2m2!1d-3.6901921!2d40.4412381?entry=ttu&g_ep=EgoyMDI0MTExMC4wIKXMDSoASAFQAw%3D%3D)


- [Barceló Emperatriz](https://www.barcelo.com/es-es/barcelo-emperatriz/?utm_source=google&utm_medium=organic&utm_campaign=my_business&utm_content=7329)  
  - *Distance*: approx. 11-minute walk  
  - *Price*: €200-€250 a night  
  - [Google Maps](https://www.google.com/maps/dir/Barcel%C3%B3+Emperatriz,+C.+de+L%C3%B3pez+de+Hoyos,+4,+Salamanca,+28006+Madrid,+Spain/C.+de+Jos%C3%A9+Guti%C3%A9rrez+Abascal,+2,+Chamart%C3%ADn,+28006+Madrid,+Spain/@40.4385119,-3.695054,16z/data=!3m2!4b1!5s0xd4228eefdac1ae3:0x639f47bf9a33d635!4m13!4m12!1m5!1m1!1s0xd4228ec628bcf19:0xca052388d2889590!2m2!1d-3.6885047!2d40.4363686!1m5!1m1!1s0xd4228eef8a087c5:0x141d274e9dbb7a8e!2m2!1d-3.6901921!2d40.4412381?entry=ttu&g_ep=EgoyMDI0MTExMC4wIKXMDSoASAFQAw%3D%3D)


**How do I get to the venue?**

We encourage participants to consider the environment and travel by land wherever practicable. However, if you need to fly, [Adolfo Suárez Madrid–Barajas Airport (MAD)](https://maps.app.goo.gl/egHUyKoCV4JqLe4r7) is the nearest. By train: [Getting to Madrid by Train](https://www.esmadrid.com/en/madrid-by-train).  
     
The venue is close to the [*Nuevos Ministerios* metro and train station](https://maps.app.goo.gl/ssAXSrg8mPudUzgq8), and it can also be reached by those arriving to the [*Gregorio Marañón* metro station](https://maps.app.goo.gl/QYKRyxHmwjTJh4eLA).  Both are at less than 10-minute walk.  

{{% /accordion-section %}}
{{% /accordion %}}



## Contact us

If you have any questions, contact [events@crossref.org](mailto:events@crossref.org).  
Social Media Links: [LinkedIn](https://www.linkedin.com/company/crossref/), [Mastodon](https://mastodon.online/@crossref), [Twitter](https://x.com/CrossrefOrg), [Instagram](https://www.instagram.com/crossreforg/) 