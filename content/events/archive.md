+++
title = "Crossref events archive"
date = "2021-01-11"
draft = true
author = "Rosa Morais Clark"
weight = 20
rank = 2
+++

An archive of all community events which Crossref people have participated in years past such as conferences, workshops, hack days, and seminars.  

{{% accordion %}}

{{% accordion-section "2022 events" %}}

[Munin Conference Panel on Open Identifiers](https://site.uit.no/muninconf/program-2/)  
Better Together: Complete Metadata as Robust Infrastructure (APAC region) - [Recording](https://youtu.be/vS808JSSCjA) -  November 28   
[Feria Internacional del Libro de Guadalajara (
Guadalajara International Book Fair)](https://www.fil.com.mx/) - November 28-30   
[ISMTE 2022 Global Event](https://www.ismte.org/events/EventDetails.aspx?id=1630533&group=) - November 1  
[2022 Charleston Conference](https://www.charleston-hub.com/the-charleston-conference/) - November 2 - 5   
[2022 SSP Regional Meetup](https://customer.sspnet.org/ssp/EventDisplayNPGF.aspx?WebsiteKey=d1424957-06c0-458c-aa62-316197b4295d&hkey=b98b9701-0def-45d5-8f29-d85f8a3fd729&EventKey=REUK221027) - October 27 - Oxford, UK   
[Frankfurt Book Fair 2022, Stand M5, Hall 4.2](https://www.buchmesse.de/en) - October 17 - 21   
Better Together: Facilitating FAIR Research Output Sharing (APAC time zones) (with ORCID, Crossref, and DataCite) [Recording](https://vimeo.com/1029813295?share=copy#t=0)    
[ABEC (Associação Brasileira de Editores Científicos)](https://www.abecbrasil.org.br/novo/) Annual Meeting - Oct 4     
[Pubmet](https://pubmet2022.unizd.hr/programme/) - Sept 14 - 16 - Online  
[ALPSP Annual Conference and Awards 2022](https://www.alpsp.org/Conference) - September 14 - 16 - In person  
[OASPA Online Conference on Open Access Scholarly Publishing](https://oaspa.org/conference/) - September 20 - 22 - Online  
[DataCite Annual Member Meeting](https://datacite.org/member-meeting-2022.html) - September 22 - Online  
[Europe PMC AGM](https://europepmc.org/) - September 22 - London, UK  
[Plagiarism detection in the evolving publishing landscape: Best practices for journals](https://lp.scholasticahq.com/webinar-plagiarism-detection-journals/) - September 22 - Online  
[Better Together: Open new possibilities with Open Infrastructure (APAC time zones)(with ORCID, Crossref, and DataCite)](https://crossref.zoom.us/webinar/register/9016540028489/WN_b4fgKvczRueOjsg9IrFFXA) - June 27, 2022      
[Data Policy IG: Exploring features and improving standards for data availability statements](https://www.rd-alliance.org/plenaries/rda-19th-plenary-meeting-part-international-data-week-20%E2%80%9323-june-2022-seoul-south-korea-10) - June 21, 2022    
{{% logo-inline %}} [Crossref community call: the Research Nexus](https://outreach.crossref.org/acton/ct/16781/e-06ed-2206/Bct/l-tst/l-tst:1/ct1_0/1/lu?sid=TV2%3Ah33hCJvAB), [Google slides](https://docs.google.com/presentation/d/1oPGBfT_gJ-bcUOXYdRcM89iFZpzB0PTxkErsbOL0uvg/present?slide=id.g12c048515d9_2_13), [pdf slides](https://zenodo.org/record/6671346) - June 14, 2022   
[Working with Scholarly APIs: A NISO Training Series](http://www.niso.org/events/working-scholarly-apis-niso-training-series) - May 12 - Online  
[ALPSP Climate change: Practical steps to take action](https://www.alpsp.org/events-training) - March 16 - Online  
[NISO Plus 2022: Global Conversations: Global Connections](https://niso.plus/) - February 15 - 17 - Online  
[Paris Open Science European Conference](https://www.ouvrirlascience.fr/paris-open-science-european-conference-osec/) - February 4 - 6 - Online   


{{% /accordion-section %}}
{{% /accordion %}}


{{% accordion %}}

{{% accordion-section "2021 events" %}}

[Academic Publishing in Europe Nr. 16 (APE)](https://www.ape2021.eu/) - January 12 - 13 - Online  
[PIDapalooza](https://www.pidapalooza.org/) - January 28 - Online   
NAS Journal Summit - March 22, 2021  
[Establishing Open & FAIR Research Data: Initiatives and Coordination](https://www.eventbrite.nl/e/establishing-open-fair-research-data-initiatives-and-coordination-tickets-103804895236)  - March 22 - Online  
[Mozfest 2021](https://www.mozillafestival.org/en/) - March 16 - Online  
STM Research Data - March 22 - Online  
[NORF Open Research in Ireland webinar: Infrastructures for Open Research](https://norf.ie/index.php/2021/03/10/norf-open-research-in-ireland-infrastructures/) - March 30 - Online    
[UKSG](https://www.uksg.org/event/uksgconference2021) - April 12 - 14 - Online   
[EARMA 2021](https://earma.org/conferences) - April 14 - 20 - Online  
[RDA 17th Plenary Meeting](https://www.rd-alliance.org/plenaries/rda-17th-plenary-meeting-edinburgh-virtual/promoting-data-citation-adoption-scholix) - April 20 - 23 - Online  
[JATS-con 2021](https://jats.nlm.nih.gov/jats-con/upcoming.html) - April 27 – 29 - Online  
[Los Metadatos Para la Comunidad de Investigacion](http://www.revistas.ucn.cl/2021/04/19/04demayo/) - May 4 - Online - [Recording](https://youtu.be/tUeKOQoM5eE) - [Slides](https://www.slideshare.net/CrossRef/los-metadatos-para-la-comunidad-de-investigacion)  
[LIS-Bibliometrics 2021](https://thebibliomagician.wordpress.com/2021/03/25/lis-bibliometrics-conference-2021/) - May 5 - Online   
[EARMA Digital Event - Global grant identifiers: building a richer picture of research support](https://earma.wildapricot.org/event-4235140)  - May 6 - Online   
[Library Publishing Forum](https://librarypublishing.org/forum/) - May 10 - 14 - Online  
UNAM webinar: Infraestructura Académica Abierta: uso y explotación de metadatos - May 13 (Online)   
[SSP 2021 Virtual Meeting](https://customer.sspnet.org/ssp/AM21/Home) - May 24 - 27 - Online  
{{% logo-inline %}} Crossref update: The Road Ahead (Western timezones timezones) - June 8 - Online [Recording](https://youtu.be/Vj20ySUG1Ms), [Slides](https://docs.google.com/presentation/d/1dHCy_RIbFeyfjt34TU2FUtLHDsJ1LCjVnBGjv9tVJ44/present#slide=id.gd8afd7ca38_1_14)  
{{% logo-inline %}} Crossref update: The Road Ahead (Eastern timezones)  - June 9 - Online [Recording](https://youtu.be/NorhCsl9BfM), [Slides](https://docs.google.com/presentation/d/1dHCy_RIbFeyfjt34TU2FUtLHDsJ1LCjVnBGjv9tVJ44/present#slide=id.gd8afd7ca38_1_14)    
[EOSC Symposium 2021 Programme](https://www.eoscsecretariat.eu/eosc-symposium-2021-programme) - June 15 - Online   
[Japan Open Science Summit 2021](https://joss.rcos.nii.ac.jp/) - June 15 - Online   
[PKP Annual Meeting 2021](https://pkp.sfu.ca/2021/06/02/you-are-invited-pkp-2021-agm/) - June 18 - Online    
Crossref LIVE Indonesia webinar series - July 13 - 15 - Online - [Recordings](https://www.youtube.com/playlist?list=PLe_-TawAqQj0H-vrtBLT_y0BOgYqtBILm)  
[ASAPBio #feedbackASAP](https://asapbio.org/feedbackASAP) - July 21 - Online   
[The Geneva Workshop on Innovations in Scholarly Communication](https://oai.events) - September 6 - 10 - Online  
[OAI12](https://oai.events/oai12/) - September 6 - 10 - Online  
[Korean Council of Science Editors 10th anniversary conference](https://www.kcse.org/bbs/event.php?sid=121&year=2021) - September 8 - Online  
[The 25th International Conference on Science, Technology and Innovation Indicators, STI 2021](http://www.sti2020.org/?a=programme) - September 15 - Online   
[OASPA conference 2021](https://oaspa.org/conference/ ) - September 21 - Online  
[Peer Review Week 2021](https://peerreviewweek.wordpress.com/peer-review-week-2021/) - September 20 - 24 - Online  
[RORing-at-Crossref community webinar](/blog/some-rip-roring-news-for-affiliation-metadata/) - September 29 - Online  
[COPE: Seminar 2021: Reducing the inadvertent spread of retracted science: taxonomy considerations](https://publicationethics.org/resources/seminars-and-webinars/retractions-taxonomy) - September 29 - Online    

{{% /accordion-section %}}

{{% /accordion %}}

{{% accordion %}}

{{% accordion-section "2020 events" %}}

{{% logo-inline %}} [OpenCon Oxford 2020](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0055/t/page/fm/0) in in collaboration with the Bodleian Library - December 4 - Online    
{{% logo-inline %}} [Crossref LIVE20](/crossref-live-annual/) - November 10 - Online   
[Scholix Working Group: stakeholder uptake and next steps for article/data linking](https://www.rd-alliance.org/plenaries/rda-16th-plenary-meeting-costa-rica-virtual/scholix-working-group-stakeholder-uptake-and) - November 12 - Online  
[Frankfurt Book Fair 2020](/blog/crossref-at-the-frankfurt-digital-book-fair/) - October 14 - 18 - Online   
[OASPA 2020 Conference](https://oaspa.org/oaspa-2020-conference-program/) - September 21-24 - Online  
[Platform Strategies 2020](https://www.silverchair.com/community/platform-strategies/) - September 23 - 24 -  New York, NY  
[ABEC Annual Meeting 2020](https://meeting20.abecbrasil.org.br/) - September 22-25   - Online  
{{% logo-inline %}} [Crossref Live ](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0055/t/page/fm/0) (US timezones) -  October 6 - Online  
[Workshop on Open Citations and Open Scholarly Metadata 2020](https://workshop-oc.github.io/) - September 9 - Online  
[PUBMET2020](http://pubmet.unizd.hr/pubmet2020/) - Septelmber 16-18 - Online  
[PIDapalooza](https://www.eventbrite.com/e/pidapalooza-2020-registration-60971406117) - January 29-30 2020 - Lisbon, Portugal   
[ROR Community Meeting](https://www.eventbrite.com/e/the-ror-community-meeting-lisbon-registration-82814758171) - January 28 - Lisbon, Portugal             
[ASAPbio January 2020 workshop: A Roadmap for Transparent and FAIR Preprints in Biology and Medicine](https://asapbio.org/meetings/preprints-roadmap-2020) - January 20 - Hinxton, UK
[Academic Publishing in Europe (APE)](https://web.archive.org/web/20200125135749/https://www.ape2020.eu/) - January 13-14 2020 - Berlin, Germany    
[NISO Plus](https://www.niso.org/events/2020/02/niso-plus-conference) - February 23 -25 - Baltimore, USA     
[SocietyStreet (virtual)](https://www.societystreet.org/) - March 26 - Washington, DC   
[ER&L](https://2020erl.sched.com/event/XVhQ/w03-using-the-crossref-api-pulling-and-working-with-crossref-publication-and-funder-metadata-in-r) - March 8 - 11 - Austin, TX       
[CASE (webinar)](https://www.asianeditor.org/event/2020/index.php) - August 21 - Online

{{% /accordion-section %}}

{{% /accordion %}}

{{% accordion %}}

{{% accordion-section "2019 events" %}}


[OpenCon Oxford Satellite](https://www.eventbrite.com/e/opencon-oxford-2019-tickets-74992752341)  -  December 6 - Oxford, UK    
[SPARC Africa Open Access Symposium 2019](http://www.sparcafricasymp.uct.ac.za/sparc/programme) - December 4-7 - Cape Town, South Africa     
[STM Week](https://www.stm-assoc.org/events/stm-week-2019/) - December 3-4 - London, UK  
[Munin Conference on Scholarly Publishing](https://site.uit.no/muninconf/) - November 27-28 - Tromsø, Norway          
[SpotOn London](https://www.eventbrite.co.uk/e/spoton-london-2019-communicating-research-for-societal-impact-tickets-67002112121) - November 21 - London, UK      
[euroCRIS Strategic Membership Meeting](https://eurocris.uni-muenster.de/#section-schedule) - November 18-20 - Münster, Germany    
[7th Annual PKP Conference](https://conference.pkp.sfu.ca/index.php/pkp2019/pkp2019) - November 18-20 - Barcelona, Spain    
[Crossref Annual Community Meeting #CRLIVE19](/crossref-live-annual/) - November 13-14 - Amsterdam, Netherlands    
[2LATmetrics conference](https://www.altmetric.com/events/latmetrics-2/) - November 4-6 - Cusco, Perú    
[Charleston Library Conference](https://charlestonlibraryconference.com/full-schedule/) - November 4-8 - Charleston, SC    
[RDA](https://www.rd-alliance.org/rda-14th-plenary-helsinki-espoo) - October 23-25, Helsinki, Finland    
[FORCE2019](https://www.force11.org/meetings/force2019) - October 16-17 - Edinburgh, UK    
[Frankfurt Book Fair 2019](https://www.buchmesse.de/en) - October 16-20 - Frankfurt, Germany     
[6:AM Altmetrics Conference](http://www.altmetricsconference.com/) - October 8-11 - Stirling, UK     
[ISMTE Europe](https://www.ismte.org/page/Conferences) - October 3 - Oxford, UK        
[Transforming Research](http://www.transformingresearch.org/) - September 26-27 - Washington, DC     
[Silverchair Platform Strategies](https://www.silverchair.com/community/platform-strategies/) - September 25-26 - New York, NY      
[European Research Innovation Days](https://ec.europa.eu/info/research-and-innovation/events/upcoming-events/european-research-and-innovation-days_en) - September 24-26 - Brussels, Belgium     
[PubMet](http://pubmet.unizd.hr/pubmet2019) - September 18-20 - Zadar, Croatia     
[ARMS 2019](http://www.arms2019.org.au/) - September 17-20 - Adelaide, South Australia     
[iPres](https://ipres2019.org) - September 16-20 - Amsterdam, Netherlands       
[ALPSP 2019](https://www.alpsp.org/Conference) - September 11-13 - Berkshire, UK     
[17th International Conference on Scientometrics & Infometrics (ISSI2019)](https://web.archive.org/web/20191105203524/https://www.issi2019.org/) - September 2-5 - Rome, Italy       
[RDA UK Workshop](https://www.rd-alliance.org/group/rda-united-kingdom/post/second-rda-uk-workshop-16-july-save-date) - July 16 - London, UK     
[OAI11](https://indico.cern.ch/event/786048/) - June 19-21 - Geneva, Switzerland      
[ARMA conference, 2019](http://www.armaconference.com/) - June 17-18 - Belfast, Northern Ireland       
[4th Regional Meeting of Academic Journal Editors](https://jasolutions.com.co/4to-encuentro-editores-revistas/) - June 5-7 - Medellín, Columbia     
[International Conference on Electronic Publishing (ElPub) 2019](https://elpub2019.sciencesconf.org/) - June 2-4 - Marseille, France     
[CALJ Annual Conference 2019](https://calj-acrs.ca/) - June 1-2 - Vancouver, BC     
[SSP 41st Annual Meeting](https://customer.sspnet.org/SSP/AM-Archive/2019-Annual-Meeting/ssp/AM19/Home.aspx?hkey=621797eb-e077-4f96-87f3-9ce7a40cd6ac) - May 29-31 - San Diego, CA      
[iAnnotate 2019](https://iannotate.org/) - May 22-23 - Washington, DC      
[JATS-Con 2019](https://www.eventsforce.net/wgcconferencecentre/frontend/reg/thome.csp?pageID=5088&eventID=17) - May 21 - Cambridge, UK      
[Library Publishing Forum](https://librarypublishing.org/library-publishing-forum/) - May 8-10 - Vancouver, BC      
[8th International Scientific and Practical Conference](http://conf.rasep.ru/WCSP/WCSP2019 ) - April 23-26 - Moscow, Russia     
[STM US Annual Conference 2019](https://www.stm-assoc.org/events/stm-us-2019/) - April 11-12  - Washington, DC          
[UKSG 42nd Annual Conference and Exhibition](https://www.uksg.org/event/conference19) - April 8-10 - Telford, UK     
[Metrics in Transition Workshop 2019](https://metrics-project.net/en/events/workshop2019/) -  March 27-28 - Göttingen, Germany   
[The London Book Fair 2019](https://www.londonbookfair.co.uk/) - March 12-14 - London, UK    
[IFLA SIG on Library Publishing 2019 Midterm Meeting](https://librarypublishing.org/cfp-ifla-2019-midterm-meeting/) - February 28 - March 1 - Dublin, Ireland     
[AAP/PSP 2019 Annual Conference](https://www.aap2019pspconference.com/new-page) - February 6-8 - Washington, DC     
[Publisher workshop: metadata, Open Access and more](https://www.eventbrite.com/e/publisher-workshop-metadata-open-access-and-more-tickets-52858612533) - February 5 - London, UK     
[PIDapalooza 2019](https://www.eventbrite.com/e/pidapalooza-2019-registration-49295286529) - January 23-24 - Dublin, Ireland     
[APE 2019](https://ape-archiv.eu/ape2019/) - January 16 - Berlin, Germany     
[SSP Pre-Conference](https://www.sspnet.org/events/past-events/ssp-pre-conference-in-conjunction-with-ape-2018-ramping-up-relevance/) - January 15 - Berlin, Germany    

{{% /accordion-section %}}

{{% /accordion %}}

{{% accordion %}}

{{% accordion-section "2018 events" %}}

[Coko London](https://coko.foundation/community/) - December 5 - London, UK  
[STM Week 2018 Tools and Standards](https://www.stm-assoc.org/events/day-1-stm-week-2018-tools-standards/) - Decemer 4 - London, UK  
[OpenCon Oxford](https://www.opencon2018.org/) - November 30 - Oxford, UK  
[Munin Conference on Scholarly Publishing](http://site.uit.no/muninconf/) - November 28 - 29 - Tromsø, Norway  
[Crossref LIVE18 #CRLIVE18](/crossref-live-annual/) - November 13 -14 - Toronto, Canada  
[SpotOn London](https://www.biomedcentral.com/p/spoton-2018) - November 3 - London, UK  
[Workshop on Research Objects](http://www.researchobject.org/ro2018/) - October 29 - Amsterdam, Netherlands  
[RDA 12th Plenary Meeting](https://www.rd-alliance.org/plenaries/rda-twelfth-plenary-meeting-part-international-data-week-2018-gaborone-botswana) - October 26 - Gaborone, Botswana  
[SciDataCon 2018](http://www.scidatacon.org/) - October 22 - 26 - Gaborone, Botswana  
[Frankfurt Book Fair](https://www.buchmesse.de/en) - October 10 - 12 - Frankfurt, Germany  
[FORCE2018](https://force2018.sched.com/) - October 10 - 12 - Montreal, Canada  
[Transforming Research](http://www.transformingresearch.org/) - October 3 - 4 - Providence, RI  
[SciELO 20 Years](https://www.scielo20.org/en/) - September 26 - 28 - Sao Paulo, Brazil  
[5AM London 2018](http://www.altmetricsconference.com/) - September 25-28 - London, UK  
[COASP 2018 Conference](https://oaspa.org/conference/coasp-2018-program/) - September 19 - Vienna, Austria  
[Dublin Core Metadata Initiative](http://dublincore.org/) - September 10 - 13 - Porto, Portugal  
[OpenCitations workshop](https://workshop-oc.github.io/#about) - September 3 - 5 - Bologna, Italy  
[European Association of Science Editors (EASE)](http://www.ease.org.uk/ease-events/14th-ease-conference-bucharest-2018/programme-2018/) - June 7 - 10 - Bucharest, Romania  
[INORMS Conference](http://www.inorms2018.org/) - Jun 6 - 8 - Edinburgh, UK  
[SSP 40th Annual Meeting, Booth #212A](https://customer.sspnet.org/SSP/AM-Archive/2018-Meeting/ssp/AM18/Home.aspx?hkey=621797eb-e077-4f96-87f3-9ce7a40cd6ac) - May 30 - June 1 - Chicago, IL, USA  
[Library Publishing Forum](https://librarypublishing.org/library-publishing-forum/) - May 21 - 23 - Minneapolis, MN, USA  
[3er Congreso Internacional de Editores Redalyc](https://web.archive.org/web/20200316022408/http://congreso.redalyc.org/ocs/public/congresoEditores/index.html) - May 16 - 18 - Trujillo, Peru  
[CSE 2018 Conference](https://www.councilscienceeditors.org/events/previous-annual-meetings/cse-2018-annual-meeting/) -  May 5 - 8 - New Orleans, LA, USA  
[MIT Better Science Ideathon](https://betterscience.mit.edu/) - April 23 - Cambridge, MA, USA  
[Computers in Libraries 2018](http://computersinlibraries.infotoday.com/Speakers/Patricia-Feeney.aspx) - April 17 - 19 - Arlington, VA, USA  
[EARMA Conference 2018](http://www.earmaconference.com/) - April 16 - 18 - Brussels, Belgium  
[International Publishing Symposium](https://publishing.brookes.ac.uk/conference/2018_international_publishing_symposium/) - April 12 - 13 - Oxford, UK  
[UKSG 2018](https://www.uksg.org/event/conference18) - April 9 - 11 - Glasgow, Scotland  
[ISMTE 2018 Asia Pacific Conference](https://www.ismte.org/page/PastConferences) - March 27 - 28 - Singapore  
[NFAIS 2018 Annual Conference](http://www.nfais.org/2018-conference) - February 28 - March 2 - Alexandrea, VA, USA  
[Researcher to Reader](https://r2rconf.com) -  February 26 - 27 - London, UK  
[ASAPBio Meeting](http://asapbio.org/peer-review) - February 7 - 9 - Chevy Chase, Maryland, USA  
[LIS Bibliometrics](https://www.eventbrite.com.au/e/responsible-use-of-bibliometrics-in-practice-tickets-39687409109) - January 30 - London, UK  
Peer Review Transparency Workshop - January 24 - Cambridge, MA, USA  
[PIDapalooza](https://pidapalooza.org/) - January 23 - 24 - Girona, Catalonia, Spain  

{{% /accordion-section %}}

{{% /accordion %}}


{{% accordion %}}

{{% accordion-section "2017 events" %}}

[AGU](https://fallmeeting.agu.org/2017/) - December 14 - 15 - New Orleans, LA, USA  
[STM Innovations Seminar 2017](http://www.stm-assoc.org/events/innovations-seminar-2017/) - December 6 - London, UK  
[Crossref #LIVE17](/crossref-live-annual/) - November 14 - 15 - Singapore  
[XUG eXtyles User Group Meeting](http://www.inera.com/customers/eXtyles-user-group-meeting) - November 2-3 - Cambridge, MA, USA  
[Dublin Core 2017](http://dcevents.dublincore.org/IntConf/dc-2017) - October 26-29 - Washington, DC, USA  
[FORCE 2017](https://www.force11.org/event/force2017-berlin-oct-25-27) - October 25-27 - Berlin, Germany  
[Frankfurt Book Fair, #FBM17](https://www.buchmesse.de/en) - October 11-15 - Frankfurt, Germany  
[Altmetrics Conference 4:AM](http://www.altmetricsconferencearchive.com/) -  September 26 - 29 -  Toronto, Canada  
[COASP 2017](http://oaspa.org/coasp-2017-save-date/) - September 20 - 21 - Lisbon, Portugal  
[ALPSP Conference](https://www.alpsp.org/conference) - September 13 - 15 - Netherlands  
[ISMTE North American Conference](http://www.ismte.org/page/Conferences) - August 10-11 - Denver, CO, USA  
[LIBER 2017 Conference](http://liber2017.lis.upatras.gr/) - July 5-7 - Patras, Greece  
[ALA Annual Conference (ALCTS CRS)](https://www.eventscribe.com/2017/ala-annual/fsPopup.asp?Mode=presInfo&PresentationID=266284) - June 25 - Chicago, IL, USA  
[EMUG 2017](http://www.cvent.com/events/emug-2017/event-summary-e99312253d7f45b2b3b6a0f4dfdb7900.aspx) - June 22-23 - Boston, MA, USA  
[ORCID Identifiers and Intellectual Property Workshop](https://orcid.org/content/identifiers-and-intellectual-property-workshop) - June 22 - Paris, France  
[32nd Annual NASIG Conference 2017](http://www.nasig.org/) - June 8 -11 - Indianapolis, IN, USA  
[SSP 39th Annual Meeting - Booth 500A](https://www.sspnet.org/events/past-events/annual-meeting-2017/event-home/) - May 31-June 2 - Boston, MA, USA  
[5th World Conference on Research Integrity](https://web.archive.org/web/20170614193134/http://wcri2017.org/) - May 28 - 31 - Amsterdam, The Netherlands  
[WikiCite 2017](https://meta.wikimedia.org/wiki/WikiCite_2017) - May 23-25 - Vienna, Austria  
[CSE 2017 Annual Meeting](http://www.resourcenter.net/Scripts/4Disapi07.dll/4DCGI/events/2017/634-Events.html?Action=Conference_Detail&ConfID_W=634&ConfID_W=634) - May 20-23 - San Diego, CA, USA  
[2017 ScholarONE User Conference](https://starcite.smarteventscloud.com/rsvp/invitation/invitation.asp?id=/m2faf11a-TE6MHQR1YP56&EPRegistrationForGuest=True) - May 3-4 - Madrid, Spain  
[UKSG 2017 Annual Conference](http://www.uksg.org/event/conference17) - April 10-12 - Harrogate, UK  
[Highwire Spring Publishers Meeting](http://events.r20.constantcontact.com/register/event?llr=q9cys5lab&oeidk=a07eds09n4bb350024e) - April 4-6 - Stanford, CA, USA  
[CNI Spring 2017 Membership Meeting](https://www.cni.org/events/membership-meetings/upcoming-meeting/spring-2017) - April 3-4 - Albuquerque, NM  
[ISMTE 2017 Asian-Pacific Conference](http://www.ismte.org/event/Beijing2017) - March 27-28 - Beijing, China  
[COPE China Seminar 2017](http://publicationethics.org/events/cope-china-seminar-2017) - March 26 - Beijing, China  
[ACRL 2017 Conference](http://conference.acrl.org/) - March 22-25 - Baltimore, MD, USA  
[#FuturePub 10 - New Developments in Scientific Collaboration Tech](https://www.eventbrite.com/e/futurepub-10-new-developments-in-scientific-collaboration-tech-tickets-32012536358) - March 13 - London, UK  
[Research Libraries UK (RLUK)](http://rlukconference.com/) - March 8-10 - London, UK  
PSP 2017 Annual Conference - February 1-3 - Washington, DC, USA  

{{% /accordion-section %}}

{{% /accordion %}}

{{% accordion %}}

{{% accordion-section "2016 events" %}}

[STM Digital Publishing Conference](http://www.stm-assoc.org/events/stm-digital-publishing-2016/) – December 6-8 – London, UK  
[OpenCon 2016](http://www.opencon2016.org/updates) – November 12-14 – Washington DC, USA  
[PIDapalooza](http://pidapalooza.org/) – November 9-10 – Reykjavik, Iceland  
[Frankfurt Book Fair](https://www.buchmesse.de/en) – October 19-23 – Frankfurt, Germany (Hall 4.2, Stand 4.2 M 85)  
[ORCID Outreach Conference](https://orcid.org/about/events/) – October 5-6 – Washington DC, USA  
[3:AM Conference](http://altmetricsconference.com/) – September 26 – 28 – Bucharest, Romania  
[OASPA](http://oaspa.org/conference/) – September 21-22 – Arlington VA, USA  
[ALPSP](https://www.alpsp.org/Conference) – September 14-16 – London, UK  
[SciDataCon](http://www.scidatacon.org/2016/) – September 11-17 – Denver CO, USA  
[Vivo 2016 Conference](http://vivoconference.org/) – August 17-19 – Denver CO, USA  
ACSE Annual Meeting 2016 – August 10-11 - Dubai, UAE  
[CASE 2016 Conference](http://asianeditor.org/event/2016/index.php) – July 20-22 - Seoul, South Korea  
[SHARE Community Meeting](http://www.share-research.org/2016/04/share-2016-community-meeting/) - July 11-14 - Charlottesville, VA, USA  

{{% /accordion-section %}}

{{% /accordion %}}

Contact our [outreach team](mailto:feedback@crossref.org) if you'd like us to participate in an event or meet us at one of the above.
