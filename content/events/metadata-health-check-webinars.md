+++
title = "Metadata health check webinars"
date = "2024-08-28"
draft = false 
author = "Crossref"
image = "/images/banner-images/blue-prep-webinar.png"
[menu.main]
rank = 4
parent = "Webinars and events"
weight = 30
+++

## How good is your metadata?

Are you curious about the quality of the metadata you're depositing with Crossref? Join our hands-on session to learn how to use the Participation Reports tool to assess the quality of your metadata with Crossref. We'll show you why good metadata matters and provide help on improving your metadata completeness and quality. **Good metadata makes your content easier to find and use.**  

### Why attend?

Get practical help and advice to improve your metadata on-the-spot, adding useful information for completeness and quality.

### What to expect?

We’ll guide you through the Participation Reports tool, showing how it helps you to understand the quality of your metadata. Many people who have attended these sessions have found them helpful for improving their practices.

### Choose a session that suits your schedule

We will have several one-hour sessions, so watch this space and pick a session that works best for you: 

<!-- HTML for Buttons -->
</p>


<p style="text-align: left; margin: 0; padding: 0;">
  <button onclick="window.location.href='https://crossref.zoom.us/webinar/register/9317404983507/WN_JJ0XBXMcSH6SbN9ZhAG_QQ'" 
          style="background-color:#005f83; color: white; margin: 0; padding: 10px; font-size: 1.25em; font-weight: bold; border: none; cursor: pointer;"> 
   OJS focused - Thursday, Mar 13, 2025 - 08:30 UTC 
  </button>
</p><p>

<p style="text-align: left; margin: 0; padding: 0;">
  <button onclick="window.location.href='https://crossref.zoom.us/webinar/register/2017411040419/WN__lvRJwClRgmXeN2uDcjnpw'" 
          style="background-color:#005f83; color: white; margin: 0; padding: 10px; font-size: 1.25em; font-weight: bold; border: none; cursor: pointer;"> 
    OJS focused (Indonesia) - Tuesday, Mar 18, 2025 - 02:00 UTC 
  </button>
</p><p>


<p style="text-align: left; margin: 0; padding: 0;">
  <button onclick="window.location.href='https://crossref.zoom.us/webinar/register/2317405691985/WN_paRclzqdQaqjO_VJ0mLXkA'" 
          style="background-color:#005f83; color: white; margin: 0; padding: 10px; font-size: 1.25em; font-weight: bold; border: none; cursor: pointer;"> 
    OJS focused - Thursday, Mar 27, 2025 - 16:00 UTC 
  </button>
</p><p>

 

Here is a [helpful time converter.](https://www.timeanddate.com/worldclock/converter.html)

<!-- Markdown Links -->
For more details on what we'll cover, check out our [Participation Reports page](https://www.crossref.org/members/prep/). If you'd like to dive deeper into the tool and its benefits, visit our [detailed documentation](https://www.crossref.org/documentation/reports/participation-reports/).

See you on Zoom!  

