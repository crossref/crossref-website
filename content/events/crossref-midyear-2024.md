+++
title = "The shape of things to come - Crossref community call"
slug = "the-shape-of-things-to-come"
date = "2024-03-20"
draft = false 
author = "Crossref"
image = "/images/banner-images/header-midyear24.png"
rank = 6
parent = "Webinars and events"
weight = 100
+++


We joined together to learn about our strategies for making Crossref sustainable and how we planned for the future. We also shared exciting updates on our initiatives and tools that enhance data management, improve accessibility, and ensure metadata is complete. Highlights included our work on preprint matching.

Here's what was on the agenda:
- Examined our strategic direction & achievements
- Resourced Crossref for future sustainability
- Revealed our 2024 Product Roadmap
- Discussed the metadata development pipeline
- Explored Crossref for Grants
- Explored the latest in preprint matching

Webinar was held on Wednesday, March 8, 2024. 

[Slides](/pdfs/public-community-call-the-shape-of-things-to-come-May-8-2024.pdf)  
[Recording](10.5281/zenodo.12549191)  
[Slides](/pdfs/public-community-call-the-shape-of-things-to-come-May-8-2024.pdf)
[Poll questions](/pdfs/results-of-poll-questions-from-crossref-community-call_the-shape-of-things-to-come.pdf)  
[Q&A report](/pdfs/q-a-report-from-crossref-community-call-the-shape-of-things-to-come.pdf)