+++
title = "Keep your account details up-to-date "
date = "2022-07-28"
draft = false
author = "Amanda Bartell"
identifier = "members-area/update-contacts"
rank = 4
weight = 140200
aliases = [
  "/education/metadata-stewardship/maintaining-your-crossref-membership",
  "/education/metadata-stewardship/maintaining-your-crossref-membership/",
  "/documentation/metadata-stewardship/maintaining-your-crossref-membership",
  "/documentation/metadata-stewardship/maintaining-your-crossref-membership/"
]
+++
To make sure that we are contacting the right people at your organization, and to make sure that we include the correct information on your invoices, we need to know if anything changes for you.

Please let us know by completing our [contact form](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) if any of the following changes:
* Your organization name
* Your mailing or billing address (If you are using our payment portal, you can update the billing address associated with your credit card there, but if you need a different billing address to appear on your invoice, you will need to let us know.)
* A significant change to your organizations's annual publishing revenue (this may mean that we need to change your [annual member fee](/fees/#annual-membership-fees-non-funders) tier.)
* A change to one or more of the key contacts on your account.

### Five key contacts for each account <a id='00022' href='#00022'><i class='fas fa-link'></i></a>
When you join Crossref, you provide contact details for (ideally) at least three different people at your organization to undertake five roles. These are key to making your relationship with Crossref and the rest of the community a success, so do think carefully about who will take on each of these roles. These will be the people we contact to confirm any changes to your account.

{{< snippet "/_snippet-sources/contact-types.md" >}}

If you work with Crossref through a sponsor, we only need a Primary contact and a Voting contact from you. 

[Back to members area](/members-area)
