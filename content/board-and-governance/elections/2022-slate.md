+++
title = "Board election 2022 candidates"
date = "2022-09-16"
draft = false
author = "Lucy Ofiesh"
rank = 2
parent = "Elections"
weight = 6
type = 'dontindexcontent'
+++

If you are a voting member of Crossref your ‘voting contact’ will receive an email the week of September 19th. Please follow the instructions in that email which includes links to the relevant election process and policy information.

From about 40 applications, our [Nominating Committee](/committees/nominating/) put forward the following seven candidates to fill the five seats open for election to the Crossref Board of Directors. Please read their candidate statements below.  

### Tier 1, Small and mid-sized members  
| Member organization  | Candidate standing  | Title   | Org type  |  Country |
|---|---|---|---|---|
|[eLife](#eLife)|Damian Pattinson | Executive Director| Nonprofit publisher |UK|  
|[Pan Africa Science Journal](#PASJ)| Oscar Donde|  Editor in Chief | Publisher | Kenya |    

### Tier 2, Large members  
| Member organization  | Candidate standing  | Title   | Org type  |  Country |
|---|---|---|---|---|
|[Clarivate](#clarivate)|Christine Stohn | Director of Product Management | Assessment Service | US  |    
|[Elsevier](#elsevier)|Rose L'Huillier | Senior Vice President Researcher Products | Publisher | Netherlands  |    
|[The MIT Press](#mit) |Nick Lindsay| Journals and Open Access Director | Academic Press | US  |   
|[Springer Nature](#springer) |Anjalie Nawaratne | VP Data Transformation & Chief Business Architect| Publisher | UK |   
 |[Wiley](#wiley) |Allyn Molina | Group Vice President, Research Publishing  | Publisher | US |   


---

##  Tier 1, Small and mid-sized members (electing one seats)

<a id="eLife"></a>
## Damian Pattinson, eLife, UK<br>

<img src="/images/board-governance/damian-pattinson.jpeg" alt="Damian Pattinson, eLife" width="225px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}

### Personal statement <br>
I am a committed member of the Crossref Board, and am currently serving on the Executive Committee as Board Treasurer, helping Ed and the team to oversee Crossref’s sustainable position. I would love to continue to participate, since I believe strongly in Crossref’s mission of open, permanent foundational infrastructure, and open metadata. eLife’s preprint review platform, Sciety, has also signed up to POSI and I am firmly committed to Crossref’s ongoing pursuit of  transparent operations, open community governance, and equitable participation for all entrants in the scholarly communications space. I have contributed to policy on Research Integrity and given evidence at a UK House of Commons Science and Technology Select Committee to position open metadata and infrastructure as a vital component of a trustworthy research ecosystem, referring extensively to Crossref’s services (as posted on Crossref’s blog).

My background is in publishing innovation, having run PLOS ONE for many years, and then founded the preprint server, Research Square. My current role as Executive Director of eLife means I have the experience of running a nonprofit organisation, so I can support Crossref not only in innovation in publishing workflows and underlying technologies, but also organisational and operational challenges of running a non-profit.

I feel I can offer expertise on new models of publishing, particularly in the rapidly-growing preprints space, and a deep understanding of Crossref’s strategic and financial affairs (through my work as Treasurer and Executive Committee member). Most importantly, I have proven I’m willing to commit time and energy to Crossref’s Board to ensure the organisation continues to grow and thrive for the benefit of all stakeholders in the scholarly ecosystem.

### Organization statement

eLife is a non-profit publisher, founded by research funders to experiment with new models of publishing. We have been on the Crossref Board for the past 6 years and have contributed to working groups on metadata, JATS XML, and preprints, and we have served on the Nominating Committee, Membership & Fees Committee, and, most recently, serve on the Executive Committee in the role of Board Treasurer. eLife is deeply committed to Crossref’s principles of open metadata; we are highly ranked for completeness of metadata records and follow best practices as shown in Crossref’s member participation reports.

eLife is committed to improving the publishing ecosystem. This includes broadening participation across all areas of the globe and all demographics. We have been funding PREreview for the past two years to widen participation for under-represented scientists, and ensure that eLife’s own activities are as inclusive and equitable as possible. As a journal, we are moving to a fully preprint-first publishing model, and building new technologies to support emerging reviewing communities. As such we represent the new models of publishing and those innovating in the rapidly-growing preprint-review space.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration personnelle du candidat
Membre impliqué du conseil de Crossref, je fais actuellement partie du comité exécutif en tant que Trésorier du conseil, où j’aide Ed et l’équipe à superviser la position durable de Crossref. J’aimerais poursuivre cette activité, car je crois fermement à la mission de Crossref en matière d’infrastructure de base permanente et ouverte, et de métadonnées ouvertes. La plateforme d’examen des prépublications d’eLife, Sciety, a également adhéré à POSI et je suis fermement engagé dans la quête continue de Crossref en matière d’opérations transparentes, de gouvernance communautaire ouverte et de participation équitable pour tous les entrants dans l’espace des communications savantes. J’ai participé à la politique portant sur l’intégrité de la recherche et témoigné devant une commission d’enquête de la Chambre des communes britannique sur la science et la technologie afin de positionner les métadonnées ouvertes et l’infrastructure comme élément d’un écosystème de recherche fiable, me référant largement aux services de Crossref (comme posté sur le blog de Crossref).

Je suis spécialisé dans l’innovation dans le domaine de l’édition, j’ai d’ailleurs dirigé PLOS ONE pendant de nombreuses années avant de fonder le serveur de prépublication, Research Square. J’exerce actuellement les fonctions de Directeur général, ce qui signifie que j’ai l’expérience de la gestion d’une organisation à but non lucratif, je peux donc soutenir Crossref, non seulement dans le domaine de l’innovation dans les flux de travail d’édition et des technologies sous-jacentes, mais également en ce qui a trait aux défis organisationnels et opérationnels qu’implique la gestion d’un organisme à but non lucratif.

Je pense pouvoir offrir mon expertise sur de nouveaux modèles d’édition, en particulier dans le domaine en pleine expansion des prépublications, ainsi qu’une profonde compréhension des questions stratégiques et financières (grâce à mon expérience en tant que Trésorier et membre du comité exécutif). Plus important encore, j’ai prouvé que je suis prêt à consacrer du temps et de l’énergie au conseil de Crossref pour garantir la poursuite du développement et de la prospérité de l’organisation au bénéfice de l’ensemble des parties prenantes dans l’écosystème universitaire.


### Déclaration organisationnelle
eLife est un éditeur à but non lucratif, fondé par des bailleurs de recherche afin d’expérimenter de nouveaux modèles d’édition. Nous sommes membres du conseil Crossref depuis ces 6 dernières années et avons contribué à des groupes de travail sur les métadonnées, JATS, XML et les prépublications, et nous avons siégé au comité des nominations, au comité des adhésions et des cotisations et, plus récemment, au comité exécutif en tant que trésorier du conseil. eLife est profondément attachée aux principes d’ouverture des métadonnées de Crossref ; nous sommes très bien classés pour l’exhaustivité des enregistrements de métadonnées et nous suivons les meilleures pratiques comme le montrent les rapports de participation des membres de Crossref.

eLife s’engage à améliorer l’écosystème de l’édition. Ceci implique d’élargir la participation à toutes les régions du monde et à toutes les tranches démographiques. Depuis deux ans, nous finançons PREview afin d’élargir la participation des scientifiques sous représentés et veiller à ce que les propres activités d’eLife soient aussi inclusives et équitables que possible. En tant que revue, nous évoluons vers un modèle de publication entièrement basé sur la prépublication et nous développons de nouvelles technologies pour soutenir les communautés de révision émergentes. À ce titre, nous représentons les nouveaux modèles de publication et ceux qui innovent dans l’espace de prépublication-révision, qui connaît une croissance rapide.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração pessoal do candidato
Sou um membro comprometido do Conselho da Crossref e atualmente trabalho no Comitê Executivo como Tesoureiro do Conselho, ajudando Ed e a equipe a supervisionar a posição sustentável da Crossref. Eu adoraria continuar participando, pois acredito fortemente na missão de infraestrutura fundacional aberta e permanente e metadados abertos da Crossref. A plataforma de revisão de pré-publicação da eLife, Sciety, também está inscrita no POSI e estou firmemente comprometido com a busca contínua da Crossref por operações transparentes, governança aberta da comunidade e participação equitativa para todos os participantes no âmbito da comunicação acadêmica. Contribuí para a política de integridade da pesquisa e dei evidências em um Comitê Seleto de Ciência e Tecnologia da Câmara dos Comuns do Reino Unido para posicionar metadados e infraestrutura abertos como um componente vital de um ecossistema de pesquisa confiável, fazendo referência extensivamente aos serviços da Crossref (conforme postado no blog da Crossref) .

Minha experiência é em inovação editorial, tendo chefiado a PLOS ONE por muitos anos e depois fundado o servidor de pré-publicação, Research Square. Meu cargo atual como Diretor Executivo da eLife significa que tenho a experiência de administrar uma organização sem fins lucrativos, para que eu possa dar suporte a Crossref não apenas na inovação de fluxos de trabalho de publicação e tecnologias subjacentes, mas também nos desafios organizacionais e operacionais de administrar uma organização sem fins lucrativos.

Acredito que posso oferecer experiência em novos modelos de publicação, particularmente no campo de pré-publicações, que cresce rapidamente, e um profundo entendimento dos assuntos estratégicos e financeiros da Crossref (por meio do meu trabalho como Tesoureiro e membro do Comitê Executivo). Ainda mais importante, provei que estou disposto a dedicar tempo e energia ao Conselho da Crossref para garantir que a organização continue a crescer e prosperar em benefício de todas as partes interessadas no ecossistema acadêmico.


### Declaração da organização
A eLife é uma editora sem fins lucrativos, fundada por financiadores de pesquisa para experimentar novos modelos de publicação. Pelos últimos 6 anos, estivemos na Crossref Board e contribuímos para grupos de trabalho sobre metadados, JATS XML e pré-publicações, e temos atuado no Comitê de Indicação, no Comitê de Filiação e Taxas e, mais recentemente, atuamos no Comitê Executivo na função de Tesoureiro do Conselho. A eLife está profundamente comprometida com os princípios de metadados abertos da Crossref; somos altamente renomados pela integridade dos registros de metadados e seguimos as melhores práticas, conforme mostrado nos relatórios de participação de membros da Crossref.

A eLife está empenhada em melhorar o ecossistema editorial. Isso inclui ampliar a participação em todas as áreas do mundo e em todas as demografias. Temos financiado a PREreview nos últimos dois anos para ampliar a participação de cientistas sub-representados e garantir que as atividades da própria eLife sejam o mais inclusivas e equitativas possível. Como um periódico, estamos migrando para um modelo de publicação totalmente pré-publicado e criando novas tecnologias para apoiar as comunidades de revisão emergentes. Como tal, representamos os novos modelos de publicação e aqueles que estão renovando o ambiente em rápido crescimento de revisão de pré-publicação.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración personal del candidato
Soy un miembro comprometido de la junta directiva de Crossref y, actualmente, formo parte del comité ejecutivo como tesorero de la junta, donde ayudo a Ed y al equipo a supervisar la posición de Crossref en materia de sostenibilidad. Me encantaría seguir participando porque creo completamente en la misión de Crossref de establecer una infraestructura transparente, permanente y sostenible, así como metadatos abiertos. Sciety, la plataforma de revisión de prepublicaciones de eLife, también se ha inscrito en POSI, y estoy firmemente comprometido con el objetivo constante de Crossref de lograr operaciones transparentes, una administración comunitaria y abierta y la participación equitativa de todos los integrantes del espacio de comunicaciones académicas. He contribuido a la política en materia de integridad en la investigación y presentado pruebas en un comité especial de ciencia y tecnología de la Cámara de los Comunes del Reino Unido para posicionar los metadatos abiertos y la infraestructura como componentes fundamentales de un ecosistema de investigación fiable, con amplias referencias a los servicios de Crossref (según lo publicado en el blog de Crossref).

Tengo experiencia en innovación editorial, pues dirigí PLOS ONE durante muchos años y, posteriormente, fundé el servidor de prepublicación Research Square. Mi puesto actual como director ejecutivo de eLife refleja que tengo experiencia en dirigir organizaciones sin ánimo de lucro, por lo que puedo apoyar a Crossref en la innovación de flujos editoriales y tecnologías subyacentes, y en los desafíos operativos y organizativos que surjan al dirigir este tipo de organizaciones.

Considero que puedo compartir mi experiencia en nuevos modelos de publicación, en particular, en un espacio de prepublicaciones de rápido crecimiento, y mis amplios conocimientos sobre los asuntos estratégicos y financieros de Crossref (a través de mi trabajo como tesorero y miembro del comité ejecutivo). Y, lo que es más importante, he demostrado que estoy dispuesto a dedicar tiempo y energía a la junta directiva de Crossref para garantizar que la organización siga creciendo y prosperando para el beneficio de todas las partes interesadas del ecosistema académico.


### Declaración de la organización
eLife es una editorial sin ánimo de lucro, fundada por financiadores de investigación, con el fin de experimentar nuevos modelos de publicación. Hace seis años que formamos parte de la junta directiva de Crossref y hemos colaborado con grupos de trabajo en materia de metadatos, JATS XML y prepublicación. También hemos participado en el comité de nominación, el comité de miembros y tarifas y, más recientemente, en el comité ejecutivo como tesoreros de la junta. En eLife estamos profundamente comprometidos con los principios de Crossref sobre metadatos abiertos, gozamos de una elevada clasificación en materia de integridad de registros de metadatos y seguimos las prácticas recomendadas tal y como se indica en los informes de participación de los miembros de Crossref.

eLife se compromete a mejorar el ecosistema editorial. Esto incluye ampliar la participación en todas las áreas geográficas y demográficas. Hemos financiado PREreview durante los últimos dos años para ampliar la participación de los científicos menos representados y garantizar que las actividades de eLife sean lo más inclusivas y equitativas posible. Como revista, estamos avanzando hacia un modelo editorial íntegro de prepublicación inicial y desarrollando nuevas tecnologías para respaldar a las comunidades de revisión emergentes. Como tal, representamos los nuevos modelos editoriales y aquellos que innovan en un espacio de revisión de prepublicaciones de rápido crecimiento.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

## 후보자의 개인 소개
저는 Crossref 이사회의 헌신적인 회원이며, 현재 집행위원회에서 이사회 재무를 담당하며 Ed와 팀이 Crossref의 지속 가능한 위치를 감독할 수 있도록 돕고 있습니다. 저는 개방적이고 영구적인 기반 인프라 및 개방형 메타데이터라는 Crossref의 사명을 강하게 믿고 있기 때문에 계속해서 참여하고 싶습니다. eLife의 출판 전 논문 검토 플랫폼인 Sciety 역시 POSI에 등록했으며, 저는 Crossref의 투명한 운영, 개방적인 커뮤니티 거버넌스 및 학술 커뮤니케이션 분야의 모든 참가자를 위한 공정한 참여를 지속적으로 추구할 것을 굳게 약속합니다. 저는 연구 무결성에 대한 정책에 기여했고 Crossref의 서비스를 광범위하게 참조하여 영국 하원의 과학기술 선정 위원회에서 오픈 메타데이터와 인프라를 신뢰할 수 있는 연구 생태계의 중요한 구성요소로 포지셔닝하기 위한 증거를 제시했습니다(Crossref 블로그 참조).

제 배경은 출판의 혁신입니다.저는 수년 동안 PLOS ONE을 운영한 후 출판 전 논문 서버인 Research Square를 개발했습니다. 저는 현재 eLife의 상무 이사로 재직 중입니다. 이것은 제가 비영리 조직을 운영해 본 경험이 있다는 사실을 의미하기 때문에 출판 워크플로우 및 기반 기술 혁신뿐만 아니라 비영리 조직 및 운영상의 과제에서도 Crossref를 지원할 수 있습니다.

저는 특히 급성장하는 출판 전 논문 분야에서 새로운 출판 모델에 대한 전문 지식을 제공하고 Crossref의 전략 및 재무 문제에 대한 심도 깊은 지식을 제공할 수 있다고 생각합니다(재무 담당자 및 집행위원회 회원으로 재직한 경험을 통해). 가장 중요한 것은, 제가 Crossref 이사회에 시간과 에너지를 투자함으로써 학술 생태계의 모든 이해 관계자들의 이익을 위해 조직이 지속적으로 성장하고 번창할 수 있도록 보장할 용의가 있다는 것을 증명했다는 것입니다.

### 단체 소개
eLife는 새로운 출판 모델을 실험하기 위해 연구 기금에 의해 설립된 비영리 출판사입니다. 저희는 지난 6년간 Crossref 이사회의 일원으로서 메타데이터, JATS XML 및 출판 전 논문 관련 작업 그룹에 기여했으며, 임명 위원회, Membership & Fees 위원회에서 활동했으며, 가장 최근에는 집행 위원회에서 이사회 재무를 담당했습니다. eLife는 Crossref의 개방형 메타데이터 원칙을 집중적으로 따르고 있습니다. 저희는 메타데이터 기록의 완성도에 대해 좋은 평가를 받고 있으며, Crossref의 회원 참여 보고서에 나와 있는 모범 사례를 따릅니다.

eLife는 출판 생태계를 개선하는 데 주력하고 있습니다. 여기에는 전 세계 모든 지역과 모든 인구 통계에 걸친 광범위한 참여가 포함됩니다. 저희는 대표성이 부족한 과학자들의 참여를 확대하고 eLife 자체 활동이 최대한 포괄적이고 공정해지도록 보장하기 위해 지난 2년 동안 사전 검토에 자금을 투자해왔습니다. 저희는 저널사로서 완전한 출판 전 논문 우선 출판 모델로 전환하고 새로운 검토 커뮤니티를 지원하는 새로운 기술을 구축하고 있습니다. 따라서 우리는 급성장하는 출판 전 논문 검토 분야에서 새로운 출판 모델과 혁신하는 사람들을 대표하고 있습니다.


{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="PASJ"></a>
## Oscar Donde, Pan Africa Science Journal, Kenya<br>
<img src="/images/board-governance/oscar-donde.jpg" alt="Oscar Donde, Pan Africa Science Journal" width="225px" class="img-responsive" />

{{% accordion %}}
{{% accordion-section "in English" %}}

### Personal statement<br>
My name is Oscar Donde (Ph.D.). I am a Kenyan, a researcher, academician, manager and a publisher, and I interact with many individuals and organizations within the field of research, academics and publications. As a board member, I will endeavor to provide proper financial oversight, ensure the organization has adequate resources through appropriate plans and budgeting, create a strategic plan that is followed, ensure legal compliance and ethical integrity, manage resources responsibly, enhance the organization’s public standing and strengthen its programs and services to all the continents, including those considered to be lagging behind on research and publication. All these will be possible based on my long experience in research, publication and managerial role, and also on being knowledgeable on the Crossref’s services that include; content registration for DOIs, iThenticate similarity check, Cross-mark and cited-by services, that I have acquired and strengthen through my current role as [Crossref ambassador](/community/our-ambassadors/). My main vision for Crossref revolves around my slogan “bring Crossref home”. My service to Crossref as a board member will be driven by that slogan, where my overall focus will be to make ultimate contribution for increased research visibility in Africa and other low income continents, through awareness on data and research article citation, link, assess and re-use, as well as increased membership in those regions.

### Organization statement
Pan Africa Science Journal (PASJ) (https://panafricajournal.com/index.php/home), where I serve as the editor-in-Chief, is one of the few African based journals that are current members of Crossref. This has been driven by PASJ’s mission of being a model journal in Africa with quality services to researchers and academicians. Therefore, presence of PASJ in the Crossref board membership will contribute to the Crossref’s key mandate of making research objects easy to find, cite, link, assess, and reuse, by promoting its presence in continental Africa, a region whose research and publishing trend has just started to take a rising trajectory, as well as in other continents with challenges similar what Africa experiences. Being a member of the board will equally bring along the African research and publishing perspective and ensure that the African voice is heard, considered and promoted within the Crossref, and by far within the scholarly community. This will aim at making Crossref reach and be felt in this low-income region, where access and sharing of research input is still a challenge. Moreover, being a young and uprising African researcher, a manager, academician and scholar, with affiliation to a publishing journal (PASJ) and a University (Egerton University, Kenya), I purpose to bring a unique and all-round approach to the promotion of the Crossref’s services to African Universities as well as to other publishing and research communities in the region. My experience in journal management as well as being the Head of the Department of Environmental Science at Egerton University have also given me the managerial prowess as well as the knowledge on strategic plans, skills that will be key to taking Crossref to the next greater level, and in promoting open scholarly initiatives as envisaged in its mission statement.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}


### Déclaration personnelle du candidat

Je m’appelle Oscar Donde (Ph.D.). Je suis Kényan, chercheur, académicien, responsable et
éditeur, et j’interagis avec de nombreuses personnes et organisations dans le domaine de la recherche, académique et des publications. En tant que membre du conseil d’administration, je mettrai tout en œuvre pour assurer un contrôle financier adéquat, veiller à ce que l’organisation dispose des ressources adaptées via des plans appropriés et l’établissement d’un budget. Je créerai un plan stratégique qui sera suivi, je veillerai à la conformité légale et l’intégrité éthique, je gèrerai les ressources de façon responsable, j'améliorerai le positionnement public de l’organisation et je renforcerai ses programmes et ses services sur tous les continents, notamment ceux dont on estime qu’ils sont en retard dans les domaines de la recherche et de l’édition. Tout cela sera possible grâce à ma longue expérience en matière de recherche, de publication et de gestion, ainsi qu’à ma connaissance des services de Crossref qui comprennent l’enregistrement de contenu pour les DOI, le contrôle de similarité iThenticate, les services Cross-mark et cited-by, que j’ai acquis et renforcés grâce à mon rôle actuel [d’ambassadeur Crossref](/community/our-ambassadors/). Mon principal objectif pour Crossref s’articule autour de mon slogan « Accroître la visibilité de Crossref ». Ce slogan guidera mon service en tant que membre du conseil de Crossref et j’aurai pour principal objectif de contribuer à améliorer la visibilité de la recherche en Afrique et sur d’autres continents à faibles revenus, par la sensibilisation aux citations, aux liens, à l’évaluation et à la réutilisation des données et des articles de recherche, ainsi que par l’augmentation du nombre de membres dans ces régions.

### Déclaration organisationnelle

Pan Africa Science Journal (PASJ) (https://panafricajournal.com/index.php/home), où j’occupe les fonctions de rédacteur en chef est l’une des rares revues basées en Afrique qui sont actuellement membres de Crossref. Cela a été rendu possible par la mission du PASJ, celle d’être une revue modèle en Afrique avec des services de qualité destinés aux chercheurs et aux universitaires. La présence du PASJ dans la composition du conseil d’administration de Crossref contribuera à la principale mission de Crossref : faciliter la recherche, les citations, la liaison, l’évaluation et la réutilisation des objets de recherche. Cela sera possible en promouvant sa présence en Afrique continentale, une région où la recherche et l’édition commencent tout juste à prendre une trajectoire ascendante, ainsi que sur d’autres continents confrontés à des défis similaires à ceux que connaît l’Afrique. En tant que membre du conseil d’administration, j’apporterai également la perspective africaine de la recherche et de l’édition et je veillerai à ce que la voix africaine soit entendue, prise en compte et promue au sein de Crossref et, de loin, au sein de la communauté scientifique. Cela favorisera la portée de Crossref et lui permettra de se faire connaître dans cette région à faibles revenus où l’accès et le partage des données de recherche restent un défi. En outre, en tant que jeune chercheur africain en plein essor, responsable, académicien et universitaire, affilié à une revue d’édition (PASJ) et à une université (Egerton University, Kenya), j’ai l’intention d’apporter une approche unique et globale à la promotion des services de Crossref auprès des universités africaines ainsi que d’autres communautés d’édition et de recherche de la région. Mon expérience dans la gestion de revues ainsi que ma fonction de directeur du département des sciences de l’environnement à l’université d’Egerton m’ont permis d’acquérir des compétences en gestion et des connaissances en matière de plans stratégiques, des aptitudes qui s’avéreront essentielles pour faire passer Crossref à la vitesse supérieure et pour promouvoir les initiatives de recherche ouverte telles qu’elles sont envisagées dans sa déclaration de mission.



{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração pessoal do candidato

Meu nome é Oscar Donde (Ph.D.). Sou queniano, pesquisador, acadêmico, gestor e editor, e interajo com muitos indivíduos e organizações no campo da pesquisa, acadêmicos e publicações. Como membro do conselho, me esforçarei para fornecer supervisão financeira apropriada, garantir que a organização tenha recursos adequados por meio de planos e orçamentos apropriados, criar um plano estratégico que seja seguido, garantir conformidade legal e integridade ética, gerenciar recursos com responsabilidade, melhorar a posição pública da organização e fortalecer seus programas e serviços para todos os continentes, incluindo aqueles considerados defasados em pesquisa e publicação. Tudo isso será possível com base na minha longa experiência em pesquisa, publicação e função gerencial, e também no conhecimento dos serviços da Crossref que incluem; registro de conteúdo para DOIs, verificação de similaridade iThenticate, Cross-mark e serviços Cited-by, que adquiri e fortaleço através da minha atual função como [embaixador da Crossref](/community/our-ambassadors/). Minha visão principal para a Crossref gira em torno do meu slogan “leve a Crossref para casa”. Meu serviço à Crossref como membro do conselho será impulsionado por esse slogan, em que meu foco geral será contribuir para aumentar a visibilidade da pesquisa na África e em outros continentes de baixa renda, por meio da conscientização sobre dados e citação de artigos de pesquisa, link, avaliação e reutilização, bem como o aumento do número de membros nessas regiões.


### Declaração da organização

O Pan Africa Science Journal (PASJ) (https://panafricajournal.com/index.php/home), onde atuo como editor-chefe, é um dos poucos periódicos africanos que são membros da Crossref atualmente. O impulso para isso foi a missão da PASJ de ser um periódico modelo na África com serviços de qualidade para pesquisadores e acadêmicos foi. Portanto, a presença do PASJ na diretoria da Crossref contribuirá para a principal determinação da Crossref, que é tornar os objetos de pesquisa fáceis de encontrar, citar, vincular, avaliar e reutilizar, promovendo sua presença na África continental, uma região cuja tendência de pesquisa e publicação apenas começou a assumir uma trajetória ascendente, assim como em outros continentes com desafios semelhantes aos que a África vivencia. Igualmente, ser membro do conselho trará a perspectiva africana de pesquisa e publicação e garantirá que a voz africana seja ouvida, considerada e promovida dentro da Crossref e, de longe, dentro da comunidade acadêmica. O objetivo disso é fazer com que a Crossref alcance e seja percebido nessa região de baixa renda, onde o acesso e o compartilhamento de informações de pesquisa ainda são um desafio. Além disso, sendo um pesquisador, gestor, acadêmico e estudioso, filiado a um periódico editorial (PASJ) e a uma universidade (Egerton University, Quênia), pretendo trazer uma abordagem única e abrangente à promoção dos serviços da Crossref às universidades africanas, bem como a outras comunidades editoriais e de pesquisa na região. Minha experiência em gerenciamento de periódicos, além de ser o chefe do Departamento de Ciências Ambientais da Egerton University, também me deu a capacidade gerencial, além de conhecimento sobre planos estratégicos, habilidades que serão essenciais para tornar a Crossref ainda melhor e na promoção de iniciativas acadêmicas abertas, conforme previsto em sua declaração de missão.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración personal del candidato

Me llamo Oscar Donde (doctor). Soy de Kenia y soy investigador, académico, gerente y editor, e interactúo con muchas personas y organizaciones del ámbito académico, editorial y de investigación. Como miembro de la junta directiva, trataré de proporcionar una supervisión financiera apropiada, me aseguraré de que la organización cuente con los recursos necesarios mediante planes y presupuestos adecuados, crearé un plan estratégico que se seguirá, garantizaré el cumplimiento jurídico y la integridad ética, gestionaré recursos de manera responsable, mejoraré la imagen pública de la organización y reforzaré sus programas y servicios en todos los continentes, incluidos aquellos que se han quedado rezagados en materia de investigación y publicación. Todo esto será posible gracias a mi vasta experiencia en investigación, publicación y gestión, así como por mis conocimientos sobre los servicios de Crossref (entre los que se incluye el registro de contenido para DOI, iThenticate Similarity Check, los servicios Cited-By y Crossmark) que he adquirido y reforzado en mi puesto actual como [embajador de Crossref](/community/our-ambassadors/). Mi visión principal para Crossref se articula en torno a mi eslogan «Abramos las puertas de nuestra casa a Crossref». Mis servicios a Crossref como miembro de la junta directiva estarán impulsados por este eslogan, donde mi enfoque principal será hacer una contribución definitiva para aumentar la visibilidad de la investigación en África y otros continentes de bajos recursos a través de la concienciación en materia de citación, enlace, evaluación y reutilización de artículos de investigación y datos, así como para incrementar el número de miembros de tales regiones.


### Declaración de la organización

Pan Africa Science Journal (PASJ) (https://panafricajournal.com/index.php/home), donde ejerzo como editor jefe, es una de las pocas revistas con sede en África que forma parte de Crossref en la actualidad. Esto se debe a la misión de PASJ de convertirse en una revista modelo en África que ofrece servicios de calidad a investigadores y académicos. Por ello, la presencia de PASJ en la junta directiva de Crossref contribuirá al principal mandato de Crossref de lograr que los objetos de investigación sean fáciles de encontrar, citar, evaluar y reutilizar promoviendo su presencia en África continental, una región cuya tendencia en investigación y publicación acaba de tomar una trayectoria ascendente, así como en otros continentes con desafíos similares a los que se enfrenta África. Ser miembro de la junta directiva traerá consigo la perspectiva de investigación y publicación de África y garantizará que sus opiniones se escuchen, consideren y promuevan en Crossref y en la comunidad académica. Esto tiene como objetivo lograr que Crossref llegue y esté presente en esta región de bajos recursos, donde el acceso y el uso compartido de información sobre investigaciones sigue siendo un desafío. Por otra parte, al ser un investigador, gerente, académico y erudito africano joven y emergente, y estar afiliado a una revista editorial (PASJ) y a una universidad (Universidad de Egerton, Kenia), propongo adoptar un enfoque único y global hacia la promoción de los servicios de Crossref en universidades africanas, así como en otras comunidades de publicación e investigación de la región. Mi experiencia en la gestión de revistas, así como mi puesto como jefe del Departamento de Ciencias Ambientales de la Universidad de Egerton, también me ha dotado con destrezas en materia de gestión y conocimientos sobre planes estratégicos, habilidades que serán fundamentales para llevar a Crossref al siguiente nivel y promover iniciativas académicas transparentes, tal y como se recoge en su declaración de objetivos.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

### 후보자의 개인 소개

제 이름은 Oscar Donde(박사)입니다. 저는 케냐 사람으로, 연구원, 학술원, 경영자, 그리고 출판업자이기도 합니다.
저는 리서치, 학계, 출판 분야에서 많은 개인 및 단체들과
협력하고 있습니다. 저는 이사회 구성원으로서 적절한 재정 감독을 제공하고,
적절한 계획과 예산을 통해 적절한 자원을 확보하며,
튼튼한 전략 계획을 수립하고, 법적 준수와 윤리적 무결성을 보장하며, 책임감 있게 자원을 관리하고, 조직의 공공성을 높이고, 연구 및 출판 기술이 뒤처진 지역을 포함한 모든 대륙을 대상으로 프로그램을 강화하기 위해 노력할 것입니다. 이 모든 것은 연구, 출판 및 관리 역할에 대한 저의 오랜 경험과 DOI, iThenticate 유사성 검사, 크로스 마크 및 인용 기준 서비스에 대한 콘텐츠 등록을 포함하여 현재 Crossref 앰버서더 역할을 통해 제가 획득하고 강화한 Crossref의 서비스에 대한 지식을 바탕으로 가능할 것입니다 https://www.crossref.org/community/our-ambassadors. Crossref에 대한 저의 주요 비전은 "Crossref를 집으로 데려오세요"라는 슬로건이 핵심입니다. 이사회 회원으로서 Crossref에 대한 저의 봉사는 이 슬로건을 바탕으로 추진될 것이며, 전반적인 목표는 데이터와 연구 기사 인용, 링크, 평가 및 재사용에 대한 인식, 그리고 아프리카 및 기타 저소득 대륙에서 회원 증원을 통해 이 지역 내에서 연구 가시성을 높이는 데 궁극적인 기여를 하는 것입니다.


### 단체 소개

제가 편집장으로 재직 중인 Pan Africa Science Journal(PASJ) (https://panafricajournal.com/index.php/home)은
현재 Crossref 회원인 몇 안 되는 아프리카 기반 저널 중 하나입니다. 이것은 연구자와 학자들에게 양질의 서비스를 제공함으로써 아프리카의 모범 저널이 되겠다는 PASJ의 사명을 바탕으로 추진되었습니다. 따라서, Crossref 이사회 멤버십에서 PASJ의 존재는 연구 및 출판 추세가 이제 막 상승 궤적을 걷기 시작한 아프리카 대륙은 물론 아프리카와 유사한 도전과제를 가진 다른 대륙에서 그 존재를 홍보함으로써 연구 대상을 쉽게 찾고, 인용, 연결, 평가 및 재사용하게 만드는 Crossref의 핵심 권한에 기여할 것입니다. 이사회의 일원이 되면 아프리카의 연구 및 출판 관점을 동등하게 표출할 수 있을 것이며, 아프리카인들의 의견이 Crossref 내에서 그리고 학계 내에서 듣고, 고려되고, 홍보될 수 있을 것입니다. 이것은 연구 정보의 접근과 공유가 여전히 어려운 이 저소득 지역에서 Crossref의 서비스를 제공하고 느낄 수 있게 하기 위한 것입니다. 더욱이, 저는 새롭게 떠오르는 젊은 아프리카 연구자, 경영자, 학자로서 출판 저널(PASJ)과 대학(케냐 Egerton 대학교)에 소속되어 있기 때문에, Crossref의 서비스를 아프리카 대학뿐만 아니라 해당 지역 내 다른 출판 및 연구 커뮤니티에도 홍보하는 독특하고 포괄적인 접근법을 취하고자 합니다. Egerton 대학교에서 환경 과학 학과장일 뿐만 아니라 저널 관리 경험은 전략 계획에 대한 지식, Crossref를 다음 단계로 끌어올리는 데 핵심이 될 기술, 그리고 해당 미션에서 예상한 대로 개방적인 학문적 이니셔티브를 홍보하는 데 있어 저에게 경영 능력도 제공해 주었습니다.



{{% /accordion %}}
{{% /accordion-section %}}

***

## Tier 2, Large members (electing four seats)

<a id="clarivate"></a>
## Christine Stohn, Clarivate, US<br>
<img src="/images/board-governance/Christine-Stohn-bis.jpg" alt="Christine Stohn, Clarivate" width="225px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}
### Personal statement <br>
Clarivate is publisher-independent, and we collaborate with every content platform our customers and users choose with the goal of enabling meaningful connections between publications, content platforms, researchers, and others in the community. My job is to make sure that such integrations work as smoothly as possible to accelerate research and innovation – which aligns well with Crossref’s mission to make scholarly communications better.  

In my current role as Director of Product Management for Library Discovery and Research Information Systems, I’m involved in developing product enhancements and new features, as well as integrations with external platforms and systems.


With my background first in A&I publishing, and then in library systems and specifically library discovery I come with very distinct technical and practical knowledge of the everyday challenges users face, and how to resolve them.

Crossref has been an essential source for us for many years to identify and connect content. The collaborative nature of Crossref resonates well with my understanding of how we should mutually serve researchers and other users of academic content, across organizations and communities. I also serve as a co-chair in one of the NISO topic committees (IDI) with the same goal, to achieve the best possible collaboration across stakeholders.

I thank the committee for considering my nomination.

### Organization statement <br>
Clarivate provides the tools and resources researchers, publishers, institutions and research funders need to monitor, measure, and make an impact in the world of research. Like Crossref, we want to “make scholarly communications better.”

Guided by the legacy of Dr Eugene Garfield, inventor of the world’s first citation index, our goal is to provide guaranteed quality, impact and neutrality through world-class research literature and meticulously captured metadata and citation connections. We devised and curate the largest, most authoritative citation database available, the Web of Science, with over 1 billion cited reference connections indexed from high quality peer reviewed journals, books and proceedings from 1900 to the present.  

Our other brands and products include ProQuest, a key partner to content holders encompassing 90,000 authoritative sources, 6 billion digital pages and six centuries; Ex Libris, whose mission is to allow academic institutions to create, manage, and share knowledge; and ScholarOne, which provides comprehensive workflow management systems for scholarly journals, books and conferences (to name just a few!) We too “make tools and services – all to help put scholarly content in context.”

Dr Nandita Quaderi has served the existing Crossref board representing us as an entirely publisher-independent organisation. As a hugely diverse organization ourselves, we complement the membership and add to the diverse nature of the board. Our presence on the Crossref board enhances our organizations’ shared goals of revealing meaningful connections among research, collaborators, and data as we strive to enable researchers to accelerate discovery, evaluate impact, and benefit society worldwide.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration personnelle de la candidate

Clarivate est indépendant des éditeurs et collabore avec toutes les plateformes de contenu choisies par nos clients et utilisateurs dans le but de permettre des connexions significatives entre les publications, les plateformes de contenu, les chercheurs et les autres membres de la communauté. Mon travail consiste à m’assurer que ces intégrations fonctionnent aussi bien que possible afin d’accélérer la recherche et l’innovation, ce qui correspond bien à la mission de Crossref, qui est d’améliorer les communications savantes.  

À mon poste actuel de directrice de la gestion des produits pour des systèmes d’information de recherche et de découverte des bibliothèques, je participe au développement des améliorations et des nouvelles fonctionnalités des produits, ainsi qu’aux intégrations avec des plateformes et des systèmes externes.


Grâce à mon expérience, d’abord dans le domaine de l’édition A&I, puis dans celui des systèmes de bibliothèque et plus particulièrement de la recherche en bibliothèque, j’ai une connaissance technique et pratique très distincte des défis quotidiens auxquels les utilisateurs sont confrontés et de la manière de les résoudre.

Crossref a été une source essentielle pour nous pendant de nombreuses années pour identifier et connecter le contenu. La nature collaborative de Crossref correspond bien à ma conception de la manière dont nous devrions servir mutuellement les chercheurs et les autres utilisateurs de contenu universitaire, à travers les organisations et les communautés. Je suis également co-présidente de l’un des comités thématiques de la NISO (IDI), qui poursuit le même objectif : obtenir la meilleure collaboration possible entre les parties prenantes.

Je remercie le comité d’avoir pris en compte ma nomination.

### Déclaration organisationnelle

Clarivate fournit les outils et les ressources dont les chercheurs, les éditeurs, les institutions et les bailleurs de fonds de la recherche ont besoin pour surveiller, mesurer et avoir un impact dans le monde de la recherche. Tout comme Crossref, nous voulons « améliorer les communications universitaires ».

Guidé par l’héritage du Dr Eugene Garfield, inventeur du premier index des citations au monde, notre objectif est de fournir une qualité garantie, une portée et une neutralité par l’intermédiaire de documents de recherche de calibre mondial et de métadonnées et de liens de citations méticuleusement collectés. Nous avons conçu et gérons la base de données de citations la plus importante et la plus fiable qui soit, le Web of Science, avec plus d’un milliard de connexions de références citées, indexées à partir de revues, de livres et d’actes de congrès de haute qualité évalués par des pairs, de 1900 à nos jours.  

Parmi nos autres marques et produits, citons ProQuest, un partenaire clé des détenteurs de contenu, avec 90 000 sources faisant autorité, 6 milliards de pages numériques et six siècles ; Ex Libris, dont la mission est de permettre aux institutions universitaires de créer, gérer et partager la connaissance ; et ScholarOne, qui fournit des systèmes complets de gestion des flux de travail pour les revues, livres et conférences scientifiques (pour n’en citer que quelques-uns !). Nous aussi, « nous créons des outils et des services — tout cela pour aider à mettre le contenu universitaire en contexte. »

Dr Nandita Quaderi a servi le conseil d’administration actuel de Crossref, nous représentant comme une organisation entièrement indépendante des éditeurs. En tant qu’organisation extrêmement diversifiée, nous sommes une valeur ajoutée aux membres et contribuons à la nature diversifiée du conseil. Notre présence au sein du conseil d’administration de Crossref renforce les objectifs communs de nos organisations, qui consistent à révéler des connexions significatives entre la recherche, les collaborateurs et les données, dans le but de permettre aux chercheurs d’accélérer la découverte, d’évaluer l’impact et de bénéficier à la société dans le monde entier.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração pessoal da candidata

A Clarivate é independente de editoras. Colaboramos com todas as plataformas de conteúdo que os nossos clientes e usuários escolhem, para possibilitar conexões significativas entre publicações, plataformas de conteúdo, pesquisadores e outros integrantes da comunidade. Meu trabalho é garantir que essas integrações funcionem da melhor forma possível para acelerar a pesquisa e a inovação – algo perfeitamente alinhado à missão da Crossref, que é melhorar as comunicações acadêmicas.  

Em minha função atual, diretora de gerenciamento de produtos para sistemas de descoberta de bibliotecas e de informação de pesquisas, participo do desenvolvimento de aprimoramento de produtos e novos recursos e também de integrações a plataformas e sistemas externos.


Devido à minha experiência no mercado editorial de A&I e, em seguida, em sistemas de biblioteca (mais especificamente, descoberta de bibliotecas), tenho um conhecimento técnico e prático muito específico sobre os desafios que os usuários enfrentam no dia a dia e os meios de superá-los.

Há muitos anos, a Crossref é uma fonte essencial para nós no que se refere à identificação e conexão de conteúdo. A característica colaborativa da Crossref condiz com a abordagem que eu defendo: ajuda mútua entre pesquisadores e outros usuários de conteúdo acadêmico, em várias organizações e comunidades. Além disso, sou copresidente de um dos comitês de tópicos (IDI) da NISO com o mesmo objetivo: obter a melhor colaboração possível entre as partes interessadas.

Agradeço ao comitê por considerar a minha indicação.

### Declaração da organização

A Clarivate fornece as ferramentas e recursos que pesquisadores, editores, instituições e financiadores de pesquisas necessitam para monitorar, medir e criar impacto no mundo da pesquisa. Assim como a CrossRef, queremos “melhorar a comunicação acadêmica”.

Guiados pelo legado do Dr. Eugene Garfield, inventor do primeiro índice de citações do mundo, nosso objetivo é oferecer qualidade, neutralidade e impacto garantidos através de literatura de pesquisa de primeira linha e conexões de citações e metadados meticulosamente capturados. Desenvolvemos e fizemos curadoria do maior e mais confiável banco de dados de citações disponível, o Web of Science, com mais de 1 bilhão de conexões de referências citadas indexadas de periódicos de alta qualidade revisados por pares, livros e documentos datados de 1900 até os dias atuais.  

Temos outras marcas e produtos, como o ProQuest, um importante parceiro dos detentores de conteúdo, que inclui 90.000 fontes confiáveis, 6 bilhões de páginas digitais e seis séculos; Ex Libris, cuja missão é permitir que as instituições acadêmicas criem, gerenciem e compartilhem conhecimento; e o ScholarOne, que fornece sistemas abrangentes de fluxos de trabalho para conferências, livros e periódicos acadêmicos (entre muitos outros!) Também “criamos ferramentas e serviços – tudo para ajudar a colocar o conteúdo acadêmico em contexto”.

A Dra. Nandita Quaderi integrou o conselho atual da Crossref, representando-nos como uma organização totalmente independente de editoras. Devido à ampla diversidade da nossa organização, complementamos a membresia e contribuímos para a diversidade do conselho. Nossa presença no Conselho da CrossRef aprimora os objetivos compartilhados pelas nossas organizações de revelar conexões significativas entre pesquisa, colaboradores e dados, enquanto nos esforçamos para capacitar os pesquisadores para a aceleração de descobertas, avaliar impactos e beneficiar a sociedade global.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración personal de la candidata

Clarivate es independiente de las editoriales, y colaboramos con todas las plataformas de contenidos que eligen nuestros clientes y usuarios con el objetivo de permitir conexiones significativas entre las publicaciones, las plataformas de contenidos, los investigadores y otros miembros de la comunidad. Mi trabajo consiste en asegurarme de que estas integraciones funcionen lo mejor posible para acelerar la investigación y la innovación, lo que coincide con la misión de Crossref de mejorar las comunicaciones académicas.  

En mi puesto actual como directora de gestión de productos para la localización de bibliotecas y los sistemas de información sobre investigaciones, participo en el desarrollo de mejoras y nuevas funciones de los productos, así como en las integraciones con plataformas y sistemas externos.


Gracias a mi experiencia, primero en la editorial A&I, y luego en sistemas bibliotecarios y, en concreto, en la localización de bibliotecas, tengo un conocimiento técnico y práctico muy claro de los retos diarios a los que se enfrentan los usuarios, y de cómo resolverlos.

Crossref ha sido una fuente esencial para nosotros durante muchos años para identificar y conectar contenidos. La naturaleza colaborativa de Crossref coincide con mi idea de cómo debemos servir mutuamente a los investigadores y a otros usuarios de contenidos académicos, a través de todas las organizaciones y comunidades. También soy copresidenta de uno de los comités temáticos de NISO (IDI) con el mismo objetivo, lograr la mejor colaboración posible entre las partes interesadas.

Agradezco a la comisión que haya considerado mi candidatura.

### Declaración de la organización

Clarivate proporciona las herramientas y los recursos que los investigadores, los editores, las instituciones y los patrocinadores de investigaciones necesitan para supervisar y medir el mundo de la investigación, así como para tener un impacto en este. Al igual que Crossref, queremos «mejorar la difusión académica».

Con el legado del Dr. Eugene Garfield por bandera, el inventor del primer código de citación del mundo, nuestro objetivo es ofrecer una garantía de calidad, impacto y neutralidad mediante obras de investigación de categoría mundial y conexiones de citas y metadatos seleccionados cuidadosamente. Hemos diseñado y organizado la base de datos de citas mayor y más fidedigna disponible, la Web of Science, con más de mil millones de conexiones de referencias citadas indexadas, desde revistas de gran calidad revisadas por homólogos hasta libros y actas que van desde 1900 hasta la actualidad.  

Nuestras otras marcas y productos incluyen a ProQuest, un socio clave para los titulares de contenidos que abarca 90 000 fuentes autorizadas, 6000 millones de páginas digitales y seis siglos; Ex Libris, cuya misión es permitir que las instituciones académicas creen, gestionen y compartan el conocimiento; y ScholarOne, que ofrece sistemas integrales de gestión del flujo de trabajo para revistas, libros y conferencias académicas (por nombrar solo algunos). Nosotros también «facilitamos herramientas y servicios, todo ello para contribuir a contextualizar el contenido académico».

La Dra. Nandita Quaderi ha prestado sus servicios a la actual junta directiva de Crossref, representándonos como una organización totalmente independiente de los editores. Como organización enormemente diversa que somos, complementamos a los miembros y nos sumamos a la naturaleza diversa de la junta directiva. Nuestra presencia en la junta directiva de Crossref potencia los objetivos compartidos de nuestras organizaciones de mostrar las valiosas conexiones entre investigaciones, colaboradores y datos, ya que tratamos de ayudar a que los investigadores agilicen los descubrimientos, a la evaluación del impacto y a beneficiar a la sociedad mundial.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

### 후보자의 개인 소개

Clarivate는 게시자 독립적이며 고객과 사용자가 선택한 모든 콘텐츠 플랫폼과 협력하여 출판물, 콘텐츠 플랫폼, 연구원 및 커뮤니티 내 사람들 간의 의미 있는 연결을 지원하게 하는 것을 목표로 합니다. 이러한 통합이 가능한 한 원활하게 작동하여 연구와 혁신을 가속화하는 것이 저의 임무이며, 이는 학술 커뮤니케이션을 개선하려는 Crossref의 사명과도 잘 부합합니다.  

현재 도서관 검색 및 연구 정보 시스템의 제품 관리 이사로 근무하면서 제품 개선 사항 및 새로운 기능을 개발하고 외부 플랫폼 및 시스템과의 통합에도 관여하고 있습니다.


먼저 A&I 출판에 대한 배경 지식을 바탕으로 도서관 시스템, 특히 도서관 발견 분야에서 사용자들이 직면하는 일상적인 문제와 이를 해결하는 방법에 대한 매우 뚜렷한 기술적 실무적 지식을 가지고 있습니다.

Crossref는 수년 동안 콘텐츠를 식별하고 연결하는 데 필수적인 소스였습니다. Crossref의 협력적 특성은 조직과 커뮤니티 전반에 걸쳐 연구자와 학술 콘텐츠의 다른 사용자에게 상호 서비스를 제공하는 방법에 대한 저의 이해와 잘 맞아 떨어졌습니다. 또한 이해 관계자 간에 가능한 최고의 협업을 달성하기 위해 동일한 목표를 가진 NISO 주제 위원회(IDI) 중 하나에서 공동 의장으로 활동하고 있습니다.

제 추천을 고려해 주신 위원회에 감사드립니다.

### 단체 소개

Clarivate은 연구자, 출판사, 기관 및 연구 자금 지원 기관이 연구계를 모니터링, 평가 및 영향을 미치는 데 필요한 도구 및 자원을 제공합니다. Crossref와 마찬가지로 우리는 “학문적 커뮤니케이션을 개선”하고자 합니다.

세계 최초의 인용 색인 발명자인 Eugene Garfield 박사의 업적에 따라, 우리의 목표는 세계적 수준의 연구 문헌과 꼼꼼하게 캡처한 메타데이터 및 인용 연결을 통해 품질, 영향 및 중립성을 보장하는 것입니다. 우리는 1900년부터 현재까지 고품질 동료 검토 저널, 서적 및 회보에서 색인이 붙은 10억 건이 넘는 참조 연결을 사용하여 가장 크고 가장 권위 있는 인용 데이터베이스인 Web of Science를 고안하고 정리했습니다.  

당사의 다른 브랜드 및 제품에는 90,000개의 신뢰할 수 있는 소스, 60억 개의 디지털 페이지 및 6세기 분량의 콘텐츠 소유자의 핵심 파트너인 ProQuest, 학술 기관이 지식을 생성, 관리 및 공유할 수 있도록 지원하는 Ex Libis, 학술지, 서적 및 컨퍼런스(몇 가지만 들자면!)를 위한 포괄적인 워크플로우 관리 시스템을 제공하는 ScholarOne 등이 있습니다. 또한, 우리는 “학문적 콘텐츠를 맥락에서 이해하는 데 도움이 되는 도구와 서비스를 만듭니다.”

Nandita Quaderi 박사는 출판사로부터 완전히 독립적인 조직으로서 우리를 대표하는 기존 Crossref 이사회에 봉사했습니다. 매우 다양한 조직으로서 우리는 회원 자격을 보완하고 이사회의 다양한 특성을 추가합니다. 우리는 Crossref 이사회에 참여함으로써 연구자들이 전 세계적으로 발견을 가속하고 영향을 평가하며 사회에 혜택을 줄 수 있도록 노력하면서 연구, 공동 작업자 및 데이터 간의 의미 있는 연결을 밝히려는 우리 조직의 공유된 목표를 향상합니다.

{{% /accordion %}}
{{% /accordion-section %}}

---


<a id="elsevier"></a>
## Rose L'Huillier, Elsevier, Netherlands<br>
<img src="/images/board-governance/Rose-LHuillier.jpg" alt="Rose L'Huillier, Elsevier" width="200px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}
### Personal statement

In my 18 years of experience in the STM industry, I gained a wide variety of skills and knowledge that could help guide Crossref through the next phase in its development. Due to a variety of roles that I fulfilled at Elsevier, I have a strong background in strategy, publishing, analytics and product management. I was lucky to have been at the forefront of the implementation of open access options at Elsevier, including the building of a new infrastructure, and I am proud to see Elsevier now as one of the largest and fastest growing open access publishers. As a digital product professional with wide STM expertise (currently overseeing products like ScienceDirect and Mendeley), I care deeply about creating value with products and services for the benefit of research and society. I am also a strong believer in community collaboration, as for example shown by the recent ScienceDirect content syndication pilot to improve dissemination and discovery of research.

In my first tenure at Crossref, I served on the Strategy Committee, the M&F Committee, and am currently on the Executive Board. I also serve on the GetFTR board. Crossref has already evolved significantly in the past few years: we published a new strategic narrative with two new goals, covering exciting developments like adopting the POSI principles, which led to more transparency and improved governance. I’d be honored to continue to support Crossref through its continuing journey of balancing high performance and member satisfaction on existing services with investing in the future of a fully connected open research ecosystem.


### Organizational statement <br>
At Elsevier, we believe that to meet the ever-growing needs of researchers and scholars, the information system supporting research must be founded upon the principles of source neutrality, interoperability and transparency, placing researchers under full control of their information. As one of the largest open access publishers, we provide authors with a choice of publication models, and are investing in tools such as SSRN, Mendeley Data, and text and data mining to support Open Science.

Today, a vibrant array of companies, non-profits, universities, and researchers provide scholarly tools and information resources. Common standards and shared infrastructure play a vital role enabling innovation and fostering competition towards finding ever better ways for researchers and their institutions to manage the unprecedented amount of knowledge and data that is available to today. As an industry leader, Elsevier has a strong track record of supporting many of the standards and infrastructure services we rely on today: We were charter members of the International DOI Foundation, have been involved with Crossref since its start, and are founding members of ORCID and CHORUS.

As the largest financial contributor to Crossref, we are proud of the role we play in sustaining this vital shared resource, and as a large organization, we are also able to commit significant staff time to the adoption of new initiatives, such as ROR, the Article Sharing Framework (ASF), and new schema. We continue to support the maintenance and enhancement of the Crossref Funder Registry as a free service to the community.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration personnelle de la candidate
Au cours de mes 18 années d’expérience dans le secteur des STM, j’ai acquis une grande variété de compétences et de connaissances qui pourraient aider à guider Crossref dans la prochaine phase de son développement. Les différents postes que j’ai occupés chez Elsevier m’ont permis d’acquérir une solide expérience en matière de stratégie, d’édition, d’analyse et de gestion de produits. J’ai eu la chance de me trouver à l’avant-garde de la mise en œuvre des options d’accès ouvert chez Elsevier, y compris la construction d’une nouvelle infrastructure, et je suis fière de voir qu’aujourd’hui, Elsevier est l’un des éditeurs d’accès ouvert les plus importants et à la croissance la plus rapide. En tant que professionnelle des produits numériques dotée d’une vaste expertise en matière de STM (je supervise actuellement des produits tels que ScienceDirect et Mendeley), j’ai à cœur de créer de la valeur avec des produits et des services au profit de la recherche et de la société. Je crois aussi fermement en la collaboration entre communautés, comme le montre le récent projet pilote de syndication de contenu de ScienceDirect visant à améliorer la diffusion et la découverte de la recherche.

Lors de mon premier mandat à Crossref, j’ai fait partie du comité stratégique, du comité M&F et je suis actuellement membre du conseil d’administration. Je siège également au conseil d’administration de GetFTR. Crossref a déjà beaucoup évolué ces dernières années : nous avons publié un nouveau récit stratégique avec deux nouveaux objectifs, couvrant des développements passionnants comme l’adoption des principes POSI, qui ont conduit à plus de transparence et à une meilleure gouvernance. Je serais honorée de continuer à soutenir Crossref tout au long de son parcours qui consiste à équilibrer les hautes performances et la satisfaction des membres sur les services existants tout en investissant dans l’avenir d’un écosystème de recherche ouvert entièrement connecté.

### Déclaration organisationnelle
Chez Elsevier, nous pensons que pour répondre aux besoins sans cesse croissants des chercheurs et des universitaires, le système d’information soutenant la recherche doit reposer sur les principes de neutralité des sources, d’interopérabilité et de transparence, en plaçant les chercheurs sous le contrôle total de leurs informations. En tant que l’un des plus grands éditeurs en libre accès, nous offrons aux auteurs un choix de modèles de publication et nous investissons dans des outils tels que SSRN, Mendeley Data et l’exploration de textes et de données pour soutenir la science ouverte.

Aujourd’hui, un riche éventail d’entreprises, d’organisations à but non lucratif, d’universités et de chercheurs fournissent des outils spécialisés et des sources d’informations précieuses. Travailler avec des normes communes, sur une infrastructure partagée, est d’une importance capitale pour stimuler l’innovation et la concurrence qui mettront entre les mains des chercheurs et de leurs institutions des solutions toujours plus efficaces pour gérer le volume sans précédent de connaissances et de données aujourd’hui disponibles. À titre de leader du secteur, Elsevier a déjà témoigné un soutien actif à plusieurs des normes et services d’infrastructure sur lesquels nous nous appuyons aujourd’hui : Nous avons été des membres fondateurs de l’International DOI Foundation, nous participons à Crossref depuis ses débuts et nous sommes membres fondateurs d’ORCID et de CHORUS.

En tant que plus importants contributeurs financiers de Crossref, nous sommes fiers du rôle que nous jouons dans le maintien de cette ressource partagée vitale. Et en tant qu’organisation importante, nous sommes également en mesure de consacrer beaucoup de temps à l’adoption de nouvelles initiatives, telles que ROR, l’Article Sharing Framework (ASF) et les nouveaux schémas. Nous continuons à assurer le maintien et l’amélioration du registre des bailleurs de fonds Crossref, service que nous offrons gracieusement à la communauté.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração pessoal da candidata
Nos meus 18 anos de experiência no setor de STM, adquiri uma ampla variedade de habilidades e conhecimentos que podem ajudar a guiar a Crossref em sua próxima fase de desenvolvimento. Devido às diversas funções que desempenhei na Elsevier, tenho muita experiência em estratégia, análise, gerenciamento de produtos e no mercado editorial. Tive a sorte de estar na vanguarda da implementação de opções de acesso aberto na Elsevier, que incluiu o desenvolvimento de uma nova infraestrutura, e atualmente tenho o orgulho de ver a Elsevier como uma das maiores editoras e uma das que apresenta o crescimento mais rápido. Como profissional de produtos digitais com ampla experiência em STM (atualmente supervisiono produtos como ScienceDirect e Mendeley), valorizo profundamente a criação de valor com produtos e serviços para o benefício da pesquisa e da sociedade. Além disso, acredito firmemente na colaboração com a comunidade, demonstrada, por exemplo, pelo recente piloto de sindicação de conteúdo da ScienceDirect para aprimorar a disseminação e descoberta de pesquisas.

Em meu primeiro período na Crossref, participei do Comitê de Estratégia e do Comitê de M&F e atualmente faço parte do Conselho Executivo. Também faço parte do conselho da GetFTR. A Crossref já evoluiu bastante nos últimos anos: publicamos uma nova narrativa estratégica com duas novas metas, cobrindo desdobramentos empolgantes, como a adoção dos princípios POSI, que aumentou a transparência e aprimorou a governança. Seria uma honra continuar a apoiar a Crossref em sua jornada contínua de equilíbrio entre alto desempenho e satisfação dos membros com os serviços existentes, investindo no futuro de um ecossistema de pesquisa aberto e totalmente conectado.

### Declaração da organização
Na Elsevier, acreditamos que, para satisfazer as crescentes necessidades de pesquisadores e acadêmicos, o sistema de informação que dá suporte à pesquisa deve ser fundamentado sobre os princípios de neutralidade de fonte, interoperabilidade e transparência, oferecendo aos pesquisadores o controle absoluto de suas informações. Nossa empresa, uma das maiores editoras de acesso aberto, fornece aos autores várias opções de modelos de publicação. Além disso, estamos investindo em ferramentas como SSRN, Mendeley Data e mineração de texto e dados para apoiar a Ciência Aberta.

Hoje, um grupo vibrante de empresas, organizações sem fins lucrativos, universidades e pesquisadores oferece recursos de informações e ferramentas. Padrões comuns e infraestrutura compartilhada desempenham um papel vital, possibilitando inovações e fomentando a competição voltada à descoberta de maneiras ainda melhores para que pesquisadores e suas instituições gerenciem o volume de conhecimento e dados sem precedentes disponíveis atualmente. Como líder no setor, a Elsevier tem um forte histórico de suporte a muitos dos padrões e serviços de infraestrutura nos quais nos apoiamos hoje. Somos sócios fundadores da Fundação DOI Internacional, estamos envolvidos com a Crossref desde o início e somos membros fundadores do ORCID e do CHORUS.

Enquanto maior contribuinte financeiro da Crossref, temos orgulho do papel que desempenhamos na manutenção deste fundamental recurso compartilhado e, como uma grande organização, também podemos dedicar parcela significativa do tempo da equipe à adoção de novas iniciativas, como o ROR, a Article Sharing Framework (ASF) e o novo esquema. Seguimos apoiando a manutenção e aprimoramento do Registro de financiador da Crossref como um serviço gratuito à comunidade.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración personal de la candidata
En mis 18 años de experiencia en el sector de STM, adquirí una gran variedad de habilidades y conocimientos que podrían ayudar a guiar a Crossref en la siguiente fase de su desarrollo. Debido a la variedad de funciones que desempeñé en Elsevier, tengo una sólida experiencia en estrategia, publicación, análisis y gestión de productos. Tuve la suerte de estar al frente de la implementación de las opciones de acceso abierto en Elsevier, incluyendo la construcción de una nueva infraestructura, y estoy orgullosa de ver que Elsevier es ahora una de las editoriales de acceso abierto más grandes y de más rápido crecimiento. Como profesional de productos digitales con amplia experiencia en STM (actualmente supervisando productos como ScienceDirect y Mendeley), me interesa mucho crear valor con productos y servicios en beneficio de la investigación y la sociedad. También creo firmemente en la colaboración con la comunidad, como demuestra, por ejemplo, el reciente proyecto piloto de sindicación de contenidos de ScienceDirect para mejorar la difusión y el descubrimiento de la investigación.

En mi primera etapa en Crossref, formé parte del Comité de Estrategia, el Comité de M&F y actualmente estoy en la junta directiva. También formo parte de la junta directiva de GetFTR. Crossref ya ha evolucionado significativamente en los últimos años: hemos publicado una nueva narrativa estratégica con dos nuevos objetivos, que abarcan desarrollos apasionantes como la adopción de los principios POSI, que han llevado a una mayor transparencia y a una gobernanza mejorada. Sería un honor para mí seguir apoyando a Crossref en su viaje continuo para equilibrar el alto rendimiento y la satisfacción de los miembros en los servicios existentes con la inversión en el futuro de un ecosistema de investigación abierta totalmente conectada.

### Declaración de la organización
En Elsevier, creemos que para satisfacer las necesidades cambiantes de los investigadores y académicos, el sistema de información que sustenta la investigación debe basarse en los principios de la neutralidad, la interoperabilidad y la transparencia de las fuentes, concediendo el control total de la información a los investigadores. Como una de las mayores editoriales de acceso abierto, ofrecemos a los autores la posibilidad de elegir entre varios modelos de publicación y estamos invirtiendo en herramientas como SSRN, Mendeley Data y la minería de textos y datos para apoyar la Ciencia abierta (Open Science).

Actualmente, una dinámica selección de empresas, organizaciones sin ánimo de lucro, universidades e investigadores ofrecen herramientas académicas y fuentes de información. Los estándares comunes y la infraestructura compartida desempeñan un papel fundamental en el fomento de la innovación y la promoción de la competición con el fin de hallar vías mejores para que los investigadores y sus instituciones gestionen el volumen de conocimientos y datos sin precedentes que existe en la actualidad. Como líder del sector, Elsevier cuenta con una sólida trayectoria en la promoción de los numerosos estándares y servicios de infraestructura que existen hoy en día: Fuimos miembros fundadores de la International DOI Foundation, participamos en Crossref desde sus inicios y somos miembros fundadores de ORCID y CHORUS.

Como mayor contribuyente financiero de Crossref, estamos orgullosos del papel que desempeñamos en el mantenimiento de este recurso compartido vital, y como organización grande, también podemos dedicar un tiempo considerable del personal a la adopción de nuevas iniciativas, como ROR, el Article Sharing Framework (ASF) y los nuevos esquemas. Seguimos apoyando el mantenimiento y la mejora del Funder Registry (registro de financiadores) de Crossref como servicio gratuito a la comunidad.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

### 후보자의 개인 소개
저는 STM 업계에서 18년간 활동하면서 Crossref 개발의 다음 단계를 안내하는 데 도움이 될 수 있는 다양한 기술과 지식을 얻었습니다. 그리고 Elsvier에서 수행한 다양한 역할 덕분에 전략, 출판, 분석 및 제품 관리에 대한 강력한 배경 지식을 보유하고 있습니다. 저는 운이 좋게도 새로운 인프라 구축을 포함하여 Elsevier에서 개방형 액세스 옵션 구현의 선두에 서게 되었고, Elsevier가 현재 가장 크고 빠르게 성장하는 오픈 액세스 게시자 중 하나임을 자랑스럽게 생각합니다. 저는 광범위한 STM 전문지식을 갖춘 디지털 제품 전문가로서(현재 ScienceDirect, Mendely와 같은 제품을 감독하고 있음) 연구 및 사회의 이익을 위한 제품과 서비스로 가치를 창출하는 데 깊은 관심을 가지고 있습니다. 또한 최근 ScienceDirect 컨텐츠 신디케이션 파일럿이 연구의 보급과 발견을 개선하기 위해 실시한 사례에서 볼 수 있듯이, 저는 커뮤니티 협업의 강력한 신봉자입니다.

Crossref에서 처음으로 재직했을 때 전략 위원회, M&F 위원회에서 근무했으며 현재는 집행 위원회에서 일하고 있습니다. 또한 GetFTR 이사회에서도 활동하고 있습니다. Crossref는 지난 몇 년 동안 크게 발전했습니다. 우리는 POSI 원칙을 채택하는 것과 같은 흥미로운 발전을 다루는 두 가지 새로운 목표를 가진 새로운 전략적 서사를 발표했고, 이는 더 많은 투명성과 향상된 거버넌스로 이어졌습니다. 저는 완전히 연결된 개방형 연구 생태계의 미래에 대한 투자와 함께 기존 서비스에 대한 높은 성능과 회원 만족도의 균형을 유지하는 지속적인 여정을 통해 Crossref를 계속 지원하게 된 것을 영광으로 생각합니다.

### 단체 소개
Elsevier는 끊임없이 증가하는 연구자와 학자들의 요구를 충족시키기 위해 연구를 지원하는 정보 시스템이 출처 중립성, 상호 운용성 및 투명성의 원칙에 기초하여 연구자들이 정보를 완전히 통제할 수 있어야 한다고 믿고 있습니다. 가장 큰 오픈 액세스 출판사 중 한 곳인 우리는 저자에게 출판 모델을 선택할 수 있는 기회를 제공하고 Open Science를 지원하기 위해 SSRN, Mendeley Data, 텍스트 및 데이터 마이닝과 같은 도구에 투자하고 있습니다.

오늘날 상당히 많은 기업, 비영리 단체, 대학 및 연구자가 학술 도구 및 정보 자원을 제공하고 있습니다. 일반적인 기준과 공유된 인프라는 연구자와 기관이 오늘날 이용할 수 있는 전례 없이 많은 양의 지식과 데이터를 관리하는 더 나은 방법을 찾도록 혁신을 가능하게 하고 경쟁을 촉진하는 데 중요한 역할을 합니다. 업계 선두주자로서 Elsevier는 오늘날 우리가 의존하는 많은 기준 및 인프라 서비스의 지원에 있어 눈부신 실적을 보유하고 있습니다. 우리는 국제 DOI 재단의 창립 회원이었으며, Crossref가 시작된 이래로 Crossref에 참여했으며 ORCID 및 CHORUS의 창립 회원입니다.

Crossref의 가장 큰 재정적 기부자로서, 우리는 이 중요한 공유 자원을 유지하는 데 있어 우리의 역할을 자랑스럽게 여기며, 또한 대규모 조직으로서 ROR, 기사 공유 프레임워크(ASF) 및 새로운 스키마와 같은 새로운 이니셔티브 채택에 상당한 직원 시간을 할애할 수 있습니다. 우리는 계속해서 커뮤니티에 대한 무료 서비스로 Crossref Funder Registry의 유지 관리 및 향상을 지원하고 있습니다.


{{% /accordion %}}
{{% /accordion-section %}}

---

<a id="mit"></a>
## Nick Lindsay, The MIT Press, US<br>

<img src="/images/board-governance/NLindsay.jpeg" alt="Scott Delman, Association for Computing Machinery (ACM)" width="150px" class="img-responsive" />

{{% accordion %}}
{{% accordion-section "in English" %}}

### Personal statement

I’ve worked in scholarly publishing for nearly twenty years and am responsible for the strategic direction and performance of the MIT Press scholarly journals division and open access efforts across both books and journals. I’ve previously served as an alternate on the Crossref board, have served on Crossref committees, and regularly attend Crossref events, all of which have prepared me to take the next step and I would be honored to serve a full term on the board.

In July 2020 the Crossref board passed a motion to, “...proactively lead an effort to explore, with other infrastructure organizations and initiatives, how we can improve the scholarly research ecosystem.” This deeply exciting development underlies the kind of broad, encompassing vision for scholarly communications that is really core to the mission of Crossref and I can think of no other organization that is better positioned or suited to make this a reality. Crossref sits as the central node in the scholarly communications network and has the ability to establish and strengthen connections with all the key stakeholders like no other organization. Succeeding here will empower new forms of research and discovery and will allow for a much fuller picture of the breadth and depth of the academic communications landscape to emerge. Working on these challenges would be very meaningful and rewarding and I would look forward to devoting my energy to fulfilling this vision.

### Organizational statement <br>

MIT Press’s mission is to lead by pushing the boundaries of scholarly publishing in active partnership with the MIT community and aligned with MIT's mission to advance knowledge in science, technology, the arts, and other areas of scholarship that will best serve the nation and the world in the twenty-first century. This mission has guided and propelled MIT Press’ longstanding commitment to innovation and experimentation and has established the Press in a leadership position in the university press world. MIT Press shares the outlook and concerns of many members of the university press community, and because of this we believe we’d share a valuable perspective on the board. The Press is very attuned to the needs of university presses across all tiers and shares many of the same objectives: expanding usage of DOIs for all members while at the same time experimenting with new initiatives such as publishing peer reviews and establishing connections between preprints, peer reviews and finished articles that we publish.

MIT Press has been a longstanding Crossref member and has deep familiarity with the service offerings from Crossref. We would work closely with other university presses to bring their perspectives and requirements to the board and would be keen to contribute to the enhancement of existing services and the development of new ones. We are a highly experimental organization, and in that spirit we’d hope to bring novel ideas to Crossref that could help push scholarly communications in new directions for the benefit of all.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration personnelle du candidat
Je travaille dans l’édition universitaire depuis près de vingt ans et je suis responsable de l’orientation stratégique et des performances de la division des revues scientifiques de MIT Press et des efforts en matière d’accès ouvert pour les livres et les revues. J’ai déjà été suppléant au conseil d’administration de Crossref, j’ai fait partie de comités de Crossref et j’assiste régulièrement à des événements de Crossref. Tout cela m’a préparé à franchir la prochaine étape et je serais honoré de servir un mandat complet au conseil d’administration.

En juillet 2020, le conseil d’administration de Crossref a adopté une motion visant à « ... diriger de manière proactive un effort afin d’explorer, avec d’autres organisations et initiatives d’infrastructure, les possibilités d’améliorer l’écosystème de la recherche universitaire ». Ce passionnant développement est à l’origine de la vision large et globale des communications savantes qui se trouve au cœur de la mission de Crossref et selon moi, aucune autre organisation n’est mieux placée ou mieux adaptée pour concrétiser cela. Crossref est le nœud central du réseau des communications savantes et a la capacité d’établir et de renforcer les liens avec toutes les parties prenantes clés comme aucune autre organisation. La réussite dans ce domaine favorisera de nouvelles formes de recherche et de découverte et permettra de dresser un tableau beaucoup plus complet de l’étendue et de la profondeur du paysage des communications universitaires. Le fait de travailler sur ces défis serait très utile et gratifiant et je me réjouirais de consacrer mon énergie à la réalisation de cette vision.

### Déclaration organisationnelle
La mission de MIT Press est de se positionner à l’avant-garde en repoussant les limites de l’édition universitaire en partenariat actif avec la communauté du MIT et en accord avec la mission du MIT de faire progresser les connaissances dans les domaines de la science, de la technologie, des arts et d’autres domaines d’études qui serviront le mieux la nation et le monde au XXIe siècle. Cette mission a guidé et propulsé l’engagement de longue date de MIT Press en faveur de l’innovation et de l’expérimentation et a permis à la presse d’occuper une position de leader dans le monde de la presse universitaire. MIT Press partage les perspectives et les préoccupations de nombreux membres de la communauté de la presse universitaire, et c’est pourquoi nous pensons que nous partagerons une perspective précieuse au sein du conseil. Très à l’écoute des besoins des presses universitaires à tous les niveaux, MIT Press partage bon nombre de leurs objectifs : développer l’utilisation des DOI pour tous les membres tout en expérimentant de nouvelles initiatives telles que la publication d’examens par les pairs et l’établissement de liens entre les preprints, les examens par les pairs et les articles finis que nous publions.

MIT Press est membre de longue date de Crossref et connaît bien les offres de services de Crossref. Nous travaillerons en étroite collaboration avec d’autres presses universitaires afin de faire part de leurs points de vue et de leurs besoins au conseil d’administration et nous serons ravis de contribuer à l’amélioration des services existants et au développement de nouveaux services. Nous sommes une organisation très expérimentale, et dans cet esprit nous espérons apporter des idées nouvelles à Crossref qui pourraient aider à lancer les communications savantes dans de nouvelles directions pour le bénéfice de tous.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração pessoal do candidato
Trabalho na área editorial acadêmica há quase vinte anos e sou responsável pela direção estratégica, pelo desempenho da divisão de periódicos acadêmicos da MIT Press e pelos esforços de acesso aberto aos livros e periódicos. Anteriormente, atuei como substituto no conselho da Crossref, fiz parte de comitês da Crossref e participei regularmente de eventos da empresa. Tudo isso me preparou para dar o próximo passo, e seria uma honra exercer um mandato completo no conselho.

Em julho de 2020, o conselho da Crossref aprovou uma moção para “[...] liderar proativamente um esforço para determinar, com outras organizações e iniciativas da infraestrutura, como podemos melhorar o ecossistema de pesquisa acadêmica”. Esse empolgante desdobramento fundamenta a visão ampla e abrangente em relação às comunicações acadêmicas que é crucial para a missão da Crossref. Acredito que não há nenhuma outra organização mais bem preparada ou adequada para concretizar essa visão. A Crossref encontra-se no ponto central da rede de comunicações acadêmicas e tem, mais do que qualquer outra organização, a capacidade de estabelecer e fortalecer as conexões com todos os principais interessados. O sucesso nesse empreendimento possibilitará novas formas de pesquisa e descoberta e permitirá o surgimento de um quadro mais completo da amplitude e profundidade do panorama de comunicações acadêmicas. O enfrentamento desses desafios será muito significativo e gratificante. Sinceramente desejo empregar minha energia na concretização dessa visão.

### Declaração da organização
A missão da MIT Press é liderar ampliando as fronteiras do mercado editorial acadêmico em uma parceria ativa com a comunidade do MIT e em alinhamento à missão do MIT, que é fazer avançar o conhecimento na ciência, na tecnologia, nas artes e em outras áreas acadêmicas que prestarão melhores serviços ao país e ao mundo no século XXI. Essa missão guia e impulsiona o antigo compromisso da MIT Press com a inovação e a experimentação e colocou a empresa em uma posição de liderança no mundo editorial universitário. A MIT Press compartilha a perspectiva e as preocupações de muitos membros da comunidade editorial acadêmica. Por isso, acreditamos que devemos compartilhar uma perspectiva valiosa no conselho. A empresa conhece muito bem as necessidades de editoras acadêmicas de todos os níveis e tem muitos objetivos em comum com elas: ampliar a utilização de DOIs por todos os membros e, ao mesmo tempo, vivenciar novas iniciativas, como publicar revisões por pares e estabelecer conexões entre pré-impressões, revisões por pares e os artigos finalizados que publicamos.

A MIT Press é membro da Crossref há muito tempo e está plenamente familiarizada com as ofertas de serviços da empresa. Trabalharíamos em conjunto com outras editoras universitárias para expressar suas perspectivas e seus requisitos ao conselho e contribuiríamos para o aprimoramento dos serviços existentes e o desenvolvimento de novos serviços. Somos uma organização altamente experimental e, em conformidade com essa característica, esperamos apresentar à Crossref novas ideias que possam ajudar a traçar novos rumos para as comunicações acadêmicas, para benefício de todos.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración personal del candidato
He trabajado en el sector de las publicaciones académicas durante casi veinte años y soy responsable de la dirección estratégica y el rendimiento del departamento de revistas académicas de MIT Press y de los esfuerzos de acceso abierto tanto en libros como en revistas. Anteriormente, he sido suplente en la junta directiva de Crossref, he formado parte de los comités de Crossref y asisto con regularidad a los eventos de Crossref, todo lo cual me ha preparado para dar el siguiente paso y sería un honor para mí cumplir un mandato completo en la junta directiva.

En julio de 2020, la junta directiva de Crossref aprobó una moción para «… liderar proactivamente un esfuerzo para explorar, con otras organizaciones e iniciativas de infraestructura, cómo podemos mejorar el ecosistema de la investigación académica». Este desarrollo tan emocionante subyace en el tipo de visión amplia y abarcadora de las comunicaciones académicas que es realmente el núcleo de la misión de Crossref, y no se me ocurre ninguna otra organización que esté mejor posicionada o sea más adecuada para hacerla realidad. Crossref es el nodo central de la red de comunicaciones académicas y tiene la capacidad de establecer y fortalecer las conexiones con todas las partes interesadas clave como ninguna otra organización. El éxito en este sentido potenciará nuevas formas de investigación y descubrimiento y permitirá obtener una imagen mucho más completa de la amplitud y profundidad del panorama de las comunicaciones académicas. Trabajar en estos retos sería muy significativo y gratificante y me gustaría dedicar mi energía a cumplir esta visión.

### Declaración de la organización
La misión de MIT Press es liderar el avance de los límites de las publicaciones académicas en colaboración activa con la comunidad de MIT, y en consonancia con la misión de MIT de avanzar en el conocimiento de la ciencia, la tecnología, las artes y otras áreas académicas que servirán mejor a la nación y al mundo en el siglo XXI. Esta misión ha guiado e impulsado el largo compromiso de MIT Press con la innovación y la experimentación y ha situado a la editorial en una posición de liderazgo en el mundo editorial universitario. MIT Press comparte la perspectiva y las preocupaciones de muchos miembros de la comunidad editorial universitaria, y por ello creemos que compartiríamos una valiosa perspectiva en la junta directiva. La editorial está muy en sintonía con las necesidades de las editoriales universitarias de todos los niveles y comparte muchos de los mismos objetivos: ampliar el uso de los DOI para todos los miembros y, al mismo tiempo, experimentar con nuevas iniciativas como la publicación de revisiones por pares y establecer conexiones entre las preimpresiones, las revisiones por pares y los artículos terminados que publicamos.

MIT Press es miembro de Crossref desde hace mucho tiempo y está muy familiarizado con la oferta de servicios de Crossref. Trabajaríamos estrechamente con otras editoriales universitarias para aportar sus perspectivas y necesidades a la junta directiva y estaríamos dispuestos a contribuir a la mejora de los servicios existentes y al desarrollo de otros nuevos. Somos una organización muy experimental, y con ese espíritu esperamos aportar a Crossref ideas novedosas que puedan ayudar a impulsar las comunicaciones académicas en nuevas direcciones en beneficio de todos.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}


### 후보자의 개인 소개
저는 거의 20년 동안 학술 출판 분야에서 일했으며, MIT Press 학술지 부서의 전략적 방향과 성과와 도서와 저널에 대한 오픈 액세스 노력을 책임지고 있습니다. 이전에는 Crossref 이사회의 대리인으로 일했고, Crossref 위원회에서 봉사했으며, 정기적으로 Crossref 행사에 참석했습니다. 이 모든 것들이 제가 다음 단계를 밟을 수 있도록 준비시켜 주었고, 이사회에서 전체 임기를 수행할 수 있게 된 것을 영광으로 생각합니다.

2020년 7월, Crossref 이사회는 "...다른 인프라 조직 및 이니셔티브와 함께 학술 연구 생태계를 개선할 수 있는 방법을 모색하기 위한 노력을 능동적으로 주도한다"라는 동의안을 통과시켰습니다. 이 매우 흥미진진한 발전은 Crossref 사명의 핵심인 학술 커뮤니케이션에 대한 광범위하고 포괄적인 비전의 기초가 되고 있습니다. 이를 실현하기에 더 나은 위치에 있거나 적합한 조직은 없습니다. Crossref는 학술 커뮤니케이션 네트워크의 중심 노드로 언급되며 다른 어떤 조직에서도 볼 수 없는 모든 주요 이해 관계자들과의 연결을 설정하고 강화할 수 있는 능력을 가지고 있습니다. 여기에서 성공하면 새로운 형태의 연구와 발견에 힘을 실어주고 학술 커뮤니케이션 환경의 폭과 깊이에 대한 훨씬 더 완전한 그림을 보여줄 것입니다. 이러한 도전은 매우 의미 있고 보람이 있는 일이 될 것이고 저는 이 비전을 실현하기 위해 제 에너지를 바칠 수 있기를 기대합니다.

### 단체 소개
MIT Press의 사명은 MIT 커뮤니티와 적극적인 파트너십을 통해 학술 출판의 경계를 확장하고 21세기에 국가와 세계에 가장 잘 봉사할 과학, 기술, 예술 및 기타 학문 분야의 지식을 발전시키려는 MIT의 사명과 일치하도록 이끄는 것입니다. 이 임무는 혁신과 실험에 대한 MIT Press의 오랜 약속을 이끌고 추진했으며 대학 언론계에서 선도적인 위치에 서게 해주었습니다. MIT Press는 대학 언론계의 많은 구성원들의 전망과 우려를 공유하고 있으며, 이 때문에 우리는 이사회에서 귀중한 관점을 공유하게 될 것이라고 믿습니다. Press는 모든 계층에 걸쳐 대학 언론의 요구에 매우 민감하며, 모든 구성원에 대한 DOI의 사용을 확대하는 동시에 동료 리뷰와 같은 새로운 이니셔티브를 실험하고 우리가 발행하는 사전 인쇄물, 동료 리뷰 및 완성된 기사 간의 연결을 확립합니다.

MIT Press는 오랫동안 Crossref 회원이었으며 Crossref에서 제공하는 서비스에 대해 깊이 알고 있습니다. 우리는 다른 대학 언론과 긴밀히 협력하여 그들의 관점과 요구 사항을 이사회에 전달하고 기존 서비스의 향상과 새로운 서비스의 개발에 기여하기를 열망합니다. 우리는 매우 실험적인 조직이고, 그러한 정신으로 우리는 모든 사람들을 위해 학문적 소통을 새로운 방향으로 추진하는데 도움을 줄 수 있는 새로운 아이디어를 Crossref에 가져오기를 희망합니다.



{{% /accordion %}}
{{% /accordion-section %}}

---

<a id="springer"></a>
## Anjalie Nawaratne, Springer Nature, UK<br>

<img src="/images/board-governance/anjalie_nawaratne_1548162993.png" alt="Scott Delman, Association for Computing Machinery (ACM)" width="150px" class="img-responsive" />

{{% accordion %}}
{{% accordion-section "in English" %}}

### Personal statement

Crossref continues to grow through its vast network of 16,000 members and 30 ambassadors across 140 countries. Through this network Crossref is able to have a vast impact, across boundaries, to make research objects easy to find, cite, link, assess, and reuse. Crossref’s mission is clear. The complexity and magnitude of the mission is understood but the approach is simple. Through strategies of Rally, Tag, Run, Play and Make, Crossref is able to move their mission forward in very tangible and impactful ways.

The world of scholarly publishing is experiencing change as business models shift and we move towards Open Science. In an ever changing and interdependent ecosystem the Cross-ref Community will continue to connect, learn and guide people across the industry. Ensuring that the services and solutions created are contextually suitable for the now and the future.

Who am I? I’m an enthusiastic digital specialist with extensive, hands-on experience in building products/services and leading teams/divisions through transformation. I help organizations respond to change and capitalise on technology developments such as Digital, Data, AI and Customer Centricity, by building effective, efficient and resilient capabilities that are flexible and can weather change.  Currently I am responsible for the data transformation at Springer Nature. Working across the organization to rally data producers and consumers together to build a culture of trust, collaboration and innovation.  I believe what I bring to the community is my knowledge and experience of technology and data within publishing, compassion for an ever changing landscape and  a thirst for learning and working with others.

### Organizational statement <br>

The connection between Springer Nature and Crossref has been long established. Springer Nature is one of the founding members of Crossref and has been a very active and loyal board member ever since. Being a large academic publisher Springer Nature is a custodian of a large corpus of content and understands its responsibility to ensure the long-term digital preservation of content. Supporting Crossref projects that lead and develop how consistently find-able content is, is a key reason for Springer Nature to be active on the board.

Springer Nature is truly a global organization. Working in Springer Nature, managing international teams and living around the world I have been fortunate to be exposed to many perspectives and cultures. I would aim to bring a compassionate perspective to discussions to ensure that we serve the wider Crossref community.

Our internal mission around data has similar principles to that of Crossref and is centered around supporting and enabling a community to create opportunities and create new ways forward. Through leading this initiative at Springer Nature I believe I could contribute to the board our very practical lessons learned as an organization operating in a similar context but also learn and propagate best practice.

Springer Nature is committed to work together with Crossref and its membership to tag, run and play to improve the scholarly communications landscape.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration personnelle de la candidate

Crossref continue de se développer grâce à son vaste réseau constitué de 16 000 membres et 30 ambassadeurs dans 140 pays. Grâce à ce réseau, Crossref est en mesure d’avoir un impact considérable, au-delà des frontières, pour faciliter la recherche, la citation, la liaison, l’évaluation et la réutilisation des objets de recherche. La mission de Crossref est claire. La complexité et l’ampleur de la mission sont comprises, mais l’approche est simple. Grâce à des stratégies telles que Rallye, Tag, Run, Play et Make, Crossref est en mesure de faire avancer sa mission de manière très concrète et efficace.

Le monde de l’édition universitaire est en pleine mutation, les modèles économiques évoluent et nous nous dirigeons vers la science ouverte. Dans un écosystème interdépendant et en constante évolution, la communauté Cross-ref continuera de connecter, d’apprendre et de guider les gens à travers l’industrie. Elle veillera à ce que les services et les solutions créés soient adaptés au contexte d’aujourd’hui et de demain.

Qui suis-je ? Je suis une spécialiste du numérique enthousiaste, dotée d’une vaste expérience pratique dans la création de produits/services et la conduite d’équipes/divisions dans le cadre de transformations. J’aide les organisations à répondre au changement et à capitaliser sur les développements technologiques tels que le numérique, les données, l’IA et la primauté du client, en construisant des capacités efficaces, efficientes et résilientes qui sont flexibles et peuvent résister au changement.  Actuellement, je suis responsable de la transformation des données chez Springer Nature. Je travaille dans toute l’organisation pour rallier les producteurs et les consommateurs de données afin de construire une culture de confiance, de collaboration et d’innovation.  Je pense que ce que j’apporte à la communauté est ma connaissance et mon expérience de la technologie et des données dans le domaine de l’édition, ma compassion pour un paysage en constante évolution et ma soif d’apprendre et de travailler avec les autres.

### Déclaration organisationnelle

Le lien entre Springer Nature et Crossref est établi depuis longtemps. Springer Nature est l’un des membres fondateurs de Crossref et c’est depuis toujours un membre très actif et loyal du conseil d’administration. En tant que grand éditeur universitaire, Springer Nature est le gardien d’un large corpus de contenu et comprend sa responsabilité d’assurer la préservation numérique à long terme du contenu. Le soutien des projets de Crossref, qui permettent d’améliorer la facilité de recherche du contenu, est une raison essentielle pour Springer Nature de participer activement au conseil d’administration.

Springer Nature est véritablement une organisation mondiale. En travaillant chez Springer Nature, en gérant des équipes internationales et en vivant aux quatre coins du monde, j’ai eu la chance d’être exposée à de nombreuses perspectives et cultures. Je m’efforcerais d’apporter une perspective compatissante aux discussions afin de garantir que nous servons la communauté Crossref au sens large.

Notre mission interne autour des données a des principes similaires à ceux de Crossref et est centrée sur le soutien et l’habilitation d’une communauté à créer des opportunités et de nouvelles voies. En menant cette initiative chez Springer Nature, je pense pouvoir apporter au conseil d’administration les leçons très pratiques que nous avons tirées en tant qu’organisation opérant dans un contexte similaire, mais aussi apprendre et propager les meilleures pratiques.

Springer Nature s’engage à collaborer avec Crossref et ses membres pour marquer, exécuter et jouer afin d’améliorer le paysage des communications savantes.



{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração pessoal da candidata

A Crossref continua crescendo por meio de sua vasta rede de 16.000 membros e 30 embaixadores em 140 países. Por meio dessa rede, a Crossref pode causar um grande impacto, que atravessa fronteiras, para facilitar a localização, citação, vinculação, avaliação e reutilização de objetos de pesquisa. A missão da Crossref é clara. A complexidade e magnitude da missão são bem compreendidas, mas a abordagem é simples. Adotando as estratégias de Rally (unir), Tag (marcar), Run (operar), Play (experimentar) e Make (criar), a Crossref consegue cumprir sua missão de forma tangível e impactante.

O mundo editorial acadêmico está passando por uma transformação devido à mudança dos modelos e à adoção gradual da Ciência Aberta. Em um ecossistema interdependente e em constante mudança, a comunidade da Crossref continuará a conectar, aprender e guiar as pessoas do setor. Para garantir que os serviços e soluções criados sejam adequados para o presente e o futuro.

Quem sou eu? Sou uma especialista digital cheia de entusiasmo, com ampla experiência prática no desenvolvimento de produtos/serviços e na liderança de equipes/divisões por meio da transformação. Ajudo as organizações a responder à mudança e aproveitar os desenvolvimentos tecnológicos, como Digital, Dados, IA e Centricidade do Cliente, desenvolvendo recursos eficazes, eficientes e resilientes que são flexíveis e sobrevivem à mudança.  No momento, sou responsável pela transformação de dados na Springer Nature. Trabalho com toda a organização para unir produtores e consumidores de dados com o objetivo de desenvolver uma cultura de confiança, colaboração e inovação.  Acredito que posso oferecer à comunidade o meu conhecimento e experiência com tecnologia e dados dentro do mercado editorial, a compaixão em um panorama em constante mudança e a sede de aprender e trabalhar com outras pessoas.

### Declaração da organização

A relação entre a Springer Nature e a Crossref é bastante antiga. A Springer Nature é um dos membros fundadores da CrossRef e tem sido um membro do Conselho bastante ativo e leal desde então. Por ser uma editora acadêmica de grande porte, a Springer Nature é a guardiã de um grande corpus de conteúdo e entende a sua responsabilidade na garantia da preservação digital do conteúdo em longo prazo. O apoio aos projetos da Crossref que lideram e desenvolvem a possibilidade de localização do conteúdo de forma consistente é um dos principais motivos que levam a Springer Nature a ser ativa no conselho.

A Springer Nature é uma organização verdadeiramente global. Ao trabalhar na Springer Nature, gerenciar equipes internacionais e morar em várias partes do mundo, tive a sorte de conhecer muitas perspectivas e culturas. Meu objetivo seria levar uma perspectiva humanitária às discussões, para garantir que a comunidade da Crossref seja beneficiada.

Nossa missão interna em relação aos dados tem princípios semelhantes aos da Crossref: baseia-se no apoio à comunidade e em sua capacitação para criar oportunidades e novas formas de avançar. Ao liderar essa iniciativa na Springer Nature, acredito que posso oferecer ao conselho as lições muito práticas que aprendemos na qualidade de organização que opera em um contexto semelhante e também aprender e propagar boas práticas.

A Springer Nature tem o compromisso de trabalhar em conjunto com a Crossref e seus membros para adotar a abordagem tag (marcar), run (operar) e play (experimentar), com o objetivo de melhorar o panorama das comunicações acadêmicas.



{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración personal de la candidata

Crossref sigue creciendo a través de su amplia red de 16 000 miembros y 30 embajadores en 140 países. A través de esta red, Crossref es capaz de tener un amplio impacto, más allá de las fronteras, para hacer que los objetos de investigación sean fáciles de encontrar, citar, vincular, evaluar y reutilizar. La misión de Crossref está bien definida. Se entiende la complejidad y la magnitud de la misión, pero el enfoque es sencillo. A través de las estrategias de Rally, Tag, Run, Play y Make (agrupar, etiquetar, ejecutar, reproducir y crear), Crossref es capaz de hacer avanzar su misión de manera muy tangible e impactante.

El mundo de las publicaciones académicas está experimentando un cambio a medida que los modelos de negocio cambian y avanzamos hacia la Ciencia abierta. En un ecosistema siempre cambiante e interdependiente, la Comunidad Crossref seguirá conectando, aprendiendo y guiando a las personas del sector. Garantizando que los servicios y soluciones creados sean contextualmente adecuados para el presente y el futuro.

¿Quién soy yo? Soy una especialista digital entusiasta con amplia experiencia práctica en la creación de productos/servicios y en la dirección de equipos/departamentos a través de la transformación. Ayudo a las organizaciones a responder al cambio y a sacar provecho de los avances tecnológicos, como la centricidad en la tecnología digital, los datos, la inteligencia artificial y el cliente, mediante la creación de capacidades eficaces, eficientes y adaptables que sean flexibles y puedan afrontar el cambio.  Actualmente soy responsable de la transformación de datos en Springer Nature. Trabajando en toda la organización para reunir a los productores y consumidores de datos para construir una cultura de confianza, colaboración e innovación.  Creo que lo que aporto a la comunidad es mi conocimiento y experiencia en tecnología y datos dentro del sector editorial, la compasión por un mundo en constante cambio y la sed de aprender y trabajar con los demás.

### Declaración de la organización

La conexión entre Springer Nature y Crossref se estableció desde hace mucho tiempo. Springer Nature es uno de los miembros fundadores de Crossref y ha sido un miembro muy activo y leal de la junta directiva desde entonces. Al ser una gran editorial académica, Springer Nature custodia un gran corpus de contenidos y es consciente de su responsabilidad de garantizar la conservación digital de los mismos a largo plazo. Apoyar los proyectos de Crossref, que lideran y desarrollan cómo se puede encontrar el contenido de forma coherente, es una razón clave para que Springer Nature participe activamente en la junta directiva.

Springer Nature es realmente una organización global. Al trabajar en Springer Nature, dirigir equipos internacionales y vivir alrededor del mundo, he tenido la suerte de conocer muchas perspectivas y culturas. Mi objetivo es aportar una perspectiva empática a los debates para garantizar que sirvamos a la comunidad de Crossref en general.

Nuestra misión interna en torno a los datos tiene principios similares a los de Crossref y se centra en apoyar y permitir a una comunidad crear oportunidades y nuevas formas de avanzar. A través del liderazgo de esta iniciativa en Springer Nature, creo que podría aportar a la junta directiva nuestras lecciones de carácter práctico aprendidas como una organización que opera en un contexto similar, además de aprender y divulgar las mejores prácticas.

Springer Nature se compromete a trabajar junto con Crossref y sus miembros para identificar, ejecutar y participar en la mejora del panorama de las comunicaciones académicas.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}


### 후보자의 개인 소개

Crossref는 140개국에 걸쳐 16,000명의 회원과 30명의 대사들로 구성된 방대한 네트워크를 통해 계속 성장하고 있습니다. 이 네트워크를 통해 Crossref는 연구 대상을 쉽게 찾고, 인용하고, 연결하고, 평가하고, 재사용할 수 있도록 경계를 넘어 막대한 영향을 미칠 수 있습니다. Crossref의 사명은 분명합니다. 임무의 복잡성과 규모는 이해되지만 접근 방식은 간단합니다. Rally, Tag, Run, Play 및 Make의 전략을 통해 Crossref는 매우 구체적이고 임팩트 있는 방식으로 사명을 추진할 수 있습니다.

학술 출판의 세계는 비즈니스 모델이 오픈 사이언스로 이동함에 따라 변화를 경험하고 있습니다. 끊임없이 변화하고 상호 의존적인 생태계에서 Crossref 커뮤니티는 업계 전반에 걸쳐 사람들을 계속 연결하고 학습하고 안내할 것입니다. 생성된 서비스와 솔루션이 현재와 미래에 상황에 맞는지 확인합니다.

나는 누구인가? 저는 제품/서비스를 구축하고 혁신을 통해 팀/사업부를 이끄는 광범위한 실무 경험을 가진 열정적인 디지털 전문가입니다. 저는 유연하고 탄력적이며 변화를 극복할 수 있는 효과적이고 효율적이며 탄력적인 기능을 구축하여 조직이 변화에 대응하고 디지털, 데이터, AI 및 고객 중심성과 같은 기술 개발에 활용할 수 있도록 지원합니다.  현재 저는 Springer Nature에서 데이터 변환을 담당하고 있습니다. 조직 전반에서 데이터 생산자와 소비자를 결집하여 신뢰, 협업 및 혁신의 문화를 구축합니다.  제가 커뮤니티에 제공하는 것은 출판 분야의 기술과 데이터에 대한 지식과 경험, 끊임없이 변화하는 환경에 대한 연민, 사람들과 함께 배우고 일하려는 갈증이라고 생각합니다.

### 단체 소개

Springer Nature와 Crossref 사이의 소통은 오랜 기간에 걸쳐 구축되었습니다. Springer Nature는 Crossref의 창립 회원 중 한 곳이며 그 이후로 매우 활동적이고 충실한 이사회 회원이었습니다. 대규모 학술 출판사인 Springer Nature는 방대한 콘텐츠의 관리인이며 콘텐츠의 장기적인 디지털 보존을 보장해야 하는 책임을 이해하고 있습니다. 콘텐츠를 지속적으로 찾을 수 있도록 이끌고 개발하는 Crossref 프로젝트를 지원하는 것은 Springer Nature가 이사회에서 활동하는 주요 이유입니다.

Springer Nature는 진정한 글로벌 조직입니다. Springer Nature에서 일하고, 국제 팀을 관리하고, 전 세계에 살면서 많은 관점과 문화를 접할 수 있었던 것은 행운이었습니다. 저는 우리가 더 넓은 Crossref 커뮤니티에 봉사할 수 있도록 토론에 동정적인 관점을 가져오는 것을 목표로 합니다.

데이터에 대한 우리의 내부 사명은 Crossref와 유사한 원칙을 가지고 있으며 커뮤니티가 기회를 창출하고 새로운 방법을 만들 수 있도록 지원하는 데 중점을 두고 있습니다. Springer Nature에서 이 이니셔티브를 이끌면서 저는 유사한 맥락에서 운영되는 조직으로서 배운 매우 실용적인 교훈을 이사회에 기여할 수 있을 뿐만 아니라 모범 사례를 배우고 전파할 수 있다고 생각합니다.

Springer Nature는 학술적 커뮤니케이션 환경을 개선하기 위해 Crossref 및 회원과 협력하여 태그를 지정하고 실행하고 운용하기 위해 최선을 다하고 있습니다.



{{% /accordion %}}
{{% /accordion-section %}}

---

<a id="wiley"></a>
## Allyn Molina, Wiley, US<br>

<img src="/images/board-governance/allyn-molina.jpg" alt="Allyn Molina, Wiley" width="150px" class="img-responsive" />

{{% accordion %}}
{{% accordion-section "in English" %}}

### Personal statement

I am currently Group Vice President for Wiley’s Research Publishing business, where I work across business units and publishing partners to develop and implement the strategic growth of Wiley’s diverse journal portfolio, many from prestigious learned societies. The pace of the market and forces that drive it have exposed the critical role that industry collaboration has to play for those compelled to cope with the increasing complexity laid bare in an open access world.

The scholarly community is wrestling with some urgent existential questions about the value we deliver in the new era of publishing. There is increasing need to extract more value from scholarly communications for a growing variety of open research outputs. And this is true whether you are a large commercial publisher or a small, learned society publisher. A core strength of Crossref is working across publishers of all shapes and sizes to develop and advance the underlying infrastructure that drives discovery and facilitates the widest possible adoption.

I have worked in scholarly publishing for almost 20 years, first at Elsevier where I got my start managing backend publishing processes. Since joining Wiley in 2013, I have held a variety of roles with increasing senior-level responsibility. As Publishing Director, I oversaw the journals team in North America before going on secondment to establish our team in India. I have also held the role of Senior Director, Global Sales, where I lead the launch of Wiley’s newest digital database product.

I love working across customers in different markets and segments of the publishing landscape and I believe that collaboration will make it possible for us to meet a wide variety of business models and needs.  I would be honored to serve on the board and bring my energy, insights, and ideas to help us address our current challenges and opportunities.



### Organizational statement <br>

For more than 200 years Wiley has helped people and organizations develop the skills and knowledge they need to unlock human potential.  With more than 7,000 colleagues in 40+ countries, Wiley develops digital education, learning, assessment, and certification solutions to help universities, businesses, and individuals move between education and employment and to achieve their ambitions.  Our online scientific, technical, medical, and scholarly journals, books, and other digital content build on a heritage of high-quality publishing.

Crossref occupies a central position in the research publishing ecosystem and is an indispensable partner to researchers, funders, and scholarly publishers globally.  As a founding member of Crossref, Wiley is committed to helping Crossref as an organization meet its goals and objectives. We feel that Crossref is in a unique position to help content providers and other stakeholders meet the demands of scholarly infrastructure in a market that is increasingly open.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration personnelle de la candidate
Je suis actuellement vice-présidente du groupe pour l’activité d’édition de recherche de Wiley, où je travaille avec les unités commerciales et les partenaires d’édition pour développer et mettre en œuvre la croissance stratégique du portefeuille diversifié de revues de Wiley, dont beaucoup proviennent de sociétés savantes prestigieuses. Le rythme du marché et les forces qui l’animent ont mis en évidence le rôle essentiel que la collaboration du secteur doit jouer pour ceux contraints de faire face à la complexité croissante d’un monde en libre accès.

La communauté universitaire est confrontée à des questions existentielles urgentes sur la valeur que nous offrons dans la nouvelle ère de l’édition. Extraire davantage de valeur des communications savantes pour une variété croissante de résultats de recherche en libre accès s’avère en outre de plus en plus nécessaire. Et cela est vrai pour le grand éditeur commercial comme le petit éditeur de société savante. L’une des principales forces de Crossref est de travailler avec des éditeurs de toutes formes et de toutes tailles pour développer et faire progresser l’infrastructure sous-jacente qui favorise la découverte et facilite l’adoption la plus large possible.

Je travaille dans l’édition universitaire depuis près de 20 ans, d’abord chez Elsevier où j’ai fait mes premières armes dans la gestion des processus d’édition backend. Depuis que j’ai rejoint Wiley en 2013, j’ai occupé diverses fonctions avec des responsabilités de plus en plus importantes. En tant que directrice d’édition, j’ai supervisé l’équipe des revues en Amérique du Nord avant d’être détachée pour mettre en place notre équipe en Inde. J’ai également occupé le poste de directrice principale des ventes mondiales, où j’ai dirigé le lancement du tout nouveau produit de base de données numériques de Wiley.

J’aime travailler avec des clients de différents marchés et segments du paysage de l’édition et je pense que la collaboration nous permettra de répondre à une grande variété de modèles commerciaux et de besoins.  Je serais honorée de siéger au conseil d’administration et d’apporter mon énergie, mes idées et mes réflexions pour nous aider à relever les défis actuels et à saisir les opportunités.

### Déclaration organisationnelle

Depuis plus de 200 ans, Wiley aide les personnes et les organisations à développer les compétences et les connaissances dont elles ont besoin pour libérer le potentiel humain.  Avec plus de 7 000 collègues dans plus de 40 pays, Wiley développe des solutions numériques d’éducation, d’apprentissage, d’évaluation et de certification pour aider les universités, les entreprises et les individus à passer de l’éducation à l’emploi et à réaliser leurs ambitions.  Nos revues scientifiques, techniques, médicales et académiques en ligne, nos livres et autres contenus numériques reposent sur un héritage d’édition de haute qualité.

Crossref occupe une position centrale dans l’écosystème de l’édition de recherche et c’est un partenaire indispensable pour les chercheurs, les bailleurs de fonds et les éditeurs savants du monde entier.  En tant que membre fondateur de Crossref, Wiley s’engage à aider Crossref en tant qu’organisation à atteindre ses buts et objectifs. Nous pensons que Crossref se trouve dans une position unique pour aider les fournisseurs de contenu et les autres parties prenantes à répondre aux exigences de l’infrastructure scientifique dans un marché qui est de plus en plus ouvert.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração pessoal da candidata
No momento, sou vice-presidente de grupo da Wiley’s Research Publishing, onde trabalho com várias unidades de negócios e parceiros editores para desenvolver e implementar o crescimento estratégico do diversificado portfólio de periódicos da Wiley, muitos dos quais procedem de prestigiosas sociedades acadêmicas. A velocidade do mercado e as forças que o impulsionam revelaram a função crítica que a colaboração do setor deve exercer para aqueles que querem lidar com a complexidade cada vez maior existente em um mundo de acesso aberto.

A comunidade acadêmica enfrenta questões existenciais urgentes relacionadas ao valor que geramos na nova era do mercado editorial. Há uma necessidade cada vez maior de extrair mais valor das comunicações acadêmicas para uma crescente variedade de produtos de pesquisa aberta. Isso vale tanto para grandes editoras comerciais quanto para pequenas editoras de sociedades acadêmicas. Um dos principais pontos fortes da Crossref é o trabalho com os mais diversos tipos de editoras, de variados portes, para desenvolver e fazer avançar a infraestrutura subjacente que impulsiona a descoberta e facilita a adoção mais ampla possível.

Trabalho há quase 20 anos na área editorial acadêmica, tendo iniciado na Elsevier, onde gerenciei processos administrativos da área editorial. Ingressei na Wiley em 2013 e, desde então, desempenhei diversas funções, com responsabilidade crescente de nível sênior. Como diretora editorial, supervisionei a equipe de periódicos na América do Norte e, posteriormente, trabalhei em regime de destacamento para estabelecer a nossa equipe na Índia. Também desempenhei as funções de diretora sênior de vendas globais, na qual liderei o lançamento do mais recente produto de banco de dados digital da Wiley.

Adoro trabalhar com clientes de diversos mercados e segmentos do panorama editorial e acredito que a colaboração possibilitará o trabalho com vários modelos de negócios e o atendimento de diversas necessidades.  Seria uma honra fazer parte do conselho e empregar minha energia, meus insights e minhas ideias para ajudar a superar os desafios atuais e aproveitar oportunidades.

### Declaração da organização

Há mais de 200 anos, a Wiley vem ajudando pessoas e organizações a desenvolver as habilidades e o conhecimento necessários para liberar o potencial humano.  Com mais de 7.000 colegas em mais de 40 países, a Wiley desenvolve soluções de educação digital, aprendizagem, avaliação e certificação para ajudar universidades, empresas e pessoas a fazer a transição entre a educação e o emprego e realizar suas ambições.  Nossos periódicos, livros e outros formatos de conteúdo digital online nas áreas cientifica, médica e acadêmica têm como base uma tradição de alta qualidade editorial.

A Crossref ocupa uma posição central no ecossistema editorial acadêmico e é um parceiro indispensável para pesquisadores, financiadores e editoras acadêmicas do mundo todo.  Na qualidade de membro fundador da Crossref, a Wiley tem o compromisso de ajudar a Crossref, como organização, a cumprir suas metas e objetivos. Acreditamos que a Crossref ocupa uma posição privilegiada e pode ajudar os fornecedores de conteúdo e outros interessados a atender as demandas da infraestrutura acadêmica em um mercado cada vez mais aberto.


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración personal de la candidata
En la actualidad soy vicepresidenta del Grupo de publicaciones de investigación de Wiley, donde trabajo con las unidades comerciales y los socios editoriales para desarrollar e implementar el crecimiento estratégico de la diversa cartera de revistas de Wiley, muchas de ellas de prestigiosas sociedades científicas. El ritmo del mercado y las fuerzas que lo impulsan han puesto de manifiesto el papel fundamental que debe desempeñar la colaboración en el sector para quienes se ven obligados a hacer frente a la creciente complejidad que pone de manifiesto un mundo de acceso abierto.

La comunidad académica está luchando con algunas cuestiones existenciales urgentes sobre el valor que ofrecemos en la nueva era de la industria editorial. Cada vez es más necesario extraer más valor de las comunicaciones académicas para una creciente variedad de resultados de la investigación abierta. Y esto es así tanto si se trata de una gran editorial comercial como de una pequeña editorial de una sociedad científica. Uno de los puntos fuertes de Crossref es la colaboración con editoriales de todas las formas y tamaños para desarrollar y avanzar en la infraestructura subyacente que impulsa el descubrimiento y facilita la adopción más amplia posible.

Llevo casi 20 años trabajando en el sector de la edición académica, primero en Elsevier, donde me inicié en la gestión de los procesos editoriales de apoyo. Desde que me incorporé a Wiley en 2013, he desempeñado diversas funciones con creciente responsabilidad de alto nivel. Como directora de publicaciones, supervisé el equipo de revistas en Norteamérica antes de ir en comisión de servicio para establecer nuestro equipo en la India. También he desempeñado el papel de directora superior de ventas globales, donde dirigí el lanzamiento del novedoso producto de base de datos digital de Wiley.

Me encanta trabajar con clientes de diferentes mercados y segmentos del panorama editorial y creo que la colaboración nos permitirá satisfacer una gran variedad de modelos de negocio y necesidades.  Sería un honor para mí formar parte de la junta directiva y aportar mi energía, mis conocimientos y mis ideas para ayudarnos a afrontar nuestros retos y oportunidades actuales.

### Declaración de la organización

Durante más de 200 años, Wiley ha ayudado a las personas y a las organizaciones a desarrollar las habilidades y los conocimientos que necesitan para liberar el potencial humano.  Con más de 7000 colaboradores en más de 40 países, Wiley desarrolla soluciones digitales de educación, aprendizaje, evaluación y certificación para ayudar a las universidades, las empresas y los individuos a moverse entre la educación y el empleo y a lograr sus aspiraciones.  Nuestras revistas científicas, técnicas, médicas y académicas en línea, así como nuestros libros y otros contenidos digitales, se basan en una herencia de publicaciones de alta calidad.

Crossref ocupa una posición central en el ecosistema de las publicaciones de investigación y es un socio indispensable para los investigadores, financiadores y editores académicos de todo el mundo.  Como miembro fundador de Crossref, Wiley se compromete a ayudar a la organización Crossref a alcanzar sus metas y objetivos. Creemos que Crossref está en una posición única para ayudar a los proveedores de contenidos y a otras partes interesadas a satisfacer las demandas de infraestructura académica en un mercado cada vez más abierto.



{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}


### 후보자의 개인 소개
저는 현재 Wiley의 연구 출판 사업부의 그룹 부사장으로, 여러 사업부와 출판 파트너를 통해 일하며 저명한 학계에서 온 Wiley의 다양한 저널 포트폴리오의 전략적 성장을 개발하고 구현합니다. 시장의 속도와 이를 이끄는 원동력은 개방형 액세스 세계에서 점점 더 복잡해지는 상황에 대처해야 하는 사람들을 위해 업계 협력이 수행해야 하는 중요한 역할을 드러냈습니다.

학계는 출판의 새로운 시대에 우리가 제공하는 가치에 대한 몇 가지 긴급한 실존적 질문과 씨름하고 있습니다. 점점 더 다양해지는 공개 연구 결과를 위해 학술 커뮤니케이션에서 더 많은 가치를 추출해야 할 필요성이 증가하고 있습니다. 그리고 이것은 큰 상업 출판사이든 소규모의 학식 있는 사회 출판사이든 마찬가지입니다. Crossref의 핵심 강점은 모든 형태와 규모의 출판사에 걸쳐 발견을 주도하고 최대한 가장 광범위한 채택을 촉진하는 기본 인프라를 개발하고 발전시키기 위해 노력하고 있다는 것입니다.

저는 거의 20년 동안 학술 출판 분야에서 일했으며, 처음에는 Elsevier에서 백엔드 출판 프로세스 관리를 시작했습니다. 2013년 Wiley에 합류한 이후로 고위직 책임이 증가하면서 다양한 역할을 수행했습니다. 퍼블리싱 디렉터로서 저는 인도에 팀을 설립하기 위해 파견되기 전에 북미의 저널 팀을 감독했습니다. 또한 Wiley의 최신 디지털 데이터베이스 제품 출시를 이끄는 글로벌 영업 담당 선임 이사의 역할도 맡았습니다.

저는 출판 환경의 다양한 시장과 세그먼트에 있는 고객과 일하는 것을 좋아하며 협업을 통해 다양한 비즈니스 모델과 요구 사항을 충족할 수 있다고 생각합니다.  저는 이사회에서 일하고 현재의 당면 과제와 기회를 해결하는 데 도움이 되는 에너지, 통찰력 및 아이디어를 제공하는 것을 영광으로 생각합니다.

### 단체 소개

Wiley는 200년이 넘는 세월 동안 사람과 조직이 인간의 잠재력을 발휘하는 데 필요한 기술과 지식을 개발하도록 도왔습니다.  40개 이상의 국가에서 7,000명 이상의 동료들과 함께 Wiley는 대학, 기업 및 개인이 교육과 고용 사이를 이동하고 그들의 야망을 성취할 수 있도록 지원하는 디지털 교육, 학습, 평가 및 인증 솔루션을 개발합니다.  당사의 온라인 과학, 기술, 의학 및 학술 저널, 서적 및 기타 디지털 콘텐츠는 고품질 출판의 유산을 기반으로 합니다.

Crossref는 연구 출판 생태계에서 중심적인 위치를 차지하고 있으며 전 세계의 연구원, 자금 제공자 및 학술 출판인들에게 없어서는 안될 파트너입니다.  Crossref의 창립 멤버로서 Wiley는 조직으로서 Crossref가 목표와 목표를 달성할 수 있도록 돕기 위해 최선을 다하고 있습니다. 우리는 Crossref가 콘텐츠공급자와 기타 이해 관계들이 점점 더 개방적인 시장에서 학술 인프라의 요구 사항을 충족할 수 있도록 돕는 독특한 위치에 있다고 생각합니다.



{{% /accordion %}}
{{% /accordion-section %}}
