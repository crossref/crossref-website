+++
title = "Board election 2023 candidates"
date = "2023-08-21"
draft = false
author = "Lucy Ofiesh"
rank = 2
parent = "Elections"
weight = 6
type = 'dontindexcontent'
+++

From 87 applications, our [Nominating Committee](/committees/nominating/) put forward the following 11 candidates to fill the seven seats open for election to the Crossref Board of Directors. We will elect two large member seats and five small/midsized member seats. Please read their candidate statements below.  

If you are a voting member of Crossref your ‘voting contact’ will receive an email the week of September 25th. Please follow the instructions in that email which includes links to the relevant election process and policy information.

### Tier 1, Small and mid-sized members (electing five seats) 

| Member organization | Candidate standing | Title | Org type | Country  | PREP                         |
|---------------------|--------------------|-------|----------|----------|------------------------------|
| [Beilstein-Institut](#beilstein)   | Wendy Patterson  | Scientific Director  | Researcher service | Germany  | [Participation Report](https://www.crossref.org/members/prep/2086) |
| [Lujosh Ventures Limited](#lujosh)   | Olu Joshua  | Director of Publication | Scholar Publisher | Nigeria  | [Participation Report](https://www.crossref.org/members/prep/21123) |
| [Korean Council of Science Editors](#kcse)   | Kihong Kim     | President | Society | South Korea  | [Participation Report](https://www.crossref.org/members/prep/4099) |
| [NISC Ltd](#nisc)        | Mike Schramm  | Managing Director | Publisher | South Africa  | [Participation Report](https://www.crossref.org/members/prep/14412) |
| [OpenEdition](#openedition)        | Marin Dacos    | Senior Advisory | Publisher | France  | [Participation Report](https://www.crossref.org/members/prep/2399) |
| [Universidad Autónoma de Chile](#autonoma)        | Dr. Ivan Suazo   | Vice Rector of Research | University | Chile  | [Participation Report](https://www.crossref.org/members/prep/17608) |
 | [Vilnius University](#vilnius)        | Vincas Grigas   | Head of Scholarly Journals | University | Lithuania  | [Participation Report](https://www.crossref.org/members/prep/6097) |

### Tier 2, Large members (electing two seats)

| Member organization | Candidate standing | Title | Org type | Country  | PREP                         |
|---------------------|--------------------|-------|----------|----------|------------------------------|
| [Association for Computing Machinery (ACM)](#acm)        | Scott Delman | Director of Publications | Society | US  | [Participation Report](https://www.crossref.org/members/prep/320) |
| [Oxford University Press](#oup)        | James Phillpotts  | Director of Content Transformation & Standards | University | UK  | [Participation Report](https://www.crossref.org/members/prep/286) |
| [Public Library of Science (PLOS)](#plos)        | Dan Shanahan | Publishing Director | Publisher | US  | [Participation Report](https://www.crossref.org/members/prep/340) |
| [University of Chicago Press](#ucp)        | Ashley Towne  | Journals Director | University  | US  | [Participation Report](https://www.crossref.org/members/prep/200) |


---

##  Tier 1, Small and mid-sized members (electing five seats)

<a id="beilstein"></a>

<a id="Organization"></a>

###  Wendy Patterson, Scientific Director, Beilstein-Institut
#### Germany<br>

<img src="/images/board-governance/wendy-patterson-square.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Wendy Patterson's statements" %}}
### Personal statement <br>
I am a reliable and committed member of the Crossref Board and have enjoyed serving on the Executive Committee and having regular interaction with the Leadership Team. I would be honored to be re-elected for a second term.

Serving as the Scientific Director and Member of the Board of Directors of a similarly sized non-profit, I feel well-positioned to support the Leadership Team by sharing my experiences with strategy development and experience with financial oversight. As a former academic researcher turned non-profit publisher, I can also provide a unique perspective with hands-on experience in scholarly publishing together with insight into the struggles and barriers that smaller publishers face.

My past involvement with the Crossref Metadata Interest Group and current involvement with the Preprint Advisory Board has allowed me to appreciate even more Crossref’s true community-oriented approach in gaining feedback and involving all stakeholders, large and small. We are well aware of the services and benefits to the community that Crossref provides, and the importance of the role that Crossref plays in the development of new identifier and metadata initiatives cannot be overstated.

If re-elected, during my second term, I would focus on finding ways to support more independent diamond open access publishers and scholar-led journals in becoming Crossref members. I would also continue to focus on improved preprint metadata, improving transparency in scholarly publishing, and supporting FAIR data.

If re-elected my alternate would be Dr. Carsten Kettner, who has been involved with the Beilstein-Institut’s open science initiatives as well as many community initiatives for better standards and reproducibility in science for over 15 years.

### Organization statement
Background: The Beilstein-Institut is a charitable foundation located in Frankfurt, Germany. The results of all of our projects are freely available, and we do not charge any fees for our services. We support science and scientists worldwide with our diamond open access journals (no costs for authors or readers), data standardization projects and scientific events. We have built our own submission, tracking and publishing system from the ground up, in addition to a preprint server, and we are thus aware of the challenges in implementing new procedures or workflows. We are actively involved in community initiatives including OASPA, Free Journal Network, DORA, OA Switchboard and ALPSP, just to name a few.

Benefits to Crossref: We have a long and unique history in the German/European chemistry and open science community. Our diverse network, coming from the many conferences that we have organized and sponsored on the topics of open science and tools for scholarly communication, could be of benefit to Crossref. Additionally, we have built and operate our own preprint server, giving us exceptional insight into the interplay between preprints and published articles, and we would like to assist in bringing next-generation metadata to preprints.

How we represent the community and add diversity: Like us, approximately 14,000 of the publishers listed in the Directory of Open Access Journals (DOAJ) have two or fewer titles, and most Crossref content providers deliver <1,000 items per year and are in the “low fee tier” category. We, the (very) small, independent publisher, are clearly an important sector in publishing but are severely underrepresented in discussions and on boards where policy and decisions are made. Crossref aims to expand its outreach and interact further with the global publishing community, thus it is necessary to include these diverse voices and engage with these (very) small publishers of scholarly works.

Conclusion: We, like you, want to make scholarly communication better and we aim to provide a sustainable and shared infrastructure to support research communication. We feel that the vision and goals of our two non-profit organizations are very well aligned and that we are well-positioned to represent the much-needed voice of the very small publisher.
{{% /accordion %}} {{% /accordion-section %}}


<a id="lujosh"></a>
### Olu Joshua, Director, Lujosh Ventures Limited
#### Nigeria<br>

<img src="/images/board-governance/Olu_J3-square.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Olu Joshua's statements" %}}

### Personal statement <br>
I Olu Joshua (Director of Publication at Lujosh Ventures Limited) present myself as one of the aspirants contesting for a sit among the board of directors in Crossref. I believe that I am qualified to be a member of the board for the following reasons:
1. I am a member of board of directors for different organizations, in which the firm that I represent on Crossref is one of them, so I possess the experience for board room interactions, functionality and governance that create productivity in the long run.
2. With over 10 years background in scientific publication, information and communication technology, I understand how Crossref disseminate her services by parading the community which includes members, publisher, libraries, institution of learning etc., around the globe for metadata which is the foundation of its services.
3. As a researcher and author, I understand what the scholastic environment demand from organization like Crossref in making scholarly communications better.

My goals:
* To bring fresh voices on the board and ensure the board doesn't become sclerotic.
* To contribute my expertise in encouraging more productive policies that will take Crossref to the next greater height by collaboration and partnership with other organizations with already existing and robust scholarly infrastructure.
* To give undivided attention and support with the other board members on operation, innovations and financial oversight that will strategically position and enhance the productivity of Crossref and its members at large.

I solicit for your support and kindly cast your vote for me.
Thank you

### Organization statement
Lujosh Ventures Limited as a company is delighted to have the privilege in vying for a sit as a member of the board. Our presence on the board will bring the much-needed minority view across board considering our West African location and style of governance. We are currently publishing over 20 academic journals digitally with more to be added soon. As an organization we are committed to promulgate small, scholastic materials publishers and assist in identifying ways to make scholarly interaction and communication better and transparent. We are currently working on a program to encourage more funders to participate in academic scholastic endeavors in our country and West Africa. 

Scholarly publishing keep getting novel innovations and association, content providers emergent is increasing day by day, Crossref been known to give content linking services and supports will need more stability into the future In supporting Crossref mission, it is key that its governing Board is made of diverse scholarly content providers and publishers. Lujosh Ventures Ltd can make available on the board viable perspective. We will be involved in the governance that will bring valuable progress to the stakeholders by undivided oversight functions on the operation and finances of the organization. We will work with other members of the board to encourage best practices, enact and uphold productivity policies where all stakeholders will benefit. We will be an advocate of innovative ideals from the management to the promotion of Crossref. The growth of Crossref is our priority and that is all what we will be adhering to as a member of the board. 
{{% /accordion %}} {{% /accordion-section %}}


<a id="kcse"></a>
### Kihong Kim, President, Korean Council of Science Editors
#### South Korea<br>

<img src="/images/board-governance/kk.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Kihong Kim's statements" %}}

### Personal statement <br>
I am a professor at Ajou University in Korea, specializing in physics. Since 2012, I have held various positions within the KCSE, including Chair of the Committee on Publishing, Vice President, and Editor-in-Chief of Science Editing, the official journal of the KCSE. This year, I began my three-year tenure as the President of the KCSE. Over the past twelve years, I have actively participated in KCSE activities, gaining valuable experience in various aspects of journal editing and publishing. Additionally, I have served as a board member of Crossref since 2020, including participation in the nominating committee last year.

I still vividly recall attending the Crossref Annual Meeting in 2014 and being deeply impressed by the innovative ideas presented there. I am familiar with the services provided by Crossref and have made efforts to stay updated with their latest advances. As the Editor-in-Chief of Science Editing, I have played a significant role in publishing many articles introducing Crossref's activities.

If re-elected to the board, I will strive to foster a stronger collaboration between the KCSE and Crossref. I will actively gather the needs and opinions of Korean editors, publishers, and researchers, providing input to Crossref. As an active researcher myself, I can convey the perspectives of researchers and authors to the board. The internet has revolutionized information sharing and academic research. There are rapid changes anticipated across all areas of publishing. Crossref stands as one of the most important organizations leading these advancements, and it would be a great honor for me to have another opportunity to participate in such activities.

### Organization statement
The Korean Council of Science Editors (KCSE) was established in 2011 with the aim of promoting the quality of science journals published in Korea to meet international standards. Currently, KCSE has 333 member journals published in Korea, covering all areas of science, engineering, and medicine. KCSE takes the initiative in organizing and leading various activities, including workshops and training programs, annually catering to editors, manuscript editors, publishers, and researchers. The council places great importance on international cooperation and played a pivotal role in the establishment of the Council of Asian Science Editors in 2014. Additionally, it operates the Korean Regional Chapter in association with the European Association of Science Editors.

Since its inception, the KCSE has prioritized the enhancement of style and format in Korean science journals to adhere to international standards. Specifically, the services and technologies developed by Crossref have played a significant role in many KCSE programs. Presently, the estimated annual number of papers deposited by KCSE member journals to Crossref exceeds 25,000. In addition, its official journal, Science Editing, has featured many articles that introduce Crossref's visions and services. Both the KCSE and Crossref share a mutual vision that scholarly information can be easily accessed and utilized through the linking and networking of DOIs and metadata. As a representative organization in Korea, the KCSE is well-positioned to effectively introduce Crossref's latest advancements and services to Korea. Moreover, through international activities, the KCSE can extend the promotion of Crossref's technology to multiple Asian countries. These contributions significantly assist Crossref in expanding its outreach, diversifying its impact, and acquiring fresh perspectives.

{{% /accordion %}} {{% /accordion-section %}}

<a id="nisc"></a>
### Mike Schramm, Managing Director, NISC Ltd
### South Africa<br>

<img src="/images/board-governance/mike-schramm.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Mike Schramm's statements" %}}

### Personal statement <br>
As Managing Director of NISC, I provide the strategic direction for the company. My academic background is in the biological sciences with wide-ranging published research interests. Over the past 25 years, I have held management positions in both the not-for-profit and commercial publishing sectors in South Africa, moving to NISC in 2008 where I am also a shareholder. 

Our own experience at NISC and the day-to-day engagement with journal editors and the societies and institutions whose titles we publish have given me a good understanding of the challenges faced by the small, regional publisher. These challenges go beyond the financial and relate to impediments many journals experience in implementing best practices that will get the work of their authors noticed and cited. A consequence of these difficulties is that too much important, good quality research published on the African continent does not get the exposure it deserves. I would like to see an improved understanding amongst the often overlooked regional, small publishers regarding the ways Crossref tools and services can help to shine a light on hitherto poorly exposed scholarship. 

I have a particular passion for working with early career scholars and to this end have involved NISC in a number of externally funded programmes involving mentor-driven article writing courses, collaborations with new journal editors and publishing of monographs of young humanities scholars. These initiatives have engaged participants from across the African-continent and given opportunities for new voices to be heard.

I am a non-executive director (and board chairman)of African Journals Online (AJOL), a widely respected non-profit organisation that shares many of the goals of NISC to improve access to the scholarship of Africa, by users both within the continent and to the North.

### Organization statement
NISC (Pty) Ltd is the publisher of a range of premier African research products that promote African scholarship to a global audience. We publish high-quality research on behalf of institutions, societies, and associations in Africa. The business started with building, licensing and publishing bibliographic databases, targeting research institutions around the world. Database development remains a key activity at NISC, but we now publish academic journals on behalf of local scholarly societies and research institutes and have many of Africa's leading peer-reviewed academic journals in our stable. NISC also publishes scholarly books, primarily monographs.

In the past we were able to keep pace with the big publishers with respect to the developments in production standards and online hosting – indeed NISC has been an early adopter of new technologies gaining us multiple Top 100 Technology Company awards in South Africa. However, in common with other small publishers we faced challenges in competing with bigger enterprises able to offer large and diverse title bundles in country or consortium deals. As a remedy, we have secured international publishing partnerships, working with companies who share our ethos of shining a light on the scholarship of Africa. From our base in a small university town in the Eastern Cape province of South Africa, these partnerships have put the best of African research into the hands of scholars across the world.

Our engagement with Crossref, from its early days, was in pursuit of best publishing practice and particularly improving discoverability. Alongside our co-publishing partners, we have continued to include these tools and services in all our publishing workflow. We have a good understanding of the challenges that the small regional scholarly publishing enterprise faces in raising visibility and discoverability of the work of their authors and believe that we are well positioned to represent this sector.
{{% /accordion %}} {{% /accordion-section %}}


<a id="openedition"></a>
### Marin Dacos, Senior Advisor, OpenEdition
#### France<br>

<img src="/images/board-governance/Marin--Dacos.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Marin Dacos's statements" %}}

### Personal statement <br>
I created OpenEdition 25 years ago and have been its director for 20 years. I am very familiar with the diversity of the diamond open access publishing ecosystem in Europe.  

I consider Crossref as a major Open Science Infrastructure. I have served as a member of the Board of Directors of Crossref for six years now and have learned a lot. I have tried to contribute to a broader definition of the mission of Crossref, including open science as a core principle.

I have also been involved in public policies, as National Open Science Coordinator at the Ministry of Higher Education, Research and Innovation in France, being responsible for the two National Open Science Plans (2018 and 2021). The plan is pushing in favor of links between digital research objects, beyond publications only, and implementing as far as possible norms like CRediT (Contributor Roles Taxonomy).

My alternate is Marie Pellen, Director of OpenEdition. Marie Pellen joined OpenEdition in 2007. She created in 2010 the LusOpenEdition project, supported by the Calouste Gulbenkian foundation. Based in Lisbon, she learned Portuguese. Marie Pellen became Deputy Editorial Director of OpenEdition in 2017. She is now the director of OpenEdition, a centre composed of 55 people. She is also a member of the Open Scientific Publishing groups of the French Open Science Committee.

### Organization statement
OpenEdition is an international infrastructure dedicated to HSS. OpenEdition is developing an international platform dedicated to books and another to journals. OpenEdition hosts content from over twenty countries and publishes content in all languages. 84% of journals and books are open access. OpenEdition represents bibliodiversity, hosting small and medium sized publishers, in many languages (French, Spanish, English). OpenEdition trains and supports more than 600 journals and more than 100 publishers to become part of the open science ecosystem. OpenEdition has been a member of Crossref for over 15 years and has over 500,000 DOIs.

As DOIs and Crossref have become key services for open science, it is necessary to take into account small and medium sized publishers and platforms, in order to provide a comprehensive service for the whole ecosystem, and not only to major players and international initiatives. In HSS, citation practices often call for a combination of recent and old publications (mainly without DOI). The figures of OpenEdition show that DOI coverage for all content (old and recent) needs to be improved (see https://www.openedition.org/42231?file=1 page 37). It is essential for Crossref to be as inclusive as possible, including the long tail of small sized publishers in the context of bibliodiversity.

{{% /accordion %}} {{% /accordion-section %}}


<a id="autonoma"></a>
### Dr. Ivan Suazo, Vice Rector of Research, Universidad Autónoma de Chile 
#### Chile<br>

<img src="/images/board-governance/IvanSuazo-square.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Dr. Ivan Suazo's statements" %}}

### Personal statement <br>
As the Vice Rector of Research at the Universidad Autónoma de Chile, I have taken a leadership role in fostering a culture of open access within our community. I have spearheaded the implementation of structures, policies, and mechanisms that have enabled the establishment of a robust governance system for scientific data and articles, which we make available to the community.

In my position, I have gained knowledge of Crossref services and its role in advancing open scholarly infrastructure. I understand the value of complete, open, and global scholarly metadata and the importance of fostering connections and collaborations within the research community. I am familiar with Crossref's strategic agenda and its commitment to openness, transparency, and sustainability.

If elected to the board, I would bring this knowledge and experience to contribute actively to Crossref's mission. I envision a community where scholarly metadata is not only rich and connected but also easily accessible and usable by researchers worldwide. I would work towards expanding the network of metadata and relationships, encouraging best practices among members, and ensuring the accuracy and transparency of the metadata we collect.

Furthermore, I am dedicated to fostering inclusivity and diversity within the community. I recognize the significance of representing the Latin American culture, which has enthusiastically embraced open access. By integrating our perspectives, experiences, and needs into the board's decision-making process, we can create a more comprehensive and representative scholarly infrastructure.

In conclusion, I offer an understanding of Crossref's services and a vision for our community that embraces open access, collaboration, and inclusivity. I am committed to advancing the strategic agenda of Crossref, promoting open scholarly infrastructure, and ensuring the long-term sustainability and transparency of our scholarly ecosystem.

### Organization statement
The Universidad Autónoma de Chile would be an invaluable asset to Crossref as a board member, aligning with its strategic agenda and upholding open infrastructure principles. Our presence would bring diversity and represent the Latin American culture, which has enthusiastically embraced open access.

Our university has taken significant steps towards open access and scholarly communication. We have established an academic repository, scientific journals, and a science communication center. Notably, we are proud to be the only university press that offers all its content openly and freely. These initiatives exemplify our commitment to complete, open, and global scholarly metadata and relationships.

In support of Crossref's strategic agenda, we recognize the need for a more connected scholarly record. We endorse flexibility, clearer metadata provenance, and improved accuracy and transparency. As a board member, we would actively contribute to expanding the network of metadata and relationships, encouraging our members to meet best practices and promoting the value of richer open metadata.

Furthermore, we embrace the Principles of Open Scholarly Infrastructure (POSI). We advocate for broad community governance, transparent operations, and non-discriminatory membership. A board-governed organization that considers community consensus and diverse interests builds trust and confidence. We are committed to inclusive governance, reflecting the demographics of the membership and ensuring transparent processes. We fully support open-source software and open data practices. We acknowledge privacy constraints while emphasizing the availability of relevant data through periodic dumps. 

Electing the Universidad Autónoma de Chile to the board would bring a diverse perspective, representing the Latin American culture's integration into the open access movement. Our university's concrete actions and unwavering commitment to open infrastructure principles make us an ideal partner in advancing Crossref's strategic agenda and ensuring the organization's sustainability and transparency.

{{% /accordion %}} {{% /accordion-section %}}

<a id="vilnius"></a>
### Vincas Grigas, Head of Scholarly Journals, Vilnius University 
#### Lithuania<br>

<img src="/images/board-governance/VincasGrigas045-square.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Vincas Griga's statements" %}}

### Personal statement <br>
In my role at Vilnius University Press, I manage scholarly journals, and as an Associate Professor at Vilnius University, I am deeply engaged in projects in scholarly publishing. In 2019, I assumed the presidency of the Association of Lithuanian Serials, making it my duty to voice the needs of small publishers within Lithuania. As the only Crossref Sponsoring organization in Lithuania, the Association is instrumental in elevating the concerns of these publishers.

Given my diverse responsibilities and research interests, which include media and information literacy, book piracy, information behaviour, and scholarly communication, I could bring a unique perspective to my role as a Crossref Board Member. 

My vision for Crossref is to increase the efficiency of its services for small publishers. I aim to create tools and methods to submit and use metadata without the need for complex software or special skills. A core challenge faced by these publishers in leveraging Crossref to its full potential is often related to the lack of skills or necessary tools, a problem I plan to address. I also believe in sharing best practices for the efficient utilization of Crossref services to enhance scholarly communication among small publishers.

### Organization statement
In an era where initiatives such as 'Plan S' and 'cOAlition S' are accelerating the transition to full and immediate Open Access to scientific publications, this moment presents a golden opportunity for university presses to regain prominence on the scholarly publishing stage. 

Vilnius University Press, in operation since 1575 and the largest scholarly publisher in the Baltic states, is driving forward to implement full Open Access across all its journals and proceedings. As a small, non-profit publisher that does not charge for article submissions or levy article processing charges, we grapple with the fascinating challenge of publishing high-quality journals on a minimal budget. 

We champion services like Crossref, which enable us to participate in scholarly communication at an exceptionally low cost yet with significant value. We advocate for the use of open-source products, in line with our vision of making Open Access not just about content, but also the tools used to create and share that content. 

We are actively involved in the Association of European Universities Presses (AEUP), where discussions about the use of open-source tools at all stages of publishing are ongoing.  

We hope to serve as a beacon for other small publishers, demonstrating the feasibility and effectiveness of implementing services offered by Crossref. We firmly believe that the Crossref board should be balanced and diversified, encompassing representatives from both large and small publishers from various regions and countries. 
{{% /accordion %}} {{% /accordion-section %}}


##  Tier 2, Large members (electing two seats)

<a id="acm"></a>
### Scott Delman, Director of Publications, Association for Computing Machinery (ACM)
#### United States<br>

<img src="/images/board-governance/Scott-Delman.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Scott Delman's statements" %}}

### Personal statement <br>
During my 30 years working in STM Publishing, I’ve had the opportunity to play an active and leading role in many of the significant changes in our industry from the transition to online journal subscriptions while serving as an editor and young publishing manager in the early 1990s when Kluwer launched one of the first online journals to the consolidation of large commercial publishers and rise of the “big deal” and “library consortia” having participated in several of the industry’s largest acquisitions at the time as a publishing director and vice president at Kluwer in the mid-1990s and early 2000s to the introduction of the first open access and ebook business models as vice president of business development at Springer. All of these experiences have given me an appreciation for the role technology plays in supporting, enabling, and driving positive change in our industry, a focus on the need for continuous innovation, and the knowledge and experience to help guide Crossref through the industry’s future challenges, such as the rapid increase in research and publication related misconduct and open access publication. 
 
Over the years, I have been an active participant in Crossref’s governance, having served on and led many of the organization’s committees, including the nominating committee, membership & fees committee, and executive committee. Prior to the pandemic, I served as Crossref Board Treasurer and during the pandemic I had the sincere privilege to serve as Crossref’s Board Chairman working closely with Ed, Lucy, Geoffrey, and Ginny to support and guide the organization through a time of extreme risk and uncertainty. After a brief two-year hiatus from Crossref’s governance, I am hoping to rejoin the Board to continue supporting this organization I feel so passionately about. 

### Organization statement
The Association for Computing Machinery or ACM, as it is widely known, is one of the world’s leading technology associations and publishers, focused entirely on the field of Computer Science. As a non-profit society, our mission is to support the scientific community above all else, but as a mid-sized STM publisher we face many of the same financial, editorial, and technical challenges that many of Crossref’s members face lacking the financial and human resources that exist within larger publishers. With these challenges, we tend to identify more with the long tail of small publishers and organizations than with the large commercial publishers and can bring this perspective to the Board.
 
Over the past decade, we have evolved from an organization operating in a largely defensive mode with respect to Open Access and Research Integrity to one of the leading innovators of new OA business models and research integrity solutions. Participating in the governance of organizations like Crossref, arXiv, Portico, and CHORUS has enabled ACM to make a greater impact on our community than would have been possible without them. Many of our current constraints are with technology and accurate data and metadata that underpin our evolving business models and research integrity initiatives, all areas where Crossref has a significant role to play.  
 
While Crossref has many challenges ahead of it, I see Open Access Publication and Research Integrity as the two most significant in that they will require Crossref to more actively engage with a wider range of members and stakeholders in the industry than ever before, including universities, individual researchers, commercial technology providers, research funders, policy makers and governments, and of course publishers in deeper and more collaborative relationships than have historically been necessary for Crossref. Expanding and building on Crossref’s existing services, including unique identifiers, Similarity Check, Crossmark, funder registry, and others will be necessary to address these challenges. 

{{% /accordion %}} {{% /accordion-section %}}


<a id="oup"></a>
### James Phillpotts, Director of Content Transformation & Standards, Oxford University Press 
#### United Kingdom<br>

<img src="/images/board-governance/james-phillpotts-square.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "James Phillpotts's statements" %}}

### Personal statement <br>
I have worked at Oxford University Press for nearly 15 years across a variety of roles and am currently Director of Content Transformation & Standards, with responsibility for digital content assets, models, and management solutions, including application of content metadata standards and interoperability. In this role I have had direct involvement with the importance of building and maintaining infrastructure to connect researchers with research outputs, and I would like to continue to offer the Crossref Board my experience and enthusiasm for doing so.

I have a keen interest in collaborative initiatives that improve the sustainability of scholarly communications. As a current member of the Crossref Board, I have been engaged with the organization’s work and priorities first-hand, including serving on the Executive Committee. In addition to my participation with Crossref, I represent OUP’s voting membership of NISO, am co-Chair of NASIG’s Digital Preservation Committee, and serve on the board of the CLOCKSS digital archive.

Shared standards and the services that Crossref provides are of critical value to the wider publishing community. While the fundamentals may be easy to take for granted, the central role Crossref plays – and the benefits to scholarly communications as a result – should not be under-estimated. Crossref not only provides vital metadata registration and dissemination, but also plays a significant role in the development of tools and standards which support innovative publishing.  As one of the most successful examples of collaboration in our industry, it would be an honour to continue to serve on the Crossref Board.

### Organization statement
Oxford University Press is a department of the University of Oxford, with a clear mission of excellence in research, scholarship, and education by publishing worldwide, which informs everything we do. We have an incredibly diverse publishing programme, with products covering a broad academic and educational spectrum, and we aim to ensure that this content is readily discoverable and available to our users, in support of the University’s aims of furthering education and disseminating knowledge. Frequently this is in partnership with academic societies (on behalf of which we publish the majority of our journals list), as well as working with other university presses to help make the best scholarly publishing from around the world easily accessible online.

In working to fulfil our mission, we share Crossref’s vision of a scholarly record for the benefit of society globally – and we are keen to continue to build on our existing connections within the scholarly communications community to explore how we may work together to further that goal. As the largest University Press, OUP combines the perspectives of both a large publishing organization and the academic community. We believe that a continuing presence of this broad perspective on Crossref’s board will enable us to help the organization maintain its reflection of those intersecting considerations and values.

{{% /accordion %}} {{% /accordion-section %}}


<a id="plos"></a>
### Dan Shanahan, Publishing Director, Public Library of Science (PLOS) 
#### United States<br>

<img src="/images/board-governance/DS-Headshot-square.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Dan Shanahan's statements" %}}


### Personal statement <br>
My extensive career in scholarly publishing has spanned publishing, editorial and product management, providing me with a nuanced understanding of scholarly communications, as well as the trends and challenges that are influencing the evolving research landscape. In my current role as Publishing Director at Public Library of Science (PLOS), I lead cross-functional business units and partners in shaping and implementing the strategic development and expansion of PLOS’ journal portfolio.
 
I have deep experience in publishing innovation, having been Head of Product at F1000, leading the health sciences portfolio at BioMed Central (subsequently Springer Nature) and as Product Lead at Cochrane. Throughout my career I have worked closely with Crossref in multiple capacities including, supporting Advisory Groups and chairing the Linked Clinical Trials Working Group. I have similarly been closely involved in affiliated initiatives, for example chairing the Publishing Group for Metadata 2020, and as a Board Member for Open Trials.
 
I have considerable experience in not just bringing new innovations to market, but also driving adoption. Crossref has a fantastic record of innovative and impactful enhancements, which provide real value to the scholarly communications ecosystem. My skills and expertise put me in a strong position to support the organisation, ensuring that robust implementation plans achieve the necessary behavioural change and successful adoptions. 
 
With the ever-expanding corpus of literature, I believe l that Crossref is uniquely placed to drive significant progress in discoverability, tackling other research objects, such as article recommendations, modularizing DOI metadata, and really targeting machine learners and AI, and I feel that my longstanding experience in the development of architecture, products and underlying infrastructure that promote and support high-quality structured metadata would ensure that I can make a meaningful contribution to this ambition. As a Board Member, I would be honoured to contribute to the Crossref ambition of enabling connections and giving greater context, particularly in all areas of discoverability. 

### Organization statement
PLOS is a non-profit Open Access (OA) publisher, founded to empower researchers to make science immediately and publicly available online, without restrictions. In the last 20 years we have propelled the movement for OA alternatives to subscription journals. We established the first multi-disciplinary publication inclusive of all rigorous research regardless of novelty or impact, and we demonstrated the importance of open data availability.
 
We have been a member of Crossref since the launch of our first journal – PLOS Biology – in 2003, and PLOS remains deeply committed to Crossref’s principles of open metadata; we are highly ranked for completeness of metadata records and follow best practices as demonstrated by Crossref’s member participation reports.
 
Our goals haven’t changed and remain closely aligned with the strategic direction for Crossref. Our objective is to promote an equitable, global, collaborative Open Science future. The needs of the research community are changing, so new solutions need to be co-created with the communities themselves and all actors in the ecosystem. Crossref is in a unique position to achieve this, as a fundamental partner to researchers, funders and publishers globally, and working together with PLOS puts us in the best position to achieve our shared vision of a scholarly record that the global community can build on forever, for the benefit of everyone.

{{% /accordion %}} {{% /accordion-section %}}

<a id="ucp"></a>
### Ashley Towne, Journals Director, University of Chicago Press 
#### United States<br>

<img src="/images/board-governance/Ashley-Towne-square.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Ashley Towne's statements" %}}

### Personal statement <br>
As Journals Director for the University of Chicago Press, I oversee a publishing program of more than 90 journals, which we publish on behalf of the University of Chicago and 53 publishing partners. My responsibilities include setting the strategic direction for the Journals Division and growing our network of partnerships. Additionally, I serve on the University of Chicago Press Management Committee, and I am a founding member of the Press D&I Committee. In 2020 I completed the Executive Program for Emerging Leaders at the University of Chicago Booth School of Business. 

Our primary goals are to expand the reach and readership of the scholarship published in our journals and to make it easier to connect researchers with research. These goals drive everything we do as a university press journals program, and they align with Crossref’s mission. 

Since I started as Director in 2018, our journals portfolio has added 26 new or newly acquired journals, and usage on our platform has doubled. During this time, I spearheaded a partnership with Benetech’s Bookshare initiative to provide accessible journal articles for people with reading barriers such as dyslexia, blindness, and cerebral palsy—this agreement is unique within journals publishing. I also launched the Chicago Emerging Nations Initiative (CENI) Direct, a program that works with institutions to provide free electronic access to every issue of every journal we publish for nearly 1,000 institutions in 100 developing countries.

Combining the practical experience gained from nearly 25 years in journals publishing with a wider lens of strategic vision-setting ensures I’ll bring a valuable point of view to the Crossref Board. I appreciate the opportunity to share this experience and the perspective of the University of Chicago Press. Thank you for your consideration.  

### Organization statement
Since its origins in 1890 as one of the three main divisions of the University of Chicago, the Press has embraced as its mission the obligation to disseminate scholarship of the highest standard. Through our books and journals, we seek to publish serious works that foster public understanding, provide an authoritative foundation for informed dialogue, and enrich the diversity of cultural life. 
 
The Press publishes more than 90 scholarly journals primarily in the humanities and social sciences disciplines, but our portfolio also includes art, education, and life science journals. Our publishing partners include scholarly societies and associations, foundations, museums, and other not-for-profit organizations. Some of our publishing partnerships were established more than 50 years ago.

{{% /accordion %}} {{% /accordion-section %}}



