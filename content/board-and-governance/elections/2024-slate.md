+++
title = "Board election 2024 candidates"
summary = "Help elect the incoming class of 2025 Crossref board members"
date = "2024-09-16"
draft = false
author = "Lucy Ofiesh"
parent = "Elections"
rank = 2
weight = 6
type = 'dontindexcontent'
+++

From 53 applications, our [Nominating Committee](/committees/nominating/) put forward the following seven candidates to fill the four seats open for election to the Crossref Board of Directors. We will elect two large member seats and two small/midsized member seats. Please read their candidate statements below.  

If you are a voting member of Crossref your ‘voting contact’ will receive an email the week of September 23rd. Please follow the instructions in that email which includes links to the relevant election process and policy information.

### Tier 1, Small and mid-sized members (electing two seats) 

| Member organization | Candidate standing | Title | Org type | Country  |
|---------------------|--------------------|-------|----------|----------|------------------------------|
| [Austrian Science Fund (FWF)](#fwf)   | Katharina Rieck  | Open Science Manager | Research funder | Austria  |
| [California Digital Library](#cdl)   | Lisa Schiff  | Associate Director, Publishing, Archives, & Digitization | Library| US  |
| [Health Services Academy; Pakistan Journal of Public Health](#pjph)   | Ejaz Khan| Editor in Chief; Professor | Government agency/NGO | Pakistan  | 
| [MM Publishers](#mm)  | Karthikeyan Ramalingam | Editorial Manager; Associate Dean| Publisher | India | 

### Tier 2, Large members (electing two seats)

| Member organization | Candidate standing | Title | Org type | Country |
|---------------------|--------------------|-------|----------|----------|------------------------------|
| [American Psychological Association](#apa)| Aaron Wood | Head, Product & Content Management | Society | US  |
| [Public Library of Science (PLOS)](#plos)   | Dan Shanahan | Publishing Director | Publisher | US  |
| [Taylor and Francis](#taf)  | Amanda Ward  | Director of Open Research | Publisher  | UK  | 


---

##  Tier 1, Small and mid-sized members (electing two seats)

<a id="fwf"></a>

<a id="Organization"></a>

###  Katharina Rieck, Open Science Manager, Austrian Science Fund (FWF)
#### Austria<br>

<img src="/images/board-governance/foto_fwf.png" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Katharina Rieck's statements" %}}
### Personal statement <br>
With over a decade of experience in research funding and specialized expertise in scholarly communication, open science, policy development, and research data management, I can bring a broad perspective to the Crossref Board. As Open Science manager at the Austrian Science Fund (FWF), I am responsible for the development and implementation of the FWF’s Open Science strategy. I coordinate funding initiatives such as the FWF Open Access Block Grant and the Open Research Data Pilot, ensuring robust support for Open Science. Additionally, I represent the FWF in various national and international organizations, including the national ORCID consortium, cOAlition S, Science Europe, and COARA. This involvement facilitates regular exchanges with diverse stakeholders in the scholarly information ecosystem.
 
My role also includes managing projects aimed at enhancing the accessibility and interoperability of the FWF's grant information. Notably, I led the implementation of the Crossref Grant DOI for FWF-funded projects, ensuring the FWF’s grant data is openly accessible, interoperable, and reusable.
 
I am committed to Crossref’s vision of comprehensive, open, and global scholarly metadata—the research nexus, and am eager to contribute to this vision and collaborate with fellow board members to advance the quality and reach of scholarly communication.


### Organization statement
The Austrian Science Fund (FWF) is Austria's leading funding organization for basic research. Based on a rigorous international peer review process, the FWF supports excellent researchers and their ground-breaking ideas. Since the early 2000s, the FWF has been supporting and promoting open science, ensuring that research outcomes and information are as openly available as possible.
 
As one of the first funding agencies worldwide to implement an open access policy for research outputs in 2004, the FWF possesses a wide range of knowledge and experience in the scholarly communications landscape. The FWF strongly advocates for transparent, robust, PID-based research information, and has been registering Crossref Grant DOIs for all its research projects since 2023. Additionally, since 2016, the FWF has mandated ORCID for all funded researchers and includes ROR IDs in its research project metadata.
 
The FWF's presence on the Crossref Board would bring the perspective of a research funder to the scholarly record and Crossref’s work in the research information landscape. Given Crossref's project to broaden the metadata record to capture richer funding affiliation and work towards a holistic research nexus, the FWF can ensure that the funders' voice is heard and represented on the board. This would enhance the board's diversity and strengthen its ability to serve the entire Crossref community effectively.
{{% /accordion %}} {{% /accordion-section %}}


<a id="cdl"></a>
### Lisa Schiff, Associate Director, Publishing, Archives, & Digitization, California Digital Library
#### United States<br>

<img src="/images/board-governance/2024-candidates/l_schiff_cms.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Lisa Schiff's statements" %}}

### Personal statement <br>
As Associate Director of the Publishing, Archives, & Digitization group at the California Digital Library (CDL) at the University of California (UC), I guide strategic planning, development, and operations of services supporting publishing, research, and learning activities of UC’s academic community, including UC’s open access publishing platform (with 90+ scholarly journals), a research information management system undergirding UC’s open access policies, an institutional repository, and two preprint servers. I am a member of the Crossref board (and am chair), the Directory of Open Access Journals advisory board, a NISO Topic Committee, and co-chair the Digital Library Federation Committee on Equity and Inclusion.

My perspective as an institution-based member (a significant area of Crossref’s growth) strongly informs my efforts on the Crossref board. Currently in my second year as board president, I have collaborated with Crossref leadership and board members to ensure the organization supports expansive and equitable global participation and a strong, open scholarly communication ecosystem. 

My familiarity with the scholarly communication ecosystem, and experience on the Crossref board and implementing Crossref services, give me a valuable view of Crossref, including with regard to library publishers, institutional members, and community infrastructure developers. I understand the benefits and challenges of PIDs and have strengths in organizational development, management, strategic planning, and assessment.  I am thus well positioned to support Crossref in its forward progress and am eager to continue this rewarding work in another board term. If re-elected, I would propose Chad Nelson, a Technical Team Manager at CDL, as my alternate.

### Organization statement
The California Digital Library (CDL) at the University of California (UC) has been a leader in library-based publishing for over 20 years, now with 90+ community-led journals on eScholarship, our flagship open access publishing platform. Our program supports subscription journals flipping to open access, emerging disciplines, under-represented voices, fields with limited commercial markets, practitioners in applied fields, and graduate and undergraduate students. 

CDL also provides a UC systemwide institutional repository; a research information management system implementing UC’s expansive open access policies; and preprint servers for the geoscience and the ecology, evolution and conservation communities.  CDL is thus situated within the needs and aims of our publishing partners--and as a technology center, collaborates on building community-supported open source software attuned to meeting community ambitions, including the challenges in gathering core metadata, integrating platforms, and providing optimal user experiences. 

CDL is actively involved at leadership levels with essential scholarly communications  initiatives and organizations, such as: Crossref; Library Publishing Coalition; Directory of Open Access Journals; Coalition of Open Access Policy Institutions; National Information Standards Organization; Open Access Scholarly Publishing Association; and Research Organization Registry.  To these efforts, CDL brings awareness of the requirements, goals, and impact of institutionally supported publishing activities, an expanding sector scholars increasingly turn to for trusted solutions and that is reflected in Crossref’s membership growth. Continuity of the library publisher voice on the board would further strengthen Crossref’s ability to meet its efforts to broaden stakeholder and community engagement, and service and metadata improvement.

{{% /accordion %}} {{% /accordion-section %}}


<a id="pjph"></a>
### Ejaz Khan, Editor in Chief & Professor, Health Services Academy, Pakistan Journal of Public Health
#### Pakistan<br>

<img src="/images/board-governance/2024-candidates/Prof Ejaz Khan.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Ejaz Khan's statements" %}}

### Personal statement <br>
As a seasoned academician, researcher, and consultant in Public Health, I bring to the table over two decades of experience encompassing teaching, training, research, and program management. My extensive background in being an editor of various international journals positions me uniquely to contribute meaningfully to the Crossref community.

Having published prolifically in reputable international journals, I understand the critical role that scholarly communication plays in advancing knowledge. My work, with an h-index of 100 and i10 of 115, underscores my commitment to rigorous research and dissemination of findings. Additionally, my editorial roles in esteemed journals such as Pakistan Journal of Public Health, Plos ONE, Cities&Health and BMC Public Health have honed my understanding of publication processes.

I possess a comprehensive understanding of Crossref services and recognize their pivotal role in facilitating scholarly communication and metadata management. Leveraging my expertise, I envision fostering a more inclusive and collaborative community within Crossref. This entails advocating for enhanced accessibility and discoverability of research outputs, particularly from underrepresented regions.

Furthermore, I am passionate about promoting ethical research practices and ensuring the integrity of scholarly communications. I aim to collaborate with Crossref members to develop initiatives that uphold the highest standards of transparency and accountability.

### Organization statement
The inclusion of the Health Services Academy (HSA) on the Crossref board would undoubtedly enrich the organization's perspective and enhance its capacity to serve the diverse needs of the global scholarly community.

As a premier institution in public health education, research, and capacity building, HSA brings a unique lens to the table. Our multidisciplinary approach and commitment to evidence-based practice align closely with Crossref's mission of facilitating open and accessible scholarly communication. Its Pakistan Journal of Public Health is being supported by the Crossref, and the academy highly appreciates this contribution, and proudly upholds it through its peer-reviewed high quality scholarly articles on quarterly basis. 

HSA's presence on the board would ensure representation from a region where access to research resources and publication platforms can be limited. This perspective is invaluable in guiding Crossref's efforts to bridge existing gaps and promote inclusivity in scholarly communication. Additionally, our extensive network and collaborations with local and international stakeholders offer opportunities for Crossref to expand its reach and impact.

Moreover, HSA's expertise in ethics, data management, and capacity building complements Crossref's focus on promoting responsible research practices and advancing metadata standards. By leveraging our knowledge and resources, Crossref can further strengthen its role as a trusted steward of scholarly information.

HSA's participation on the Crossref board would bring diversity, expertise, and a global perspective to the table. We are committed to collaborating effectively with Crossref and its members to advance the shared goals of enhancing scholarly communication and fostering innovation in research dissemination.

{{% /accordion %}} {{% /accordion-section %}}

<a id="mm"></a>
### Karthikeyan Ramalingam, Editorial Manager & Associate Dean, MM Publishers
### India<br>

<img src="/images/board-governance/RK1.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Karthikeyan Ramalingam's statements" %}}

### Personal statement <br>
I spearheaded the successful migration of our DOI services from Medknow to MM Publishers, ensuring a seamless transition in collaboration with the Crossref team. I take pride in our journal's inclusion in the Web of Science and the ongoing indexation of several others. My role involves utilizing Crossref services for effective metadata management across multiple journals, ensuring open access to our published articles. 

I have played a key role in streamlining journal operations, facilitating coordination between editorial teams across various dental specialties, and collaborating with the copyediting and software teams to maintain continuous publishing activities. These efforts have greatly benefited our authors and researchers, enabling them to showcase their work to a broader audience.

As a team player, I actively encourage my colleagues to share their ideas, carefully evaluate their merits, and foster a supportive environment that motivates voluntary contributions. I am committed to backing their academic endeavors, helping them grow within our institution, and empowering them to become self-reliant. 

My passion lies in advancing open-access publishing, particularly for researchers in developing countries who face challenges such as high article processing charges, and limited access to English language editing, and journal formatting services. 

I believe Crossref plays a crucial role in expanding the reach of scholarly content, promoting open access, preventing plagiarism, and supporting global research efforts. I am always open to communication and collaboration, and eager to contribute to the ongoing evolution of academic publishing.


### Organization statement
Saveetha Institute of Medical and Technical Sciences, Chennai, Tamil Nadu, India, was founded by our Chancellor, Dr. N.M. Veeraiyan. He was a visionary dentist with a mission to create a model dental college in the country. Today, under the dynamic leadership of our Pro-chancellor, Dr. Deepak Nallaswamy, we are achieving global recognition. Our dental college is ranked World No. 1 in h-index and citations among QS World Rankings. We have successfully retained the No:1 position for the third consecutive year in the NIRF Rankings. We have secured the 8th position in the SCIMAGO rankings. We have surpassed the Harvard School of Dentistry in SCOPUS-indexed publications, establishing ourselves as the highest-publishing dental school in the world. We take immense pride in integrating research into our undergraduate curriculum, making it a routine activity for our students. Our commitment to pioneering interdisciplinary research is evident through our investment in advanced equipment, some of which are the first of their kind in Asia.

MM Publishers, an entrepreneurial incubation initiative, is dedicated to fostering scientific publications across various fields of dentistry. Our mission is to democratize the power of publishing, connecting authors and readers globally, and enabling the free exchange of ideas with flexibility and creativity. We are committed to advancing communities, patients, and society at large by supporting researchers and healthcare professionals in progressing science and improving health outcomes. Our high-quality educational resources are designed to prepare the citizens, policymakers, educators, and researchers of the future. We uphold principles of excellence and reliability, ensuring customer value and satisfaction. 

Our comprehensive range of open-access publishing options caters to authors and researchers across multiple dental specialties. We are proud to publish peer-reviewed, open-access e-journals that cover a wide array of dental disciplines, including Dental Anatomy, Oral Medicine and Radiology, Oral and Maxillofacial Surgery, Oral and Maxillofacial Pathology, Microbiology, Periodontology, Implantology, Prosthodontics, Pedodontics and Preventive Dentistry, Orthodontics, Public Health Dentistry, Oral Biology, Oral and Maxillofacial Orthopedics, and Restorative and Aesthetic Dentistry. Our journals aim to provide high-quality information and fresh perspectives on key topics, along with up-to-date coverage in various fields of dentistry. We have published five textbooks in Basic Medical Sciences till now.

As one of the few regional members on the prestigious Crossref Board, we are deeply committed to promoting research integrity and maximizing the discoverability of scholarly work. Our dedication to authentic research ensures that every student has the opportunity to contribute to the advancement of science.


{{% /accordion %}} {{% /accordion-section %}}


##  Tier 2, Large members (electing two seats)

<a id="apa"></a>
### Aaron Wood, Head, Product & Content Management, American Psychological Association
#### United States<br>

<img src="/images/board-governance/AaronWood-CrossrefBoard.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Aaron Wood's statements" %}}

### Personal statement <br>
I lead product development and strategy, as well as indexing and production systems, for the American Psychological Association, a publisher of books, journals, courseware, video, and discovery solutions with global reach. My experience in scholarly communications and publishing is broad and multinational. I have led metadata and technical services at academic libraries and consortia, streaming video and audio platforms, print and electronic book distribution, full-text journal and book production and distribution, and abstracting and indexing discovery solutions.   
 
I have been a member of the Crossref Board since 2022 and am committed to making data open and interoperable to benefit all stakeholders in scholarly communications. I have contributed to the development of national and international standards, including with W3C and NISO, as both an academic librarian and commercial and non-profit publisher, and continue to be involved in open standards development through the Standards and Technology Executive Committee of the International Association of Scientific, Technical, and Medical Publishers, including ongoing involvement in research integrity-, metadata-, and AI-related task forces and working groups.   

One of the ways I wish to contribute to Crossref and to improving scholarly infrastructure is by leveraging my experience in multiple disciplines, digital formats, and geographic regions for broad unbiased discovery and access to scholarly content. It is my conviction that all disciplines and formats of research and scholarly output, regardless of where they originate, should be equally discoverable, accessible, and interconnected. Crossref plays a central role in scholarly infrastructure, and I would be honored to continue to serve on its Board.  


### Organization statement
The American Psychological Association (APA) is the leading scientific and professional non-profit organization representing psychology. Its mission is to promote the advancement, communication, and application of psychological science and knowledge to benefit society and improve lives, with a focus on human rights, equity, and nondiscrimination for all peoples. APA supports and drives advocacy, policy, and science to achieve its mission, including through its leading journal, book, and abstract and indexing publishing program in psychology and the behavioral sciences.  

APA publishes over 90 journals in collaboration with national and international psychology societies and organizations, as well as academic, professional, and children’s books and the authoritative global index of psychological and behavioral science research, APA PsycInfo, covering a full range of commercial, non-profit, and multilanguage publishers and organizations.   
 
APA has devoted significant attention to diversity, equity, and inclusion (DEI) since the 1960s, with various boards, committees, and divisions focused on the concerns of marginalized groups. Recently, APA developed a DEI framework to ensure a systemic approach to organizational DEI and incorporate a DEI lens into all aspects of the organization. APA’s mission includes enabling a scholarly publishing program that is diverse, equitable, and inclusive as reflected in its composition, goals, policies, standards, operations, practices, culture, and climate. This program has resulted in publicly available DEI toolkits for journal editors, recommendations for research models and design, and reporting standards adopted across disciplines and organizational types.  

APA shares Crossref’s vision of a scholarly record that benefits and connects research and a global society. Open, rigorous, and transparent research is essential to protecting the integrity of the scholarly record. By continuing to work with Crossref through metadata deposits, standards guidance, and infrastructure building, APA believes its perspective on the Board as a leader in psychological science, publishing, and DEI can help Crossref make further advancements in open science and interoperability for the benefit of research, researchers, and an inclusive global society. 


{{% /accordion %}} {{% /accordion-section %}}


<a id="plos"></a>
### Dan Shanahan, Publishing Director, Public Library of Science (PLOS) 
#### United States<br>

<img src="/images/board-governance/DS-Headshot.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Dan Shanahan's statements" %}}


### Personal statement <br>
My career in scholarly publishing has spanned publishing, editorial and product management, providing me with a nuanced understanding of scholarly communications, as well as the trends and challenges that are influencing the evolving research landscape. In my current role as Publishing Director at Public Library of Science (PLOS), I lead cross-functional business units and partners in shaping and implementing the strategic development and expansion of PLOS’ journal portfolio.

I have deep experience in publishing innovation, having been Head of Product at F1000, leading the health sciences portfolio at BioMed Central (subsequently Springer Nature) and as Product Lead at Cochrane. Throughout my career I have worked closely with Crossref in multiple capacities including, supporting Advisory Groups and chairing the Linked Clinical Trials Working Group. I have similarly been closely involved in affiliated initiatives, for example chairing the Publishing Group for Metadata 2020, and as a Board Member for Open Trials.

I have considerable experience in not just bringing new innovations to market, but also driving adoption. Crossref has a fantastic record of innovative and impactful enhancements, which provide real value to the scholarly communications ecosystem. My skills and expertise put me in a strong position to support the organisation, ensuring that robust implementation plans achieve the necessary behavioural change and successful adoptions.

With the ever-expanding corpus of literature, I believe l that Crossref is uniquely placed to drive significant progress in discoverability, tackling other research objects, such as article recommendations, modularizing DOI metadata, and really targeting machine learners and AI, and I feel that my longstanding experience in the development of architecture, products and underlying infrastructure that promote and support high-quality structured metadata would ensure that I can make a meaningful contribution to this ambition. As a Board Member, I would be honoured to contribute to the Crossref ambition of enabling connections and giving greater context, particularly in all areas of discoverability.


### Organization statement
PLOS is a non-profit Open Access (OA) publisher, founded to empower researchers to make science immediately and publicly available online, without restrictions. In the last 20 years we have propelled the movement for OA alternatives to subscription journals. We established the first multi-disciplinary publication inclusive of all rigorous research regardless of novelty or impact, and we demonstrated the importance of open data availability.

We have been a member of Crossref since the launch of our first journal – PLOS Biology – in 2003, and PLOS remains deeply committed to Crossref’s principles of open metadata; we are highly ranked for completeness of metadata records and follow best practices as demonstrated by Crossref’s member participation reports.

Our goals haven’t changed and remain closely aligned with the strategic direction for Crossref. Our objective is to promote an equitable, global, collaborative Open Science future. The needs of the research community are changing, so new solutions need to be co-created with the communities themselves and all actors in the ecosystem. Crossref is in a unique position to achieve this, as a fundamental partner to researchers, funders and publishers globally, and working together with PLOS puts us in the best position to achieve our shared vision of a scholarly record that the global community can build on forever, for the benefit of everyone.

{{% /accordion %}} {{% /accordion-section %}}

<a id="taf"></a>
### Amanda Ward, Director of Open Research, Taylor and Francis
#### United Kingdom<br>

<img src="/images/board-governance/AW.jpg" alt="Candidate, organization" width="225px" class="img-responsive" />  

{{% accordion %}} {{% accordion-section "Amanda Ward's statements" %}}

### Personal statement <br>
As the Director of Open Research at Taylor & Francis, I would like to offer my expertise in open research and traditional publishing activities. I lead the strategic development of our Open Research journals program, ensuring it meets the evolving needs of the academic communities we serve and remains robust and future-proof.

With over 20 years of experience in academic publishing, I have held various roles in operations, technology, and editorial. My recent focus has been on enhancing information dissemination for researchers, initially at Springer Nature and now at Taylor & Francis. My knowledge of Crossref service is expansive: throughout my career, I have supported multiple Crossref initiatives, including DOIs, Similarity Check, FundRef, ORCID integration, and CrossMark.

I am keen to leverage my diverse perspectives and my vision for our community is to help Crossref maintain its position as a trusted intermediary and continue its support of the evolving landscape of academic publishing – particularly by focusing on metadata quality and transparency in research dissemination. I am especially interested in exploring new services that leverage artificial intelligence to improve metadata accuracy and safeguard the integrity of research records.

I understand the importance of research infrastructure in ensuring the discoverability and democratisation of research. My work with Crossref to date has been immensely rewarding, and I am keen to contribute even more purposefully to the development of future Crossref services and the impact this organisation has on the scholarly ecosystem.


### Organization statement
This is an exciting time for scholarly publishing. Technology is providing researchers with new opportunities to share their work quickly and perhaps more importantly, comprehensively. Furthermore, we're witnessing the emergence of new scholarly content providers, tailoring their services to the evolving needs of the academic community. Crossref's role in supporting this widening ecosystem remains evermore critical.

Taylor & Francis is a well-established publisher with extensive experience in scholarly publishing serving diverse communities. Our varied portfolio which includes F1000, Dove Medical Press and PeerJ along with a strong presence in the Humanities and Social Sciences (HSS) fields, grants us unique insights into a wide array of challenges faced by scholars.

We have a proven track record of innovation, evident in our data sharing policies across all our publications, support for non-traditional research outputs and peer-review models, involvement with preprint servers, and close collaboration with numerous research funders and institutions. These are all areas that clearly align with Crossref’s vision and priorities. 

As the scholarly eco-system continues to grow, the stability and cohesion provided by Crossref in linking content and facilitating the use and reuse of research becomes increasingly crucial. To aid Crossref in fulfilling its mission, it is key that its governing Board comprises a diverse mix of scholarly content publishers and providers. My comprehensive background, focus on the transition to open research coupled with Taylor & Francis’s commitment to placing researchers at the centre of publishing can offer a valuable perspective, representing both traditional and innovative publishing solutions that cater to a broad spectrum of researchers.

{{% /accordion %}} {{% /accordion-section %}}



