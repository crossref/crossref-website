+++
title = "Crossref Assigns 50 Millionth DOI"
date = "2011-11-15"
parent = "News"
weight = 1
+++

*15 November  2011*

**15 November, Cambridge, MA USA**—The number of Crossref Digital Object Identifiers (DOIs) assigned to scholarly documents has surpassed 50 million, in time for Crossref’s twelfth annual meeting today.  

The 50 millionth DOI has been identified as [http://dx.doi.org/10.3406/lsoc.1985.2030](http://dx.doi.org/10.3406/lsoc.1985.2030), a 1985 linguistics article from the journal Langage et Société, made available by the Persée Program, which has participated in Crossref since 2007.

Viviane Boulétreau of the Persée Program, said, “Although this article was published many years ago, it has only recently has been assigned a DOI as part of our ongoing project to digitize older scholarly material. Our participation in Crossref promotes the linking of our content to the international community of scientific resources and improves its visibility and citability for our audience of readers.”

“This article serves as an excellent example of the diversity of the content at Crossref,” noted Executive Director Ed Pentz. “Our members come from 67 nations and multiple disciplines including sciences, social sciences, and increasingly the arts and humanities. Persée is typical of the small, international publishers joining Crossref in great numbers and contributing valuable content to the interconnected web of scholarship.”

“I continue to be amazed at the subject breadth and the chronological depth of the coverage of member content at Crossref,” adds Crossref Board Chair Linda Beebe, Senior Director at the American Psychological Association. “This milestone provides another in a series of indicators that the scholarly citation linking system facilitated by Crossref is here to stay.”

The complete citation of the 50 millionth DOI is below:

Darré, Jean-Pierre, “Les dialogues entre agriculteurs (Etude comparative dans deux
villages français, Bretagne et Lauragais),” Dialogs between farmer (a comparative study between two French villages, Brittany and Lauragais), Langage et société, 1985. 33, pp. 43-64, http://dx.doi.org/10.3406/lsoc.1985.2030.

**About Crossref**
Crossref (http://www.crossref.org) is a not-for-profit membership association of publishers. Since its founding in 2000, Crossref has provided reference linking services for over 50 million content items, including journal articles, books and book chapters, conference proceedings, reference entries, technical reports, standards, and data sets. Crossref also provides additional services designed to improve trust in the scholarly communications process.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
