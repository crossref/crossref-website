+++
title = "KISTI registers 44,000 Crossref DOIs for Korean scholarly papers"
date = "2010-12-06"
parent = "News"
weight = 1
+++

*06 December 2010*

**Daejeon, Korea and Lynnfied, MA, USA, 6 December 2010**—The Korea Institute of Science and Technology Information (KISTI) has registered 44,000 items to the Crossref system and assigned 44,000 Digital Object Identifier (DOIs) for 87 academic journals from 64 Korean society publishers since becoming a Crossref Sponsoring Member in December 2007.

Seon Heui Choi, senior researcher of KISTI's Department of Knowledge Resources, noted, “Assigning Crossref DOIs to Korean journals increases their exposure and the total number of citations they receive. These papers now flow into the mainstream of international academic information distribution.”

Ed Pentz, Executive Director of Crossref, added, “As a Crossref Sponsoring Member, KISTI provides Crossref Content Registration and Reference Linking services to societies in Korea that would otherwise be difficult for us to support. KISTI serves as the main contact point with these important publishers on our behalf, and handles all deposits and linking. KISTI has followed Crossref best practices in implementing the KoreaScience landing page. Other publishers can now more easily discover these articles and link to them.”

KISTI also participates in the CrossCheck plagiarism screening service to protect the intellectual property of its Korean journals and societies. CrossCheck is powered by iThenticate and utilizes distinctive plagiarism verification technology. In addition, KISTI offers Crossref’s Cited -by linking service. This service shows the citation and reach information of Korean journals hosted by KISTI.

**About KISTI**
KISTI (http://www.kisti.re.kr) is a specialized institute that has provided science and technology information services to the public to promote Korean competitiveness since 1962. To maximize the value of customers by implementing customer-oriented information services, KISTI develops and secures local and foreign information resources, strategic information analysis, supercomputing and advanced information infrastructure.

---

Please contact [our communications team](mailto:news@crossref.org) with any questions.
