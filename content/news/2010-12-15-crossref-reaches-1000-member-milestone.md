+++
title = "Crossref Reaches 1000 Member Milestone"
date = "2010-12-15"
parent = "News"
weight = 1
+++

*15 December 2010*

**15 December 2010, Lynnfield, MA USA**—Crossref, the not-for-profit association of scholarly publishers that provides collaborative business and infrastructure services, has signed its 1000th voting member, St. Plum-Blossom Press of Melbourne, Australia. Voting members represent over 3200 publishers and societies that participate in Crossref’s Digital Object Identifier (DOI) reference linking system.

“Crossref’s membership is increasingly diverse,” notes Crossref Executive Director Edward Pentz. “Well over half of our participating publishers are not-for-profit. Most of our membership growth comes from small publishers all over the globe. In fact, our members now come from more than 50 countries. They employ all business models, from subscription access, to open access, to hybrid models.”

Crossref is working to expand its network of Sponsors around the world as a way of improving services to smaller international publishers. A recent agreement with the Organization of Open Access Publishers (OASPA), and long-standing arrangements with respected organizations in the developing world and in Asia allow publishers served by these organizations to take advantage of Crossref services at a lower cost and with local support. Crossref is working to expand such relationships in other regions to provide better service.

Participation in Crossref requires that publishers agree to assign Crossref DOIs, which are persistent links, to their current journal content. Members also agree to incorporate outbound links in the references of their journal content to content from other members. Crossref provides both the business and technological infrastructure to make reference linking possible among scholarly publishers, to the benefit of the researcher.

Many members go far beyond the minimum requirement of linking current journals by assigning Crossref DOIs to and creating reference links from other types of content, such as book titles and chapters, conference proceedings, and components like tables, images, and supplementary data.

Crossref’s services extend beyond reference linking, the service created by the organization in 1999. Crossref also provides content discovery through an OpenURL interface to allow libraries to look up Crossref DOIs and bibliographic metadata at no charge. It aids discovery and analysis of scholarly content by licensing bibliographic metadata to secondary publishers and providers of research tools through Crossref Metadata Services. Crossref also provides Cited-by Linking, showing readers what subsequently published content has cited a particular document.

Crossref’s newest service, CrossCheck, allows publishers to screen submissions for duplication against other scholarly and web content. The plagiarism screening tool, powered by iThenticate, has prevented numerous members from inadvertently publishing plagiarized content.

Crossref will soon launch the Crossmark service, which will help researchers identify the provenance of scholarly literature on the web and to easily determine if documents have been updated since published.

Crossref (http://www.crossref.org) is a not-for-profit membership association of publishers. Since its founding in 1999, Crossref has provided reference linking services for 44 million content items, including journal articles, conference proceedings, books, book chapters, reference entries, technical reports, standards, and data sets. Crossref's goal is to be a trusted collaborative organization with broad community connections; authoritative and innovative in support of a persistent, sustainable infrastructure for scholarly communication.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
