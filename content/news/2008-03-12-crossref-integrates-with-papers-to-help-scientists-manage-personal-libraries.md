+++
title = "Crossref Integrates with Papers to Help Scientists Manage Personal Libraries"
date = "2008-03-12"
parent = "News"
weight = 1
+++

*12 March 2008*

**Lynnfield, MA . March 12, 2008** -- Crossref, the multi-publisher linking platform, announced today that [Mekentosj](http://papersapp.com/), creator of Papers, had signed on as a Crossref affiliate in order to integrate DOIs and Crossref metadata into its services. Papers is an award-winning application for researchers that improves their Mac-based workflow for searching, downloading, and managing PDF articles.

Papers already uses the DOI as a standard way to identify and lookup scientific articles. With the new partnership, Papers will add a tighter integration with Crossref's OpenURL service to facilitate the discovery of both new and existing scientific publications. As a result of the Crossref integration, Papers can recognize the DOI in PDF files and on web-pages, and automatically retrieve the available bibliographic information, including title, authors and journal names, from Crossref's metadata database. With one click, this information is then added to the researcher's personal library, making scientific articles more accessible and manageable.

Papers' creator, Alexander Griekspoor, reports: “In future releases we plan on expanding the integration of the OpenURL and various other Crossref services -- including the new blogger tool -- to give researchers full access to the rich metadata that is available and add innovative ways to take full advantage of this information.”

**About Mekentosj**: Mekentosj BV is a Dutch software company that aims to create innovative Mac OS X applications for scientists, of which Papers is a prime example. The company has been awarded three Apple Design Awards to date.


Links and more info:

* Website: [Papers](http://papersapp.com/)


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
