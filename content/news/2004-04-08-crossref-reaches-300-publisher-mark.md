+++
title = "Crossref Reaches 300-Publisher Mark"
date = "2004-04-07"
parent = "News"
weight = 1
+++

*07 April 2004*

**LYNNFIELD, MA, April 7, 2004** -- Crossref, the cross-publisher reference linking service, announced today that it had signed on its 300th member. Crossref's rate of growth has nearly doubled in recent months, due to the new fee structures that took effect in January of this year. Over 50 publishers have joined Crossref since the start of 2004, including The American Academy of Pediatrics, Intel, The International Reading Association, Mars Informatics, Now Publishing, Research Information, The World Bank, among many other notable US and international publishers and societies.

Crossref has also signed on several new libraries and affiliates in 2004, including Nerac (www.nerac.com), the Connecticut-based research and information discovery service. The majority of Crossref's 10.5 million DOIs are assigned to journal articles, from over 9,500 journals. However, Crossref is also registering DOIs for a wide variety of record types, including several hundred thousand conference proceeding and book DOIs already in its database.

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
