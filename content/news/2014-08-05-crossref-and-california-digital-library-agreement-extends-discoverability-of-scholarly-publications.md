+++
title = 'Crossref and California Digital Library Agreement Extends Discoverability of Scholarly Publications'
date = "2014-08-05"
parent = "News"
weight = 1
+++

*5 August 2014*

**Lynnfield, MA and Oakland, CA, 5 August 2014** ---Crossref and the California Digital Library (CDL) have signed an agreement that opens a route for library publishers to participate in the scholarly communications hub created by Crossref. Through the agreement, publishers using CDL’s EZID digital identifier service now may choose to deposit the metadata for their content in the Crossref system. In this way they may use the full range of Crossref services, including search and discovery, persistent linking, tracking of funding and licensing information, text and data mining, and more. As a Crossref Sponsoring Member, EZID will handle the technical and administrative processes for its client publishers, reducing barriers to their Crossref participation.

CDL is a founding member of the DataCite consortium, which, like Crossref, is one of nine Digital Object Identifier (DOI) Registration Agencies (RAs). Crossref assigns DOIs to multiple record types including articles, books and book chapters, data, and many others. DataCite also assigns DOIs to data. A key difference between the two organizations is the primary communities they work with. DataCite assigns DOIs mainly for institutional and subject repositories, while Crossref is a membership organization of scholarly publishers. While both enable persistent identifiers for scholarly content, the two RAs provide different types of applications and services for that content.

“Our goal is to maintain and preserve the scholarly citation system. When many RAs assign DOIs and provide different services for them, ensuring the interoperability of DOI systems is crucial. We’re happy to be working with CDL to take this important step forward in integrating published scholarly documents from an increasingly diverse array of sources,” said Crossref Executive Director Ed Pentz. “By collaborating, EZID and Crossref make it easier for everyone to find information about and use services for scholarly content.”

"We're very pleased to see EZID working on behalf of its clients to create new partnerships that expand the reach and impact of research and scholarship,” said Rebecca Kennison, Director, Center for Digital Research and Scholarship, Columbia University Libraries/Information Services, a participant in EZID. “Sponsored memberships to Crossref through the CDL open up new possibilities for smaller organizations and those who work with scholars to innovate at the fringes of new publication models, integrating the future of online publication as it assumes various forms. No longer an either-or proposition, membership with EZID for the creation of long-term identifiers for datasets, text, and more now works seamlessly with the cross-linking and research-and-discovery systems with which our partners in research and scholarship are already familiar.”

“The partnership between EZID and Crossref presents an exciting opportunity for eScholarship and our [journal partners](http://escholarship.org/uc/search?smode=browse;browse-journal=yes),” said Catherine Mitchell, Director, Access & Publishing Group, California Digital Library. “The inclusion of our Open Access publications in new discovery services via Crossref, particularly its metadata feed, will dramatically increase the exposure of the important scholarship published on our platform.”

**About EZID and the California Digital Library**  
[EZID](http://ezid.cdlib.org/) (easy-eye-dee) makes it easy to create and manage unique, persistent identifiers for digital objects, ensuring their future discoverability. EZID is a service conceived, built, and maintained by the [California Digital Library (CDL)](http://www.cdlib.org/), founded by the University of California in 1997 to take advantage of emerging technologies that transform the way digital information is published and accessed. CDL has assembled one of the world's largest digital research libraries and has changed the ways that faculty, students, and researchers discover and access information. CDL is a founding member of the international [DataCite](http://www.datacite.org/) consortium, which is dedicated to providing the social and technical infrastructure to enable data sharing and publication.

**About Crossref**  
Crossref [(www.crossref.org)](/) serves as a digital hub for the scholarly communications community. A not-for profit membership organization of global scholarly publishers, Crossref's innovations shape the future of scholarly communications by fostering collaboration among multiple stakeholders. Crossref provides a wide spectrum of services for identifying, locating, linking to, and assessing the reliability and provenance of scholarly content.  



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
