+++
title = "Crossref Lowers Back-File Deposit Fee to 12 Cents, Creating Incentive for Registration of Older Publications"
date = "2007-08-09"
parent = "News"
weight = 1
+++

*09 August 2007*

**LYNNFIELD MA, August 9, 2007** -- Crossref, the non-profit publisher association and citation linking service, announced that it is lowering its fee for registration of back-file content. From now through the end of 2008, Crossref will charge only 12 cents per back-file record, as opposed to the 17-cent fee that is has charged for the past several years, amounting to a 30% discount.

The move comes as part of a larger push for Crossref to expand its already impressive coverage of older research publications. Of the 28 million-plus Digital Object Identifiers® (DOIs)® registered with Crossref to date, over 400,000 point to resources published in the 17th, 18th, or 19th centuries. The Oldenburg Epistle Dedicatory, dating from 1665 and published in the first volume of Philosophical Transactions of the Royal Society, is the earliest publication linkable via Crossref DOI.  

According to Amy Brand, Crossref’s Director of Business and Product Development, “several larger academic publishers are in the process of digitizing all of their archival material or have already done so, and Crossref would like to insure that its fee structure enables smaller publishers and societies to participate in the growing web of interlinked historical research content.”

Crossref will continue to charge a $1 fee for registration of current content items, where “current” refers to works published within the last three years. Crossref’s various Content Registration fees have remained unchanged or been reduced for several years running. Crossref registers approximately 15,000 unique content records each day.


_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
