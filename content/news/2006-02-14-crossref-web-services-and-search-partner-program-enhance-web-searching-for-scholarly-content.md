+++
title = "Crossref Web Services and Search Partner Program Enhance Web Searching for Scholarly Content"
date = "2006-02-14"
parent = "News"
weight = 1
+++

*14 February 2006*

**London, U.K., 30 November 2005** - Crossref, the independent cross-publisher linking service, announced today two forthcoming initiatives that will enhance the way search engines index scholarly content: **Crossref Web Services** and the **Search Partner Program**.

Crossref Web Services will create an easy-to-use tool for authorized search and web services partners to gather metadata to streamline web crawling. The Crossref metadata database contains records for the more than 18 million items from over 1,500 publishers, the majority of whom are expected to choose to participate in this optional new service. The Crossref Search Partner program provides standard terms of use for search engines, libraries, and other partners to use the metadata available from Crossref Web Services – terms that promote copyright compliance and the important role published works of record play in scholarly communication.

Crossref Web Services also provides search partners with a map to published scholarly content on the Web. In this way, it functions as a notification, or “ping”, mechanism for the publication of new content. Alerting crawlers to new or revised content to be indexed greatly reduces the need for ongoing re-crawling of publisher sites.  “Search engines want better ways to gather standard, cross-publisher metadata to enhance their search indexes. Publishers want to streamline the way they provide metadata to a growing number of partners. Crossref Web Services and the Search Partner Program fill this void,” said Ed Pentz, Executive Director of Crossref.  “With Crossref repurposing parts of its metadata database and using common protocols like standardized XML and OpenURL (and SOAP, RSS and other protocols in future), these services can significantly enhance indexes.”

Mr. Pentz continued, “Just as the core Crossref linking service removes the need for bilateral linking agreements among publishers, the Search Partner Program removes the need for bilateral agreements covering use of metadata between search engines and publishers. Libraries, search engines, publishers, and researchers will all benefit as the high-quality structured metadata collected by Crossref becomes an easily available web resource with template terms and conditions of use.”

These new services from Crossref will improve search results by enabling the use of the digital object identifier (DOI), in all search results. Crossref DOIs provide persistent links to scholarly content, helping users get to the authoritative, published version of the content they are searching for, even when the content changes location or ownership. Crossref’s OpenURL interface (one of the first OpenURL resolvers to be fully compliant with the new NISO OpenURL 1.0 standard) is already in place and in use by a growing number of libraries.

More information will be forthcoming in early 2006 when these exciting new services launch.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
