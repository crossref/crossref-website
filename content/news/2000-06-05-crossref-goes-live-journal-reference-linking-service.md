+++
title = "Crossref Goes Live: Journal Reference Linking Service Membership Roster Grows to 33 Publishers"
date = "2000-06-05"
parent = "News"
weight = 1
+++

*05 June 2000*

**BURLINGTON, Mass.--June 5, 2000**--Ed Pentz, Executive Director of Crossref, today announced that the first links enabled by the collaborative reference-linking service have been activated on the World Wide Web.

"Crossref is now well on its way to becoming the comprehensive source for linking journal articles. The reference links that researchers want and need are starting to appear," said Mr. Pentz.

Mr. Pentz reported that as of today, ten of the member publishers have submitted more than 1.3 million article records in about 2,700 journals to Crossref's Metadata Database. Submission of the records is the first step of the multi-step process; Crossref members then use this information to enable the links from references in their journal articles to other publishers' content. Approximately 1,100 journals now have live reference links enabled by Crossref; examples are Biochimica et Biophysica Acta (Elsevier), Brain Research (Elsevier), Cancer (Wiley), European Journal of Biochemistry (Blackwell), Journal of Comparative Neurology (Wiley), Journal of Investigative Dermatology (Blackwell), Journal of Molecular Biology (Academic), Journal of the Association for Research in Otolaryngology (Springer), and World Journal of Surgery (Springer). Crossref enabled links will appear in a few different formats, depending on the preferences of each publisher (for example, Crossref button or "Article" in html). In the coming weeks and months, a constantly increasing number of records will be submitted and live reference links will appear.  

According to Mr. Pentz, eleven publishers have recently joined Crossref, bringing the current total to 33 member publishers. These include:

* American Chemical Society
* American Geophysical Union
* American Physical Society
* Annual Reviews
* Association of Learned and Professional Society Publishers
* British Medical Journal
* CAB International
* Institution of Electrical Engineers (IEE)
* Mary Ann Liebert, Inc.
* The Royal Society
* Thieme Verlag

Active discussions are underway with many more scientific and scholarly primary journal publishers to expand this broad-based, industry-wide initiative, which was initially announced in November 1999.  

Crossref is operated under the aegis of the not-for-profit organization jointly formed by the member publishers and incorporated as PILA. Its Board of Directors includes Eric A. Swanson, John Wiley & Sons, Inc., Chairman (NYSE:JW.a); Pieter Bolman, Academic Press, Treasurer (NYSE:H); Michael Spinella, AAAS; Marc Brodsky, American Institute of Physics; John R. White, Association for Computing Machinery; John Strange, Blackwell Science; John Regazzi, Elsevier Science (NYSE:ENL)(NYSE:RUK); Anthony Durniak, IEEE; Jeffrey K. Smith, Kluwer Academic Publishers; Stefan Von Holtzbrinck, Nature Publishing Group; Martin Richardson, Oxford University Press; and Ruediger Gebauer, Springer-Verlag.

Crossref operates behind-the-scenes by enabling member publishers to add reference links to their online journals. Users of the online journals see, click on, and follow the links directly to the content; there is no visible Crossref interface. Publishers control access to their content. Crossref is run from a central facility operated by Publishers International Linking Association, Inc. (PILA), and utilizes the Digital Object Identifier (DOI) to ensure permanent links.

Once the service is fully launched, more than three million articles across thousands of journals will be linked through Crossref, and more than half a million more articles will be linked each year thereafter. Such linking will enhance the efficiency of browsing and reading the primary scientific and scholarly literature. It will enable readers to gain access to logically related articles with one or two clicks -- an objective widely accepted among researchers as a natural and necessary part of scientific and scholarly publishing in the digital age. For more information about Crossref, visit http://www.crossref.org.

CONTACT:
John Wiley & Sons (for Crossref)
Susan Spilka, 212/850-6147
[sspilka@wiley.com](mailto:sspilka@wiley.com)


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
