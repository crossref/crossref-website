+++
title = "The Royal Society Digital Archive Now Registered in Crossref"
date = "2007-01-22"
parent = "News"
weight = 1
+++

*22 January 2007*

**LYNNFIELD, MA -- January 22, 2007** Crossref, the reference-linking service for scholarly and professional literature, is pleased to announce that The Royal Society has registered all of its historical back-file content with Crossref. The Oldenburg Epistle Dedicatory, dating from 1665 and published in the first volume of Philosophical Transactions of the Royal Society, is now the earliest publication linkable via Crossref DOI.

In registering DOIs for its complete journal archive, the Royal Society joins several Crossref member publishers who have recently completed, or are in the midst of, vast retro-digitization initiatives, including Elsevier, Springer, Sage, Kluwer, Wiley,
Blackwell, and the American Association for the Advancement of Science, among others. JSTOR, another Crossref member, deposited over a million and a half DOIs in 2006. “As more and more historical scholarly content is digitized and cross-referenced, the online web of scholarship will continue to fill in and grow backwards in time. This creates truly exciting opportunities for historically oriented researchers,” according to Amy Brand, Crossref’s Director of Business and Product Development.

Some highlights of the Royal Society’s legacy content include:

An Account of an Experiment Made by Mr. Hook, of Preserving Animals Alive by Blowing through Their Lungs with Bellows -  
Robert Hooke, 1667  
[http://dx.doi.org/10.1098/rstl.1666.0043](http://dx.doi.org/10.1098/rstl.1666.0043)

A legendary kite experiment, drawing down lightning and showing its electrical nature - Benjamin Franklin, 1752  
[http://dx.doi.org/10.1098/rstl.1751.0032](http://dx.doi.org/10.1098/rstl.1751.0032)

The Complementary Structure of Deoxyribonucleic Acid - FHC Crick and JD Watson, 1954  
[http://dx.doi.org/10.1098/rspa.1954.0101](http://dx.doi.org/10.1098/rspa.1954.0101)

On the Hoyle-Narlikar Theory of Gravitation - SW Hawking, 1965  
[http://dx.doi.org/10.1098/rspa.1965.0146](http://dx.doi.org/10.1098/rspa.1965.0146)

The complete archive is available via [http://www.pubs.royalsoc.ac.uk/archive](https://web.archive.org/web/20061017091911/http://www.pubs.royalsoc.ac.uk/index.cfm?page=1373). It contains material from historically significant events such as the early experiments to demonstrate Newton's Law of Gravity and Harvey's theories of circulation, as well as the drawings that Charles Darwin produced while he was developing the theory of evolution.

**About The Royal Society** - The Royal Society is the independent scientific academy of the UK and the Commonwealth
dedicated to promoting excellence in science. The Society plays an influential role in national and international science policy
and supports developments in science engineering and technology in a wide range of ways.




---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
