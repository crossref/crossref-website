+++
title = "Crossref hits the books—deposits grow, guidelines released"
date = "2009-07-29"
parent = "News"
weight = 1
+++

*29 July 2009*

**29 July 2009, Lynnfield, MA USA**—For the second year in a row Crossref deposits for books are growing faster than any other record type in the reference linking system. As of July 2009, more than 1.8 million Crossref Digital Object Identifiers (DOIs) have been assigned for books. Each Crossref DOI represents a citable book title, chapter, or reference entry that can be used to link references from scholarly content. Book deposits range from monographs with a single Crossref DOI to massive reference works with tens of thousands of individual entries.

To encourage publishers to ramp up reference linking for scholarly books, and to explain how Crossref DOIs for books work, Crossref has published two documents. The first, Best Practices for Books, was created by Crossref’s Book Working Group. The second is a Frequently Asked Questions (FAQ) document explaining the relationship between Crossref DOIs and other DOI applications, such as the ISBN-A.

“We are very encouraged at the growth of books being used in Crossref reference linking,” said Michael Forster, Chair of Crossref’s Book Working Group, and Vice President and Associate Publishing Director, Physical Sciences, Scientific, Technical, Medical, and Scholarly Publishing, John Wiley & Sons, Inc. “We solicited feedback from the entire Crossref membership before finalizing these guidelines. Our goal is to encourage reference linking among books, proceedings, and journal content, and to enhance the discoverability of professional, reference, technical, and scholarly books.”

The best practices document, available at http://www.crossref.org/06members/best_practices_for_books.html, includes suggestions for improving reference matching results. It identifies minimum and recommended book metadata for deposits and queries in the Crossref system. Ways to handle editions and other types of versions, so important in book publishing, are also addressed.
Crossref’s FAQ explains the differences between Crossref DOIs used for reference linking and some new types of identifiers emerging for books. The document, available at http://www.crossref.org/06members/otherdoifaq.html, goes into detail about an application called the ISBN-A—ISBNs made actionable through the DOI System.

 “We wanted to make sure that people understand the purposes of these different applications of DOI technology,” said Crossref Executive Director Ed Pentz. “Crossref is a registration agency for the scholarly publishing community, and Crossref DOIs are designed to make citation linking possible in content published by this group. Other DOI applications serve the needs of other communities and provide other services, and we fully support making the different DOI applications interoperable.”

Almost 60 publishers have deposited Crossref DOIs for nearly 84,000 book titles since Crossref began accepting book deposits.

[More information about assigning DOIs to books and book reference linking](/education/content-registration/content-types-intro/books-and-chapters/) is available on the Crossref website.

**About Crossref**

Crossref (http://www.crossref.org) is a not-for-profit membership association founded and directed by publishers. Its mission is to enable easy identification and use of trustworthy electronic content by promoting the cooperative development and application of a sustainable infrastructure. Crossref provides reference linking services for 37 million content items.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
