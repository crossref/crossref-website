+++
title = "Crossref™ Launches Pilot Program of Crossref Search, Powered By Google"
date = "2004-02-28"
parent = "News"
weight = 1
+++

*28 April 2004*

_New Initiative Enables Cross-Publisher, Full-Text Searches of the Latest Medical and Scholarly Research_

**Lynnfield, MA, April 28, 2004** -- Crossref announced today a new initiative that enables users to search the full text of high-quality, peer-reviewed journal articles, conference proceedings, and other resources covering the full spectrum of scholarly research from nine leading publishers. Called Crossref Search, this new pilot program utilizes the collaborative environment of Crossref, the reference-linking service for scholarly publishing, and Google™ search technologies.

Crossref is very excited to work with Google on this pilot program. Researchers, scientists and librarians should find Crossref Search a valuable search tool,” said Ed Pentz, executive director of Crossref. “Now, researchers and students interested in mining published scholarship have immediate access to targeted, interdisciplinary and cross-publisher search on full text using the powerful and familiar Google technology,” Mr. Pentz continued. “Crossref Search, like Crossref itself, breaks down barriers between publishers on behalf of research and library communities.”

Crossref Search is available to all users, free of charge, on the websites of participating publishers, and encompasses current journal issues as well as back files. The results are delivered from the regular Google index but filter out everything except the participating publishers’ content, and will link to the content on publishers’ websites via DOIs (Digital Object Identifiers) or regular URLs. Crossref itself doesn’t host any content or perform searches – Crossref works behind the scenes with Google to facilitate the crawling of content on publishers’ sites and sets the policies and guidelines governing publisher participation in the initiative. As well as enabling Crossref Search, the partnership with Google also means that full-text content from the publishers is also referenced by the main Google.com index in its more general searches. Participating publishers, with links to the Crossref Search pages, are:

* American Physical Society (https://web.archive.org/web/20110717070405/http://prola.aps.org/xrs.html)

* Annual Reviews (http://arjournals.annualreviews.org/search/external)

* Association for Computing Machinery (https://web.archive.org/web/20060501081454/http://portal.acm.org/xrs.cfm)

* Blackwell Publishing [_Editor's update: Link to blackwellsynergy.com broken and removed, January 2021_]

* Institute of Physics Publishing (https://web.archive.org/web/20110309164626/http://iopscience.iop.org/search)

* International Union of Crystallography (http://journals.iucr.org/ --click “search” and scroll down the page)

* Nature Publishing Group (https://web.archive.org/web/20040603053209/http://www.nature.com/dynasearch/app/dynasearch.taf)

* Oxford University Press (https://web.archive.org/web/20041204100259/http://hmg.oupjournals.org/search.dtl -- each journal’s search page includes a link)

* John Wiley & Sons, Inc. (http://www3.interscience.wiley.com/crossref.html)

The Crossref Search pilot will run through 2004 to evaluate functionality and to gather feedback from scientists, scholars and librarians for the purpose of fine-tuning the program. Participating publishers are also investigating how DOIs can be used to improve indexing of content and enable persistent links from search results to the full text of content at publishers’ sites. Crossref is also in discussion with other search engines.

Crossref is an independent membership association (currently it has 300 members), founded and directed by publishers. Its general mission is to facilitate access to published scholarship through collaborative technologies. Specifically, Crossref operates a cross-publisher citation linking system that enables a researcher to click on a reference citation in a journal on one publisher’s platform and link to the cited article at another publisher’s platform. In this way, Crossref functions as a sort of digital switchboard. It holds no full text content, but rather effects linkages through DOIs (Digital Object Identifiers), which are tagged to article metadata supplied by the participating publishers. A DOI allows for persistent linking, because once material has been given a DOI it never changes, unlike a URL which becomes obsolete when it is moved. The end result is an efficient, scalable linking system. More information about Crossref is available at http://www.crossref.org.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
