+++
title = "Crossref to accept preprints in change to long-standing policy"
date = "2015-05-05"
parent = "News"
weight = 5
+++

*05 May 2015*
_Crossref will enable members to register preprints in order to clarify the scholarly citation record and better support the changing publishing models of its members_

Crossref will enable registration for preprints by August 2016. Crossref's original registration policy prevented its members from registering content and assigning Digital Object Identifiers (DOIs) to "duplicative works." The new policy means that preprints will have separate DOIs from any later versions that may be registered, according to an announcement from the organization.

Preprint is a term that has several meanings, but in the proposal adopted by Crossref's Board of Directors it will be defined for Crossref purposes as "original content which is intended for formal publication, including content that has been submitted, but has not yet been accepted for publication."

Under the new Crossref policy, each preprint will be required to link to any future related versions of the work. Crossref will provide tools to make it easier for members to do that. This process may involve more than one publisher. For example, the publisher of a preprint and the publisher of any subsequent related works may be different.

Geoffrey Bilder, Director of Strategic Initiatives, says "A number of Crossref members are exploring publishing workflows that blur the historically hard distinctions between a draft manuscript, a preprint, a revised proof, an accepted manuscript, the version-of-record, and subsequent corrections and updates, any of which may be used and cited at almost any point in the publishing process."

Ed Pentz, Executive Director, says "Adapting to the needs of our members, while remaining neutral toward their business models, is critical to Crossref's fundamental ability to maintain a clear citation record and let researchers easily identify the best available version of a document or research object."

Community Comments:

> Richard Sever, Co-Founder, bioRxiv at Cold Spring Harbor Laboratory Press: "A great initiative from Crossref that will allow readers to distinguish the different versions of an article and navigate between them."

> Neil Jacobs, head of scholarly communications support at Jisc: "This is another example of Crossref playing an increasing important and highly constructive role in scholarly communication. There is a growth in sharing early versions of research papers, and the scholarly record risks being incomplete without a reliable way for these versions to be identified. Crossref has helpfully spotted and provided a solution to address this risk."

> Ian Bannerman, Managing Director, Taylor & Francis Journals: "This is an elegant solution to a thorny problem. By requiring preprints to link to any future related versions, Crossref can offer a fuller picture of the history of a research work. Crossref's robust infrastructure will ensure that these links are persistent and reliable."

---

Please contact [our communications team](mailto:news@crossref.org) with any questions.
