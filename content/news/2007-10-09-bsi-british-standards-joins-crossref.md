+++
title = "BSI British Standards Joins Crossref"
date = "2007-10-09"
parent = "News"
weight = 1
+++

*09 October 2007*

**LYNNFIELD MA, October 9, 2007** -- Crossref is pleased to announce that BSI British Standards (http://www.bsigroup.com/britishstandards) has joined as a new Crossref member. As a member, subject to the creation of an appropriate protocol, BSI will be able to register standards with Crossref and implement interlinking with other scholarly and scientific publications. This will be achieved through use of the DOI® (Digital Object Identifier) System for which Crossref provides Content Registration services. Crossref includes hundreds of publishers and societies, with 28.9 million content items registered to date. BSI British Standards has 27,000 current standards.

Crossref and BSI British Standards will join forces to establish a working group of interested parties, made up of both Crossref members and outside organizations, to discuss best practice for assigning DOI names to standards. Ed Pentz, Executive Director of Crossref, said, “The Crossref system currently supports the registration of standards and the partnership between Crossref and BSI seeks to improve this by strengthening versioning and dissemination  of standards around the world. Crossref and BSI British Standards aim to make standards more visible, provide a common way to cite standards, and enable linking between standards and the rest of the scholarly literature.” Vincent Cassidy, Commercial Director of BSI Business Standards, said, “Standards are often link-rich documents, and BSI British Standards is keen to work with Crossref to unlock this value for customers”.  For further information or to participate please contact BSI British Standards at [britishstandards@bsigroup.com](mailto:britishstandards@bsigroup.com) or Crossref at [info@crossref.org](mailto:info@crossref.org).

About BSI British Standards: BSI British Standards is the UK’s national standards organization, recognized globally for its independence, integrity and innovation in the production of standards and information products that promote and share best practices. BSI British Standards works with businesses, consumers and government to represent UK interests and to make sure that British, European and international standards are useful, relevant and authoritative.  For further information please visit http://www.bsigroup.com/britishstandards.

BSI British Standards is part of BSI Group, a global independent business services organization that inspires confidence and delivers assurance to customers with standards-based solutions. Originating as the world’s first national standards body, the Group has over 2,250 staff operating in over 100 countries through more than 50 global offices.

The Group’s key offerings are:
* The development and sale of private, national and international standards and supporting information
* Second and third-party management systems assessment and certification
* Product testing and certification of services and products
* Performance management software solutions
* Training services in support of standards implementation and business best practice.

For further information please visit http://www.bsigroup.com.



_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
