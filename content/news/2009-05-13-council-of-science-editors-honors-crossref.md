+++
title = "Council of Science Editors Honors Crossref"
date = "2009-05-13"
parent = "News"
weight = 1
+++

*12 May 2009*

**Lynnfield, MA. May 13, 2009** -- The Council of Science Editors (CSE) bestowed its Meritorious Achievement Award on Crossref at its annual meeting in Pittsburgh last week.

“This is only the second time CSE has granted the award to an organization rather than to an individual,” commented Monica Bradford, Chair of the CSE Awards Committee. “We felt that the significant contributions Crossref has made in improving the experience for users of online scholarly material merited our highest recognition.”

“Crossref epitomizes the goal of the award,” added CSE President Diane Scott-Lichter, “which is to recognize the individual, and in this case the organization, that makes efforts to improve scientific communication through the pursuit of high standards. Crossref’s linking service and new initiatives such as CrossCheck are terrific examples of collaboration and cooperation in the scholarly community.”

“We are honored by CSE’s recognition of Crossref,” commented Crossref Executive Director Ed Pentz. “Both our staff and board members are very proud to receive this award from an organization that has done so much to encourage excellence in publishing. It comes at an especially auspicious time as we make plans to celebrate our 10th anniversary.”

**About Crossref**

Crossref (http://www.crossref.org) is a not-for-profit membership association founded and directed by publishers. Its mission is to enable easy identification and use of trustworthy electronic content by promoting the cooperative development and application of a sustainable infrastructure.

**About the Council of Science Editors**

CSE is a professional organization with 900 members dedicated to promoting excellence in the communication of scientific information. It was established in 1957 by joint action of the National Science Foundation and the American Institute of Biological Sciences. Today, it enjoys close relationships with a number of scientific publishing organizations, both national and international, but it functions autonomously, relying on the vigor of its members to attain the goals of the organization.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
