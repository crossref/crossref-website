+++
title = "Department of Energy grey Literature Now Registered with Crossref"
date = "2007-06-04"
parent = "News"
type = "News"
weight = 1
+++

*04 June 2007*
## Department of Energy grey Literature Now Registered with Crossref

**LYNNFIELD, MA. June 4, 2007** -- The U.S. Department of Energy and Crossref are very pleased to announce that the Office of Scientific and Technical Information (OSTI) has completed DOI ® registrations for more than 86,000 DOE technical reports with Crossref, the earliest report dating from 1933.

According to Ed Pentz, Crossref’s Executive Director, “Crossref’s discussions with OSTI about depositing technical reports began several years ago, and we are really thrilled that the mission is now accomplished. It marks a milestone in Crossref’s endeavor to establish a truly robust citation network, one that spans decades of research, different record types, and all disciplines.”

Dr. Walter Warnick, Director of OSTI, commented, “Coupling the vast resources available on OSTI’s Information Bridge with the capabilities of Crossref speeds access to DOE scientific and technical literature, placing it on the same footing as journal articles in terms of reference linking.”

About OSTI - The Office of Scientific and Technical Information (OSTI) is a U.S. Department of Energy (DOE) program within the Office of Science, the single largest supporter of basic research in the physical sciences in the United States. Since 1947, OSTI and its predecessor agencies have been nationally recognized for contributions to the sharing and exchange of science information. OSTI works to accelerate access to research results to speed the advancement of science and technology. DOE, one of the largest sponsors of R&D for the nation, supports thousands of research projects annually.


_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
