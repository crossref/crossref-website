+++
title = "Elsevier joins Crossref Cited-by-linking"
date = "2010-11-16"
parent = "News"
weight = 1
+++

*16 November 2010*

**16 November, London, UK**—Elsevier now participates in Crossref’s Cited-by Linking Service. The service allows Crossref member publishers to display links on an article page to other articles that cite that particular document. Elsevier joins over 200 publishers that deposit references from their scholarly content and display citing links from Crossref publishers back to that content.

Since August, Crossref DOI-based cited-by links have been added to over 10 million Elsevier articles on SciVerse ScienceDirect. The next step in Elsevier’s implementation will be to submit approximately 200 million references to Crossref from its large library of scholarly publications. Depositing these references will allow other Crossref members to display links to Elsevier articles that cite their content.

“Having Elsevier join Cited-by Linking is great news for researchers,” commented Ed Pentz, Crossref’s Executive Director. “As with all Crossref services, the more publishers who participate, the greater the benefit to those in the scholarly communications system. Adding the huge number of Elsevier articles and references to the system will greatly improve individual researchers’ ability to evaluate the importance of the literature they look at and to navigate to related research.”

"Elsevier has added Crossref Cited-by Linking to our existing navigation features on SciVerse ScienceDirect because we believe it will further improve researchers’ ability to search and discover scholarly content that is relevant to their work,” notes Chris Shillum, Vice President for Product Management, Platform and Content at Elsevier.  “We remain strongly committed to providing researchers access to trusted information tailored to their research needs, and this enhancement reflects that commitment.”

Crossref Cited-by Linking (previously known as Forward Linking) has grown steadily since its inception in 2006. Currently, almost 20% of the 44 million Crossref DOIs deposited also have their references available to create Cited-by Links. A total of more than 125 million Cited-by Links exist among participating publishers. Nearly 17 million Crossref DOIs have at least one Cited-by Link referencing them. Cited-by Linking is an optional service for Crossref members and is available at no additional cost. More information is available at the Crossref web site: http://www.crossref.org/citedby.html

**About Crossref**
Crossref (http://www.crossref.org) is a not-for-profit membership association of publishers. Since its founding in 1999, Crossref has provided reference linking services for 44 million content items, including journal articles, conference proceedings, books, book chapters, reference entries, technical reports, standards, and data sets. Crossref currently has 980 voting members, representing 3237 publishers. Almost 60% of Crossref publishers are not-for-profit organizations. Crossref also serves more than 1600 libraries and provides citation look-up services to a number of affiliated organizations.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
