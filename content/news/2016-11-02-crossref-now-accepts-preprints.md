+++
title = "Crossref now accepts preprints"
date = "2016-11-02"
parent = "News"
weight = 3
+++

_02 November 2016_
_Crossref completes infrastructure to support DOIs for preprints, and linking to best available version_

Crossref, the not-for-profit member organization for publishers, announced today that it is now accepting the registration of Crossref DOIs for preprints. The key decision is to acknowledge that preprints are a valuable part of the research story, and ensure that authors' own or institutional repository versions can be linked up with any eventual version on a publisher's site.

In July 2015, the Crossref board voted to change its membership rules to reflect inclusion of "pre-publication" content. Crossref already accepted working papers and technical reports.

In 2016, Crossref has been implementing changes to its systems to accept preprints. This will clearly identify them, and ensure they can be linked to the accepted manuscript.

This work will also ensure:

* links to these publications persist over time
* preprints are connected to the full history of the shared research results
* the citation record is clear and up-to-date

<img src="/images/services-images/preprints-nexus.png" alt="Preprints as part of the article nexus; as content evolves, connections persist and new links are added" width="100%">

As this graphic suggests, Crossref preprints will have their own nexus of connections, in terms of references and citations, with other preprints and articles. When these preprints become articles, they will then create another connection, to the article itself. The metadata will always maintain a clear record of connection between the new references and citations, and those they accrued as preprints.

Ed Pentz, Executive Director, said: "Based on the needs of our members the board approved adding preprints as a record type in the Crossref system. This enables Crossref to maintain a clear and comprehensive citation record and lets researchers more easily identify when different documents are related".

Martyn Rittman, from Preprints, operated by MDPI added: "Preprints.org is delighted to be the very first to integrate the Crossref schema for preprints. We believe it is an important step in allowing working papers and preliminary results to be fully citable as soon as they are available. It also makes it easy to link to the final peer-reviewed version, regardless of where it is published. Thanks to the hard work of Crossref and clear documentation, the schema was very simple to implement and has been applied retrospectively to all preprints at Preprints.org".

Jessica Polka, Director, ASAPbio said: "ASAPbio is a scientist-driven community initiative to promote the productive use of preprints in the life sciences. We're thrilled to see Crossref's development of a service that enables preprints to better contribute to the scholarly record. This infrastructure lays a necessary foundation for increasing acceptance of preprints as a valuable form of scientific communication among biologists".

---

Please contact [our communications team](mailto:news@crossref.org) with any questions.
