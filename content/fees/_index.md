+++
title = "Fees"
date = "2025-01-06"
draft = false
author = "Amanda Bartell"
toc = true
aliases = [
    "/02publishers/20pub_fees.html",
    "/04intermediaries/34affiliate_fees.html"
]
[menu.main]
rank = 5
weight = 40
parent = "Get involved"
+++

We have different fees for different kinds of community participation. These include annual membership fees and content registration fees for members, optional member fees for Similarity Check, and annual subscription fees for Metadata Plus.

Our [Membership and Fees Committee](/committees/membership-and-fees) regularly reviews our fees and makes recommendations to our Board. In July 2019 our board voted to approve our [fee principles](/operations-and-sustainability/#fee-principles).  We haven't increased the regular annual membership or content registration fees since 2004 years but please note that fees may be changed soon as part of the [Resourcing Crossref for Future Sustainability (RCFS) Program](/community/special-programs/resourcing-crossref/) that started in 2024.

## Member fees

If you want to get a Crossref DOI prefix for your organization so you can create unique and persistent links and distribute metadata through us, you’ll need to become a member. Most members pay an [annual fee](/fees/#annual-membership-fees) at the beginning of each calendar year plus a [one-time fee for each content item registered](/fees/#content-registration-fees). There are also fees for the [optional Similarity Check service](/fees/#similarity-check-fees).

### Annual membership fees

If you are a member through a sponsor, your sponsor will pay membership fees on your behalf (but they may also charge you for their services - [more here](/membership/about-sponsors)).

If you are eligible for the GEM programme, you will not pay annual membership fees - [more here](/gem).

All other members pay an annual membership fee to Crossref. There are different membership fee tiers depending on whether you will be registering published content with us, or whether you are a funder and will be registering grants. These fees are tiered, depending on your publishing revenues or expenses, or the value of the grants that you award.

 (IMPORTANT: your membership fee gives you access to our services, but DOES NOT include content registration. There are [separate content registration fees](/fees/#content-registration-fees) payable.)

#### Annual membership fee tiers (for organizations registering published content)

{{< snippet "/_snippet-sources/annual-membership-fees-non-funders.md" >}}

#### Annual membership fees (for funders who will be registering grants)

{{< snippet "/_snippet-sources/annual-membership-fees-funders.md" >}}

---

### Content Registration fees

{{< snippet "/_snippet-sources/content-registration-fees.md" >}}

---

### Similarity Check fees

Members are able to participate in Similarity Check. As a participant, you pay an annual service fee to use Similarity Check plus a per document charge each time you check a document. Even members who are part of the [GEM Program](/gem) will pay these fees.

#### Similarity Check annual service fee

{{< snippet "/_snippet-sources/similarity-check-annual-service-fees.md" >}}

#### Similarity Check per-document fees

{{< snippet "/_snippet-sources/similarity-check-per-document-fees.md" >}}

---

## Metadata Plus subscriber fees

{{< snippet "/_snippet-sources/metadata-plus-subscriber-fees.md" >}}

---

## Sponsor fees

{{< snippet "_snippet-sources/sponsoring-organization-fees.md" >}}

---

## How to pay us and other FAQs

There's more information about how to pay us, when you'll be billed and other billing FAQs [here](/members-area/all-about-invoicing-and-payment/).

 Do contact our [member support team](mailto:member@crossref.org) if you have any further questions.

---
