set -e

# Install Homebrew
if which brew; then
  echo "Homebrew installed OK"
else
  echo "Need to install Homebrew"
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

# Install Hugo -- check if an upgrade is available but do not upgrade automatically
if hugo version; then
  echo "Hugo already installed OK"
  brew upgrade -n hugo
else
  echo "Need to install Hugo"
  brew install hugo
fi

cd -- "$(dirname "$BASH_SOURCE")"

# Launch hugo site in the default browser
open "http://localhost:1313/" && hugo serve --config config.toml,config-local.toml
