//	Modified from http://javascript.internet.com/forms/check-email.html

isEmailAddress = function(emailStr) 
	{
	// http://data.iana.org/TLD/tlds-alpha-by-domain.txt
	// Version 2016040701, Last Updated Fri Apr  8 07:07:01 2016 UTC
	var tlds = [
		"AAA",
		"AARP",
		"ABB",
		"ABBOTT",
		"ABBVIE",
		"ABOGADO",
		"ABUDHABI",
		"AC",
		"ACADEMY",
		"ACCENTURE",
		"ACCOUNTANT",
		"ACCOUNTANTS",
		"ACO",
		"ACTIVE",
		"ACTOR",
		"AD",
		"ADAC",
		"ADS",
		"ADULT",
		"AE",
		"AEG",
		"AERO",
		"AF",
		"AFL",
		"AG",
		"AGENCY",
		"AI",
		"AIG",
		"AIRFORCE",
		"AIRTEL",
		"AL",
		"ALIBABA",
		"ALIPAY",
		"ALLFINANZ",
		"ALLY",
		"ALSACE",
		"AM",
		"AMICA",
		"AMSTERDAM",
		"ANALYTICS",
		"ANDROID",
		"ANQUAN",
		"AO",
		"APARTMENTS",
		"APP",
		"APPLE",
		"AQ",
		"AQUARELLE",
		"AR",
		"ARAMCO",
		"ARCHI",
		"ARMY",
		"ARPA",
		"ARTE",
		"AS",
		"ASIA",
		"ASSOCIATES",
		"AT",
		"ATTORNEY",
		"AU",
		"AUCTION",
		"AUDI",
		"AUDIO",
		"AUTHOR",
		"AUTO",
		"AUTOS",
		"AVIANCA",
		"AW",
		"AWS",
		"AX",
		"AXA",
		"AZ",
		"AZURE",
		"BA",
		"BAIDU",
		"BAND",
		"BANK",
		"BAR",
		"BARCELONA",
		"BARCLAYCARD",
		"BARCLAYS",
		"BAREFOOT",
		"BARGAINS",
		"BAUHAUS",
		"BAYERN",
		"BB",
		"BBC",
		"BBVA",
		"BCG",
		"BCN",
		"BD",
		"BE",
		"BEATS",
		"BEER",
		"BENTLEY",
		"BERLIN",
		"BEST",
		"BET",
		"BF",
		"BG",
		"BH",
		"BHARTI",
		"BI",
		"BIBLE",
		"BID",
		"BIKE",
		"BING",
		"BINGO",
		"BIO",
		"BIZ",
		"BJ",
		"BLACK",
		"BLACKFRIDAY",
		"BLOOMBERG",
		"BLUE",
		"BM",
		"BMS",
		"BMW",
		"BN",
		"BNL",
		"BNPPARIBAS",
		"BO",
		"BOATS",
		"BOEHRINGER",
		"BOM",
		"BOND",
		"BOO",
		"BOOK",
		"BOOTS",
		"BOSCH",
		"BOSTIK",
		"BOT",
		"BOUTIQUE",
		"BR",
		"BRADESCO",
		"BRIDGESTONE",
		"BROADWAY",
		"BROKER",
		"BROTHER",
		"BRUSSELS",
		"BS",
		"BT",
		"BUDAPEST",
		"BUGATTI",
		"BUILD",
		"BUILDERS",
		"BUSINESS",
		"BUY",
		"BUZZ",
		"BV",
		"BW",
		"BY",
		"BZ",
		"BZH",
		"CA",
		"CAB",
		"CAFE",
		"CAL",
		"CALL",
		"CAMERA",
		"CAMP",
		"CANCERRESEARCH",
		"CANON",
		"CAPETOWN",
		"CAPITAL",
		"CAR",
		"CARAVAN",
		"CARDS",
		"CARE",
		"CAREER",
		"CAREERS",
		"CARS",
		"CARTIER",
		"CASA",
		"CASH",
		"CASINO",
		"CAT",
		"CATERING",
		"CBA",
		"CBN",
		"CC",
		"CD",
		"CEB",
		"CENTER",
		"CEO",
		"CERN",
		"CF",
		"CFA",
		"CFD",
		"CG",
		"CH",
		"CHANEL",
		"CHANNEL",
		"CHASE",
		"CHAT",
		"CHEAP",
		"CHLOE",
		"CHRISTMAS",
		"CHROME",
		"CHURCH",
		"CI",
		"CIPRIANI",
		"CIRCLE",
		"CISCO",
		"CITIC",
		"CITY",
		"CITYEATS",
		"CK",
		"CL",
		"CLAIMS",
		"CLEANING",
		"CLICK",
		"CLINIC",
		"CLINIQUE",
		"CLOTHING",
		"CLOUD",
		"CLUB",
		"CLUBMED",
		"CM",
		"CN",
		"CO",
		"COACH",
		"CODES",
		"COFFEE",
		"COLLEGE",
		"COLOGNE",
		"COM",
		"COMMBANK",
		"COMMUNITY",
		"COMPANY",
		"COMPARE",
		"COMPUTER",
		"COMSEC",
		"CONDOS",
		"CONSTRUCTION",
		"CONSULTING",
		"CONTACT",
		"CONTRACTORS",
		"COOKING",
		"COOL",
		"COOP",
		"CORSICA",
		"COUNTRY",
		"COUPON",
		"COUPONS",
		"COURSES",
		"CR",
		"CREDIT",
		"CREDITCARD",
		"CREDITUNION",
		"CRICKET",
		"CROWN",
		"CRS",
		"CRUISES",
		"CSC",
		"CU",
		"CUISINELLA",
		"CV",
		"CW",
		"CX",
		"CY",
		"CYMRU",
		"CYOU",
		"CZ",
		"DABUR",
		"DAD",
		"DANCE",
		"DATE",
		"DATING",
		"DATSUN",
		"DAY",
		"DCLK",
		"DE",
		"DEALER",
		"DEALS",
		"DEGREE",
		"DELIVERY",
		"DELL",
		"DELOITTE",
		"DELTA",
		"DEMOCRAT",
		"DENTAL",
		"DENTIST",
		"DESI",
		"DESIGN",
		"DEV",
		"DIAMONDS",
		"DIET",
		"DIGITAL",
		"DIRECT",
		"DIRECTORY",
		"DISCOUNT",
		"DJ",
		"DK",
		"DM",
		"DNP",
		"DO",
		"DOCS",
		"DOG",
		"DOHA",
		"DOMAINS",
		"DOWNLOAD",
		"DRIVE",
		"DUBAI",
		"DURBAN",
		"DVAG",
		"DZ",
		"EARTH",
		"EAT",
		"EC",
		"EDEKA",
		"EDU",
		"EDUCATION",
		"EE",
		"EG",
		"EMAIL",
		"EMERCK",
		"ENERGY",
		"ENGINEER",
		"ENGINEERING",
		"ENTERPRISES",
		"EPSON",
		"EQUIPMENT",
		"ER",
		"ERNI",
		"ES",
		"ESQ",
		"ESTATE",
		"ET",
		"EU",
		"EUROVISION",
		"EUS",
		"EVENTS",
		"EVERBANK",
		"EXCHANGE",
		"EXPERT",
		"EXPOSED",
		"EXPRESS",
		"EXTRASPACE",
		"FAGE",
		"FAIL",
		"FAIRWINDS",
		"FAITH",
		"FAMILY",
		"FAN",
		"FANS",
		"FARM",
		"FASHION",
		"FAST",
		"FEEDBACK",
		"FERRERO",
		"FI",
		"FILM",
		"FINAL",
		"FINANCE",
		"FINANCIAL",
		"FIRESTONE",
		"FIRMDALE",
		"FISH",
		"FISHING",
		"FIT",
		"FITNESS",
		"FJ",
		"FK",
		"FLICKR",
		"FLIGHTS",
		"FLORIST",
		"FLOWERS",
		"FLSMIDTH",
		"FLY",
		"FM",
		"FO",
		"FOO",
		"FOOTBALL",
		"FORD",
		"FOREX",
		"FORSALE",
		"FORUM",
		"FOUNDATION",
		"FOX",
		"FR",
		"FRESENIUS",
		"FRL",
		"FROGANS",
		"FRONTIER",
		"FUND",
		"FURNITURE",
		"FUTBOL",
		"FYI",
		"GA",
		"GAL",
		"GALLERY",
		"GALLO",
		"GALLUP",
		"GAME",
		"GARDEN",
		"GB",
		"GBIZ",
		"GD",
		"GDN",
		"GE",
		"GEA",
		"GENT",
		"GENTING",
		"GF",
		"GG",
		"GGEE",
		"GH",
		"GI",
		"GIFT",
		"GIFTS",
		"GIVES",
		"GIVING",
		"GL",
		"GLASS",
		"GLE",
		"GLOBAL",
		"GLOBO",
		"GM",
		"GMAIL",
		"GMBH",
		"GMO",
		"GMX",
		"GN",
		"GOLD",
		"GOLDPOINT",
		"GOLF",
		"GOO",
		"GOOG",
		"GOOGLE",
		"GOP",
		"GOT",
		"GOV",
		"GP",
		"GQ",
		"GR",
		"GRAINGER",
		"GRAPHICS",
		"GRATIS",
		"GREEN",
		"GRIPE",
		"GROUP",
		"GS",
		"GT",
		"GU",
		"GUCCI",
		"GUGE",
		"GUIDE",
		"GUITARS",
		"GURU",
		"GW",
		"GY",
		"HAMBURG",
		"HANGOUT",
		"HAUS",
		"HDFCBANK",
		"HEALTH",
		"HEALTHCARE",
		"HELP",
		"HELSINKI",
		"HERE",
		"HERMES",
		"HIPHOP",
		"HITACHI",
		"HIV",
		"HK",
		"HM",
		"HN",
		"HOCKEY",
		"HOLDINGS",
		"HOLIDAY",
		"HOMEDEPOT",
		"HOMES",
		"HONDA",
		"HORSE",
		"HOST",
		"HOSTING",
		"HOTELES",
		"HOTMAIL",
		"HOUSE",
		"HOW",
		"HR",
		"HSBC",
		"HT",
		"HTC",
		"HU",
		"HYUNDAI",
		"IBM",
		"ICBC",
		"ICE",
		"ICU",
		"ID",
		"IE",
		"IFM",
		"IINET",
		"IL",
		"IM",
		"IMMO",
		"IMMOBILIEN",
		"IN",
		"INDUSTRIES",
		"INFINITI",
		"INFO",
		"ING",
		"INK",
		"INSTITUTE",
		"INSURANCE",
		"INSURE",
		"INT",
		"INTERNATIONAL",
		"INVESTMENTS",
		"IO",
		"IPIRANGA",
		"IQ",
		"IR",
		"IRISH",
		"IS",
		"ISELECT",
		"IST",
		"ISTANBUL",
		"IT",
		"ITAU",
		"IWC",
		"JAGUAR",
		"JAVA",
		"JCB",
		"JCP",
		"JE",
		"JETZT",
		"JEWELRY",
		"JLC",
		"JLL",
		"JM",
		"JMP",
		"JO",
		"JOBS",
		"JOBURG",
		"JOT",
		"JOY",
		"JP",
		"JPMORGAN",
		"JPRS",
		"JUEGOS",
		"KAUFEN",
		"KDDI",
		"KE",
		"KERRYHOTELS",
		"KERRYLOGISTICS",
		"KERRYPROPERTIES",
		"KFH",
		"KG",
		"KH",
		"KI",
		"KIA",
		"KIM",
		"KINDER",
		"KITCHEN",
		"KIWI",
		"KM",
		"KN",
		"KOELN",
		"KOMATSU",
		"KP",
		"KPMG",
		"KPN",
		"KR",
		"KRD",
		"KRED",
		"KUOKGROUP",
		"KW",
		"KY",
		"KYOTO",
		"KZ",
		"LA",
		"LACAIXA",
		"LAMBORGHINI",
		"LAMER",
		"LANCASTER",
		"LAND",
		"LANDROVER",
		"LANXESS",
		"LASALLE",
		"LAT",
		"LATROBE",
		"LAW",
		"LAWYER",
		"LB",
		"LC",
		"LDS",
		"LEASE",
		"LECLERC",
		"LEGAL",
		"LEXUS",
		"LGBT",
		"LI",
		"LIAISON",
		"LIDL",
		"LIFE",
		"LIFEINSURANCE",
		"LIFESTYLE",
		"LIGHTING",
		"LIKE",
		"LIMITED",
		"LIMO",
		"LINCOLN",
		"LINDE",
		"LINK",
		"LIVE",
		"LIVING",
		"LIXIL",
		"LK",
		"LOAN",
		"LOANS",
		"LOCUS",
		"LOL",
		"LONDON",
		"LOTTE",
		"LOTTO",
		"LOVE",
		"LR",
		"LS",
		"LT",
		"LTD",
		"LTDA",
		"LU",
		"LUPIN",
		"LUXE",
		"LUXURY",
		"LV",
		"LY",
		"MA",
		"MADRID",
		"MAIF",
		"MAISON",
		"MAKEUP",
		"MAN",
		"MANAGEMENT",
		"MANGO",
		"MARKET",
		"MARKETING",
		"MARKETS",
		"MARRIOTT",
		"MBA",
		"MC",
		"MD",
		"ME",
		"MED",
		"MEDIA",
		"MEET",
		"MELBOURNE",
		"MEME",
		"MEMORIAL",
		"MEN",
		"MENU",
		"MEO",
		"MG",
		"MH",
		"MIAMI",
		"MICROSOFT",
		"MIL",
		"MINI",
		"MK",
		"ML",
		"MM",
		"MMA",
		"MN",
		"MO",
		"MOBI",
		"MOBILY",
		"MODA",
		"MOE",
		"MOI",
		"MOM",
		"MONASH",
		"MONEY",
		"MONTBLANC",
		"MORMON",
		"MORTGAGE",
		"MOSCOW",
		"MOTORCYCLES",
		"MOV",
		"MOVIE",
		"MOVISTAR",
		"MP",
		"MQ",
		"MR",
		"MS",
		"MT",
		"MTN",
		"MTPC",
		"MTR",
		"MU",
		"MUSEUM",
		"MUTUAL",
		"MUTUELLE",
		"MV",
		"MW",
		"MX",
		"MY",
		"MZ",
		"NA",
		"NADEX",
		"NAGOYA",
		"NAME",
		"NATURA",
		"NAVY",
		"NC",
		"NE",
		"NEC",
		"NET",
		"NETBANK",
		"NETWORK",
		"NEUSTAR",
		"NEW",
		"NEWS",
		"NEXUS",
		"NF",
		"NG",
		"NGO",
		"NHK",
		"NI",
		"NICO",
		"NIKON",
		"NINJA",
		"NISSAN",
		"NISSAY",
		"NL",
		"NO",
		"NOKIA",
		"NORTHWESTERNMUTUAL",
		"NORTON",
		"NOWRUZ",
		"NP",
		"NR",
		"NRA",
		"NRW",
		"NTT",
		"NU",
		"NYC",
		"NZ",
		"OBI",
		"OFFICE",
		"OKINAWA",
		"OM",
		"OMEGA",
		"ONE",
		"ONG",
		"ONL",
		"ONLINE",
		"OOO",
		"ORACLE",
		"ORANGE",
		"ORG",
		"ORGANIC",
		"ORIGINS",
		"OSAKA",
		"OTSUKA",
		"OVH",
		"PA",
		"PAGE",
		"PAMPEREDCHEF",
		"PANERAI",
		"PARIS",
		"PARS",
		"PARTNERS",
		"PARTS",
		"PARTY",
		"PASSAGENS",
		"PE",
		"PET",
		"PF",
		"PG",
		"PH",
		"PHARMACY",
		"PHILIPS",
		"PHOTO",
		"PHOTOGRAPHY",
		"PHOTOS",
		"PHYSIO",
		"PIAGET",
		"PICS",
		"PICTET",
		"PICTURES",
		"PID",
		"PIN",
		"PING",
		"PINK",
		"PIZZA",
		"PK",
		"PL",
		"PLACE",
		"PLAY",
		"PLAYSTATION",
		"PLUMBING",
		"PLUS",
		"PM",
		"PN",
		"POHL",
		"POKER",
		"PORN",
		"POST",
		"PR",
		"PRAXI",
		"PRESS",
		"PRO",
		"PROD",
		"PRODUCTIONS",
		"PROF",
		"PROMO",
		"PROPERTIES",
		"PROPERTY",
		"PROTECTION",
		"PS",
		"PT",
		"PUB",
		"PW",
		"PWC",
		"PY",
		"QA",
		"QPON",
		"QUEBEC",
		"QUEST",
		"RACING",
		"RE",
		"READ",
		"REALTOR",
		"REALTY",
		"RECIPES",
		"RED",
		"REDSTONE",
		"REDUMBRELLA",
		"REHAB",
		"REISE",
		"REISEN",
		"REIT",
		"REN",
		"RENT",
		"RENTALS",
		"REPAIR",
		"REPORT",
		"REPUBLICAN",
		"REST",
		"RESTAURANT",
		"REVIEW",
		"REVIEWS",
		"REXROTH",
		"RICH",
		"RICOH",
		"RIO",
		"RIP",
		"RO",
		"ROCHER",
		"ROCKS",
		"RODEO",
		"ROOM",
		"RS",
		"RSVP",
		"RU",
		"RUHR",
		"RUN",
		"RW",
		"RWE",
		"RYUKYU",
		"SA",
		"SAARLAND",
		"SAFE",
		"SAFETY",
		"SAKURA",
		"SALE",
		"SALON",
		"SAMSUNG",
		"SANDVIK",
		"SANDVIKCOROMANT",
		"SANOFI",
		"SAP",
		"SAPO",
		"SARL",
		"SAS",
		"SAXO",
		"SB",
		"SBS",
		"SC",
		"SCA",
		"SCB",
		"SCHAEFFLER",
		"SCHMIDT",
		"SCHOLARSHIPS",
		"SCHOOL",
		"SCHULE",
		"SCHWARZ",
		"SCIENCE",
		"SCOR",
		"SCOT",
		"SD",
		"SE",
		"SEAT",
		"SECURITY",
		"SEEK",
		"SELECT",
		"SENER",
		"SERVICES",
		"SEVEN",
		"SEW",
		"SEX",
		"SEXY",
		"SFR",
		"SG",
		"SH",
		"SHARP",
		"SHAW",
		"SHELL",
		"SHIA",
		"SHIKSHA",
		"SHOES",
		"SHOUJI",
		"SHOW",
		"SHRIRAM",
		"SI",
		"SINA",
		"SINGLES",
		"SITE",
		"SJ",
		"SK",
		"SKI",
		"SKIN",
		"SKY",
		"SKYPE",
		"SL",
		"SM",
		"SMILE",
		"SN",
		"SNCF",
		"SO",
		"SOCCER",
		"SOCIAL",
		"SOFTBANK",
		"SOFTWARE",
		"SOHU",
		"SOLAR",
		"SOLUTIONS",
		"SONG",
		"SONY",
		"SOY",
		"SPACE",
		"SPIEGEL",
		"SPOT",
		"SPREADBETTING",
		"SR",
		"SRL",
		"ST",
		"STADA",
		"STAR",
		"STARHUB",
		"STATEFARM",
		"STATOIL",
		"STC",
		"STCGROUP",
		"STOCKHOLM",
		"STORAGE",
		"STORE",
		"STREAM",
		"STUDIO",
		"STUDY",
		"STYLE",
		"SU",
		"SUCKS",
		"SUPPLIES",
		"SUPPLY",
		"SUPPORT",
		"SURF",
		"SURGERY",
		"SUZUKI",
		"SV",
		"SWATCH",
		"SWISS",
		"SX",
		"SY",
		"SYDNEY",
		"SYMANTEC",
		"SYSTEMS",
		"SZ",
		"TAB",
		"TAIPEI",
		"TALK",
		"TAOBAO",
		"TATAMOTORS",
		"TATAR",
		"TATTOO",
		"TAX",
		"TAXI",
		"TC",
		"TCI",
		"TD",
		"TEAM",
		"TECH",
		"TECHNOLOGY",
		"TEL",
		"TELECITY",
		"TELEFONICA",
		"TEMASEK",
		"TENNIS",
		"TF",
		"TG",
		"TH",
		"THD",
		"THEATER",
		"THEATRE",
		"TICKETS",
		"TIENDA",
		"TIFFANY",
		"TIPS",
		"TIRES",
		"TIROL",
		"TJ",
		"TK",
		"TL",
		"TM",
		"TMALL",
		"TN",
		"TO",
		"TODAY",
		"TOKYO",
		"TOOLS",
		"TOP",
		"TORAY",
		"TOSHIBA",
		"TOTAL",
		"TOURS",
		"TOWN",
		"TOYOTA",
		"TOYS",
		"TR",
		"TRADE",
		"TRADING",
		"TRAINING",
		"TRAVEL",
		"TRAVELERS",
		"TRAVELERSINSURANCE",
		"TRUST",
		"TRV",
		"TT",
		"TUBE",
		"TUI",
		"TUNES",
		"TUSHU",
		"TV",
		"TVS",
		"TW",
		"TZ",
		"UA",
		"UBS",
		"UG",
		"UK",
		"UNICOM",
		"UNIVERSITY",
		"UNO",
		"UOL",
		"US",
		"UY",
		"UZ",
		"VA",
		"VACATIONS",
		"VANA",
		"VC",
		"VE",
		"VEGAS",
		"VENTURES",
		"VERISIGN",
		"VERSICHERUNG",
		"VET",
		"VG",
		"VI",
		"VIAJES",
		"VIDEO",
		"VIG",
		"VIKING",
		"VILLAS",
		"VIN",
		"VIP",
		"VIRGIN",
		"VISION",
		"VISTA",
		"VISTAPRINT",
		"VIVA",
		"VLAANDEREN",
		"VN",
		"VODKA",
		"VOLKSWAGEN",
		"VOTE",
		"VOTING",
		"VOTO",
		"VOYAGE",
		"VU",
		"VUELOS",
		"WALES",
		"WALTER",
		"WANG",
		"WANGGOU",
		"WATCH",
		"WATCHES",
		"WEATHER",
		"WEATHERCHANNEL",
		"WEBCAM",
		"WEBER",
		"WEBSITE",
		"WED",
		"WEDDING",
		"WEIBO",
		"WEIR",
		"WF",
		"WHOSWHO",
		"WIEN",
		"WIKI",
		"WILLIAMHILL",
		"WIN",
		"WINDOWS",
		"WINE",
		"WME",
		"WOLTERSKLUWER",
		"WORK",
		"WORKS",
		"WORLD",
		"WS",
		"WTC",
		"WTF",
		"XBOX",
		"XEROX",
		"XIHUAN",
		"XIN",
		"XN--11B4C3D",
		"XN--1CK2E1B",
		"XN--1QQW23A",
		"XN--30RR7Y",
		"XN--3BST00M",
		"XN--3DS443G",
		"XN--3E0B707E",
		"XN--3PXU8K",
		"XN--42C2D9A",
		"XN--45BRJ9C",
		"XN--45Q11C",
		"XN--4GBRIM",
		"XN--55QW42G",
		"XN--55QX5D",
		"XN--6FRZ82G",
		"XN--6QQ986B3XL",
		"XN--80ADXHKS",
		"XN--80AO21A",
		"XN--80ASEHDB",
		"XN--80ASWG",
		"XN--8Y0A063A",
		"XN--90A3AC",
		"XN--90AIS",
		"XN--9DBQ2A",
		"XN--9ET52U",
		"XN--9KRT00A",
		"XN--B4W605FERD",
		"XN--BCK1B9A5DRE4C",
		"XN--C1AVG",
		"XN--C2BR7G",
		"XN--CCK2B3B",
		"XN--CG4BKI",
		"XN--CLCHC0EA0B2G2A9GCD",
		"XN--CZR694B",
		"XN--CZRS0T",
		"XN--CZRU2D",
		"XN--D1ACJ3B",
		"XN--D1ALF",
		"XN--E1A4C",
		"XN--ECKVDTC9D",
		"XN--EFVY88H",
		"XN--ESTV75G",
		"XN--FCT429K",
		"XN--FHBEI",
		"XN--FIQ228C5HS",
		"XN--FIQ64B",
		"XN--FIQS8S",
		"XN--FIQZ9S",
		"XN--FJQ720A",
		"XN--FLW351E",
		"XN--FPCRJ9C3D",
		"XN--FZC2C9E2C",
		"XN--G2XX48C",
		"XN--GCKR3F0F",
		"XN--GECRJ9C",
		"XN--H2BRJ9C",
		"XN--HXT814E",
		"XN--I1B6B1A6A2E",
		"XN--IMR513N",
		"XN--IO0A7I",
		"XN--J1AEF",
		"XN--J1AMH",
		"XN--J6W193G",
		"XN--JLQ61U9W7B",
		"XN--JVR189M",
		"XN--KCRX77D1X4A",
		"XN--KPRW13D",
		"XN--KPRY57D",
		"XN--KPU716F",
		"XN--KPUT3I",
		"XN--L1ACC",
		"XN--LGBBAT1AD8J",
		"XN--MGB9AWBF",
		"XN--MGBA3A3EJT",
		"XN--MGBA3A4F16A",
		"XN--MGBAAM7A8H",
		"XN--MGBAB2BD",
		"XN--MGBAYH7GPA",
		"XN--MGBB9FBPOB",
		"XN--MGBBH1A71E",
		"XN--MGBC0A9AZCG",
		"XN--MGBCA7DZDO",
		"XN--MGBERP4A5D4AR",
		"XN--MGBPL2FH",
		"XN--MGBT3DHD",
		"XN--MGBTX2B",
		"XN--MGBX4CD0AB",
		"XN--MIX891F",
		"XN--MK1BU44C",
		"XN--MXTQ1M",
		"XN--NGBC5AZD",
		"XN--NGBE9E0A",
		"XN--NODE",
		"XN--NQV7F",
		"XN--NQV7FS00EMA",
		"XN--NYQY26A",
		"XN--O3CW4H",
		"XN--OGBPF8FL",
		"XN--P1ACF",
		"XN--P1AI",
		"XN--PBT977C",
		"XN--PGBS0DH",
		"XN--PSSY2U",
		"XN--Q9JYB4C",
		"XN--QCKA1PMC",
		"XN--QXAM",
		"XN--RHQV96G",
		"XN--ROVU88B",
		"XN--S9BRJ9C",
		"XN--SES554G",
		"XN--T60B56A",
		"XN--TCKWE",
		"XN--UNUP4Y",
		"XN--VERMGENSBERATER-CTB",
		"XN--VERMGENSBERATUNG-PWB",
		"XN--VHQUV",
		"XN--VUQ861B",
		"XN--W4R85EL8FHU5DNRA",
		"XN--WGBH1C",
		"XN--WGBL6A",
		"XN--XHQ521B",
		"XN--XKC2AL3HYE2A",
		"XN--XKC2DL3A5EE0H",
		"XN--Y9A3AQ",
		"XN--YFRO4I67O",
		"XN--YGBI2AMMX",
		"XN--ZFR164B",
		"XPERIA",
		"XXX",
		"XYZ",
		"YACHTS",
		"YAHOO",
		"YAMAXUN",
		"YANDEX",
		"YE",
		"YODOBASHI",
		"YOGA",
		"YOKOHAMA",
		"YOU",
		"YOUTUBE",
		"YT",
		"YUN",
		"ZA",
		"ZARA",
		"ZERO",
		"ZIP",
		"ZM",
		"ZONE",
		"ZUERICH",
		"ZW" ];
	/*
	if (ao__emailRolePat == undefined)
		var ao__emailRolePat =/^(abuse|admin|administrator|anti-spam|antispam|billing|contact|customerservice|designer|info|hostmaster|lawyer|mail-daemon|mail-deamon|marketing|no-reply|noreplies|noreply|nospam|postmaster|returns|root|sales|spam|support)$/
	*/

	//	The following pattern is used to check if the entered email address
 	//	fits the user@domain format.  It also is used to separate the username
  	//	from the domain.
	var emailPat=/^(.+)@(.+)$/i
	
	//	The following string represents the pattern for matching all special
	//	characters.  We don't want to allow special characters in the address. 
	//	This includes ( ) < > @ , ; : \ " . [ ] */ and non-english characters
	var specialChars="\\(\\)<>@,;:¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ\\\\\\\"\\.\\[\\]"

	//	The following string represents the range of characters allowed in a 
	//	username or domainname.  It really states which chars aren't allowed. */
	var validChars="\[^\\s" + specialChars + "\]"
	
	//	The following pattern applies if the "user" is a quoted string (in
	//	which case, there are no rules about which characters are allowed
	//	and which aren't; anything goes).  E.g. "jiminy cricket"@disney.com
	//	is a legal email address. */
	var quotedUser="(\"[^\"]*\")"
	
	//	The following pattern applies for domains that are IP addresses,
	//	rather than symbolic names.  E.g. joe@[123.124.233.4] is a legal
	//	email address. NOTE: The square brackets are required. */
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
	
	//	The following string represents an atom (basically a series of
	//	non-special characters.) */
	var atom=validChars + '+'
	
	//	The following string represents one word in the typical username.
	//	For example, in john.doe@somewhere.com, john and doe are words.
	//	Basically, a word is either an atom or quoted string. */
	var word="(" + atom + "|" + quotedUser + ")"
	
	// The following pattern describes the structure of the user
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
	
	//	The following pattern describes the structure of a normal symbolic
	//	domain, as opposed to ipDomainPat, shown above. */
	var domainPat=new RegExp(/^([a-zA-Z0-9][a-zA-Z0-9.-]*\.)[a-zA-Z]{2,}?$/)

	//	Finally, let's start trying to figure out if the supplied address is
	//	valid.

	//	Begin with the coarse pattern to simply break up user@domain into
	//	different pieces that are easy to analyze.
	var matchArray=emailStr.match(emailPat)
	if (matchArray==null) 
		{
	  	//	Too many/few @'s or something; basically, this address doesn't
	    //	even fit the general mould of a valid email address.
		return false
		}
	var user=matchArray[1]

	var domain=matchArray[2]

	//	See if "user" is valid 
	if (user.match(userPat)==null) 
		{
	    //	User is not valid
	    return false
		}

	/*
	if (user.match( ao__emailRolePat )!= null)
		{
		return false;
		}
	*/

	//	if the email address is at an IP address (as opposed to a symbolic
	//	host name) make sure the IP address is valid.
	var IPArray=domain.match(ipDomainPat)
	if (IPArray!=null) 
		{
	    // this is an IP address
		for (var i=1;i<=4;i++) 
			{
		    if (IPArray[i]>255) 
		    	{
				return false
			    }
	 		}
	    return true
		}

	//	Domain is symbolic name
	
	//	Special handling of localhost situation
	if (domain == "localhost")
		return true;
	
	var domainArray=domain.match(domainPat)
	if (domainArray==null) 
		{
	    return false
		}

	//	Now we need to break up the domain to get a count of how many atoms
	//	it consists of.

	var atomPat=new RegExp(atom,"g")
	var domArr=domain.match(atomPat)
	var len=domArr.length
	if (len >= 2)
		{
		var sfx = domArr [len-1].toUpperCase()
		for (var i = 0; i < tlds.length; ++i)
			if (sfx == tlds[i])
				return true;
		}

	return false;
	};

//	Field Validators

validateNonBlank = function (value)
	{
	return value.length > 0;
	};
	
validateNumber = function (value)
	{
	return ! isNaN (value);
	};

implicitValidateLength = function ()
	{
	// args: value len arg2 id
	var args = Array.prototype.slice.call(arguments); 
	var value = args[0];
	var len = args[1];

	if (typeof(value) == undefined) return true;
	if (typeof(len) == undefined) return true;

	return value.length < len+1;
	}

implicitValidateNumberRange = function ()
	{
	// args: value lowrange highrange id
	var args = Array.prototype.slice.call(arguments); 
	var value = args[0];
	var lowrange = args[1];
	var highrange = args[2];

	if (isNaN (value)) return false;

	if ((lowrange < value) && (value < hirange))
		return true;

	return false;
	};
	
implicitValidateConfirm = function ()
	{
	// args: value arg1 arg2 id
	var args = Array.prototype.slice.call(arguments); 

	var primaryInput = args[3];
	var secondaryInput = args[3]+'-confirm';

	return document.getElementById(primaryInput).value == document.getElementById(secondaryInput).value;
	};
	
implicitValidateDate = function ()
	{
	// args: value arg1 arg2 id
	var args = Array.prototype.slice.call(arguments); 

	var dateHidden = args[3];

	var dateOutputPattern = document.getElementById(dateHidden+'_pattern').value;

	var MM, dd, day, yy, yyyy;
	var MMField = document.getElementById(dateHidden+'_MM');		if (MMField && MMField.value)		MM = MMField.value;
	var ddField = document.getElementById(dateHidden+'_dd');		if (ddField && ddField.value)		dd = ddField.value;
	var dayField = document.getElementById(dateHidden+'_Day');		if (dayField && dayField.value)		dd = dayField.value;
	var yyField = document.getElementById(dateHidden+'_yy');		if (yyField && yyField.value)		yy = yyField.value;
	var yyyyField = document.getElementById(dateHidden+'_yyyy');	if (yyyyField && yyyyField.value)	yyyy = yyyyField.value;

	if (yyyy != null && yy == null) yy = yyyy % 100;

	if (yy != null && yyyy == null) yyyy = 2000+parseInt(yy);	// remember y2k?

	var daysInMonth = [ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

	if (yyyy != null && MM == 2)
		{
		var febDays;
		if ((yyyy % 400) == 0)
			febDays = 29;
		else if ((yyyy % 100) == 0)
			febDays = 28;
		else if ((yyyy % 4) == 0)
			febDays = 29;
		else
			febDays = 28;
		if (dd > febDays)
			return false;
		}
	else if (dd > daysInMonth[MM-1])
		return false;

	var MMM = MM != null ? [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ][MM-1] : null;

	var MMMMM = MM != null ? [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ][MM-1] : null;

	var E = null;

	if (yyyy != null && MM != null && dd != null) E = new Date(yyyy, MM-1, dd).getDay();

	var EEE = E != null ? [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ][E] : null;

	var EEEEEEE = E != null ? [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ][E] : null;

	var result = '';

	switch (dateOutputPattern)
	{
	case "MM/dd/yyyy":				if (MM && dd)    result = MM+'/'+dd+(yyyy != null ? '/'+yyyy : '');												break;
	case "dd/MM/yyyy":				if (dd && MM)    result = dd+'/'+MM+(yyyy != null ? '/'+yyyy : '');												break;
	case "MM/dd/yy":				if (MM && dd)    result = MM+'/'+dd+(yy != null ? '/'+yy : '');													break;
	case "dd/MM/yy":				if (dd && MM)    result = dd+'/'+MM+(yy != null ? '/'+yy : '');													break;
	case "yyyy/MM/dd":				if (MM && dd)    result = (yyyy != null ? yyyy+'/' : '')+MM+'/'+dd;												break;
	case "MM-dd-yyyy":				if (MM && dd)    result = MM+'-'+dd+(yyyy != null ? '-'+yyyy : '');												break;
	case "dd-MM-yyyy":				if (dd && MM)    result = dd+'-'+MM+(yyyy != null ? '-'+yyyy : '');												break;
	case "MM-dd-yy":				if (MM && dd)    result = MM+'-'+dd+(yy != null ? '-'+yy : '');													break;
	case "dd-MM-yy":				if (dd && MM)    result = dd+'-'+MM+(yy != null ? '-'+yy : '');													break;
	case "yyyy-MM-dd":				if (MM && dd)    result = (yyyy != null ? yyyy+'-' : '')+MM+'-'+dd;												break;
	case "dd MMM yyyy":				if (dd && MMM)   result = dd+' '+MMM+(yyyy != null ? ' '+yyyy : '');											break;
	case "MMM dd yyyy":				if (MMM && dd)   result = MMM+' '+dd+(yyyy != null ? ' '+yyyy : '');											break;
	case "dd MMMMM yyyy":			if (dd && MMMMM) result = dd+' '+MMMMM+(yyyy != null ? ' '+yyyy : '');											break;
	case "EEE, dd MMM yyyy":		if (dd && MMM)   result = (EEE != null ? EEE+', ' : '')+dd+' '+MMM+(yyyy != null ? ' '+yyyy : '');				break;
	case "EEEEEEE, dd MMMMM yyyy":	if (dd && MMMMM) result = (EEEEEEE != null ? EEEEEEE+', ' : '')+dd+' '+MMMMM+(yyyy != null ? ' '+yyyy : '');	break;
	}

	document.getElementById(dateHidden).value = result;

	return true;
	};

deconstructDate = function(dateHidden)
	{
	var dateValue = document.getElementById(dateHidden).value;

	var dateOutputPattern = document.getElementById(dateHidden+'_pattern').value;

	var MM, MMM, MMMMM, dd, yy, yyyy;

	var MMMs = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];

	var MMMMMs = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];

	switch (dateOutputPattern)
	{
	case "MM-dd-yyyy":
	case "MM/dd/yyyy":		
									MM = dateValue.substring(0, 2);
									dd = dateValue.substring(3, 5);
									yyyy = dateValue.substring(6, 10);
									yy = yyyy % 100;
									break;
	case "MM/dd/yy":
	case "MM-dd-yy":
									MM = dateValue.substring(0, 2);
									dd = dateValue.substring(3, 5);
									yy = dateValue.substring(6, 8);
									yyyy = 2000 + yy;	// y2k
									break;
	case "dd/MM/yy":
	case "dd-MM-yy":
									dd = dateValue.substring(0, 2);
									MM = dateValue.substring(3, 5);
									yy = dateValue.substring(6, 8);
									yyyy = 2000 + yy;	// y2k
									break;
	case "yyyy/MM/dd":
	case "yyyy-MM-dd":
									yyyy = dateValue.substring(0, 4);
									MM = dateValue.substring(5, 7);
									dd = dateValue.substring(8, 10);
									yy = yyyy % 100;
									break;
	case "dd/MM/yyyy":
	case "dd-MM-yyyy":
									dd = dateValue.substring(0, 2);
									MM = dateValue.substring(3, 5);
									yyyy = dateValue.substring(6, 10);
									yy = yyyy % 100;
									break;
	case "dd MMM yyyy":
									dd = dateValue.substring(0, 2);
									MMM = dateValue.substring(3, 6);
									MM = 1+MMM.indexOf(MMM);
									if (MM < 10) MM='0'+MM;
									yyyy = dateValue.substring(7, 11);
									yy = yyyy % 100;
									break;
	case "MMM dd yyyy":
									MMM = dateValue.substring(0, 3);
									MM = 1+MMMs.indexOf(MMM);
									if (MM < 10) MM='0'+MM;
									dd = dateValue.substring(4, 6);
									yyyy = dateValue.substring(7, 11);
									yy = yyyy % 100;
									break;
	case "dd MMMMM yyyy":
									dd = dateValue.substring(0, 2);
									MMMMM = dateValue.substring(3, dateValue.length-5);
									MM = 1+MMMMMs.indexOf(MMMMM);
									if (MM < 10) MM='0'+MM;
									yyyy = dateValue(dateValue.length-4, dateValue.length);
									yy = yyyy % 100;
									break;
	case "EEE, dd MMM yyyy":
									dd = dateValue.substring(dateValue.length-11, dateValue.length-9);
									MMM = dateValue.substring(dateValue.length-8, dateValue.length-5);
									MM = 1+MMMs.indexOf(MMM);
									if (MM < 10) MM='0'+MM;
									yyyy = dateValue.substring(dateValue.length-4, dateValue.length);
									yy = yyyy % 100;
									break;
	case "EEEEEEE, dd MMMMM yyyy":
									var ix = dateValue.indexOf(' ');
									dd = dateValue.substring(ix+1, ix+3);
									var iy = dateValue.lastIndexOf(' ');
									MMMMM = dateValue.substring(ix+3, iy);
									MM = 1+MMMMMs.indexOf(MMMMM);
									if (MM < 10) MM='0'+MM;
									yyyy = dateValue.substring(iy+1, iy+5);
									yy = yyyy % 100;
									break;
	}

	if (MM != null)
		{
		var mmE = document.getElementById(dateHidden+'_MM');
		if (mmE) mmE.value = MM;
		}

	if (dd != null)
		{
		var ddE = document.getElementById(dateHidden+'_dd');
		if (ddE) ddE.value = dd;
		var dayE = document.getElementById(dateHidden+'_Day');
		if (dayE) dayE.value = dd;
		}

	var yyE = document.getElementById(dateHidden+'_yy');
	if (yyE && yy != null) yyE.value = yy;

	var yyyyE = document.getElementById(dateHidden+'_yyyy');
	if (yyyyE && yyyy != null) yyyyE.value = yyyy;
	};
	
validateEmail = function (value)
	{
	value = value.replace(/^\s+/, '').replace(/\s+$/, '');
	// Return true if empty or is valid email
	if( value.length > 0 )
		return isEmailAddress(value);
	return true;
	};

validateNoRoleNoPublicEmail = function (value)
	{
	return validateNoPublicEmail(value) && validateNoRoleEmail(value);
	}

var publicEmailPatterns = [
	/@163\.com/i, /@aol\.com/i, /@bellsouth\.net/i, /@btconnect\.com/i, /@charter\.com/i, /@comcast\.net/i,
	/@cox\.net/i, /@earthlink\.net/i, /@email\.com/i, /@gmail\.co/i, /@gmail\.com/i, /@hotmail\.co\.../i, /@hotmail\.com/i,
	/@juno\.com/i, /@mail\.com/i, /@mail\.ru/i, /@mindspring\.com/i, /@msn\.com/i, /@orange\.fr/i, /@rogers\.com/i,
	/@sbcglobal\.net/i, /@shaw\.ca/i, /@sympatico\.ca/i, /@telus\.net/i, /@verizon\.net/i, /@yahoo\.ca/i, /@yahoo\.co\.../i,
	/@yahoo\.com/i, /@ymail\.com/i, /@virgin\.net/i, /@virginmedia\.com/i, /@ntlworld\.com/i, /@blueyonder\.co\.../i,
	/@icloud\.com/i, /@me\.com/i, /@mac\.com/i];

validateNoPublicEmail = function (value)
	{
	value = value.replace(/^\s+/, '').replace(/\s+$/, '');
	// Return true if empty or is valid email
	if (value.length <= 0)
		return true;

	for (var i = 0; i < publicEmailPatterns.length; ++i)
		if (value.match(publicEmailPatterns[i]))
			return false;

	return isEmailAddress(value);
	};

var roleEmailPatterns = [
	/^abuse@/i, /^admin@/i, /^administrator@/i, /^anti-spam@/i, /^antispam@/i, /^billing@/i, /^contact@/i, /^customerservice@/i,
	/^designer@/i, /^info@/i, /^it@/i, /^hostmaster@/i, /^lawyer@/i, /^mail-daemon@/i, /^mail-deamon@/i, /^marketing@/i,
	/^no-reply@/i, /^noreplies@/i, /^noreply@/i, /^nospam@/i, /^postmaster@/i, /^returns@/i, /^root@/i, /^sales@/i,
	/^spam@/i, /^support@/i ];


validateNoRoleEmail = function (value)
	{
	value = value.replace(/^\s+/, '').replace(/\s+$/, '');
	// Return true if empty or is valid email
	if (value.length <= 0)
		return true;

	for (var i = 0; i < roleEmailPatterns.length; ++i)
		if (value.match(roleEmailPatterns[i]))
			return false;

	return isEmailAddress(value);
	};

function validatePhoneNumberLength(value)
	{
	var digits = value.replace(/[^\d.]/g, "");
	var digitsLength = digits.length;
	return  !(digitsLength < 7 || digitsLength > 15);
	}

validateIntlPhone = function (value)
	{
	if (!value || value.length == 0)
		return true;

	if (!validatePhoneNumberLength(value))
		return false;

	//http://blog.stevenlevithan.com/archives/validate-phone-number
	//var regex = /^\+(?:[0-9] ?){6,14}[0-9]$/;

	// AO-8389
	//http://www.karlhorky.com/2010/07/jquery-international-phone-number.html
	var regex = /^((\+)?[1-9]{1,2})?([-\s\.])?(\(\d\)[-\s\.]?)?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}(\s*(ext|x)\s*\.?:?\s*([0-9]+))?$/;
	return value.match(regex);
	}
	
validateAnyPhone = function (value)
	{
	if (!value || value.length == 0) 
		return true;

	return validateUSPhone(value) || validateIntlPhone(value);
	}
	
validateUSPhone = function (value)
	{
	if (!value || value.length == 0) 
		return true;

	if (!validatePhoneNumberLength(value))
		return false;

	// http://stackoverflow.com/questions/123559/a-comprehensive-regex-for-phone-number-validation
	var regPhone = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
	return  value.match(regPhone);
	};
	
//	Field validator registry
//	these are exposed to the end user in the choice dropdown
var	validators = [ 
   	[ "Should be a number",											"NUMBER",		validateNumber,					"Please enter a number." ],
   	[ "Should be a phone number",									"ANYPHONE",		validateAnyPhone,				"Please enter a valid phone number." ],   	
   	[ "Should be an email address",									"EMAIL",		validateEmail,					"Please enter a valid email address." ],
   	[ "Should be a non-role-based email address",					"NREMAIL",		validateNoRoleEmail,			"Please enter a valid, non-role-based email address." ],
   	[ "Should be a non-consumer email address",						"NPEMAIL",		validateNoPublicEmail,			"Please enter a valid, non-consumer email address." ],
   	[ "Should be a non-consumer, and non-role-based email address",	"NPREMAIL",		validateNoRoleNoPublicEmail,	"Please enter a valid, non-consumer, non-role-based email address." ],
   	[ "Should be a US phone number",								"USPHONE",		validateUSPhone,				"Please enter a valid US phone number." ],
   	[ "Should be an international phone number",					"INTLPHONE",	validateIntlPhone,				"Please enter a valid international phone number." ]
 	];

//	these can be used, but are not presented in the dropdown

var implicitValidators = [
	[ "Between range",														"RANGE",		implicitValidateNumberRange	],
	[ "Don't exceed maximum length",										"LENGTH",		implicitValidateLength		],
	[ "Please verify that the highlighted field matches the field below it","CONFIRM",		implicitValidateConfirm		],
	[ "Should be a valid date",												"DATE",			implicitValidateDate		]
	];
	
//	Password Field Checker

doubleCheck = function (idPrimaryField, idCheckerField, idLabel)
	{
	
	var primary = document.getElementById(idPrimaryField);
	var checker = document.getElementById(idCheckerField);
	
	var label   = document.getElementById(idLabel);
	
	if (!primary) return;
	if (!checker) return;
	
	if (primary.value != checker.value)
		label.className = 'formFieldLabelBad';
	else
		label.className = 'formFieldLabelGood';
	};
	
	
//	Text Field Checker

singleCheck = function (idField, validationType, idLabel)
	{
	var value = document.getElementById(idField).value;
	var label = document.getElementById(idLabel);

	if (!value) return;
	
	for (var i = 0; i < validators.length; i++)
		{
		var validationValue 	= validators[i][1];
		var validationFunction	= validators[i][2];

		if (validationValue == validationType)
			{
			if (validationFunction (value))
				label.className = 'formFieldLabelGood';
			else
				label.className = 'formFieldLabelBad';
			break;
			}
		}
	};
	

