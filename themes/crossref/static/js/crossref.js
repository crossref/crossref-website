jQuery(document).ready(function ($) {

  $('#contact-form').on('submit', function(e) {
    if(grecaptcha.getResponse() == "") {
      e.preventDefault();
      // show a nice error
      $('#contact-form #html_element').after( "<p class='error' id='captcha_error'>You must tick the reCaptcha above to proceed.</p>" );
      
    } else {
      // just let the form submit
      //alert("Thank you");
      $('#captcha_error').hide();
    }
});

  /** 
  * documentation menu mobile handling
  */
  function documentationmobile() {
    w = $(window).width(),
    stickyNav = $('.nav-sticky');


    if (w <= 991) {
      $(".nav-tabs").hide();
      $("#documentation-nav-burger").show();
    } else {
      $(".nav-tabs").show();
      $("#documentation-nav-burger").hide();
    }

    $("#documentation-nav-burger").on("click", function() {
      //console.log('clicked');
      $(".nav-tabs").toggle();
    });
  };


  

  /**
  * and prev next on bootstrap tabs
  */
  $('.btnNext').text($('#v-tab > .active').last().nextAll('a').not('.subsub').first().text());
  $('.btnPrevious').text($('#v-tab  > .active').prevAll('a').not('.subsub').first().text());

  $('.btnNext').attr("href", ($('#v-tab > .active').last().nextAll('a').not('.subsub').first().attr("href")));
  $('.btnPrevious').attr("href", ($('#v-tab > .active').prevAll('a').not('.subsub').first().attr("href")));
 

  stickybacknav();
  documentationmobile();
  $(window).resize(function() {
    stickybacknav();
    documentationmobile();
  });

  function stickybacknav () {
    var documentation_nav = $('#documentation-sticky').offset()


    if (typeof documentation_nav !== 'undefined') {
      documentation_nav = documentation_nav.top;
    }

    var documentation_width =  $('#documentation-nav').width();
    $('#documentation-sticky').css({width: documentation_width});

    $(window).scroll(function() {                  // assign scroll event listener

        var currentScroll = $(window).scrollTop(); // get current position
        //console.log(currentScroll);

        
        if (currentScroll >= documentation_nav) {           // apply position: fixed if you
            
            $('#documentation-sticky').css({                      // scroll to that element or below it
                position: 'fixed',
                top: 0,
                width: documentation_width 
            });

        } else {                                   // apply position: static
            
            $('#documentation-sticky').css({                      // if you scroll above it
                position: 'relative',
                width: documentation_width,
            });
        }

    });
  };


  /**
  * contact form filtering
  **/

  $( "#contact-form-submit" ).on("click", function( event ) {

    var profanities = $("#profanity_filter").val().split(",");

    // lose the last one
    if (profanities.length > 0 && profanities[profanities.length - 1].length == 0) {
      profanities.pop();
    }
    //console.log(profanities);

    // now check through all the text elements for profanities
    $("#contact-form input:text, #contact-form textarea").each(function(i,obj) {

      //console.log($(obj));

      var found = false;
      $(obj)[0].setCustomValidity('');
      $.each(profanities, function(i,profanity) {
        var n = $(obj).val().toLowerCase().indexOf(profanity.toLowerCase());
        if (n > -1) {
          found = true;
          $(obj)[0].setCustomValidity('Profanity/block word detected');
        }
      })
    })
    
    //console.log( "form submitted" );
    if (this.form.checkValidity()) {
      console.log("it's valid");
    } else {
      event.preventDefault();
      this.form.reportValidity();
    }
    
  });

  /**
  * contact form display text for joining option
  */

  $("#contact_select").change(function() {
    var id = $(this).children(":selected").attr("id");    
    //contact_joining_text
    if (id == 'jc') {
      $("#contact_joining_text").show();
    } else {
      $("#contact_joining_text").hide();
    }   

    if (id == 'tscr') {
      $("#contact_content_reg_method").attr('required',true);
     $("#contact_techsupport_reg").show();
    } else {
      $("#contact_content_reg_method").removeAttr('required');
      $("#contact_techsupport_reg").hide();
    }  
  });

  var urlParams = new URLSearchParams(window.location.search);
  //console.log(urlParams.toString());
  
  $option_selected = parseInt(urlParams.getAll('option'));
  $('#contact_select option:eq(' + $option_selected + ')').attr('selected', 'selected');
  $("#contact_select").change();

  //$('[name = "_referrer_url"]').val(urlParams.getAll('url'));

  var initreferrer = document.referrer;
  $('[name = "_referrer_url"]').val(initreferrer);
  

  $("#contact_content_reg_method").change(function() {
    var id = $(this).children(":selected").attr("id");    
    //contact_joining_text
    if (id == 'crm_other') {
      $("#contact_reg_method_other_block").show();
    } else {
      $("#contact_reg_method_other_block").hide();
    }   
  });


  

  /**
  * Some jQuery datatables
  */

  $('.member-list').DataTable({
    "paging": true,
    "searching": true, 
    "buttons": [
      'csv',
    ],   
    "sDom": '<"top"<"info"lfpi><"actions"B><"clear">>rt<"bottom">',
    "infoCallback": function( settings, start, end, max, total, pre ) {
      return "Total rows " + total;
    }
  });

  $('.member-list-0').DataTable({
    "paging": true,
    "searching": true, 
    "buttons": [
      'csv',
    ],   
    "sDom": '<"top"<"info"lfpi><"actions"B><"clear">>rt<"bottom">',
    "infoCallback": function( settings, start, end, max, total, pre ) {
      return "Total rows " + total;
    }
  });


  /**
  * Search stuff
  */

  var mobiSearchTrigger = $('.search-trigger'),
      mobiSearchBar = $('.hcwh-box-top'),
      hcwhInner = $('.hcwh-inner');

  mobiSearchTrigger.click(function(){
    var  $this = $(this);

    $this.toggleClass('active');
    mobiSearchBar.slideToggle(function(){
      hcwhInner.focus();
    });
  });

  $.cookieBar({
    fixed: true
  });

  var hcwhIcon = $('.hcwh-icon'),
  hcwhMobileDd = $('.hcwh-mobile-dd'),
  hcwhInnerMob = $('.hcwh-inner-mobile'),
  hcwhInner = $('.hcwh-inner');

  hcwhIcon.click(function () {
    hcwhMobileDd.slideToggle('fast', function () {
      hcwhInnerMob.focus();
    });

    var $this = $(this),
    src = $this.find('img').attr('src');

    src.attr('src', '')
  });


  /**
  * This block of code controls the New Desktop Nav
  */

  var leftNav = $('.navigation-left'),
  subMenuItem = leftNav.find('.sub-menu'),
  subSubMenuItem = leftNav.find('.sub-sub-menu'),
  h = $(document).height(),
  w = $(window).width(),
  stickyNav = $('.nav-sticky');


  if (w > 750) {
    stickyNav.stick_in_parent();
  }


  // Controls the reveal and setting width classes of navigation.

  subMenuItem.hover(function () {
    var _this = $(this),
    childItems = _this.find('.child-items');

    leftNav.delay(1000).toggleClass('wide');

    childItems.toggle();
  });

  // subSubMenuItem.hover(function () {
  //
  //     var _this = $(this),
  //         childChildItems = _this.find('.child-child-items');
  //
  //     leftNav.delay(1000).toggleClass('ultra-wide');
  //
  //     childChildItems.toggle();
  // });

  var menuIcon = $('.menu-icon'),
  burger = $('.menu-icon'),
  navLeft = $('.left-nav'),
  mobileMenu = $('.mobile-menu');

   /**
   * New navigation functionality, https://www.alimontgomery.com/crossref/new-nav-2.html
   */

  $('[data-toggle-nav]').each(function(){
    
    var $button = $(this);
    var target = $button.data('toggle-nav');
    var $target = $('#'+target);
    $(this).click(function(){
      $('.nav-burger').toggleClass('open'); 
      $target.toggleClass('nav-container--hidden', 500);
      $('body').toggleClass('stop-scroll');
    });



  });

  $('[data-toggle-sibling]').each(function(e){
    $(this).click(function(){

      var $not = $('[data-toggle-sibling]').not(this);

      $not.each(function(){
        $(this).next().removeClass('child-list--show');  
        $('.minus', $(this)).removeClass('rotate-minus');  
      })

      $(this).next().toggleClass("child-list--show");
      $('.minus', $(this)).toggleClass('rotate-minus');  
    });
  });

 $(document).mouseup(function(e) 
  {
      var container = $(".navigation");

      // if the target of the click isn't the container nor a descendant of the container
      if (!container.is(e.target) && container.has(e.target).length === 0) 
      {
          
        $(".nav-burger").removeClass("open");
        if (!$(".navigation").hasClass('mobile-menu')) {
          menu.css({
            left: "-250px"
          }, 500);
        } else {
          //mobileMenu.toggle();
        }
      }
  });
  
  burger.click(function() {
    
    // just do the showmenu slide in and out if full size
    if (!$(".navigation").hasClass('mobile-menu')) {
      toggleMenu();
      
    } else {
      // 
      mobileMenu.toggle();
      $('.navigation').css({
        left: "0"
      });
      if (mobileMenu.css('display') == "block"){
        $(".nav-burger").addClass("open");
      } else {
        $(".nav-burger").removeClass("open");
      }
    }
  })




  // Adds the reveal functionality to the mobile menu.


  var mobileArrow = $('.menu-arrow'),
  thirdLvlMenuArrow = $('.third-lvl-menu-arrow');

  mobileArrow.on('click', function () {

    var $this = $(this);
    mobileChildren = $this.closest('.mobile-sub-menu').find('.bb-items');

    $this.toggleClass('active');
    mobileChildren.slideToggle('fast');

  });

  // thirdLvlMenuArrow.on('click', function () {
  //
  //     var $this = $(this);
  //     mobileChildren = $this.closest('.sub-sub-menu').find('.child-child-items');
  //
  //     $this.toggleClass('active');
  //     mobileChildren.slideToggle('fast');
  // });


  /**
  * This block of code controls the Reveal functionality
  */

  var menu = $('.navigation'),
  menuTimeout = null,
  showArea = 30,
  peakArea = 125,
  $window = $(window);


  //Hide menu on page init

  if ($window.width() < 570) {
    $( ".hcwh-box-top" ).insertAfter( ".site-header" );
  }

  if ($window.width() > 750) {

    menu.css({
      left: "-250px"
    });

    //$window.on('mousemove', mouseMoveHandler);
    //$window.on('touchmove', mouseMoveHandler);
  }

  function peakMenu() {
    menu.css({
      left: "-215px"
    });
  }

  function toggleMenu() {

    left = menu.css("left");
    if (left == "-250px"){
      newleft = "0";
      $(".nav-burger").addClass("open");
    } else {
      newleft = "-250px";
      $(".nav-burger").removeClass("open");
    }
    menu.css({
      left: newleft
    }, 500);

  }

  function hideMenu() {
    menu.css({
      left: "-235px"
    });
  }

  function mouseMoveHandler(e) {



    if (e.pageX < peakArea) {
      //if its clear the user is going to the left hand side of the window
      //show a peak out from the side
      clearTimeout(menuTimeout);
      menuTimeout = null;
      peakMenu();
    }

    if (e.pageX < showArea || menu.is(':hover')) {
      // Show the menu if mouse is within 250 pixels
      // from the left or we are hovering over it
      clearTimeout(menuTimeout);
      menuTimeout = null;
      showMenu();
    } else if (menuTimeout === null) {
      // Hide the menu if the mouse is further than 20 pixels
      // from the left and it is not hovering over the menu
      // and we aren't already scheduled to hide it
      menuTimeout = setTimeout(hideMenu, 300);
    }
  }
});

(function ($) {


  /**
  * This block of code controls the collapse functionality New Mobile Navigation
  *
  */

  $.fn.replaceClass = function (pFromClass, pToClass) {
    return this.removeClass(pFromClass).addClass(pToClass);
  };

  var w = $(window),
  navigation = $('.navigation'),
  subMenuItem = navigation.find('.sub-menu'),
  childItems = navigation.find('.child-items'),
  mobileBreakPoint = 750;

  function resize() {

    if (w.width() < mobileBreakPoint) {

      return navigation.addClass('mobile-menu');

    }

    navigation.removeClass('mobile-menu');
  }

  w.resize(resize).trigger('resize');


  var mobileMenu = $('.mobile-menu'),
  mobileSubMenu = $('.mobile-sub-menu');

  w.resize(function () {

    if (navigation.hasClass('mobile-menu')) {

      navigation.removeClass('navigation-left');
      subMenuItem.replaceClass('sub-menu', 'mobile-sub-menu');
      childItems.replaceClass('child-items', 'mobile-child-items');

    } else {

      navigation.addClass('navigation-left');
      subMenuItem.replaceClass('mobile-sub-menu', 'sub-menu');
      childItems.replaceClass('mobile-child-items', 'child-items');

    }

  });

  if (w.width() < mobileBreakPoint) {
    navigation.removeClass('navigation-left');
    subMenuItem.replaceClass('sub-menu', 'mobile-sub-menu');
    childItems.replaceClass('child-items', 'mobile-child-items');
  } else {
    navigation.addClass('navigation-left');
    subMenuItem.replaceClass('mobile-sub-menu', 'sub-menu');
    childItems.replaceClass('mobile-child-items', 'child-items');
  }

})(jQuery);


(function ($) {

  e = jQuery.Event("keypress");
  e.which = 13;

  $('#howcanwehelpx').on('input', function () {
    $("#howcanwehelpx").keypress().trigger(e);
    var input = $(this).val();
    if (input.length >= 3) {
      console.log("showing results");
      $('#results').show();
    } else {
      console.log("hiding results");
      $('#results').hide();
    }

  });
})(jQuery);


(function ($) {

  /*global jQuery */
  /*!
  * FitText.js 1.2
  *
  * Copyright 2011, Dave Rupert http://daverupert.com
  * Released under the WTFPL license
  * http://sam.zoy.org/wtfpl/
  *
  * Date: Thu May 05 14:23:00 2011 -0600
  */
  $.fn.fitText = function (kompressor, options) {

    // Setup options
    var compressor = kompressor || 1,
    settings = $.extend({
      'minFontSize': Number.NEGATIVE_INFINITY,
      'maxFontSize': Number.POSITIVE_INFINITY
    }, options);

    return this.each(function () {

      // Store the object
      var $this = $(this);

      // Resizer() resizes items based on the object width divided by the compressor * 10
      var resizer = function () {
        fontsize = Math.max(Math.min($this.width() / (compressor * 10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize));
        $this.css( {
          'font-size': fontsize,
          'height':fontsize,
          'line-height': fontsize + 'px'
        } );
      };

      // Call once to set.
      resizer();

      // Call on resize. Opera debounces their resize by default.
      $(window).on('resize.fittext orientationchange.fittext', resizer);

    });

  };

  $.fn.digits = function () {
    return this.each(function () {
      $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    })
  }

})(jQuery);

// Floating label headings for the contact form
(function ($) {
  $("body").on("input propertychange", ".floating-label-form-group", function (e) {
    $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
  }).on("focus", ".floating-label-form-group", function () {
    $(this).addClass("floating-label-form-group-with-focus");
  }).on("blur", ".floating-label-form-group", function () {
    $(this).removeClass("floating-label-form-group-with-focus");
  });

})(jQuery);

// Navigation Scripts to Show Header on Scroll-Up
jQuery(document).ready(function ($) {
  var MQL = 1170;

  $("section#search").css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0});

  $cookietab = Cookies.get('activetab');

  $("#tabs-search").each(function () {
    // For each set of tabs, we want to keep track of
    // which tab is active and its associated content
    var $active, $content, $links = $(this).find('a');


    // If the location.hash matches one of the links, use that as the active tab.
    // If no match is found, use the first link as the initial active tab.
    $linkhash = '';
    if (location.hash != '') {
      $linkhash = location.hash;
    } else if ($cookietab !='') {
      $linkhash = $cookietab;
    }
    $active = $($links.filter('[href="' + $linkhash + '"]')[0] || $links[0]);
    $active.addClass('active');
    $active.parent().addClass('active');

    $content = $($active[0].hash);

    // Hide the remaining content
    $links.not($active).each(function () {
      $(this.hash).hide();
    });

    // Bind the click event handler
    $(this).on('click', 'a', function (e) {


      // Make the old tab inactive.
      $active.removeClass('active');
      $active.parent().removeClass('active');
      $content.hide();

      // Update the variables with the new link and content
      $active = $(this);
      $content = $(this.hash);


      Cookies.set('activetab', this.hash) ;

      // Make the tab active.
      $active.addClass('active');

      $active.parent().addClass('active');
      $content.show();

      $content.find("input").attr("value", $searchstring);

      // Prevent the anchor's default click action
      e.preventDefault();
    });
  });

  // rotate thru the colors on the dashboard counts
  var colors = ["#d8d2c4", "#ef3340", "#4f5858", "#ffc72c", "#3eb1c8"];
  $(".dashboard-counts").css("background-color", function (index) {
    return colors[index % colors.length];
  });

  // make the dashboard count fill the space
  $(".bignumber").digits().fitText(1.2);

  // or maybe just give it some commas
  $(".withcommas").digits();

  //primary navigation slide-in effect
  if ($(window).width() > MQL) {
    var headerHeight = $('.navbar-custom').height();
    $(window).on('scroll', {
      previousTop: 0
    },
    function () {
      var currentTop = $(window).scrollTop();
      //check if user is scrolling up
      if (currentTop < this.previousTop) {
        //if scrolling up...
        if (currentTop > 0 && $('.navbar-custom').hasClass('is-fixed')) {
          $('.navbar-custom').addClass('is-visible');
        } else {
          $('.navbar-custom').removeClass('is-visible is-fixed');
        }
      } else {
        //if scrolling down...
        $('.navbar-custom').removeClass('is-visible');
        if (currentTop > headerHeight && !$('.navbar-custom').hasClass('is-fixed')) $('.navbar-custom').addClass('is-fixed');
      }
      this.previousTop = currentTop;
    });
  }
});



(function($) {

  // auto open if supplied in hash
  let url = location.href.replace(/\/$/, "");
  const hash = url.split("#");

  setTimeout(function() {
    $("#" + hash[1]).trigger('click');
  },10);

  var allPanels = $('.accordion > dd').hide();
  var allHeaders = $('.accordion > dt');
    
  $('.accordion > dt > a').click(function() {
      $this = $(this);
      $header = $this.parent();
      $target =  $this.parent().next();

      if(!$target.hasClass('open')){
         allPanels.removeClass('open').slideUp();
         allHeaders.removeClass('open');
         $target.addClass('open').slideDown();
         $header.addClass('open');
      } else if($target.hasClass('open')){
         $target.removeClass('open').slideUp();
         $header.removeClass('open');
      }
      
    return false;
  });

})(jQuery);


$(function () {
    $(".member-list").stickyTableHeaders();
});

/*! Copyright (c) 2011 by Jonas Mosbech - https://github.com/jmosbech/StickyTableHeaders
  MIT license info: https://github.com/jmosbech/StickyTableHeaders/blob/master/license.txt */

;
(function ($, window, undefined) {
    'use strict';

    var name = 'stickyTableHeaders',
        id = 0,
        defaults = {
            fixedOffset: 0,
            leftOffset: 0,
            marginTop: 0,
            scrollableArea: window
        };

    function Plugin(el, options) {
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;

        // Access to jQuery and DOM versions of element
        base.$el = $(el);
        base.el = el;
        base.id = id++;
        base.$window = $(window);
        base.$document = $(document);

        // Listen for destroyed, call teardown
        base.$el.bind('destroyed',
        $.proxy(base.teardown, base));

        // Cache DOM refs for performance reasons
        base.$clonedHeader = null;
        base.$originalHeader = null;

        // Keep track of state
        base.isSticky = false;
        base.hasBeenSticky = false;
        base.leftOffset = null;
        base.topOffset = null;

        base.init = function () {
            base.$el.each(function () {
                var $this = $(this);

                // remove padding on <table> to fix issue #7
                $this.css('padding', 0);

                base.$originalHeader = $('thead:first', this);
                base.$clonedHeader = base.$originalHeader.clone();
                $this.trigger('clonedHeader.' + name, [base.$clonedHeader]);

                base.$clonedHeader.addClass('tableFloatingHeader');
                base.$clonedHeader.css('display', 'none');

                base.$originalHeader.addClass('tableFloatingHeaderOriginal');

                base.$originalHeader.after(base.$clonedHeader);

                base.$printStyle = $('<style type="text/css" media="print">' +
                    '.tableFloatingHeader{display:none !important;}' +
                    '.tableFloatingHeaderOriginal{position:static !important;}' +
                    '</style>');
                $('head').append(base.$printStyle);
            });

            base.setOptions(options);
            base.updateWidth();
            base.toggleHeaders();
            base.bind();
        };

        base.destroy = function () {
            base.$el.unbind('destroyed', base.teardown);
            base.teardown();
        };

        base.teardown = function () {
            if (base.isSticky) {
                base.$originalHeader.css('position', 'static');
            }
            $.removeData(base.el, 'plugin_' + name);
            base.unbind();

            base.$clonedHeader.remove();
            base.$originalHeader.removeClass('tableFloatingHeaderOriginal');
            base.$originalHeader.css('visibility', 'visible');
            base.$printStyle.remove();

            base.el = null;
            base.$el = null;
        };

        base.bind = function () {
            base.$scrollableArea.on('scroll.' + name, base.toggleHeaders);
            if (!base.isWindowScrolling) {
                base.$window.on('scroll.' + name + base.id, base.setPositionValues);
                base.$window.on('resize.' + name + base.id, base.toggleHeaders);
            }
            base.$scrollableArea.on('resize.' + name, base.toggleHeaders);
            base.$scrollableArea.on('resize.' + name, base.updateWidth);
        };

        base.unbind = function () {
            // unbind window events by specifying handle so we don't remove too much
            base.$scrollableArea.off('.' + name, base.toggleHeaders);
            if (!base.isWindowScrolling) {
                base.$window.off('.' + name + base.id, base.setPositionValues);
                base.$window.off('.' + name + base.id, base.toggleHeaders);
            }
            base.$scrollableArea.off('.' + name, base.updateWidth);
        };

        base.toggleHeaders = function () {
            if (base.$el) {
                base.$el.each(function () {
                    var $this = $(this),
                        newLeft,
                        newTopOffset = base.isWindowScrolling ? (
                        isNaN(base.options.fixedOffset) ? base.options.fixedOffset.outerHeight() : base.options.fixedOffset) : base.$scrollableArea.offset().top + (!isNaN(base.options.fixedOffset) ? base.options.fixedOffset : 0),
                        offset = $this.offset(),

                        scrollTop = base.$scrollableArea.scrollTop() + newTopOffset,
                        scrollLeft = base.$scrollableArea.scrollLeft(),

                        scrolledPastTop = base.isWindowScrolling ? scrollTop > offset.top : newTopOffset > offset.top,
                        notScrolledPastBottom = (base.isWindowScrolling ? scrollTop : 0) < (offset.top + $this.height() - base.$clonedHeader.height() - (base.isWindowScrolling ? 0 : newTopOffset));

                    if (scrolledPastTop && notScrolledPastBottom) {
                        newLeft = offset.left - scrollLeft + base.options.leftOffset;
                        base.$originalHeader.css({
                            'position': 'fixed',
                                'margin-top': base.options.marginTop,
                                'left': newLeft,
                                'z-index': 3 // #18: opacity bug
                        });
                        base.leftOffset = newLeft;
                        base.topOffset = newTopOffset;
                        base.$clonedHeader.css('display', '');
                        if (!base.isSticky) {
                            base.isSticky = true;
                            // make sure the width is correct: the user might have resized the browser while in static mode
                            base.updateWidth();
                        }
                        base.setPositionValues();
                    } else if (base.isSticky) {
                        base.$originalHeader.css('position', 'static');
                        base.$clonedHeader.css('display', 'none');
                        base.isSticky = false;
                        base.resetWidth($('td,th', base.$clonedHeader), $('td,th', base.$originalHeader));
                    }
                });
            }
        };

        base.setPositionValues = function () {
            var winScrollTop = base.$window.scrollTop(),
                winScrollLeft = base.$window.scrollLeft();
            if (!base.isSticky || winScrollTop < 0 || winScrollTop + base.$window.height() > base.$document.height() || winScrollLeft < 0 || winScrollLeft + base.$window.width() > base.$document.width()) {
                return;
            }
            base.$originalHeader.css({
                'top': base.topOffset - (base.isWindowScrolling ? 0 : winScrollTop),
                    'left': base.leftOffset - (base.isWindowScrolling ? 0 : winScrollLeft)
            });
        };

        base.updateWidth = function () {
            if (!base.isSticky) {
                return;
            }
            // Copy cell widths from clone
            if (!base.$originalHeaderCells) {
                base.$originalHeaderCells = $('th,td', base.$originalHeader);
            }
            if (!base.$clonedHeaderCells) {
                base.$clonedHeaderCells = $('th,td', base.$clonedHeader);
            }
            var cellWidths = base.getWidth(base.$clonedHeaderCells);
            base.setWidth(cellWidths, base.$clonedHeaderCells, base.$originalHeaderCells);

            // Copy row width from whole table
            base.$originalHeader.css('width', base.$clonedHeader.width());
        };

        base.getWidth = function ($clonedHeaders) {
            var widths = [];
            $clonedHeaders.each(function (index) {
                var width, $this = $(this);

                if ($this.css('box-sizing') === 'border-box') {
                    width = $this[0].getBoundingClientRect().width; // #39: border-box bug
                } else {
                    var $origTh = $('th', base.$originalHeader);
                    if ($origTh.css('border-collapse') === 'collapse') {
                        if (window.getComputedStyle) {
                            width = parseFloat(window.getComputedStyle(this, null).width);
                        } else {
                            // ie8 only
                            var leftPadding = parseFloat($this.css('padding-left'));
                            var rightPadding = parseFloat($this.css('padding-right'));
                            // Needs more investigation - this is assuming constant border around this cell and it's neighbours.
                            var border = parseFloat($this.css('border-width'));
                            width = $this.outerWidth() - leftPadding - rightPadding - border;
                        }
                    } else {
                        width = $this.width();
                    }
                }

                widths[index] = width;
            });
            return widths;
        };

        base.setWidth = function (widths, $clonedHeaders, $origHeaders) {
            $clonedHeaders.each(function (index) {
                var width = widths[index];
                $origHeaders.eq(index).css({
                    'min-width': width,
                        'max-width': width
                });
            });
        };

        base.resetWidth = function ($clonedHeaders, $origHeaders) {
            $clonedHeaders.each(function (index) {
                var $this = $(this);
                $origHeaders.eq(index).css({
                    'min-width': $this.css('min-width'),
                        'max-width': $this.css('max-width')
                });
            });
        };

        base.setOptions = function (options) {
            base.options = $.extend({}, defaults, options);
            base.$scrollableArea = $(base.options.scrollableArea);
            base.isWindowScrolling = base.$scrollableArea[0] === window;
        };

        base.updateOptions = function (options) {
            base.setOptions(options);
            // scrollableArea might have changed
            base.unbind();
            base.bind();
            base.updateWidth();
            base.toggleHeaders();
        };

        // Run initializer
        base.init();
    }

    // A plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[name] = function (options) {
        return this.each(function () {
            var instance = $.data(this, 'plugin_' + name);
            if (instance) {
                if (typeof options === 'string') {
                    instance[options].apply(instance);
                } else {
                    instance.updateOptions(options);
                }
            } else if (options !== 'destroy') {
                $.data(this, 'plugin_' + name, new Plugin(this, options));
            }
        });
    };

})(jQuery, window);



// subscribe form validation specific messages.
/*jQuery(document).ready(function ($) {
  $("#subscribe-form").validate();

  $.extend(jQuery.validator.messages, {
    required: "Please enter an email address.",
    email: "Please enter a valid email address."
  });

});*/

document.addEventListener('DOMContentLoaded', function () {
  loadVimeoPlayerScript(function() {
    const players = {};

    // Initialize all players on the page with default videos
    document.querySelectorAll('.vimeo-player-container').forEach(container => {
      const playerId = container.getAttribute('id');
      const playerElement = document.getElementById(playerId);
      const defaultVideoId = container.getAttribute('data-default-video');

      if (playerElement) {
        const player = new Vimeo.Player(playerElement, {
          id: defaultVideoId,
          responsive: true
        });

        players[`vimeo-${playerId}`] = player;
      } else {
        console.error('Player element not found for ID:', playerId);
      }
    });

    // Setup event listeners for changing videos
    document.querySelectorAll('.video-language-button').forEach(button => {
      button.addEventListener('click', function(event) {
        event.preventDefault();
        const videoId = button.getAttribute('data-videoid');
        const playerContainerId = button.getAttribute('data-playerid');
        const player = players[`vimeo-${playerContainerId}`];
        player.loadVideo(videoId).then(function(id) {
          console.log('Video successfully loaded');
        }).catch(function(error) {
          console.error('Error loading video:', error);
        });
      });
    });
  });
});

function loadVimeoPlayerScript(callback) {
  var scriptId = 'vimeo-player-api'; // Unique ID to prevent multiple inclusions

  var existingScript = document.getElementById(scriptId);
  if (!existingScript) {
    var script = document.createElement('script');
    script.id = scriptId;
    script.type = 'text/javascript';
    script.async = true;
    script.src = 'https://player.vimeo.com/api/player.js';
    script.onload = function() {
      console.log("Vimeo API script loaded successfully.");
      callback(); // Execute the callback only after the script is fully loaded
    };
    document.head.appendChild(script);
  } else if (existingScript && existingScript.readyState && (existingScript.readyState === 'complete' || existingScript.readyState === 'loaded')) {
    // This checks for older IE browsers where onload may not fire properly.
    callback(); // Execute callback if script is already loaded and readyState is complete or loaded
  } else {
    existingScript.addEventListener('load', callback); // Attach callback if script exists but not yet loaded
  }
}
